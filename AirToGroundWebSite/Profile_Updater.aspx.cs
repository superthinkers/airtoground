﻿using System;
using ATGDB;

public partial class Profile_Updater : System.Web.UI.Page
{
    [System.Web.Services.WebMethod]
    public static void UpdateProfile(string keys) {
        // Get the division.
        ATGDB.Companydivision div = Companydivision.GetCompanyDivisionById(keys.Substring(0, keys.IndexOf(":")));
        
        // Get the customer.
        ATGDB.Customer cust = Customer.GetCustomerById(keys.Substring(keys.IndexOf(":") + 1,
            keys.Length - keys.IndexOf(":") - 1));
        
        // Update Profile.
        ProfileCommon p = (ProfileCommon)System.Web.HttpContext.Current.Profile;
        p.Companydivisionid = div.Companydivisionid.ToString();
        p.Companydivisiondescription = div.Description;
        p.Companydivisionlocation = div.Location;
        p.Customerid = cust.Customerid.ToString();
        p.Customerdescription = cust.Description;
        //Profile.SiteTheme = cust.Sitetheme;
        p.Save();
    }
}