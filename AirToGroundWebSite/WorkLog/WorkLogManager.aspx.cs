﻿using System;
using System.Linq;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class WorkLog_WorkLogManager : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_WORKLOG_MANAGER;
    }
    // For postbacks in the JavaScript in the page.
    protected string PostBackString;
    protected string SearchPostBackString;

    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // For postbacks in the JavaScript in the page.
        PostBackString = Page.ClientScript.GetPostBackEventReference(this, "");
        SearchPostBackString = Page.ClientScript.GetPostBackEventReference(btnSearch, "OnClick");
        //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
        //m.IsActive = true;

        if (!Page.IsPostBack) {
            RadGrid1.MasterTableView.Columns.FindByUniqueName("CostColumn").Display = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKLOG_RATE);
            RadGrid1.MasterTableView.Columns.FindByUniqueName("RateColumn").Display = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKLOG_RATE);

            // Load some defaults. 
            Session.Add("REFRESH_WORK_LOG_MANAGER_DIALOG", "");
            // Work date is today.
            dtWorkDate.SelectedDate = DateTime.Now.Date;
            //dtWorkDate.MaxDate = DateTime.Now.Date;
        } else {
            if ((Session["REFRESH_WORK_LOG_MANAGER_DIALOG"] != null) && (Session["REFRESH_WORK_LOG_MANAGER_DIALOG"].ToString() != "")) {
                LoadWorkLogs();
            }
        }
    }
    protected void btnSearch_OnClick(object sender, EventArgs e) {
        LoadWorkLogs();
    }
    protected void LoadWorkLogs() {
        RadGrid1.DataSourceID = "ObjectDataSource1";
        if (dtWorkDate.SelectedDate == null) {
            dtWorkDate.SelectedDate = DateTime.Now.Date;
        }
        ObjectDataSource1.SelectParameters["workDate"].DefaultValue = dtWorkDate.SelectedDate.Value.Date.ToShortDateString();
        ObjectDataSource1.SelectParameters["unapprovedOnly"].DefaultValue = chkUnapprovedOnly.Checked.ToString();
        RadGrid1.DataBind();
        //RadGrid1.MasterTableView.CurrentPageIndex = 0;
        if (RadGrid1.Items.Count > 0) {
            RadGrid1.Items[0].Selected = true;
            //RadGrid1_SelectedIndexChanged(null, null);
        }
    }
    //protected void RadGrid1_SelectedIndexChanged(object sender, EventArgs e) {
    //    string custId = RadGrid1.SelectedValues["Customerid"].ToString();
    //    LinqDataSource1.WhereParameters["Customerid"].DefaultValue = custId;
    //    LinqDataSource1.DataBind();
    //}
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e) {
        if (e.Item is GridDataItem) {
            if (!e.Item.IsInEditMode) {
                // COLOR THE ROWS, HIDE THE DELETE BUTTON.
                GridDataItem row = e.Item as GridDataItem;
                TableCell apprCell = row["ApproveColumn"];

                // Handy vars.

                DateTime? apprDt = null;
                if (((DataRowView)(e.Item.DataItem)).Row["Approvaldate"].ToString() != "") {
                    apprDt = Convert.ToDateTime(((DataRowView)(e.Item.DataItem)).Row["Approvaldate"].ToString());
                }

                if (apprDt == null) {
                    // Modify label is visible
                    // So, do nothing
                } else {
                    // Clear the modify cell
                    apprCell.Controls.Clear();
                    apprCell.Text = "";
                }
            }
        }
    }
    protected void btnExportToExcel_OnClick(object sender, EventArgs e) {
        if (RadGrid1.MasterTableView.Items.Count > 0) {
            RadGrid1.MasterTableView.Columns.FindByUniqueName("TailColumn1").Display = false;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("TailColumn2").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("EqtypeColumn").Display = true;
            RadGrid1.Rebind();
            RadGrid1.MasterTableView.ExportToExcel();
        }
    }
}