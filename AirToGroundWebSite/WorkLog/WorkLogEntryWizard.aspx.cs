﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Text;
using Telerik.Web.UI;
using ATGDB;

public partial class WorkLog_WorkLogEntryWizard : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ENTER_MY_TIME_WIZ;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            //LoadJobCodes();
            lstEquipment.DataSource = Equipment.GetEquipmentNotOnHold(int.Parse(Profile.Customerid));
            lstEquipment.DataBind();
        }
    }
    protected void RadGrid2_ItemCreated(object sender, GridItemEventArgs e) {
        if (e.Item is GridDataItem) {
            e.Item.PreRender += new EventHandler(RadGrid2_ItemPreRender);
        }
    }
    private void RadGrid2_ItemPreRender(object sender, EventArgs e) {
        ((sender as GridDataItem)["CheckBoxTemplateColumn"].FindControl("chkSelected") as CheckBox).Checked = (sender as GridDataItem).Selected;
    }
    protected void ToggleRowSelection1(object sender, EventArgs e) {
        // Uncheck everything. Effectively making this act like a radio button.
        foreach (GridDataItem dataItem in RadGrid1.MasterTableView.Items) {
            (dataItem.FindControl("chkSelected") as CheckBox).Checked = false;
            dataItem.Selected = false;
        }
        // Now check the user's current selection.
        GridDataItem item = (sender as CheckBox).NamingContainer as GridDataItem;
        item.Selected = true;
        item.Selected = true;
        hiddenSSID.Value = RadGrid1.SelectedValues["Servicescheduleid"].ToString();
    }
    protected void ToggleRowSelection2(object sender, EventArgs e) {
        // Now check the user's current selection.
        ((sender as CheckBox).NamingContainer as GridItem).Selected = true;
    }
    protected void ToggleSelectedState1(object sender, EventArgs e) {
        CheckBox headerCheckBox = (sender as CheckBox);
        foreach (GridDataItem dataItem in RadGrid1.MasterTableView.Items) {
            (dataItem.FindControl("chkSelected") as CheckBox).Checked = headerCheckBox.Checked;
            dataItem.Selected = headerCheckBox.Checked;
        }
    }
    protected void ToggleSelectedState2(object sender, EventArgs e) {
        CheckBox headerCheckBox = (sender as CheckBox);
        foreach (GridDataItem dataItem in RadGrid2.MasterTableView.Items) {
            (dataItem.FindControl("chkSelected") as CheckBox).Checked = headerCheckBox.Checked;
            dataItem.Selected = headerCheckBox.Checked;
        }
    }
    protected void CustomValidator1_OnServerValidate(object sender, ServerValidateEventArgs e) {
        e.IsValid = RadGrid1.SelectedItems.Count > 0;
    }
    protected void CustomValidator2_OnServerValidate(object sender, ServerValidateEventArgs e) {
        e.IsValid = false;
        // See if something is checked.
        bool somethingChecked = false;
        for (int lcv = 0; lcv < lstSections.Items.Count(); lcv++) {
            somethingChecked = lstSections.Items[lcv].Checked;
            if (somethingChecked) {
                break;
            }
        }
        if (somethingChecked) {
            // Something is checked. Now check for notes.
            e.IsValid = !String.IsNullOrEmpty(txtExceptionNotes.Text);
        } else {
            // Always valid.
            e.IsValid = true;
        }
    }
    protected void CustomValidator3_OnServerValidate(object sender, ServerValidateEventArgs e) {
        e.IsValid = false;
        if (RadGrid2.SelectedItems.Count > 0) {
            // Must have comments.
            e.IsValid = !String.IsNullOrEmpty(txtCompletionNotes.Text);
        } else {
            // No items selected, so valid.
            e.IsValid = true;
        }
    }
    protected void CustomValidator4_OnServerValidate(object sender, ServerValidateEventArgs e) {
        e.IsValid = ((dtEndTime.SelectedDate != null) && (dtStartTime != null)) || txtQuantity.Value > 0;
    }
    protected void Wizard1_OnActiveStepChanged(object sender, EventArgs e) {
        if (Wizard1.ActiveStepIndex == 2) {
            // Get the service schedules for the selected date.
            var data = Serviceschedule.GetServiceSchedulesByDate(dtWorkedDate.SelectedDate.Value.Date,
                          Convert.ToInt32(lstEquipment.SelectedValue));
            RadGrid1.DataSource = data;
            RadGrid1.DataBind();

            // Clear some variables.
            dtStartTime.Clear();
            dtEndTime.Clear();
            txtQuantity.Text = "0";
            txtQuantity.Value = 0;
        }

        if (Wizard1.ActiveStepIndex == 3) {
            // Now, see if there are pending zone exceptions.
            // Get the exceptions.
            var data = Serviceexception.GetOutstandingExceptionsByServiceScheduleId(Convert.ToInt32(RadGrid1.SelectedValue));

            // Hide/Show label/text box.
            lblCompletionNotes.Visible = (data.Count() > 0);
            txtCompletionNotes.Visible = (data.Count() > 0);

            // Bind.
            RadGrid2.DataSource = data;
            RadGrid2.DataBind();

            // Check all items now. Default.
            if (data.Count() > 0) {
                foreach (GridDataItem item in RadGrid2.Items) {
                    item.Selected = true;
                }
            }
        }

        // Redirect to step 3, or 4, based on the service type.
        if (Wizard1.ActiveStepIndex == 4) {
            // Get the service.
            ATGDB.Service svc = Service.GetServiceByScheduleId(Convert.ToInt32(hiddenSSID.Value));

            //if (st.Flatfeeorhourly == "F") {
            //    // Flat-fee, so jump forward one.
            //    Wizard1.ActiveStepIndex = 5;
            //}

            // Load Division areas
            ATGDataContext db = new ATGDataContext();
            var data = from x in db.Divisionareas
                       orderby x.Description
                       select x;
            cmbArea.DataSource = data;
            cmbArea.DataBind();

            // Enable the validation
            reqArea.Enabled = svc.Requiresdivisionarea;
        }

        // Redirect to step 3, or 4, based on the service type.
        if (Wizard1.ActiveStepIndex == 5) {
            // Get the service type.
            //ATGDB.Service st = Service.GetServiceByScheduleId(Convert.ToInt32(hiddenSSID.Value));

            //if (st.Flatfeeorhourly == "H") {
            //    // Hourly, so jump forward one.
            //    Wizard1.ActiveStepIndex = 6;
            //}
            var data = Section.GetSectionsByServiceScheduleId(Convert.ToInt32(hiddenSSID.Value));
            // Bind.
            lstSections.DataSource = data;
            lstSections.DataBind();
        }

        //if (Wizard1.ActiveStepIndex == 6) {
        //    var data = Section.GetSectionsByServiceScheduleId(Convert.ToInt32(hiddenSSID.Value));
        //    // Bind.
        //    lstSections.DataSource = data;
        //    lstSections.DataBind();
        //}

        // Build a summary.
        if (Wizard1.ActiveStepIndex == 7) {

            // Build a summary.
            StringBuilder s = new StringBuilder();

            // SERVICE SCHEDULES
            int count = 0;
            foreach (GridDataItem item in RadGrid1.SelectedItems) {
                count++;
                ATGDB.Serviceschedule ss = Serviceschedule.GetServiceSchedule(Convert.ToInt32(item.GetDataKeyValue("Servicescheduleid")));

                // Break if there is more than one service schedule section.
                if (count > 1) {
                    s.Append("************************************************************");
                }

                s.Append("<div style=\"text-align:left\">");
                s.Append("<div class=\"lblTitle\">Aircraft</div>");
                s.Append("<div class=\"lblInfo\">" + ss.Equipment.Description + "</div>");
                s.Append("<br />");

                s.Append("<div class=\"lblTitle\">Service</div>");
                s.Append("<div class=\"lblInfo\">" + ss.Service.Description + "</div>");
                s.Append("<br />");

                //s.Append("<div class=\"lblTitle\">Location</div>");
                //s.Append("<div class=\"lblInfo\">" + ss.Companydivision.Location + "</div>");
                //s.Append("<br />");

                if (int.Parse(txtQuantity.Text) == 0) {
                    s.Append("<div class=\"lblTitle\">Date/Time, Hours</div>");
                    s.Append("<div class=\"lblInfo\">" + ss.Servicedate.ToShortDateString() + "&nbsp;&nbsp;-&nbsp;&nbsp;" +
                        dtStartTime.SelectedDate.Value.ToShortTimeString() + "&nbsp;&nbsp;TO&nbsp;&nbsp;" +
                        dtEndTime.SelectedDate.Value.ToShortTimeString() + "</div>");
                    s.Append("<br />");
                } else {
                    s.Append("<div class=\"lblTitle\">Date, Quantity</div>");
                    s.Append("<div class=\"lblInfo\">" + ss.Servicedate.ToShortDateString() + "&nbsp;&nbsp;-&nbsp;&nbsp;" +
                        txtQuantity.Text + "&nbsp;&nbsp;Units" + "</div>");
                    s.Append("<br />");
                }
            }

            // PENDING ZONE EXCEPTIONS
            s.Append("************************************************************");
            s.Append("<div class=\"lblTitle\">Pending Zone Exceptions Being Completed</div>");
            if (RadGrid2.SelectedItems.Count == 0) {
                s.Append("<div class=\"lblInfo\">NONE</div>");
                s.Append("<br />");
            } else {
                foreach (GridDataItem item in RadGrid2.SelectedItems) {
                    s.Append("<div class=\"lblInfo\">" + 
                        item.Cells[3].Text + " - " +
                        item.Cells[4].Text + " - " +
                        item.Cells[5].Text + "</div>");
                }
                s.Append("<br />");
            }

            // NEW ZONE EXCEPTIONS
            s.Append("************************************************************");
            s.Append("<div class=\"lblTitle\">New Zone Exceptions</div>");
            bool hadOne = false;
            for (int lcv = 0; lcv < lstSections.Items.Count(); lcv++) {
                if (lstSections.Items[lcv].Checked) {
                    hadOne = true;
                    s.Append("<div class=\"lblInfo\">" + lstSections.Items[lcv].Text + "</div>");
                }
            }
            if (!hadOne) {
                s.Append("<div class=\"lblInfo\">NONE</div>");
            } else {
                s.Append("<br/>Exception Notes:<br/><div class=\"lblInfo\">" + txtExceptionNotes.Text + " </div>");
            }
            s.Append("<br />");

            // GENERAL NOTES
            s.Append("************************************************************");
            s.Append("<div class=\"lblTitle\">Notes</div>");
            s.Append("<div class=\"lblInfo\">" + txtNotes.Text ?? "None" + "</div>");
            s.Append("<br />");
            s.Append("</div>");

            // Write it.
            lblSummary.Text = s.ToString();
        }
    }
    protected void Wizard1_OnFinishButtonClick(object sender, EventArgs e) {
        // Gather convenience vars for processing.
        string ssId = hiddenSSID.Value.ToString();
        DateTime workDate = dtWorkedDate.SelectedDate.Value.Date;
        DateTime startTime = DateTime.Parse(workDate.ToShortDateString() + " " +
            (dtStartTime.SelectedDate == null ? DateTime.MinValue.ToShortTimeString() : dtStartTime.SelectedDate.Value.ToShortTimeString()));
        DateTime endTime = DateTime.Parse(workDate.ToShortDateString() + " " +
            (dtEndTime.SelectedDate == null ? DateTime.MinValue.ToShortTimeString() : dtEndTime.SelectedDate.Value.ToShortTimeString()));
        int qty = 0;
        if (txtQuantity.Text != "") {
            qty = int.Parse(txtQuantity.Text);
        }

        // Build the completed exception items.
        string exCompletedSelections = "";
        foreach (Telerik.Web.UI.GridDataItem item in RadGrid2.SelectedItems) {
            if (exCompletedSelections == "") {
                exCompletedSelections = item.GetDataKeyValue("Sectionid").ToString();
            } else {
                exCompletedSelections = exCompletedSelections + ":" + item.GetDataKeyValue("Sectionid").ToString();
            }
        }

        // Build the new exception items.
        string exSelections = "";
        foreach (Telerik.Web.UI.RadListBoxItem item in lstSections.CheckedItems) {
            if (exSelections == "") {
                exSelections = item.Value.ToString();
            } else {
                exSelections = exSelections + ":" + item.Value.ToString();
            }
        }
        try {
            // Process the Service Schedule. This is a very important procedure!
            Servicesectionrule.ProcessServiceSchedule(ssId, Profile.UserName, workDate, startTime, endTime,
                                                      qty, cmbArea.SelectedValue, txtNotes.Text,
                                                      exSelections, txtExceptionNotes.Text,
                                                      exCompletedSelections, txtCompletionNotes.Text);
        } catch (Exception ex) {
            RadWindowManager1.RadAlert(ex.Message, null, null, "Error", "");
        }
    }
    //protected void lstJobCodes_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
    //    ATGDB.Jobcode jc = ATGDB.Jobcode.GetJobcodeById(e.Value);
    //    if (jc != null) {
    //        lblJobCodeName.Text = jc.Description;
    //    } else {
    //        lblJobCodeName.Text = "None Selected";
    //    }
    //}
    //protected void LoadJobCodes() {
    //    //ATGDB.Serviceschedule ss = Serviceschedule.GetServiceSchedule(int.Parse(Request.QueryString["ssId"].ToString()));
    //    string companyDivisionId = Profile.Companydivisionid;
    //    string customerId = Profile.Customerid;
    //    if (customerId != "") {
    //        DataSet data = ATGDB.Jobcode.GetCompanyCustomerJobJoinsDS(companyDivisionId, customerId);
    //        lstJobCodes.DataSource = data;
    //        lstJobCodes.DataBind();
    //    }
    //}
}