﻿using System;
using System.Linq;
using ATGDB;

public partial class WorkLog_WorkLog_Updater : System.Web.UI.Page
{
    [System.Web.Services.WebMethod]
    public static string ApproveWorkLog(string workLogId) {
        ATGDataContext dc = new ATGDataContext();
        Worklog wl = dc.Worklogs.SingleOrDefault(x => x.Worklogid == int.Parse(workLogId));
        if (wl != null) {
            wl.Approvaldate = DateTime.Now;
            wl.Approvaluserid = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            dc.SubmitChanges();

            return "Success";
        } else {
            return "Fail";
        }
    }
}