﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="WorkLogEntryWizard.aspx.cs" Inherits="WorkLog_WorkLogEntryWizard" Buffer="true"
    Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <%--<devart:DbLinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="" OrderBy="Tailnumber" Select="new (Description, Equipmentid, Tailnumber)"
        TableName="Equipments" Where="Customerid == @Customerid">
        <WhereParameters>
            <asp:ProfileParameter Name="Customerid" PropertyName="Customerid" Type="Int32" />
        </WhereParameters>
    </devart:DbLinqDataSource>--%>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="true" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" Width="250" Height="75">
    </telerik:RadWindowManager>
    <asp:HiddenField ID="hiddenSSID" runat="server" Value="" />
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Work Log Entry Wizard"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Work Log Entry Wizard</div>
            <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" DisplayCancelButton="true"
                BackColor="#F2F0F2" DisplaySideBar="false"
                OnActiveStepChanged="Wizard1_OnActiveStepChanged" StepStyle-HorizontalAlign="Center"
                Width="100%" OnFinishButtonClick="Wizard1_OnFinishButtonClick" CancelDestinationPageUrl="~/WorkLog/WorkLogEntryWizard.aspx">
                <WizardSteps>
                    <asp:WizardStep ID="WizardStep1" runat="server" Title="Step 1">
                        <br />
                        <div class="lblTitle" style="width: 30%">
                            Please select the Aircraft being Serviced</div>
                        <br />
                        <%--<asp:Label ID="lblJobCode" runat="server" SkinID="InfoLabelBold" Text="Job Code"></asp:Label>
                        <br />
                        <asp:Label ID="lblJobCodeName" runat="server" SkinID="SmallLabel" Text=""></asp:Label>
                        <br />
                        <telerik:RadComboBox ID="lstJobCodes" runat="server" DataValueField="Jobcodeid" DataTextField="Code" Width="300px"
                            HighlightTemplatedItems="true" AutoPostBack="true" OnSelectedIndexChanged="lstJobCodes_SelectedIndexChanged">
                            <HeaderTemplate>
                                <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                    width: 30%">
                                    Code</div>
                                <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                    width: 70%">
                                    Description</div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="lblInfo" style="position: relative; float: left; width: 30%">
                                    <%# Eval("Code") %>
                                </div>
                                <div class="lblInfo" style="position: relative; float: left; width: 70%">
                                    <%# Eval("Jobcodedescr")%>
                                </div>
                            </ItemTemplate>
                        </telerik:RadComboBox>
                        <asp:RequiredFieldValidator ID="reqJobCodes" runat="server" CssClass="stdValidator" ErrorMessage="Please select the Job Code"
                            ControlToValidate="lstJobCodes" Display="Dynamic"></asp:RequiredFieldValidator>
                        <br />--%>
                        <asp:Label ID="lblEquipment" runat="server" SkinID="InfoLabelBold" Text="Equipment"></asp:Label>
                        <br />
                        <telerik:RadComboBox ID="lstEquipment" runat="server" Width="200px" DataTextField="Tailnumber"
                            DataValueField="Equipmentid" MarkFirstMatch="true" HighlightTemplatedItems="true">
                            <%--<ItemTemplate>
                                    <ul>
                                        <li class="cbCol1">
                                            <%# DataBinder.Eval(Container.DataItem, "Tailnumber") %></li>
                                        <li class="cbCol2">
                                            <%# DataBinder.Eval(Container.DataItem, "Description") %></li>
                                    </ul>
                                </ItemTemplate>--%>
                        </telerik:RadComboBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" CssClass="stdValidator"
                            ErrorMessage="Please select the Aircraft" ControlToValidate="lstEquipment" Display="Dynamic"></asp:RequiredFieldValidator>
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep2" runat="server" Title="Step 2">
                        <br />
                        <div class="lblTitle" style="width: 30%">Please select the Date the Work was Performed</div>
                        <br />
                        <div class="lblInfo">
                            Work Date
                            <telerik:RadDatePicker id="dtWorkedDate" runat="server">
                            </telerik:RadDatePicker>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="stdValidator"
                                ErrorMessage="Please enter a Worked Date" ControlToValidate="dtWorkedDate" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                        <br />
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep3" runat="server" Title="Step 3">
                        <br />
                        <div class="lblTitle" style="width: 35%">Please Select the Service Schedule Being Worked On</div>
                        <br />
                        <div style="text-align: left">
                            <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="true" HorizontalAlign="NotSet"
                                AutoGenerateColumns="false" AllowMultiRowSelection="true" OnItemCreated="RadGrid2_ItemCreated">
                                <PagerStyle Mode="NextPrevAndNumeric" />
                                <MasterTableView Name="MainView" runat="server" AutoGenerateColumns="false" RowIndicatorColumn-Display="false"
                                    GridLines="Both" RowIndicatorColumn-Visible="false"
                                    ShowHeadersWhenNoRecords="false" DataKeyNames="Servicescheduleid" Font-Names="Arial"
                                    HorizontalAlign="Left" AllowAutomaticDeletes="false" AllowAutomaticUpdates="false" EditMode="InPlace">
                                    <Columns>
                                        <telerik:GridTemplateColumn UniqueName="CheckBoxTemplateColumn">
                                            <%--<HeaderTemplate>
                                                <asp:CheckBox ID="headerChkbox" OnCheckedChanged="ToggleSelectedState1" AutoPostBack="True"
                                                    runat="server"></asp:CheckBox>
                                            </HeaderTemplate>--%>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelected" OnCheckedChanged="ToggleRowSelection1" AutoPostBack="True"
                                                    runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="15px" />
                                            <ItemStyle Width="15px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="Servicescheduleid" UniqueName="ServicescheduleidColumn"
                                            HeaderText="Id" ReadOnly="true" Visible="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Equipmentdescr" UniqueName="EquipmentdescrColumn"
                                            HeaderText="Aircraft" ReadOnly="true">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TailNumber" UniqueName="TailNumberColumn" HeaderText="TailNumber"
                                            ReadOnly="true">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Servicedescr" UniqueName="ServicedescrColumn"
                                            HeaderText="Service Description" ReadOnly="true">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Location" UniqueName="LocationColumn" HeaderText="Location"
                                            ReadOnly="true">
                                        </telerik:GridBoundColumn>
                                        <%--<telerik:GridTemplateColumn AllowFiltering="false" HeaderText="AZX">
                                            <EditItemTemplate>
                                                <asp:CheckBox ID="chkAZX" runat="server" Enabled="false" Checked='<%# Eval("Hasactiveexception") %>' />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkAZX" runat="server" Checked='<%# Eval("Hasactiveexception") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>--%>
                                        <telerik:GridDateTimeColumn DataField="Servicedate" UniqueName="ServicedateColumn"
                                            DataFormatString="{0:d}" HeaderText="Service Date" ReadOnly="false">
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridDateTimeColumn DataField="Servicetime" UniqueName="ServicetimeColumn"
                                            DataFormatString="{0:t}" PickerType="TimePicker" HeaderText="Service Time" ReadOnly="false">
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridBoundColumn DataField="Comments" UniqueName="CommentsColumn" HeaderText="Comments"
                                            ReadOnly="false" MaxLength="255">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                    <NoRecordsTemplate>
                                        <div class="lblInfo">
                                            No Matching Service Schedules. Please continue.</div>
                                    </NoRecordsTemplate>
                                    <AlternatingItemStyle BackColor="#F2F0F2" />
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" CssClass="stdValidator"
                                ErrorMessage="Please Select the Service Schedules(s) You Worked On" OnServerValidate="CustomValidator1_OnServerValidate"
                                Display="Dynamic"></asp:CustomValidator>
                        </div>
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep4" runat="server" Title="Step 4">
                        <br />
                        <div class="lblTitle" style="width: 30%">Complete Any Pending Aircraft Service Exceptions?</div>
                        <br />
                        <div style="position:relative;width:100%;text-align:left">
                            <telerik:RadGrid ID="RadGrid2" runat="server" AllowPaging="false" HorizontalAlign="NotSet"
                                AutoGenerateColumns="false" AllowMultiRowSelection="false"
                                OnItemCreated="RadGrid2_ItemCreated">
                                <PagerStyle Mode="NextPrevAndNumeric" />
                                <MasterTableView Name="MainView2" runat="server" AutoGenerateColumns="false" RowIndicatorColumn-Display="false"
                                    GridLines="Both" CellPadding="0" CellSpacing="0" RowIndicatorColumn-Visible="false"
                                    ShowHeadersWhenNoRecords="false" DataKeyNames="Serviceexceptionid,Sectionid" Font-Names="Arial"
                                    HorizontalAlign="Left" AllowAutomaticDeletes="false" AllowAutomaticUpdates="false"
                                    EditMode="InPlace">
                                    <Columns>
                                        <telerik:GridTemplateColumn UniqueName="CheckBoxTemplateColumn">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="headerChkbox" OnCheckedChanged="ToggleSelectedState2" AutoPostBack="True"
                                                    runat="server"></asp:CheckBox>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelected" OnCheckedChanged="ToggleRowSelection2" AutoPostBack="True"
                                                    runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="15px" />
                                            <ItemStyle Width="15px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="Section.Description" UniqueName="SectiondescriptionColumn"
                                            HeaderText="Section Description" ReadOnly="true">
                                            <HeaderStyle Width="150px" />
                                            <ItemStyle Width="150px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridDateTimeColumn DataField="Exceptiondate" UniqueName="ExceptiondateColumn"
                                            DataFormatString="{0:d}" HeaderText="Exception Date" ReadOnly="false">
                                            <HeaderStyle Width="90px" />
                                            <ItemStyle Width="90px" />
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridBoundColumn DataField="Exceptionnotes" UniqueName="ExceptionNotesColumn" HeaderText="Notes"
                                            ReadOnly="true">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                    <NoRecordsTemplate>
                                        <div class="lblInfo">
                                            There are no pending Service Exceptions. Please continue.</div>
                                    </NoRecordsTemplate>
                                    <AlternatingItemStyle BackColor="#F2F0F2" />
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                    <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <br />
                        <br />
                        <div style="position:relative;width:100%">
                            <asp:Label id="lblCompletionNotes" runat="server" CssClass="lblNotes">Exception Completion Notes (required if you COMPLETE an Exception)</asp:Label>
                            <br />
                            <asp:TextBox ID="txtCompletionNotes" runat="server" Width="400px" MaxLength="255"
                                Rows="10" Height="100px" Wrap="true" TextMode="MultiLine"></asp:TextBox>
                            <asp:CustomValidator ID="CustomValidator3" runat="server" CssClass="stdValidator"
                                ErrorMessage="<br/>When Completing Section Exceptions, Notes are Required" OnServerValidate="CustomValidator3_OnServerValidate"
                                Display="Dynamic"></asp:CustomValidator>
                            <br />
                        </div>
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep5" runat="server" Title="Step 5">
                        <br />
                        <div class="lblTitle" style="width: 30%">Enter Your Start and End Time OR Quantity</div>
                        <br />
                        <div class="lblInfo">
                            Start Time
                            <telerik:RadTimePicker ID="dtStartTime" runat="server">
                            </telerik:RadTimePicker>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="stdValidator" ErrorMessage="Please enter a Start Time"
                                ControlToValidate="dtStartTime" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                        </div>
                        <br />
                        <div class="lblInfo">
                            End Time
                            <telerik:RadTimePicker ID="dtEndTime" runat="server">
                            </telerik:RadTimePicker>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" CssClass="stdValidator" ErrorMessage="Please enter an End Time"
                                ControlToValidate="dtEndTime" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                        </div>
                        <br />
                        <div class="lblInfo">
                            Quantity
                            <telerik:RadNumericTextBox ID="txtQuantity" runat="server" NumberFormat-DecimalDigits="2"
                                MinValue="0" MaxValue="50" Value="0" SelectionOnFocus="SelectAll">
                            </telerik:RadNumericTextBox>
                            <asp:CustomValidator ID="CustomValidator4" runat="server" CssClass="stdValidator"
                                ErrorMessage="<br/>Start Time and End Time, or a Quantity > 0 is Required" 
                                OnServerValidate="CustomValidator4_OnServerValidate"
                                Display="Dynamic"></asp:CustomValidator>
                        </div>
                        <br />
                        <div class="lblInfo">
                            Division Area
                            <telerik:RadComboBox ID="cmbArea" runat="server" AppendDataBoundItems="true" Width="150px" DataTextField="Description"
                                DataValueField="Divisionareaid" MarkFirstMatch="true">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox>
                            <asp:RequiredFieldValidator ID="reqArea" runat="server" Enabled="false" CssClass="stdValidator"
                                ErrorMessage="Please select the Division Area" ControlToValidate="cmbArea" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                    </asp:WizardStep>
                    <%--<asp:WizardStep ID="WizardStep6" runat="server" Title="Step 6">
                        <br />
                        <div class="lblTitle">Enter the Quantity</div>
                        <br />
                        <div class="lblInfo">
                            Quantity
                            <telerik:RadNumericTextBox ID="txtQuantity" runat="server" IncrementSettings-Step="1" MinValue="1" MaxValue="50" Value="1">
                            </telerik:RadNumericTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="stdValidator" ErrorMessage="Please enter a Quantity"
                                ControlToValidate="txtQuantity" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                    </asp:WizardStep>--%>
                    <asp:WizardStep ID="WizardStep7" runat="server" Title="Step 6">
                        <div style="position: relative; width: 40%; left: 0%; text-align: left">
                            <br />
                            <div class="lblTitle" style="text-align: center">Are There Any NEW Section Exceptions?</div>
                            <br />
                            <telerik:RadListBox ID="lstSections" runat="server" CheckBoxes="true"
                                Width="100%" DataKeyField="Sectionid" DataTextField="Description" DataValueField="Sectionid">
                            </telerik:RadListBox>
                            <br />
                            <br />
                            <div class="lblNotes">Exception Notes (required if you add an Exception)</div>
                            <asp:TextBox ID="txtExceptionNotes" runat="server" Width="400px" MaxLength="255" Rows="10"
                                Height="100px" Wrap="true" TextMode="MultiLine"></asp:TextBox>
                            <asp:CustomValidator ID="CustomValidator2" runat="server" CssClass="stdValidator"
                                ErrorMessage="<br/>When Adding Section Exceptions, Notes are Required" OnServerValidate="CustomValidator2_OnServerValidate"
                                Display="Dynamic"></asp:CustomValidator>
                            <br />
                        </div>
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep8" runat="server" Title="Step 7">
                        <br />
                        <div class="lblTitle" style="width: 30%">Are there any Notes you would like to add?</div>
                        <br />
                        <asp:TextBox ID="txtNotes" runat="server" Width="400px" MaxLength="255" Rows="10"
                            Height="150px" Wrap="true" TextMode="MultiLine"></asp:TextBox>
                        <br />
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep9" runat="server" Title="Step 8" StepType="Finish">
                        <br />
                        <div class="lblTitle" style="width: 20%">Summary</div>
                        <br />
                        <asp:Label ID="lblSummary" runat="server" Width="300px"></asp:Label>
                        <br />
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep10" runat="server" Title="Step 9" StepType="Complete">
                        <br />
                        <br />
                        <div class="lblTitle" style="width: 30%">Your Time Entry was Successfully Created!</div>
                        <br />
                        <br />
                        <asp:HyperLink ID="lnkWorkLogWizard" runat="server" NavigateUrl="~/WorkLog/WorkLogEntryWizard.aspx"
                            Text="Enter More Time"></asp:HyperLink>
                        <br />
                        <br />
                    </asp:WizardStep>
                </WizardSteps>
            </asp:Wizard>
        </div>
    </div>
</asp:Content>

