﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class WorkLog_ModifyExceptionsDialog : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_WORKLOG_MODIFY_EX;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);
        if (!Page.IsPostBack) {
            // Clear Refresh var.
            Session.Add("REFRESH_WORK_LOG_MANAGER_DIALOG", "");
            if (Request.QueryString["workLogId"] != null) {
                // Get worklog.
                ATGDB.Worklog wl = ATGDB.Worklog.GetWorkLog(Request.QueryString["workLogId"]);
                if (wl != null) {
                    if (wl.Serviceschedules.Count > 0) {
                        // Header summary
                        ATGDB.Serviceschedule ss = ATGDB.Serviceschedule.GetServiceSchedule(wl.Serviceschedules[0].Servicescheduleid);
                        lblCustomer.Text = ss.Service.Customer.Description;
                        lblTailNumber.Text = ss.Equipment.Tailnumber;
                        lblServiceDescr.Text = ss.Service.Description;
                        lblServiceDate.Text = ss.Servicedate.ToShortDateString();

                        // Other defaults.
                        txtWorkDate.SelectedDate = wl.Workeddate;
                        txtStartTime.SelectedDate = wl.Starttime;
                        txtEndTime.SelectedDate = wl.Endtime;
                        txtQuantity.Text = wl.Quantity.ToString();
                        txtNotes.Text = wl.Notes;
                        
                        // Job codes.
                        var data = ATGDB.Jobcode.GetUniqueJobsByCompanyCustomer(wl.Companydivisionid.ToString(), wl.Customerid.ToString());
                        lstJobCodes.DataSource = data;
                        lstJobCodes.DataBind();
                        lstJobCodes.SelectedValue = wl.Jobcodeid.ToString();

                        // Load up the Exception Sections
                        var data1 = ATGDB.Section.GetSectionsByServiceId(ss.Serviceid);
                        // Bind.
                        lstSections.DataSource = data1;
                        lstSections.DataBind();

                        // Load up any current exceptions.
                        string exNotes = "";
                        var data2 = ATGDB.Serviceexception.GetAllExceptionSections(wl.Serviceschedules[0].Servicescheduleid, out exNotes);
                        var data3 = ATGDB.Serviceexception.GetServiceexceptionsByWorkLogId(wl.Worklogid);

                        // Check the exceptions we have, if any.
                        if (data2.Count > 0) {
                            txtExNotes.Text = exNotes;
                            ATGDB.Section sect = null;
                            ATGDB.Serviceexception sex = null;
                            foreach (RadListBoxItem item in lstSections.Items) {
                                sect = data2.SingleOrDefault(x => x.Sectionid == int.Parse(item.Value));
                                item.Checked = sect != null;
                                if (sect != null) {
                                    sex = data3.SingleOrDefault(x => x.Sectionid == int.Parse(item.Value));
                                    item.Enabled = ((sex.Closeddate == null) && (sex.Completeddate == null));
                                    if (item.Enabled == false) {
                                        //item.BackColor = ColorUtils.GetSS_CompletedColor(Page.Theme);
                                        if (sex.Closeddate != null) {
                                            item.Text = "(Closed) " + item.Text;
                                        } else if (sex.Completeddate != null) {
                                            item.Text = "(Completed) " + item.Text;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        // ERROR: The Service Schedules were not Found. Shouldn't happen, unless someone just deleted out from another terminal.
                        // UI Stuff
                        btnSaveChanges.Visible = false;
                        btnClose.Text = "Close";
                        lblError2.Visible = true;

                        // Disable all user input fields
                        lstJobCodes.Enabled = false;
                        txtWorkDate.Enabled = false;
                        txtStartTime.Enabled = false;
                        txtEndTime.Enabled = false;
                        txtQuantity.Enabled = false;
                        txtNotes.Enabled = false;
                        txtExNotes.Enabled = false;
                        lstSections.Enabled = false;
                    }
                } else {
                    // ERROR: The Work Log was not Found. Shouldn't happen, unless someone just deleted out from another terminal.
                    // UI Stuff
                    btnSaveChanges.Visible = false;
                    btnClose.Text = "Close";
                    lblError1.Visible = true;

                    // Disable all user input fields
                    lstJobCodes.Enabled = false;
                    txtWorkDate.Enabled = false;
                    txtStartTime.Enabled = false;
                    txtEndTime.Enabled = false;
                    txtQuantity.Enabled = false;
                    txtNotes.Enabled = false;
                    txtExNotes.Enabled = false;
                    lstSections.Enabled = false;
                }
            }
        }
    }
    protected void reqExNotes_OnServerValidate(object sender, ServerValidateEventArgs e) {
        e.IsValid = ((lstSections.CheckedItems.Count() > 0) && (txtExNotes.Text != "")) ||
                    ((lstSections.CheckedItems.Count() == 0) && (txtExNotes.Text == ""));
    }
    protected void btnSaveChanges_OnClick(object sender, EventArgs e) {
        if (!reqExNotes.IsValid) {
            return;
        }
        
        // Save Changes.
        ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();

        // Get the values into handy members
        int jobcodeid = int.Parse(lstJobCodes.SelectedValue);
        DateTime? startDt = txtStartTime.SelectedDate;
        DateTime? endDt = txtEndTime.SelectedDate;
        float qty = float.Parse(txtQuantity.Value.ToString());
        string notes = txtNotes.Text;
        string exNotes = txtExNotes.Text;

        // Update Exceptions.
        // Load up any current exceptions.
        // Get worklog.
        bool changesMade = false;
        ATGDB.Worklog wl = ATGDB.Worklog.GetWorkLog(Request.QueryString["workLogId"]);
        if (wl != null) {
            // Get the rate
            ATGDB.Ratedata rate = ATGDB.Ratedata.GetRatedataByVals(jobcodeid, (int)wl.Companydivisionid, (int)wl.Customerid);

            // Update base fields.
            ATGDB.Worklog w = db.Worklogs.SingleOrDefault(x => x.Worklogid == wl.Worklogid);
            w.Jobcodeid = int.Parse(lstJobCodes.SelectedValue);
            // Rate stuff.
            w.Rateid = rate.Rateid;
            w.Cost = rate.Cost;
            w.Rate = rate.Rate;
            w.Flatfeeorhourly = rate.Flatfeeorhourly;
            w.Stdhours = rate.Stdhours;
            w.Stdquantity = rate.Stdquantity;
            // Rest of the fields.
            w.Starttime = startDt;
            w.Endtime = endDt;
            w.Quantity = qty;
            w.Notes = notes;
            // Clear the approval.
            w.Approvaldate = null;
            w.Approvaluserid = "";
            db.SubmitChanges();
            changesMade = true;

            var data2 = ATGDB.Serviceexception.GetServiceexceptionsByWorkLogId(wl.Worklogid);
            // Check the exceptions we have, if any.
            if (data2.Count() > 0) {
                ATGDB.Serviceexception sex = null;
                foreach (RadListBoxItem item in lstSections.Items) {
                    if (item.Checked) {
                        // Item is checked, do we already have a ServiceEx?
                        sex = data2.SingleOrDefault(x => x.Sectionid == int.Parse(item.Value));
                        if (sex == null) {
                            // Don't have one, so create one.
                            ATGDB.Serviceexception.CreateServiceException(wl, wl.Serviceschedules[0], int.Parse(item.Value), exNotes);
                            changesMade = true;
                        }
                    } else {
                        // Item is not checked, do we have a ServiceEx? If so, delete it.
                        sex = data2.SingleOrDefault(x => x.Sectionid == int.Parse(item.Value));
                        if (sex != null) {
                            // We have one, so delete it.
                            ATGDB.Serviceexception s = db.Serviceexceptions.SingleOrDefault(x => x.Serviceexceptionid == sex.Serviceexceptionid);
                            if (s != null) {
                                db.Serviceexceptions.DeleteOnSubmit(s);
                                db.SubmitChanges();
                                changesMade = true;
                            }
                        }
                    }
                }
            } else {
                // No previous exceptions. Let's potentially create new ones.
                foreach (RadListBoxItem item in lstSections.Items) {
                    if (item.Checked) {
                        // Don't have one, so create one.
                        ATGDB.Serviceexception.CreateServiceException(wl, wl.Serviceschedules[0], int.Parse(item.Value), exNotes);
                        changesMade = true;
                    }
                }
            }
        }

        // UI Stuff
        btnSaveChanges.Visible = false;
        btnClose.Text = "Close";
        if (changesMade == true) {
            lblStatus1.Visible = true;
        } else {
            lblStatus2.Visible = true;
        }
        
        // Disable all user input fields
        lstJobCodes.Enabled = false;
        txtWorkDate.Enabled = false;
        txtStartTime.Enabled = false;
        txtEndTime.Enabled = false;
        txtQuantity.Enabled = false;
        txtNotes.Enabled = false;
        txtExNotes.Enabled = false;
        lstSections.Enabled = false;

        // Refresh trigger.
        Session.Add("REFRESH_WORK_LOG_MANAGER_DIALOG", "REFRESH");
    }
}