﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModifyExceptionsDialog.aspx.cs"
    Inherits="WorkLog_ModifyExceptionsDialog" Buffer="true" Strict="true" Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Modify Exceptions</title>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function doCancel() {
                var oWnd = GetRadWindow();
                var oArg = new Object();
                oWnd.close(oArg);
            }
        </script>
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server" AsyncPostBackTimeout="600"
            SupportsPartialRendering="true" EnablePartialRendering="true"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin" ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator2" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <div id="mainDiv" runat="server" style="text-align: center; margin: 10px 10px 10px 10px">
            <div style="text-align: center; width: 100%">
                <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Work Log Entry Dialog"></asp:Label>
            </div>
            <div style="position: relative; float: left; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 98%">
                <table style="border-collapse: separate; width: 100%">
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblCustomerLbl" runat="server" SkinID="InfoLabel" Text="Customer:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCustomer" runat="server" SkinID="InfoLabelBold" Text="Customer"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblTailNumberLbl" runat="server" SkinID="InfoLabel" Text="Tail Number:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblTailNumber" runat="server" SkinID="InfoLabelBold" Text="Tail Number"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblServiceDescrLbl" runat="server" SkinID="InfoLabel" Text="Service:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblServiceDescr" runat="server" SkinID="InfoLabelBold" Text="Service"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblServiceDateLbl" runat="server" SkinID="InfoLabel" Text="Service Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblServiceDate" runat="server" SkinID="InfoLabelBold" Text="Service Date"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: relative; float: left; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 98%; background-color: #F2F0F2">
                <table style="border-collapse: separate; width: 100%">
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblJobCode" runat="server" SkinID="InfoLabel" Text="Job Code:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadComboBox ID="lstJobCodes" runat="server" DataValueField="Jobcodeid" DataTextField="Code" Width="78%"
                                HighlightTemplatedItems="true">
                                <HeaderTemplate>             
                                    <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                        width: 30%">
                                        Code</div>
                                    <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                        width: 70%">
                                        Description</div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="lblInfo" style="position: relative; float: left; width: 30%">
                                        <%# Eval("Code") %>
                                    </div>
                                    <div class="lblInfo" style="position: relative; float: left; width: 70%">
                                        <%# Eval("Description")%>
                                    </div>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                            <asp:RequiredFieldValidator ID="reqJobCode" runat="server" CssClass="stdValidator"
                                ControlToValidate="lstJobCodes" ErrorMessage="*"></asp:RequiredFieldValidator>
                            <br />
                            <asp:Label ID="lblJobCodeName" runat="server" SkinID="SmallLabel" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblWorkDate" runat="server" SkinID="InfoLabel" Text="Work Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadDatePicker ID="txtWorkDate" runat="server">
                            </telerik:RadDatePicker>
                        </td>
                        <asp:RequiredFieldValidator ID="reqWorkDate" runat="server" CssClass="stdValidator"
                            ControlToValidate="txtWorkDate" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </tr>
                    <tr id="st1" runat="server">
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblStartTime" runat="server" SkinID="InfoLabel" Text="Start Time:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadTimePicker ID="txtStartTime" runat="server">
                            </telerik:RadTimePicker>
                            <asp:RequiredFieldValidator ID="reqStartTime" runat="server" CssClass="stdValidator"
                                ControlToValidate="txtStartTime" ErrorMessage="*"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblEndTime" runat="server" SkinID="InfoLabel" Text="End Time:"></asp:Label>
                            <telerik:RadTimePicker ID="txtEndTime" runat="server">
                            </telerik:RadTimePicker>
                            <asp:RequiredFieldValidator ID="reqEndTime" runat="server" CssClass="stdValidator"
                                ControlToValidate="txtEndTime" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="q1" runat="server">
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblQuantity" runat="server" SkinID="InfoLabel" Text="Quantity:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadNumericTextBox ID="txtQuantity" runat="server" IncrementSettings-Step="1"
                                MinValue="0" NumberFormat-DecimalDigits="0">
                            </telerik:RadNumericTextBox>
                            <asp:RequiredFieldValidator ID="reqQuantity" runat="server" CssClass="stdValidator"
                                ControlToValidate="txtQuantity" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblNotes" runat="server" SkinID="InfoLabel" Text="Notes:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtNotes" runat="server" Width="98%" MaxLength="255" Rows="10" Height="40px"
                                Wrap="true" TextMode="MultiLine">
                            </asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <div style="text-align: left; width: 98%; background-color: #F2F0F2;
                    margin: 0px 10px 0px 10px">
                    <div class="stdLabel" style="text-align: left">
                        Work Log Exceptions
                        <asp:Label ID="lblSections" runat="server" SkinID="SubscriptLabel" Text="(You Cannot Change Closed/Completed Exceptions)"></asp:Label>
                    </div>
                    <telerik:RadListBox ID="lstSections" runat="server" CheckBoxes="true" Width="99%"
                        DataKeyField="Sectionid" DataTextField="Description" DataValueField="Sectionid" Height="100px">
                    </telerik:RadListBox>
                    <br />
                </div>
                <div style="text-align: left; width: 98%; background-color: #F2F0F2;
                    margin: 0px 10px 0px 10px">
                    <div class="stdLabel" style="text-align: left">
                        Exception Notes (required if you had an Exception)
                    </div>
                    <asp:TextBox ID="txtExNotes" runat="server" Width="98%" MaxLength="255" Rows="10"
                        Height="40px" Wrap="true" TextMode="MultiLine">
                    </asp:TextBox>
                    <asp:CustomValidator ID="reqExNotes" runat="server" CssClass="stdValidator"
                        OnServerValidate="reqExNotes_OnServerValidate" ErrorMessage="<br/>Selected Exceptions, and Exception Notes are Mutually Required"></asp:CustomValidator>
                </div>
                <br />
                <div style="text-align: center; width: 100%">
                    <asp:LinkButton ID="btnSaveChanges" runat="server" CssClass="lblInfo" Text="Save These Changes<br />"
                        OnClick="btnSaveChanges_OnClick">
                    </asp:LinkButton>
                    <asp:Label ID="lblStatus1" runat="server" ForeColor="Green" SkinID="InfoLabel" Visible="false"
                        Text="Changes Successfully Saved!<br />"></asp:Label>
                    <asp:Label ID="lblStatus2" runat="server" ForeColor="Green" SkinID="InfoLabel" Visible="false"
                        Text="No Changes.<br />"></asp:Label>
                    <asp:Label ID="lblError1" runat="server" ForeColor="Red" SkinID="InfoLabel" Visible="false"
                        Text="ERROR: Work Log was Not Found.<br />"></asp:Label>
                    <asp:Label ID="lblError2" runat="server" ForeColor="Red" SkinID="InfoLabel" Visible="false"
                        Text="ERROR: Work Log has No Service Schedules.<br />"></asp:Label>
                    <br />
                    <asp:LinkButton ID="btnClose" runat="server" Text="Cancel" OnClientClick="doCancel(); return false;">
                    </asp:LinkButton>
                </div>
                <br />
            </div>
        </div>
    </form>
</body>
</html>
