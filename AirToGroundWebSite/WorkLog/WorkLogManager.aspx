﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="WorkLogManager.aspx.cs" Inherits="WorkLog_WorkLogManager"
    Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetWorkLogDS"
        UpdateMethod="UpdateWorkLog" DeleteMethod="DeleteWorkLog" DataObjectTypeName="ATGDB.Worklog"
        TypeName="ATGDB.Worklog" EnableCaching="false">
        <SelectParameters>
            <asp:Parameter Name="workDate" Type="DateTime" />
            <asp:Parameter Name="unapprovedOnly" Type="Boolean" DefaultValue="false" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Companycustomerjobjoin" OrderBy="JobCode.Code"
        TableName="Companycustomerjobjoins" Select="new (Companydivisionid, Customerid, Jobcodeid, Jobcode.Code as Code)"
        Where='iif(@Customerid == "-1", true == true, Customerid == Convert.ToInt32(@Customerid))'>
        <WhereParameters>
            <asp:Parameter Name="Customerid" Type="String" DefaultValue="-1" />
        </WhereParameters>
    </devart:DbLinqDataSource>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="lnkApprove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="false" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" Width="250" Height="25" EnableViewState="false">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" NavigateUrl="ModifyExceptionsDialog.aspx"
                Behaviors="Close,Resize" OnClientClose="onClientClose1" ShowContentDuringLoad="false"
                VisibleStatusbar="false" VisibleTitlebar="false" EnableShadow="false" ReloadOnShow="true"
                Title="Log Work Done" Width="700" Height="620" Modal="true" AutoSize="false">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>     
    <telerik:RadCodeBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            // This fixes problems when exporting.
            function onRequestStart(sender, args) {
                if ((args.get_eventTarget().indexOf("btnExportToExcel") >= 0) || 
                    (args.get_eventTarget().indexOf("xxx") >= 0)) {
                    // Disable ajax
                    args.set_enableAjax(false);
                }
            }
            function onClientClose1(oWnd, args) {
                // Force a page postback. Causes refresh.
                <%= PostBackString %>
            }
            function openWin1(workLogId) {
                radopen("ModifyExceptionsDialog.aspx?workLogId=" + workLogId, "RadWindow1");
            }
            function DoApprove(workLogId) {
                //alert("DoApprove: " + workLogId);

                var loc = "WorkLog_Updater.aspx";
                var args = '"workLogId":"' + workLogId + '"';
                $.ajax({
                    type: "POST",
                    url: loc + "/ApproveWorkLog",
                    data: "{" + args + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        // Replace the div's content with the page method's return.
                        //alert(msg.d);
                        // Force a search click. Causes grid refresh.
                        //< %= SearchPostBackString %>
                        $find("<%= RadGrid1.ClientID %>").get_masterTableView().rebind();
                    },
                    fail: function (msg) {
                        // Replace the div's content with the page method's return.
                        alert("The Work Log Could not be Verified");
                    }
                });
            }
        </script>
    </telerik:RadCodeBlock>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Work Log Manager"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Work Log Manager</div>
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="true" Skin="Office2007">
            </telerik:RadAjaxLoadingPanel>
            <div style="margin: 10px 10px 10px 10px">
                <br />
                <!-- SEARCH -->
                <table width="97%">
                    <tr>
                        <td style="width: 70%">
                            <!-- SEARCH -->
                            <fieldset style="width: 400px">
                                <legend>Search Criteria:</legend>
                                <div class="lblInfo" style="margin: 10px 10px 10px 10px">
                                    <table width="99%">
                                        <tr>
                                            <td style="width: 33%">
                                                Work Date:
                                            </td>
                                            <td rowspan="2" style="width: 33%">
                                                &nbsp;-&nbsp;OR&nbsp;-&nbsp;
                                                <asp:CheckBox ID="chkUnapprovedOnly" runat="server" Checked="false" Text="Un-verified Only" />
                                            </td>
                                            <td align="center" valign="middle" rowspan="2">
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_OnClick" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <telerik:RadDatePicker ID="dtWorkDate" runat="server" Width="120px">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </fieldset>
                        </td>                
                    </tr>
                </table>
                <br />
                <div class="lblInfoBold">Selected Work Logs:</div>
                <!-- Work Log Results -->
                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
                    <telerik:RadGrid ID="RadGrid1" GridLines="None" Width="100%" runat="server"
                        AllowAutomaticDeletes="true" AllowAutomaticInserts="false" AllowAutomaticUpdates="false"
                        AllowMultiRowEdit="false" AllowMultiRowSelection="false" ShowGroupPanel="false"
                        AllowPaging="false" PageSize="10" AllowSorting="false" AutoGenerateColumns="false"
                        OnItemDataBound="RadGrid1_ItemDataBound">
                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="Worklogid,Customerid"
                            AllowAutomaticUpdates="false" AllowAutomaticInserts="false" AllowAutomaticDeletes="true"
                            NoMasterRecordsText="No Work Logs Found" AllowSorting="true" AllowMultiColumnSorting="false"
                            CommandItemSettings-ShowRefreshButton="false" CommandItemSettings-ShowAddNewRecordButton="false"
                            TableLayout="Fixed" Font-Size="12px" HorizontalAlign="NotSet" RowIndicatorColumn-Display="false"
                            AutoGenerateColumns="false" EditMode="InPlace">
                            <PagerStyle AlwaysVisible="true" Mode="NumericPages" Position="Bottom" />
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="" UniqueName="ModifyExceptionsDialogColumn" AllowFiltering="false">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <a href="#" onclick='<%# "openWin1(" + Eval("Worklogid").ToString() + "); return false;" %>'>
                                            <asp:ImageButton ID="btnEditRed" runat="server" SkinID="Edit" />
                                        </a>
                                    </ItemTemplate>
                                    <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                    <ItemStyle Width="50px" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="" UniqueName="ApproveColumn" AllowFiltering="false">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkApprove" runat="server" OnClientClick='<%# "DoApprove(" + Eval("Worklogid").ToString() + "); return false;" %>'>
                                            Verify
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                    <ItemStyle Width="50px" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="Worklogid" HeaderText="Id" AllowSorting="false"
                                    UniqueName="WorklogidColumn" ReadOnly="true" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Workeddate" HeaderText="Work Date" AllowSorting="true"
                                    UniqueName="WorkeddateColumn" ReadOnly="true" Visible="true" DataFormatString="{0:d}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Tailnumber" HeaderText="Tail" AllowSorting="true"
                                    UniqueName="TailnumberColumn" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Servicedescr" HeaderText="Service Descr" AllowSorting="true"
                                    UniqueName="ServicedescrColumn" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="Hasex" HeaderText="Has Ex?" AllowSorting="true"
                                    UniqueName="HasexColumn" ReadOnly="true" Visible="true">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridDateTimeColumn DataField="Starttime" HeaderText="Start Time" AllowSorting="true"
                                    UniqueName="StarttimeColumn" ReadOnly="false" Visible="true" DataFormatString="{0:t}"
                                    PickerType="TimePicker" DefaultInsertValue="08:00:00 AM" ColumnEditorID="StarttimeEditor">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="Endtime" HeaderText="End Time" AllowSorting="true"
                                    UniqueName="EndtimeColumn" ReadOnly="false" Visible="true" DataFormatString="{0:t}"
                                    PickerType="TimePicker" DefaultInsertValue="05:00:00 PM" ColumnEditorID="EndtimeEditor">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridNumericColumn DataField="Quantity" HeaderText="Qty" UniqueName="QuantityColumn"
                                    ReadOnly="false" Visible="true" ColumnEditorID="QuantityEditor" DataFormatString="{0:f}">
                                </telerik:GridNumericColumn>
                                <telerik:GridDropDownColumn HeaderText="Job Code" DataField="Jobcodeid" DataSourceID="LinqDataSource1"
                                    UniqueName="JobcodeidColumn" ColumnEditorID="JobcodeDropDownListEditor" ListTextField="Code"
                                    ListValueField="Jobcodeid" DropDownControlType="RadComboBox" EnableEmptyListItem="true"
                                    AllowSorting="true" SortExpression="Jobcodecode" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                    AutoPostBackOnFilter="true">
                                </telerik:GridDropDownColumn>
                                <telerik:GridBoundColumn DataField="Flatfeeorhourly" HeaderText="FF/Hr" AllowSorting="true"
                                    UniqueName="FlatfeeorhourlyColumn" ReadOnly="true" Visible="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridNumericColumn DataField="Cost" HeaderText="Cost" UniqueName="CostColumn"
                                    ReadOnly="true" Visible="true" DataFormatString="{0:c}">
                                </telerik:GridNumericColumn>
                                <telerik:GridNumericColumn DataField="Rate" HeaderText="Rate" UniqueName="RateColumn"
                                    ReadOnly="true" Visible="true" DataFormatString="{0:c}">
                                </telerik:GridNumericColumn>
                                <telerik:GridBoundColumn DataField="Notes" HeaderText="Notes"
                                    AllowSorting="false" UniqueName="NotesColumn" ReadOnly="false" Visible="true" ColumnEditorID="NotesEditor">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Approvaldate" HeaderText="Verify Date" AllowSorting="true"
                                    UniqueName="ApprovaldateColumn" ReadOnly="true" Visible="true" DataFormatString="{0:d}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Approvaluserid" HeaderText="Verify UserId" AllowSorting="true"
                                    UniqueName="ApprovaluseridColumn" ReadOnly="true" Visible="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Upddt" HeaderText="Upd Dt" AllowSorting="true"
                                    UniqueName="UpddtColumn" ReadOnly="true" Visible="true" DataFormatString="{0:d}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Upduserid" HeaderText="Upd UserId" AllowSorting="true"
                                    UniqueName="UpduseridColumn" ReadOnly="true" Visible="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record? Note: This will delete all related records such as Scheduled Services and Service Queues."
                                    ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                                    UniqueName="DeleteCommandColumn" CommandName="Delete">
                                    <ItemStyle Width="40px" />
                                    <HeaderStyle Width="40px" />
                                </telerik:GridButtonColumn>
                                <%--<telerik:GridTemplateColumn HeaderText="" UniqueName="DeleteColumn" AllowFiltering="false">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDeleteRed" runat="server" SkinID="Delete" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                    <ItemStyle Width="50px" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>--%>
                            </Columns>
                        </MasterTableView>
                        <ExportSettings FileName="WorkLogManager" OpenInNewWindow="true" ExportOnlyData="true"
                            IgnorePaging="true" HideStructureColumns="true">
                        </ExportSettings>
                        <ClientSettings>
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <telerik:GridDateTimeColumnEditor ID="StarttimeEditor" runat="server" TextBoxStyle-Width="98%">
                    </telerik:GridDateTimeColumnEditor>
                    <telerik:GridDateTimeColumnEditor ID="EndtimeEditor" runat="server" TextBoxStyle-Width="98%">
                    </telerik:GridDateTimeColumnEditor>
                    <telerik:GridNumericColumnEditor ID="QuantityEditor" runat="server" NumericTextBox-Width="98%"
                        NumericTextBox-MinValue="0" NumericTextBox-NumberFormat-DecimalDigits="2" NumericTextBox-SelectionOnFocus="SelectAll">
                    </telerik:GridNumericColumnEditor>
                    <telerik:GridTextBoxColumnEditor ID="NotesEditor" runat="server" TextBoxStyle-Width="98%" TextBoxMaxLength="255">
                    </telerik:GridTextBoxColumnEditor>
                </telerik:RadAjaxPanel>
                <br />
            </div>
        </div>
    </div>
</asp:Content>

