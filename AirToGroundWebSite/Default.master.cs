﻿using System;
using Telerik.Web.UI;
using ATGDB;

public partial class _Default_Master : System.Web.UI.MasterPage
{
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_LoadComplete(object sender, EventArgs e) {
        // Update RadGrid.ImagePaths
        // Omit the ServiceQueue2 Page.
        if (!(Page.AppRelativeVirtualPath.Contains("ServiceQueue2"))) {
            PageUtils.SetRadGridImagePathAllControls(this, Page.Theme);
        }
        // Audit Trail
        if (Page is IAuditTrailPage) {
            AuditUtils.AuditTrail.Log(Profile.Userid, ((IAuditTrailPage)Page).GetAuditPageName());
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // Customer changer
        lstCustDivision.DataSource = Companydivision.GetDivisionsAndCustomersByUser(Profile.Userid);
        lstCustDivision.DataBind();
        // Line up on current value.
        lstCustDivision.SelectedValue = Profile.Companydivisionid + ":" + Profile.Customerid;

        // Hook LoadComplete event.
        Page.LoadComplete += Page_LoadComplete;

        // Dynamically register the PULSE SCRIPT
        // All pages with this master will keep the session alive.
        PageUtils.RegisterSessionTimeoutPulse(Page);

        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManagerMasterPage);

        if (!Page.IsPostBack) {
            // Reinitialize profile if necessary.
            if (Profile.IsAnonymous == true) {
                throw new Exception("Default.master.cs Profile is Anonymous");
            }

            // Menu Setup
            if (SecurityUtils.GetATGMode() == SecurityUtils.ATG_MODE_CORP) {
                // CORP MODE

				// IMPORT MANAGER
				if ((ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN) ||
					ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_IMPORT_MANAGER))) {
					RadMenu menu = (RadMenu)Page.Master.FindControl("mnuMain");
					if (menu != null) {
						RadMenuItem menuitem = new RadMenuItem("Importing");
						menu.Items.Add(menuitem);
						RadMenuItem subitem;
						subitem = new RadMenuItem("Import Manager", "~/Import/ImportManager.aspx");
						subitem.ImageUrl = "~/App_Themes/Corp/Images/report.bmp";
						menuitem.Items.Add(subitem);
					}
				}
				
				// Careers.
                if ((ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN) ||
                    ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RECEIVE_JOBAPP))) {
                    RadMenu menu = (RadMenu)Page.Master.FindControl("mnuMain");
                    if (menu != null) {
                        RadMenuItem menuitem = new RadMenuItem("Careers");
                        menu.Items.Add(menuitem);
                        RadMenuItem subitem;
                        subitem = new RadMenuItem("Job Application Manager", "~/Careers/JobAppManager.aspx");
                        subitem.ImageUrl = "~/App_Themes/Corp/Images/CustServiceSections.bmp";
                        menuitem.Items.Add(subitem);
                    }
                }

                if (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN) ||
                    ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_CODETABLES) ||
                    ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_USERSECURITY)) {
                    RadMenu menu = (RadMenu)Page.Master.FindControl("mnuMain");
                    if (menu != null) {
                        RadMenuItem menuitem = new RadMenuItem("Administration", "~/Admin/AdminHome.aspx");
                        menu.Items.Add(menuitem);
                        // Subitems          
                        if (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_CODETABLES) ||
                            ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN)) {
                            RadMenuItem subitem;
                            subitem = new RadMenuItem("Company Divisions", "~/Admin/EditCompanyDivisions.aspx");
                            subitem.ImageUrl = "~/App_Themes/Corp/Images/JobRate_CompCust.bmp";
                            menuitem.Items.Add(subitem);

                            subitem = new RadMenuItem("Customers, Services, Sections", "~/Admin/EditCustomers.aspx");
                            subitem.ImageUrl = "~/App_Themes/Corp/Images/CustServiceSections.bmp";
                            menuitem.Items.Add(subitem);
                            
                            subitem = new RadMenuItem("Division Areas", "~/Admin/EditDivisionAreas.aspx");
                            subitem.ImageUrl = "~/App_Themes/Corp/Images/Areas.bmp";
                            menuitem.Items.Add(subitem);
                            
                            subitem = new RadMenuItem("Aircraft", "~/Admin/EditEquipment.aspx");
                            subitem.ImageUrl = "~/App_Themes/Corp/Images/Airplane1.png";
                            menuitem.Items.Add(subitem);
                            
                            subitem = new RadMenuItem("Aircraft Types", "~/Admin/EditEquipmentTypes.aspx");
                            subitem.ImageUrl = "~/App_Themes/Corp/Images/Airplane2.png";
                            menuitem.Items.Add(subitem);
                            
                            subitem = new RadMenuItem("Services and Service Rules", "~/Admin/EditServices.aspx");
                            subitem.ImageUrl = "~/App_Themes/Corp/Images/ServiceRules.bmp";
                            menuitem.Items.Add(subitem);
                            
                            subitem = new RadMenuItem("Service Short Descriptions", "~/Admin/EditServiceShortDescr.aspx");
                            subitem.ImageUrl = "~/App_Themes/Corp/Images/ServiceRules.bmp";
                            menuitem.Items.Add(subitem);
                            
                            subitem = new RadMenuItem("Service Categories", "~/Admin/EditServiceCategories.aspx");
                            subitem.ImageUrl = "~/App_Themes/Corp/Images/ServiceRules.bmp";
                            menuitem.Items.Add(subitem);
                            
                            subitem = new RadMenuItem("Service Sections", "~/Admin/EditSections.aspx");
                            subitem.ImageUrl = "~/App_Themes/Corp/Images/Section.png";
                            menuitem.Items.Add(subitem);
                            
                            subitem = new RadMenuItem("Job Codes", "~/Admin/EditJobs.aspx");
                            subitem.ImageUrl = "~/App_Themes/Corp/Images/ServiceRules.bmp";
                            menuitem.Items.Add(subitem);
                            if (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RATESCHEDULE) ||
                                ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN)) {
                                subitem = new RadMenuItem("Rate Schedule", "~/Admin/Rate/EditRateSchedule.aspx");
                                subitem.ImageUrl = "~/App_Themes/Corp/Images/ServiceRules.bmp";
                                menuitem.Items.Add(subitem);
                            }
                            if ((ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_USERSECURITY)) ||
                                ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN)) {
                                subitem = new RadMenuItem("Users and User Roles", "~/Admin/ManageUserRoles.aspx");
                                subitem.ImageUrl = "~/App_Themes/Corp/Images/Users.bmp";
                                menuitem.Items.Add(subitem);
                            }
                        }
                    }
                }

                // Reports.
                RadMenu menu2 = (RadMenu)Page.Master.FindControl("mnuMain");
                RadMenuItem rpt = menu2.FindItemByValue("_ReportRevenue");
                rpt = menu2.FindItemByValue("_ReportRevenue");
                if (rpt != null) {
                    rpt.Visible = (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_REVENUE) ||
                        ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN));
                }

                // Work Reports
                rpt = menu2.FindItemByValue("_ReportWork");
                if (rpt != null) {
                    rpt.Visible = (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_WORK) ||
                        ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN));
                }

                // Equipment Reports
                rpt = menu2.FindItemByValue("_ReportEquip");
                if (rpt != null) {
                    rpt.Visible = (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_EQUIP) ||
                        ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN));
                }

                // WorkLog
                rpt = menu2.FindItemByValue("_MenuWorkLog");
                if (rpt != null) {
                    rpt.Visible = (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKLOG) ||
                        ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN));
                }

                // TimeSheet
                rpt = menu2.FindItemByValue("_MenuTimeSheet");
                if (rpt != null) {
                    rpt.Visible = (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_TIMESHEETS) ||
                        ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN));
                }

                // Job Reports
                rpt = menu2.FindItemByValue("_Job");
                if (rpt != null) {
                    rpt.Visible = (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_JOB) ||
                        ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN));
                }

                // Audit Trails
                rpt = menu2.FindItemByValue("_ReportAuditing");
                if (rpt != null) {
                    rpt.Visible = ((SecurityUtils.GetATGMode() != SecurityUtils.ATG_MODE_CUSTOMER) &&
                        ((Profile.UserName.ToUpper() == "ADMINISTRATOR") || (Profile.UserName.ToUpper() == "CNILEST")));
                }

                // Service Schedule
                rpt = menu2.FindItemByValue("_VerifyServiceSchedules");
                if (rpt != null) {
                    rpt.Visible = ((SecurityUtils.GetATGMode() != SecurityUtils.ATG_MODE_CUSTOMER) &&
                        ((
                          Profile.UserName.ToUpper() == "ADMINISTRATOR") || 
                          (Profile.UserName.ToUpper() == "CNILEST") || 
                          ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_VERIFY_SS)
                        ));
                }
            }

            // Footer.
            txtFooter.Text = Resources.Resource.WebSiteFooterText;

        }
    }    
}
