﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default"
    Title="Air to Ground Website - Login" Buffer="true" Strict="true" Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Air to Ground Website - Login</title>
    <style type="text/css">
        body
        {
            background-color: white !important;
        }
    </style>
</head>
<body style="overflow: auto">
    <form id="form1" runat="server" method="post" action="Default.aspx">
        <div style="text-align: center; align-content: center; ">
            <div style="background-image: url('Images/SiteSplash.png'); background-origin: content-box; background-repeat: no-repeat; background-size: cover; 
                position: relative; width: 957px; height: 56px;">
            </div>
            <div style="background-image: url('Images/pagepic_cleaning.jpg'); background-origin: content-box; background-repeat: no-repeat; background-size: cover; 
                position: relative; width: 957px; height: 156px; filter: alpha(opacity=50); opacity: 0.5">
            </div>
            <div style="background-image: url('Images/Decoration2.jpg'); background-origin: content-box; background-repeat: no-repeat; 
                position: relative; width: 254px; height: 273px; float: left; filter: alpha(opacity=50); opacity: 0.5">
            </div>
            <div style="position: relative; float: left; height: 1.4em; width: 454px">
                <span style="font-size: 1.4em; font-style: italic">&quot;We Clean with Integrity and Dedication&quot;</span>
                <div style="text-align: center; background-color: transparent; position: relative; left: 72px; width: 304px">
                    <asp:Login ID="Login1" runat="server" MembershipProvider="ATG_MembershipProvider"
                        DestinationPageUrl="Home.aspx" OnLoggedIn="Login1_OnLoggedIn"  OnLoggingIn="Login1_LoggingIn" DisplayRememberMe="true" RememberMeSet="true">
                        <LayoutTemplate>
                            <table style="padding: 0px; line-height: 25px; margin: 0px 2px 5px 2px; width: 304px">
                                <tr>
                                    <td style="text-align: center;" colspan="2" class="lblInfo">
                                        Log In
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; width: 40%">
                                        <asp:Label ID="UserNameLabel" runat="server" SkinID="InfoLabel" AssociatedControlID="UserName">User Name:</asp:Label>
                                    </td>
                                    <td style="text-align: center; width: 60%">
                                        <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" CssClass="stdValidator"
                                            ControlToValidate="UserName" ErrorMessage="<br />Required." ToolTip="User Name is required."
                                            ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; width: 40%">
                                        <asp:Label ID="PasswordLabel" runat="server" SkinID="InfoLabel" AssociatedControlID="Password">Password:</asp:Label>
                                    </td>
                                    <td style="text-align: center; width: 60%">
                                        <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" CssClass="stdValidator"
                                            ControlToValidate="Password" ErrorMessage="<br />Required." ToolTip="Password is required."
                                            ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" colspan="2">
                                        <asp:CheckBox ID="RememberMe" runat="server" CssClass="lblInfo" Text="Remember me next time." />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center; color: Red; font-size: 10px">
                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" colspan="2">
                                        <asp:HyperLink ID="lnkPasswordRecovery" runat="server" Font-Size="10px" Text="Forgot Password?"
                                            NavigateUrl="~/Security/PasswordRecovery.aspx"></asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" colspan="2">
                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" ValidationGroup="Login1" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                    </asp:Login>
                </div>
                <div style="width: 100%; margin-left: 10px;">
                    <!-- FOOTER AREA -->
                    <asp:Label ID="txtFooter" runat="server" ForeColor="Gray" Font-Names="Arial, Helvetica"
                        Font-Size="8px" Width="85%"></asp:Label><br />
                    <asp:Label ID="txtRev" runat="server" ForeColor="Green" Font-Names="Arial, Helvetica"
                        Font-Size="8px" Font-Bold="true" Width="80px">Rev: 4/6/2014a</asp:Label>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
