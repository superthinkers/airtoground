﻿using System;
using System.Linq;
using Telerik.Web.UI;

public partial class Security_NoDefaults : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = "Corp";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        RadMenu menu = (RadMenu)PageUtils.FindControlRecursive(Page.Master, "mnuMain");
        if (menu != null) {
            menu.Visible = false;
        }
    }
}