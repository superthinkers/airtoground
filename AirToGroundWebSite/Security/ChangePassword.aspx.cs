﻿using System;

public partial class Security_ChangePassword : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_HELP_CHANGE_PASSWORD;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            // Set the login's membership provider here.
            string providerRoot = System.Web.HttpContext.Current.Profile.PropertyValues["ProviderRoot"].PropertyValue.ToString();
            ChangePassword1.MembershipProvider = providerRoot + "_MembershipProvider";
        }
    }
    protected void ChangePassword1_OnChangedPassword(object sender, EventArgs e) {
        EmailUtils.SendSecurityChangedMail(Page.Server, Profile.UserName);
    }
}