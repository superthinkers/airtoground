﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SessionTimeout.aspx.cs" Inherits="Security_SessionTimeout"
    Title="Air to Ground Website - Login" Buffer="true" Strict="true" Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Air to Ground Website - Session Timeout</title>
    <style type="text/css">
        body
        {
            background-color: white !important;
        }
        .siteSplash2 
        {
	        height: 76px; 
	        top: 30px; 
	        background-image: url('../Images/SiteSplash.png');
            background-repeat: no-repeat;
        }
        .siteHeaderStub2 
        {
	        height: 76px; 
	        top: 30px; 
	        background-image: url('../Images/HeaderStub.png');
            background-repeat: repeat-x;
        }    
        .borderTable2
        {
            position: absolute; 
            top: 26%; 
            border-collapse: collapse; 
            width: 100%; 
            height: 90%; 
        }
    </style>
</head>
<body style="overflow: auto">
    <form id="form1" runat="server">
        <script type="text/javascript">
            //<![CDATA[
            $(function () {
                //alert("load");
            });
            //]]>
        </script>
        <div class="siteHeaderStub2">
            <div class="siteSplash2">
            </div>
        </div>
        <div style="left: 0px; width: 100%; height: 25%; filter: alpha(opacity=30); opacity: 0.3">
            <img id="img1" alt="" src="../Images/pagepic_cleaning.jpg" width="100%" height="100%" />
        </div>
        <div style="position: relative; left: 0px; float: left; filter: alpha(opacity=30); opacity: 0.3">
            <img id="img2" alt="" src="../Images/Decoration2.jpg" />
        </div>
        <div style="position: absolute; left: 35%; top: 25%; width: 40%">
            <span style="font-size: 1.4em; font-style: italic">&quot;We Clean with Integrity and Dedication&quot;</span>
        </div>
        <div style="position: absolute; top: 50%; width: 100%">
            <div style="text-align: center; position: relative; left: 25%; top: 0px; width: 45%;
                height: 180px">
                <span style="font-size: x-large; color: green">Your Session has timed out. You must Log In Again.</span>
                <br />
                <br />
                If you have Questions, Please contact your Administrator.
                <br />
                <br />
                <asp:LinkButton ID="lnkLogin" Font-Names="Arial" ForeColor="Blue" runat="server"
                    Text="Click Here to Log In" OnClick="lnkLogin_OnClick" />
                <br />
                <br />
            </div>
        </div>
        <div style="text-align: center; width: 100%">
            <!-- FOOTER AREA -->
            <asp:Label ID="txtFooter" runat="server" ForeColor="Gray" Font-Names="Arial, Helvetica"
                Font-Size="8px" Width="85%"></asp:Label>
        </div>
    </form>
</body>
</html>
