﻿// OVERRIDE group rendering and handling
"use strict";
$.extend($.jgrid, {
	// OVERRIDE the BASE template extension for the summary band so we can add the formatting of the group columns. Currently
	// the jqGrid does not apply the default formatters to these columns. I have overridden this extension and have
	// corrected the behavior. NOTE: the author has been informed. Hopefully this will GO AWAY in a future release!!!!!
	template: function (format) { //jqgformat
		var args = $.makeArray(arguments).slice(1), j, al = args.length;
		if (format === null) { format = ""; }
		var $grid = $("#" + $(this).gridName);
		return format.replace(/\{([\w\-]+)(?:\:([\w\.]*)(?:\((.*?)?\))?)?\}/g, function (m, i) {
			if (!isNaN(parseInt(i, 10))) {
				return args[parseInt(i, 10)];
			}
			for (j = 0; j < al; j++) {
				if ($.isArray(args[j])) {
					var nmarr = args[j], k = nmarr.length;
					while (k--) {
						if (i === nmarr[k].nm) {
							//original
							//return nmarr[k].v;
							//var vx = $grid.fmatter($grid[0], "", nmarr[k].v, k + 1, format);
							var vx = $grid[0].formatter("", nmarr[k].v, k + 1, format); // Note the "k + 1"...the "+ 1" accounts for the "empty" column.
							return vx;
						}
					}
				}
			}
			return "ERROR";
		});
	}
});

$.jgrid.extend({
	// OVERRIDES BASE. This is a BUG FIX. The function doesn't work
	// with the custom aggregates feature. Duh! See below.
	getCol: function (col, obj, mathopr) {
		var ret = [], val, sum = 0, min, max, v;
		obj = typeof obj !== 'boolean' ? false : obj;
		if (mathopr === undefined) { mathopr = false; }
		this.each(function () {
			var $t = this, pos = -1;
			if (!$t.grid) { return; }
			if (isNaN(col)) {
				$($t.p.colModel).each(function (i) {
					if (this.name === col) {
						pos = i;
						return false;
					} else {
						return true;
					}
				});
			} else {
				pos = parseInt(col, 10);
			}

			if (pos >= 0) {
				var ln = $t.rows.length, i = 0, dlen = 0;
				if (ln && ln > 0) {
					while (i < ln) {
						if (($t.rows[i].className.indexOf('jqgrow') >= 0) && (!$t.rows[i].className.indexOf('jqgroup2') >= 0)) {
							try {
								val = $.unformat.call($t, $($t.rows[i].cells[pos]), { rowId: $t.rows[i].id, colModel: $t.p.colModel[pos] }, pos);
							} catch (e) {
								val = $.jgrid.htmlDecode($t.rows[i].cells[pos].innerHTML);
							}
							/* OLD CODE
							if (mathopr) {
							v = parseFloat(val);
							if (!isNaN(v)) {
							sum += v;
							if (max === undefined) { max = min = v; }
							min = Math.min(min, v);
							max = Math.max(max, v);
							dlen++;
							}
							}
							else if (obj) { ret.push({ id: $t.rows[i].id, value: val }); }
							else { ret.push(val); }
							*/
							// NEW CODE
							if (!$.isFunction(mathopr) && (mathopr)) {
								v = parseFloat(val);
								if (!isNaN(v)) {
									sum += v;
									if (max === undefined) { max = min = v; }
									min = Math.min(min, v);
									max = Math.max(max, v);
									//dlen++;
								}
							} else if (obj) {
								ret.push({ id: $t.rows[i].id, value: val });
							} else {
								ret.push(val);
							}
							dlen++; // NEW, counting string fields.
							// END NEW CODE
						}
						i++;
					}
					/* OLD CODE
					if(mathopr) {
					switch(mathopr.toLowerCase()){
					case 'sum': ret =sum; break;
					case 'avg': ret = sum/dlen; break;
					case 'count': ret = (ln-1); break;
					case 'min': ret = min; break;
					case 'max': ret = max; break;
					}
					}*/
					// NEW CODE
					if (!$.isFunction(mathopr) && (mathopr)) {
						switch (mathopr.toLowerCase()) {
							case 'sum': ret = sum; break;
							case 'avg': ret = sum / dlen; break;
							case 'count': ret = dlen; break; // NEW, Changed from "ln"
							case 'min': ret = min; break;
							case 'max': ret = max; break;
							default: ret = eval(mathopr)(sum, i, $t.rows);
						}
					} else if ($.isFunction(mathopr)) {
						ret = mathopr(val, col, $t.rows);
					}
					// END NEW CODE
				}
			}
		});
		return ret;
	},
	// OVERRIDES BASE. Allows for custom aggregate functions to be described as a string, opposed to a function pointer.
	groupingCalculations: {
		handler: function (fn, v, field, round, roundType, rc) {
			var funcs = {
				sum: function () {
					return parseFloat(v || 0) + parseFloat((rc[field] || 0));
				},

				min: function () {
					if (v === "") {
						return parseFloat(rc[field] || 0);
					}
					return Math.min(parseFloat(v), parseFloat(rc[field] || 0));
				},

				max: function () {
					if (v === "") {
						return parseFloat(rc[field] || 0);
					}
					return Math.max(parseFloat(v), parseFloat(rc[field] || 0));
				},

				count: function () {
					if (v === "") { v = 0; }
					if (rc.hasOwnProperty(field)) {
						return v + 1;
					}
					return 0;
				},

				avg: function () {
					// the same as sum, but at end we divide it
					// so use sum instead of duplicating the code (?)
					return funcs.sum();
				}
			};

			// OLD CODE
			//if(!funcs[fn]) {
			//	throw ("jqGrid Grouping No such method: " + fn);
			//}
			//var res = funcs[fn]();
			// NEW CODE - creating possibility for CUSTOM AGGREGATES in jqGridCustomAggregateFunctions.js
			var res = null;
			if (funcs[fn]) {
				res = funcs[fn]();
			} else if (typeof fn === "string") {
				res = eval(fn)(v, field, rc);
			}
			// END NEW CODE

			if (round != null) {
				if (roundType === 'fixed') {
					res = res.toFixed(round);
				} else {
					var mul = Math.pow(10, round);
					res = Math.round(res * mul) / mul;
				}
			}

			return res;
		}
	},
	// OVERRIDE BASE. Author calculating averages incorrectly.
	groupingPrepare: function (record, irow, rn) {
		this.each(function () { // JASON
			//if ((this.length > 0) && (this[0])) {
			// HACK
			//if (!this[0]) {
			//	return;
			//}

			var grp = this.p.groupingView, $t = this, i,
			grlen = grp.groupField.length,
			fieldName,
			v,
			displayName,
			displayValue,
			changed = 0;

			// CUSTOMIZATION - Ensure groups are available before attempting to load them. This is a component
			// BUG FIX due to the rendering happing faster then the grouping, sometimes. This loop ensures
			// they are available in a timely manner, in the minimum time possible.
			//if ((!grp.groups) || (grp.groups.length === 0)) {
			//	setTimeout(function () {
			//		var $g = $("#" + I.ggGridName);
			//		$g.groupingPrepare(record, irow, rn);
			//	}, 10);
			//	return false;
			//}

			for (i = 0; i < grlen; i++) {
				fieldName = grp.groupField[i];
				displayName = grp.displayField[i];
				v = record[fieldName];
				displayValue = displayName == null ? null : record[displayName];

				if (displayValue == null) {
					displayValue = v;
				}
				if (v !== undefined) {
					if (irow === 0) {
						// First record always starts a new group
						grp.groups.push({ idx: i, dataIndex: fieldName, value: v, displayValue: displayValue, startRow: irow, cnt: 1, summary: [] });
						grp.lastvalues[i] = v;
						grp.counters[i] = { cnt: 1, pos: grp.groups.length - 1, summary: $.extend(true, [], grp.summary) };
						$.each(grp.counters[i].summary, function () {
							if ($.isFunction(this.st)) {
								this.v = this.st.call($t, this.v, this.nm, record);
							} else {
								/* OLD CODE
								this.v = $($t).jqGrid('groupingCalculations.handler', this.st, this.v, this.nm, this.sr, this.srt, record);
								if (this.st.toLowerCase() === 'avg' && this.sd) {
								this.vd = $($t).jqGrid('groupingCalculations.handler', this.st, this.vd, this.sd, this.sr, this.srt, record);
								}*/
								// NEW CODE
								if (this.st.toLowerCase() === 'avg') {// && this.sd) {
									//this.vd = $($t).jqGrid('groupingCalculations.handler',this.st, this.vd, this.sd, this.sr, this.srt, record);
									this.vd = $($t).jqGrid('groupingCalculations.handler', this.st, this.vd, this.nm, this.sr, this.srt, record);
									this.v = this.vd / (grp.counters[i].cnt);
								} else {
									this.v = $($t).jqGrid('groupingCalculations.handler', this.st, this.v, this.nm, this.sr, this.srt, record);
								}
								// END NEW CODE
							}
						});
						grp.groups[grp.counters[i].pos].summary = grp.counters[i].summary;
					} else {
						if (typeof v !== "object" && ($.isArray(grp.isInTheSameGroup) && $.isFunction(grp.isInTheSameGroup[i]) ? !grp.isInTheSameGroup[i].call($t, grp.lastvalues[i], v, i, grp) : grp.lastvalues[i] !== v)) {
							// This record is not in same group as previous one
							grp.groups.push({ idx: i, dataIndex: fieldName, value: v, displayValue: displayValue, startRow: irow, cnt: 1, summary: [] });
							grp.lastvalues[i] = v;
							changed = 1;
							grp.counters[i] = { cnt: 1, pos: grp.groups.length - 1, summary: $.extend(true, [], grp.summary) };
							$.each(grp.counters[i].summary, function () {
								if ($.isFunction(this.st)) {
									this.v = this.st.call($t, this.v, this.nm, record);
								} else {
									/* OLD CODE
									this.v = $($t).jqGrid('groupingCalculations.handler', this.st, this.v, this.nm, this.sr, this.srt, record);
									if (this.st.toLowerCase() === 'avg' && this.sd) {
									this.vd = $($t).jqGrid('groupingCalculations.handler', this.st, this.vd, this.sd, this.sr, this.srt, record);
									}*/
									// NEW CODE
									if (this.st.toLowerCase() === 'avg') {// && this.sd) {
										//this.vd = $($t).jqGrid('groupingCalculations.handler',this.st, this.vd, this.sd, this.sr, this.srt, record);
										this.vd = $($t).jqGrid('groupingCalculations.handler', this.st, this.vd, this.nm, this.sr, this.srt, record);
										this.v = this.vd / (grp.counters[i].cnt);
									} else {
										this.v = $($t).jqGrid('groupingCalculations.handler', this.st, this.v, this.nm, this.sr, this.srt, record);
									}
									// END NEW CODE
								}
							});
							grp.groups[grp.counters[i].pos].summary = grp.counters[i].summary;
						} else {
							if (changed === 1) {
								// This group has changed because an earlier group changed.
								grp.groups.push({ idx: i, dataIndex: fieldName, value: v, displayValue: displayValue, startRow: irow, cnt: 1, summary: [] });
								grp.lastvalues[i] = v;
								grp.counters[i] = { cnt: 1, pos: grp.groups.length - 1, summary: $.extend(true, [], grp.summary) };
								$.each(grp.counters[i].summary, function () {
									if ($.isFunction(this.st)) {
										this.v = this.st.call($t, this.v, this.nm, record);
									} else {
										/* OLD CODE
										this.v = $($t).jqGrid('groupingCalculations.handler', this.st, this.v, this.nm, this.sr, this.srt, record);
										if (this.st.toLowerCase() === 'avg' && this.sd) {
										this.vd = $($t).jqGrid('groupingCalculations.handler', this.st, this.vd, this.sd, this.sr, this.srt, record);
										}*/
										// NEW CODE
										if (this.st.toLowerCase() === 'avg') {// && this.sd) {
											//this.vd = $($t).jqGrid('groupingCalculations.handler',this.st, this.vd, this.sd, this.sr, this.srt, record);
											this.vd = $($t).jqGrid('groupingCalculations.handler', this.st, this.vd, this.nm, this.sr, this.srt, record);
											this.v = this.vd / (grp.counters[i].cnt);
										} else {
											this.v = $($t).jqGrid('groupingCalculations.handler', this.st, this.v, this.nm, this.sr, this.srt, record);
										}
										// END NEW CODE
									}
								});
								grp.groups[grp.counters[i].pos].summary = grp.counters[i].summary;
							} else {
								grp.counters[i].cnt += 1;
								grp.groups[grp.counters[i].pos].cnt = grp.counters[i].cnt;
								$.each(grp.counters[i].summary, function () {
									if ($.isFunction(this.st)) {
										this.v = this.st.call($t, this.v, this.nm, record);
									} else {
										/* OLD CODE
										this.v = $($t).jqGrid('groupingCalculations.handler', this.st, this.v, this.nm, this.sr, this.srt, record);
										if (this.st.toLowerCase() === 'avg' && this.sd) {
										this.vd = $($t).jqGrid('groupingCalculations.handler', this.st, this.vd, this.sd, this.sr, this.srt, record);
										}*/
										// NEW CODE
										if (this.st.toLowerCase() === 'avg') {// && this.sd) {
											//this.vd = $($t).jqGrid('groupingCalculations.handler',this.st, this.vd, this.sd, this.sr, this.srt, record);
											this.vd = $($t).jqGrid('groupingCalculations.handler', this.st, this.vd, this.nm, this.sr, this.srt, record);
											this.v = this.vd / (grp.counters[i].cnt);
										} else {
											this.v = $($t).jqGrid('groupingCalculations.handler', this.st, this.v, this.nm, this.sr, this.srt, record);
										}
										// END NEW CODE
									}
								});
								grp.groups[grp.counters[i].pos].summary = grp.counters[i].summary;
							}
						}
					}
				}
			}
			//gdata.push( rData );
			// JASON COME BACK HERE, do we need to push?
		});
		return rn;
	},
	// OVERRIDE the BASE groupingRemove to use the "jqgroup2" class to find the groups to remove.
	groupingRemove: function (current) {
		return this.each(function () {
			var $t = this;
			if (current === undefined) {
				current = true;
			}
			$t.p.grouping = false;
			if (current === true) {
				var grp = $t.p.groupingView, i;
				// show previous hidden groups if they are hidden and weren't removed yet
				for (i = 0; i < grp.groupField.length; i++) {
					if (!grp.groupColumnShow[i] && grp.visibiltyOnNextGrouping[i]) {
						$($t).jqGrid('showCol', grp.groupField);
					}
				}
				// ********************************
				// CHANGED THIS TO "tr.jqgroup2"
				// ********************************
				$("tr.jqgroup2, tr.jqfoot", "#" + $.jgrid.jqID($t.p.id) + " tbody:first").remove();
				// ********************************
				$("tr.jqgrow:hidden", "#" + $.jgrid.jqID($t.p.id) + " tbody:first").show();
			} else {
				$($t).trigger("reloadGrid");
			}
		});
	},
	// OVERRIDE the BASE group toggle mechanism. Collapse works the same, but the expand first expands each level every
	// time the user clicks "expand" until each level is expanded, and then finally the data appears. When they click
	// an individual group to expand ALL nodes fully expand at that time. This is a novel approach to expanding data
	// and is well liked by users.
	groupingToggle: function (hid) {
		this.each(function () {
			var $t = this,
			grp = $t.p.groupingView,
			strpos = hid.split('_'),
			num = parseInt(strpos[strpos.length - 2], 10);
			strpos.splice(strpos.length - 2, 2);
			var uid = strpos.join("_"),
			minus = grp.minusicon,
			plus = grp.plusicon,
			tar = $("#" + $.jgrid.jqID(hid)),
			r = tar.length ? tar[0].nextSibling : null,
			tarspan = $("#" + $.jgrid.jqID(hid) + " span." + "tree-wrap-" + $t.p.direction),
			getGroupingLevelFromClass = function (className) {
				var nums = $.map(className.split(" "), function (item) {
					if (item.substring(0, uid.length + 1) === uid + "_") {
						return parseInt(item.substring(uid.length + 1), 10);
					}
				});
				return nums.length > 0 ? nums[0] : undefined;
			},
			itemGroupingLevel,
			showData,
			collapsed = false;
			if (tarspan.hasClass(minus)) {
				if (grp.showSummaryOnHide) {
					if (r) {
						while (r) {
							if ($(r).hasClass('jqfoot')) {
								var lv = parseInt($(r).attr("jqfootlevel"), 10);
								if (lv <= num) {
									break;
								}
							}
							$(r).hide();
							r = r.nextSibling;
						}
					}
				} else {
					if (r) {
						while (r) {
							itemGroupingLevel = getGroupingLevelFromClass(r.className);
							if (itemGroupingLevel !== undefined && itemGroupingLevel <= num) {
								break;
							}
							if (itemGroupingLevel !== undefined) {
								$(r).find(">td>span." + "tree-wrap-" + $t.p.direction).removeClass(minus).addClass(plus);
							}
							$(r).hide();
							r = r.nextSibling;
						}
					}
				}
				tarspan.removeClass(minus).addClass(plus);
				collapsed = true;
			} else {
				if (r) {
					showData = undefined;
					while (r) {
						itemGroupingLevel = getGroupingLevelFromClass(r.className);
						if (showData === undefined) {
							showData = itemGroupingLevel === undefined; // if the first row after the opening group is data row then show the data rows
						}
						if (itemGroupingLevel !== undefined) {
							if (itemGroupingLevel <= num) {
								break; // next item of the same lever are found
							}
							if (itemGroupingLevel === num + 1) {
								$(r).show().find(">td>span." + "tree-wrap-" + $t.p.direction).removeClass(minus).addClass(plus);
							}
						} else if (showData) {
							$(r).show();
						}
						r = r.nextSibling;
					}
				}
				tarspan.removeClass(plus).addClass(minus);
			}
			$($t).triggerHandler("jqGridGroupingClickGroup", [hid, collapsed]);
			if ($.isFunction($t.p.onClickGroup)) { $t.p.onClickGroup.call($t, hid, collapsed); }
		});

		// Add the expand-collapse links.
		$(this).setupExpandCollapseLinks(true);

		return false;
	},
	// OVERRIDE the BASE drawing of the group summary band. This allows us to create table structure in the same
	// row as the group itself. ONLY ONE LINE OF CODE CHANGED...To call OUR function. See below.
	groupingRender: function (grdata, colspans) {
		return this.each(function () {
			var $t = this,
		grp = $t.p.groupingView,
		str = "", icon = "", hid, clid, pmrtl = grp.groupCollapse ? grp.plusicon : grp.minusicon, gv = "", cp = [], len = grp.groupField.length;
			pmrtl += " tree-wrap-" + $t.p.direction;
			$.each($t.p.colModel, function (i, n) {
				var ii;
				for (ii = 0; ii < len; ii++) {
					if (grp.groupField[ii] === n.name) {
						cp[ii] = i;
						break;
					}
				}
			});
			var toEnd = 0;
			function findGroupIdx(ind, offset, grp) {
				var ret = false, i;
				if (offset === 0) {
					ret = grp[ind];
				} else {
					var id = grp[ind].idx;
					if (id === 0) {
						ret = grp[ind];
					} else {
						for (i = ind; i >= 0; i--) {
							if (grp[i].idx === id - offset) {
								ret = grp[i];
								break;
							}
						}
					}
				}
				return ret;
			}
			function buildSummaryTd(i, ik, grp, foffset) {
				var fdata = findGroupIdx(i, ik, grp.groups), cm = $t.p.colModel, vv = "", grlen = fdata.cnt, str = "", k;
				for (k = foffset; k < colspans; k++) {
					var tmpdata = "<td " + $t.formatCol(k, 1, '') + ">&#160;</td>", tplfld = "{0}";
					$.each(fdata.summary, function () {
						if (this.nm === cm[k].name) {
							if (cm[k].summaryTpl) {
								tplfld = cm[k].summaryTpl;
							}
							if (typeof this.st === 'string' && this.st.toLowerCase() === 'avg') {
								if (this.sd && this.vd) {
									this.v = (this.v / this.vd);
								} else if (this.v && grlen > 0) {
									this.v = (this.v / grlen);
								}
							}
							try {
								//vv = $t.formatter('', this.v, k, this);
								vv = $.jgrid.template(grp.groupText[k], this.v, k, fdata.summary);
							} catch (ef) {
								vv = this.v;
							}
							tmpdata = "<td " + $t.formatCol(k, 1, '') + ">" + $.jgrid.format(tplfld, vv) + "</td>";
							return false;
						} else {
							return false;
						}
					});
					str += tmpdata;
				}
				return str;
			}
			var sumreverse = $.makeArray(grp.groupSummary);
			sumreverse.reverse();
			$.each(grp.groups, function (i, n) {
				toEnd++;
				clid = $t.p.id + "ghead_" + n.idx;
				hid = clid + "_" + i;
				icon = "<span style='position: absolute; float: left; cursor:pointer;' class='ui-icon " + pmrtl + "' onclick=\"jQuery('#" + $.jgrid.jqID($t.p.id) + "').jqGrid('groupingToggle','" + hid + "');return false;\"></span>";
				try {
					if ($.isArray(grp.formatDisplayField) && $.isFunction(grp.formatDisplayField[n.idx])) {
						n.displayValue = grp.formatDisplayField[n.idx].call($t, n.displayValue, n.value, $t.p.colModel[cp[n.idx]], n.idx, grp);
						gv = n.displayValue;
					} else {
						//gv = $t.formatter(hid, n.displayValue, cp[n.idx], n.value);
						gv = $.jgrid.template(grp.groupText[n.idx], n.displayValue, cp[n.idx], n.summary);
					}
				} catch (egv) {
					gv = n.displayValue;
				}
				if (grp.groupSummaryPos[n.idx] === 'header') {
					str += "<tr id=\"" + hid + "\"" + (grp.groupCollapse && n.idx > 0 ? " style=\"display:none;\" " : " ") + "role=\"row\" class= \"ui-widget-content jqgroup ui-row-" + $t.p.direction + " " + clid + "\"><td style=\"padding-left:" + (n.idx * 12) + "px;" + "\">" + icon + $.jgrid.template(grp.groupText[n.idx], gv, n.cnt, n.summary) + "</td>";
					str += buildSummaryTd(i, i + 1, grp, 1);
					str += "</tr>";
				} else {
					// ******************************************
					// OLD CODE
					// ******************************************
					//str += "<tr id=\"" + hid + "\"" + (grp.groupCollapse && n.idx > 0 ? " style=\"display:none;\" " : " ") + "role=\"row\" class= \"ui-widget-content jqgroup ui-row-" + $t.p.direction + " " + clid + "\"><td style=\"padding-left:" + (n.idx * 12) + "px;" + "\" colspan=\"" + colspans + "\">" + icon + $.jgrid.template(grp.groupText[n.idx], gv, n.cnt, n.summary) + "</td></tr>";
					// ******************************************
					// BBCRM CHANGES
					// ******************************************
					//alert(gridId + "......." + n.idx);
					//alert(getGroupRow(gridId, n.idx));
					var rowClass = "";
					if (n.idx === 0) {
						rowClass = "jqGridCustTableRoot";
					} else {
						rowClass = "jqGridCustTableChild";
					}
					// NOTE: changed jqgroup to jqgroup2 in class
					var rowTag = "<tr id=\"" + hid + "\"" + (grp.groupCollapse && n.idx > 0 ? " style=\"display:none;\" " : " ") + "role=\"row\" class= \"ui-widget-content jqgroup2 ui-row-" + $t.p.direction + " " + clid + " " + rowClass + "\">";
					icon = "<span style='position: absolute; float: left; left:" + (n.idx * $($t).groupPadding) + "px; cursor:pointer;' class='ui-icon " + pmrtl + "' onclick=\"jQuery('#" + $.jgrid.jqID($t.p.id) + "').jqGrid('groupingToggle','" + hid + "');return false;\"></span>";

					/// JASON ----------------CACHE THE GROUP ROW TEMPLATE !!!!!!!!!!!!!!!!!!!!!
					// note, cache for each group level
					str += $.jgrid.template($($t.grid).getGroupRow(n.idx, rowTag, icon), gv, n.cnt, n.summary);
					//str += $($t.grid).getGroupRow(n.idx, rowTag, icon);

					///////////////*******************************

					//alert(str);
					// ******************************************
				}

				// THIS SECTION COPIES IN THE DETAIL RECORDS

				var leaf = len - 1 === n.idx;
				if (leaf) {
					var gg = grp.groups[i + 1], kk, ik;
					var end = gg !== undefined ? grp.groups[i + 1].startRow : grdata.length;
					for (kk = n.startRow; kk < end; kk++) {
						str += grdata[kk].join('');
					}
					if (grp.groupSummaryPos[n.idx] !== 'header') {
						var jj;
						if (gg !== undefined) {
							for (jj = 0; jj < grp.groupField.length; jj++) {
								if (gg.dataIndex === grp.groupField[jj]) {
									break;
								}
							}
							toEnd = grp.groupField.length - jj;
						}
						for (ik = 0; ik < toEnd; ik++) {
							if (!sumreverse[ik]) { continue; }
							var hhdr = "";
							if (grp.groupCollapse && !grp.showSummaryOnHide) {
								hhdr = " style=\"display:none;\"";
							}
							str += "<tr" + hhdr + " jqfootlevel=\"" + (n.idx - ik) + "\" role=\"row\" class=\"ui-widget-content jqfoot ui-row-" + $t.p.direction + "\">";
							str += buildSummaryTd(i, ik, grp, 0);
							str += "</tr>";
						}
						toEnd = jj;
					}
				}
			});
			$("#" + $.jgrid.jqID($t.p.id) + " tbody:first").append(str);
			// free up memory
			str = null;
		});
	},

	// OVERRIDE BASE. Make it work with GROUPING ENABLED
	setFrozenColumns: function () {
		return this.each(function () {
			if (!this.grid) { return; }
			var $t = this, cm = $t.p.colModel, i = 0, len = cm.length, maxfrozen = -1, frozen = false;
			// TODO treeGrid and grouping  Support
			// OLD CODE
			//if($t.p.subGrid === true || $t.p.treeGrid === true || $t.p.cellEdit === true || $t.p.sortable || $t.p.scroll || $t.p.grouping )
			//{
			//	return;
			//}
			// END OLD CODE
			if ($t.p.rownumbers) { i++; }
			if ($t.p.multiselect) { i++; }

			// get the max index of frozen col
			while (i < len) {
				// from left, no breaking frozen
				if (cm[i].frozen === true) {
					frozen = true;
					maxfrozen = i;
				} else {
					break;
				}
				i++;
			}
			if (maxfrozen >= 0 && frozen) {
				var top = 29; // $t.p.caption ? $($t.grid.cDiv).outerHeight() : 0,
				hth = $(".ui-jqgrid-htable", "#gview_" + $.jgrid.jqID($t.p.id)).height();
				//headers
				if ($t.p.toppager) {
					top = top + $($t.grid.topDiv).outerHeight();
				}
				if ($t.p.toolbar[0] === true) {
					if ($t.p.toolbar[1] !== "bottom") {
						top = top + $($t.grid.uDiv).outerHeight();
					}
				}
				$t.grid.fhDiv = $('<div style="position:absolute;left:0px;top:' + top + 'px;height:' + hth + 'px;" class="frozen-div ui-state-default ui-jqgrid-hdiv"></div>');
				$t.grid.fbDiv = $('<div style="position:absolute;left:0px;top:' + (parseInt(top, 10) + parseInt(hth, 10) + 1) + 'px;overflow-y:hidden" class="frozen-bdiv ui-jqgrid-bdiv"></div>');
				$("#gview_" + $.jgrid.jqID($t.p.id)).append($t.grid.fhDiv);
				var htbl = $(".ui-jqgrid-htable", "#gview_" + $.jgrid.jqID($t.p.id)).clone(true);
				// groupheader support - only if useColSpanstyle is false
				if ($t.p.groupHeader) {
					$("tr.jqg-first-row-header, tr.jqg-third-row-header", htbl).each(function () {
						$("th:gt(" + maxfrozen + ")", this).remove();
					});
					var swapfroz = -1, fdel = -1, cs, rs;
					$("tr.jqg-second-row-header th", htbl).each(function () {
						cs = parseInt($(this).attr("colspan"), 10);
						rs = parseInt($(this).attr("rowspan"), 10);
						if (rs) {
							swapfroz++;
							fdel++;
						}
						if (cs) {
							swapfroz = swapfroz + cs;
							fdel++;
						}
						if (swapfroz === maxfrozen) {
							return false;
						}

						return true;
					});
					if (swapfroz !== maxfrozen) {
						fdel = maxfrozen;
					}
					$("tr.jqg-second-row-header", htbl).each(function () {
						$("th:gt(" + fdel + ")", this).remove();
					});
				} else {
					$("tr", htbl).each(function () {
						$("th:gt(" + maxfrozen + ")", this).remove();
					});
				}
				$(htbl).width(1);
				// resizing stuff
				$($t.grid.fhDiv).append(htbl)
				.mousemove(function (e) {
					if ($t.grid.resizing) {
						$t.grid.dragMove(e);
						return false;
					} else {
						return true;
					}
				});
				$($t).bind('jqGridResizeStop.setFrozenColumns', function (e, w, index) {
					var rhth = $(".ui-jqgrid-htable", $t.grid.fhDiv);
					$("th:eq(" + index + ")", rhth).width(w);
					var btd = $(".ui-jqgrid-btable", $t.grid.fbDiv);
					$("tr:first td:eq(" + index + ")", btd).width(w);
				});
				// sorting stuff
				$($t).bind('jqGridSortCol.setFrozenColumns', function (e, index, idxcol) {

					var previousSelectedTh = $("tr.ui-jqgrid-labels:last th:eq(" + $t.p.lastsort + ")", $t.grid.fhDiv), newSelectedTh = $("tr.ui-jqgrid-labels:last th:eq(" + idxcol + ")", $t.grid.fhDiv);

					$("span.ui-grid-ico-sort", previousSelectedTh).addClass('ui-state-disabled');
					$(previousSelectedTh).attr("aria-selected", "false");
					$("span.ui-icon-" + $t.p.sortorder, newSelectedTh).removeClass('ui-state-disabled');
					$(newSelectedTh).attr("aria-selected", "true");
					if (!$t.p.viewsortcols[0]) {
						if ($t.p.lastsort !== idxcol) {
							$("span.s-ico", previousSelectedTh).hide();
							$("span.s-ico", newSelectedTh).show();
						}
					}
				});

				// data stuff
				//TODO support for setRowData
				$("#gview_" + $.jgrid.jqID($t.p.id)).append($t.grid.fbDiv);
				$($t.grid.bDiv).scroll(function () {
					$($t.grid.fbDiv).scrollTop($(this).scrollTop());
				});
				if ($t.p.hoverrows === true) {
					$("#" + $.jgrid.jqID($t.p.id)).unbind('mouseover').unbind('mouseout');
				}
				$($t).bind('jqGridAfterGridComplete.setFrozenColumns', function () {
					$("#" + $.jgrid.jqID($t.p.id) + "_frozen").remove();
					$($t.grid.fbDiv).height($($t.grid.bDiv).height() - 16);
					var btbl = $("#" + $.jgrid.jqID($t.p.id)).clone(true);
					$("tr[role=row]", btbl).each(function () {
						$("td[role=gridcell]:gt(" + maxfrozen + ")", this).remove();
					});

					$(btbl).width(1).attr("id", $t.p.id + "_frozen");
					$($t.grid.fbDiv).append(btbl);
					if ($t.p.hoverrows === true) {
						$("tr.jqgrow", btbl).hover(
							function () { $(this).addClass("ui-state-hover"); $("#" + $.jgrid.jqID(this.id), "#" + $.jgrid.jqID($t.p.id)).addClass("ui-state-hover"); },
							function () { $(this).removeClass("ui-state-hover"); $("#" + $.jgrid.jqID(this.id), "#" + $.jgrid.jqID($t.p.id)).removeClass("ui-state-hover"); }
						);
						$("tr.jqgrow", "#" + $.jgrid.jqID($t.p.id)).hover(
							function () { $(this).addClass("ui-state-hover"); $("#" + $.jgrid.jqID(this.id), "#" + $.jgrid.jqID($t.p.id) + "_frozen").addClass("ui-state-hover"); },
							function () { $(this).removeClass("ui-state-hover"); $("#" + $.jgrid.jqID(this.id), "#" + $.jgrid.jqID($t.p.id) + "_frozen").removeClass("ui-state-hover"); }
						);
					}
					// NEW CODE, add a row for the summary label when grouped.
					var $row = $("<tr><td class=\"ui-jqgrid-ftable-summarylabel\">" + $(this)[0].p.summaryFooterLabel + "</td></tr>");
					$(btbl).append($row);
					// END NEW CODE
					btbl = null;
				});
				if (!$t.grid.hDiv.loading) {
					$($t).triggerHandler("jqGridAfterGridComplete");
				}
				$t.p.frozenColumns = true;
			}
		});
	}

});
