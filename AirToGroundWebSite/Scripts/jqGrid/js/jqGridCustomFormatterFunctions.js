﻿// Provides custom cell formatting, including summary cells as well as data cells.
// ** ALL COLUMNS IN COLUMN MODEL REQUIRE A FORMATTER! USE THIS ONE, AT LEAST, AS A DEFAULT!!!
// You will create two of these.
$.fn.fmatter.stringFormatter = function (cellval, opts, rowObject, action) {
	return ($.fmatter.isValue(cellval) && cellval !== "") ? cellval : opts.defaultValue || "&#160;";
};

// Base functionality. The others would be in locale customizations like /js/grid.locale-de.custom.js
if (!$.jgrid.formatter.CUSTOM_FORMATTER_REGISTERED) {
	$.jgrid.formatter.toLocaleString = function (val, lookupType) {
		var strings = {};//{ 0: "Ze Columns" };

		if (!isNaN(lookupType)) {
			if (strings[lookupType]) {
				return strings[lookupType];
			} else {
				return val;
			}
		} else {
			return val;
		}
	}
};

$.fn.fmatter.localizedText = function (cellval, lookupType) {
	return $.jgrid.formatter.toLocaleString(cellval, lookupType);
};
//function stringFormatter(cellval, opts, rowObject) {
//	return 5;// cellval;// $.fn.fmatter.stringFormatter(cellval, opts, rowObject);
//}
