﻿// CUSTOM jqGrid functionality
/*jshint eqeqeq:false */
/*global jQuery */
"use strict";

/*
jqGrid custom debugging feature.

debug()
	args:
		$this: the passing class
		depth: the level
			-1 to clear the console
			0 log
			1 +info
			2 +warn
			3 +error [console.dir($this)] - the properties of the object (essentially displays all info)
			else error, fix the debug args
		msg: the information to log
		debugOverride: debug without instance...used during initialization
*/
$.extend($.jgrid, {
	debugMode: false, // Whether to turn on the internal console logging. 
	debugDepth: 0, // ggDebugDepth is what level to display (0 log, 1 info, 2 warn, 3 error, 4 all). Note, this is additive...
	debug: function ($this, depth, msg, debugOverride) {

		if ((window.console) && ((debugOverride === true) || ($.jgrid.debugMode === true))) {
			var m1 = "";
			var m2 = "";
			var m = "";

			// First half of message
			if ($this != undefined) {
				m1 = $.jgrid.getClass($this) + "::";
			} else {
				m1 = "Undefined Source::";
			}

			// Second half of message
			if (typeof (msg) === "string") {
				m2 = msg;
			} else {
				if (depth === 3) {
					if ($this != undefined) {
						m2 += "SOURCE OBJECT PROPERTIES\n" + $.jgrid.getPropsAndVals($this);
					}
				}
				m2 += "TARGET OBJECT PROPERTIES\n" + $.jgrid.getPropsAndVals(msg);
			}

			// Account for bad debugging arguments
			if (depth <= 3) {
				m = m1 + m2;
			} else {
				m = m1 + "INVALID DEBUG ARGUMENT::" + m2;
			}

			// Write to console
			switch (depth) {
				case -1: window.console.clear();
				case 0: if ($.jgrid.debugDepth >= 0) { window.console.log(m); } break;
				case 1: if ($.jgrid.debugDepth >= 1) { window.console.info(m); } break;
				case 2: if ($.jgrid.debugDepth >= 2) { window.console.warn(m); } break;
				case 3: if ($.jgrid.debugDepth >= 3) { window.console.error(m); } break;
				default: window.console.error(m);
			}
		}
		return true;
	},
	getClass: function (obj) {
		if (obj && obj.constructor && obj.constructor.toString) {
			var arr = obj.constructor.toString().match(/function\s*(\w+)/);

			if (arr && arr.length == 2) {
				return arr[1];
			}
		}
		return undefined;
	},
	getPropsAndVals: function (obj) {
		var results = new Array();
		for (var key in obj) {
			if (typeof obj[key] !== 'function') {
				results.push(key + ":" + obj[key]);
			}
		}
		return results;
	}
});

// CUSTOM PROPERTIES AND METHODS
$.jgrid.extend({
	gridName: "myGroupGrid1", // DIV name, grid name, ID value

	// These ensure the default group sorting values are applied correctly.
	defaultGroupColumn: "LIST_CODE_DESC",
	defaultGroupSortOrder: "asc",

	// Group stuff
	startGroupsCollapsed: false,
	primaryGroupPositionLabel: "Group by", // Positioned in front of initial group link in top group bar (trailing space when renendered is automatically included)
	naryGroupPositionLabel: "then group by", // Positioned in second and beyond group links in top group bar (trailing space when renendered is automatically included)

	// Provides ability to customize expand/collapse labels
	expandAllLabel: "Expand All", // Label for expanding all
	expandAllHint: "Expand All Groups", // Hint for expanding all
	expandAllHtml: "", // Html for expand all click
	expandOneLevelLabel: "Expand One Level", // Label for expanding one level at a time
	expandOneLevelHint: "Expand One Level at a Time", // Hint for expanding one level at a time
	expandLevelHtml: "", // Html for expand one click
	collapseAllLabel: "Collapse All", // Label for collapsing all
	collapseAllHint: "Collapse All Groups", // Hint for collapsing all
	collapseAllHtml: "", // Html for collapse all click
	expandLevel: 0, // Current expand level
	expandCollapseSeparatorHtml: "<span>&nbsp;|&nbsp;</span>", // Link separator. Must be in html tag like <div> or <span>
	expandCollapseLabelMode: "HIDE", // "DISABLE" or "HIDE", makes links disappear or disable based on availability

	// Footer cell zero summary label
	summaryFooterLabel: "Totals:",

	// NEW group UI helpers for the group UI icons
	groupPadding: 12, // MAKE CONFIGURABLE. Update CSS in HTML and .css
	groupIcon: 16, // MAKE CONFIGURABLE. Update CSS in HTML and .css
	groupIconFudge: 2, // MAKE CONFIGURABLE. A fudge factor.

	// Can we collapse all?
	canCollapseAll: function () {
		return this.collapseableCount() > 0;
	},

	// Can we expand all? (or one level?)
	canExpandAll: function () {
		return this.expandableCount() > 0;
	},

	// How many groups can be collapsed?
	collapseableCount: function () {
		return this.find("span.ui-icon.ui-icon-circlesmall-minus.tree-wrap-" + this[0].p.direction).length;
	},

	// Creates group bar buttons and performs grouping
	createGroupButton: function (field, label, sortdir) {
		var $list = $("#" + this[0].p.gridName + "_ol");

		// Does the column already exist? If so, remove it. This effectively allows user to "move" a grouping
		var $oneThatExists = $("#jqgroupedlistcol_" + field);
		if ($oneThatExists.length > 0) {
			// Duplicate found. Move the group button to the end.
			if ($list[0].children.length > 1) {
				$oneThatExists.insertAfter($list[0].children[$list[0].children.length - 1]);
				this.fixGroupByPrefix();
			}
		} else {
			// Create the clickable group button being grouped

			var $sortIcons = $(
				"<span class=\"jqGridGroupButtonSortIcons\">" +
					"<span class=\"ui-grid-ico-sort ui-icon-asc ui-icon ui-icon-triangle-1-n ui-sort-ltr\" sort=\"asc\"></span>" +
					"<span class=\"ui-grid-ico-sort ui-icon-desc ui-state-disabled ui-icon ui-icon-triangle-1-s ui-sort-ltr\" sort=\"desc\"></span>" +
				"</span>");

			var $groupLabel = $("<span class=\"jqGridGroupButtonLabelText\" title=\"" + $.jgrid.formatter.toLocaleString("Sort by This Group", $.jgrid.formatter.GROUPS_HINT_SORT_GROUP) + "\">" + label + "</span>").click(function () {
				//alert(this.parentNode.attributes[0].textContent);

				// Get the grid.
				var $grid = $("#" + $(this).gridName);

				// Get the current sort
				var $groupCols = $("#" + $grid[0].p.gridName + "_ol li:not(.placeholderlist)").map(function () { return $(this).attr("data-column"); }).get();
				var sortField = this.parentNode.id.substring(("jqgroupedlistcol_".length), this.parentNode.id.length);
				var index = $groupCols.indexOf(sortField);
				var curSort = $grid[0].p.groupingView.groupOrder[index];
				var newSort = curSort === "asc" ? "desc" : "asc";

				// Apply the sort.
				$grid.doSort(sortField, -1, newSort);

				// Refresh the group.
				$grid.doGroup();

				// Fix the icons.
				$($(this).next()[0].childNodes[newSort === "asc" ? 0 : 1]).removeClass("ui-state-disabled");
				$($(this).next()[0].childNodes[newSort === "asc" ? 1 : 0]).addClass("ui-state-disabled");
			});

			var $groupingColumn = $(
				"<li id=\"jqgroupedlistcol_" + field + "\" data-column=\"" + field + "\">" +
					"<span class=\"jqGridGroupButtonLeadText\">" + this[0].p.primaryGroupPositionLabel + "</span>" +
				"</li>");

			$groupLabel.appendTo($groupingColumn);
			$sortIcons.appendTo($groupingColumn);

			// Fix the icon initial direction
			$($sortIcons[0].childNodes[sortdir === "asc" ? 0 : 1]).removeClass("ui-state-disabled");
			$($sortIcons[0].childNodes[sortdir === "asc" ? 1 : 0]).addClass("ui-state-disabled");

			$("<span class=\"jqGridGroupButton\" title=\"" + $.jgrid.formatter.toLocaleString("Remove This Group", $.jgrid.formatter.GROUPS_HINT_REMOVE_GROUP) + "\"></span>").click(function () {
				var parent = $($(this)[0].parentElement);

				// Re-show the column since grouping hides it.
				var $grid = $("#" + $(this).gridName);
				$grid.jqGrid("showCol", parent.attr("data-column"));

				// Now remove the group button at the top.
				parent.remove();

				// Fixup the leading group prefix label.
				$grid.fixGroupByPrefix();

				// Perform grouping
				$grid.doGroup();

			}).appendTo($groupingColumn);

			// Add the grouping button to the drop target
			//$groupingColumn.append(label);
			$groupingColumn.appendTo($list);

			// Fixup the leading group prefix label. 
			this.fixGroupByPrefix();
		}

		return true;
	},

	// Internal group method
	doGroup: function () {
		// Clear all grouping.
		var $grid = $(this);
		//$grid.jqGrid("groupingRemove");
		$grid.groupingRemove();

		// Regroup
		var $groupCols = $("#" + $grid[0].p.gridName + "_ol li:not(.placeholderlist)").map(function () { return $(this).attr("data-column"); }).get();
		// Do we have any groups?
		if ($groupCols.length > 0) {
			// Show "EMPTY" column.
			if (!$grid[0].p.frozenColumns) {
				$grid.jqGrid("showCol", "EMPTY");
				// TURN BACK ON WHEN THE FROZEN COLUMN FEATURE WORKS WITH EXPAND/COLLAPSE
				//$grid.jqGrid("setFrozenColumns");
			}

			//alert("got groups");
			var aGroupCols = new Array($groupCols.length);
			var aColumnShow = new Array($groupCols.length);
			var aGroupOrders = new Array($groupCols.length);
			var aGroupSummaryBot = new Array($groupCols.length);
			for (var x = 0; x < $groupCols.length; x++) {
				aGroupCols[x] = $groupCols[x];
				aColumnShow[x] = false; // Show the grouped column. Allows for sorting.
				// Summary in top
				aGroupSummaryBot[x] = false; // Change to false if summary in top. 
				var sortOrd = $grid[0].p.groupingView.groupOrder[$grid[0].p.groupingView.groupField.indexOf($groupCols[x])];
				aGroupOrders[x] = sortOrd ? sortOrd : $grid[0].p.defaultGroupSortOrder;
			}
			$grid[0].p.grouping = true;
			$grid.toggleSummaryFooter(true);
			$grid[0].p.userDataOnFooter = true;

			//$grid[0].p.groupingView.minusicon = "ui-icon-circlesmall-minus-NEW";
			//$grid[0].p.groupingView.plusicon = "ui-icon-circlesmall-plus-NEW";

			// Perform the grouping.
			// *****We also set groupDataSorted to true, so that it will pass the groupField column name as part of the 
			// sortCol argument on server-side requests. This means that all server-side requests will now have a sortCol 
			// in the form of {groupField} asc, {sortCol}. The need for this will become apparent when we adjust our paging query.
			$grid.groupingGroupBy(aGroupCols,
				{ groupField: aGroupCols, groupColumnShow: aColumnShow,
					groupCollapse: $grid[0].p.startGroupsCollapsed, groupOrder: aGroupOrders, groupSummary: aGroupSummaryBot,
					groupDataSorted: false, showSummaryOnHide: false
				});
			//$grid.groupingSetup();

			// Add the expand-collapse links.
			$grid.setupExpandCollapseLinks(true);
		} else if ($("#" + $grid[0].p.gridName + "_ol li").length === 0) {
			// No group columns left, put the place holder in
			//alert("no groups");
			//$grid.jqGrid('setGridParam', { grouping: false });
			$grid[0].p.grouping = false;
			$grid.toggleSummaryFooter(false);
			$grid[0].p.userDataOnFooter = false;

			// Empty group drop target.
			var $list = $("#" + $grid[0].p.gridName + "_ol");
			$('<li class="placeholderlist">' + $grid[0].p.captionLabel + '</li>').appendTo($list);

			// TURN BACK ON WHEN THE FROZEN COLUMN FEATURE WORKS WITH EXPAND/COLLAPSE
			// Hide "EMPTY" column.
			//if ($grid[0].p.frozenColumns === true) {
			//$grid.jqGrid("destroyFrozenColumns");
			$grid.jqGrid("hideCol", "EMPTY");
			//}

			// Remove the expand-collapse links.
			$grid.setupExpandCollapseLinks(false);
		}

		return true;
	},

	// Customized sorting considering the groups
	doSort: function (colname, colidx, sortdir) {
		//alert("onSortCol args (col name, col index, sort order): " + colname + " " + colidx + " " + sortdir);
		// Get our grouped columns.
		var $grid = $(this);
		var $groupCols = $("#" + $grid[0].p.gridName + "_ol li:not(.placeholderlist)").map(function () { return $(this).attr("data-column"); }).get();

		if (($groupCols.length > 0) && ($groupCols.indexOf(colname) > -1)) {
			//alert("group sort");
			// Build group sorting options.
			var cols = new Array($groupCols.length);
			var sdirs = new Array($groupCols.length);
			var gview = $(this)[0].p.groupingView;
			for (var x = 0; x < $groupCols.length; x++) {
				cols[x] = $groupCols[x];
				if ($groupCols[x] === colname) {
					sdirs[x] = sortdir;
				} else {
					sdirs[x] = gview.groupOrder[x];
				}
			}
			// Apply group sorting options.
			//gview.groupField = cols;
			gview.groupOrder = sdirs;
		} else if ($groupCols.length === 0) {
			//alert("no group sort");
			// No grouped columns. Clear group sorting options.
			var gview = $(this)[0].p.groupingView;
			//gview.groupField = new Array(0);
			gview.groupOrder = new Array(0);
		} else if ($groupCols.indexOf(colname) === -1) {
			//alert("regular col sort");
			// Regular column sorting.
			//alert(grid[0].p.sortname);
			$grid[0].p.sortname = colname;
			$grid[0].p.sortorder = sortdir;
			//return "stop"; // This will effectively kill the sorting.
		}
		// Restore the page so it ends up where it came from after sorting.
		$grid[0].p.page = $grid[0].p.postData.page;

		return true;
	},

	// How many groups can be expanded?
	expandableCount: function () {
		return this.find("span.ui-icon.ui-icon-circlesmall-plus.tree-wrap-" + this[0].p.direction).length;
	},

	// NEW method for expanding and collapsing ALL groups. Mode: "EXPAND_ONE", "EXPAND_ALL" ,"COLLAPSE_ALL"
	expandCollapseGroups: function (mode) {
		var $grid = this;
		if (mode === "EXPAND_ONE") {
			// Expand one group level - the next lowest level
			var high = $grid[0].p.groupingView.groupField.length - 1 || 0;
			var lowFound = false;
			var items = [];
			// Spin from bottom and look for the lowest levels last opened.
			while (high >= 0) {
				items = $grid.find("tr[id^='" + $grid[0].p.gridName + "ghead_" + high + "']" + " span.ui-icon.ui-icon-circlesmall-plus.tree-wrap-" + $grid[0].p.direction);
				if (items.length > 0) {
					// Expand the items.
					items.each(function () {
						if (this.parentNode.parentNode.style["display"] !== "none") {
							$(this).trigger("click");
						}
					});
				}
				high--;
			}
		} else if (mode === "EXPAND_ALL") {
			// Expand all non-expanded groups
			$grid.find("tr[id^='" + $grid[0].p.gridName + "ghead_']" + " span.ui-icon.ui-icon-circlesmall-plus.tree-wrap-" + $grid[0].p.direction).each(function () {
				$(this).trigger("click");
			});
		} else if (mode === "COLLAPSE_ALL") {
			// Collapse all non-collapsed groups
			$grid.find("tr[id^='" + $grid[0].p.gridName + "ghead_0']" + " span.ui-icon.ui-icon-circlesmall-minus.tree-wrap-" + $grid[0].p.direction).each(function () {
				$(this).trigger("click");
			});
		}

		// Intelligent paging as a result of this. When paging,
		// will keep collapsed or expanded.
		$grid[0].p.startGroupsCollapsed = $grid.canExpandAll();
		$grid[0].p.groupingView.groupCollapse = $grid[0].p.startGroupsCollapsed;

		return true;
	},

	// Fixup the leading group prefix label.
	fixGroupByPrefix: function () {
		var $list = $("#" + $(this).gridName + "_ol");
		var prefix = $(this)[0].p.primaryGroupPositionLabel;
		if ($list[0].children.length > 0) {
			// Position zero always "Group by"
			$list[0].children[0].childNodes[0].innerHTML = $(this)[0].p.primaryGroupPositionLabel;
			$list[0].children[0].childNodes[0].innerHTML = prefix;

			// Fix the one on the end too.
			if ($list[0].childElementCount > 2) {
				prefix = $(this)[0].p.naryGroupPositionLabel;
				$list[0].children[$list[0].children.length - 1].childNodes[0].innerHTML = prefix;
			}
		}

		return true;
	},

	// Creates the layout for the group summary band
	getGroupRow: function (groupIdx, rowTag, icon) {
		var $grid = $("#" + this.gridName);
		var colModel = $grid[0].p.colModel;

		var sLeft = rowTag;
		var sMid = "";
		var sRight = "</tr>";
		var colStartTag = "<td>";
		var colEndTag = "</td>";
		var align = "right";

		for (var x = 0; x < colModel.length; x++) {
			// No summmary info. Just pump in the data.
			if (colModel[x].align) {
				// Default the align to user provided model value
				align = colModel[x].align;
			} else {
				// Default right if no value provided
				align = "right";
			}

			if (true === true) {
				if (sMid === "") {
					var sTpl = "{0} - ({1}) Items";  // default

					if (colModel[x].summaryTpl) {
						if (colModel[x].summaryTpl.indexOf("{0}") > -1) {
							sTpl = colModel[x].summaryTpl.replace("{0}", "{" + colModel[x].name + "}") + colEndTag; // User override
						}
					}

					colStartTag = "<td style='text-align: " + align + "; padding-left: " + ((groupIdx * $grid.groupPadding) + $grid.groupIcon) + "px; width: ";
					if (groupIdx === 0) {
						sMid = colStartTag + (colModel[x].width - $grid.groupIcon + $grid.groupIconFudge) + "px; font-weight: 900'>" + icon + sTpl + colEndTag; // Initial label line
					} else {
						sMid = colStartTag + (colModel[x].width - (groupIdx * $grid.groupPadding) - $grid.groupIcon + $grid.groupIconFudge) + "px; font-style: italic'>" + icon + sTpl + colEndTag; // Initial label line
					}
				} else {
					if (colModel[x].hidden === false) {
						if (colModel[x].summaryType) {
							// Summary Type provided: must result in a number value type, align right
							align = "right";
							colStartTag = "<td style='text-align: " + align + "; width: " + colModel[x].width + "px'>";

							// If there is a summary template, use it, or use the default.
							if (colModel[x].summaryTpl) {
								if (colModel[x].summaryTpl.indexOf("{0}") > -1) {
									// User passed standard substitution template, replace zero value with actual column name.
									sMid = sMid + colStartTag + colModel[x].summaryTpl.replace("{0}", "{" + colModel[x].name + "}") + colEndTag;
								} else {
									// Use whatever the user passes.
									sMid = sMid + colStartTag + colModel[x].summaryTpl + colEndTag;
								}
							} else {
								// No template provided, use a default one.
								sMid = sMid + colStartTag + "{" + colModel[x].name + "}" + colEndTag;
							}
						} else {
							colStartTag = "<td style='text-align: " + align + "; width: " + colModel[x].width + "px'>";
							sMid = sMid + colStartTag + "{" + colModel[x].name + "}" + colEndTag;
						}
					}
				}
			}
		}
		return sLeft + sMid + sRight;
	},

	// Get a specific column's title based on field name
	getTitle: function (colname) {
		for (var x = 0; x < this[0].p.colModel.length; x++) {
			if (this[0].p.colModel[x].name === colname) {
				return this[0].p.colNames.length > x ? this[0].p.colNames[x] : this[0].p.colModel[x].name;
			}
		}

		return colname;
	},

	// Call this to load a group
	loadGroups: function (field, label, sortdir) {
		// Set initial group
		// Remove the group drop target
		this.removeDropTarget();

		// Create the group button and put it on the group bar
		this.createGroupButton(field, label, sortdir);

		// Perform grouping
		this.doGroup();

		return true;
	},

	// Move the summary footer. Only done one time during initialization.
	moveSummaryFooter: function () {
		var $divDataBottom = $("#gbox_" + $(this)[0].p.gridName + " .ui-jqgrid-bdiv:not(.frozen-bdiv)");
		var $divSummaryFooter = $("#gbox_" + $(this)[0].p.gridName + " .ui-jqgrid-sdiv");
		if ($divDataBottom && $divSummaryFooter) {
			$divSummaryFooter.appendTo($divDataBottom);
		}
	},

	// Convenience method to remove the group drop target HTML
	removeDropTarget: function () {
		var $this = $("#" + this[0].p.gridName + "_ol");
		var $oneThatExists = $this.find('.placeholderlist');
		if ($oneThatExists) {
			$oneThatExists.remove();
		}
		return true;
	},

	// Remove a group
	removeGroup: function (field) {
		var $oneThatExists = $("#jqgroupedlistcol_" + field);
		if ($oneThatExists.length > 0) {
			// Remove the group
			$oneThatExists.remove();

			// Show the column
			this.jqGrid("showCol", field);

			// Reapply the grouping
			this.doGroup();
		}
		return true;
	},

	// Remove all groups
	removeAllGroups: function () {
		var $list = $("#" + this[0].p.gridName + "_ol");
		var $allThatExist = $("#" + this[0].p.gridName + "_ol li");
		if ($allThatExist.length > 0) {

			// Get list of groups
			var $groupCols = $("#" + this[0].p.gridName + "_ol li:not(.placeholderlist)").map(function () { return $(this).attr("data-column"); }).get();
			this.jqGrid("showCol", $groupCols);

			// Remove the group buttons
			$allThatExist.remove();

			// Add the drop label back
			$('<li class="placeholderlist">' + $(this)[0].p.captionLabel + '</li>').appendTo($list);

			// Ungroup
			this.groupingRemove();

			// TURN BACK ON WHEN THE FROZEN COLUMN FEATURE WORKS WITH EXPAND/COLLAPSE
			// Hide "EMPTY" column.
			//if (this[0].p.frozenColumns === true) {
			//	this.jqGrid("destroyFrozenColumns");
			this.jqGrid("hideCol", "EMPTY");
			//}

			// Remove the expand/collapse links
			this.setupExpandCollapseLinks(false);

			// Turn off the footer.
			this.toggleSummaryFooter(false);
		}
		return true;
	},

	// Create the expand/collapse links
	setupExpandCollapseLinks: function (adding) {
		var $grid = $(this);
		var $topLinksTarget = $("#" + $grid[0].p.gridName + "_jqGridExpandCollapseLinksTopTarget");
		var $botLinksTarget = $("#" + $grid[0].p.gridName + "_jqGridExpandCollapseLinksBotTarget");
		var $topLinks = $("#" + $grid[0].p.gridName + "_jqGridExpandCollapseLinksTop");
		var $botLinks = $("#" + $grid[0].p.gridName + "_jqGridExpandCollapseLinksBot");

		if (adding === true) {

			// Can we expand?
			var expandAllHtml = "";
			if ($grid[0].p.expandCollapseLabelMode === "HIDE") {
				// Links in "appear / disappear" mode.
				if ($grid.canExpandAll() === true) {
					expandAllHtml = $grid[0].p.expandAllHtml + $grid[0].p.expandCollapseSeparatorHtml + $grid[0].p.expandLevelHtml;
				}
			} else {
				// Links in "enable / disable" mode, as a default in case the developer misspells something.
				var p1 = $grid[0].p.expandAllHtml;
				var p2 = $grid[0].p.expandCollapseSeparatorHtml;
				var p3 = $grid[0].p.expandLevelHtml
				// Modify HTML to be disabled.
				if ($grid.canExpandAll() === false) {
					p1 = $(p1).removeClass("jqGridExpandCollapseLinksExpandAll").addClass("jqGridDisabledExpandLinks")[0].outerHTML;
					p2 = $(p2).removeClass("jqGridExpandCollapseLinksMisc").addClass("jqGridDisabledExpandLinks")[0].outerHTML;
					p3 = $(p3).removeClass("jqGridExpandCollapseLinksExpandOne").addClass("jqGridDisabledExpandLinks")[0].outerHTML;
				}
				expandAllHtml = p1 + p2 + p3;
			}

			// Can we collapse?
			var collapseAllHtml = "";
			if ($grid[0].p.expandCollapseLabelMode === "HIDE") {
				// Links in "appear / disappear" mode.
				if ($grid.canCollapseAll() === true) {
					collapseAllHtml = $grid[0].p.collapseAllHtml;
				}
			} else {
				// Links in "enable / disable" mode, as a default in case the developer misspells something.
				collapseAllHtml = $grid[0].p.collapseAllHtml;
				// Modify HTML to be disabled.
				if ($grid.canCollapseAll() === false) {
					collapseAllHtml = $(collapseAllHtml).removeClass("jqGridExpandCollapseLinksCollapseAll").addClass("jqGridDisabledCollapseLink")[0].outerHTML;
				}
			}

			// Extra separator
			if ((expandAllHtml != "") && (collapseAllHtml != "")) {
				expandAllHtml += $grid[0].p.expandCollapseSeparatorHtml;
			}

			var gotLinks = ((expandAllHtml != "") || (collapseAllHtml != ""));

			// Build the links string
			var $links = $("<div id=\"" + $grid[0].p.gridName +
				"_jqGridExpandCollapseLinksTop\" class=\"jqGridExpandCollapseLinksContainer\">" + expandAllHtml + collapseAllHtml + "</div>");

			// Apply the top links
			if (gotLinks === true) {
				if (($topLinks === null) || ($topLinks.length <= 0)) {
					// Add the button to the bar
					$links.appendTo($topLinksTarget);
				} else {
					// Remove first, then add
					$topLinks.remove();
					$links.appendTo($topLinksTarget);
				}
			}

			// Make a copy...Change ID
			$links = $("<div id=\"" + $grid[0].p.gridName +
				"_jqGridExpandCollapseLinksBot\" class=\"jqGridExpandCollapseLinksContainer\">" + expandAllHtml + collapseAllHtml + "</div>");

			// Apply the bottom links
			if (gotLinks === true) {
				if (($botLinks === null) || ($botLinks.length <= 0)) {
					// Add the button to the bar
					$links.appendTo($botLinksTarget);
				} else {
					// Remove first, then add
					$botLinks.remove();
					$links.appendTo($botLinksTarget);
				}
			}
		} else {
			if ($topLinks) {
				$topLinks.remove();
			}
			if ($botLinks) {
				$botLinks.remove();
			}
		}

		return true;
	},

	// CUSTOM. Please see OVERRIDE getCol() in jqGridOverrides.js
	showTotals: function (options) {
		var o = $.extend({
			total: "Totals",
			totalRow: undefined,
			includeRows: []
		}, options || {})
		return this.each(function () {
			if (!this.grid) {
				return;
			}
			var self = this;
			var showTotals = function () {
				var totals = {};
				$.each(self.p.colModel, function (index, data) {
					//if ((index > 0) && (o.totalRow && o.totalRow == data.name)) {
					//	totals[data.name] = o.total;
					if (index === 0) {
						totals[data.name] = $(self)[0].p.summaryFooterLabel;
					} else {
						if (data.summaryType) {
							var val = NaN;
							if (o.includeRows.length) {
								if ($.inArray(data.name, o.includeRows) > -1) {
									val = $(self).jqGrid('getCol', index, false, data.summaryType); // Could pass data.name instead of index, but is faster to pass index.
								}
							} else {
								val = $(self).jqGrid('getCol', index, false, data.summaryType); // Could pass data.name instead of index, but is faster to pass index.
							}
							if (!isNaN(val)) {
								totals[data.name] = val;
							}
						}
					}
				});
				$(self).jqGrid("footerData", "set", totals);
			};
			//Already loaded?
			if (this.rows.length) {
				showTotals();
			}
		});

		return true;
	},

	// Turn on or off the summary footer. Used in doGroup()
	toggleSummaryFooter: function (show) {
		var $divSummaryFooter = $("#gbox_" + $(this)[0].p.gridName + " .ui-jqgrid-sdiv");
		if ($divSummaryFooter) {
			if (show === true) {
				$divSummaryFooter.css("display", "inline");
			} else {
				$divSummaryFooter.css("display", "none");
			}
		}
	}
});

