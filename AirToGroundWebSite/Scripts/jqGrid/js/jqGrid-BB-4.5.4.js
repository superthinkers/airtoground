//******************************************************
// CUSTOM IMPLEMENTATION for jqGrid 4.5.4
//******************************************************
function createGroupGrid(I) {
	$.jgrid.debug(this, 1, I, true);
	var xUrl = I.ggUrl;
	var xDataSourceType = I.ggDataSourceType;

	var $grid = $('#' + I.ggGridName).jqGrid({
		// USER CONFIGURATION
		gridName: I.ggGridName,
		debugMode: I.ggDebugMode,
		debugDepth: I.ggDebugDepth,

		url: "",
		datatype: "local", //I.ggDataSourceType, // "local" make the load delayed.
		xmlReader: { root: I.ggXmlRowDataRootKey, row: I.ggXmlRowDataRowKey, repeatitems: false, id: I.ggPKColName },
		colNames: I.ggColumnTitleArray,
		colModel: I.ggJSONColumnModel,
		pager: I.ggPagerName,
		rowList: I.ggPagerPagerSizeSelectionList,
		sortname: "", // Will set later
		sortorder: "", // Will set later

		// Initial group setup
		caption: I.ggCaption,
		captionLabel: I.ggCaptionLabel,
		grouping: false,
		footerrow: true, // LEAVE TRUE...this <div> is handled manually in code.
		userDataOnFooter: true,
		startGroupsCollapsed: I.ggStartGroupsCollapsed,
		defaultGroupColumn: I.ggDefaultGroupColumn,
		defaultGroupSortOrder: I.ggDefaultGroupSortOrder,
		primaryGroupPositionLabel: I.ggPrimaryGroupPositionLabel, // Positioned in front of initial group link in top group bar (trailing space when renendered is automatically included)
		naryGroupPositionLabel: I.ggNaryGroupPositionLabel, // Positioned in second and beyond group links in top group bar (trailing space when renendered is automatically included)

		// Expand/Collapse labels
		expandAllLabel: I.ggExpandAllLabel,
		expandAllHint: I.ggExpandAllHint,
		expandAllHtml: I.ggExpandAllHtml,
		expandLevelLabel: I.ggExpandLevelLabel,
		expandLevelHint: I.ggExpandLevelHint,
		expandLevelHtml: I.ggExpandLevelHtml,
		collapseAllLabel: I.ggCollapseAllLabel,
		collapseAllHint: I.ggCollapseAllHint,
		collapseAllHtml: I.ggCollapseAllHtml,
		expandCollapseSeparatorHtml: I.ggExpandCollapseSeparatorHtml,
		expandCollapseLabelMode: I.ggExpandCollapseLabelMode,

		// Footer
		summaryFooterLabel: I.ggSummaryFooterLabel,

		// Misc
		width: ((I.ggGridWidth != "auto") && (I.ggGridWidth != "100%")) ? I.ggGridWidth : undefined,
		autowidth: ((I.ggGridWidth === "auto") || (I.ggGridWidth === "100%")),

		// DEFAULTS - NOT USER CONFIGURABLE VIA WRAPPER
		autoencode: true,
		hidegrid: false, // The hide grid button to the right of the grid caption.
		shrinkToFit: false,
		forceFit: false,
		height: "auto",

		// Pager stuff
		pagerpos: "right",
		toppager: true,
		rowNum: I.ggPagerPagerSizeSelectionList.length > 0 ? I.ggPagerPagerSizeSelectionList[0] : 10,
		pgbuttons: true,
		//pgtext: "",
		viewrecords: false, // The "view 1 of x" label

		//scroll: 1,
		scrollrows: true, // Scroll to selected row when setSelection is used.
		gridview: true,
		rownumbers: false,
		treeGrid: false,

		// Load message
		loadtext: "Loading...",
		loadui: "enable",

		// Sorting
		deselectAfterSort: false, // Prevents a "resetSelection" from firing. Only affects local data mode.
		sortable: true, // Column drag-n-drop ordering, conflicts with group header drag-n-drop when true.
		multisort: true,
		loadonce: true, // If true, after data is loaded, further manipulations must be done on the client. Pager functions will be disabled if false.
		viewsortcols: [false], // This is an array - true, false, 'vertical'

		// Custom group sorting
		onSortCol: function (colname, colidx, sortdir) {
			//alert("onSortCol args (col name, col index, sort order): " + colname + " " + colidx + " " + sortdir);
			if (this.p.data.length > 0) {
				$(this).doSort(colname, colidx, sortdir);
			}
		},

		loadComplete: function (id) {
			// Refresh the frozen columns if used.
			//alert("loadComplete: " + this.p.data.length);
			//$(this).jqGrid("setFrozenColumns");
			if (this.p.data.length > 0) {
				//alert("showTotals");
				$(this).showTotals();
				// Reset the links
				$(this).setupExpandCollapseLinks(true);
			}
		},

		gridComplete: function (id) {
			// Calculate the footer totals
			//alert("gridComplete");
			//$(this).showTotals();
		}
	});

	// Move the summary footer up a little.
	$grid.moveSummaryFooter();

	// This will display a column based quick-filter.
	if (I.ggShowColumnQuickFilters === true) {
		$grid.jqGrid("filterToolbar", { stringResult: true, searchOnEnter: true });
	}

	// Here is the navigator and search. Clone to top. Works with "toppager" property in the grid.
	$grid.jqGrid("navGrid", $grid[0].p.pager, { edit: false, add: false, del: false, refresh: false, cloneToTop: true }, 
		{}, {}, {}, { multipleSearch: true, multipleGroup: true, showQuery: true});

	// Here is the column chooser
	$grid.jqGrid("navButtonAdd", $grid[0].p.pager,
		{ 
		caption: $.jgrid.formatter.toLocaleString("Columns", $.jgrid.formatter.COLUMN_CHOOSER), 
		title: $.jgrid.formatter.toLocaleString("Reorder Columns", $.jgrid.formatter.COLUMN_CHOOSER_HINT),
		onClickButton: function () {
			$("#" + $grid[0].p.gridName).jqGrid("columnChooser");
		} 
	});
	
	// Clone the column chooser and put in the top also
	$grid.jqGrid("navButtonAdd", $grid[0].p.gridName + "_toppager",
		{
		caption: $.jgrid.formatter.toLocaleString("Columns", $.jgrid.formatter.COLUMN_CHOOSER),
		title: $.jgrid.formatter.toLocaleString("Reorder Columns", $.jgrid.formatter.COLUMN_CHOOSER_HINT),
		onClickButton: function () {
			$("#" + $grid[0].p.gridName).jqGrid("columnChooser");
		}
	});

	// Let's add a cursor to help the user distinguish where to grab the column for moving the column position
	$("#gview_" + $grid[0].p.gridName + ".ui-jqgrid-view div.ui-state-default div.ui-jqgrid-hbox table.ui-jqgrid-htable thead tr.ui-jqgrid-labels th").each(function (e) {
		if (this.id.indexOf("EMPTY") === -1) {
			this.style.cursor = "ew-resize";
		}
	});

	// Create the draggable column DIV tag for grouping a column
	$("#gview_" + $grid[0].p.gridName + ".ui-jqgrid-view div.ui-state-default div.ui-jqgrid-hbox table.ui-jqgrid-htable thead tr.ui-jqgrid-labels th div").draggable({
		appendTo: "#" + $grid[0].p.gridName + "_ol",
		helper: "clone"
	});

	// When the column is dropped, it will perform the "group" function
	$("#" + $grid[0].p.gridName + "_ol").droppable({
		activeClass: "ui-state-default",
		hoverClass: "ui-state-hover",
		drop: function (event, ui) {
			var $g = $("#" + $(this).jqGrid.gridName);
			$g.loadGroups(ui.draggable.attr("id").replace("jqgh_" + $grid[0].p.gridName + "_", ""), ui.draggable.text(), $g[0].p.defaultGroupSortOrder);
		}
	});

	// Make a landing space for the expand-collapse links to the right of the column chooser.
	// TOP
	var $topPagerLoc = $("#" + $grid[0].p.gridName + "_toppager_left");
	var $links = $("<div id=\"" + $grid[0].p.gridName + "_jqGridExpandCollapseLinksTopTarget\" class=\"jqGridExpandCollapseLinksTarget\"></div>");
	$links.appendTo($topPagerLoc);
	// BOTTOM
	var $botPagerLoc = $($grid[0].p.pager + "_left");
	var $links2 = $("<div id=\"" + $grid[0].p.gridName + "_jqGridExpandCollapseLinksBotTarget\"></div>");
	$links2.appendTo($botPagerLoc);

	// Set the data access info.
	$grid.setGridParam({ url: xUrl });
	$grid.setGridParam({ datatype: xDataSourceType });

	// Load the data
	//
	// THIS TRICKERY allows the data to get loaded into the grid before grouping occurs. In effect 
	// it is a "delay" or "sleep" mechanism with a 5 second exit trap.
	// Load the data, and subsequently the groups.
	x = $grid.trigger("reloadGrid");

	// Set Sort Order
	// DON'T DO HERE - GROUPING WILL DO THIS
	//$grid[0].p.sortname = $grid[0].p.defaultGroupColumn.length > 0 ? $grid[0].p.defaultGroupColumn : $grid[0].p.sortname;
	//$grid[0].p.sortorder = $grid[0].p.defaultGroupSortOrder.length > 0 ? $grid[0].p.defaultGroupSortOrder : $grid[0].p.sortorder;

	// Load the groups.
	var loadCounter = 0;
	var loadCounterMax = 500;
	var myInterval = setInterval(function () { myLoadTimer() }, 10);
	function myLoadTimer() {
		loadCounter++;
		if ((loadCounter === loadCounterMax) || ($grid[0].p.data.length > 0)) {
			clearInterval(myInterval);
			$grid.loadGroups($grid[0].p.defaultGroupColumn, $grid.getTitle($grid[0].p.defaultGroupColumn), $grid[0].p.defaultGroupSortOrder); //"List Description"
		}
	}
	return $grid;
}

