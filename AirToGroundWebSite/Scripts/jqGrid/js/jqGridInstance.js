﻿/*
	**************************************************
    HOW IT WORKS
	**************************************************

		This class based on {SUPER: SYSTEM} JavaScript techniques.
		http://stackoverflow.com/questions/387707/whats-the-best-way-to-define-a-class-in-javascript

		The Instance is created and the grid is "built" using strings describing the column model and various 
		user-configurable properties. These strings are formatted into JSON strings and turned into JSON 
		objects which are applied to the extended jqGrid grid. After applied, the corresponding data from 
		the database can be loaded and the grid will be capable of grouping and aggregating according to 
		the developer's specifications/customizations.

	**************************************************
	FILES
	**************************************************

			This utility is pre-configured to run the demo data as an example. However it will not affect
			utilization during production. The sample, debugging, and test data files are:

				js/index.htm
				js/testdata.xml

			The actual jqGrid instance is created here. If someone wants to create a new instance using
			a new version of jqGrid, they would clone this file and mimic the naming convention, folder
			naming conventions, and then could leverage a new version of jqGrid with our modifications.
		
				js/jqGrid-BB-4.5.4.js

				Corresponding folder:

				js/jquery.jqGrid-4.5.4
				

			This resource loader class allows versions of jqGrid to be redirected to different file and
			folder locations allowing different versions of jqGrid to be used with our modifications.

				js/jqGridResourceLoader.js

			The instance class is used to interact with when doing actual page development, or Infinity
			component implementation. It represents a wrapper class that encapsulates the functionality
			of the grid only exposing what is necessary to use it effectively.

				js/jqGridInstance.js

			All specifically overridden jqGrid methods are found in the override class. You can create
			your own override methods in the same fashion and override these methods.

				js/jqGridOverrides.js

			All specifically NEW and CUSTOM jqGrid methods are found in the override class. You can 
			create your own extension methods in the same fashion OR override these extensions similar
			to the overrides class.

				js/jqGridExtensions.js

			The custom formatter class provides the ability to format any cell using custom HTML. The
			value is provided to the function allowing the developer to go crazy with html.

				js/jqGrid.CustomFormatterFunctions.js
		
			The custom summary type class allows custom aggregations in the group summary band to
			be created. And, like the custom formatter, passes cell value, row index, and all the 
			data in the grid allowing one to do calculate anything they wish with the data and 
			display it any way they wish.

				js/jqGrid.CustomSummaryTypeFunctions.js
	
    **************************************************
	DEVELOPMENT
	**************************************************

		The Basics - Create a Grid in your page with Sample Data
		---------------------------------------------------------	

			1. Create a new GroupGridInstance() in your custom UIModel .htm or .aspx file in a script tag. Pass the following fields at a minimum.
			Note the "ggGridVersion". This represents a partial folder name for the jqGrid source under the css/ and js/ folders and is used
			to give the developer the flexibility to use the customizations with different versions of jqGrid, like new releases, and to make
			their own customizations easily. You must have a default grouped column and width to trigger the horizontal scroll bars. If you
			use "auto" for ggGridWidth, the grid will be as wide as the sum of the column widths.

				<script type="text/javascript">
    			$(function () {
    				// Create the wrapper
    				var $ggI = new GroupGridInstance(
						{
							ggGridVersion: "4.5.4", ggGridName: "myGroupGrid1", ggPagerName: "pagermyGroupGrid1",
							ggDefaultGroupColumn: "LIST_CODE_DESC", ggDefaultGroupSortOrder: "asc",
							ggGridWidth: "1200", ggShowColumnQuickFilters: true
						});

			**For debugging, pass "ggDebugMode: true". You will be able to trace the Javascript classes.

			2. Add the columns you wish to display. Every column, at a mininum, requires a formatter of stringFormatter. This formatter is responsible
			for formatting the data in the group summary row. To see how to create your own, see the jqGridCustomFormatterFunctions.js file
			(if not using base options: min "stringFormatter", actions, checkbox, currency, date, defaultFormat, email, 
			integer, link, number, select, showlink, rowactions). Using the feature you can create any data in any cell with any html.

			The columns may also have custom summary types. This is the rollup group summary row. You may create custom Javascript functions easily
			which are passed the current record value, the current index, and all the records. This allows you to calculate the cell value any way you
			can conceive containing any html. See jqGridCustomSummaryTypeFunctions.js.

			All columns require: colField, colTitle, colWidth, align, isHidden, isHiddenInColumnChooser, isShownInSearch, isColFrozen, isSortable,
			sortType (int, integer, float, number, currency, date, test), summaryTpl (min {0} meaning "itself" or {1} meaning "my value" or "{COLUMN_NAME}"
			meaning a specific column's value for that row, and formatter (min "stringFormatter", actions, checkbox, currency, date, defaultFormat, email, 
			integer, link, number, select, showlink, rowactions)

    				// Add the columns
    				//addColumn: function (
    				//	colField, colTitle, xmlMapKey, colWidth, align, 
    				//	isColTitleVisible, isEditable, isVisible, isHidden, isHiddenInColumnChooser, isShownInSearch, isColFrozen, isSortable, sortType, 
    				//	summaryType, summaryTpl, formatter, decimalSeparator, thousandsSeparator, decimalPlaces, prefix, suffix, defaultValue) {

    				// Notice the custom "stringFormatter" for the formatters. You need to use "stringFormatter" for all "string" formatter types. All fields get formatters, with "stringFormatter" at a minimum.
    				$ggI.addColumn("LIST_CODE_DESC", "List Code Desc", "", 120, "left", true, false, true, false, false, true, false, true, "text", "count", "{0}", "stringFormatter", "", "", 2, "", "", 0);
    				$ggI.addColumn("LIST_CODE", "List Code", "", 150, "left", true, false, true, false, false, true, false, true, "text", "count", "{0}", "stringFormatter", "", "", 2, "", "", 0);
    				$ggI.addColumn("MAIL_CODE", "Source Code", "", 120, "left", true, false, true, false, false, true, false, true, "text", "count", "{0}", "stringFormatter", "", "", 2, "", "", 0);
    				$ggI.addColumn("QTY_MAILED", "Qty Mailed", "", 120, "right", true, false, true, false, false, true, false, true, "integer", "sum", "{0}", "integer", "", "", 0, "", "", 0);
    				$ggI.addColumn("NEW_DONORS", "New Donors", "", 100, "right", true, false, true, false, false, true, false, true, "integer", "sum", "{0}", "integer", "", "", 2, "", "", 0);
    				$ggI.addColumn("COST_PER_DONOR", "Avg Cost / Donor", "", 110, "right", true, false, true, false, false, true, false, true, "currency", "avg", "{0}", "currency", "", "%", 2, "", "", 0);
    				$ggI.addColumn("ACQ_RESPONSE_RATE", "Avg Response Rate", "", 140, "right", true, false, true, false, false, true, false, true, "float", "avg", "{0}", "number", "", "%", 2, "", "", 0);
    				// This one has a custom summaryType for the aggregate weighted average formula.
    				$ggI.addColumn("AVG_ACQ_GIFT_AMT", "Avg Acq Gift Amt", "", 120, "right", true, false, true, false, false, true, false, true, "currency", "customAggregateWeightedAverage", "{0}", "currency", "", "", 2, "", "", 0);
    				$ggI.addColumn("ACQ_NET_PER_PIECE", "Acq Net Per Piece", "", 120, "right", true, false, true, false, false, true, false, true, "currency", "sum", "{0}", "currency", "", "", 2, "", "", 0);
    				$ggI.addColumn("NET_6MO_VAL_PER_PIECE", "Net 6 MO Val Per Piece", "", 150, "right", true, false, true, false, false, true, false, true, "currency", "sum", "{0}", "currency", "", "", 2, "", "", 0);
    				$ggI.addColumn("NET_12MO_VAL_PER_PIECE", "Net 12 MO Val Per Piece", "", 150, "right", true, false, true, false, false, true, false, true, "currency", "sum", "{0}", "currency", "", "", 2, "", "", 0);
    				$ggI.addColumn("NET_18MO_VAL_PER_PIECE", "Net 18 MO Val Per Piece", "", 150, "right", true, false, true, false, false, true, false, true, "currency", "sum", "{0}", "currency", "", "", 2, "", "", 0);
    				$ggI.addColumn("NET_24MO_VAL_PER_PIECE", "Net 24 MO Val Per Piece", "", 150, "right", true, false, true, false, false, true, false, true, "currency", "sum", "{0}", "currency", "", "", 2, "", "", 0);
    				$ggI.addColumn("GROSS_6MO_DONOR_VAL", "Gross 6 MO Donor Val", "", 150, "right", true, false, true, false, false, false, true, true, "currency", "sum", "{0}", "currency", "", "", 2, "", "", 0);
    				$ggI.addColumn("GROSS_12MO_DONOR_VAL", "Gross 12 MO Donor Val", "", 150, "right", true, false, true, false, false, false, true, true, "currency", "sum", "{0}", "currency", "", "", 2, "", "", 0);
    				$ggI.addColumn("GROSS_18MO_DONOR_VAL", "Gross 18 MO Donor Val", "", 150, "right", true, false, true, false, false, false, true, true, "currency", "sum", "{0}", "currency", "", "", 2, "", "", 0);
    				$ggI.addColumn("GROSS_24MO_DONOR_VAL", "Gross 24 MO Donor Val", "", 150, "right", true, false, true, false, false, false, true, true, "currency", "sum", "{0}", "currency", "", "", 2, "", "", 0);

			3. Now that the instance is configured, call "bindGrid" to create the extended jqGrid.

    				// Bind the grid - this paints it and loads the data.
    				$ggI.bindGrid();
    			});
			</script>

			4. Finally, place the jqGrid <table> and pager's <div> tag wherever you want in the body. The pager <div> id must be passed as ggPagerName.

				<body>
					<h1>jqGrid Grouped Summary Grid Tester</h1>
					<table id="myGroupGrid1">
					</table>
					<div id="pagermyGroupGrid1">
					</div>
				</body>

		The Basics - Modify your XXXX and bind to your data
		---------------------------------------------------------	

			1. Create a Stored Procedure that...

			2. Create a UI Model to call the Stored Procedure...

			3. Setup the grid's data URL location, data source type, columns, and then bind. Voila, you have data

	**************************************************
	ADVANCED INSTRUCTIONS
    **************************************************

		Remote-controlling the Grid
		---------------------------------------------------------	
		
		1. Using the group grid instance variable you created, you can access the grid and manipulate it however you
		like. Let's demonstrate some of the general capabilities. Create some buttons in your UI and bind them in your 
		Javascript like this:

		<head>
			<script type="text/javascript">
			
				...
    			// You just called bind...
				$ggI.bindGrid();

				// Add a group
    			$("#Button1").click(function () {
    				$ggI.loadGroups("LIST_CODE", "List Code", "asc");
    			});

				// Remove a group
    			$("#Button2").click(function () {
    				$ggI.removeGroup("LIST_CODE");
    			});

    			// Remove all groups
    			$("#Button3").click(function () {
    				$ggI.removeAllGroups();
    			});

    			// Collapse All
    			$("#Button4").click(function () {
		    		// Mode: "EXPAND_ONE", "EXPAND_ALL" ,"COLLAPSE_ALL"
    				$ggI.expandCollapseGroups("COLLAPSE_ALL");
    			});

    			// Collapse All
    			$("#Button5").click(function () {
    				// Mode: "EXPAND_ONE", "EXPAND_ALL" ,"COLLAPSE_ALL"
    				$ggI.expandCollapseGroups("EXPAND_ALL");
    			});

			</script>
		</head>
		<body>
			<button id="Button1">Add "LIST_CODE" Group</button>
			<button id="Button2">Remove "LIST_CODE" Group</button>
			<button id="Button3">Remove Groups</button>
			<button id="Button4">Collapse All</button>
			<button id="Button5">Expand All</button>
			<!-- GRID -->
			<table id="myGroupGrid1">
			</table>
			<div id="pagermyGroupGrid1">
			</div>
		</body>


		Accessing Data the internal customized jqGrid
		---------------------------------------------------------	

			1. The "$ggI" group grid instance variable you created is a handle to the instance wrapper
			exposing only the methods you need access to for regular development or Infinity component development.
			However, it has a "getGrid()" method that will return the internal jqGrid for accessing the
			full features of the grid, including our customizations.

			This uses the instance variable to remove all the current groupings, including the buttons at the top:

				$ggI.removeAllGroups();

			This uses the actual grid to do the same:

				$ggI.getGrid().removeAllGroups();

			Not all methods from the instance are a 1:1 match with the internal grid. Some were created for 
			convenience to the developer. If you go internal...it is up to you to make it work correctly!!

		Binding to events and running custom functions
		---------------------------------------------------------	
			Javascript .js class:

			1.	$("#" + $grid[0].p.gridName).bind("loadComplete", loadComplete);				
				function loadComplete(){}
		
			HTML-side <script> tag:

			1. Bind to an event name that you designate

				Example:

					var $ggI = GroupGridInstance();
					$ggI.bind("refresh", function() {
						$ggI.doGridRebind();
						alert("I got refreshed");
					});

			2. Trigger the event in your code where you need to

				Example:

					// Triggers the new refresh event
					$ggI.trigger("refresh"); 

	**************************************************
	RESOURCES
	**************************************************

		Main
		http://www.trirand.com/jqgridwiki/doku.php

		Documentation (navigation link…really means “Concepts and Main How-To’s”)
		http://www.trirand.com/jqgridwiki/doku.php?id=wiki:jqgriddocs

		API Docs, DEMO Tester, All-in-one
		http://www.trirand.com/blog/jqgrid/jqgrid.html

		Fundamental Data Retrieval / Binding Concepts
		http://www.trirand.com/jqgridwiki/doku.php?id=wiki:retrieving_data

		***Working with huge datasources – best practices
		http://www.trirand.net/documentation/aspnet/_2si0uaf6c.htm

		Using paged queries, simplified approach.
		http://www.trirand.net/examples/grid/loading_data/million_sql/default.aspx

		REST
		http://stackoverflow.com/questions/7344310/using-jqgrids-inline-editing-with-restful-urls
		http://www.trirand.com/blog/?page_id=393/feature-request/rest-support/    (THESE are the people that support jqGrid, Trirand)
		java…but interesting http://www.javacodegeeks.com/2011/07/jqgrid-rest-ajax-spring-mvc-integration.html
		…THERE ARE PLENTY AVAILABLE: GOOGLE "jqGrid" and the feature you are interested in. 

		WCF
		http://www.codeproject.com/Articles/38298/Working-with-jqGrid-and-a-WCF-Service

		Using Web service 
		http://csharptechies.blogspot.com/2011/05/improving-performance-of-jqgrid-for.html

		MVC architecture approach
		http://www.trirand.net/aspnetmvc/grid/performancelinqsearch

		Linqdatasource. Interesting reading
		http://www.trirand.net/examples/grid/loading_data/million_linqdatasource/default.aspx

		Critical grouping information, changing the SQL “group by”
		http://www.cutterscrossing.com/index.cfm/2012/4/4/Intro-to-jqGrid-Part-7-Grouping

    ******************************************************************************************************************************************************
	MODIFICATION LOG
	******************************************************************************************************************************************************
    01/23/2014 jmp Initial development.



	******************************************************************************************************************************************************
*/
function GroupGridInstance(J) {
	$.jgrid.debugDepth = 3; // DEEP
	$.jgrid.debug(this, 0, "BEGIN", true);
	$.jgrid.debug(this, 1, J, true);

	// Check required fields.
	if ((!J.ggGridName || (J.ggGridName.length === 0)) ||
		(!J.ggGridVersion || (J.ggGridVersion.length === 0)) ||
		(!J.ggUrl || (J.ggUrl.length === 0)) ||
		(!J.ggDataSourceType || (J.ggDataSourceType.length === 0)) ||
		(!J.ggPKColName || (J.ggPKColName.length === 0)) ||
		(!J.ggDefaultGroupColumn || (J.ggDefaultGroupColumn.length === 0)) ||
		(!J.ggDefaultGroupSortOrder || (J.ggDefaultGroupSortOrder.length === 0))
		) {
		// ERROR
		alert("Insufficient arguments: ggGridName, ggGridVersion, ggUrl, ggDataSourceType,\nggPKColName, ggDefaultGroupColumn, ggDefaultGroupSortOrder are required at a minimum.");
		return false;
	}

	var DEFAULT = "DEFAULT_VALUE";
    // Class properties
	var I = {
        // CONFIGURATION PROPERTIES
		// REQUIRED FROM HERE
		// Whether to turn on the internal javascript alerts and messages for debugging the code. 
		// Note, to turn off minified jqGrid base code, change jqGrid-BB-4.5.4.js jqGridLoadResources(false, "4.5.4", "en"); to jqGridLoadResources(true, "4.5.4", "en");
		ggDebugMode: false,
		ggDebugDepth: 0, // How deep. 0 is shallow, 5 is deep.

		ggGridVersion: "4.5.4", // Version and partial folder name for corresponding jqGrid resources
		ggGridName: "" || "myGroupGrid1", // Name of corresponding HTML grid DIV tag in page
		ggPagerName: "" || "pagermyGroupGrid1", // Name of corresponding HTML grid DIV tag in page

		ggUrl: "/js/testdata.xml", 
        ggDataSourceType: "xml", 

		//xmlReader: { root: "ROWDATA", row: "ROW", repeatitems: false, id: "C0" },
        ggXmlRowDataRootKey: "ROWDATA",
        ggXmlRowDataRowKey: "ROW",
        ggPKColName: "C0",

        // JSON Strings, used for creating JSON Objects
        ggColumnTitleArray: [],
        ggJSONColumns: [], // the ggJSONColumnModel is built from this array of JSON columns
        ggJSONColumnModel: Object,

		// NOT REQUIRED FROM HERE
        ggCaption: "", // Will be an ordered-list html element.
        ggCaptionLabel: "Drag and drop column headers here to group data",

		// Pager
		ggPagerPagerSizeSelectionList: [10, 25, 50, 100],

		// Group stuff
		ggStartGroupsCollapsed: false, // Start the groups collapsed
		ggDefaultGroupColumn: "LIST_CODE_DESC",
		ggDefaultGroupSortOrder: "asc",
		ggPrimaryGroupPositionLabel: "Group by", // Positioned in front of initial group link in top group bar (trailing space when renendered is automatically included)
		ggNaryGroupPositionLabel: "then group by", // Positioned in second and beyond group links in top group bar (trailing space when renendered is automatically included)
		ggShowGrandTotals: true,
		ggCustomGrandTotals: true,

		// Expand/Collapse labels
		ggExpandAllLabel: "Expand All",
		ggExpandAllHint: "Expand All Groups",
		ggExpandAllHtml: "",
		ggExpandOneLevelLabel: "Expand One Level",
		ggExpandOneLevelHint: "Expand One Level at a Time",
		ggExpandOneLevelHtml: "",
		ggCollapseAllLabel: "Collapse All",
		ggCollapseAllHint: "Collapse All Groups",
		ggCollapseAllHtml: "",
		ggExpandCollapseSeparatorHtml: "<span class=\"jqGridExpandCollapseLinksMisc\">&nbsp;|&nbsp;</span>", // Must be in an HTML tag, like <div> or <span>
		ggExpandCollapseLabelMode: "HIDE", // Can be "DISABLE" or "HIDE, changing whether they are hidden or disabled when their availability changes.

		// Footer
		ggSummaryFooterLabel: "Totals:",
		
		// Sorting
		ggSortName: "LIST_CODE_DESC", // *** It is critical to set this default value to correspond with the default group value
		ggSortOrder: "asc", // *** It is critical to set this default value to correspond with the default group value

		// Misc
		ggGridWidth: "auto",
		ggShowColumnQuickFilters: false
    };
	$.jgrid.debug(this, 1, I, true);

    // Merge the input arguments with our properties
    for (var key in J) {
        if (J.hasOwnProperty(key)) {
            if (key in I) {
                I[key] = J[key];
            } else {
                alert("The property you are trying to assign: " + key + " does not exist");
            }
        }
    }

	// Is localization needed to be applied? This would be true if grid.locale-de.custom.js were loaded as a resource, for example.
    $.jgrid.debug(this, 0, "Apply Localization", true);
    if ($.jgrid.formatter.applyLocaleSettings) {
		$.jgrid.formatter.applyLocaleSettings(I);
	}

	// Build the LOCALIZED HTML values
	I.ggExpandAllHtml = "<a class=\"jqGridExpandCollapseLinksExpandAll\" href=\"#\" id=\"btnExpandAll\" title=\"" +
		I.ggExpandAllHint + "\" onclick=\"jQuery(&quot;#" + I.ggGridName + "&quot;).expandCollapseGroups('EXPAND_ALL'); return false;\">" +
		I.ggExpandAllLabel + "</a>";
	I.ggExpandLevelHtml = "<a class=\"jqGridExpandCollapseLinksExpandOne\" href=\"#\" id=\"btnExpandLevel\" title=\"" +
		I.ggExpandOneLevelHint + "\" onclick=\"jQuery(&quot;#" + I.ggGridName + "&quot;).expandCollapseGroups('EXPAND_ONE'); return false;\">" +
		I.ggExpandOneLevelLabel + "</a>";
	I.ggCollapseAllHtml = "<a class=\"jqGridExpandCollapseLinksCollapseAll\" href=\"#\" id=\"btnCollapseAll\" title=\"" +
		I.ggCollapseAllHint + "\" onclick=\"jQuery(&quot;#" + I.ggGridName + "&quot;).expandCollapseGroups('COLLAPSE_ALL'); return false;\">" +
		I.ggCollapseAllLabel + "</a>";

	// LOCALIZE other html values
	I.ggCaption = "<ol id=\"" + I.ggGridName + "_ol\" class=\"jqGridGroupDropZone\"><li class=\"placeholderlist\">" + I.ggCaptionLabel + "</li></ol>";

	// Let's create the "GroupGridInstance" and then return it.
	$.jgrid.debug(this, 0, "Build the GroupGridInstance", true);
    var self = {
    	BBjqGrid: null,
    	// Add grid columns and update the grid column model
    	addColumn: function (colField, colTitle, xmlMapKey, colWidth, align,
			isColTitleVisible, isEditable, isVisible, isHidden, isHiddenInColumnChooser, isShownInSearch, isColFrozen, isSortable, sortType,
			summaryType, summaryTpl, formatter, decimalSeparator, thousandsSeparator, decimalPlaces, prefix, suffix, defaultValue) {

    		// The Column Names
    		//colNames: ["", "Rec No", "List Description", "List Code", "Mail Code", "Qty Mailed", "New Donors", "Response Rate", "Avg Gift Amt (CUST AGG)", "Cost Per New Donor",
    		//	"Acq Net Per Piece", "Net 6 MO Value Per Piece", "Net 12 MO Value Per Piece", "Net 18 MO Value Per Piece", "Net 24 MO Value Per Piece",
    		//	"Gross 6 MO Donor Value Per Piece", "Gross 12 MO Donor Value Per Piece", "Gross 18 MO Donor Value Per Piece", "Gross 24 MO Donor Value Per Piece"
    		//],

    		// The "EMPTY" column must always exist.
    		if (I.ggColumnTitleArray.length === 0) {
    			I.ggColumnTitleArray[0] = ""; // No label for empty column
    		}
    		I.ggColumnTitleArray[I.ggColumnTitleArray.length || 0] = colTitle;

    		// The Column Model
    		//{ name: "EMPTY", width: 250, frozen: true, editable: false, hidedlg: true,
    		//	title: false, visible: true, search: false, sortable: false, formatter: stringFormatter
    		//},
    		//{ name: "MAIL_CODE", width: 100, xmlmap: "MAIL_CODE", visible: true, sorttype: "string", formatter: stringFormatter, summaryType: "count" },
    		//{ name: "QTY_MAILED", width: 80, xmlmap: "QTY_MAILED", visible: true, sorttype: "float",
    		//	formatter: 'number', formatoptions: { decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "", defaultValue: 0.00 },
    		//	align: "right", summaryType: 'sum', summaryTpl: '{0}'
    		//},

    		// The "EMPTY" column must always exist.
    		if (I.ggJSONColumns.length === 0) {
    			var sE = '"name":"EMPTY",' +
                     '"xmlmap":"",' +
                     '"align":"left",' +
                     '"width":250,' +
                     '"title":false,' +
                     '"editable":false,' +
                     '"visible":true,' +
                     '"hidedlg":true,' +
                     '"search":false,' +
                     '"frozen":true,' +
                     '"sortable":false,' +
					 '"formatter":"stringFormatter"';
    			I.ggJSONColumns[0] = sE;
    		}

    		// Default the xml key for this column if not provided. Ignored if using JSON datasource.
    		if (xmlMapKey.length === 0) {
    			xmlMapKey = colField;
    		}

    		var s = '"name":"' + colField + '",' +
                     '"width":' + colWidth + ',' +
                     '"xmlmap":"' + xmlMapKey + '",' +
                     '"align":"' + align + '",' +
                     '"title":' + isColTitleVisible + ',' +
                     '"editable":' + isEditable + ',' +
                     '"visible":' + isVisible + ',' +
                     '"hidden":' + isHidden + ',' +
                     '"hidedlg":' + isHiddenInColumnChooser + ',' +
                     '"search":' + isShownInSearch + ',' +
                     '"frozen":' + isColFrozen + ',' +
                     '"sortable":' + isSortable + ',' +
                     '"sorttype":"' + sortType + '",' +
                     '"summaryType":"' + summaryType + '",' +
                     '"summaryTpl":"' + summaryTpl + '",' +
					 '"formatter":"' + formatter + '"';
    				 //'"formatter":"' + formatter + '",' +
    				 //'"formatoptions":{"decimalSeparator":"' + decimalSeparator + '","thousandsSeparator":"' + thousandsSeparator + '","decimalPlaces":' + decimalPlaces + ',"prefix":"' + prefix + '","suffix":"' + suffix + '","defaultValue":"' + defaultValue + '"}';
    		I.ggJSONColumns[I.ggJSONColumns.length || 0] = s;

    		// Now update the JSON objects.
    		self.updateJSONObjects();

    		// Now update the grid models.
    		self.updateGridModels();

    		return true;
    	},

    	// Grid data binding methods
    	// Invoke this to render the grid
    	bindGrid: function () {
    		if (I.ggGridName == "") {
    			alert("You must set the grid name 'ggGridName' before binding the grid");
    			return false;
    		} else {
    			// Define and bind the grid.
    			// **********************************ENHANCE TO USE A SPECIFIC VERSION....
    			$.jgrid.debug(this, 0, "Creating Group Grid", true);
    			self.BBjqGrid = createGroupGrid(I);
    			$.jgrid.debug(this, 0, "Group Grid Created", true);
    			return self.BBjqGrid;
    		}
    	},

    	// Creates group bar buttons and performs grouping
    	createGroupButton: function (field, label) {
    		return this.getGrid().createGroupButton(field, label);
    	},


    	// Refresh the grid by re-reading from the grid datasource
    	doGridRebind: function () {
    		// Custom grid rebind function
    		// Used in conjunction with autoBind = false. Does not
    		// bind data until read() is called. Allows configuration of
    		// grid first, then binding second, and only one time.
    		var $grid = self.getGrid();
    		$grid.trigger("reloadGrid");

    		return true;
    	},

    	// System: Perform the grouping. Use loadGroups() instead. Use for refresh.
    	doGroup: function () {
    		return this.getGrid().doGroup();
    	},

    	// Customized sorting considering the groups
    	doSort: function (colname, colidx, sortdir) {
    		return this.getGrid().doSort(colname, colidx, sortdir);
    	},

    	// NEW method for expanding and collapsing ALL groups. Pass whether to expand all (true) or collapse all (false) groups.
    	// Mode: "EXPAND_ONE", "EXPAND_ALL" ,"COLLAPSE_ALL"
    	expandCollapseGroups: function (mode) {
    		return this.getGrid().expandCollapseGroups(mode);
    	},

    	// Fixup the leading group prefix label. *********Make configurable.....
    	fixGroupByPrefix: function () {
    		return this.getGrid().fixGroupByPrefix();
    	},

    	// Column setup
    	getColumnTitleArray: function () {
    		return I.ggColumnTitleArray;
    	},

    	// Return the jq grid object using caching, or a little intelligence.
    	getGrid: function () {
    		if ((!isNaN(self.BBjqGrid)) || (self.BBjqGrid === null)) {
    			// Not cached yet, try retrieve via name
    			var $grid = $("#" + I.ggGridName).jqGrid;
    			// Cache it.
    			self.BBjqGrid = $grid;
    			return $grid;
    		} else {
    			// Return cached instance
    			return self.BBjqGrid;
    		}
    	},

    	// Creates the layout for the group summary band
    	getGroupRow: function (groupIdx, rowTag, icon) {
    		return this.getGrid().getGroupRow(groupIdx, rowTag, icon);
    	},

    	// Return the computed JSON column model for the grid.
    	getJSONColumnModel: function () {
    		return I.ggJSONColumnModel;
    	},

    	// PK developer supplied
    	getPKColName: function () {
    		return I.ggPKColName
    	},

    	// Get a specific column's title based on field name
    	getTitle: function (colname) {
    		this.getGrid().getTitle(colname);
    	},

    	// XML INFO - the root of the record set
    	getXmlRowDataRootKey: function () {
    		return I.ggXmlRowDataRootKey;
    	},

    	// XML INFO - the row key
    	getXmlRowDataRowKey: function () {
    		return I.ggXmlRowDataRowKey
    	},

    	// Call this to load a group
    	loadGroups: function (field, label, sortdir) {
    		return this.getGrid().loadGroups(field, label, sortdir);
    	},

    	// Convenience method to remove the group drop target HTML <li> tag
    	removeDropTarget: function () {
    		return this.getGrid().removeDropTarget();
    	},

    	// Remove a group.
    	removeAllGroups: function (field) {
    		return this.getGrid().removeAllGroups();
    	},

    	// Remove a group.
    	removeGroup: function (field) {
    		return this.getGrid().removeGroup(field);
    	},

    	// Create the expand/collapse links
    	setupExpandCollapseLinks: function (adding) {
    		return this.getGrid().setupExpandCollapseLinks(adding);
    	},

    	// For calculating the footer totals.
    	showTotals: function (options) {
    		return this.getGrid().showTotals(options);
    	},

    	// System invoked method to update the grid models. If the developer
    	// manipulates the models manually this will have to be called.
    	updateGridModels: function () {
    		var $grid = self.getGrid();
    		if ($grid) {
    			$grid.colNames = I.ggJSONColumnTitleArray;
    			$grid.colModel = I.ggJSONColumnModel;
    		}
    		return true;
    	},

    	// System invoked method to update the JSON objects. If the developer
    	// manipulates the columns manually this will have to be called.
    	updateJSONObjects: function () {
    		// Column Model
    		var s = "";
    		// Build the JSON string
    		for (var lcv = 0; lcv < I.ggJSONColumns.length; lcv++) {
    			if (s == "") {
    				s = "{" + I.ggJSONColumns[lcv] + "}";
    			} else {
    				s = s + ",{" + I.ggJSONColumns[lcv] + "}";
    			}
    		}
    		// Complete the JSON string
    		s = "[" + s + "]";
    		//alert(s);
    		// Parse the JSON string into a JSON object
    		I.ggJSONColumnModel = $.parseJSON(s);
    		return true;
    	}
    };
    $.jgrid.debug(this, 0, "Group Grid Instance Created", true);
    $.jgrid.debug(this, 0, "END", true);
    return self;
}


