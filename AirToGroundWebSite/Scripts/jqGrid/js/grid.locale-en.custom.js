/**

	Blackbaud Custom GERMAN Localization

	Usage: 

	$.jgrid.formatter.toLocaleString("Sort by This Group", $.jgrid.formatter.GROUPS_HINT_SORT_GROUP)

**/
(function ($) {
	$.jgrid = $.jgrid || {};
	$.extend($.jgrid.formatter, {
		// SYSTEM
		CUSTOM_FORMATTER_REGISTERED: true,

		// Lookup Types
		COLUMN_CHOOSER: 0, // Found on the Column Chooser button label
		COLUMN_CHOOSER_HINT: 1, // Found on the "Columns" column chooser button hint
		GROUPS_HINT_REMOVE_GROUP: 2, // Found on the "X" of the group button hint
		GROUPS_HINT_SORT_GROUP: 3, // Found on the label text of the group button

		toLocaleString: function (val, lookupType) {
			var strings = 
				{
					0: "Columns", // COLUMN_CHOOSER
					1: "Column Chooser", // COLUMN_CHOOSER_HINT
					2: "Remove This Group", // GROUPS_HINT_REMOVE_GROUP
					3: "Sort by This Group" // GROUPS_HINT_SORT_GROUP
				};

			if (!isNaN(lookupType)) {
				if (strings[lookupType]) {
					return strings[lookupType];
				} else {
					return val;
				}
			} else {
				return val;
			}
		},
		applyLocaleSettings: function (I) {
			var J = {
//				// Group stuff
//				ggPrimaryGroupPositionLabel: "Group by", // Positioned in front of initial group link in top group bar (trailing space when renendered is automatically included)
//				ggNaryGroupPositionLabel: "then group by", // Positioned in second and beyond group links in top group bar (trailing space when renendered is automatically included)

//				// Expand/Collapse labels
//				ggExpandAllLabel: "Expand All",
//				ggExpandAllHint: "Expand All Groups",
//				ggExpandOneLevelLabel: "Expand One Level",
//				ggExpandOneLevelHint: "Expand One Group Level at a Time",
//				ggCollapseAllLabel: "Collapse All",
//				ggCollapseAllHint: "Collapse All Groups",
//				ggCaptionLabel: "Drag and drop column headers here to group data",
//				ggSummaryFooterLabel: "Summary Totals:"
			}

			// Fix the caption html.
			I.ggCaption = "<ol id=\"" + I.ggGridName + "_ol\" class=\"groupDropZone\"><li class=\"placeholderlist\">" + I.ggCaptionLabel + "</li></ol>";

			// Write the properties back to "I". The grid will now use them.
			for (var key in J) {
				if (J.hasOwnProperty(key)) {
					if (key in I) {
						I[key] = J[key];
					} else {
						alert("Die Eigenschaft, die Sie versuchen, zuzuweisen ist nicht vorhanden: " + key);
					}
				}
			}
		}
		////formatter : {
		//		integer : {thousandsSeparator: ".", defaultValue: '0'},
		//		number : {decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 2, defaultValue: '0,00'},
		//		currency : {decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 2, prefix: "", suffix:" €", defaultValue: '0,00'},
		//		date : {
		//			dayNames:   [
		//				"So", "Mo", "Di", "Mi", "Do", "Fr", "Sa",
		//				"Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"
		//			],
		//			monthNames: [
		//				"Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez",
		//				"Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"
		//			],
		//			AmPm : ["","","",""],
		//			S: function () {return '.';}, // one can also use 'er' instead of '.' but one have to use additional word like 'der' or 'den' before
		//			srcformat: 'Y-m-d',
		//			newformat: 'd.m.Y',
		//			parseRe : /[Tt\\\/:_;.,\t\s-]/,
		//			masks : {
		//				// see http://php.net/manual/en/function.date.php for PHP format used in jqGrid
		//				// and see http://docs.jquery.com/UI/Datepicker/formatDate
		//				// and https://github.com/jquery/globalize#dates for alternative formats used frequently
		//				ISO8601Long: "Y-m-d H:i:s",
		//				ISO8601Short: "Y-m-d",
		//				// short date:
		//				//    d - Day of the month, 2 digits with leading zeros
		//				//    m - Numeric representation of a month, with leading zeros
		//				//    Y - A full numeric representation of a year, 4 digits
		//				ShortDate: "d.m.Y",	// in jQuery UI Datepicker: "dd.MM.yyyy"
		//				// long date:
		//				//    l - A full textual representation of the day of the week
		//				//    j - Day of the month without leading zeros
		//				//    F - A full textual representation of a month
		//				//    Y - A full numeric representation of a year, 4 digits
		//				LongDate: "l, j. F Y", // in jQuery UI Datepicker: "dddd, d. MMMM yyyy"
		//				// long date with long time:
		//				//    l - A full textual representation of the day of the week
		//				//    j - Day of the month without leading zeros
		//				//    F - A full textual representation of a month
		//				//    Y - A full numeric representation of a year, 4 digits
		//				//    H - 24-hour format of an hour with leading zeros
		//				//    i - Minutes with leading zeros
		//				//    s - Seconds, with leading zeros
		//				FullDateTime: "l, j. F Y H:i:s", // in jQuery UI Datepicker: "dddd, d. MMMM yyyy HH:mm:ss"
		//				// month day:
		//				//    d - Day of the month, 2 digits with leading zeros
		//				//    F - A full textual representation of a month
		//				MonthDay: "d F", // in jQuery UI Datepicker: "dd MMMM"
		//				// short time (without seconds)
		//				//    H - 24-hour format of an hour with leading zeros
		//				//    i - Minutes with leading zeros
		//				ShortTime: "H:i", // in jQuery UI Datepicker: "HH:mm"
		//				// long time (with seconds)
		//				//    H - 24-hour format of an hour with leading zeros
		//				//    i - Minutes with leading zeros
		//				//    s - Seconds, with leading zeros
		//				LongTime: "H:i:s", // in jQuery UI Datepicker: "HH:mm:ss"
		//				SortableDateTime: "Y-m-d\\TH:i:s",
		//				UniversalSortableDateTime: "Y-m-d H:i:sO",
		//				// month with year
		//				//    F - A full textual representation of a month
		//				//    Y - A full numeric representation of a year, 4 digits
		//				YearMonth: "F Y" // in jQuery UI Datepicker: "MMMM yyyy"
		//			},
		//			reformatAfterEdit : false
		//		},
		//		baseLinkUrl: '',
		//		showAction: '',
		//		target: '',
		//		checkbox : {disabled:true},
		//		idName : 'id'
		////}
	});
})(jQuery);
