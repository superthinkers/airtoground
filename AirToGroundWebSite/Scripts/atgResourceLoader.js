/*
	atgResourceLoader.atgLoadResources

	isProd: can be "true" or "false"
	version: can be 4.5.4
	lang: can be "en" (english), "de" (german), 
	
	DEBUG - located in jqGrid-BB-4.5.4.js (or eq. version)
	jqGridLoadResources(false, "4.5.4");
*/
$.atg = $.atg || {};
$.extend($.atg, {
	atgLoadResources: function (isProd, version, lang) {
	    var pathtojsfiles = "Scripts/";
		var pathtocssfiles = "";

		// set include to false if you do not want some scriptModules to be included
		var linkModules = [];

		// jqGrid CSS Resources
		linkModules = [
		{ include: true, incfile: "App_Themes/Corp/StyleSheet.css" },
		{ include: true, incfile: "Scripts/jqGrid/css/jquery-ui-1.10.3.custom/smoothness/jquery-ui-1.10.3.custom.min.css" },
		{ include: true, incfile: "Scripts/jqGrid/css/jquery.jqGrid-" + version + "/ui.jqgrid.css" },
		{ include: true, incfile: "Scripts/jqGrid/css/jquery.jqGrid-" + version + "/jqGrid-BB-4.5.4.css" }
		];

		// jqGrid SCRIPT Resources
		var scriptModules = [];
		if (isProd === true) {
			scriptModules = [
			// jQuery Resources
			{ include: true, incfile: "Scripts/jquery-2.1.0.min.js" }, // jqGrid translation
			// *************** Telerik Resources ***************
			{ include: true, incfile: "Scripts/Telerik/Common/Core.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Grid/RadGridScripts.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Common/jQuery.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Ajax/Ajax.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Calendar/RadDatePicker.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Calendar/RadTmeViewScripts.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Calendar/RadDateTimePickerScript.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Input/TextBox/RadInputScript.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Input/DateInput/RadDateInputScript.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Common/jQueryPlugins.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Button/RadButton.js" }, // jqGrid translation
            // *************** jqGrid Resources ***************
			// jQuery Resources
			{ include: true, incfile: "Scripts/jqGrid/js/jquery.jqGrid-" + version + "/jquery-ui-1.10.3.custom.min.js" }, // jqGrid translation
			// Localization
			{ include: true, incfile: "Scripts/jqGrid/js/jquery.jqGrid-" + version + "/js/i18n/grid.locale-" + lang + ".js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/jqGrid/js/grid.locale-" + lang + ".custom.js" }, // BB Extensions/Customizations
			// jQuery PLUGINS Load First
			{ include: true, incfile: "Scripts/jqGrid/js/jquery.jqGrid-" + version + "/plugins/jquery.searchFilter.js" }, // Search feature
			{ include: false, incfile: "Scripts/jqGrid/js/jquery.jqGrid-" + version + "/plugins/jquery.contextmenu.js" },
			{ include: false, incfile: "Scripts/jqGrid/js/jquery.jqGrid-" + version + "/plugins/jquery.tablednd.js" },
			{ include: false, incfile: "Scripts/jqGrid/js/jquery.jqGrid-" + version + "/plugins/ui.multiselect.js" }, // LEAVE OFF or the column selector gets messed up
			// PRODUCTION CODE
			{ include: true, incfile: "Scripts/jqGrid/js/jquery.jqGrid-" + version + "/js/minified/jquery.jqGrid.min.js" }, // PRODUCTION jqGrid CODE, MINIFIED
			// PLUGINS
			{ include: false, incfile: "Scripts/jqGrid/js/jquery.jqGrid-" + version + "/plugins/grid.addons.js" },
			{ include: false, incfile: "Scripts/jqGrid/js/jquery.jqGrid-" + version + "/plugins/grid.postext.js" },
			{ include: false, incfile: "Scripts/jqGrid/js/jquery.jqGrid-" + version + "/plugins/grid.setcolumns.js" },
			// OUR CUSTOMIZATIONS
			{ include: true, incfile: "Scripts/jqGrid/js/jqGridOverrides.js" }, // CUSTOM OVERRIDES to jqGrid
			{ include: true, incfile: "Scripts/jqGrid/js/jqGridExtensions.js" }, // CUSTOM EXTENSIONS to jqGrid
			{ include: true, incfile: "Scripts/jqGrid/js/jqGridCustomSummaryTypeFunctions.js" }, // CUSTOM SummaryType FUNCTIONS
			{ include: true, incfile: "Scripts/jqGrid/js/jqGridCustomFormatterFunctions.js" }, // CUSTOM SUMMARY TYPE FUNCTIONS
			// THE MAIN INSTANCE CLASSES
			{ include: false, incfile: "Scripts/jqGrid/js/jqGridResourceLoader.js" },
			{ include: true, incfile: "Scripts/jqGrid/js/jqGrid-BB-" + version + ".js" },
			{ include: true, incfile: "Scripts/jqGrid/js/jqGridInstance.js" }
			];
		} else {
			scriptModules = [
			// jQuery Resources
			{ include: true, incfile: "Scripts/jquery-2.1.0.min.js" }, // jqGrid translation
			// *************** Telerik Resources ***************
			{ include: true, incfile: "Scripts/Telerik/Common/Core.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Grid/RadGridScripts.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Common/jQuery.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Ajax/Ajax.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Calendar/RadDatePicker.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Calendar/RadTmeViewScripts.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Calendar/RadDateTimePickerScript.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Input/TextBox/RadInputScript.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Input/DateInput/RadDateInputScript.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Common/jQueryPlugins.js" }, // jqGrid translation
			{ include: true, incfile: "Scripts/Telerik/Button/RadButton.js" }, // jqGrid translation
            // *************** jqGrid Resources ***************
			// jQuery Resources
			{ include: true, incfile: "Scripts/jqGrid/js/jquery.jqGrid-" + version + "/jquery-ui-1.10.3.custom.min.js" }, // jqGrid translation
			// Localization
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/i18n/grid.locale-" + lang + ".js" }, // jqGrid translation
			{include: true, incfile: "grid.locale-" + lang + ".custom.js" }, // BB Extensions/Customizations
			// jQuery PLUGINS Load First
			{include: true, incfile: "jquery.jqGrid-" + version + "/plugins/jquery.searchFilter.js" }, // Search feature
			{include: false, incfile: "jquery.jqGrid-" + version + "/plugins/jquery.contextmenu.js" },
			{include: false, incfile: "jquery.jqGrid-" + version + "/plugins/jquery.tablednd.js" },
			{include: false, incfile: "jquery.jqGrid-" + version + "/plugins/ui.multiselect.js" }, // LEAVE OFF or the column selector gets messed up
			// DEBUGGING BASE CODE
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.base.js" }, // jqGrid base
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.common.js" }, // jqGrid common for editing
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.formedit.js" }, // jqGrid Form editing
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.inlinedit.js" }, // jqGrid inline editing
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.celledit.js" }, // jqGrid cell editing
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.subgrid.js" }, //jqGrid subgrid
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.treegrid.js" }, //jqGrid treegrid
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.grouping.js" }, //jqGrid grouping
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.custom.js" }, //jqGrid custom 
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.tbltogrid.js" }, //jqGrid table to grid 
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.import.js" }, //jqGrid import
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/jquery.fmatter.js" }, //jqGrid formater
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/JsonXml.js" }, //xmljson utils
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.jqueryui.js" }, //jQuery UI utils
			{include: true, incfile: "jquery.jqGrid-" + version + "/js/grid.filter.js" }, // filter Plugin
			// PLUGINS
			{include: false, incfile: "jquery.jqGrid-" + version + "/plugins/grid.addons.js" },
			{include: false, incfile: "jquery.jqGrid-" + version + "/plugins/grid.postext.js" },
			{include: false, incfile: "jquery.jqGrid-" + version + "/plugins/grid.setcolumns.js" },
			// OUR CUSTOMIZATIONS
			{include: true, incfile: "jqGridOverrides.js" }, // CUSTOM OVERRIDES to jqGrid
			{include: true, incfile: "jqGridExtensions.js" }, //, // CUSTOM EXTENSIONS to jqGrid
			{include: true, incfile: "jqGridCustomSummaryTypeFunctions.js" }, // CUSTOM SummaryType FUNCTIONS
			{include: true, incfile: "jqGridCustomFormatterFunctions.js" }, // CUSTOM SUMMARY TYPE FUNCTIONS
			// THE MAIN INSTANCE CLASSES
			{include: false, incfile: "jqGridResourceLoader.js" },
			{include: true, incfile: "jqGrid-BB-" + version + ".js" },
			{include: true, incfile: "jqGridInstance.js" }
			];
		}

		var filename;
		// Add the LINKS
		for (var i = 0; i < linkModules.length; i++) {
			if (linkModules[i].include === true) {
				filename = pathtocssfiles + linkModules[i].incfile;
				//alert(filename);
				$.atg.atgWriteLinkResourceToHeader(filename);
			}
		}

		// Add the SCRIPTS
		for (i = 0; i < scriptModules.length; i++) {
			if (scriptModules[i].include === true) {
				filename = pathtojsfiles + scriptModules[i].incfile;
				//alert(filename);
				$.atg.atgWriteScriptResourceToHeader(filename);
			}
		}
	}, //

	atgWriteLinkResourceToHeader: function (filename) {
		if (jQuery.browser) {
			if (jQuery.browser.safari) {
				jQuery.ajax({ url: filename, dataType: "link", async: false, cache: false });
			} else {
				if (jQuery.browser.msie) {
					window.document.write("<link charset=\"utf-8\" type=\"text/css\" href=\"" + filename + "\"></link>");
				} else {
					var oHead = window.document.getElementsByTagName("head")[0];
					var oScript = window.document.createElement("link");
					oScript.setAttribute("type", "text/css");
					oScript.setAttribute("href", filename);
					oScript.setAttribute("charset", "utf-8");
					oHead.appendChild(oScript);
				}
			}
		} else {
			window.document.write("<link charset=\"utf-8\" type=\"text/css\" href=\"" + filename + "\"></link>");
		}
	}, //

	atgWriteScriptResourceToHeader: function (filename) {
		if (jQuery.browser) {
			if (jQuery.browser.safari) {
				jQuery.ajax({ url: filename, dataType: "script", async: false, cache: false });
			} else {
				if (jQuery.browser.msie) {
					window.document.write("<script charset=\"utf-8\" type=\"text/javascript\" src=\"" + filename + "\"></script>");
				} else {
					var oHead = window.document.getElementsByTagName("head")[0];
					var oScript = window.document.createElement("script");
					oScript.setAttribute("type", "text/javascript");
					oScript.setAttribute("language", "javascript");
					oScript.setAttribute("src", filename);
					oScript.setAttribute("charset", "utf-8");
					oHead.appendChild(oScript);
				}
			}
		} else {
			window.document.write("<script charset=\"utf-8\" type=\"text/javascript\" src=\"" + filename + "\"></script>");
		}
	} //
});
// LOAD ALL REQUIRED jqGrid resources
// arg1: "false" to debug, "true" for production minified versions
// arg2: the version (the part of the jqGrid folder you consistently named, not the whole folder name)
// arg3: the culture code/lang
$.atg.atgLoadResources(false, "4.5.4", "en");
