Type.registerNamespace("Telerik.Web.UI");
(function(){var a=Telerik.Web.UI,b;
a.PivotGridAggregateLabel=function(c){a.PivotGridAggregateLabel.initializeBase(this,[c]);
this._owner=null;
this._position=null;
this._level=null;
};
a.PivotGridAggregateLabel.prototype={initialize:function(){a.PivotGridAggregateLabel.callBaseMethod(this,"initialize");
},dispose:function(){a.PivotGridAggregateLabel.callBaseMethod(this,"dispose");
},get_owner:function(){return this._owner;
},set__owner:function(c){this._owner=c;
},get_position:function(){return this._position;
},get_level:function(){var f=this;
var c=$telerik.getChildrenByClassName(f._element.parentNode,"rpgFieldItem");
var e=0;
for(var d=0;
d<c.length;
d++){if(c[d].id==f._element.id){return e;
}else{if(c[d].style.display!="none"){e++;
}}}return f._level;
},change:function(d,c){this._owner.fireCommand("AggregateChange",d+";"+c);
},changePosition:function(c){this.change(c,this.get_level());
},changeLevel:function(c){this.change(this.get_position(),c);
},_getPositionAsZoneType:function(){var c=this.get_position();
if(c==a.PivotGridAxis.Rows){return a.PivotGridFieldZoneType.Row;
}else{if(c==a.PivotGridAxis.Columns){return a.PivotGridFieldZoneType.Column;
}}return -1;
},_zoneTypeToPivotAxis:function(c){if(a.PivotGridFieldZoneType.Row==c){return a.PivotGridAxis.Rows;
}else{if(a.PivotGridFieldZoneType.Column==c){return a.PivotGridAxis.Columns;
}}return -1;
}};
a.PivotGridAggregateLabel.registerClass("Telerik.Web.UI.PivotGridAggregateLabel",Sys.UI.Control);
a.PivotGridAxis=function(){};
a.PivotGridAxis.prototype={Rows:0,Columns:1};
a.PivotGridAxis.registerEnum("Telerik.Web.UI.PivotGridAxis",false);
})();
