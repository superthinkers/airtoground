Type.registerNamespace("Telerik.Web.UI.Scheduler");
(function(a,b){var e,c,d;
b.Scheduler.ResizeHelper=function(f){this._scheduler=f;
};
b.Scheduler.ResizeHelper.prototype={initialize:function(){e=this._scheduler;
c=e._activeModel;
d=(e.get_selectedView()==b.SchedulerViewType.MonthView);
},dispose:function(){},_updateResizingAppointmentElements:function(i,h,j){var g=i.resizeFromStart,f=(g==h<0);
if(f){this._addResizingElementPart(j,i);
g=!g;
}else{this._removeResizingElementPart(i);
}this._updateResizingAppointmentPartIndicators(i.resizingElement,g,f);
},_addResizingElementPart:function(s,o){var n=o.resizeFromStart,g=o.resizingElement,r=e._getParentTable(s),h=a(g).clone()[0],f,l;
this._updateResizingAppointmentPartIndicators(g,n,true);
if(d){var i=0;
if(n){i=s.parentNode.cells.length-1;
}l=r.rows[s.parentNode.rowIndex].cells[i];
}else{var q=c.get_slotsPerDay(),m=a(g).parents("td").get(0),j=c._getFirstModelRowIndex(m),p=j*q,k=s.offsetHeight-4;
if(c._isVertical&&e.get_showAllDayRow()){p+=j+1;
}if(n){p+=q-1;
}l=r.rows[p].cells[s.cellIndex];
}f=e._getCellWrap(l,g);
f.append(h);
if(d){h.style.left="";
h.style.width=l.offsetWidth+"px";
}else{e._setAppointmentElementPosition(h,k,0);
}this._updateElementParts(o,h);
},_updateElementParts:function(m,g){var h=m.resizingAppointment._domElements,l=m.resizeFromStart,f=this._getAppointmentAllDayElement(h),k=0,j,i;
if(l){if(!d&&f){k=1;
}Array.insert(h,k,g);
m.resizingAppointment._domElement=g;
}else{Array.add(h,g);
}if(d){j=l?1:h.length-2,i=h[j];
this._expandElementHorizontally(i,l);
}else{if(f){this._expandAllDayElement(f,m);
}else{if(e.get_showAllDayRow()&&h.length==3){this._createAllDayElement(f,m);
}else{j=l?1:h.length-2,i=h[j];
this._expandElementVertically(i,l);
}}}m.resizingElement=g;
},_expandAllDayElement:function(f,i){var g=f.parents("td").first(),h,j;
j=g.width()+f.width()+1;
if(i.resizeFromStart){h=e._getCellWrap(g.prev().get(0),f.get(0));
h.append(f.get(0));
}a(i.resizingElement).remove();
Array.remove(i.resizingAppointment._domElements,i.resizingElement);
f.width(j);
},_createAllDayElement:function(f,m){var i=m.resizingAppointment._domElements;
f=a(i[1]);
var h=f.get(0),l=f.parents("td").get(0),k=c._getFirstModelRowIndex(l),o,n,j,g,p;
if(c._isVertical){o=k*c.get_slotsPerDay();
if(e.get_showAllDayRow()){o+=k;
}n=a(l).parents("table").get(0).rows[o];
}else{n=a(e.get_element()).find(".rsTopWrap .rsAllDayTable").get(0).rows[0];
}j=n.cells[l.cellIndex];
p=a(j).width();
g=e._getCellWrap(j,h);
g.append(h);
e._setAppointmentElementPosition(h,g[0].offsetHeight-4,0);
f.find(".rsArrowTop").removeClass("rsArrowTop").addClass("rsArrowLeft");
f.find(".rsArrowBottom").removeClass("rsArrowBottom").addClass("rsArrowRight");
Array.remove(i,h);
Array.insert(i,0,h);
f.width(p);
},_getAppointmentAllDayElement:function(g){if(e.get_showAllDayRow()){for(var h=0,j=g.length;
h<j;
h++){var f=a(g[h]);
if(f.parents("table.rsAllDayTable, .rsAllDayRow").filter(":first").length>0){return f;
}}}return null;
},_expandElementVertically:function(f,k){var i=f.parentNode.parentNode,j=i.parentNode.rowIndex,n=c.get_slotsPerDay(),h=f.parentNode.parentNode.offsetHeight,g=c._getFirstModelRowIndex(i),m=g*n,l,o,p;
if(c._isVertical&&e.get_showAllDayRow()){m+=g+1;
}l=k?j-m:n-(j-m);
o=h*(l);
if(k){p=o*-1;
o+=parseFloat(f.style.top,10);
o+=a(f).height();
if(o<0){return;
}}else{o-=4;
}e._setAppointmentElementPosition(f,o,p);
},_expandElementHorizontally:function(f,j){var i=f.parentNode.parentNode,g=i.parentNode.cells.length,h,k;
if(j){k=f.offsetWidth;
k+=parseFloat(f.style.left);
k+=i.cellIndex*i.offsetWidth;
f.style.left=(-1*i.cellIndex*i.offsetWidth)+"px";
}else{h=g-i.cellIndex;
k=i.offsetWidth*h;
}f.style.width=k+"px";
},_removeResizingElementPart:function(p){var o=p.resizingElement,n=p.resizeFromStart,j=a(o.parentNode),h=j.parents("td").first(),m=p.resizingAppointment._domElements,g=this._getAppointmentAllDayElement(m),l;
if(m.length<=1){return;
}if(!d&&g){var f=g.parents("td").first(),i=n?h.next():h.prev(),k=e._getCellWrap(i.get(0),o);
k.append(o);
if(n){f=f.next();
k=e._getCellWrap(f.get(0),g.get(0));
k.append(g.get(0));
}var q=g.width()-f.width()-1;
if(q<5){g.remove();
Array.remove(m,g.get(0));
}else{g.width(q);
}}else{a(o).remove();
Array.remove(m,o);
l=n?0:m.length-1;
p.resizingElement=m[l];
}if(!d&&j.children(".rsApt").length==0){j.remove();
}},_updateResizingAppointmentPartIndicators:function(h,k,g){var f=a(h),i=k?(d?"Left":"Top"):(d?"Right":"Bottom"),l=k?"Start":"End";
if(g){var j='<a style="z-index: 80;" href="#" class="rsArrow'+i+'">'+i.toLocaleLowerCase()+"</a>";
f.find("div.rsAptResize"+l+", .rsArrow"+i).remove();
f.find("div.rsAptIn").append(j);
}else{var m="<div class='rsAptResize rsAptResize"+l+"'></div>";
f.find(".rsArrow"+i).remove();
f.append(m);
}},_getAppointmentOriginalElements:function(n){var h=[],f=n.get_elements();
for(var j=0,k=f.length;
j<k;
j++){var g=f[j],m=g.parentNode.cloneNode(false),l=g.cloneNode(true);
m.appendChild(l);
h[h.length]={appointmentElement:l,parentCell:a(g).parents("td").get(0)};
}return h;
},_restoreAppointmentOriginalElements:function(n){var m=n.resizingAppointment,l=n.originalSize.elements;
for(var j=0,k=m._domElements.length;
j<k;
j++){a(m._domElements[j]).remove();
}m._domElements=[];
m._domElement=null;
for(var j=0,k=l.length;
j<k;
j++){var h=l[j],g=h.appointmentElement,f=e._getCellWrap(h.parentCell,g);
f.append(g);
m._domElements[j]=g;
}}};
})($telerik.$,Telerik.Web.UI);
