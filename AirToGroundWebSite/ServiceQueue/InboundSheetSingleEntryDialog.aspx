﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InboundSheetSingleEntryDialog.aspx.cs"
    AsyncTimeout="5000" Inherits="ServiceQueue_InboundSheetSingleEntryDialog" Buffer="true"
    Strict="true" Explicit="true" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server" AsyncPostBackTimeout="600"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin" ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="true" 
            RequestQueueSize="15" UpdatePanelsRenderMode="Inline" >
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnAdd">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="lblCount" />
                        <telerik:AjaxUpdatedControl ControlID="btnClose" />
                        <telerik:AjaxUpdatedControl ControlID="btnCancel" />
                        <telerik:AjaxUpdatedControl ControlID="reqTailFound" />
                        <telerik:AjaxUpdatedControl ControlID="reqServices" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function doComplete() {
                // Get a reference to the current RadWindow
                var oWnd = GetRadWindow();

                // Let's the calling form know the user is committing the changes, versus cancelling.
                var oArg = new Object();
                oArg.OK = "TRUE";

                // Quick validation and then close.
                if (true == true) {
                    oWnd.close(oArg);
                } else {
                    alert("All Values are Required");
                }
            }
            function doCancel() {
                // Get a reference to the current RadWindow
                var oWnd = GetRadWindow();

                // Let's the calling form know the user is cancelling.
                var oArg = new Object();
                oArg.OK = "FALSE";

                oWnd.close(oArg);
            }
        </script>
        <div id="mainDiv" runat="server" style="text-align: center; margin: 10px 10px 10px 10px; width: 450px; height: 450px;">
            <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Enter Single Entry"></asp:Label>
            <br />
            <br />
            <div style="position: relative; text-align: left; margin: 5px 0px 5px 0px; border: 1px solid Navy; width: 440px">
                <table style="border-collapse: separate; width: 80%">
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblCompanyLbl" runat="server" SkinID="InfoLabel" Text="Company:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCompany" runat="server" SkinID="InfoLabelBold" Text="Company"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblCustomerLbl" runat="server" SkinID="InfoLabel" Text="Customer:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCustomer" runat="server" SkinID="InfoLabelBold" Text="Customer"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblDateLbl" runat="server" SkinID="InfoLabel" Text="Queue Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblDate" runat="server" SkinID="InfoLabelBold" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: relative; float: left; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 440px">
                <table style="border-collapse: separate; width: 98%">
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblTail" runat="server" SkinID="InfoLabel" Text="Tail:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadTextBox ID="txtTail" runat="server" MaxLength="30" Width="100px" SelectionOnFocus="SelectAll">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="reqTail" runat="server" ControlToValidate="txtTail"
                                CssClass="stdValidator" ErrorMessage="*">
                            </asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="reqTailFound" runat="server" ControlToValidate="txtTail"
                                CssClass="stdValidator" ToolTip="Tail Number Not Found" ErrorMessage="*" 
                                OnServerValidate="reqTailFound_OnServerValidate">
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblSvcDate" runat="server" SkinID="InfoLabel" Text="Service Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadDatePicker ID="txtSvcDate" runat="server">
                            </telerik:RadDatePicker>
                            <asp:RequiredFieldValidator ID="reqSvcDate" runat="server" ControlToValidate="txtSvcDate"
                                ErrorMessage="*" CssClass="stdValidator">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblServices" runat="server" SkinID="InfoLabel" Text="Service:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Panel ID="pnlScroll" runat="server" BorderWidth="1px" ScrollBars="Vertical"
                                Width="97%" Height="101px">
                                <telerik:RadListBox ID="lstServices" runat="server" DataKeyField="Serviceid"
                                    DataValueField="Serviceid" DataTextField="Description" CssClass="lblSmall" CheckBoxes="true"
                                    SelectionMode="Multiple" Width="100%" EnableViewState="true">
                                </telerik:RadListBox>
                            </asp:Panel>
                            <asp:CustomValidator ID="reqServices" runat="server" CssClass="stdValidator" ErrorMessage="&nbsp;*"
                                ToolTip="You Must Select One or More Services" OnServerValidate="reqServices_OnServerValidate">
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblInboundTime" runat="server" SkinID="InfoLabel" Text="Inbound Time:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadTimePicker ID="dtInboundTime" runat="server" Width="100px">
                            </telerik:RadTimePicker>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblOutboundTime" runat="server" SkinID="InfoLabel" Text="Outbound Time:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadTimePicker ID="dtOutboundTime" runat="server" Width="100px">
                            </telerik:RadTimePicker>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblRamp" runat="server" SkinID="InfoLabel" Text="Ramp Location:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadTextBox ID="txtRamp" runat="server" MaxLength="20" Width="100px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>                                                                            
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblSpare" runat="server" SkinID="InfoLabel" Text="Spare?:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:CheckBox ID="chkSpare" runat="server" Checked="false" Width="20px">
                            </asp:CheckBox>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div style="text-align: center; width:100%">
                <telerik:RadButton ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_OnClick" />
                <asp:Label ID="lblCount" runat="server" ForeColor="Green" Text="Added: 0" Visible="true"></asp:Label>
                <br />
                <br />
                <telerik:RadButton ID="btnClose" runat="server" Text="Close" Visible="false" OnClientClicked="doComplete" />
                <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClientClicked="doCancel" />
            </div>
        </div>
    </form>
</body>
</html>
