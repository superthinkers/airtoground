﻿using System;
using System.Data;

public partial class ServiceQueue_IBS_Updater : System.Web.UI.Page
{
    [System.Web.Services.WebMethod]
    public static string UpdateRecord(string tail, string id, string isChecked) {
        DataSet ds = (DataSet)System.Web.HttpContext.Current.Session["INBOUND_SHEET_CACHE"];
        DataRow dr = ds.Tables[0].Rows.Find(tail);
        dr[id] = Convert.ToBoolean(isChecked);
        return "Success";
    }
}