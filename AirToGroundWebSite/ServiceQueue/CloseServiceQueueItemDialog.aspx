﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CloseServiceQueueItemDialog.aspx.cs"
    Inherits="ServiceQueue_CloseServiceQueueItemDialog" Buffer="true" Strict="true"
    Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
            <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function doComplete() {
                // Get a reference to the current RadWindow
                var oWnd = GetRadWindow();

                // Let's the calling form know the user is committing the changes, versus cancelling.
                var oArg = new Object();
                oArg.OK = "TRUE";

                oArg.SQItemId = document.getElementById("hiddenSQItemId").value;

                oArg.NotDoneReason = document.getElementById("txtNotDoneReason").value;

                // Quick validation and then close.
                if ((oArg.SQItemId != "") && (oArg.NotDoneReason != "")) {
                    oWnd.close(oArg);
                } else {
                    alert("All Values are Required");
                }
            }
            function doCancel() {
                // Get a reference to the current RadWindow
                var oWnd = GetRadWindow();

                // Let's the calling form know the user is cancelling.
                var oArg = new Object();
                oArg.OK = "FALSE";

                oWnd.close(oArg);
            }
        </script>
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server" AsyncPostBackTimeout="600"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin" ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator2" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <asp:HiddenField ID="hiddenSQItemId" runat="server" />
        <div id="mainDiv" runat="server" style="margin: 10px 10px 10px 10px; width: 350px; height: 300px;">
            <div style="position: relative; float: left; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 98%">
                <table style="border-collapse: separate; width: 98%">
                    <tr>
                        <td style="text-align: right; width: 23%">
                            <asp:Label ID="lblCustomerLbl" runat="server" SkinID="InfoLabel" Text="Customer:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCustomer" runat="server" SkinID="InfoLabelBold" Text="Customer"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 23%">
                            <asp:Label ID="lblTailNumberLbl" runat="server" SkinID="InfoLabel" Text="Tail Number:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblTailNumber" runat="server" SkinID="InfoLabelBold" Text="Tail Number"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 23%">
                            <asp:Label ID="lblServiceDescrLbl" runat="server" SkinID="InfoLabel" Text="Service:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblServiceDescr" runat="server" SkinID="InfoLabelBold" Text="Service"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 23%">
                            <asp:Label ID="lblServiceDateLbl" runat="server" SkinID="InfoLabel" Text="Service Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblServiceDate" runat="server" SkinID="InfoLabelBold" Text="Service Date"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: relative; float: left; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 98%">
                <table style="border-collapse: separate; width: 98%">
                    <tr>
                        <td style="text-align: right; width: 23%">
                            <asp:Label ID="lblClosedDate" runat="server" SkinID="InfoLabel" Text="Close Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblClosedDate1" runat="server" SkinID="InfoLabel" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 23%">
                            <asp:Label ID="lblNotDoneReason" runat="server" SkinID="InfoLabel" Text="Reason:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtNotDoneReason" runat="server" SkinID="PopBox" Width="98%" MaxLength="255" Rows="10"
                                Height="50px" Wrap="true" TextMode="MultiLine">
                            </asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="text-align: center; width:98%">
                <button title="Done" id="Submit" onclick="doComplete(); return false;">
                    Done</button>&nbsp;&nbsp;
                <button title="Cancel" id="Cancel" onclick="doCancel(); return false;">
                    Cancel</button>
            </div>
        </div>
    </form>
</body>
</html>
