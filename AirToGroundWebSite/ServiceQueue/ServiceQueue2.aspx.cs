﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;

public partial class ServiceQueue_ServiceQueue2 : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_SERVICE_QUEUE;
    }
    // For postbacks in the JavaScript in the page.
    protected string PostBackString;

    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // For postbacks in the JavaScript in the page.
        PostBackString = Page.ClientScript.GetPostBackEventReference(this, "");
        //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
        //m.IsActive = true;

        if (!Page.IsPostBack) {
            // Clear session variable.
            //Session[ATGDB.Ratedata.SESSION_RATESCHEDULE_REFRESH] = false;

            // Load some defaults. 

            // Load combo.
            //lstCompCust.DataSource = ATGDB.Companydivision.GetCustomCCJ();
            //lstCompCust.DataBind();

            // Queue date is today.
            dtQueueDate.SelectedDate = DateTime.Now.Date;
            dtQueueDate.MaxDate = DateTime.Now.Date;

            // Load the rate schedule.
            LoadServiceQueue();
            //if (lstCompCust.SelectedValue != null) {
            //    LoadServiceQueue();
            //} else {
            //    if (lstCompCust.Items.Count > 0) {
            //        lstCompCust.Items[0].Selected = true;
            //        LoadServiceQueue();
            //    }
            //}
        } else {
            if (hiddenDoubleClick.Value != "1") {
                // Refresh
                if ((Session["REFRESH_VIEW_SCHEDULES"] != null) && (Session["REFRESH_VIEW_SCHEDULES"].ToString() != "")) {
                    // Clear the refresh flag first.
                    Session.Add("REFRESH_VIEW_SCHEDULES", "");
                    // Refresh.
                    LoadServiceQueue();
                }
            }
        }
    }
    protected string GetSortClass(string controlName) {
        HtmlGenericControl div = (HtmlGenericControl)PageUtils.FindControlRecursive(RadGrid1, controlName);
        if (div != null) {
            // Get sort order.
            string sortOrder = "";
            string inFieldName = div.ID.Substring(3, div.ID.Length - 3);
            string fieldName = "";
            if (RadGrid1.MasterTableView.SortExpressions.Count == 3) {
                sortOrder = RadGrid1.MasterTableView.SortExpressions[0].SortOrderAsString();
            } else if ((RadGrid1.MasterTableView.SortExpressions.Count == 2) &&
                       (RadGrid1.MasterTableView.SortExpressions[0].FieldName == "Servicedescr")) {
                sortOrder = RadGrid1.MasterTableView.SortExpressions[0].SortOrderAsString();
            }
            // Get current sort field name.
            if (RadGrid1.MasterTableView.SortExpressions.Count > 0) {
                fieldName = RadGrid1.MasterTableView.SortExpressions[0].FieldName;
            }
            // If field name matches sort expression, then return class.
            if (inFieldName == fieldName) {
                if (sortOrder == "DESC") {
                    return "myCustSortDesc";
                } else if (sortOrder == "ASC") {
                    return "myCustSortAsc";
                } else {
                    return "";
                }
            } else {
                return "";
            }
        } else {
            return "";
        }
    }
    protected void Sort_OnClick(object sender, EventArgs e) {
        string sortOrder = "";
        Control c = (Control)sender;
        string fieldName = c.ID.Substring(3, c.ID.Length - 3);
        if (RadGrid1.MasterTableView.SortExpressions.Count == 3) {
            sortOrder = RadGrid1.MasterTableView.SortExpressions[0].SortOrderAsString();
        } else if ((RadGrid1.MasterTableView.SortExpressions.Count == 2) &&
                   (RadGrid1.MasterTableView.SortExpressions[0].FieldName == "Servicedescr")) {
            sortOrder = RadGrid1.MasterTableView.SortExpressions[0].SortOrderAsString();
        }
        
        GridSortExpression expression;
        RadGrid1.MasterTableView.SortExpressions.Clear();
        if (String.IsNullOrEmpty(sortOrder)) {
            RadGrid1.GroupingEnabled = false;
            expression = new GridSortExpression();
            expression.FieldName = fieldName;
            expression.SetSortOrder("Descending");
            RadGrid1.MasterTableView.SortExpressions.AddSortExpression(expression);
        } else if (sortOrder == "DESC") {
            RadGrid1.GroupingEnabled = false;
            expression = new GridSortExpression();
            expression.FieldName = fieldName;
            expression.SetSortOrder("Ascending");
            RadGrid1.MasterTableView.SortExpressions.AddSortExpression(expression);
        } else if (sortOrder == "ASC") {
            RadGrid1.GroupingEnabled = true;
        }
        // Always by tail
        expression = new GridSortExpression();
        expression.FieldName = "Tailnumber";
        expression.SetSortOrder("Ascending");
        RadGrid1.MasterTableView.SortExpressions.AddSortExpression(expression);
        // Always by service descr (if not sorting on that field already)
        if (fieldName != "Servicedescr") {
            expression = new GridSortExpression();
            expression.FieldName = "Servicedescr";
            expression.SetSortOrder("Ascending");
            RadGrid1.MasterTableView.SortExpressions.AddSortExpression(expression);
        }
        // Bind.
        RadGrid1.MasterTableView.Rebind();
    }
    protected void LoadServiceQueue() {
        RadGrid1.DataSourceID = "ObjectDataSource1";
        ObjectDataSource1.SelectParameters["keys"].DefaultValue = Profile.Companydivisionid + ":" + Profile.Customerid;// lstCompCust.SelectedValue;
        if (dtQueueDate.SelectedDate == null) {
            dtQueueDate.SelectedDate = DateTime.Now.Date;
        }
        ObjectDataSource1.SelectParameters["queueDate"].DefaultValue = dtQueueDate.SelectedDate.Value.Date.ToShortDateString();
        ObjectDataSource1.SelectParameters["hideRamp"].DefaultValue = chkHideRampRecs.Checked.ToString();
        ObjectDataSource1.SelectParameters["hideClosed"].DefaultValue = chkHideClosedRecs.Checked.ToString();
        RadGrid1.DataBind();
        //RadGrid1.MasterTableView.CurrentPageIndex = 0;
        if (RadGrid1.Items.Count > 0) {
            //btnExportToExcel.Visible = true;
            RadGrid1.Items[0].Selected = true;
            hiddenServiceQueueId.Value = RadGrid1.SelectedValues["Servicequeueid"].ToString();
            hiddenServiceQueueItemId.Value = RadGrid1.SelectedValues["Servicequeueitemid"].ToString();
        } else {
            // Creates a ServiceQueue if one is not found.
            hiddenServiceQueueId.Value = ATGDB.Servicequeue.GetCurrentServiceQueueId(dtQueueDate.SelectedDate.Value, Profile.Companydivisionid + ":" + Profile.Customerid).ToString();//lstCompCust.SelectedValue).ToString();
            //btnExportToExcel.Visible = false;
        }

        // Hide the load button, conditionally. Only show if today.
        //btnLoadSheet.Visible = dtQueueDate.SelectedDate.Value.Date == DateTime.Now.Date;
    }
    protected void OpenServiceQueue() {
        // Apply any data.
        ApplyGrid();
        
        hiddenDoubleClick.Value = "";
        int sqiId = int.Parse(RadGrid1.SelectedValues["Servicequeueitemid"].ToString());
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        ATGDB.Servicequeueitem sqi = dc.Servicequeueitems.SingleOrDefault(x => x.Servicequeueitemid == sqiId);
        if ((sqi != null) && (sqi.Closeddate != null)) {
            sqi.Closeddate = null;
            sqi.Isdone = false;
            dc.SubmitChanges();

            // Hide or show options
            //GridDataItem row = RadGrid1.SelectedItems[0] as GridDataItem;
            //HtmlTableRow rowOptions = (HtmlTableRow)PageUtils.FindControlRecursive(row, "rowOptions");
            //rowOptions.Style["display"] = "inline";
            // Rebind row.
            RadGrid1.Rebind();
            // Reload
            //LoadServiceQueue();
        }
    }
    protected void DoubleClick_ValueChanged(object sender, EventArgs e) {
        OpenServiceQueue();
    }
    //protected void lstCompCust_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e) {
    //    // Reload the ServiceQueue.
    //    LoadServiceQueue();
    //}
    protected void chkHideRampRecs_OnCheckChanged(object sender, EventArgs e) {
        Session["ServiceQueueErrorList"] = new List<string>();
        // Reload the ServiceQueue.
        LoadServiceQueue();
    }
    protected void chkHideClosedRecs_OnCheckChanged(object sender, EventArgs e) {
        Session["ServiceQueueErrorList"] = new List<string>();
        // Reload the ServiceQueue.
        LoadServiceQueue();
    }
    protected void dtQueueDate_OnSelectedDateChanged(object sender, EventArgs e) {
        Session["ServiceQueueErrorList"] = new List<string>();
        // Reload the ServiceQueue.
        LoadServiceQueue();
    }
    // For coloring the rows.
    //protected void RadGrid1_SelectedIndexChanged(object sender, EventArgs e) {
    //    hiddenServiceQueueItemId.Value = RadGrid1.SelectedValues["Servicequeueitemid"].ToString();
    //    int idx = RadGrid1.SelectedItems[0].ItemIndex;
    //    RadGrid1.Rebind();
    //    RadGrid1.Items[idx].Selected = true;
    //}
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e) {
        if (e.Item is GridDataItem) {
            if (!e.Item.IsInEditMode) {
                // COLOR THE ROWS, HIDE THE DELETE BUTTON.
                GridDataItem row = e.Item as GridDataItem;
                //TableCell actionCell = row["ActionColumn"];
                //TableCell editCell = row["EditCommandColumn"];

                // Hide or show options
                HtmlTableRow rowOptions = (HtmlTableRow)PageUtils.FindControlRecursive(row, "rowOptions");

                // Handy vars.
                bool isDone = Convert.ToBoolean(((DataRowView)(e.Item.DataItem)).Row["Isdone"]);
                bool isClosed = !String.IsNullOrEmpty(((DataRowView)(e.Item.DataItem)).Row["Closeddate"].ToString());

                if (isDone && isClosed) {
                    // Done and Closed
                    row.BackColor = ColorUtils.SERVICE_QUEUE_DONE_CLOSED;
                    rowOptions.Visible = false;//.Style["display"] = "none";
                } else if (!isDone && !isClosed) {
                    // Not Done, Not Closed. Don't hide actions.
                    row.BackColor = ColorUtils.SERVICE_QUEUE_NDONE_NCLOSED;
                    //rowOptions.Visible = true;
                    rowOptions.Visible = true;//.Style["display"] = "inline";
                } else if (!isDone && isClosed) {
                    // Not Done, Closed
                    row.BackColor = ColorUtils.SERVICE_QUEUE_NDONE_CLOSED;
                    //rowOptions.Visible = false;
                    rowOptions.Visible = false;//.Style["display"] = "none";
                }

                // Error Rendering
                List<string> errorList = (List<string>)Session["ServiceQueueErrorList"];
                if ((errorList != null) && (errorList.Count > 0)) {
                    string sqid = ((DataRowView)(e.Item.DataItem)).Row["Servicequeueitemid"].ToString();
                    if (errorList.Count(x => x.Contains(sqid)) > 0) {
                        HtmlGenericControl errorPanel = (HtmlGenericControl)PageUtils.FindControlRecursive(e.Item.NamingContainer, "divRadioAndDataDivs" + sqid);
                        RadioButton rdoLog = (RadioButton)PageUtils.FindControlRecursive(e.Item.NamingContainer, "rdoLog" + sqid);
                        RadioButton rdoCancel = (RadioButton)PageUtils.FindControlRecursive(e.Item.NamingContainer, "rdoCancel" + sqid);
                        RadioButton rdoClose = (RadioButton)PageUtils.FindControlRecursive(e.Item.NamingContainer, "rdoClose" + sqid);
                        errorPanel.Style["border-color"] = "Red";
                        if ((rdoLog != null) && (errorList.Count(x => x.Contains("rdoLog" + sqid)) > 0)) {
                            rdoLog.BackColor = System.Drawing.Color.Red;
                        }
                        if ((rdoCancel != null) && (errorList.Count(x => x.Contains("rdoCancel" + sqid)) > 0)) {
                            rdoCancel.BackColor = System.Drawing.Color.Red;
                        }
                        if ((rdoClose != null) && (errorList.Count(x => x.Contains("rdoClose" + sqid)) > 0)) { 
                            rdoClose.BackColor = System.Drawing.Color.Red;
                        }
                    }
                }
            }
        }
    }
    protected void SimpleLogWork_ValueChanged(object sender, EventArgs e) {
        // If there are items that need to be applied, apply first.
        ApplyGrid();

        // Create the Work Log from the hidden fields.
        string ssId = hiddenSimpleLogWorkSsId.Value;
        string sqId = hiddenServiceQueueItemId.Value;
        if ((Session["LAST_SSID"] == null) || (Session["LAST_SSID"].ToString() != ssId)) {
            //Devart.Data.MySql.MySqlMonitor m = new MySqlMonitor();
            //m.IsActive = true;
            Session.Add("LAST_SSID", ssId);
            //string jobCodeId = hiddenSimpleLogWorkJobcodeid.Value;
            DateTime workDate = DateTime.Parse(hiddenSimpleLogWorkWorkDate.Value);
            DateTime startTime = DateTime.Parse(workDate.ToShortDateString() + " " + hiddenSimpleLogWorkStartTime.Value);
            DateTime endTime = DateTime.Parse(workDate.ToShortDateString() + " " + hiddenSimpleLogWorkEndTime.Value);
            double wQty = -1.00;
            string area = "";
            if (hiddenSimpleLogWorkArea.Value != "") {
                area = hiddenSimpleLogWorkArea.Value;
            }
            if (hiddenSimpleLogWorkQuantity.Value != "") {
                wQty = double.Parse(hiddenSimpleLogWorkQuantity.Value);
            }
            string notes = hiddenSimpleLogWorkNotes.Value;
            string exSelections = hiddenSimpleLogWorkExceptions.Value;
            string exCompletedSelections = hiddenSimpleLogWorkCompletedExceptions.Value;
            string exNotes = hiddenSimpleLogWorkExNotes.Value;
            string completedNotes = hiddenSimpleLogWorkCompletedNotes.Value;
            try {
                // Process the Service Schedule. This is a very important procedure!
                ATGDB.Servicesectionrule.ProcessServiceSchedule(ssId, Profile.UserName, workDate, startTime, endTime, wQty, area, notes, exSelections, exNotes, exCompletedSelections, completedNotes);

                // Now, Close the ServiceQueue. Done. Closed.
                ATGDB.Servicequeue.CloseServiceQueueItem(int.Parse(sqId), true, DateTime.Now, "", Profile.UserName);
            } catch (Exception ex) {
                RadWindowManager1.RadAlert(ex.Message, null, null, "Error", "");
            }

            // Finally, refresh.
            LoadServiceQueue();
        }
    }
    protected void CancelSS_ValueChanged(object sender, EventArgs e) {
        // If there are items that need to be applied, apply first.
        ApplyGrid();
        
        string ssId = hiddenCancelSS_SsId.Value;
        string sqId = hiddenServiceQueueItemId.Value;
        string notes = hiddenCancelSS_Notes.Value;
        DateTime cancelledDt = DateTime.Now.Date;
        DateTime? rescheduleDt = null;
        if (hiddenCancelSS_RescheduleDate.Value != "") {
            rescheduleDt = Convert.ToDateTime(hiddenCancelSS_RescheduleDate.Value).Date;
        }
        if ((Session["LAST_CANCEL_SSID"] == null) || (Session["LAST_CANCEL_SSID"].ToString() != ssId)) {
            Session.Add("LAST_CANCEL_SSID", ssId);
            // Cancel the service schedule.
            ATGDB.Serviceschedule.CancelServiceSchedule(ssId, cancelledDt.Date, rescheduleDt, notes);
                             
            // Now, Close the ServiceQueue. Not Done. Cancelled.
            ATGDB.Serviceschedule ss = ATGDB.Serviceschedule.GetServiceSchedule(int.Parse(ssId));
            ATGDB.Servicequeue.CloseServiceQueueItem(int.Parse(sqId), false, DateTime.Now, ss.Comments, Profile.UserName);

            // Finally, refresh.
            LoadServiceQueue();
        }
    }
    protected void ServiceQueue_ValueChanged(object sender, EventArgs e) {
        // If there are items that need to be applied, apply first.
        ApplyGrid();

        string sqitemId = hiddenServiceQueueItemId.Value;
        string sqitemNotDoneReason = hiddenServiceQueueNotDoneReason.Value;
        if ((Session["LAST_CLOSE_SQITEMID"] == null) || (Session["LAST_CLOSE_SQITEMID"].ToString() != sqitemId)) {
            Session.Add("LAST_CLOSE_SQITEMID", sqitemId);                        
            // Cancel the service.
            ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
            ATGDB.Servicequeueitem sqi = db.Servicequeueitems.SingleOrDefault(x => x.Servicequeueitemid == int.Parse(sqitemId));
            if (sqi != null) {
                sqi.Isdone = false;
                sqi.Closeddate = DateTime.Now.Date;
                sqi.Notdonereason = sqitemNotDoneReason;
                db.SubmitChanges();
            }

            // Finally, refresh.
            LoadServiceQueue();
        }
    }
    protected void OptionsGrid_OnInit(object sender, EventArgs e) {
        // Fix up the dynamic control names.
        Control c = (Control)sender;
        string sqiId;
        if (c.NamingContainer is GridDataItem) {
            sqiId = (c.NamingContainer as GridDataItem).GetDataKeyValue("Servicequeueitemid").ToString();
        } else {
            // Go a level deeper (for DateInput which is nexted in the DateTimePickers.
            sqiId = (c.NamingContainer.NamingContainer as GridDataItem).GetDataKeyValue("Servicequeueitemid").ToString();
        }
        c.ID = c.ID + sqiId;

        //if (c.ID.Contains("Area") || c.ID.Contains("divRadioAndDataDivs")) {
        //    RadAjaxManager1.AjaxSettings.AddAjaxSetting(RadGrid1, c);
        //}

        // Fixup field validators also.
        if (c is RequiredFieldValidator) {
            (c as RequiredFieldValidator).ControlToValidate = (c as RequiredFieldValidator).ControlToValidate + sqiId;
            // Enable/Disable conditionally the division area.
            //if ((c as RequiredFieldValidator).ID.Contains("Area")) {
            //    // Get the service.
            //    string ssId = (c.NamingContainer as GridDataItem).GetDataKeyValue("Servicescheduleid").ToString();
            //    ATGDB.Service svc = ATGDB.Service.GetServiceByScheduleId(Convert.ToInt32(ssId));
            //    // Enable the validation
            //    (c as RequiredFieldValidator).IsValid = !svc.Requiresdivisionarea;
            //}
        }
    }
    protected void btnSynchronize_OnClick(object sender, EventArgs e) {
        Session["ServiceQueueErrorList"] = new List<string>();
        // Reload the ServiceQueue.
        LoadServiceQueue();
        // Tell this user.
        RadWindowManager1.RadAlert("Synchronized", 200, 80, "Success!", "");
    }
    protected bool ApplyGrid() {
        // Update records with the user's changes
        bool haderrors = false;
        List<string> errorList = new List<string>();
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        bool needsSubmit = false;
        foreach (GridDataItem item in RadGrid1.Items) {
            bool errors = false;
            int sqid = int.Parse(item.GetDataKeyValue("Servicequeueitemid").ToString());
            ATGDB.Servicequeueitem sqi = dc.Servicequeueitems.SingleOrDefault(x => x.Servicequeueitemid == sqid);

            // Get the bound fields by the dynamic control names.
            RadTimePicker dtInboundTime = (RadTimePicker)PageUtils.FindControlRecursive(item.NamingContainer, "dtInboundTime" + sqi.Servicequeueitemid.ToString());
            RadTimePicker dtOutboundTime = (RadTimePicker)PageUtils.FindControlRecursive(item.NamingContainer, "dtOutboundTime" + sqi.Servicequeueitemid.ToString());
            RadTextBox txtRamp = (RadTextBox)PageUtils.FindControlRecursive(item.NamingContainer, "txtRamp" + sqi.Servicequeueitemid.ToString());
            CheckBox chkIsSpare = (CheckBox)PageUtils.FindControlRecursive(item.NamingContainer, "chkIsSpare" + sqi.Servicequeueitemid.ToString());

            // Update general data.
            if (sqi.Inboundtime != dtInboundTime.SelectedDate) {
                needsSubmit = true;
                sqi.Inboundtime = dtInboundTime.SelectedDate;
            }
            if (sqi.Outboundtime != dtOutboundTime.SelectedDate) {
                needsSubmit = true;
                sqi.Outboundtime = dtOutboundTime.SelectedDate;
            }
            if (sqi.Ramp != txtRamp.Text) {
                needsSubmit = true;
                sqi.Ramp = txtRamp.Text;
            }
            if (sqi.Isspare != chkIsSpare.Checked) {
                needsSubmit = true;
                sqi.Ramp = txtRamp.Text;
            }

            // Option buttons
            //HtmlGenericControl errorPanel = (HtmlGenericControl)PageUtils.FindControlRecursive(item.NamingContainer, "divRadioAndDataDivs" + sqi.Servicequeueitemid.ToString());
            RadioButton rdoLog = (RadioButton)PageUtils.FindControlRecursive(item.NamingContainer, "rdoLog" + sqi.Servicequeueitemid.ToString());
            RadioButton rdoCancel = (RadioButton)PageUtils.FindControlRecursive(item.NamingContainer, "rdoCancel" + sqi.Servicequeueitemid.ToString());
            RadioButton rdoClose = (RadioButton)PageUtils.FindControlRecursive(item.NamingContainer, "rdoClose" + sqi.Servicequeueitemid.ToString());
            RadioButton rdoManual = (RadioButton)PageUtils.FindControlRecursive(item.NamingContainer, "rdoManual" + sqi.Servicequeueitemid.ToString());

            if ((rdoLog.Checked || rdoCancel.Checked || rdoClose.Checked || rdoManual.Checked)) {

                // VALIDATION

                // VALIDATE LOG                
                RadComboBox cmbArea = (RadComboBox)PageUtils.FindControlRecursive(item.NamingContainer, "cmbArea" + sqi.Servicequeueitemid.ToString());
                if ((rdoLog.Checked == true) &&
                    (sqi.Serviceschedule.Service.Requiresdivisionarea == true) &&
                    (String.IsNullOrEmpty(cmbArea.SelectedValue))) {
                    errors = true;
                    errorList.Add(rdoLog.ID);
                }

                // VALIDATE CANCEL                
                if ((rdoCancel.Checked == true)) {

                }

                // VALIDATE CLOSE                
                if ((rdoClose.Checked == true)) {

                }

                // IF THERE ARE ERRORS CONTINUE
                if (errors == true) {
                    haderrors = true;
                    errorList.Add(rdoLog.ID);
                    continue; // SKIP THIS ONE.
                }

                // PROCESSING

                // Get unbound fields by the dynamic control names.
                RadTimePicker dtInTime = (RadTimePicker)PageUtils.FindControlRecursive(item.NamingContainer, "dtInTime" + sqi.Servicequeueitemid.ToString());
                RadTimePicker dtOutTime = (RadTimePicker)PageUtils.FindControlRecursive(item.NamingContainer, "dtOutTime" + sqi.Servicequeueitemid.ToString());
                RadNumericTextBox txtQty = (RadNumericTextBox)PageUtils.FindControlRecursive(item.NamingContainer, "txtQty" + sqi.Servicequeueitemid.ToString());
                RadDatePicker dtRescheduleDate = (RadDatePicker)PageUtils.FindControlRecursive(item.NamingContainer, "dtRescheduleDate" + sqi.Servicequeueitemid.ToString());
                TextBox txtRescheduleReason = (TextBox)PageUtils.FindControlRecursive(item.NamingContainer, "txtRescheduleReason" + sqi.Servicequeueitemid.ToString());
                TextBox txtCloseReason = (TextBox)PageUtils.FindControlRecursive(item.NamingContainer, "txtCloseReason" + sqi.Servicequeueitemid.ToString());

                // If on a non-closed row, and If NOT manual mode for this row...
                if ((rdoManual != null) && (rdoManual.Checked == false)) {
                    if (sqi != null) {
                        // Did the user indicate to close?
                        if ((rdoLog.Checked == true) && (((dtInTime.SelectedDate != null) && (dtOutTime.SelectedDate != null)) || (txtQty.Text != ""))) {
                            try {
                                // Process the Service Schedule. This is a very important procedure!
                                ATGDB.Servicesectionrule.ProcessServiceSchedule(sqi.Servicescheduleid.ToString(),
                                    Profile.UserName, dtQueueDate.SelectedDate.Value.Date, dtInTime.SelectedDate ?? DateTime.Now,
                                    dtOutTime.SelectedDate ?? DateTime.Now, txtQty.Value ?? 0.00, cmbArea.SelectedValue, "", "", "", "", "");

                                // Close the item.
                                sqi.Closeddate = dtQueueDate.SelectedDate.Value.Date;
                                sqi.Isdone = true;
                                needsSubmit = true;
                            } catch (Exception ex) {
                                RadWindowManager1.RadAlert(ex.Message, null, null, "Error!", "");
                            }
                        } else if ((rdoCancel.Checked == true) && (txtRescheduleReason.Text != "") /*&& (dtRescheduleDate.SelectedDate != null)*/) {
                            sqi.Closeddate = DateTime.Now;
                            sqi.Isdone = true;
                            sqi.Notdonereason = txtRescheduleReason.Text;
                            needsSubmit = true;
                            // Cancel the service schedule.
                            ATGDB.Serviceschedule.CancelServiceSchedule(sqi.Servicescheduleid.ToString(),
                                DateTime.Now.Date, dtRescheduleDate.SelectedDate, txtRescheduleReason.Text);
                        } else if ((rdoClose.Checked == true) && (txtCloseReason.Text != "")) {
                            sqi.Closeddate = DateTime.Now;
                            sqi.Isdone = false;
                            sqi.Notdonereason = txtCloseReason.Text;
                            needsSubmit = true;
                        }

                    }
                }
            }

            // Do we need to submit changes?
            if (needsSubmit == true) {
                dc.SubmitChanges();
            }

            // Button handling
            btnLoadSheet.Enabled = true;
            btnLoadSheet2.Enabled = true;
            btnSynchronize.Enabled = true;
            btnQuickLog.Enabled = true;
            btnApply.Enabled = false;
            btnExportToExcel.Enabled = true;
        }
        Session["ServiceQueueErrorList"] = errorList;
        return haderrors;
    }
    protected void RadGrid1_OnPageIndexChanged(object sender, EventArgs e) {
        // Apply
        ApplyGrid();
    }
    protected void btnApply_OnClick(object sender, EventArgs e) {
        // Apply
        bool errors = ApplyGrid();
        
        // Tell the user.
        RadWindowManager1.RadAlert(errors == false ? "Changes Applied" : "Validation Errors, Not All Records Processed", 
            errors == false ? 200 : 350, 80, 
            errors == false ? "Success!" : "Errors!", "");
 
        // Refresh
        LoadServiceQueue();
    }
    protected void btnExportToExcel_OnClick(object sender, EventArgs e) {
        if (RadGrid1.MasterTableView.Items.Count > 0) {
            RadGrid1.MasterTableView.Columns.FindByUniqueName("TailColumn1").Display = false;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("TailColumn2").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("EqtypeColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ServicequeueitemidColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ServicedateColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ServicedescrColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("InboundtimeColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("OutboundtimeColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("RampColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("IsspareColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("IsdoneColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("CloseddateColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("NotdonereasonColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("UpddtColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("UpduseridColumn").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("MainColumn").Display = false;

            RadGrid1.MasterTableView.Columns.FindByUniqueName("TailColumn1").Visible = false;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("TailColumn2").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("EqtypeColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ServicequeueitemidColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ServicedateColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ServicedescrColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("InboundtimeColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("OutboundtimeColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("RampColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("IsspareColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("IsdoneColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("CloseddateColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("NotdonereasonColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("UpddtColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("UpduseridColumn").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("MainColumn").Visible = false;
            RadGrid1.Rebind();
            RadGrid1.MasterTableView.ExportToExcel();
        }
    }
}