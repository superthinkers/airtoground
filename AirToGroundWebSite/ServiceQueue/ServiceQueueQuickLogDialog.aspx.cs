﻿using System;
using System.Linq;
using System.Collections.Generic;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using ATGDB;

public partial class ServiceQueue_ServiceQueueQuickLogDialog : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_SQ_QUICKLOG;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);
        
        if (!Page.IsPostBack) {
            if ((Request.QueryString["compId"] != null) && (Request.QueryString["custId"] != null)) {
                string compId = Request.QueryString["compId"];
                string custId = Request.QueryString["custId"];
                   
                // Header summary.
                ATGDB.Companydivision cd = Companydivision.GetCompanyDivisionById(compId);
                ATGDB.Customer c = Customer.GetCustomerById(custId);
                lblCompany.Text = cd.Description;
                lblCustomer.Text = c.Description;
                DateTime dt = DateTime.Parse(Request.QueryString["queueDt"]);
                lblDate.Text = dt.ToShortDateString();

                // Services
                var data = ATGDB.Service.GetServiceQueueServicesByCustomerId(int.Parse(custId));
                var data2 = data.Where(x => x.Showinservicequeue == true);
                lstServices.DataSource = data2;
                lstServices.DataBind();

                // Dynamically adjust screen height.
                //mainDiv.Style.Add("height", data2.Count() * 10 + "px");

                // Load Division areas
                ATGDataContext db = new ATGDataContext();
                var data3 = from x in db.Divisionareas
                           orderby x.Description
                           select x;
                cmbArea.DataSource = data3;
                cmbArea.DataBind();


                //txtTail.Focus();
            }
        }
    }
    protected void reqServices_OnServerValidate(object sender, ServerValidateEventArgs e) {
        bool hasServices = lstServices.CheckedItems.Count > 0;
        e.IsValid = hasServices;
    }
    protected void reqTailFound_OnServerValidate(object sender, ServerValidateEventArgs e) {
        string tail = txtTail.Text;
        int custId = int.Parse(Request.QueryString["custId"]);
        int eqId = ATGDB.Equipment.GetEquipmentIdByTailNumber(custId, tail);
        e.IsValid = eqId > 0;
    }
    protected void btnLog_OnClick(object sender, EventArgs e) {
        if ((!String.IsNullOrEmpty(txtTail.Text)) &&
           (lstServices.CheckedItems.Count > 0)/* &&
           (dtStartTime.SelectedDate != null) &&
           (dtEndTime.SelectedDate != null)*/) {
            // Add the ServiceQueueItem
            string tail = txtTail.Text;
            DateTime? dtStart = dtStartTime.SelectedDate;
            DateTime? dtEnd = dtEndTime.SelectedDate;
            int compId = int.Parse(Request.QueryString["compId"]);
            int custId = int.Parse(Request.QueryString["custId"]);
            DateTime qdt = DateTime.Parse(Request.QueryString["queueDt"]);
            DateTime dt = txtWorkDate.SelectedDate.Value.Date;
            double qty = txtQuantity.Value ?? 0.00;
            string area = cmbArea.SelectedValue;
            string notes = txtNotes.Text;                     

            // Add a ServiceQueueItem for each selected service.
            // Validator ensures at least one is checked.
            foreach (RadListBoxItem item in lstServices.CheckedItems) {
                // Get the ServiceQueueItems list for the current queue date and by tail and serviceId.
                List<Servicequeue.ServiceQueueItemResult> sqItems = 
                    Servicequeue.GetServiceQueueItemsByCompCustAndDateAndTailAndService(compId + ":" + custId, qdt, tail, item.Value);

                // For each open service queue item found for the tail and service, log it.
                for (int lcv = 0; lcv < sqItems.Count(); lcv++) {
                    if (sqItems.ElementAt(lcv).Closeddate == null) {
                        try {
                            // Process the Service Schedule. This is a very important procedure!
                            ATGDB.Servicesectionrule.ProcessServiceSchedule(sqItems.ElementAt(lcv).Servicescheduleid.ToString(),
                                Profile.UserName, dt, dtStart, dtEnd, qty, area, notes, "", "", "", "");

                            // Now, Close the ServiceQueue. Done. Closed.
                            ATGDB.Servicequeue.CloseServiceQueueItem(sqItems.ElementAt(lcv).Servicequeueitemid, true, dt, "", Profile.UserName);

                            // Update counter.
                            int cnt = int.Parse(lblCount.Text.Substring(8, lblCount.Text.Length - 8));
                            lblCount.Text = "Logged: " + (cnt + 1).ToString();
                            lblError.Text = "";
                        } catch (Exception ex) {
                            lblError.Text = ex.Message;
                        }
                    }
                }
            }

            // Closing now, not canceling.
            btnClose.Visible = true;
            btnCancel.Visible = false;

            // Focus at beginning.
            txtTail.Text = "";
            txtTail.Focus();

            // Triggers refresh in the ServiceQueue screen.
            Session.Add("REFRESH_VIEW_SCHEDULES", "REFRESH");
        }
    }

}