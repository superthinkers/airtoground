﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="ServiceQueue2.aspx.cs" Inherits="ServiceQueue_ServiceQueue2"
    Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetSynchronizedServiceQueueItemsByCompCustAndDateDS"
        UpdateMethod="UpdateServiceQueueItem" DataObjectTypeName="ATGDB.ServiceQueueItemResult"
        TypeName="ATGDB.Servicequeue" EnableCaching="false">
        <SelectParameters>
            <asp:Parameter Name="keys" Type="String" />
            <asp:Parameter Name="queueDate" Type="DateTime" />
            <asp:Parameter Name="hideRamp" Type="Boolean" />
            <asp:Parameter Name="hideClosed" Type="Boolean" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="" OrderBy="Description" 
        TableName="Divisionareas">
    </devart:DbLinqDataSource>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <%--<telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>                                            
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="hiddenServiceQueueId" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkHideRampRecs">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkHideClosedRecs">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="lstCompCust">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="dtQueueDate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="hiddenServiceQueueId">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="hiddenServiceQueueId" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnApply">
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSychronize">
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnExportToExcel">
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnLoadSheet">
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnLoadSheet2">
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnQuickLog">
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" ReloadOnShow="false" runat="server" EnableShadow="true" Modal="true" 
        VisibleTitlebar="false" EnableViewState="false" AutoSize="true"> 
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" NavigateUrl="~/Schedules/SimpleLogWork.aspx"
                Behaviors="Close" OnClientClose="onClientClose1" ShowContentDuringLoad="false" VisibleStatusbar="false" Title="Log Work Done">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" NavigateUrl="~/Schedules/CancelServiceScheduleDialog.aspx"
                Behaviors="Close" OnClientClose="onClientClose2" ShowContentDuringLoad="false" VisibleStatusbar="false" Title="Cancel Service Schedule">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" NavigateUrl="~/ServiceQueue/CloseServiceQueueItemDialog.aspx"
                Behaviors="Close" OnClientClose="onClientClose3" ShowContentDuringLoad="false" VisibleStatusbar="false" Title="Close Queue Item">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow4" runat="server" NavigateUrl="~/ServiceQueue/InboundSheetEntryDialog.aspx"
                Behaviors="Close" OnClientClose="onClientClose4" ShowContentDuringLoad="false" VisibleStatusbar="false" Title="Inbound Sheet Entry Dialog">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow5" runat="server" NavigateUrl="~/ServiceQueue/InboundSheetSingleEntryDialog.aspx"
                Behaviors="Close" OnClientClose="onClientClose5" ShowContentDuringLoad="false" VisibleStatusbar="false" Title="Inbound Sheet Entry Dialog">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow6" runat="server" NavigateUrl="~/ServiceQueue/ServiceQueueQuickLogDialog.aspx"
                Behaviors="Close" OnClientClose="onClientClose5" ShowContentDuringLoad="false" VisibleStatusbar="false" Title="Service Queue Quick Log">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadCodeBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            //function DoGridResize(sender, eventArgs) {
                //window.onresize();
            //}
            //$(function () {
                //DoGridResize();
            //});
            //window.onresize = function () {
            //    // Set the offsite height.
            //    var offset1 = 100;
            //
            //    // Get page window height.
            //    var windowHeight = window.innerHeight; // || document.documentElement.clientHeight;
            //    var windowWidth = window.innerWidth; // || document.documentElement.clientHeight;
            //
            //    // Reset grid height.
            //    var grid = $find("<%= RadGrid1.ClientID %>");
            //    if (grid) {
            //        grid.get_element().style.height = windowHeight - offset1 + "px";
            //        grid.get_element().style.width = windowWidth - (.062 * windowWidth) + "px";
            //        grid.repaint();
            //        if (grid.MasterTableView) {
            //           grid.MasterTableView.get_element().clientHeight = windowHeight - offset1 + "px";
            //        }
            //   }
            //}
            // This fixes problems when exporting.
            function onRequestStart(sender, args) {
                if ((args.get_eventTarget().indexOf("btnExportToExcel") >= 0) || 
                    (args.get_eventTarget().indexOf("xxx") >= 0)) {
                    // Disable ajax
                    args.set_enableAjax(false);
                } else if (args.get_eventTarget().indexOf("btnLoadSheet") >= 0) {
                    var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                    loadingPanel.visible = false;
                }
            }
            // Actions
            function openWin1(ssId, serviceId, sqitemId) {
                radopen("../Schedules/SimpleLogWork.aspx?ssId=" + ssId + "&serviceId=" + serviceId + "&sqitemId=" + sqitemId, "RadWindow1");
            }
            function openWin2(ssId, sqitemId) {
                radopen("../Schedules/CancelServiceScheduleDialog.aspx?ssId=" + ssId + "&sqitemId=" + sqitemId, "RadWindow2");
            }
            function openWin3(sqitemId) {
                radopen("CloseServiceQueueItemDialog.aspx?sqitemId=" + sqitemId, "RadWindow3");
            }
            function openWin4() {
                //var obj = $find("< %= lstCompCust.ClientID %>");
                var compId = <%= Profile.Companydivisionid %>;//obj._value.substring(0, obj._value.indexOf(":"));
                var custId = <%= Profile.Customerid %>;//obj._value.substring(obj._value.indexOf(":") + 1, obj._value.length - obj._value.indexOf(":") + 1);
                var obj2 = $get("<%= hiddenServiceQueueId.ClientID %>");
                var dt = $get("<%= dtQueueDate.ClientID %>");
                radopen("InboundSheetEntryDialog.aspx?compId=" + compId + "&custId=" + custId + 
                    "&serviceQueueId=" + obj2.value + "&queueDt=" + dt.value, "RadWindow4");
                return false;
            }
            function openWin5() {
                //var obj = $find("< %= lstCompCust.ClientID %>");
                var compId = <%= Profile.Companydivisionid %>;//obj._value.substring(0, obj._value.indexOf(":"));
                var custId = <%= Profile.Customerid %>;//obj._value.substring(obj._value.indexOf(":") + 1, obj._value.length - obj._value.indexOf(":") + 1);
                var obj2 = $get("<%= hiddenServiceQueueId.ClientID %>");
                var dt = $get("<%= dtQueueDate.ClientID %>");
                radopen("InboundSheetSingleEntryDialog.aspx?compId=" + compId + "&custId=" + custId + 
                    "&serviceQueueId=" + obj2.value + "&queueDt=" + dt.value, "RadWindow5");
                return false;
            }
            function openWin6() {
                //var obj = $find("< %= lstCompCust.ClientID %>");
                var compId = <%= Profile.Companydivisionid %>;//obj._value.substring(0, obj._value.indexOf(":"));
                var custId = <%= Profile.Customerid %>;//obj._value.substring(obj._value.indexOf(":") + 1, obj._value.length - obj._value.indexOf(":") + 1);
                var obj2 = $get("<%= hiddenServiceQueueId.ClientID %>");
                var dt = $get("<%= dtQueueDate.ClientID %>");
                radopen("ServiceQueueQuickLogDialog.aspx?compId=" + compId + "&custId=" + custId + 
                    "&serviceQueueId=" + obj2.value + "&queueDt=" + dt.value, "RadWindow6");
                return false;
            }
            function onClientClose1(oWnd, args) {
                oWnd.set_width(100);
                oWnd.set_height(100);
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg && (arg.OK == "TRUE")) {
                    // Update the hidden fields. Will trigger value changed event.
                    var hidden = $get("<%= hiddenSimpleLogWorkSsId.ClientID %>");
                    hidden.value = arg.SsId;

                    hidden = $get("<%= hiddenServiceQueueItemId.ClientID %>");
                    hidden.value = arg.SQItemId;

                    // By setting the trigger field here to ssId, it shouldn't fire too often.
                    hidden = $get("<%= hiddenSimpleLogWorkTrigger.ClientID %>");
                    hidden.value = arg.SsId;

                    //hidden = $get("<%= hiddenSimpleLogWorkJobcodeid.ClientID %>");
                    //hidden.value = arg.Jobcodeid;

                    hidden = $get("<%= hiddenSimpleLogWorkWorkDate.ClientID %>");
                    hidden.value = arg.WorkDate;

                    hidden = $get("<%= hiddenSimpleLogWorkStartTime.ClientID %>");
                    hidden.value = arg.StartTime;

                    hidden = $get("<%= hiddenSimpleLogWorkEndTime.ClientID %>");
                    hidden.value = arg.EndTime;

                    hidden = $get("<%= hiddenSimpleLogWorkQuantity.ClientID %>");
                    hidden.value = arg.Quantity;

                    hidden = $get("<%= hiddenSimpleLogWorkArea.ClientID %>");
                    hidden.value = arg.Area;

                    hidden = $get("<%= hiddenSimpleLogWorkNotes.ClientID %>");
                    hidden.value = arg.Notes;

                    hidden = $get("<%= hiddenSimpleLogWorkExceptions.ClientID %>");
                    hidden.value = arg.Exceptions;

                    hidden = $get("<%= hiddenSimpleLogWorkCompletedExceptions.ClientID %>");
                    hidden.value = arg.CompletedExceptions;

                    hidden = $get("<%= hiddenSimpleLogWorkExNotes.ClientID %>");
                    hidden.value = arg.ExNotes;

                    hidden = $get("<%= hiddenSimpleLogWorkCompletedNotes.ClientID %>");
                    hidden.value = arg.CompletedNotes;                

                    // For debugging.
                    //alert(arg.SsId + " :: Dates / Times :: " +
                    //      arg.WorkDate + " -> " + arg.StartTime + " -> " + arg.EndTime + " :: Qty :: " +
                    //      arg.Quantity + " :: Notes :: " + arg.Notes + " :: Exception Info :: " + 
                    //      arg.Exceptions + " -> " + arg.ExNotes);

                    // Force a page postback. Causes hidden's OnValueChanged to fire.
                    <%= PostBackString %>
                }
            }
            function onClientClose2(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg && (arg.OK == "TRUE")) {
                    // Update the hidden fields. Will trigger value changed event.
                    var hidden = $get("<%= hiddenCancelSS_SsId.ClientID %>");
                    hidden.value = arg.SsId;

                    hidden = $get("<%= hiddenServiceQueueItemId.ClientID %>");
                    hidden.value = arg.SQItemId;

                    hidden = $get("<%= hiddenCancelSS_RescheduleDate.ClientID %>");
                    hidden.value = arg.RescheduleDate;

                    hidden = $get("<%= hiddenCancelSS_Notes.ClientID %>");
                    hidden.value = arg.Notes;

                    // Force a page postback. Causes hidden's OnValueChanged to fire.
                    <%= PostBackString %>
                }
            }
            function onClientClose3(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg && (arg.OK == "TRUE")) {
                    // Update the hidden fields. Will trigger value changed event.

                    var hidden = $get("<%= hiddenServiceQueueItemId.ClientID %>");
                    hidden.value = arg.SQItemId;

                    hidden = $get("<%= hiddenServiceQueueNotDoneReason.ClientID %>");
                    hidden.value = arg.NotDoneReason;

                    // Force a page postback. Causes hidden's OnValueChanged to fire.
                    <%= PostBackString %>
                }
            }
            function onClientClose4(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg && (arg.OK == "TRUE")) {
                    // Force a page postback. Causes refresh.
                    <%= PostBackString %>
                }
            }
            function onClientClose5(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg && (arg.OK == "TRUE")) {
                    // Force a page postback. Causes refresh.
                    <%= PostBackString %>
                }
            }
            function onDoubleClick() {
                // Set trigger for postback event.
                hidden = $get("<%= hiddenDoubleClick.ClientID %>");
                hidden.value = "1";
                // Force a page postback. Causes refresh.
                <%= PostBackString %>
            }
            function EnableDisableButtons() {
                var btnLoadSheet = $find("<%= btnLoadSheet.ClientID %>");
                btnLoadSheet.set_enabled(false); // Can't Load - user loading via queue records right now. Must apply.
                var btnLoadSheet2 = $find("<%= btnLoadSheet2.ClientID %>");
                btnLoadSheet2.set_enabled(false); // Can't Load - user loading via queue records right now. Must apply.
                var btnSynchronize = $find("<%= btnSynchronize.ClientID %>");
                btnSynchronize.set_enabled(false); // Can't Sync - user loading via queue records right now. Must apply.
                var btnQuickLog = $find("<%= btnQuickLog.ClientID %>");
                btnQuickLog.set_enabled(false); // Can't Sync - user loading via queue records right now. Must apply.
                var btnApply = $find("<%= btnApply.ClientID %>");
                btnApply.set_enabled(true); // TRUE - Can Apply
                var btnExportToExcel = $find("<%= btnExportToExcel.ClientID %>");
                btnExportToExcel.set_enabled(false); // Can't Export - user loading via queue records right now. Must apply.
            }
            function EnableDisable(rdo, sqiId) {
                //alert(rdo + " " + sqiId);
                
                // Some button behaviors.
                EnableDisableButtons();
                                     
                // Get a handle to everything.
                var divLog = $("#divLog" + sqiId);
                var divCancel = $("#divCancel" + sqiId);
                var divClose = $("#divClose" + sqiId);
                var divManual = $("#divManual" + sqiId);
                // Hide all first.
                divLog.hide();
                divCancel.hide();
                divClose.hide();
                divManual.hide();
                if (rdo == "LOG") {
                    divLog.show();
                } else if (rdo == "CANCEL") {
                    divCancel.show();
                } else if (rdo == "CLOSE") {
                    divClose.show();
                } else if (rdo == "MANUAL") {
                    divManual.show();
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <!-- DOUBLE CLICK FEATURE -->
    <asp:HiddenField ID="hiddenDoubleClick" runat="server" Value="" OnValueChanged="DoubleClick_ValueChanged" />
    <!-- SIMPLE LOG WORK -->
    <asp:HiddenField ID="hiddenSimpleLogWorkTrigger" runat="server" Value="" OnValueChanged="SimpleLogWork_ValueChanged" />
    <asp:HiddenField ID="hiddenSimpleLogWorkSsId" runat="server" Value="" />
    <asp:HiddenField ID="hiddenSimpleLogWorkJobcodeid" runat="server" Value="" />
    <asp:HiddenField ID="hiddenSimpleLogWorkWorkDate" runat="server" Value="" />
    <asp:HiddenField ID="hiddenSimpleLogWorkStartTime" runat="server" Value="" />
    <asp:HiddenField ID="hiddenSimpleLogWorkEndTime" runat="server" Value="" />
    <asp:HiddenField ID="hiddenSimpleLogWorkQuantity" runat="server" Value="" />
    <asp:HiddenField ID="hiddenSimpleLogWorkArea" runat="server" Value="" />
    <asp:HiddenField ID="hiddenSimpleLogWorkNotes" runat="server" Value="" />
    <asp:HiddenField ID="hiddenSimpleLogWorkExceptions" runat="server" Value="" />
    <asp:HiddenField ID="hiddenSimpleLogWorkCompletedExceptions" runat="server" Value="" />
    <asp:HiddenField ID="hiddenSimpleLogWorkExNotes" runat="server" Value="" />
    <asp:HiddenField ID="hiddenSimpleLogWorkCompletedNotes" runat="server" Value="" />
    <!-- Cancel SS NOTES -->
    <asp:HiddenField ID="hiddenCancelSS_SsId" runat="server" Value="" OnValueChanged="CancelSS_ValueChanged" />
    <asp:HiddenField ID="hiddenCancelSS_RescheduleDate" runat="server" Value="" />
    <asp:HiddenField ID="hiddenCancelSS_Notes" runat="server" Value="" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="true"
        Skin="Office2007">
    </telerik:RadAjaxLoadingPanel>
    <!-- SERVICE QUEUE - THESE MUST BE HERE -->
    <asp:HiddenField ID="hiddenServiceQueueId" runat="server" Value="" />
    <asp:HiddenField ID="hiddenServiceQueueItemId" runat="server" Value="" />
    <asp:HiddenField ID="hiddenServiceQueueNotDoneReason" runat="server" Value="" OnValueChanged="ServiceQueue_ValueChanged" />
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Service Queue Manager"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Service Queue Manager</div>
            <!-- SEARCH -->
            <table style="width: 40%;">
                <tr>
                    <td style="width: 60%">
                        <!-- SEARCH -->
                        <fieldset style="width: 94%">
                            <legend>Select the Daily Queue to Manage</legend>
                            <table width="98%">
                                <tr>
                                    <%--<td>
                                        <telerik:RadComboBox ID="lstCompCust" runat="server" Width="99%" DataValueField="Keys"
                                            DataTextField="Description" AutoPostBack="true" OnSelectedIndexChanged="lstCompCust_SelectedIndexChanged">
                                        </telerik:RadComboBox>
                                    </td>--%>
                                    <td>
                                        Queue Date:
                                        <br />
                                        <telerik:RadDatePicker ID="dtQueueDate" runat="server" AutoPostBack="true" Width="120px"
                                            OnSelectedDateChanged="dtQueueDate_OnSelectedDateChanged" EnableTyping="false">
                                        </telerik:RadDatePicker>
                                        <br />
                                        <asp:CheckBox ID="chkHideRampRecs" runat="server" AutoPostBack="true" Text="Hide Items Without Ramp?"
                                            OnCheckedChanged="chkHideRampRecs_OnCheckChanged" />
                                        <br />
                                        <asp:CheckBox ID="chkHideClosedRecs" runat="server" AutoPostBack="true" Text="Hide Closed Items?"
                                            Checked="true" OnCheckedChanged="chkHideClosedRecs_OnCheckChanged" />
                                    </td>
                                        
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="width: 39%" valign="top">
                        <!-- LEGEND -->
                        <fieldset style="width: 97%">
                            <legend>Legend</legend>
                            <table width="98%">
                                <tr>
                                    <td align="left" style="background-color: Silver">
                                        Done and Closed
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="background-color: MediumSpringGreen">
                                        Not Done, Not Closed
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="background-color: LightSalmon">
                                        Not Done, Closed
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <!-- Service Queue Items -->
            <div style="margin: 4px !important; width: 99%;">
                <div class="lblInfo">Selected Service Queue Items:</div>
                <br />
                <telerik:RadButton ID="btnSynchronize" runat="server" Text="Synchronize" OnClick="btnSynchronize_OnClick" />
                <telerik:RadButton ID="btnLoadSheet" runat="server" Text="Load Inbound Sheet" OnClientClicked="openWin4" AutoPostBack="false" />
                <telerik:RadButton ID="btnLoadSheet2" runat="server" Text="Enter Single Entry" OnClientClicked="openWin5" AutoPostBack="false" />
                <telerik:RadButton ID="btnQuickLog" runat="server" Text="Quick Log" OnClientClicked="openWin6" AutoPostBack="false" />
                <telerik:RadButton ID="btnApply" runat="server" Enabled="false" Text="Apply All Changes" OnClick="btnApply_OnClick" />
                <telerik:RadButton ID="btnExportToExcel" runat="server" Text="Export to Excel" OnClick="btnExportToExcel_OnClick" />
                <br />
                <br />
                <div style="margin: 4px !important; width: 100%;">
                    <telerik:RadGrid ID="RadGrid1" GridLines="None" Width="100%" Height="600px"
                        runat="server" AllowAutomaticDeletes="false" AllowAutomaticInserts="false"
                        AllowMultiRowEdit="false" AllowMultiRowSelection="false" AllowAutomaticUpdates="false"
                        ShowGroupPanel="false" AllowPaging="true" PageSize="10" AllowSorting="false"
                        AutoGenerateColumns="false" OnItemDataBound="RadGrid1_ItemDataBound" OnPageIndexChanged="RadGrid1_OnPageIndexChanged">
                        <MasterTableView Width="100%" CommandItemDisplay="None" DataKeyNames="Servicequeueitemid,Servicequeueid,Servicescheduleid"
                            AllowAutomaticUpdates="false" NoMasterRecordsText="No Service Queue Items Today"
                            AllowSorting="true" AllowMultiColumnSorting="true" CommandItemSettings-ShowRefreshButton="false"
                            CommandItemSettings-ShowAddNewRecordButton="false" TableLayout="Fixed" GroupLoadMode="Client"
                            Font-Size="12px" HorizontalAlign="NotSet" RowIndicatorColumn-Display="false"
                            AutoGenerateColumns="false" EditMode="InPlace">
                            <PagerStyle AlwaysVisible="true" Mode="NumericPages" Position="Bottom" />
                            <GroupByExpressions>
                                <telerik:GridGroupByExpression>
                                    <SelectFields>
                                        <telerik:GridGroupByField HeaderText="Tail Number" FieldAlias="Tailnumber" FieldName="Tailnumber" />
                                    </SelectFields>
                                    <GroupByFields>
                                        <telerik:GridGroupByField FieldName="Tailnumber" />
                                    </GroupByFields>
                                </telerik:GridGroupByExpression>
                            </GroupByExpressions>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="TailColumn1" HeaderText="Tail" AllowFiltering="false" Visible="false">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>  
                                        <a href='<%# "../Schedules/ViewSchedules.aspx?Mode=3&tail=" + Eval("Tailnumber") + "&dt=" + Eval("Queuedate", "{0:d}") + "&cid=" + Eval("Customerid") %>' target="_blank">
                                            <%# Eval("Tailnumber") %></a>
                                    </ItemTemplate>
                                    <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                    <ItemStyle Width="40px" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="Tailnumber" HeaderText="Tail"
                                    UniqueName="TailColumn2" ReadOnly="true" Visible="false">
                                    <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                    <ItemStyle Width="40px" HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Eqtype" HeaderText="Type" AllowSorting="true"
                                    UniqueName="EqtypeColumn" ReadOnly="true" Visible="false">
                                    <HeaderStyle Width="55px" HorizontalAlign="Center" />
                                    <ItemStyle Width="55px" HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Servicequeueitemid" HeaderText="Id" AllowSorting="false"
                                    UniqueName="ServicequeueitemidColumn" ReadOnly="true" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Servicedate" HeaderText="Service Date" AllowSorting="true"
                                    UniqueName="ServicedateColumn" ReadOnly="true" Visible="false" DataFormatString="{0:d}">
                                    <HeaderStyle Width="90px" HorizontalAlign="Center" />
                                    <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Servicedescr" HeaderText="Description" AllowSorting="true" 
                                    UniqueName="ServicedescrColumn" ReadOnly="true" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="Inboundtime" HeaderText="Inbound Time" AllowSorting="true"
                                    UniqueName="InboundtimeColumn" ReadOnly="false" Visible="false" DataFormatString="{0:t}"
                                    PickerType="TimePicker" DefaultInsertValue="08:00:00 AM" ColumnEditorID="InboundtimeEditor">
                                    <HeaderStyle Width="90px" HorizontalAlign="Center" />
                                    <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="Outboundtime" HeaderText="Outbound Time" AllowSorting="true"
                                    UniqueName="OutboundtimeColumn" ReadOnly="false" Visible="false" DataFormatString="{0:t}"
                                    PickerType="TimePicker" DefaultInsertValue="05:00:00 PM" ColumnEditorID="OutboundtimeEditor">
                                    <HeaderStyle Width="90px" HorizontalAlign="Center" />
                                    <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="Ramp" HeaderText="Ramp Location" AllowSorting="true"
                                    UniqueName="RampColumn" ReadOnly="false" Visible="false" ColumnEditorID="RampEditor">
                                    <HeaderStyle Width="85px" HorizontalAlign="Center" />
                                    <ItemStyle Width="85px" HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="Isspare" HeaderText="Spare?" AllowSorting="true"
                                    UniqueName="IsspareColumn" ReadOnly="false" Visible="false">
                                    <HeaderStyle Width="60px" HorizontalAlign="Center" />
                                    <ItemStyle Width="60px" HorizontalAlign="Center" />
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridCheckBoxColumn DataField="Isdone" HeaderText="Done?" AllowSorting="true"
                                    UniqueName="IsdoneColumn" ReadOnly="true" Visible="false">
                                    <HeaderStyle Width="60px" HorizontalAlign="Center" />
                                    <ItemStyle Width="60px" HorizontalAlign="Center" />
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridBoundColumn DataField="Closeddate" HeaderText="Closed" AllowSorting="true"
                                    UniqueName="CloseddateColumn" ReadOnly="true" Visible="false" DataFormatString="{0:d}">
                                    <HeaderStyle Width="65px" HorizontalAlign="Center" />
                                    <ItemStyle Width="65px" HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Notdonereason" HeaderText="Why Wasn't It Done?" AllowSorting="false"
                                    UniqueName="NotdonereasonColumn" ReadOnly="true" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Upddt" HeaderText="Upd Dt" AllowSorting="true"
                                    UniqueName="UpddtColumn" ReadOnly="true" Visible="false" DataFormatString="{0:d}">
                                    <HeaderStyle Width="85px" HorizontalAlign="Center" />
                                    <ItemStyle Width="85px" HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Upduserid" HeaderText="Upd UserId" AllowSorting="true"
                                    UniqueName="UpduseridColumn" ReadOnly="true" Visible="false">
                                    <HeaderStyle Width="120px" />
                                    <ItemStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="MainColumn" HeaderText="" AllowFiltering="false">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <HeaderTemplate>
                                        <table width="100%" style="border: 0px none; border-collapse: collapse;
                                            padding: 0px 0px 0px 0px; table-layout: fixed; margin: 0px 0px 0px 0px">
                                            <tr>
                                                <td style="border-width: 0px; width: 50px; margin-left: 5px; text-align: left">
                                                    <asp:LinkButton ID="btnTailnumber" runat="server" BorderWidth="0px" Text="Tail"></asp:LinkButton>
                                                </td>
                                                <td style="border-width: 0px; width: 65px; text-align: left">
                                                    <div style="position: relative; float: left">
                                                        <asp:LinkButton ID="btnEqtype" runat="server" BorderWidth="0px"
                                                            Text="Eq Type" OnClick="Sort_OnClick" />
                                                    </div>
                                                    <div id="divEqtype" runat="server" class='<%# GetSortClass("divEqtype") %>' style="position: relative;
                                                        float: left">
                                                    </div>
                                                </td>                                                                        
                                                <td style="border-width: 0px; width: 88px; text-align: left">
                                                    <div style="position: relative; float: left">
                                                        <asp:LinkButton ID="btnServicedate" runat="server" BorderWidth="0px"
                                                            Text="Service Date" OnClick="Sort_OnClick" />
                                                    </div>
                                                    <div id="divServicedate" runat="server" class='<%# GetSortClass("divServicedate") %>'
                                                        style="position: relative; float: left">
                                                    </div>
                                                </td>
                                                <td style="border-width: 0px; width: 52%; text-align: left">
                                                    <div style="position: relative; float: left">
                                                        <asp:LinkButton ID="btnServicedescr" runat="server" BorderWidth="0px"
                                                            Text="Service" OnClick="Sort_OnClick"></asp:LinkButton>
                                                    </div>
                                                    <div id="divServicedescr" runat="server" class='<%# GetSortClass("divServicedescr") %>'
                                                        style="position: relative; float: left">
                                                    </div>
                                                </td>
                                                <td valign="top" style="border-width: 0px; width: 100px; text-align: left">
                                                    <div style="position: relative; float: left">
                                                        <asp:LinkButton ID="btnInboundtime" runat="server" BorderWidth="0px"
                                                            Text="Inbound" OnClick="Sort_OnClick"></asp:LinkButton>
                                                    </div>
                                                    <div id="divInboundtime" runat="server" class='<%# GetSortClass("divInboundtime") %>'
                                                        style="position: relative; float: left">
                                                    </div>
                                                </td>
                                                <td valign="top" style="border-width: 0px; width: 100px; text-align: left">
                                                    <div style="position: relative; float: left">
                                                        <asp:LinkButton ID="btnOutboundtime" runat="server" BorderWidth="0px"
                                                            Text="Outbound" OnClick="Sort_OnClick"></asp:LinkButton>
                                                    </div>
                                                    <div id="divOutboundtime" runat="server" class='<%# GetSortClass("divOutboundtime") %>'
                                                        style="position: relative; float: left">
                                                    </div>
                                                </td>
                                                <td style="border-width: 0px; width: 75px; text-align: left">
                                                    <div style="position: relative; float: left">
                                                        <asp:LinkButton ID="btnRamp" runat="server" BorderWidth="0px"
                                                            Text="Ramp" OnClick="Sort_OnClick"></asp:LinkButton>
                                                    </div>
                                                    <div id="divRamp" runat="server" class='<%# GetSortClass("divRamp") %>' style="position: relative;
                                                        float: left">
                                                    </div>
                                                </td>
                                                <td style="border-width: 0px; width: 80px; text-align: left">
                                                    <div style="position: relative; float: left">
                                                        <asp:LinkButton ID="btnIsspare" runat="server" BorderWidth="0px"
                                                            Text="Is Spare?" OnClick="Sort_OnClick"></asp:LinkButton>
                                                    </div>
                                                    <div id="divIsspare" runat="server" class='<%# GetSortClass("divIsspare") %>' style="position: relative;
                                                        float: left">
                                                    </div>
                                                </td>
                                                <td style="border-width: 0px; width: 80px; text-align: left">
                                                    <div style="position: relative; float: left">
                                                        <asp:LinkButton ID="btnIsdone" runat="server" BorderWidth="0px"
                                                            Text="Is Done?" OnClick="Sort_OnClick"></asp:LinkButton>
                                                    </div>
                                                    <div id="divIsdone" runat="server" class='<%# GetSortClass("divIsdone") %>' style="position: relative;
                                                        float: left">
                                                    </div>
                                                </td>
                                                <td style="border-width: 0px; width: 90px; text-align: left">
                                                    <div style="position: relative; float: left">
                                                        <asp:LinkButton ID="btnCloseddate" runat="server" BorderWidth="0px"
                                                            Text="Closed Date" OnClick="Sort_OnClick"></asp:LinkButton>
                                                    </div>
                                                    <div id="divCloseddate" runat="server" class='<%# GetSortClass("divCloseddate") %>'
                                                        style="position: relative; float: left">
                                                    </div>
                                                </td>
                                                <td style="border-width: 0px; width: 50%; text-align: left">
                                                    <asp:LinkButton ID="btnNotdonereason" runat="server" BorderWidth="0px" Text="Notes"></asp:LinkButton>
                                                </td>
                                                <td style="border-width: 0px; width: 60px; text-align: left">
                                                    <div style="position: relative; float: left">
                                                        <asp:LinkButton ID="btnUpddt" runat="server" BorderWidth="0px"
                                                            Text="Upd Dt" OnClick="Sort_OnClick"></asp:LinkButton>
                                                    </div>
                                                    <div id="divUpddt" runat="server" class='<%# GetSortClass("divUpddt") %>' style="position: relative;
                                                        float: left">
                                                    </div>
                                                </td>
                                                <td style="border-width: 0px; width: 85px; text-align: left">
                                                    <div style="position: relative; float: left">
                                                        <asp:LinkButton ID="btnUpduserid" runat="server" BorderWidth="0px"
                                                            Text="Upd UserId" OnClick="Sort_OnClick"></asp:LinkButton>
                                                    </div>
                                                    <div id="divUpduserid" runat="server" class='<%# GetSortClass("divUpduserid") %>'
                                                        style="position: relative; float: left">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <!-- NOTE: This div height set in code in itemDataBound -->
                                        <div id="divItemTemplate" runat="server" style="width: 100%; border: 1px solid Grey" ondblclick="onDoubleClick">
                                            <table id="tblOptions" runat="server" width="100%" style="border: 0px None Grey; border-collapse: collapse; table-layout: fixed" ondblclick="onDoubleClick">
                                                <tr ondblclick="onDoubleClick">
                                                    <td style="border-width: 0px; width: 32px; text-align: left" ondblclick="onDoubleClick">
                                                        <a href='<%# "../Schedules/ViewSchedules.aspx?Mode=3&tail=" + Eval("Tailnumber") + "&dt=" + Eval("Queuedate", "{0:d}") + "&cid=" + Eval("Customerid") %>'
                                                            target="_blank">
                                                            <%# Eval("Tailnumber") %>
                                                        </a>
                                                    </td>
                                                    <td style="border-width: 0px; width: 53px; text-align: left">
                                                        <%# Eval("Eqtype") %>
                                                    </td>
                                                    <td style="border-width: 0px; width: 73px; text-align: left">
                                                        <%# Eval("Servicedate", "{0:d}") %>
                                                    </td>
                                                    <td style="border-width: 0px; width: 50%; text-align: left">
                                                        <%# Eval("Servicedescr") %>
                                                    </td>
                                                    <td valign="bottom" style="border-width: 0px; width: 90px; text-align: left">
                                                        <asp:Label ID="lblInboundTime" runat="server" Visible='<%# Eval("Closeddate") != DBNull.Value %>'
                                                            Text='<%# Eval("Inboundtime") == DBNull.Value ? null : Convert.ToDateTime(Eval("Inboundtime")).ToShortTimeString() %>'></asp:Label>
                                                        <telerik:RadTimePicker ID="dtInboundTime" runat="server" Visible='<%# Eval("Closeddate") == DBNull.Value %>'
                                                            MaxDate='<%# DateTime.Now.Date.AddDays(1) %>' DateInput-Font-Size="Smaller" Width="98%"
                                                            OnInit="OptionsGrid_OnInit" OnChange="EnableDisableButtons(); return true;" SelectedDate='<%# Eval("Inboundtime") == DBNull.Value ? null : Eval("Inboundtime") %>'
                                                            ClientEvents-OnDateSelected="EnableDisableButtons">
                                                        </telerik:RadTimePicker>
                                                    </td>
                                                    <td valign="bottom" style="border-width: 0px; width: 90px; text-align: left">
                                                        <asp:Label ID="Label1" runat="server" Visible='<%# Eval("Closeddate") != DBNull.Value %>'
                                                            Text='<%# Eval("Outboundtime") == DBNull.Value ? null : Convert.ToDateTime(Eval("Outboundtime")).ToShortTimeString() %>'></asp:Label>
                                                        <telerik:RadTimePicker ID="dtOutboundTime" runat="server" Visible='<%# Eval("Closeddate") == DBNull.Value %>'
                                                            MaxDate='<%# DateTime.Now.Date.AddDays(1) %>' DateInput-Font-Size="Smaller" Width="98%"
                                                            OnInit="OptionsGrid_OnInit" OnChange="EnableDisableButtons(); return true;" SelectedDate='<%# Eval("Outboundtime") == DBNull.Value ? null : Eval("Outboundtime") %>'
                                                            ClientEvents-OnDateSelected="EnableDisableButtons">
                                                        </telerik:RadTimePicker>
                                                    </td>
                                                    <td valign="bottom" style="border-width: 0px; width: 75px; text-align: left">
                                                        <asp:Label ID="lblRamp" runat="server" Visible='<%# Eval("Closeddate") != DBNull.Value %>'
                                                            Text='<%# Eval("Ramp") %>'></asp:Label>
                                                        <telerik:RadTextBox ID="txtRamp" runat="server" Visible='<%# Eval("Closeddate") == DBNull.Value %>'
                                                            Font-Size="Smaller" Width="98%" MaxLength="20" OnInit="OptionsGrid_OnInit" OnChange="EnableDisableButtons(); return true;"
                                                            Text='<%# Eval("Ramp") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td style="border-width: 0px; width: 70px; text-align: left">
                                                        <asp:CheckBox ID="chkIsSpare" runat="server" Enabled='<%# Eval("Closeddate") == DBNull.Value %>'
                                                            OnInit="OptionsGrid_OnInit" OnChange="EnableDisableButtons(); return true;"
                                                            Checked='<%# Eval("Isspare") ?? false %>' />
                                                    </td>
                                                    <td style="border-width: 0px; width: 45px; text-align: left">
                                                        <asp:CheckBox ID="chkIsDone" runat="server" Checked='<%# Eval("Isdone") ?? false %>' Enabled="false" />
                                                    </td>
                                                    <td style="border-width: 0px; width: 75px; text-align: left">
                                                        <%# Eval("Closeddate", "{0:d}") %>
                                                    </td>
                                                    <td style="border-width: 0px; width: 50%; text-align: left">
                                                        <%# Eval("Notdonereason") %>
                                                    </td>
                                                    <td style="border-width: 0px; width: 45px; text-align: left">
                                                        <%# Eval("Upddt", "{0:d}") %>
                                                    </td>
                                                    <td style="border-width: 0px; width: 80px; text-align: left">
                                                        <%# Eval("Upduserid") %>
                                                    </td>
                                                </tr>
                                                <tr id="rowOptions" runat="server">
                                                    <td valign="middle" id="colOptions" runat="server" colspan="13" style="border-width: 0px; width: 100%">
                                                        <div id="divRadioAndDataDivs" runat="server" clientidmode="Static" style="height: 24px; width: 100%; border: 1px Solid Silver; vertical-align: middle"
                                                            oninit="OptionsGrid_OnInit">
                                                            <div style="margin-top: 2px; float: left; width: 240px; vertical-align: bottom">
                                                                <asp:RadioButton ID="rdoLog" runat="server" Text="Log" GroupName="Group1" OnInit="OptionsGrid_OnInit"
                                                                    OnClick='<%# "EnableDisable(\"LOG\", " + Eval("Servicequeueitemid").ToString() + "); return true;" %>' />
                                                                <asp:RadioButton ID="rdoCancel" runat="server" Text="Cancel" GroupName="Group1" OnInit="OptionsGrid_OnInit"
                                                                    OnClick='<%# "EnableDisable(\"CANCEL\", " + Eval("Servicequeueitemid").ToString() + "); return true;" %>' />
                                                                <asp:RadioButton ID="rdoClose" runat="server" Text="Close" GroupName="Group1" OnInit="OptionsGrid_OnInit"
                                                                    OnClick='<%# "EnableDisable(\"CLOSE\", " + Eval("Servicequeueitemid").ToString() + "); return true;" %>' />
                                                                <asp:RadioButton ID="rdoManual" runat="server" Text="Manual" GroupName="Group1" OnInit="OptionsGrid_OnInit"
                                                                    OnClick='<%# "EnableDisable(\"MANUAL\", " + Eval("Servicequeueitemid").ToString() + "); return true;" %>' />
                                                            </div>
                                                            <div id="divOptionsPanel" runat="server" style="float: left; margin-left: 5px; height: 100%; width: 750px; vertical-align: bottom">
                                                                <div id="divLog" runat="server" clientidmode="Static" style="display: none; width: 750px" oninit="OptionsGrid_OnInit">
                                                                    <table id="tblArea" runat="server" border="0" style="margin-top: 1px; border: 0px; width: 100%; border-collapse: collapse">
                                                                        <tr>
                                                                            <td style="width: 90px; text-align: right; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; border-top-color: currentColor; border-right-color: currentColor; border-bottom-color: currentColor; border-left-color: currentColor; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none;">
                                                                                Start Time:
                                                                            </td>
                                                                            <td style="width: 100px; text-align: left; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; border-top-color: currentColor; border-right-color: currentColor; border-bottom-color: currentColor; border-left-color: currentColor; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none;">
                                                                                <telerik:RadTimePicker ID="dtInTime" runat="server" MaxDate='<%# DateTime.Now.Date.AddDays(1) %>'
                                                                                    DateInput-Font-Size="Smaller" Width="100%" Visible="true" OnInit="OptionsGrid_OnInit">
                                                                                </telerik:RadTimePicker>
                                                                                <%--<asp:RequiredFieldValidator ID="reqInTime" runat="server" CssClass="stdValidator" ClientIDMode="Static" 
                                                                                    ControlToValidate="dtInTime" ErrorMessage="*" InitialValue="true" 
                                                                                    OnInit="OptionsGrid_OnInit"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                            <td style="width: 80px; text-align: right; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; border-top-color: currentColor; border-right-color: currentColor; border-bottom-color: currentColor; border-left-color: currentColor; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none;">
                                                                                End Time:
                                                                            </td>
                                                                            <td style="width: 100px; text-align: left; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; border-top-color: currentColor; border-right-color: currentColor; border-bottom-color: currentColor; border-left-color: currentColor; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none;">
                                                                                <telerik:RadTimePicker ID="dtOutTime" runat="server" MaxDate='<%# DateTime.Now.Date.AddDays(1) %>'
                                                                                    DateInput-Font-Size="Smaller" Width="100%" Visible="true" OnInit="OptionsGrid_OnInit">
                                                                                </telerik:RadTimePicker>
                                                                            </td>
                                                                            <td style="width: 50px; text-align: right; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; border-top-color: currentColor; border-right-color: currentColor; border-bottom-color: currentColor; border-left-color: currentColor; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none;">
                                                                                Quantity:
                                                                            </td>
                                                                            <td style="width: 80px; text-align: left; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; border-top-color: currentColor; border-right-color: currentColor; border-bottom-color: currentColor; border-left-color: currentColor; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none;">
                                                                                <telerik:RadNumericTextBox ID="txtQty" runat="server" MinValue="0" NumberFormat-DecimalDigits="2"
                                                                                    SelectionOnFocus="SelectAll" Value="0" Width="100%" OnInit="OptionsGrid_OnInit">
                                                                                </telerik:RadNumericTextBox>
                                                                                <%--<asp:RequiredFieldValidator ID="reqOutTime" runat="server" CssClass="stdValidator" ClientIDMode="Static"
                                                                                    ControlToValidate="dtOutTime" ErrorMessage="*" InitialValue="true"
                                                                                    OnInit="OptionsGrid_OnInit"></asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                            <td style="width: 120px; text-align: right; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; border-top-color: currentColor; border-right-color: currentColor; border-bottom-color: currentColor; border-left-color: currentColor; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none;">
                                                                                Division Area:
                                                                            </td>
                                                                            <td style="width: 120px; text-align: left; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; border-top-color: currentColor; border-right-color: currentColor; border-bottom-color: currentColor; border-left-color: currentColor; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none;">
                                                                                <telerik:RadComboBox ID="cmbArea" runat="server" AppendDataBoundItems="true" Width="100%"
                                                                                    DataSourceID="LinqDataSource1" DataTextField="Description"
                                                                                    DataValueField="Divisionareaid" OnInit="OptionsGrid_OnInit">
                                                                                    <Items>
                                                                                        <telerik:RadComboBoxItem Text="" Value="" />
                                                                                    </Items>
                                                                                </telerik:RadComboBox>
                                                                                <%--<asp:Label ID="lblArea" runat="server" Text="*" ForeColor="Red"
                                                                                    Visible="false" Width="8px" OnInit="OptionsGrid_OnInit"></asp:Label>--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="divCancel" runat="server" clientidmode="Static" style="margin-top: 1px;
                                                                    width: 100%; display: none" oninit="OptionsGrid_OnInit">
                                                                    Reschedule Date:
                                                                    <telerik:RadDatePicker ID="dtRescheduleDate" runat="server"
                                                                        MinDate='<%# DateTime.Now.Date %>' MaxDate='<%# DateTime.Now.Date.AddDays(30) %>' DateInput-Font-Size="Smaller" Width="80px"
                                                                        Visible="true" OnInit="OptionsGrid_OnInit">
                                                                    </telerik:RadDatePicker>
                                                                    <%--<asp:RequiredFieldValidator ID="reqRescheduleDate" runat="server" CssClass="stdValidator" ClientIDMode="Static"
                                                                        ControlToValidate="dtRescheduleDate"ErrorMessage="*" InitialValue="true"
                                                                        OnInit="OptionsGrid_OnInit"></asp:RequiredFieldValidator>--%>
                                                                    Reason:
                                                                    <asp:TextBox ID="txtRescheduleReason" runat="server" Height="17px"
                                                                        Font-Size="Smaller" Width="60%" MaxLength="255" Visible="true" OnInit="OptionsGrid_OnInit"
                                                                        Text='<%# Eval("Notdonereason") %>'>
                                                                    </asp:TextBox>
                                                                    <%--<asp:RequiredFieldValidator ID="reqRescheduleReason" runat="server" CssClass="stdValidator" ClientIDMode="Static"
                                                                        ControlToValidate="txtRescheduleReason" ErrorMessage="*"
                                                                        InitialValue="true" OnInit="OptionsGrid_OnInit"></asp:RequiredFieldValidator>--%>
                                                                </div>
                                                                <div id="divClose" runat="server" clientidmode="Static" style="margin-top: 1px; width: 100%;
                                                                    display: none" oninit="OptionsGrid_OnInit">
                                                                    <div style="position: relative; float: left; height:24px; text-align:left; margin-top: 3px">Close Reason:&nbsp;</div>
                                                                    <asp:TextBox ID="txtCloseReason" runat="server" Height="17px"
                                                                        Font-Size="Smaller" Width="60%" MaxLength="255" Visible="true" OnInit="OptionsGrid_OnInit"
                                                                        Text='<%# Eval("Notdonereason") %>'>
                                                                    </asp:TextBox>
                                                                    <%--<asp:RequiredFieldValidator ID="reqCloseReason" runat="server" CssClass="stdValidator" ClientIDMode="Static"
                                                                        ControlToValidate="txtCloseReason" ErrorMessage="*" InitialValue="true"
                                                                        OnInit="OptionsGrid_OnInit"></asp:RequiredFieldValidator>--%>
                                                                </div>
                                                                <div id="divManual" runat="server" clientidmode="Static" style="margin-top: 4px;
                                                                    display: none" oninit="OptionsGrid_OnInit">
                                                                    <a id="refLog" href="#" onclick='<%# "openWin1(" + Eval("Servicescheduleid").ToString() + "," + Eval("Serviceid").ToString() + "," + Eval("Servicequeueitemid").ToString() + "); return false;" %>'>
                                                                        Log</a> <a id="refCancel" href="#" onclick='<%# "openWin2(" + Eval("Servicescheduleid").ToString() + "," + Eval("Servicequeueitemid").ToString() + "); return false;" %>'>
                                                                            Cancel</a> <a id="refClose" href="#" onclick='<%# "openWin3(" + Eval("Servicequeueitemid").ToString() + "); return false;" %>'>
                                                                                Close</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <ExportSettings FileName="DailyServiceQueue" OpenInNewWindow="true" ExportOnlyData="true"
                            IgnorePaging="true" HideStructureColumns="true">
                        </ExportSettings>
                        <ClientSettings AllowGroupExpandCollapse="false" ReorderColumnsOnClient="true" AllowDragToGroup="false"
                            AllowColumnsReorder="false" ClientEvents-OnRowDblClick="onDoubleClick">
                            <Selecting AllowRowSelect="true" />
                            <Scrolling FrozenColumnsCount="0" AllowScroll="True" EnableVirtualScrollPaging="false"
                                SaveScrollPosition="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="false" />
                    </telerik:RadGrid>
                    <telerik:GridDateTimeColumnEditor ID="InboundtimeEditor" runat="server" TextBoxStyle-Width="80px">
                    </telerik:GridDateTimeColumnEditor>
                    <telerik:GridDateTimeColumnEditor ID="OutboundtimeEditor" runat="server" TextBoxStyle-Width="80px">
                    </telerik:GridDateTimeColumnEditor>
                    <telerik:GridTextBoxColumnEditor ID="RampEditor" runat="server" TextBoxMaxLength="20" TextBoxStyle-Width="70">
                    </telerik:GridTextBoxColumnEditor>
                    </div>
            </div>
        </div>
    </div>
    <%--<srp:scriptreferenceprofiler id="ScriptReferenceProfiler1" runat="server" />--%>
</asp:Content>

