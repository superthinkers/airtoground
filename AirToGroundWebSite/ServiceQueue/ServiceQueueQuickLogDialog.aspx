﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ServiceQueueQuickLogDialog.aspx.cs"
    AsyncTimeout="5000" Inherits="ServiceQueue_ServiceQueueQuickLogDialog" Buffer="true"
    Strict="true" Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server" AsyncPostBackTimeout="600"
            SupportsPartialRendering="true" EnablePartialRendering="true"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin" ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="true" 
            RequestQueueSize="15" UpdatePanelsRenderMode="Inline" >
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnLog">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="lblCount" />
                        <telerik:AjaxUpdatedControl ControlID="lblError" />
                        <telerik:AjaxUpdatedControl ControlID="btnClose" />
                        <telerik:AjaxUpdatedControl ControlID="btnCancel" />
                        <telerik:AjaxUpdatedControl ControlID="reqTailFound" />
                        <telerik:AjaxUpdatedControl ControlID="reqServices" />
                        <telerik:AjaxUpdatedControl ControlID="txtTail" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function doComplete() {
                // Get a reference to the current RadWindow
                var oWnd = GetRadWindow();

                // Let's the calling form know the user is committing the changes, versus cancelling.
                var oArg = new Object();
                oArg.OK = "TRUE";

                // Quick validation and then close.
                if (true == true) {
                    oWnd.close(oArg);
                } else {
                    alert("All Values are Required");
                }
            }
            function doCancel() {
                // Get a reference to the current RadWindow
                var oWnd = GetRadWindow();

                // Let's the calling form know the user is cancelling.
                var oArg = new Object();
                oArg.OK = "FALSE";

                oWnd.close(oArg);
            }
        </script>
        <div id="mainDiv" runat="server" style="text-align: center; margin: 10px 10px 10px 10px; width: 700px; height: 550px">
            <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Quick Log"></asp:Label>
            <br />
            <br />
            <div style="position: relative; float: left; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 690px">
                <table style="border-collapse: separate; width: 80%">
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblCompanyLbl" runat="server" SkinID="InfoLabel" Text="Company:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCompany" runat="server" SkinID="InfoLabelBold" Text="Company"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblCustomerLbl" runat="server" SkinID="InfoLabel" Text="Customer:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCustomer" runat="server" SkinID="InfoLabelBold" Text="Customer"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblDateLbl" runat="server" SkinID="InfoLabel" Text="Queue Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblDate" runat="server" SkinID="InfoLabelBold" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: relative; float: left; text-align: left; height:400px; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 690px">
                <table style="border-collapse: separate; width: 98%">
                    <tr>
                        <td rowspan="7" valign="top" style="text-align: left; width: 50%">
                            <asp:Label ID="lblServices" runat="server" SkinID="InfoLabel" Text="Select One or More Services:"></asp:Label>
                            <telerik:RadListBox ID="lstServices" runat="server" DataKeyField="Serviceid" DataValueField="Serviceid"
                                DataTextField="Description" CssClass="lblSmall" CheckBoxes="true" SelectionMode="Multiple"
                                Width="100%" Height="380px" EnableViewState="true">
                            </telerik:RadListBox>
                            <asp:CustomValidator ID="reqServices" runat="server" CssClass="stdValidator" ErrorMessage="&nbsp;*"
                                ToolTip="You Must Select One or More Services" OnServerValidate="reqServices_OnServerValidate">
                            </asp:CustomValidator>
                        </td>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblTail" runat="server" SkinID="InfoLabel" Text="Tail:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadTextBox ID="txtTail" runat="server" MaxLength="30" Width="100px" SelectionOnFocus="SelectAll">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="reqTail" runat="server" ControlToValidate="txtTail"
                                CssClass="stdValidator" ErrorMessage="*">
                            </asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="reqTailFound" runat="server" ControlToValidate="txtTail"
                                CssClass="stdValidator" ToolTip="Tail Number Not Found" ErrorMessage="*" OnServerValidate="reqTailFound_OnServerValidate">
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblWorkDate" runat="server" SkinID="InfoLabel" Text="Work Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadDatePicker ID="txtWorkDate" runat="server">
                            </telerik:RadDatePicker>
                            <asp:RequiredFieldValidator ID="reqWorkDate" runat="server" ControlToValidate="txtWorkDate"
                                ErrorMessage="*" CssClass="stdValidator">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblStartTime" runat="server" SkinID="InfoLabel" Text="Start Time:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadTimePicker ID="dtStartTime" runat="server" Width="100px" TimePopupButton-Visible="false">
                            </telerik:RadTimePicker>
                            <%--<asp:RequiredFieldValidator ID="reqStartTime" runat="server" ControlToValidate="dtStartTime"
                                ErrorMessage="*" CssClass="stdValidator">
                            </asp:RequiredFieldValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblEndTime" runat="server" SkinID="InfoLabel" Text="End Time:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadTimePicker ID="dtEndTime" runat="server" Width="100px" TimePopupButton-Visible="false">
                            </telerik:RadTimePicker>
                            <%--<asp:RequiredFieldValidator ID="reqEndTime" runat="server" ControlToValidate="dtEndTime"
                                ErrorMessage="*" CssClass="stdValidator">
                            </asp:RequiredFieldValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblQuantity" runat="server" SkinID="InfoLabel" Text="Quantity:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadNumericTextBox ID="txtQuantity" runat="server" Width="75px"
                                MinValue="0" NumberFormat-DecimalDigits="2" SelectionOnFocus="SelectAll" Value="0">
                            </telerik:RadNumericTextBox>
                            <asp:RequiredFieldValidator ID="reqQuantity" runat="server" ControlToValidate="txtQuantity"
                                ErrorMessage="*" CssClass="stdValidator">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblArea" runat="server" SkinID="InfoLabel" Text="Division Area:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadComboBox ID="cmbArea" runat="server" AppendDataBoundItems="true" Width="150px" DataTextField="Description" DataValueField="Divisionareaid">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox>
                            <%--<asp:RequiredFieldValidator ID="reqArea" runat="server" ControlToValidate="cmbArea"
                                ErrorMessage="*" CssClass="stdValidator">
                            </asp:RequiredFieldValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblNotes" runat="server" SkinID="InfoLabel" Text="Notes:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadTextBox ID="txtNotes" runat="server" Width="98%" SelectionOnFocus="SelectAll" 
                                TextMode="MultiLine" Rows="4" Height="100px" MaxLength="255">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div style="text-align: center; width:100%">
                <telerik:RadButton ID="btnLog" runat="server" Text="Log" OnClick="btnLog_OnClick" />
                <asp:Label ID="lblCount" runat="server" ForeColor="Green" Text="Logged: 0" Visible="true"></asp:Label>
                <br />
                <br />
                <telerik:RadButton ID="btnClose" runat="server" Text="Close" Visible="false" OnClientClicked="doComplete" />
                <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClientClicked="doCancel" />
            </div>
            <br />
            <asp:Label ID="lblError" runat="server" CssClass="stdValidator" ForeColor="Red" Text="" Visible="true"></asp:Label>
        </div>
    </form>
</body>
</html>
