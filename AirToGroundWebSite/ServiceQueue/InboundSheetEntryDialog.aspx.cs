﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI;
using ATGDB;

public partial class ServiceQueue_InboundSheetEntryDialog : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_INBOUND_ENTRY;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);
        
        if (!Page.IsPostBack) {
            if ((Request.QueryString["compId"] != null) && (Request.QueryString["custId"] != null)) {
                // Clear the cache.
                Session.Add("INBOUND_SHEET_CACHE", "");
                Session.Add("INBOUND_SHEET_SVCQ_CACHE", "");

                string compId = Request.QueryString["compId"];
                string custId = Request.QueryString["custId"];
                   
                // Header summary.
                ATGDB.Companydivision cd = Companydivision.GetCompanyDivisionById(compId);
                ATGDB.Customer c = Customer.GetCustomerById(custId);
                lblCompany.Text = cd.Description;
                lblCustomer.Text = c.Description;
                DateTime dt = DateTime.Parse(Request.QueryString["queueDt"]);
                lblDate.Text = dt.ToShortDateString();

                // Build the grid.
                BuildGrid(compId, custId);
                // Bind.
                RadGrid1.DataBind();
            }
        } else {
            string compId = Request.QueryString["compId"];
            string custId = Request.QueryString["custId"];
            // Build the grid.
            BuildGrid(compId, custId);
            // Bind.
            RadGrid1.DataBind();
        }
    }
    protected void btnSearch_OnClick(object sender, EventArgs e) {
        lblSearchError.Visible = false;
        if ((txtTail.Text != null) && (txtTail.Text.Length > 0)) {
            DataSet ds = (DataSet)Session["INBOUND_SHEET_CACHE"];
            DataRow row = ds.Tables[0].Rows.Find(txtTail.Text);
            if (row != null) {
                // Get the row index.
                double idx = ds.Tables[0].Rows.IndexOf(row);
                // Determine the  grid page.
                double page = Math.Floor(idx / 10d);
                // Set the page.
                RadGrid1.MasterTableView.CurrentPageIndex = (int)page;
                // Refresh.
                RadGrid1.Rebind();
            } else {
                lblSearchError.Visible = true;
            }
        }
    }
    protected void BuildGrid(string compId, string custId) {
        ATGDataContext dc = new ATGDataContext();

        // Get all tails for customer, in order.
        var tails = from t in dc.Equipments
                    where t.Customerid == int.Parse(custId)
                    orderby t.Tailnumber ascending
                    select t;

        if (tails.Count() > 0) {
            // Get today's service queue. It has all existing SS in it for this CompDiv and Customer.
            var svcQ = Servicequeue.GetSynchronizedServiceQueueItemsByCompCustAndDate(compId + ":" + custId, DateTime.Now.Date, false, false);
            Session.Add("INBOUND_SHEET_SVCQ_CACHE", svcQ);
    
            // Now, we need a list of all services for the Customer.
            var svcs = from svc in dc.Services
                       where svc.Customerid == int.Parse(custId)
                       where svc.Showinservicequeue == true
                       orderby svc.Sequence, svc.Serviceinterval, svc.Description
                       select svc;

            if (svcs.Count() > 0) {
                // Now we have everything we need to build a cross-tab grid of data.

                // Create the columns first for each service.
                RadGrid1.MasterTableView.Columns.Clear();
                // The tail                   
                RadGrid1.MasterTableView.Columns.Add(GetSimpleBoundCol("Tailnumber", "Tail"));
                // Each service
                foreach (ATGDB.Service svc in svcs) {
                    RadGrid1.MasterTableView.Columns.Add(GetCheckBoxTemplateCol("Svc" + svc.Serviceid.ToString(), svc.Serviceshortdescr.Description, svc.Description));
                }
                // Inbound time, outbound time, ramp, spare
                RadGrid1.MasterTableView.Columns.Add(GetDateTimeTemplateCol("Inboundtime", "Inbound Time"));
                RadGrid1.MasterTableView.Columns.Add(GetDateTimeTemplateCol("Outboundtime", "Outbound Time"));
                RadGrid1.MasterTableView.Columns.Add(GetTextBoxTemplateCol("Ramp", "Ramp Location"));
                RadGrid1.MasterTableView.Columns.Add(GetSimpleCheckBoxTemplateCol("Isspare", "Is Spare?", "Is this a Spare?"));

                DataSet ds = null;
                if (Session["INBOUND_SHEET_CACHE"].ToString() == "") {
                    // Now, build a dataset we can bind to.
                    ds = new DataSet("MySheetDS");
                    DataTable tbl = new DataTable("MySheetDT");
                    ds.Tables.Add(tbl);
                    // The tail
                    DataColumn col = tbl.Columns.Add("Tailnumber", typeof(string));
                    // Each service
                    foreach (ATGDB.Service svc in svcs) {
                        tbl.Columns.Add("Svc" + svc.Serviceid.ToString(), typeof(string));
                    }
                    // Inbound time, outbound time, ramp, spare
                    tbl.Columns.Add("Inboundtime", typeof(DateTime));
                    tbl.Columns.Add("Outboundtime", typeof(DateTime));
                    tbl.Columns.Add("Ramp", typeof(string));
                    tbl.Columns.Add("Isspare", typeof(Boolean));
                    // Add primary key for later.
                    tbl.PrimaryKey = new DataColumn[] { col };

                    // Add the rows for every tail
                    foreach (ATGDB.Equipment e in tails) {
                        // For every tail, add a row.

                        // Build the column array
                        object[] cols = new object[1 + svcs.Count() + 4];

                        // Fill it up.
                        // The tail
                        cols[0] = e.Tailnumber;
                        // Each service
                        int cnt = 0;
                        foreach (ATGDB.Service svc in svcs) {
                            cnt++;

                            // For this tail, and service, is one or more scheduled?
                            var sq = svcQ.Where(x => x.Tailnumber == e.Tailnumber && x.Serviceid == svc.Serviceid);
                            bool hasOne = false;
                            if (sq.Count() == 1) {
                                hasOne = sq.ElementAt(0).Closeddate == null;
                            } else if (sq.Count() > 1) {
                                foreach (ATGDB.Servicequeue.ServiceQueueItemResult q in sq) {
                                    hasOne = q.Closeddate == null;
                                    if (hasOne == true) {
                                        break;
                                    }
                                }
                            }
                            cols[cnt] = hasOne;// svc.Shortdescription;
                        }
                        // Inbound time, outbound time, ramp, spare
                        cols[cnt + 1] = null;
                        cols[cnt + 2] = null;
                        cols[cnt + 3] = "";
                        cols[cnt + 4] = false;

                        // Add it.
                        tbl.Rows.Add(cols);
                    }
                    // Cache it.
                    Session.Add("INBOUND_SHEET_CACHE", ds);
                } else {
                    // Get from cache.
                    ds = (DataSet)Session["INBOUND_SHEET_CACHE"];
                }
                // Finally, bind.
                RadGrid1.DataSource = ds;
            }
        }

    }
    protected GridBoundColumn GetSimpleBoundCol(string sDataField, string sHdr) {
        GridBoundColumn col = new GridBoundColumn();
        col.DataField = sDataField;
        col.UniqueName = sDataField + "Column";
        col.HeaderText = sHdr;
        col.HeaderTooltip = " ";
        col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        col.HeaderStyle.Width = new Unit("60px");
        col.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        col.ItemStyle.Width = new Unit("60px");
        return col;
    }
    class CustomCheckBoxTemplate : System.Web.UI.IBindableTemplate
    {
        CheckBox _chk;    
        public string Id { get; set; }
        public string Text { get; set; }
        public string ToolTip { get; set; }
        public string Tail { get; set; }
        public StateBag ViewState { get; set; }
        const string CHECK_ENABLE_DISABLE_STATUS = "CHECK_ENABLE_DISABLE_STATUS";
        public CustomCheckBoxTemplate(string id, string text, string toolTip, StateBag viewState) {
            Id = id;
            Text = text;
            ToolTip = toolTip;
            Tail = "";
            ViewState = viewState;
            ViewState[CHECK_ENABLE_DISABLE_STATUS + Tail + Id] = null;
        }
        public void InstantiateIn(System.Web.UI.Control container) {
            _chk = new CheckBox();
            _chk.AutoPostBack = false;
            _chk.ClientIDMode = ClientIDMode.Static;
            // Customize the STATIC Control ID based on the TAIL
            GridDataItem item = (GridDataItem)container.NamingContainer;
            string tail = DataBinder.Eval(item.DataItem, "Tailnumber").ToString();
            _chk.ID = tail + Id;
            
            _chk.Text = Text;
            _chk.ToolTip = ToolTip; // THERE IS A CHANCE THIS IS CAUSING PERFORMANCE PROBLEMS.
            _chk.Enabled = true;
            _chk.DataBinding += OnDataBinding;
            container.Controls.Add(_chk); 
        }
        protected void OnDataBinding(object sender, EventArgs e) {
            CheckBox chk = sender as CheckBox;
            GridDataItem item = (GridDataItem)chk.NamingContainer;
            chk.Checked = Boolean.Parse(DataBinder.Eval(item.DataItem, Id).ToString());

            // Get a reference to the datarow for performance reasons.
            Tail = DataBinder.Eval(item.DataItem, "Tailnumber").ToString();
            _chk.Attributes.Add("onclick", "return doCheckChanged('" + Tail + "','" + Id + "')");
            
            // Disable services already set up.
            // This weird logic is all about performance.
            if ((chk.ID.Contains("Svc")) && (ViewState[CHECK_ENABLE_DISABLE_STATUS + Tail + Id] == null) /*&& (chk.Checked == true) && (chk.Enabled == true) && (chk.ID.Contains("Svc"))*/) {
                // Get the current ServiceQueue cache.
                var svcQ = (List<ATGDB.Servicequeue.ServiceQueueItemResult>)System.Web.HttpContext.Current.Session["INBOUND_SHEET_SVCQ_CACHE"];
                // Does this checkbox svc already exist?
                int svcId = int.Parse(chk.ID.Substring(Tail.Length + 3, chk.ID.Length - Tail.Length - 3));
                var svcQues = svcQ.Where(x => x.Tailnumber == Tail && x.Serviceid == svcId && x.Closeddate == null);
                chk.Enabled = svcQues.Count() == 0;
                if (chk.Enabled == false) {
                    ViewState[CHECK_ENABLE_DISABLE_STATUS + Tail + Id] = "1"; // Disabled.
                    chk.ControlStyle.BackColor = System.Drawing.Color.Silver;
                    TableCell cell = (TableCell)chk.Parent;
                    cell.BackColor = System.Drawing.Color.Silver;
                } else {
                    ViewState[CHECK_ENABLE_DISABLE_STATUS + Tail + Id] = "0"; // Enabled.
                }
            } else if ((chk.ID.Contains("Svc")) && (ViewState[CHECK_ENABLE_DISABLE_STATUS + Tail + Id].ToString() == "0")) {
                // Nothing - Enabled.
            } else if ((chk.ID.Contains("Svc")) && (ViewState[CHECK_ENABLE_DISABLE_STATUS + Tail + Id].ToString() == "1")) {
                chk.Enabled = false;
                chk.ControlStyle.BackColor = System.Drawing.Color.Silver;
                TableCell cell = (TableCell)chk.Parent;
                cell.BackColor = System.Drawing.Color.Silver;
            }
        }
        IOrderedDictionary IBindableTemplate.ExtractValues(Control container) {
            OrderedDictionary oDic;
            oDic = new OrderedDictionary();
            oDic.Add(_chk.ID, _chk.Checked);
            return oDic;
        }
    }
    protected GridTemplateColumn GetCheckBoxTemplateCol(string id, string sHdr, string sToolTip) {
        GridTemplateColumn col = new GridTemplateColumn();
        col.DataField = id;
        col.DataType = typeof(bool);
        col.UniqueName = id + "Column";
        col.HeaderText = sHdr;
        col.HeaderTooltip = sToolTip;                    
        col.ItemTemplate = new CustomCheckBoxTemplate(id, sHdr, sToolTip, ViewState);
        col.HeaderStyle.Wrap = false;
        col.HeaderStyle.Width = new Unit("90px");
        col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        col.ItemStyle.Width = new Unit("90px");
        col.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        col.ItemStyle.VerticalAlign = VerticalAlign.Middle;
        col.ItemStyle.ForeColor = System.Drawing.Color.Green;
        return col;
    }
    protected GridTemplateColumn GetSimpleCheckBoxTemplateCol(string id, string sHdr, string sToolTip) {
        GridTemplateColumn col = new GridTemplateColumn();
        col.DataField = id;
        col.DataType = typeof(bool);
        col.UniqueName = id + "Column";
        col.HeaderText = sHdr;
        col.HeaderTooltip = sToolTip;
        col.ItemTemplate = new CustomCheckBoxTemplate(id, "", sToolTip, ViewState);
        col.HeaderStyle.Wrap = false;
        col.HeaderStyle.Width = new Unit("60px");
        col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        col.ItemStyle.Width = new Unit("60px");
        col.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        return col;
    }
    class CustomDateTimeTemplate : System.Web.UI.IBindableTemplate
    {
        RadTimePicker _time;
        public string Id { get; set; }
        public string Header { get; set; }
        public CustomDateTimeTemplate(string id, string hdr) {
            this.Id = id;
            this.Header = hdr;
        }
        public void InstantiateIn(System.Web.UI.Control container) {
            _time = new RadTimePicker();
            _time.ID = Id;
            _time.AutoPostBack = true;
            _time.MinDate = DateTime.Now.Date;
            _time.MaxDate = DateTime.Now.Date.AddDays(1);
            _time.Width = new Unit("98%");
            _time.SelectedDateChanged += OnSelectedDateChanged;
            _time.DataBinding += OnDataBinding;
            container.Controls.Add(_time);
        }
        protected void OnSelectedDateChanged(object sender, EventArgs e) {
            RadTimePicker time = (sender as RadTimePicker);
            GridDataItem item = (GridDataItem)time.NamingContainer;
            TableCell tailCell = item["TailnumberColumn"];
            DataSet ds = (DataSet)System.Web.HttpContext.Current.Session["INBOUND_SHEET_CACHE"];
            DataRow dr = ds.Tables[0].Rows.Find(tailCell.Text);
            dr[time.ID] = time.SelectedDate;
        }
        protected void OnDataBinding(object sender, EventArgs e) {
            RadTimePicker time = sender as RadTimePicker;
            GridDataItem item = (GridDataItem)time.NamingContainer;
            if (DataBinder.Eval(item.DataItem, Id).ToString() != "") {
                time.SelectedDate = DateTime.Parse(DataBinder.Eval(item.DataItem, Id).ToString());
            }
        }
        IOrderedDictionary IBindableTemplate.ExtractValues(Control container) {
            OrderedDictionary oDic;
            oDic = new OrderedDictionary();
            oDic.Add(_time.ID, _time.SelectedDate);
            return oDic;
        }
    }
    protected GridTemplateColumn GetDateTimeTemplateCol(string id, string sHdr) {
        GridTemplateColumn col = new GridTemplateColumn();
        col.DataField = id;
        col.DataType = typeof(string);
        col.UniqueName = id + "Column";
        col.HeaderText = sHdr;
        col.ItemTemplate = new CustomDateTimeTemplate(id, sHdr);
        col.HeaderStyle.Wrap = false;
        col.HeaderStyle.Width = new Unit("100px");
        col.ItemStyle.Width = new Unit("100px");
        return col;
    }
    class CustomTextBoxTemplate : System.Web.UI.IBindableTemplate
    {
        RadTextBox _txt;
        public string Id { get; set; }
        public string Header { get; set; }
        public CustomTextBoxTemplate(string id, string hdr) {
            this.Id = id;
            this.Header = hdr;
        }
        public void InstantiateIn(System.Web.UI.Control container) {
            _txt = new RadTextBox();
            _txt.ID = Id;
            _txt.AutoPostBack = true;
            _txt.MaxLength = 20;
            _txt.Width = new Unit("98%");
            _txt.TextChanged += OnTextChanged;
            _txt.DataBinding += OnDataBinding;
            container.Controls.Add(_txt);
        }
        protected void OnTextChanged(object sender, EventArgs e) {
            RadTextBox txt = (sender as RadTextBox);
            GridDataItem item = (GridDataItem)txt.NamingContainer;
            TableCell tailCell = item["TailnumberColumn"];
            DataSet ds = (DataSet)System.Web.HttpContext.Current.Session["INBOUND_SHEET_CACHE"];
            DataRow dr = ds.Tables[0].Rows.Find(tailCell.Text);
            dr[txt.ID] = txt.Text;
        }
        protected void OnDataBinding(object sender, EventArgs e) {
            RadTextBox txt = sender as RadTextBox;
            GridDataItem item = (GridDataItem)txt.NamingContainer;
            txt.Text = DataBinder.Eval(item.DataItem, Id).ToString();
        }
        IOrderedDictionary IBindableTemplate.ExtractValues(Control container) {
            OrderedDictionary oDic;
            oDic = new OrderedDictionary();
            oDic.Add(_txt.ID, _txt.Text);
            return oDic;
        }
    }
    protected GridTemplateColumn GetTextBoxTemplateCol(string id, string sHdr) {
        GridTemplateColumn col = new GridTemplateColumn();
        col.DataField = id;
        col.DataType = typeof(string);
        col.UniqueName = id + "Column";
        col.HeaderText = sHdr;
        col.ItemTemplate = new CustomTextBoxTemplate(id, sHdr);
        col.HeaderStyle.Wrap = false;
        col.HeaderStyle.Width = new Unit("85px");
        col.ItemStyle.Width = new Unit("85px");
        return col;
    }
    protected void btnCreate_OnClick(object sender, EventArgs e) {
        // Get the company and customer.
        int compId = int.Parse(Request.QueryString["compId"]);
        int custId = int.Parse(Request.QueryString["custId"]);
        int svcQueueId = int.Parse(Request.QueryString["serviceQueueId"]);
        
        // Get the user's data.
        DataSet ds = (DataSet)Session["INBOUND_SHEET_CACHE"]; 

        // Get the current ServiceQueue cache.
        var svcQ = (List<ATGDB.Servicequeue.ServiceQueueItemResult>)System.Web.HttpContext.Current.Session["INBOUND_SHEET_SVCQ_CACHE"];

        // For each tail, and each service column, create a schedule item.
        string tail;
        int eqId;
        int svcId;
        bool canCreate = false;
        DateTime? inTime = null;
        DateTime? outTime = null;
        string ramp = "";
        bool isSpare = false;
        foreach (DataRow row in ds.Tables[0].Rows) {
            // Get the tail and equipment.
            tail = row[0].ToString(); // Always column 0.
            eqId = ATGDB.Equipment.GetEquipmentIdByTailNumber(custId, tail);

            // For each service column, create a new service schedule record.
            foreach (DataColumn col in row.Table.Columns) {
                if (col.Caption.Contains("Svc")) {
                    if (row[col].ToString() == "True") {
                        // It was checked. See if we can create it (e.g. not already there).
                        svcId = int.Parse(col.Caption.Substring(3, col.Caption.Length - 3));
                        var svcQues = svcQ.Where(x => x.Closeddate == null && x.Serviceid == svcId && x.Tailnumber == tail);
                        canCreate = svcQues.Count() == 0;
                        if (canCreate == true) {
                            if (row["Inboundtime"].ToString() != "") {
                                inTime = DateTime.Parse(row["Inboundtime"].ToString());
                            } else {
                                inTime = null;
                            }
                            if (row["Outboundtime"].ToString() != "") {
                                outTime = DateTime.Parse(row["Outboundtime"].ToString());
                            } else {
                                outTime = null;
                            }
                            ramp = row["Ramp"].ToString();
                            isSpare = Boolean.Parse(row["Isspare"].ToString());

                            DateTime dt = DateTime.Parse(Request.QueryString["queueDt"]);

                            // Insert the service schedule.
                            int ssId = ATGDB.Serviceschedule.InsertServiceSchedule(compId, eqId, svcId, dt, inTime, "");

                            // Now create the Servicequeue.
                            ATGDB.Servicequeue.InsertServiceQueueItem(svcQueueId, compId, custId, ssId, dt, inTime, outTime, ramp, isSpare);
                        }
                    }
                }
            }
        }
        btnCreate.Visible = false;
        btnCancel.Visible = false;
        btnClose.Visible = true;
        lblDone.Visible = true;
        
        // Clear the cache. Makes screen refresh grey areas upon rebind.
        Session.Add("INBOUND_SHEET_CACHE", "");
        Session.Add("INBOUND_SHEET_SVCQ_CACHE", "");

        // Triggers refresh in the ServiceQueue screen.
        Session.Add("REFRESH_VIEW_SCHEDULES", "REFRESH");

        // Rebuild the grid to refresh the UI.
        BuildGrid(compId.ToString(), custId.ToString());
        // Bind.
        RadGrid1.DataBind();
        RadGrid1.Enabled = false;
    }

}