﻿using System;
using System.Linq;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using ATGDB;

public partial class ServiceQueue_InboundSheetSingleEntryDialog : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_INBOUND_SINGLE_ENTRY;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);
        
        if (!Page.IsPostBack) {
            if ((Request.QueryString["compId"] != null) && (Request.QueryString["custId"] != null)) {
                string compId = Request.QueryString["compId"];
                string custId = Request.QueryString["custId"];
                   
                // Header summary.
                ATGDB.Companydivision cd = Companydivision.GetCompanyDivisionById(compId);
                ATGDB.Customer c = Customer.GetCustomerById(custId);
                lblCompany.Text = cd.Description;
                lblCustomer.Text = c.Description;
                DateTime dt = DateTime.Parse(Request.QueryString["queueDt"]);
                lblDate.Text = dt.ToShortDateString();

                // Services
                var data = ATGDB.Service.GetServiceQueueServicesByCustomerId(int.Parse(custId));
                lstServices.DataSource = data;
                lstServices.DataBind();

                //txtTail.Focus();
            }
        }
    }
    protected void reqServices_OnServerValidate(object sender, ServerValidateEventArgs e) {
        bool hasServices = lstServices.CheckedItems.Count > 0;
        e.IsValid = hasServices;
    }
    protected void reqTailFound_OnServerValidate(object sender, ServerValidateEventArgs e) {
        string tail = txtTail.Text;
        int custId = int.Parse(Request.QueryString["custId"]);
        int eqId = ATGDB.Equipment.GetEquipmentIdByTailNumber(custId, tail);
        e.IsValid = eqId > 0;
    }
    protected void btnAdd_OnClick(object sender, EventArgs e) {
        if (lstServices.CheckedItems.Count > 0) {
            // Add the ServiceQueueItem
            string tail = txtTail.Text;
            DateTime? dtInbound = dtInboundTime.SelectedDate;
            DateTime? dtOutbound = dtOutboundTime.SelectedDate;
            string ramp = txtRamp.Text;
            bool isSpare = chkSpare.Checked;
            int compId = int.Parse(Request.QueryString["compId"]);
            int custId = int.Parse(Request.QueryString["custId"]);
            int sqId = int.Parse(Request.QueryString["serviceQueueId"]);
            DateTime qdt = DateTime.Parse(Request.QueryString["queueDt"]);
            DateTime dt = txtSvcDate.SelectedDate.Value.Date;
            int eqId = ATGDB.Equipment.GetEquipmentIdByTailNumber(custId, tail);

            // Add a ServiceQueueItem for each selected service.
            // Validator ensures at least one is checked.
            foreach (RadListBoxItem item in lstServices.CheckedItems) {
                // Insert the service schedule.
                int ssId = ATGDB.Serviceschedule.InsertServiceSchedule(compId, eqId, int.Parse(item.Value), dt, dtInbound, "");
                // Insert the ServiceQueueItem
                ATGDB.Servicequeue.InsertServiceQueueItem(sqId, compId, custId, ssId, qdt, dtInbound, dtOutbound, ramp, isSpare);

                // Update counter.
                int cnt = int.Parse(lblCount.Text.Substring(7, lblCount.Text.Length - 7));
                lblCount.Text = "Added: " + (cnt + 1).ToString();
            }


            // Closing now, not canceling.
            btnClose.Visible = true;
            btnCancel.Visible = false;

            // Focus at beginning.
            //txtTail.Focus();

            // Triggers refresh in the ServiceQueue screen.
            Session.Add("REFRESH_VIEW_SCHEDULES", "REFRESH");
        }
    }

}