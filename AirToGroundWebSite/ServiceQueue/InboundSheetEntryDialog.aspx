﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InboundSheetEntryDialog.aspx.cs"
    AsyncTimeout="5000" Inherits="ServiceQueue_InboundSheetEntryDialog" Buffer="true"
    Strict="true" Explicit="true" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server" AsyncPostBackTimeout="600"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin"
        ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="true" 
            RequestQueueSize="15" UpdatePanelsRenderMode="Inline" >
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadGrid1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                        <telerik:AjaxUpdatedControl ControlID="lblSearchError" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function doComplete() {
                // Get a reference to the current RadWindow
                var oWnd = GetRadWindow();

                // Let's the calling form know the user is committing the changes, versus cancelling.
                var oArg = new Object();
                oArg.OK = "TRUE";

                // Quick validation and then close.
                if (true == true) {
                    oWnd.close(oArg);
                } else {
                    alert("All Values are Required");
                }
            }
            function doCancel() {
                // Get a reference to the current RadWindow
                var oWnd = GetRadWindow();

                // Let's the calling form know the user is cancelling.
                var oArg = new Object();
                oArg.OK = "FALSE";

                oWnd.close(oArg);
            }
            function doCheckChanged(tail, id) {
                var chk = $get(tail + id);
                //alert("check changed : " + tail + ":" + id + ":" + chk.checked);
                var loc = "IBS_Updater.aspx";
                var args = '"tail":"' + tail + '","id":"' + id + '","isChecked":"' + chk.checked + '"';
                $.ajax({
                    type: "POST",
                    url: loc + "/UpdateRecord",
                    data: "{" + args + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        // Replace the div's content with the page method's return.
                        //alert(msg.d);
                    },
                    fail: function (msg) {
                        // Replace the div's content with the page method's return.
                        //alert(msg.d);
                    }
                });
            }
        </script>
        <div id="mainDiv" runat="server" style="text-align: center; margin: 10px 10px 10px 10px; width: 600px; height: 600px">
            <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Inbound Sheet Data Entry"></asp:Label>
            <br />
            <br />
            <div style="position: relative; float: left; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 590px">
                <table style="border-collapse: separate; width: 80%">
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblCompanyLbl" runat="server" SkinID="InfoLabel" Text="Company:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCompany" runat="server" SkinID="InfoLabelBold" Text="Company"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblCustomerLbl" runat="server" SkinID="InfoLabel" Text="Customer:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCustomer" runat="server" SkinID="InfoLabelBold" Text="Customer"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblDateLbl" runat="server" SkinID="InfoLabel" Text="Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblDate" runat="server" SkinID="InfoLabelBold" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 17%">
                            <asp:Label ID="lblTail" runat="server" SkinID="InfoLabel" Text="Search For Tail:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadTextBox ID="txtTail" runat="server" MaxLength="15" Width="100px">
                            </telerik:RadTextBox>
                            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_OnClick" />
                            <asp:Label ID="lblSearchError" runat="server" SkinID="SubscriptLabel" ForeColor="Red"
                                Text="Tail Number Not Found" Visible="false"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: relative; float: left; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 590px">
                <telerik:RadGrid ID="RadGrid1" Width="100%" runat="server" 
                    GridLines="None" AllowAutomaticDeletes="false" AllowAutomaticInserts="false" Height="400px"
                    AllowMultiRowEdit="false" AllowMultiRowSelection="false" AllowAutomaticUpdates="false"
                    ShowGroupPanel="false" AllowPaging="true" PageSize="10" AllowSorting="false" AutoGenerateColumns="false">
                    <MasterTableView Width="100%" CommandItemDisplay="None" DataKeyNames="Tailnumber"
                        AllowAutomaticUpdates="false" NoMasterRecordsText="No Tail Numbers for this Customer"
                        TableLayout="Fixed" AutoGenerateColumns="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                        <AlternatingItemStyle BackColor="Khaki" />
                        <PagerStyle AlwaysVisible="true" Mode="NumericPages" Position="Bottom" />
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling FrozenColumnsCount="1" AllowScroll="True" EnableVirtualScrollPaging="False"
                            SaveScrollPosition="True" UseStaticHeaders="True"  />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
            <br />
            <div style="text-align: center; width:100%">
                <asp:Label ID="lblDone" runat="server" ForeColor="Green" Text="Selected Services Created" Visible="false"></asp:Label>
                <br />
                <telerik:RadButton ID="btnCreate" runat="server" Text="Create Selected Services"
                    OnClick="btnCreate_OnClick" />
                <telerik:RadButton ID="btnClose" runat="server" Text="Close" Visible="false" OnClientClicked="doComplete" />
                <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClientClicked="doCancel" />
            </div>
        </div>
        <%--<srp:ScriptReferenceProfiler ID="ScriptReferenceProfiler1" runat="server" />--%>
    </form>
</body>
</html>
