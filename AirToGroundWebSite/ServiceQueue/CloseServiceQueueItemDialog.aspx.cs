﻿using System;
using System.Linq;
using ATGDB;

public partial class ServiceQueue_CloseServiceQueueItemDialog : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_CLOSE_SQITEM;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);

        if (!Page.IsPostBack) {
            if (Request.QueryString["sqitemId"] != null) {
                hiddenSQItemId.Value = Request.QueryString["sqitemId"];

                // Get ServiceQueueItem
                ATGDataContext dc = new ATGDataContext();
                ATGDB.Servicequeueitem sqi = dc.Servicequeueitems.SingleOrDefault(x => x.Servicequeueitemid == int.Parse(hiddenSQItemId.Value));

                // Header summary. Get Service Schedule.
                int ssId = sqi.Servicescheduleid;
                ATGDB.Serviceschedule ss = Serviceschedule.GetServiceSchedule(ssId);
                lblCustomer.Text = ss.Service.Customer.Description;
                lblTailNumber.Text = ss.Equipment.Tailnumber;
                lblServiceDescr.Text = ss.Service.Description;
                lblServiceDate.Text = ss.Servicedate.ToShortDateString();
                lblClosedDate1.Text = DateTime.Now.Date.ToShortDateString();
            }
        }
    }
}