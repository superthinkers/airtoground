﻿using System;
using System.Web.Security;
using System.Collections.Generic;
using System.Security;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_LoadComplete(object sender, EventArgs e) {
        // Populate the login controls.
        System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)PageUtils.FindControlRecursive(this, "RememberMe");
        System.Web.UI.WebControls.TextBox txtPassword = (System.Web.UI.WebControls.TextBox)PageUtils.FindControlRecursive(this, "Password");
        Login1.UserName = SecurityUtils.GetLoginUserName(chk.Checked);
        txtPassword.Attributes.Add("value", SecurityUtils.GetLoginPassWord(chk.Checked));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // Hook LoadComplete event.
        Page.LoadComplete += Page_LoadComplete;
            
        if (!Page.IsPostBack) {
        }

        // Footer.
        txtFooter.Text = Resources.Resource.WebSiteFooterText;
    }
    protected void ResetProviderRoot() {
        // This is the CORP.
        SecurityUtils.SetATGMode(SecurityUtils.ATG_MODE_CORP);

        // Set the login's membership provider here.
        string providerRoot = "ATG";
        SecurityHelper.Instance.ProviderRoot = providerRoot;
    }
    protected void Login1_LoggingIn(object sender, System.Web.UI.WebControls.LoginCancelEventArgs e) {
        // Set up some application classes. NO SESSION YET.
        ResetProviderRoot();          
    }
    protected void Login1_OnLoggedIn(object sender, EventArgs e) {
        // Set up some application classes. HAVE SESSION
        ResetProviderRoot();

        // You must have an administrator.
        //MembershipHelper.CreateAdministrator();
 
        // Create the "remember me" cookie.
        System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)PageUtils.FindControlRecursive(this, "RememberMe");
        SecurityUtils.SetLoginCookie(chk.Checked, Login1.UserName, Login1.Password);

        // Initialize the profile.
        bool needsSave = false;
        Profile.Initialize(Login1.UserName, true);

        // Update profile in case the provider root gets messed up.
        Profile.ProviderRoot = "ATG";
        Profile.SiteTheme = "Corp";
        Profile.Save();

        // Utility method to fix all profile userids.
        //MembershipHelper.UpdateAllUserIds(Profile);

        // This is a security check to prevent a user from another site (UPS, FedEx) from logging into this site.
        if (!String.IsNullOrEmpty(Profile.ProviderRoot) && (Profile.ProviderRoot != SecurityHelper.Instance.ProviderRoot)) {
            // Someone is logging into the wrong site with the wrong credentials.
            // No exceptions, signout.
            //FormsAuthentication.GetAuthCookie("", true).Expires = DateTime.Now;
            FormsAuthentication.SignOut();
            // Redirect to customer site.
            Response.Redirect("~/Default.aspx", false);
            return;
        } else if (String.IsNullOrEmpty(Profile.ProviderRoot)) {
            needsSave = true;
            Profile.ProviderRoot = SecurityHelper.Instance.ProviderRoot;
        }

        // Update the site theme. Stop-gap measure to prevent possible problems.
        if (Profile.SiteTheme != SecurityUtils.GetSiteThemeFromProviderRoot(SecurityHelper.Instance.ProviderRoot)) {
            needsSave = true;
            Profile.SiteTheme = SecurityUtils.GetSiteThemeFromProviderRoot(SecurityHelper.Instance.ProviderRoot);
        }
         
        // Make sure there is a default company and customer, if possible.
        if (String.IsNullOrEmpty(Profile.Companydivisionid) || String.IsNullOrEmpty(Profile.Customerid)) {
            List<ATGDB.Companycustomeruserjoin> data = ATGDB.Companydivision.GetCompanyCustomerUserJoins(Profile.Userid, true);
            if (data.Count > 0) {
                // They have one or more assignments, so set it to the one at the top of the list.
                Profile.Companydivisionid = data[0].Companydivisionid.ToString();
                Profile.Companydivisiondescription = data[0].Companydivision.Description;
                Profile.Customerid = data[0].Customerid.ToString();
                Profile.Customerdescription = data[0].Customer.Description;
            } else {
                // Users must have a default division and customer. Administrators can always log in.
                if (Profile.UserName != "Administrator") {
                    if (needsSave == true) {
                        Profile.Save();
                    }
                    // No exceptions, signout.
                    FormsAuthentication.SignOut();
                    // Send to error page.
                    Response.Redirect("~/Security/NoDefaults.aspx", false);
                    return;
                } else {
                    Profile.Customerid = "3"; // FedEx
                    Profile.Customerdescription = "Federal Express (FedEx)";
                    Profile.Companydivisionid = "8"; // Memphis
                    Profile.Companydivisiondescription = "Memphis (MEM)";
                    Profile.Save();
                }
            }
        }

        // Update the profile
        if (needsSave == true) {
            Profile.Save();
        }

        // All users of this location will have the CUSTOMER ROLE.
        if (SecurityUtils.GetATGMode() == SecurityUtils.ATG_MODE_CORP) {
            SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_CORP, Profile.UserName);
            ProviderUtils.RemoveUsersFromRoles(new string[] { Profile.UserName }, new string[] { SecurityUtils.ROLE_CUSTOMER });
        } else {
            SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_CUSTOMER, Profile.UserName);
            ProviderUtils.RemoveUsersFromRoles(new string[] { Profile.UserName }, new string[] { SecurityUtils.ROLE_CORP });
        }

        // Auto-create some roles. After ran the first time, you can comment these out.
		SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_IMPORT_MANAGER, "Administrator");
		//SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_RPT_JOB);
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_EXP_JOBCODES);
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_CUSTOMER_NONROUTINE_RATE);
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_DELETE_LOGGED_SS);
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_WORKLOG_RATE);
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_TIMESHEETS, "Administrator");
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_TIMESHEETS, "scheeks");
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_TIMESHEETS, "dpetry");
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_WORKLOG, "Administrator");
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_WORKLOG, "scheeks");
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_WORKLOG, "dpetry");
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_VERIFY_SS, "Administrator");
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_VERIFY_SS, "cnilest");
        //SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_VERIFY_SS, "scheeks");

        // Check job code rates. This is only ran one time after bulk loading
        // job codes. This is a utility function only and should be removed
        // after using one time.
        //ATGDB.Ratedata.CheckAllJobCodesForDefaultRate();

        // If the user has not set their question/answer, make them do that.
        MembershipUser u = ProviderUtils.GetUser(Profile.UserName);
        if ((u != null) && (u.PasswordQuestion == MembershipHelper.NO_QUESTION)) {
            // Users must have a security question.
            // Send to error page.
            Response.Redirect("~/Security/FirstLoginSecurityQuestion.aspx", false);
            return;
        }

        // Finish up the audit trails, clear the cookies.
        var profile = (ProfileCommon)System.Web.HttpContext.Current.Profile;
        if (profile != null) {
            // Audit Trail - close last entry.
            AuditUtils.AuditTrail.CloseOpenLogs(Profile.Userid);
        }
    }
}
