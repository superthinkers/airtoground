﻿
public class ScheduleHelper
{
    private static ScheduleHelper instance;
    private ScheduleHelper() {}

    public static ScheduleHelper Instance {
        get {
            if (instance == null) {
                instance = new ScheduleHelper();
            }
            return instance;
        }
    }

    // Set by ViewSchedule
    private string viewPageKey = "";
    public string ViewPageKey { 
        get {
            return viewPageKey;    
        } 
        set {
            viewPageKey = value;
        } 
    }
    // Set by MasterSchedule
    private string masterPageKey = "";
    public string MasterPageKey {
        get {
            return masterPageKey;
        }
        set {
            masterPageKey = value;
        }
    }
}
