using System;
using System.Web;

public static class SecurityUtils
{
    // Misc
    public const string ROLE_ADMIN = "Administrator"; // DO NOT CHANGE - web.config depends on this for location security
    public const string ROLE_CODETABLES = "Code Table Editor"; // DO NOT CHANGE - web.config depends on this for location security
    public const string ROLE_CORP = "Corporate User"; // DO NOT CHANGE - web.config depends on this for location security
    public const string ROLE_USERSECURITY = "User Security"; // DO NOT CHANGE - web.config depends on this for location security
    public const string ROLE_CUSTOMER = "Customer"; // DO NOT CHANGE - web.config depends on this for location security
    public const string ROLE_DELETE_LOGGED_SS = "Delete Logged Service Schedules";
    public const string ROLE_VERIFY_SS = "Verify Scheduled Services";
    // Reports
    public const string ROLE_RPT_REVENUE = "Run Revenue Reports"; // DO NOT CHANGE - web.config depends on this for location security
    public const string ROLE_RPT_WORK = "Run Work Reports"; // DO NOT CHANGE - web.config depends on this for location security
    public const string ROLE_RPT_EQUIP = "Run Equipment Reports"; // DO NOT CHANGE - web.config depends on this for location security
    public const string ROLE_RPT_JOB = "Run Job Reports"; // DO NOT CHANGE - web.config depends on this for location security
    // Exporting
    public const string ROLE_EXP_GEN = "Export - General Exporting";
    public const string ROLE_EXP_USERS = "Export Users";
    public const string ROLE_EXP_SCH = "Export Equipment Schedule";
    public const string ROLE_EXP_EQUIP = "Export Equipment List";
    public const string ROLE_EXP_TIMESHEETS = "Export Time Sheets";
    public const string ROLE_EXP_JOBCODES = "Export Job Codes";
    //public const string ROLE_EXP_SCH = "Export Equipment Schedule";
    // Rates
    public const string ROLE_WORKCARD_RATE = "View Work Card Rate";
    public const string ROLE_EMPLOYEE_RATE = "View Employee Rates";
    public const string ROLE_CUSTOMER_NONROUTINE_RATE = "View Customer Non-Routine Rates";
    // Job Applications
    public const string ROLE_MANAGE_JOBAPP = "Manage Job Applications";
    public const string ROLE_RECEIVE_JOBAPP = "Receive Job Applications";
    // Rate Schedule
    public const string ROLE_RATESCHEDULE = "Manage Rate Schedule"; // DO NOT CHANGE - web.config depends on this for location security
    // Time Sheets
    public const string ROLE_TIMESHEETS = "Time Sheet Management"; // DO NOT CHANGE - web.config depends on this for location security
    // Work Log
    public const string ROLE_WORKLOG = "Work Log Management"; // DO NOT CHANGE - web.config depends on this for location security
    public const string ROLE_WORKLOG_RATE = "View Work Log Rate";

    // Found in the Web.Config.AppSettings
    public const string ATG_MODE_CORP = "CORP";
    public const string ATG_MODE_CUSTOMER = "CUSTOMER";

	// IMPORT MANAGER
	public const string ROLE_IMPORT_MANAGER = "Import Manager";

    // Stuff for the login control
    public static void SetLoginCookie(bool rememberMe, string userName, string passWord) {
        string cookieName = "ATGTrack_" + SecurityHelper.Instance.ProviderRoot + "_123456789";
        if (rememberMe == true) {
            HttpCookie cookie = new HttpCookie(cookieName);
            cookie.Path = "/";
            cookie.Expires = DateTime.Now.AddDays(365);
            cookie.Values["UserName"] = userName;
            if (!String.IsNullOrEmpty(passWord)) {
                cookie.Values["Password"] = AESWrapper.EncryptString(passWord);
            } else {
                cookie.Values["Password"] = "";
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
        } else {
            HttpCookie cookie = new HttpCookie(cookieName, "");
            cookie.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Set(cookie);
        }
    }
    public static string GetLoginUserName(bool rememberMe) {
        string cookieName = "ATGTrack_" + SecurityHelper.Instance.ProviderRoot + "_123456789";
        if (rememberMe == true) {
            if (HttpContext.Current.Request.Cookies[cookieName] != null) {
                return HttpContext.Current.Request.Cookies[cookieName]["UserName"];
            } else {
                return "";
            }
        } else {
            return "";
        }
    }
    public static string GetLoginPassWord(bool rememberMe) {
        string cookieName = "ATGTrack_" + SecurityHelper.Instance.ProviderRoot + "_123456789";
        if (rememberMe == true) {
            if (HttpContext.Current.Request.Cookies[cookieName] != null) {
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.Cookies[cookieName]["Password"])) {
                    return AESWrapper.DecryptString(HttpContext.Current.Request.Cookies[cookieName]["Password"]);
                } else {
                    return "";
                }
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    public static string GetATGMode() {
        //if (System.Web.HttpContext.Current.Session != null) {
            if (System.Web.HttpContext.Current.Session["ATG_MODE"] != null) {
                return System.Web.HttpContext.Current.Session["ATG_MODE"].ToString();
            } else {
                System.Web.HttpContext.Current.Session["ATG_MODE"] = ATG_MODE_CORP;
                return ATG_MODE_CORP;
            }
        //} else {
        //    // This happens during an exception and redirect to the Support.aspx custom error form (web.config and tag customErrors and redirectMode="ResponseRewrite"
        //    if (((ProfileCommon)System.Web.HttpContext.Current.Profile).SiteTheme.ToUpper().Equals("CORP")) {
        //        return ATG_MODE_CORP;
        //    } else {
        //        return ATG_MODE_CUSTOMER;
        //    }
        //}
    }

    public static void SetATGMode(string sMode) {
        System.Web.HttpContext.Current.Session.Add("ATG_MODE", sMode);        
    }

    public static string GetCustId() {
        if (System.Web.HttpContext.Current.Session["ATG_CUSTID"] != null) {
            return System.Web.HttpContext.Current.Session["ATG_CUSTID"].ToString();
        } else {
            return "0";
        }
    }
    public static void SetCustId(string sId) {
        System.Web.HttpContext.Current.Session.Add("ATG_CUSTID", sId);
    }

    // NOTE THIS SETs THE AUTH COOKIE TIMEOUT!!!
    // DO NOT DO THIS. MODIFYING THE WEB.CONFIG CAUSES ALL ACTIVE SESSIONS TO DROP.
    /*public static void UpdateAuthCookie() {
        // Get the unique cookie name per customer.  
        string sId = System.Web.HttpContext.Current.Session.SessionID;

        // Get the authentication section.
        AuthenticationSection auth = ConfigurationManager.GetSection("system.web/authentication") as AuthenticationSection;

        // Make it so you can modify the element.
        var fi = typeof(ConfigurationElement).GetField("_bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        fi.SetValue(auth.Forms, false);

        // Assign value in web.config for future restarts
        auth.Forms.Name = "ATG_AuthCookie_" + sId;
        auth.Forms.Timeout = new System.TimeSpan(10, 0, 0);

        // Would be nice if this restarted the app, but it doesn't appear to     
        //c.Save();     

        // Back to readonly - SECURITY
        fi.SetValue(auth.Forms, true);

        // This seems to restart the app     
        //System.Web.HttpRuntime.UnloadAppDomain();  
    }*/

    /*
     * This method will set the providers based on a web.config app setting.
     * Called in the Global.asax.Session_Start()
     */
    /*public static void SetProviders() {
        // Get the provider root name. Via convention only.
        string providerRoot = System.Web.HttpContext.Current.Session["ProviderRoot"].ToString();
        MembershipSection ms = (MembershipSection)System.Web.Configuration.WebConfigurationManager.GetWebApplicationSection("system.web/membership");
        RoleManagerSection rm = (RoleManagerSection)System.Web.Configuration.WebConfigurationManager.GetWebApplicationSection("system.web/roleManager");
        ProfileSection ps = (ProfileSection)System.Web.Configuration.WebConfigurationManager.GetWebApplicationSection("system.web/profile");

        // Make it so you can modify the connection string.
        var fi = typeof(ConfigurationElement).GetField("_bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        fi.SetValue(ms, false);
        fi = typeof(ConfigurationElement).GetField("_bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        fi.SetValue(rm, false);
        fi = typeof(ConfigurationElement).GetField("_bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        fi.SetValue(ps, false);

        // Modify the default provider.
        ms.DefaultProvider = providerRoot + "_MembershipProvider"; // Name is the provider name in web.config.
        rm.DefaultProvider = providerRoot + "_RoleProvider"; // One each provider for each distinct connection / customer
        ps.DefaultProvider = providerRoot + "_ProfileProvider"; // So, you have 3 providers: membership, role, profile, one each for each customer.

        // Back to readonly - SECURITY.
        fi.SetValue(ms, true);
        fi.SetValue(rm, true);
        fi.SetValue(ps, true);
    }*/

    /*
     * This method will set the set the RoleManager cookie.
     * Called in the ATGRoleProvider
     */
    // DO NOT DO THIS. MODIFYING THE WEB.CONFIG CAUSES ALL ACTIVE SESSIONS TO DROP.
    /*public static void SetRoleProviderCookieName() {
        if (System.Web.HttpContext.Current.Session != null) {
            // Get the provider root name. Via convention only.
            RoleManagerSection rm = (RoleManagerSection)System.Web.Configuration.WebConfigurationManager.GetWebApplicationSection("system.web/roleManager");

            // Make it so you can modify the connection string.
            var fi = typeof(ConfigurationElement).GetField("_bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(rm, false);

            // Modify the default provider.
            string sId = System.Web.HttpContext.Current.Session.SessionID;
            if (rm.CookieName != "ATG_CachedRoles_" + sId) {
                rm.CookieName = "ATG_CachedRoles_" + sId; // One each provider for each distinct customer

                // Back to readonly - SECURITY.
                fi.SetValue(rm, true);
            }
        }
    }*/

    // Customer ATG Profile Provider is calling this.
    // Note: /Default.cs, and Customer/Default.cs this just before Profile.Initialize.
    // DO NOT DO THIS. MODIFYING THE WEB.CONFIG CAUSES ALL ACTIVE SESSIONS TO DROP.
    /*public static void UpdateSecurityDBConnectionString(NameValueCollection config) {
        // Intercept the setting of the connection string so that we can set it ourselves...        
        string specifiedConnectionString = config["connectionStringName"];
        ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings[specifiedConnectionString];

        // Make it so you can modify the connection string.
        var fi = typeof(ConfigurationElement).GetField("_bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        fi.SetValue(connectionString, false);

        // Modify the connection string.
        string db = "atg";
        if (System.Web.HttpContext.Current.Session["SecurityDBName"] != null) {
            db = System.Web.HttpContext.Current.Session["SecurityDBName"].ToString();
        }
        string server = ConfigurationManager.AppSettings.Get("CustomerServerName");
        connectionString.ConnectionString = "User Id=atg;Password=password;Host=" +
            server + ";Database=" +
            db + ";Persist Security Info=True";
        config["connectionStringName"] = connectionString.Name;

        // Back to readonly - SECURITY.
        fi.SetValue(connectionString, true);
    }*/

    // Customers type a validation code upon login to redirect them to the 
    // proper membership database. See the method above this one,
    // and Customer/Default.cs for more details.
    // DEPRECATED
    /*public static string GetCustomerDBFromValidationCode(string validationCode) {
        if (validationCode.ToUpper() == "624") {
            return "atgfedex";
        } else if (validationCode.ToUpper() == "201619") {
            return "atgups";
        } else {
            return "";
        }
    }*/

    // Customers type a validation code upon login to redirect them to the 
    // proper membership database. This code can be used to return their acronymn.
    public static string GetCustomerShortNameFromValidationCode(string validationCode) {
        if (validationCode.ToUpper() == "624") {
            return "FedEx";
        } else if (validationCode.ToUpper() == "201619") {
            return "UPS";
        } else {
            return "CORP";
        }
    }

    // Customers type a validation code upon login to redirect them to the 
    // proper membership database. See the method above this one,
    // and Customer/Default.cs for more details.
    public static string GetProviderRootFromValidationCode(string validationCode) {
        if (validationCode.ToUpper() == "624") {
            return "624"; // FedEx
        } else if (validationCode.ToUpper() == "201619") {
            return "201619"; // UPS
        } else {
            return "";
        }
    }

    public static string GetSiteThemeFromProviderRoot(string providerRoot) {
        if (providerRoot.ToUpper() == "624") {
            return "624";
        } else if (providerRoot.ToUpper() == "201619") {
            return "201619";
        } else {
            return "Corp";
        }
    }

    public static string GetCustIdFromValidationCode(string validationCode) {
        if (validationCode.ToUpper() == "624") {
            return "3"; // FEDEX
        } else if (validationCode.ToUpper() == "201619") {
            return "2"; // UPS
        } else {
            return "0";
        }
    }

    public static void AutoCreateSecurityRoleAndOptionallyAssignToUser(string sAddRole, string sUserName) {
        // Generic mechanism for adding a new role to database, and optionally to all users.
        string[] roles = ProviderUtils.GetAllRoles();
        bool found = false;
        foreach (string role in roles) {
            found = role == sAddRole;
            if (found == true) {
                break;
            }
        }
        if (!found) {
            ProviderUtils.CreateRole(sAddRole);
        }
        if ((sUserName != "") && (!ProviderUtils.IsUserInRole(sUserName, sAddRole))) {
            // Add them.
            ProviderUtils.AddUserToRole(sUserName, sAddRole);
        }
    }

    public static void AddAllRoles() {
        if (GetATGMode() != ATG_MODE_CUSTOMER) {
            // Only Corp clients get these
            AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_CORP, "");
            AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_CODETABLES, "");
            AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_RPT_JOB, "");
            AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_EXP_TIMESHEETS, "");
            AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_EXP_JOBCODES, "");
            AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_WORKCARD_RATE, "");
            AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_EMPLOYEE_RATE, "");
            AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_CUSTOMER_NONROUTINE_RATE, "");
            AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_MANAGE_JOBAPP, "");
            AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_RECEIVE_JOBAPP, "");
            AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_RATESCHEDULE, "");
        }
        // All Corp or Cust clients get these
        AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_CUSTOMER, "");
        AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_ADMIN, "");
        AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_USERSECURITY, "");
        AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_RPT_REVENUE, "");
        AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_RPT_WORK, "");
        AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_RPT_EQUIP, "");
        AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_EXP_GEN, "");
        AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_EXP_USERS, "");
        AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_EXP_SCH, "");
        AutoCreateSecurityRoleAndOptionallyAssignToUser(ROLE_EXP_EQUIP, "");
    }
}
