﻿using System.Net;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Net.Mail;
using System.Linq;
using System.Web.Security;

/// <summary>
/// Summary description for EmailUtils
/// </summary>
public static class EmailUtils
{
    private static void SendMail(HttpServerUtility server, string userName,
                                string subject, string file1) {
        MembershipUser u = ProviderUtils.GetUser(userName);

        if (u != null) {
            // Create the message.
            MailMessage msg = new MailMessage();
            msg.To.Add(new MailAddress(u.Email));
            msg.Subject = subject;
            msg.Body = ""; // no body.
            msg.IsBodyHtml = true;
            msg.ReplyToList.Clear();
            msg.ReplyToList.Add("DoNotReply@airtoground.com");

            // Turns out, if you just want to disable certificate validation altogether, you can change the ServerCertificateValidationCallback on the ServicePointManager, like so:
            // Clear down below.
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; }; 

            // Create a request for the page that you want
            string url = WebConfigurationManager.AppSettings["ATGEmailNotificationsURL"];
            WebRequest request = System.Net.HttpWebRequest.Create(url + file1);

            // Get the processed response
            WebResponse response = request.GetResponse();

            // Get the HTML for the page
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string sHTML = reader.ReadToEnd();
            reader.Close();

            // Get a stream for the HTML.
            MemoryStream stream = new MemoryStream(System.Text.Encoding.ASCII.GetBytes(sHTML));

            // Create a customized view for the HTML.
            AlternateView htmlView = new AlternateView(stream, System.Net.Mime.MediaTypeNames.Text.Html);

            // Link the splash image.
            LinkedResource resource = new LinkedResource(server.MapPath("~/Images/SiteSplash.png"), System.Net.Mime.MediaTypeNames.Image.Jpeg);

            // Name the resource
            resource.ContentId = "imgSplash";

            // Add the resource to the alternate view
            htmlView.LinkedResources.Add(resource);

            // Add the style sheet.
            string urlVirtualPath = WebConfigurationManager.AppSettings["ATGEmailNotificationsURLVirtualPath"];
            resource = new LinkedResource(server.MapPath(urlVirtualPath + "StyleSheet.css"), System.Net.Mime.MediaTypeNames.Text.Html);

            // Name the resource
            resource.ContentId = "txtStyle";

            // Add the resource to the alternate view
            htmlView.LinkedResources.Add(resource);

            // Add the views to the message.
            msg.AlternateViews.Add(htmlView);

            // Clear
            ServicePointManager.ServerCertificateValidationCallback = null;

            SmtpClient smtp = new SmtpClient(); // Uses web.config MailSettings
            /*if (smtp.Host.Contains("gmail")) {
                smtp.EnableSsl = true;
                smtp.Port = 587;
            }
            if (smtp.Host.Contains("secureserver")) {
                //smtp.Credentials = new NetworkCredential("donotreply@airtoground.com", "Pr1vat30!");
                smtp.EnableSsl = false;
                smtp.Port = 25;
            }*/

            smtp.Timeout = 60000;
            smtp.Send(msg);

            // Cleanup.
            stream.Close();
            response.Close();
        }
    }
    public static void SendPasswordChangedMail(HttpServerUtility server, string userName, string changeUserName, string newPassword) {
        bool enabled = System.Web.Configuration.WebConfigurationManager.AppSettings["Mail_Services_Enabled"].ToUpper() == "TRUE";
        if (enabled) {
            // Copy to User
            SendMail(server, userName, "Your ATG Website Password was Reset By: " + changeUserName,
                     "PasswordChanged.aspx?UserName=" + userName + "&Password=" + newPassword);
            // Copy to Administrator
            SendMail(server, "Administrator", "ATG Website Alert: " + userName + "'s Password Reset By: " + changeUserName,
                     "AdminPasswordChanged.aspx?UserName=" + userName);
        }
    }
    public const string DONOTSHOWPASSWORD = "DONOTSHOWPASSWORD";
    public static void SendSecurityChangedMail(HttpServerUtility server, string userName) {
        bool enabled = System.Web.Configuration.WebConfigurationManager.AppSettings["Mail_Services_Enabled"].ToUpper() == "TRUE";
        if (enabled) {
            // Copy to User
            SendMail(server, userName, "Your ATG Website Password was Changed",
                     "PasswordChanged.aspx?UserName=" + userName + "&Password=" + DONOTSHOWPASSWORD);
            // Copy to Administrator
            SendMail(server, "Administrator", "ATG Website Alert: " + userName + "'s Security Changed",
                     "AdminPasswordChanged.aspx?UserName=" + userName);
        }
    }
    public static void SendSimpleHtmlMail(HttpServerUtility server, string emailAddr, string subject, string body) {
        bool enabled = System.Web.Configuration.WebConfigurationManager.AppSettings["Mail_Services_Enabled"].ToUpper() == "TRUE";
        if (enabled) {
            // Create the message.
            MailMessage msg = new MailMessage();
            msg.To.Add(new MailAddress(emailAddr));
            msg.Subject = subject;
            msg.Body = body;
            msg.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient(); // Uses web.config settings.
            smtp.Timeout = 60000;
            smtp.Send(msg);
        }
    }
    public static void SendJobApplicationMail(HttpServerUtility server, 
        string uri, string args, string email, string jobAppGuidkey, bool toApplicant) {

        // Don't process if mail not enabled.
        if (WebConfigurationManager.AppSettings["Mail_Services_Enabled"] == "false") {
            return;
        }

        // Get the job app.
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        ATGDB.Jobapplication jobApp = dc.Jobapplications.SingleOrDefault(x => x.Guidkey == jobAppGuidkey);

        if (jobApp != null) {
            // Build a subject line.
            string subject = "Job Application: " + jobApp.Createdate.Value.Date.ToShortDateString() +
                             " - " + jobApp.Lastname + ", " + jobApp.Firstname;
            
            // Create the message.
            MailMessage msg = new MailMessage();
            if (toApplicant == true) {
                msg.To.Add(new MailAddress(jobApp.Signature));
            } else {
                msg.To.Add(new MailAddress(email));
            }
            msg.From = new MailAddress("donotreply@airtogroundtrack.com");
            msg.Subject = subject;
            msg.Body = ""; // no body.
            msg.BodyEncoding = System.Text.Encoding.Default;
            msg.IsBodyHtml = true;
            msg.ReplyToList.Clear();
            msg.ReplyToList.Add("DoNotReply@airtoground.com");

            // Turns out, if you just want to disable certificate validation altogether, you can change the ServerCertificateValidationCallback on the ServicePointManager, like so:
            // Clear down below.
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            
            // Create a request for the page that you want
            WebRequest request = System.Net.HttpWebRequest.Create("https://10.0.0.2/Careers/EmailFullJobApplication.aspx?" + args);

            // Get the processed response
            WebResponse response = request.GetResponse();

            // Get the HTML for the page
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string sHTML = reader.ReadToEnd();
            reader.Close();

            // Get a stream for the HTML.
            MemoryStream stream = new MemoryStream(System.Text.Encoding.ASCII.GetBytes(sHTML));

            // Create a customized view for the HTML.
            AlternateView htmlView = new AlternateView(stream, System.Net.Mime.MediaTypeNames.Text.Html);

            // Add the style sheet.
            //LinkedResource resource = new LinkedResource(server.MapPath("StyleSheet.css"), System.Net.Mime.MediaTypeNames.Text.Html);
            //resource.ContentId = "txtStyle";
            //htmlView.LinkedResources.Add(resource);

            // Add the skin file.
            //resource = new LinkedResource(server.MapPath("SkinFile.skin"), System.Net.Mime.MediaTypeNames.Text.Html);
            //resource.ContentId = "txtSkin";
            //htmlView.LinkedResources.Add(resource);

            // Add the views to the message.
            msg.AlternateViews.Add(htmlView);

            // Clear
            ServicePointManager.ServerCertificateValidationCallback = null;

            SmtpClient smtp = new SmtpClient(); // Uses web.config settings.

			// AUTHENTICATION
			//mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
			//mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "donotreply"); //set your username here
			//mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "!Pr1vat30!");	//set your password here
            //smtp.UseDefaultCredentials = false;
			//NetworkCredential credentials = new NetworkCredential("donotreply", "!Pr1vat30!");  
			//smtp.Credentials = credentials;
         
            smtp.Timeout = 60000;
            smtp.Send(msg);

            // Cleanup.
            stream.Close();
            response.Close();
        }
    }
}
