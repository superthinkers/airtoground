﻿using System;
using System.Linq;

/// <summary>
/// Summary description for Timesheet
/// </summary>
/// 

/*
 *  NOTES ON PARTIAL METHODS: The partial classes must be in
 *  the same name space as the class, in this case ATGDB, and
 *  then you can get the partial methods from the generated
 *  code (ATGDataContext.Designer.cs).
 *  
 */
namespace ATGDB
{
    public partial class Extendeduser
    {
        public static ATGDB.Extendeduser GetExtendedUserByEmpNum(string employeeNumber) {
            ATGDataContext dc = new ATGDataContext();
            ATGDB.Extendeduser ex = dc.Extendedusers.SingleOrDefault(x => x.Employeenumber.Trim().Equals(employeeNumber.Trim()));
            return ex;
        }

        public string Fullname {
            get {
                return Lastname + ", " + Firstname;
            }
        }

        #region Extensibility Method Definitions
        //partial void OnLoaded();
        //partial void OnValidate(Devart.Data.Linq.ChangeAction action) {
        //    if ((action == Devart.Data.Linq.ChangeAction.Insert) ||
        //        (action == Devart.Data.Linq.ChangeAction.Update)) {
        //    }
        //}
        //partial void OnCreated() {
        //}
        //partial void OnTimesheetidChanging(int value);
        //partial void OnTimesheetidChanged();
        //partial void OnUpdusernameChanging(string value);
        //partial void OnUpdusernameChanged();
        //partial void OnUsernameChanging(string value);
        //partial void OnUsernameChanged();
        //partial void OnCompanydivisionidChanging(int value);
        //partial void OnCompanydivisionidChanged();
        //partial void OnCustomeridChanging(int value);
        //partial void OnCustomeridChanged();
        //partial void OnJobcodeidChanging(int value);
        //partial void OnJobcodeidChanged();
        //partial void OnWorklogidChanging(System.Nullable<int> value);
        //partial void OnWorklogidChanged();
        //partial void OnReconciledstatusChanging(char value);
        //partial void OnReconciledstatusChanged();
        //partial void OnRateChanging(float value);
        //partial void OnRateChanged();
        //partial void OnHoursChanging(float value);
        //partial void OnHoursChanged();
        //partial void OnWorkeddateChanging(System.DateTime value);
        //partial void OnNotesChanging(string value);
        //partial void OnNotesChanged();
        //partial void OnWorkcardnumberChanging(string value);
        //partial void OnWorkcardnumberChanged();
        //partial void OnWorkcarddescriptionChanging(string value);
        //partial void OnWorkcarddescriptionChanged();
        //partial void OnWorkcardhourlyrateChanging(System.Nullable<float> value);
        //partial void OnWorkcardhourlyrateChanged();
        //partial void OnWorkcardcompletedChanging(System.Nullable<bool> value);
        //partial void OnWorkcardcompletedChanged();
        #endregion

    }
}