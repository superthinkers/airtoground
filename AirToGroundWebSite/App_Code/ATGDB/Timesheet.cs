﻿using System;
using System.Collections.Generic;
using System.Linq;
/// <summary>
/// Summary description for Timesheet
/// </summary>
/// 

/*
 *  NOTES ON PARTIAL METHODS: The partial classes must be in
 *  the same name space as the class, in this case ATGDB, and
 *  then you can get the partial methods from the generated
 *  code (ATGDataContext.Designer.cs).
 *  
 */
namespace ATGDB
{
    public partial class Timesheet
    {
        public class MyReconciledStatus
        {
            public string Status { get; set; }
            public string Description { get; set; }
            public MyReconciledStatus(string status, string description) {
                Status = status;
                Description = description;
            }
        }
        public static List<MyReconciledStatus> GetReconciledStatusData() {
            List<MyReconciledStatus> data = new List<MyReconciledStatus>();
            data.Add(new MyReconciledStatus("U", "Un-Reconciled"));
            data.Add(new MyReconciledStatus("R", "Reconciled"));
            data.Add(new MyReconciledStatus("T", "Totals Don't Match"));
            data.Add(new MyReconciledStatus("J", "Job Codes Don't Match"));
            data.Add(new MyReconciledStatus("W", "Worklog Not Found"));
            return data;
        }
        public static string GetReconciledDescr(string status) {
            if ((status != "\0") && (status != "")) {
                return GetReconciledStatusData().Find(x => x.Status == status).Description;
            } else {
                return GetReconciledStatusData().Find(x => x.Status == "U").Description;
            }
        }
        public static System.Drawing.Color GetReconciledColor(string status) {
            if (status == "U") {
                return System.Drawing.Color.Black;
            } else if (status == "R") {
                return System.Drawing.Color.Green;
            } else if (status == "T") {
                return System.Drawing.Color.Red;
            } else if (status == "J") {
                return System.Drawing.Color.Red;
            } else if (status == "W") {
                return System.Drawing.Color.Red;
            } else {
                return System.Drawing.Color.Black;
            }
        }


        #region Extensibility Method Definitions
        //partial void OnLoaded();
        //partial void OnValidate(Devart.Data.Linq.ChangeAction action) {
        //    if ((action == Devart.Data.Linq.ChangeAction.Insert) ||
        //        (action == Devart.Data.Linq.ChangeAction.Update)) {
        //        this.Updusername = System.Threading.Thread.CurrentPrincipal.Identity.Name;
        //    }
        //}
        partial void OnCreated() {
            this._Workeddate = DateTime.Now.Date;
            this._Starttime = Convert.ToDateTime(DateTime.Now.Date.ToShortDateString() + " " + "08:00:00 AM");
            this._Endtime = Convert.ToDateTime(DateTime.Now.Date.ToShortDateString() + " " + "05:00:00 PM");
            this._Hours = 0.00f;
            this._Rate = 0.00f;
        }
        //partial void OnTimesheetidChanging(int value);
        //partial void OnTimesheetidChanged();
        //partial void OnUpdusernameChanging(string value);
        //partial void OnUpdusernameChanged();
        //partial void OnUsernameChanging(string value);
        //partial void OnUsernameChanged();
        //partial void OnCompanydivisionidChanging(int value);
        //partial void OnCompanydivisionidChanged();
        //partial void OnCustomeridChanging(int value);
        //partial void OnCustomeridChanged();
        //partial void OnJobcodeidChanging(int value);
        //partial void OnJobcodeidChanged();
        //partial void OnWorklogidChanging(System.Nullable<int> value);
        //partial void OnWorklogidChanged();
        //partial void OnReconciledstatusChanging(char value);
        //partial void OnReconciledstatusChanged();
        //partial void OnRateChanging(float value);
        //partial void OnRateChanged();
        //partial void OnHoursChanging(float value);
        //partial void OnHoursChanged();
        //partial void OnWorkeddateChanging(System.DateTime value);
        partial void OnWorkeddateChanged() {
            if (this._Starttime != DateTime.MinValue) {
                this._Starttime = Convert.ToDateTime(Workeddate.Date.ToShortDateString() + " " + this.Starttime.ToShortTimeString());
            }
            if (this._Endtime != DateTime.MinValue) {
                this._Endtime = Convert.ToDateTime(Workeddate.Date.ToShortDateString() + " " + this.Endtime.ToShortTimeString());
            }
        }
        //partial void OnStarttimeChanging(System.DateTime value);
        partial void OnStarttimeChanged() {
            if (this._Workeddate != DateTime.MinValue) {
                // Update the start time to have the correct date, while maintaining the user's selected time.
                this._Starttime = Convert.ToDateTime(Workeddate.Date.ToShortDateString() + " " + this.Starttime.ToShortTimeString());
            }
        }
        //partial void OnEndtimeChanging(System.DateTime value);
        partial void OnEndtimeChanged() {
            if (this._Workeddate != DateTime.MinValue) {
                // Update the start time to have the correct date, while maintaining the user's selected time.
                this._Endtime = Convert.ToDateTime(Workeddate.Date.ToShortDateString() + " " + this.Endtime.ToShortTimeString());
            }
        }
        //partial void OnNotesChanging(string value);
        //partial void OnNotesChanged();
        //partial void OnWorkcardnumberChanging(string value);
        //partial void OnWorkcardnumberChanged();
        //partial void OnWorkcarddescriptionChanging(string value);
        //partial void OnWorkcarddescriptionChanged();
        //partial void OnWorkcardhourlyrateChanging(System.Nullable<float> value);
        //partial void OnWorkcardhourlyrateChanged();
        //partial void OnWorkcardcompletedChanging(System.Nullable<bool> value);
        //partial void OnWorkcardcompletedChanged();
        #endregion

        public static void Reconcile(string[] ids) {
            ATGDataContext dc = new ATGDataContext();
            Timesheet ts = null;
            //Worklog wl = null;
            bool needsCommit = false;
            // Reconcile each timesheet id.
            foreach (string tsid in ids) {
                // Get the time sheet.
                ts = dc.Timesheets.SingleOrDefault(x => x.Timesheetid == int.Parse(tsid));
                if (ts != null) {
                    // Let's look for matching worklogs.
                    //var data = from w in dc.Worklogs
                    //           where wl.Workeddate == ts.Workeddate
                    //           where wl.Jobcodeid == ts.Jobcodeid
                    //           select w;
                    //if (data.Count() > 0) {
                        // Found matching worklogs.
                        // Determine the status. Are we reconciled?
                    //    float hours = 0.00f;
                    //    foreach
                    //} else {
                        // Didn't find matching worklogs.
                        ts.Reconciledstatus = "R";
                        needsCommit = true;
                    //}
                }
            }
            if (needsCommit == true) {
                dc.SubmitChanges();
            }
        }




    }
}