﻿using System.Collections;
using System.Linq;

/// <summary>
/// Summary description for Customer
/// </summary>
namespace ATGDB
{
    public partial class Customer
    {
        public static ATGDB.Customer GetCustomerById(string customerid) {
            ATGDataContext dc = new ATGDataContext();
            ATGDB.Customer c = dc.Customers.SingleOrDefault(x => x.Customerid.Equals(customerid));
            return c;
        }
        public static IEnumerable GetCustomersByUserid(string userId) {
            ATGDataContext dc = new ATGDataContext();
            var data = (from ccu in dc.Companycustomeruserjoins
                        join c in dc.Customers on ccu.Customerid equals c.Customerid
                        where ccu.Userid == userId
                        select new {
                            Customerid = c.Customerid,
                            Description = c.Description
                        }).Distinct().ToList();
            return data;
        }
    }
}