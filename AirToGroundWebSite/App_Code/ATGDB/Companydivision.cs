﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Data;


/// <summary>
/// Summary description for Companydivision
/// </summary>
namespace ATGDB
{
    public partial class Companydivision
    {

        public class MyTheme
        {
            public string Sitetheme { get; set; }
            public string Description { get; set; }
            public MyTheme(string sitetheme, string description) {
                Sitetheme = sitetheme;
                Description = description;
            }
        }
        public List<MyTheme> GetThemeData() {
            List<MyTheme> data = new List<MyTheme>();
            data.Add(new MyTheme("Corp", "Corporate"));
            data.Add(new MyTheme("AAR", "AAR"));
            data.Add(new MyTheme("FedEx", "Federal Express"));
            data.Add(new MyTheme("UPS", "UPS"));
            return data;
        }

        public static ATGDB.Companydivision GetCompanyDivisionById(string companydivisionid) {
            ATGDataContext dc = new ATGDataContext();
            ATGDB.Companydivision c = dc.Companydivisions.SingleOrDefault(x => x.Companydivisionid.Equals(companydivisionid));
            return c;
        }
        public static IEnumerable GetDivisionsAndCustomersByUser(string userId) {
            ATGDataContext dc = new ATGDataContext();
            var data = (from ccu in dc.Companycustomeruserjoins
                        //from c in dc.Customers 
                        //from d in dc.Companydivisions 
                        //where c.Customerid == ccu.Customerid
                        //where d.Companydivisionid == ccu.Companydivisionid
                        where ccu.Userid == userId
                        select new {
                            Id = ccu.Companydivision.Companydivisionid + ":" + ccu.Customer.Customerid,
                            Description = ccu.Companydivision.Description + "/" + ccu.Customer.Description
                        }).ToList();
            return data;
        }
        public static IEnumerable GetCompanyDivisionsByUser(string userId) {
            ATGDataContext dc = new ATGDataContext();
            var data = (from ccu in dc.Companycustomeruserjoins
                        join d in dc.Companydivisions on ccu.Companydivisionid equals d.Companydivisionid
                        where ccu.Userid == userId
                        select new {
                            Companydivisionid = d.Companydivisionid,
                            Description = d.Description
                        }).ToList();
            return data;
        }
        class CCUJComparer : IEqualityComparer<ATGDB.Companycustomeruserjoin> {
            public bool Equals(Companycustomeruserjoin x, Companycustomeruserjoin y) {
                return (x.Companydivisionid.Equals(y.Companydivisionid));// &&
                       //(x.Customerid.Equals(y.Customerid));  // Note to self: Removed for TimeSheet Company Drop down in Search Criteria.
            }
            public int GetHashCode(Companycustomeruserjoin obj) {
                return obj.Companydivisionid.GetHashCode();// &obj.Customerid.GetHashCode(); // Note to self: Removed for TimeSheet Company Drop down in Search Criteria.
            }
        }
        public static List<ATGDB.Companycustomeruserjoin> GetCompanyCustomerUserJoins(string userId, bool distinct) {
            CCUJComparer ccujComparer = new CCUJComparer();

            if (distinct == true) {
                ATGDataContext dc = new ATGDataContext();
                var data = (from ccu in dc.Companycustomeruserjoins
                            where ccu.Userid == userId
                            select ccu).ToList().Distinct(ccujComparer);
                return data.ToList();
            } else {
                ATGDataContext dc = new ATGDataContext();
                var data =  from ccu in dc.Companycustomeruserjoins
                            where ccu.Userid == userId
                            select ccu;
                return data.ToList();
            }
        }
        public static DataSet GetCompanyCustomerUserJoinsDS(string userId, bool distinct) {
            List<ATGDB.Companycustomeruserjoin> data = GetCompanyCustomerUserJoins(userId, distinct);
            DataSet ds = new DataSet("ccujs");
            DataTable dt = ds.Tables.Add("ccuj");
            dt.Columns.Add("Companydivisionid", typeof(int));
            dt.Columns.Add("Customerid", typeof(int));
            dt.Columns.Add("Companydescr", typeof(string));
            dt.Columns.Add("Customerdescr", typeof(string));
            dt.Columns.Add("Userid", typeof(string));
            foreach (ATGDB.Companycustomeruserjoin ccuj in data) {
                dt.Rows.Add(new object[] { ccuj.Companydivisionid, ccuj.Customerid, ccuj.Companydivision.Description, ccuj.Customer.Description, ccuj.Userid });
            }
            return ds;
        }
        class CCJComparer : IEqualityComparer<ATGDB.Companycustomerjoin>
        {
            public bool Equals(Companycustomerjoin x, Companycustomerjoin y) {
                return (x.Companydivisionid.Equals(y.Companydivisionid)) &&
                       (x.Customerid.Equals(y.Customerid));
            }
            public int GetHashCode(Companycustomerjoin obj) {
                return obj.Companydivisionid.GetHashCode() & obj.Customerid.GetHashCode();
            }
        }
        public static List<ATGDB.Companycustomerjoin> GetCompanyCustomerJoins(bool distinct) {
            if (distinct == true) {
                ATGDataContext dc = new ATGDataContext();
                CCJComparer ccjComparer = new CCJComparer();
                var data = (from ccj in dc.Companycustomerjoins
                            select ccj).ToList().Distinct(ccjComparer);
                return data.ToList();
            } else {
                ATGDataContext dc = new ATGDataContext();
                var data = from ccj in dc.Companycustomerjoins
                           select ccj;
                return data.ToList();
            }
        }
        public static DataSet GetCompanyCustomerJoinsDS(bool distinct) {
            List<ATGDB.Companycustomerjoin> data = GetCompanyCustomerJoins(distinct);
            DataSet ds = new DataSet("ccjs");
            DataTable dt = ds.Tables.Add("ccj");
            dt.Columns.Add("Companydivisionid", typeof(int));
            dt.Columns.Add("Customerid", typeof(int));
            dt.Columns.Add("Companydescr", typeof(string));
            dt.Columns.Add("Customerdescr", typeof(string));
            foreach (ATGDB.Companycustomerjoin ccj in data) {
                dt.Rows.Add(new object[] { ccj.Companydivisionid, ccj.Customerid, ccj.Companydivision.Description, 
                    ccj.Customer.Description });
            }
            return ds;
        }
        // These classes and following method are for a custom drop down that shows
        // Company - Customer, and has a key values of CompanyDivisionId:CustomerId.
        // This was used in EditRateSchedule.aspx.cs, RateData.cs
        class CustCCJComparer : IEqualityComparer<MyCustomCCJ>
        {
            public bool Equals(MyCustomCCJ x, MyCustomCCJ y) {
                return (x.Companydivisionid.Equals(y.Companydivisionid)) &&
                       (x.Customerid.Equals(y.Customerid));
            }
            public int GetHashCode(MyCustomCCJ obj) {
                return obj.Companydivisionid.GetHashCode() & obj.Customerid.GetHashCode();
            }
        }
        public class MyCustomCCJ
        {
            public int Companydivisionid { get; set; }
            public int Customerid { get; set; }
            public string CompanyDescr { get; set; }
            public string CustomerDescr { get; set; }
            public string Keys {
                get {
                    return Companydivisionid.ToString() + ":" + Customerid.ToString();
                }
                set { }
            }
            public string Description {
                get {
                    return CompanyDescr + " (" + CustomerDescr + ")";
                }
                set { }
            }
            public MyCustomCCJ() {
            }
        }
        public static List<MyCustomCCJ> GetCustomCCJ() {
            ATGDataContext dc = new ATGDataContext();
            CustCCJComparer custCCJComparer = new CustCCJComparer();
            var data = (from ccj in dc.Companycustomerjoins
                        select new MyCustomCCJ() {
                            Companydivisionid = ccj.Companydivisionid,
                            Customerid = ccj.Customerid,
                            CompanyDescr = ccj.Companydivision.Description,
                            CustomerDescr = ccj.Customer.Description,
                            Keys = ccj.Companydivisionid + ":" + ccj.Customerid,
                            Description = ccj.Companydivision.Description + " / " + ccj.Customer.Description
                        }).ToList<MyCustomCCJ>().Distinct(custCCJComparer);
            return data.ToList<MyCustomCCJ>();
        }
        public static IQueryable<ATGDB.Companydivision> GetAllDivisions() {
            ATGDataContext db = new ATGDataContext();
            var data = from d in db.Companydivisions
                       orderby d.Description
                       select d;
            return data.AsQueryable();
        }
    }
}