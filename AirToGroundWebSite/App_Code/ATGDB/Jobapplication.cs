﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

/// <summary>
/// Summary description for Jobapplication
/// </summary>
namespace ATGDB
{
    public partial class Jobapplication
    {
        #region Extensibility Method Definitions
        partial void OnLoaded() {
            this.Createdate = DateTime.Now;
            this.Datecanstart = DateTime.Now;
        }
        //partial void OnValidate(Devart.Data.Linq.ChangeAction action);
        //partial void OnCreated();
        //partial void OnJobapplicationidChanging(int value);
        //partial void OnJobapplicationidChanged();
        //partial void OnCreatedateChanging(System.DateTime value);
        //partial void OnCreatedateChanged();
        //partial void OnFirstnameChanging(string value);
        //partial void OnFirstnameChanged();
        //partial void OnMiddlenameChanging(string value);
        //partial void OnMiddlenameChanged();
        //partial void OnLastnameChanging(string value);
        //partial void OnLastnameChanged();
        //partial void OnStatusChanging(string value);
        //partial void OnStatusChanged();
        //partial void OnStatusdateChanging(System.DateTime value);
        //partial void OnStatusdateChanged();
        //partial void OnIscurrentlyemployedChanging(bool value);
        //partial void OnIscurrentlyemployedChanged();
        //partial void OnIssecondjobChanging(bool value);
        //partial void OnIssecondjobChanged();
        //partial void OnCanworkmonthrusatChanging(bool value);
        //partial void OnCanworkmonthrusatChanged();
        //partial void OnCanworkfirstshiftChanging(bool value);
        //partial void OnCanworkfirstshiftChanged();
        //partial void OnCanworksecondshiftChanging(bool value);
        //partial void OnCanworksecondshiftChanged();
        //partial void OnCanworkthirdshiftChanging(bool value);
        //partial void OnCanworkthirdshiftChanged();
        //partial void OnWorkexperienceChanging(string value);
        //partial void OnWorkexperienceChanged();
        //partial void OnFulltimeorparttimeChanging(char value);
        //partial void OnFulltimeorparttimeChanged();
        //partial void OnIsattendingschoolChanging(bool value);
        //partial void OnIsattendingschoolChanged();
        //partial void OnSchoolsandscheduleChanging(string value);
        //partial void OnSchoolsandscheduleChanged();
        //partial void OnHasheavyequipexpChanging(bool value);
        //partial void OnHasheavyequipexpChanged();
        //partial void OnHeavyequipexpChanging(string value);
        //partial void OnHeavyequipexpChanged();
        //partial void OnHasvaliddriverslicenseChanging(bool value);
        //partial void OnHasvaliddriverslicenseChanged();
        //partial void OnHastransportationChanging(bool value);
        //partial void OnHastransportationChanged();
        //partial void OnSsnChanging(string value);
        //partial void OnSsnChanged();
        //partial void OnCurrentaddressChanging(string value);
        //partial void OnCurrentaddressChanged();
        //partial void OnCurrentcityChanging(string value);
        //partial void OnCurrentcityChanged();
        //partial void OnCurrentstateChanging(string value);
        //partial void OnCurrentstateChanged();
        //partial void OnCurrentzipChanging(string value);
        //partial void OnCurrentzipChanged();
        //partial void OnPermaddressChanging(string value);
        //partial void OnPermaddressChanged();
        //partial void OnPermcityChanging(string value);
        //partial void OnPermcityChanged();
        //partial void OnPermstateChanging(string value);
        //partial void OnPermstateChanged();
        //partial void OnPermzipChanging(string value);
        //partial void OnPermzipChanged();
        //partial void OnHasphysicallimitationsChanging(bool value);
        //partial void OnHasphysicallimitationsChanged();
        //partial void OnPhysicallimitationsChanging(string value);
        //partial void OnPhysicallimitationsChanged();
        //partial void OnPositiondesiredChanging(string value);
        //partial void OnPositiondesiredChanged();
        //partial void OnPreferredworklocationChanging(string value);
        //partial void OnPreferredworklocationChanged();
        //partial void OnAlternateworklocationsChanging(string value);
        //partial void OnAlternateworklocationsChanged();
        //partial void OnPhonenumberChanging(string value);
        //partial void OnPhonenumberChanged();
        //partial void OnWantsdayChanging(bool value);
        //partial void OnWantsdayChanged();
        //partial void OnWantsnightChanging(bool value);
        //partial void OnWantsnightChanged();
        //partial void OnDatecanstartChanging(System.DateTime value);
        //partial void OnDatecanstartChanged();
        //partial void OnDesiredsalaryChanging(int value);
        //partial void OnDesiredsalaryChanged();
        //partial void OnHasappliedbeforeChanging(bool value);
        //partial void OnHasappliedbeforeChanged();
        //partial void OnIsconvictedofafelonyChanging(bool value);
        //partial void OnIsconvictedofafelonyChanged();
        //partial void OnFelonymonthChanging(System.Nullable<int> value);
        //partial void OnFelonymonthChanged();
        //partial void OnFelonyyearChanging(System.Nullable<int> value);
        //partial void OnFelonyyearChanged();
        //partial void OnElemschoolnameaddrChanging(string value);
        //partial void OnElemschoolnameaddrChanged();
        //partial void OnElemschoolyearsChanging(System.Nullable<int> value);
        //partial void OnElemschoolyearsChanged();
        //partial void OnElemschoolgradChanging(System.Nullable<bool> value);
        //partial void OnElemschoolgradChanged();
        //partial void OnElemschoolsubjChanging(string value);
        //partial void OnElemschoolsubjChanged();
        //partial void OnMidschoolnameaddrChanging(string value);
        //partial void OnMidschoolnameaddrChanged();
        //partial void OnMidschoolyearsChanging(System.Nullable<int> value);
        //partial void OnMidschoolyearsChanged();
        //partial void OnMidschoolgradChanging(System.Nullable<bool> value);
        //partial void OnMidschoolgradChanged();
        //partial void OnMidschoolsubjChanging(string value);
        //partial void OnMidschoolsubjChanged();
        //partial void OnHighschoolnameaddrChanging(string value);
        //partial void OnHighschoolnameaddrChanged();
        //partial void OnHighschoolyearsChanging(System.Nullable<int> value);
        //partial void OnHighschoolyearsChanged();
        //partial void OnHighschoolgradChanging(System.Nullable<bool> value);
        //partial void OnHighschoolgradChanged();
        //partial void OnHighschoolsubjChanging(string value);
        //partial void OnHighschoolsubjChanged();
        //partial void OnCollegenameaddrChanging(string value);
        //partial void OnCollegenameaddrChanged();
        //partial void OnCollegeyearsChanging(System.Nullable<int> value);
        //partial void OnCollegeyearsChanged();
        //partial void OnCollegegradChanging(System.Nullable<bool> value);
        //partial void OnCollegegradChanged();
        //partial void OnCollegesubjChanging(string value);
        //partial void OnCollegesubjChanged();
        //partial void OnTradenameaddrChanging(string value);
        //partial void OnTradenameaddrChanged();
        //partial void OnTradeyearsChanging(System.Nullable<int> value);
        //partial void OnTradeyearsChanged();
        //partial void OnTradegradChanging(System.Nullable<bool> value);
        //partial void OnTradegradChanged();
        //partial void OnTradesubjChanging(string value);
        //partial void OnTradesubjChanged();
        //partial void OnSpecialstudyorresearchChanging(string value);
        //partial void OnSpecialstudyorresearchChanged();
        //partial void OnSpecialskillsChanging(string value);
        //partial void OnSpecialskillsChanged();
        //partial void OnActivitiesChanging(string value);
        //partial void OnActivitiesChanged();
        //partial void OnMilitaryserviceChanging(string value);
        //partial void OnMilitaryserviceChanged();
        //partial void OnRankChanging(string value);
        //partial void OnRankChanged();
        //partial void OnNationalguardorreservesChanging(string value);
        //partial void OnNationalguardorreservesChanged();
        //partial void OnEmp1frommonthChanging(System.Nullable<int> value);
        //partial void OnEmp1frommonthChanged();
        //partial void OnEmp1fromyearChanging(System.Nullable<int> value);
        //partial void OnEmp1fromyearChanged();
        //partial void OnEmp1tomonthChanging(System.Nullable<int> value);
        //partial void OnEmp1tomonthChanged();
        //partial void OnEmp1toyearChanging(System.Nullable<int> value);
        //partial void OnEmp1toyearChanged();
        //partial void OnEmp1nameChanging(string value);
        //partial void OnEmp1nameChanged();
        //partial void OnEmp1phoneChanging(string value);
        //partial void OnEmp1phoneChanged();
        //partial void OnEmp1positionChanging(string value);
        //partial void OnEmp1positionChanged();
        //partial void OnEmp1salaryChanging(System.Nullable<int> value);
        //partial void OnEmp1salaryChanged();
        //partial void OnEmp1reasonChanging(string value);
        //partial void OnEmp1reasonChanged();
        //partial void OnEmp2frommonthChanging(System.Nullable<int> value);
        //partial void OnEmp2frommonthChanged();
        //partial void OnEmp2fromyearChanging(System.Nullable<int> value);
        //partial void OnEmp2fromyearChanged();
        //partial void OnEmp2tomonthChanging(System.Nullable<int> value);
        //partial void OnEmp2tomonthChanged();
        //partial void OnEmp2toyearChanging(System.Nullable<int> value);
        //partial void OnEmp2toyearChanged();
        //partial void OnEmp2nameChanging(string value);
        //partial void OnEmp2nameChanged();
        //partial void OnEmp2phoneChanging(string value);
        //partial void OnEmp2phoneChanged();
        //partial void OnEmp2positionChanging(string value);
        //partial void OnEmp2positionChanged();
        //partial void OnEmp2salaryChanging(System.Nullable<int> value);
        //partial void OnEmp2salaryChanged();
        //partial void OnEmp2reasonChanging(string value);
        //partial void OnEmp2reasonChanged();
        //partial void OnEmp3frommonthChanging(System.Nullable<int> value);
        //partial void OnEmp3frommonthChanged();
        //partial void OnEmp3fromyearChanging(System.Nullable<int> value);
        //partial void OnEmp3fromyearChanged();
        //partial void OnEmp3tomonthChanging(System.Nullable<int> value);
        //partial void OnEmp3tomonthChanged();
        //partial void OnEmp3toyearChanging(System.Nullable<int> value);
        //partial void OnEmp3toyearChanged();
        //partial void OnEmp3nameChanging(string value);
        //partial void OnEmp3nameChanged();
        //partial void OnEmp3phoneChanging(string value);
        //partial void OnEmp3phoneChanged();
        //partial void OnEmp3positionChanging(string value);
        //partial void OnEmp3positionChanged();
        //partial void OnEmp3salaryChanging(System.Nullable<int> value);
        //partial void OnEmp3salaryChanged();
        //partial void OnEmp3reasonChanging(string value);
        //partial void OnEmp3reasonChanged();
        //partial void OnEmp4frommonthChanging(System.Nullable<int> value);
        //partial void OnEmp4frommonthChanged();
        //partial void OnEmp4fromyearChanging(System.Nullable<int> value);
        //partial void OnEmp4fromyearChanged();
        //partial void OnEmp4tomonthChanging(System.Nullable<int> value);
        //partial void OnEmp4tomonthChanged();
        //partial void OnEmp4toyearChanging(System.Nullable<int> value);
        //partial void OnEmp4toyearChanged();
        //partial void OnEmp4nameChanging(string value);
        //partial void OnEmp4nameChanged();
        //partial void OnEmp4phoneChanging(string value);
        //partial void OnEmp4phoneChanged();
        //partial void OnEmp4positionChanging(string value);
        //partial void OnEmp4positionChanged();
        //partial void OnEmp4salaryChanging(System.Nullable<int> value);
        //partial void OnEmp4salaryChanged();
        //partial void OnEmp4reasonChanging(string value);
        //partial void OnEmp4reasonChanged();
        //partial void OnEmp5frommonthChanging(System.Nullable<int> value);
        //partial void OnEmp5frommonthChanged();
        //partial void OnEmp5fromyearChanging(System.Nullable<int> value);
        //partial void OnEmp5fromyearChanged();
        //partial void OnEmp5tomonthChanging(System.Nullable<int> value);
        //partial void OnEmp5tomonthChanged();
        //partial void OnEmp5toyearChanging(System.Nullable<int> value);
        //partial void OnEmp5toyearChanged();
        //partial void OnEmp5nameChanging(string value);
        //partial void OnEmp5nameChanged();
        //partial void OnEmp5phoneChanging(string value);
        //partial void OnEmp5phoneChanged();
        //partial void OnEmp5positionChanging(string value);
        //partial void OnEmp5positionChanged();
        //partial void OnEmp5salaryChanging(System.Nullable<int> value);
        //partial void OnEmp5salaryChanged();
        //partial void OnEmp5reasonChanging(string value);
        //partial void OnEmp5reasonChanged();
        //partial void OnEmp6frommonthChanging(System.Nullable<int> value);
        //partial void OnEmp6frommonthChanged();
        //partial void OnEmp6fromyearChanging(System.Nullable<int> value);
        //partial void OnEmp6fromyearChanged();
        //partial void OnEmp6tomonthChanging(System.Nullable<int> value);
        //partial void OnEmp6tomonthChanged();
        //partial void OnEmp6toyearChanging(System.Nullable<int> value);
        //partial void OnEmp6toyearChanged();
        //partial void OnEmp6nameChanging(string value);
        //partial void OnEmp6nameChanged();
        //partial void OnEmp6phoneChanging(string value);
        //partial void OnEmp6phoneChanged();
        //partial void OnEmp6positionChanging(string value);
        //partial void OnEmp6positionChanged();
        //partial void OnEmp6salaryChanging(System.Nullable<int> value);
        //partial void OnEmp6salaryChanged();
        //partial void OnEmp6reasonChanging(string value);
        //partial void OnEmp6reasonChanged();
        //partial void OnCancontactempChanging(System.Nullable<bool> value);
        //partial void OnCancontactempChanged();
        //partial void OnCantcontactreasonChanging(string value);
        //partial void OnCantcontactreasonChanged();
        //partial void OnRef1nameChanging(string value);
        //partial void OnRef1nameChanged();
        //partial void OnRef1phoneChanging(string value);
        //partial void OnRef1phoneChanged();
        //partial void OnRef1businessChanging(string value);
        //partial void OnRef1businessChanged();
        //partial void OnRef1yearsChanging(System.Nullable<int> value);
        //partial void OnRef1yearsChanged();
        //partial void OnRef2nameChanging(string value);
        //partial void OnRef2nameChanged();
        //partial void OnRef2phoneChanging(string value);
        //partial void OnRef2phoneChanged();
        //partial void OnRef2businessChanging(string value);
        //partial void OnRef2businessChanged();
        //partial void OnRef2yearsChanging(System.Nullable<int> value);
        //partial void OnRef2yearsChanged();
        //partial void OnRef3nameChanging(string value);
        //partial void OnRef3nameChanged();
        //partial void OnRef3phoneChanging(string value);
        //partial void OnRef3phoneChanged();
        //partial void OnRef3businessChanging(string value);
        //partial void OnRef3businessChanged();
        //partial void OnRef3yearsChanging(System.Nullable<int> value);
        //partial void OnRef3yearsChanged();
        //partial void OnIcecontactnameChanging(string value);
        //partial void OnIcecontactnameChanged();
        //partial void OnIcecontactaddrChanging(string value);
        //partial void OnIcecontactaddrChanged();
        //partial void OnIcecontactphoneChanging(string value);
        //partial void OnIcecontactphoneChanged();
        //partial void OnSigdateChanging(System.Nullable<System.DateTime> value);
        //partial void OnSigdateChanged();
        //partial void OnSignatureChanging(string value);
        //partial void OnSignatureChanged();
        //partial void OnResultinterviewedbyChanging(string value);
        //partial void OnResultinterviewedbyChanged();
        //partial void OnResultdateChanging(System.Nullable<System.DateTime> value);
        //partial void OnResultdateChanged();
        //partial void OnResultremarksChanging(string value);
        //partial void OnResultremarksChanged();
        //partial void OnResultneatnessChanging(string value);
        //partial void OnResultneatnessChanged();
        //partial void OnResultabilityChanging(string value);
        //partial void OnResultabilityChanged();
        //partial void OnResultishiredChanging(System.Nullable<bool> value);
        //partial void OnResultishiredChanged();
        //partial void OnResultpositionChanging(string value);
        //partial void OnResultpositionChanged();
        //partial void OnResultdeptChanging(string value);
        //partial void OnResultdeptChanged();
        //partial void OnResultsalaryChanging(System.Nullable<float> value);
        //partial void OnResultsalaryChanged();
        //partial void OnResultstartdateChanging(System.Nullable<System.DateTime> value);
        //partial void OnResultstartdateChanged();
        //partial void OnResultChanging(string value);
        //partial void OnResultChanged();
        //partial void OnResultapproval1Changing(string value);
        //partial void OnResultapproval1Changed();
        //partial void OnResultapproval2Changing(string value);
        //partial void OnResultapproval2Changed();
        //partial void OnResultapproval3Changing(string value);
        //partial void OnResultapproval3Changed();
        //partial void OnResultmanagerChanging(string value);
        //partial void OnResultmanagerChanged();
        //partial void OnResultdeptheadChanging(string value);
        //partial void OnResultdeptheadChanged();
        //partial void OnIs18yearsoldChanging(bool value);
        //partial void OnIs18yearsoldChanged();
        //partial void OnDrugprintnameChanging(string value);
        //partial void OnDrugprintnameChanged();
        //partial void OnDrugemailChanging(string value);
        //partial void OnDrugemailChanged();
        //partial void OnDrugdateChanging(System.DateTime value);
        //partial void OnDrugdateChanged();
        //partial void OnReferredbyChanging(string value);
        //partial void OnReferredbyChanged();
        #endregion

        public static List<ATGDB.Jobapplication> GetJobApplicationsByParams(DateTime dtFrom, DateTime dtTo, string lastName, string sSortFieldName) {
            ATGDataContext dc = new ATGDataContext();
            var data = from x in dc.Jobapplications
                       where x.Createdate.Value.Date >= dtFrom.Date
                       where x.Createdate.Value.Date <= dtTo.Date
                       where ((lastName != "") && (lastName != null)) ? x.Lastname == lastName : true
                       select x;
            
            return data.ToList<ATGDB.Jobapplication>();
        }
        public static DataView GetJobApplicationsByParamsDS(DateTime dtFrom, DateTime dtTo, string lastName, string sSortFieldName) {
            List<ATGDB.Jobapplication> list = GetJobApplicationsByParams(dtFrom, dtTo, lastName, sSortFieldName);

            DataSet ds = GetJobAppDataSet();
            DataTable tbl = ds.Tables[0];
            foreach (ATGDB.Jobapplication ja in list) {
                tbl.Rows.Add(new Object[] { 
                    ja.Jobapplicationid,
                    ja.Sigdate,
                    ja.Firstname,
                    ja.Middlename,
                    ja.Lastname,
                    ja.Signature,
                    ja.Phonenumber,
                    ja.Currentaddress,
                    ja.Currentcity,
                    ja.Currentstate,
                    ja.Currentzip,
                    ja.Guidkey                
                });
            }

            DataView view = tbl.AsDataView();
            view.Sort = sSortFieldName;
            view.ApplyDefaultSort = false;
            return view;
        }
        private static DataSet GetJobAppDataSet() {        // Create a dataset.
            DataSet ds = new DataSet();
            // Add a table.
            DataTable tbl = ds.Tables.Add("Job Applications");
            // Create the columns.
            DataColumn pk0 = tbl.Columns.Add("Jobapplicationid", typeof(Int32));
            tbl.Columns.Add("Sigdate", typeof(DateTime));
            tbl.Columns.Add("Firstname", typeof(string));
            tbl.Columns.Add("Middlename", typeof(string));
            tbl.Columns.Add("Lastname", typeof(string));
            tbl.Columns.Add("Signature", typeof(string));
            tbl.Columns.Add("Phonenumber", typeof(string));
            tbl.Columns.Add("Currentaddress", typeof(string));
            tbl.Columns.Add("Currentcity", typeof(string));
            tbl.Columns.Add("Currentstate", typeof(string));
            tbl.Columns.Add("Currentzip", typeof(string));
            tbl.Columns.Add("Guidkey", typeof(string));
            // Set primary key.
            tbl.PrimaryKey = new[] { pk0 };
            return ds;
        }
    }
}