﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Equipmenttype
/// </summary>
namespace ATGDB
{
    public partial class Equipmenttype
    {
        public static IEnumerable<ATGDB.Equipmenttype> GetEquipmentTypesByEquipmentId(Int32 equipmentid) {
            ATGDataContext dc = new ATGDataContext();
            ATGDB.Equipment e = dc.Equipments.SingleOrDefault(x => x.Equipmentid.Equals(equipmentid));
            var data = from et in dc.Equipmenttypes
                       where et.Customerid == e.Customerid
                       select et;
            return data;
        }

        public static IEnumerable<ATGDB.Equipmenttype> GetEquipmentTypesByCustomerId(Int32 customerid) {
            ATGDataContext dc = new ATGDataContext();
            var data = from et in dc.Equipmenttypes
                       where et.Customerid == customerid
                       orderby et.Description
                       select et;
            return data;
        }
    }
}