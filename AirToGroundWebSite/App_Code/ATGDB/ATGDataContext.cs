﻿using System;
using System.Linq;
                               
/// <summary>
/// Summary description for ATGDataContext
/// </summary>
/// 
namespace ATGDB
{
    public partial class ATGDataContext
    {
        #region Extensibility Method Definitions

        //partial void OnCreated();
        //partial void OnSubmitError(Devart.Data.Linq.SubmitErrorEventArgs args);

        //partial void InsertAspnetUser(AspnetUser instance);
        //partial void UpdateAspnetUser(AspnetUser instance);
        //partial void DeleteAspnetUser(AspnetUser instance);
        //partial void InsertAudittrail(Audittrail instance);
        //partial void UpdateAudittrail(Audittrail instance);
        //partial void DeleteAudittrail(Audittrail instance);
        //partial void InsertCompanycustomerjobjoin(Companycustomerjobjoin instance);
        //partial void UpdateCompanycustomerjobjoin(Companycustomerjobjoin instance);
        //partial void DeleteCompanycustomerjobjoin(Companycustomerjobjoin instance);
        //partial void InsertCompanycustomerjoin(Companycustomerjoin instance);
        //partial void UpdateCompanycustomerjoin(Companycustomerjoin instance);
        //partial void DeleteCompanycustomerjoin(Companycustomerjoin instance);
        //partial void InsertCompanycustomeruserjoin(Companycustomeruserjoin instance);
        //partial void UpdateCompanycustomeruserjoin(Companycustomeruserjoin instance);
        //partial void DeleteCompanycustomeruserjoin(Companycustomeruserjoin instance);
        //partial void InsertCompanydivision(Companydivision instance);
        //partial void UpdateCompanydivision(Companydivision instance);
        //partial void DeleteCompanydivision(Companydivision instance);
        //partial void InsertCustomer(Customer instance);
        //partial void UpdateCustomer(Customer instance);
        //partial void DeleteCustomer(Customer instance);
        //partial void InsertDivisionarea(Divisionarea instance);
        //partial void UpdateDivisionarea(Divisionarea instance);
        //partial void DeleteDivisionarea(Divisionarea instance);
        //partial void InsertEquipment(Equipment instance);
        //partial void UpdateEquipment(Equipment instance);
        //partial void DeleteEquipment(Equipment instance);
        //partial void InsertEquipmentstatuslog(Equipmentstatuslog instance);
        //partial void UpdateEquipmentstatuslog(Equipmentstatuslog instance);
        //partial void DeleteEquipmentstatuslog(Equipmentstatuslog instance);
        //partial void InsertEquipmenttype(Equipmenttype instance);
        //partial void UpdateEquipmenttype(Equipmenttype instance);
        //partial void DeleteEquipmenttype(Equipmenttype instance);
        //partial void InsertExtendeduser(Extendeduser instance);
        //partial void UpdateExtendeduser(Extendeduser instance);
        //partial void DeleteExtendeduser(Extendeduser instance);
        //partial void InsertJobapplication(Jobapplication instance);
        //partial void UpdateJobapplication(Jobapplication instance);
        //partial void DeleteJobapplication(Jobapplication instance);
        //partial void InsertJobcode(Jobcode instance);
        //partial void UpdateJobcode(Jobcode instance);
        //partial void DeleteJobcode(Jobcode instance);
        //partial void InsertJobratejoin(Jobratejoin instance);
        //partial void UpdateJobratejoin(Jobratejoin instance);
        //partial void DeleteJobratejoin(Jobratejoin instance);
        //partial void InsertRatedata(Ratedata instance);
        //partial void UpdateRatedata(Ratedata instance);
        //partial void DeleteRatedata(Ratedata instance);
        //partial void InsertSection(Section instance);
        //partial void UpdateSection(Section instance);
        //partial void DeleteSection(Section instance);
        //partial void InsertService(Service instance);
        //partial void UpdateService(Service instance);
        //partial void DeleteService(Service instance);
        //partial void InsertServicecategory(Servicecategory instance);
        //partial void UpdateServicecategory(Servicecategory instance);
        //partial void DeleteServicecategory(Servicecategory instance);
        //partial void InsertServiceequipmenttypejobcodejoin(Serviceequipmenttypejobcodejoin instance);
        //partial void UpdateServiceequipmenttypejobcodejoin(Serviceequipmenttypejobcodejoin instance);
        //partial void DeleteServiceequipmenttypejobcodejoin(Serviceequipmenttypejobcodejoin instance);
        //partial void InsertServiceexception(Serviceexception instance);
        //partial void UpdateServiceexception(Serviceexception instance);
        //partial void DeleteServiceexception(Serviceexception instance);
        //partial void InsertServicequeue(Servicequeue instance);
        //partial void UpdateServicequeue(Servicequeue instance);
        //partial void DeleteServicequeue(Servicequeue instance);
        //partial void InsertServicequeueitem(Servicequeueitem instance);
        //partial void UpdateServicequeueitem(Servicequeueitem instance);
        //partial void DeleteServicequeueitem(Servicequeueitem instance);
        partial void InsertServiceschedule(Serviceschedule instance) {
            instance.UpdDt = DateTime.Now;
            instance.UpdUserId = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            instance.ChangeHistory = "INSERT " + DateTime.Now.ToString() +
                              " Service: " + (instance.Service == null ? "none" : instance.Service.Description) +
                              " Service Date: " + instance.Servicedate.ToShortDateString() +
                              " Service Time: " + (instance.Servicetime == null ? "none" : instance.Servicetime.Value.ToShortTimeString()) +
                              " Completed Date: " + (instance.Completeddate == null ? "none" : instance.Completeddate.Value.ToShortDateString()) +
                              " Completed Time: " + (instance.Completeddate == null ? "none" : instance.Completedtime.Value.ToShortTimeString()) +
                              " Cancelled Date: " + (instance.Cancelleddate == null ? "none" : instance.Cancelleddate.Value.ToShortTimeString()) +
                              " Comments: " + instance.Comments +
                              " Upd Dt: " + instance.UpdDt +
                              " Upd User Id: " + instance.UpdUserId;
            base.ExecuteDynamicInsert(instance);
        }
        partial void UpdateServiceschedule(Serviceschedule instance) {
            instance.UpdDt = DateTime.Now;
            instance.UpdUserId = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            instance.ChangeHistory = instance.ChangeHistory + "\nUPDATE " + DateTime.Now.ToString() +
                              " Service: " + (instance.Service == null ? "none" : instance.Service.Description) +
                              " Service Date: " + instance.Servicedate.ToShortDateString() +
                              " Service Time: " + (instance.Servicetime == null ? "none" : instance.Servicetime.Value.ToShortTimeString()) +
                              " Completed Date: " + (instance.Completeddate == null ? "none" : instance.Completeddate.Value.ToShortDateString()) +
                              " Completed Time: " + (instance.Completedtime == null ? "none" : instance.Completedtime.Value.ToShortTimeString()) +
                              " Cancelled Date: " + (instance.Cancelleddate == null ? "none" : instance.Cancelleddate.Value.ToShortTimeString()) +
                              " Comments: " + instance.Comments +
                              " Upd Dt: " + instance.UpdDt +
                              " Upd User Id: " + instance.UpdUserId;
            base.ExecuteDynamicUpdate(instance);
        }
        //partial void DeleteServiceschedule(Serviceschedule instance);
        //partial void InsertServicesectionrule(Servicesectionrule instance);
        //partial void UpdateServicesectionrule(Servicesectionrule instance);
        //partial void DeleteServicesectionrule(Servicesectionrule instance);
        //partial void InsertServiceshortdescr(Serviceshortdescr instance);
        //partial void UpdateServiceshortdescr(Serviceshortdescr instance);
        //partial void DeleteServiceshortdescr(Serviceshortdescr instance);

        partial void InsertTimesheet(Timesheet instance) {
            instance.UpdDt = DateTime.Now;
            instance.UpdUserId = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            instance.ChangeHistory = "INSERT " + DateTime.Now.ToString() +
                              " Employee: " + MembershipHelper.GetUserById(instance.Userid).Extendeduser.Employeenumber +
                              " Name: " + MembershipHelper.GetUserById(instance.Userid).Extendeduser.Fullname +
                              " Company Division: " + Companydivision.GetCompanyDivisionById(instance.Companydivisionid.ToString()).Description +
                              " Customer: " + Customer.GetCustomerById(instance.Customerid.ToString()).Description + 
                              " Job Code: " + Jobcode.GetJobcodeById(instance.Jobcodeid.ToString()).Code +
                              " Quantity: " + instance.Quantity +
                              " Hours: " + instance.Hours +
                              " Start time: " + instance.Starttime +
                              " End time: " + instance.Endtime +
                              " Work Card #: " + (instance.Workcard == null ? "None" : instance.Workcard.Cardnumber) +
                              " Upd Dt: " + instance.UpdDt + 
                              " Upd User Id: " + instance.UpdUserId;
            base.ExecuteDynamicInsert(instance);
        }
        partial void UpdateTimesheet(Timesheet instance) {
            instance.UpdDt = DateTime.Now;
            instance.UpdUserId = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            instance.ChangeHistory = instance.ChangeHistory + "\nUPDATE " + DateTime.Now.ToString() +
                              " Employee: " + instance.Extendeduser.Employeenumber +
                              " Name: " + instance.Extendeduser.Fullname +
                              " Company Division: " + instance.Companydivision.Description +
                              " Customer: " + instance.Customer.Description +
                              " Job Code: " + Jobcode.GetJobcodeById(instance.Jobcodeid.ToString()).Code +
                              " Quantity: " + instance.Quantity +
                              " Hours: " + instance.Hours +
                              " Start time: " + instance.Starttime +
                              " End time: " + instance.Endtime +
                              " Work Card #: " + (instance.Workcard == null ? "None" : instance.Workcard.Cardnumber) +
                              " Upd Dt: " + instance.UpdDt +
                              " Upd User Id: " + instance.UpdUserId;
            base.ExecuteDynamicUpdate(instance);
        }
        //partial void DeleteTimesheet(Timesheet instance);

        partial void InsertWorkcard(Workcard instance) {
            instance.UpdDt = DateTime.Now;
            instance.UpdUserId = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            instance.ChangeHistory = "INSERT " + DateTime.Now.ToString() +
                              " Card Number: " + instance.Cardnumber +
                              " Description: " + instance.Description +
                              " Budget Amount: " + instance.Budgetamount +
                              " Budget Hours: " + instance.Budgethours +
                              " Completed: " + instance.Completed +
                              " Upd Dt: " + instance.UpdDt +
                              " Upd User Id: " + instance.UpdUserId;
            base.ExecuteDynamicInsert(instance);
        }
        partial void UpdateWorkcard(Workcard instance) {
            instance.UpdDt = DateTime.Now;
            instance.UpdUserId = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            instance.ChangeHistory = instance.ChangeHistory + "\nUPDATE " + DateTime.Now.ToString() +
                              " Card Number: " + instance.Cardnumber +
                              " Description: " + instance.Description +
                              " Budget Amount: " + instance.Budgetamount +
                              " Budget Hours: " + instance.Budgethours +
                              " Completed: " + instance.Completed +
                              " Upd Dt: " + instance.UpdDt +
                              " Upd User Id: " + instance.UpdUserId;
            base.ExecuteDynamicUpdate(instance);
        }
        //partial void DeleteWorkcard(Workcard instance);

        partial void InsertWorklog(Worklog instance) {
            instance.UpdDt = DateTime.Now;
            instance.UpdUserId = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            instance.ChangeHistory = instance.ChangeHistory + "INSERT " + DateTime.Now.ToString() +
                              " Job Code: " + Jobcode.GetJobcodeById(instance.Jobcodeid.ToString()).Code +
                              " Worked Date: " + instance.Workeddate +
                              " Quantity: " + instance.Quantity +
                              " Hours: " + instance.Hours +
                              " Start time: " + instance.Starttime +
                              " End time: " + instance.Endtime +
                              " Notes: " + instance.Notes +
                              " Upd Dt: " + instance.UpdDt +
                              " Upd User Id: " + instance.UpdUserId;
            base.ExecuteDynamicInsert(instance);
        }
        partial void UpdateWorklog(Worklog instance) {
            instance.UpdDt = DateTime.Now;
            instance.UpdUserId = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            instance.ChangeHistory = instance.ChangeHistory + "\nUPDATE " + DateTime.Now.ToString() +
                              " Job Code: " + Jobcode.GetJobcodeById(instance.Jobcodeid.ToString()).Code +
                              " Worked Date: " + instance.Workeddate +
                              " Quantity: " + instance.Quantity +
                              " Hours: " + instance.Hours +
                              " Start time: " + instance.Starttime +
                              " End time: " + instance.Endtime +
                              " Notes: " + instance.Notes +
                              " Upd Dt: " + instance.UpdDt +
                              " Upd User Id: " + instance.UpdUserId;
            base.ExecuteDynamicUpdate(instance);
        }
        //partial void DeleteWorklog(Worklog instance);

        //partial void InsertServicesectionjoin(Servicesectionjoin instance);
        //partial void UpdateServicesectionjoin(Servicesectionjoin instance);
        //partial void DeleteServicesectionjoin(Servicesectionjoin instance);

        #endregion
    }
}