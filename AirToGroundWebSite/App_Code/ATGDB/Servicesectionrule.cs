﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for ServiceSectionRule
/// </summary>
namespace ATGDB
{
    public partial class Servicesectionrule
    {
        public class MyRule
        {
            public string RuleAction { get; set; }
            public string Description { get; set; }
            public MyRule(string ruleAction, string description) {
                RuleAction = ruleAction;
                Description = description;
            }
        }
        public List<MyRule> GetRuleActionData() {
            List<MyRule> data = new List<MyRule>();
            // AUTO-SCHEDULE
            data.Add(new MyRule("AS", "AUTO-SCHEDULE Next Service"));
            // Like the 30-Day example, which on the 60-Day, get's closed, and the revenue is lost.
            data.Add(new MyRule("C2", "CLOSE the Exception, COMPLETE the Trigger Seq SS"));
            // This would never actually occur.
            //data.Add(new MyRule("X2", "CLOSE the Exception, CANCEL the Trigger Seq SS"));
            // Like the 60-Day Part-B scenario, which on the 90-Day gets completed, along with the 90-Day
            data.Add(new MyRule("C1", "COMPLETE the Exception, COMPLETE the Trigger Seq SS"));
            // Like the 60-Day Part-A scenario, which on the 90-Day gets completed, and replaces the 90-Day,
            // thus cancelling the 90-Day and the 90-Day revenue is lost.
            data.Add(new MyRule("C3", "COMPLETE the Exception, CANCEL the Trigger Seq SS"));
            return data;
        }

        // This is the main processing procedure. It is responsible for
        // processing the rules, and creating WorkLogs, completing Exceptions,
        // and Completing ServiceSchedules. Of course, anything can happen,
        // according to the rules. And, that's what this procedure does.
        public static void ProcessServiceSchedule(string ssId, string userName, 
                                                  DateTime workDate, DateTime? startTime, DateTime? endTime,
                                                  double wQty, string divisionAreaId, string notes,
                                                  string exSelections, string exNotes,
                                                  string exCompletedSelections, string completedNotes) {
            ATGDataContext db = new ATGDataContext();

            // Get the Serviceschedule.
            ATGDB.Serviceschedule ss = db.Serviceschedules.Single(x => x.Servicescheduleid == int.Parse(ssId));

            // Get the profile
            ProfileCommon profile = (ProfileCommon)ProfileCommon.Create(userName);
            profile.Initialize(userName, false);

            // Get the last service.
            ATGDB.Service lastSvc;
            if ((ss.Service.Sequence != null) && (ss.Service.Sequence > 0)) {
                lastSvc = Service.GetLastService(ss.Service.Sequence, ss.Equipment.Customerid);
            } else {
                // Latring, interior cleaning, or services that have no rules, and seq of 0
                lastSvc = ss.Service;
            }

            // Only items that have a non-zero sequence can have rules
            // that we are interested in processing in this method.
            // We are NOT processing AUTO-SCHEDULES here.
            if (ss.Service.Sequence > 0) {
                // Get all the rules for the customer.
                var rules = from r in db.Servicesectionrules
                            where r.Sectionid != null
                            where r.Section.Customerid == ss.Equipment.Customerid
                            where r.Triggerseq == ss.Service.Sequence
                            where r.Ruleaction.Contains("C")
                            orderby r.Triggerseq, r.Ingroup, r.Ruleaction
                            select r;

                // Here are the rules we will work with.
                IEnumerable<ATGDB.Servicesectionrule> filteredRules = null;

                // See if this is a group situation.
                bool mayBeGroup = ((exSelections != "") && (exSelections.IndexOf(":") > 0));

                // If it is a group situation, we need to see if there is a set of rules for the group.
                bool hasMatch = false;
                if (mayBeGroup == true) {
                    // Get the rules. 
                    StringTokenizer st = new StringTokenizer(exSelections, ':');
                    int cnt = 0;
                    foreach (string s in st) {
                        if (cnt == 0) {
                            filteredRules = rules.Where(x => x.Sectionid.ToString() == s && x.Ingroup == true);
                        } else {
                            filteredRules.Union(rules.Where(x => x.Sectionid.ToString() == s && x.Ingroup == true));
                        }
                        cnt++;
                    }

                    // Compare the count to the original number of exceptions.
                    if (filteredRules.Count() == st.Count) {
                        // We found an exact match. We will follow these rules.
                        // No more filtering necessary.
                        hasMatch = true;
                    } else if (filteredRules.Count() > st.Count) {
                        // There's more than one match. We have to look for which
                        // is the most recent closed service schedule, and use
                        // that set of rules. Then, we have to check again, to
                        // see if that set is a match.
                        int svcId = 0;
                        DateTime cDt = DateTime.Parse("1/1/0001 12:00:00 AM");
                        DateTime tmpDt = DateTime.Parse("1/1/0001 12:00:00 AM");
                        var svcs = filteredRules.GroupBy(s => s.Serviceid);
                        foreach (ATGDB.Servicesectionrule z in svcs) {
                            var sss = db.Serviceschedules.Where(s => s.Serviceid == z.Serviceid && s.Completeddate != null);
                            if (sss.Count() > 0) {
                                tmpDt = (DateTime)sss.Max(x => x.Completeddate);
                                if (tmpDt > cDt) {
                                    cDt = tmpDt;
                                    svcId = z.Serviceid;
                                }
                            }
                        }
                        if (svcId > 0) {
                            // Get the service schedule for the date and service id.
                            ATGDB.Serviceschedule ss2 = db.Serviceschedules.Single(s => s.Serviceid == svcId && s.Servicedate == cDt);

                            // Filter the rules based on this service schedule.
                            filteredRules = rules.Where(x => x.Serviceid == ss2.Serviceid && x.Ingroup == true);

                            // Finally, check the counts.
                            hasMatch = (filteredRules.Count() == st.Count);
                        } else {
                            hasMatch = false;
                        }

                        if (hasMatch == false) {
                            // No match. So, filter out the groups.
                            filteredRules = rules.Where(x => x.Ingroup == false);
                        }
                    } else {
                        // No match. So, filter out the groups.
                        filteredRules = rules.Where(x => x.Ingroup == false);
                    }
                } else {
                    // No Group. So, filter out the groups.
                    filteredRules = rules.Where(x => x.Ingroup == false);
                }

                // Set the division area
                if (!String.IsNullOrEmpty(divisionAreaId)) {
                    ss.Divisionareaid = int.Parse(divisionAreaId);
                }

                // Let's create the work log now. We need the id later on.
                ss.Companydivisionid = int.Parse(profile.Companydivisionid);
                int workLogId = Worklog.CreateWorkLog(ss, profile.Userid, profile.UserName, workDate, startTime, endTime, wQty, notes, exSelections, exNotes);

                // If we didn't have any group matches, we still need to filter to the rules we need.
                if (hasMatch == false) {

                    int svcId = 0;
                    DateTime cDt = DateTime.Parse("1/1/0001 12:00:00 AM");
                    DateTime tmpDt = DateTime.Parse("1/1/0001 12:00:00 AM");
                    foreach (ATGDB.Servicesectionrule z in filteredRules) {
                        var sss = db.Serviceschedules.Where(s => s.Serviceid == z.Serviceid && s.Completeddate != null);
                        if (sss.Count() > 0) {
                            tmpDt = (DateTime)sss.Max(x => x.Completeddate);
                            if (tmpDt > cDt) {
                                cDt = tmpDt;
                                svcId = z.Serviceid;
                            }
                        }
                    }
                    if (svcId > 0) {
                        // Get the service schedule for the date and service id.
                        //ATGDB.Serviceschedule ss2 = db.Serviceschedules.Single(s => s.Serviceid == svcId && s.Servicedate == cDt);

                        // Filter the rules based on this service schedule.
                        filteredRules = rules.Where(x => x.Serviceid == svcId && x.Ingroup == false);
                    }
                }

                // We have the correct rules in filteredRules now.
                // We have the current service schedule.
                // If there are rules, apply them to the exceptions.
                if (filteredRules.Count() > 0) {
                    foreach (ATGDB.Servicesectionrule r in filteredRules) {
                        // Service id being worked on.
                        //int seq = r.Triggerseq;

                        // Get the open Exceptions for the last service's id.
                        var sex = from ex in db.Serviceexceptions
                                  where ex.Equipmentid == ss.Equipmentid
                                  where ex.Serviceid == lastSvc.Serviceid
                                  where ex.Completeddate == null
                                  where ex.Closeddate == null
                                  where ex.Originalworklogid != workLogId
                                  select ex;

                        // For exception matching the current rule, apply the rule.
                        if (sex.Count() > 0) {
                            foreach (ATGDB.Serviceexception x in sex) {
                                //if (x.Service.Sequence == seq) {
                                if (true == true) {
                                    // Do what the rule says to do.
                                    bool alreadyCompletedDontCancel = false;

                                    // If the user indicated that the service exception was 
                                    // completed (e.g. checked in the UI), then apply
                                    // the rules.
                                    if (exCompletedSelections.Contains(x.Sectionid.ToString())) {
                                        if (r.Ruleaction == "C1") {
                                            //"COMPLETE the Exception, COMPLETE the Trigger Seq SS"
                                            x.Completeddate = workDate;
                                            x.Completedby = userName;
                                            x.Completednotes = completedNotes;
                                            x.Completedworklogid = workLogId;
                                            // Clear the active exception flag.
                                            x.Serviceschedule.Hasactiveexception = false;
                                            // Complete the SS.
                                            ss.Companydivisionid = int.Parse(profile.Companydivisionid);
                                            ss.Completeddate = workDate;
                                            ss.Completedtime = endTime;
                                            ss.Comments = notes;
                                            // If was previously cancelled, un-cancel. 
                                            // Always favor completion over cancellation.
                                            if (ss.Cancelleddate != null) {
                                                ss.Cancelleddate = null;
                                            }
                                            alreadyCompletedDontCancel = true;
                                        } else if (r.Ruleaction == "C2") {
                                            //"CLOSE the Exception, COMPLETE the Trigger Seq SS"
                                            x.Closeddate = workDate;
                                            x.Closedby = userName;
                                            x.Closednotes = completedNotes;
                                            x.Closedworklogid = workLogId;
                                            // Clear the active exception flag.
                                            x.Serviceschedule.Hasactiveexception = false;
                                            // Complete the SS.
                                            ss.Companydivisionid = int.Parse(profile.Companydivisionid);
                                            ss.Completeddate = workDate;
                                            ss.Completedtime = endTime;
                                            ss.Comments = notes;
                                            // If was previously cancelled, un-cancel.
                                            // Always favor completion over cancellation.
                                            if (ss.Cancelleddate != null) {
                                                ss.Cancelleddate = null;
                                            }
                                            alreadyCompletedDontCancel = true;
                                        } else if (r.Ruleaction == "C3") {
                                            //"COMPLETE the Exception, CANCEL the Trigger Seq SS"
                                            x.Completeddate = workDate;
                                            x.Completedby = userName;
                                            x.Completednotes = completedNotes;
                                            x.Completedworklogid = workLogId;
                                            // Clear the active exception flag.
                                            x.Serviceschedule.Hasactiveexception = false;
                                            // Since the SS has already been completed, don't cancel it.
                                            if (alreadyCompletedDontCancel == false) {
                                                ss.Companydivisionid = int.Parse(profile.Companydivisionid);
                                                ss.Cancelleddate = workDate;
                                                ss.Comments = notes;
                                                //ss.Cancelednotes = notes;
                                            }
                                        }
                                    } else {
                                        // The user unchecked a previous exception.
                                        // This means it didn't get done. So, we will
                                        // Close it and complete the SS.
                                        x.Closeddate = workDate;
                                        x.Closedby = userName;
                                        x.Closednotes = completedNotes;
                                        // Clear the active exception flag anyway.
                                        x.Serviceschedule.Hasactiveexception = false;
                                        // Complete the SS.
                                        ss.Companydivisionid = int.Parse(profile.Companydivisionid);
                                        ss.Completeddate = workDate;
                                        ss.Completedtime = endTime;
                                        ss.Comments = notes;
                                        alreadyCompletedDontCancel = true;
                                    }

                                } // if
                            } // x loop

                        } else {
                            ss.Companydivisionid = int.Parse(profile.Companydivisionid);
                            ss.Completeddate = workDate;
                            ss.Completedtime = endTime;
                            ss.Comments = notes;
                        }

                    } // rules loop.

                    // Save all the changes.
                    db.SubmitChanges();

                } else {
                    // Getting here could mean the exceptions are 
                    // triggered in the next SS, spanning across service schedules
                    // to the next one, or beyond. So, just complete the SS.
                    // The WL was already created above, already.

                    // Complete the SS.
                    ss.Companydivisionid = int.Parse(profile.Companydivisionid);
                    ss.Completeddate = workDate;
                    ss.Completedtime = endTime;
                    ss.Comments = notes;

                    // Save all the changes.
                    db.SubmitChanges();
                }
            } else {
                // Items with a sequence of zero have no rules.
                // Complete or close all exceptions.

                // Set the division area
                if (!String.IsNullOrEmpty(divisionAreaId)) {
                    ss.Divisionareaid = int.Parse(divisionAreaId);
                }
                
                // Let's create the work log now. We need the id later on.
                ss.Companydivisionid = int.Parse(profile.Companydivisionid);
                int workLogId = Worklog.CreateWorkLog(ss, profile.Userid, profile.UserName, workDate, startTime, endTime, wQty, notes, exSelections, exNotes);

                // Get the open Exceptions for the last service's id.
                var sex = from ex in db.Serviceexceptions
                          where ex.Equipmentid == ss.Equipmentid
                          where ex.Serviceid == lastSvc.Serviceid
                          where ex.Completeddate == null
                          where ex.Closeddate == null
                          where ex.Originalworklogid != workLogId
                          select ex;

                // Process any Service Exceptions.
                if (sex.Count() > 0) {
                    foreach (ATGDB.Serviceexception x in sex) {
                        if (exCompletedSelections.Contains(x.Sectionid.ToString())) {
                            //"COMPLETE the Exception, COMPLETE the Trigger Seq SS"
                            x.Completeddate = workDate;
                            x.Completedby = userName;
                            x.Completednotes = completedNotes;
                            x.Completedworklogid = workLogId;
                            // If was previously cancelled, un-cancel. 
                            if (ss.Cancelleddate != null) {
                                ss.Cancelleddate = null;
                            }
                            // Clear the active exception flag.
                            x.Serviceschedule.Hasactiveexception = false;
                            // Complete the SS.
                            ss.Companydivisionid = int.Parse(profile.Companydivisionid);
                            ss.Completeddate = workDate;
                            ss.Completedtime = endTime;
                            ss.Comments = notes;
                        } else {
                            //"CLOSE the Exception, COMPLETE the Trigger Seq SS"
                            x.Closeddate = workDate;
                            x.Closedby = userName;
                            x.Closednotes = completedNotes;
                            x.Closedworklogid = workLogId;
                            // If was previously cancelled, un-cancel. 
                            if (ss.Completeddate != null) {
                                ss.Completeddate = null;
                            }
                            // Clear the active exception flag.
                            x.Serviceschedule.Hasactiveexception = false;
                            // Complete the SS.
                            ss.Companydivisionid = int.Parse(profile.Companydivisionid);
                            ss.Completeddate = workDate;
                            ss.Completedtime = endTime;
                            ss.Comments = notes;
                        }
                    }
                } else {
                    // Complete the SS.
                    ss.Companydivisionid = int.Parse(profile.Companydivisionid);
                    ss.Completeddate = workDate;
                    ss.Completedtime = endTime;
                    ss.Comments = notes;
                }
                // Save all the changes.
                db.SubmitChanges();

                // If was COMPLETED, specifically, let's AUTOMATICALLY schedule the next one
                if (ss.Completeddate != null) {
                    // Auto-schedule
                    AutoSchedule(ss);
                }
            }
        }
        // This method looks up the SS and will Auto-Schedule the next one.
        // The input SS must be completed to Auto-Schedule the next one.
        public static void AutoSchedule(ATGDB.Serviceschedule ss) {
            if (ss.Completeddate != null) {
                ATGDataContext db = new ATGDataContext();
                // Get the AutoSchedule rules for this service
                var rules = from r in db.Servicesectionrules
                            where r.Serviceid == ss.Serviceid
                            where r.Sectionid == 999999 // Do not Change - used in Serviceschedule.cs also
                            where r.Ruleaction == "AS"
                            orderby r.Triggerseq, r.Ingroup, r.Ruleaction
                            select r;

                // If there are rules for this service,
                if (rules.Count() > 0) {
                    // There could be more than one rule. A single service
                    // could trigger more than one service.
                    ATGDB.Serviceschedule newSS;
                    bool needsCommit = false;
                    foreach (ATGDB.Servicesectionrule rule in rules) {
                        // Using the rule's trigger sequence, get the next
                        // possible service to schedule. Note, if the 
                        // trigger sequence is zero, this means "yourself" - e.g. same service only.
                        if (rule.Triggerseq == 0) {
                            // Create the next schedule like the input service, based on input service's interval
                            needsCommit = true;
                            newSS = new ATGDB.Serviceschedule();
                            //newSS.Companydivisionid = ss.Companydivisionid;
                            newSS.Equipmentid = ss.Equipmentid;
                            newSS.Serviceid = ss.Serviceid;
                            // Set the next date based on the COMPLETED date and CURRENT SERVICE'S INTERVAL
                            newSS.Servicedate = ss.Completeddate.Value.Date.AddDays(ss.Service.Serviceinterval);
                            newSS.Servicetime = ss.Servicetime;
                            // Insert it.
                            db.Serviceschedules.InsertOnSubmit(newSS);
                        } else {
                            // Create the next schedule.
                            // Use the trigger sequence to find the next service to schedule.
                            ATGDB.Service svc = db.Services.SingleOrDefault(x => x.Customerid == ss.Equipment.Customerid &&
                                                                                 x.Sequence > 0 &&
                                                                                 x.Sequence == rule.Triggerseq);
                            needsCommit = true;
                            newSS = new ATGDB.Serviceschedule();
                            //newSS.Companydivisionid = ss.Companydivisionid;
                            newSS.Equipmentid = ss.Equipmentid;
                            newSS.Serviceid = svc.Serviceid;
                            // Set the next date based on the COMPLETED date and NEXT SERVICE'S INTERVAL
                            newSS.Servicedate = ss.Completeddate.Value.Date.AddDays(svc.Serviceinterval);
                            newSS.Servicetime = ss.Servicetime;
                            // Insert it.
                            db.Serviceschedules.InsertOnSubmit(newSS);
                        }
                    }
                    if (needsCommit == true) {
                        db.SubmitChanges();
                    }
                }
            }
        }
    }

}