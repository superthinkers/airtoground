﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Ratedata
/// </summary>
namespace ATGDB
{
    public partial class Ratedata
    {
        public const string SESSION_RATESCHEDULE_REFRESH = "SESSION_RATESCHEDULE_REFRESH";
        public class MyJobRateJoinResult
        {
            public int Jobratejoinid { get; set; }
            public int Jobcodeid { get; set; }
            public int Rateid { get; set; }
            public int? Companydivisionid { get; set; }
            public int? Customerid { get; set; }
            public string CompanyDescr { get; set; }
            public string CustomerDescr { get; set; }
            public string Keys {
                get {
                    return Companydivisionid.ToString() + ":" + Customerid.ToString();
                }
            }
            public string Description {
                get {
                    return CompanyDescr + " / " + CustomerDescr;
                }
            }
            public string Jobcodedescr { get; set; }
            public string Ratedescr { get; set; }
            public string Flatfeeorhourly { get; set; }
            public double Cost { get; set; }
            public double Rate { get; set; }
            public double Stdhours { get; set; }
            public double Stdquantity { get; set; }
            public string Changehistory { get; set; }
            public DateTime Upddt { get; set; }
            public string Upduserid { get; set; }
            public string Imagesrc { get; set; }

            public MyJobRateJoinResult() {
            }
        }

        // Method to find the rates.
        public static Ratedata GetRatedataByVals(int jobcodeid, int compid, int custid) {
            ATGDataContext db = new ATGDataContext();
            Jobratejoin jrj = db.Jobratejoins.SingleOrDefault(x => x.Jobcodeid == jobcodeid &&
                                                             (x.Companydivisionid == compid) &&
                                                             (x.Customerid == custid));
            if (jrj != null) {
                // This would represent a comp/cust override;
                return jrj.Ratedata;
            } else {
                // No comp/cust. Check for cust override;
                jrj = db.Jobratejoins.SingleOrDefault(x => x.Jobcodeid == jobcodeid &&
                                                     (x.Companydivisionid == null) &&
                                                     (x.Customerid == custid));
                if (jrj != null) {
                    // This would represent a cust override;
                    return jrj.Ratedata;
                } else {
                    // No cust. Check for comp override;
                    jrj = db.Jobratejoins.SingleOrDefault(x => x.Jobcodeid == jobcodeid &&
                                                         (x.Companydivisionid == compid) &&
                                                         (x.Customerid == null));
                    if (jrj != null) {
                        // This would represent a comp override;
                        return jrj.Ratedata;
                    } else {
                        // No comp. Return the corp rate. Must exist;
                        jrj = db.Jobratejoins.SingleOrDefault(x => x.Jobcodeid == jobcodeid &&
                                                             (x.Companydivisionid == null) &&
                                                             (x.Customerid == null));
                        return jrj.Ratedata;
                    }
                }
            }
        }

        public static List<MyJobRateJoinResult> GetRatedataByCompCustKeys(string keys) {
            ATGDataContext dc = new ATGDataContext();

            // Break up the keys. Similar idea in Timesheets
            string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
            string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);

            // Get all the data, starting with the most common, to the most discrete.
            // This would be corporate.
            var data1 = from jc in dc.Companycustomerjobjoins
                        where jc.Companydivisionid == int.Parse(companyDivisionId)
                        where jc.Customerid == int.Parse(customerId)
                        from jrj in dc.Jobratejoins
                        where jrj.Jobcodeid == jc.Jobcodeid
                        where jrj.Companydivisionid == null
                        where jrj.Customerid == null
                        from r in dc.Ratedatas
                        where r.Rateid == jrj.Rateid
                        where r.Isactive == true
                        select new MyJobRateJoinResult {
                            Jobratejoinid = jrj.Jobratejoinid,
                            Jobcodeid = jrj.Jobcodeid,
                            Rateid = jrj.Rateid,
                            Companydivisionid = null,
                            Customerid = null,
                            CompanyDescr = "All Companies",
                            CustomerDescr = "All Customers",
                            Jobcodedescr = jrj.Jobcode.Description,
                            Ratedescr = r.Description,
                            Flatfeeorhourly = r.Flatfeeorhourly,
                            Cost = r.Cost,
                            Rate = r.Rate,
                            Stdhours = r.Stdhours,
                            Stdquantity = r.Stdquantity,
                            Changehistory = r.Changehistory,
                            Upddt = r.Upddt,
                            Upduserid = r.Upduserid,
                            Imagesrc = "../../App_Themes/Corp/Images/JobRate_Corp.bmp"
                        };

            // Company override.
            var data2 = from jc in dc.Companycustomerjobjoins
                        where jc.Companydivisionid == int.Parse(companyDivisionId)
                        where jc.Customerid == int.Parse(customerId)
                        from jrj in dc.Jobratejoins
                        where jrj.Jobcodeid == jc.Jobcodeid
                        where jrj.Companydivisionid == int.Parse(companyDivisionId)
                        where jrj.Customerid == null
                        from r in dc.Ratedatas
                        where r.Rateid == jrj.Rateid
                        where r.Isactive == true
                        select new MyJobRateJoinResult {
                            Jobratejoinid = jrj.Jobratejoinid,
                            Jobcodeid = jrj.Jobcodeid,
                            Rateid = jrj.Rateid,
                            Companydivisionid = jrj.Companydivisionid,
                            Customerid = null,
                            CompanyDescr = jrj.Companydivision.Description,
                            CustomerDescr = "All Customers",
                            Jobcodedescr = jrj.Jobcode.Description,
                            Ratedescr = r.Description,
                            Flatfeeorhourly = r.Flatfeeorhourly,
                            Cost = r.Cost,
                            Rate = r.Rate,
                            Stdhours = r.Stdhours,
                            Stdquantity = r.Stdquantity,
                            Changehistory = r.Changehistory,
                            Upddt = r.Upddt,
                            Upduserid = r.Upduserid,
                            Imagesrc = "../../App_Themes/Corp/Images/JobRate_Comp.bmp"
                        };

            // Customer override.
            var data3 = from jc in dc.Companycustomerjobjoins
                        where jc.Companydivisionid == int.Parse(companyDivisionId)
                        where jc.Customerid == int.Parse(customerId)
                        from jrj in dc.Jobratejoins
                        where jrj.Jobcodeid == jc.Jobcodeid
                        where jrj.Companydivisionid == null
                        where jrj.Customerid == int.Parse(customerId)
                        from r in dc.Ratedatas
                        where r.Rateid == jrj.Rateid
                        where r.Isactive == true
                        select new MyJobRateJoinResult {
                            Jobratejoinid = jrj.Jobratejoinid,
                            Jobcodeid = jrj.Jobcodeid,
                            Rateid = jrj.Rateid,
                            Companydivisionid = null,
                            Customerid = jrj.Customerid,
                            CompanyDescr = "All Companies",
                            CustomerDescr = jrj.Customer.Description,
                            Jobcodedescr = jrj.Jobcode.Description,
                            Ratedescr = r.Description,
                            Flatfeeorhourly = r.Flatfeeorhourly,
                            Cost = r.Cost,
                            Rate = r.Rate,
                            Stdhours = r.Stdhours,
                            Stdquantity = r.Stdquantity,
                            Changehistory = r.Changehistory,
                            Upddt = r.Upddt,
                            Upduserid = r.Upduserid,
                            Imagesrc = "../../App_Themes/Corp/Images/JobRate_Cust.bmp"
                        };

            // Company/Cust override.
            var data4 = from jc in dc.Companycustomerjobjoins
                        where jc.Companydivisionid == int.Parse(companyDivisionId)
                        where jc.Customerid == int.Parse(customerId)
                        from jrj in dc.Jobratejoins
                        where jrj.Jobcodeid == jc.Jobcodeid
                        where jrj.Companydivisionid == int.Parse(companyDivisionId)
                        where jrj.Customerid == int.Parse(customerId)
                        from r in dc.Ratedatas
                        where r.Rateid == jrj.Rateid
                        where r.Isactive == true
                        select new MyJobRateJoinResult {
                            Jobratejoinid = jrj.Jobratejoinid,
                            Jobcodeid = jrj.Jobcodeid,
                            Rateid = jrj.Rateid,
                            Companydivisionid = jrj.Companydivisionid,
                            Customerid = jrj.Customerid,
                            CompanyDescr = jrj.Companydivision.Description,
                            CustomerDescr = jrj.Customer.Description,
                            Jobcodedescr = jrj.Jobcode.Description,
                            Ratedescr = r.Description,
                            Flatfeeorhourly = r.Flatfeeorhourly,
                            Cost = r.Cost,
                            Rate = r.Rate,
                            Stdhours = r.Stdhours,
                            Stdquantity = r.Stdquantity,
                            Changehistory = r.Changehistory,
                            Upddt = r.Upddt,
                            Upduserid = r.Upduserid,
                            Imagesrc = "../../App_Themes/Corp/Images/JobRate_CompCust.bmp"
                        };

            // Now, eliminate the data, starting from the most common, working towards the most discrete.
            // The resulting list should be a complete list of job codes, with the overrides applied correctly.
            // The count of data1 should match the count of resultdata3, when done.

            // Customer Only
            Comparer1 comparer1 = new Comparer1();
            var resultdata1 = data2.ToList<MyJobRateJoinResult>().Union(data1, comparer1);

            // Company Only
            Comparer2 comparer2 = new Comparer2();
            var resultdata2 = data3.ToList<MyJobRateJoinResult>().Union(resultdata1, comparer2);

            // Company Customer
            Comparer3 comparer3 = new Comparer3();
            var resultdata3 = data4.ToList<MyJobRateJoinResult>().Union(resultdata2, comparer3);

            return resultdata3.ToList<MyJobRateJoinResult>().OrderBy(x => x.Jobcodedescr).ToList<MyJobRateJoinResult>();
        }
        // Mark rows with Company Only Rates
        class Comparer1 : IEqualityComparer<MyJobRateJoinResult>
        {
            public bool Equals(MyJobRateJoinResult item1, MyJobRateJoinResult item2) {
                if (item1 == null && item2 == null) {
                    return true;
                } else if ((item1 != null && item2 == null) ||
                           (item1 == null && item2 != null)) {
                    return false;
                } else if (
                    // Matching job
                           (item1.Jobcodeid == item2.Jobcodeid) &&
                    // Customer is null
                           (item1.Customerid == null) &&
                    // Company is not null.
                           (item1.Companydivisionid != null)
                          ) {
                    return true;
                } else {
                    return false;
                }
            }
            public int GetHashCode(MyJobRateJoinResult item) {
                return item.Jobcodeid.GetHashCode() ^ 
                       item.Customerid.GetHashCode();
            }
        }
        // Mark rows with Customer Division Only Rates
        class Comparer2 : IEqualityComparer<MyJobRateJoinResult>
        {
            public bool Equals(MyJobRateJoinResult item1, MyJobRateJoinResult item2) {
                if (item1 == null && item2 == null) {
                    return true;
                } else if ((item1 != null && item2 == null) ||
                           (item1 == null && item2 != null)) {
                    return false;
                } else if (
                    // Matching job
                           (item1.Jobcodeid == item2.Jobcodeid) &&
                    // Company is null
                           (item1.Companydivisionid == null) &&
                    // Customers is not null.
                           (item1.Customerid != null) 
                          ) {
                    return true;
                } else {
                    return false;
                }
            }
            public int GetHashCode(MyJobRateJoinResult item) {
                if ((item.Companydivisionid != null) && (item.Customerid != null)) {
                    return item.Companydivisionid.GetHashCode() ^ item.Customerid.GetHashCode();
                } else if (((item.Companydivisionid == null) && (item.Customerid != null)) ||
                           ((item.Companydivisionid != null) && (item.Customerid == null))) {
                    return item.Jobcodeid.GetHashCode();
                } else {
                    return item.Jobcodeid.GetHashCode() ^ 
                           item.Companydivisionid.GetHashCode() ^ 
                           item.Customerid.GetHashCode();
                }
            }
        }
        // Mark rows with Company - Customer Rates
        class Comparer3 : IEqualityComparer<MyJobRateJoinResult>
        {
            public bool Equals(MyJobRateJoinResult item1, MyJobRateJoinResult item2) {
                if (item1 == null && item2 == null) {
                    return true;
                } else if ((item1 != null && item2 == null) ||
                           (item1 == null && item2 != null)) {
                    return false;
                } else if (
                    // Matching job
                           (item1.Jobcodeid == item2.Jobcodeid) &&
                    // Company is not null
                           (item1.Companydivisionid != null) &&
                    // Cust is not null
                           (item1.Customerid != null)
                          ) {
                    return true;
                } else {
                    return false;
                }
            }
            public int GetHashCode(MyJobRateJoinResult item) {
                return item.Jobcodeid.GetHashCode();
            }
        }

        // Spin through all job codes and make sure they have a corporate rate.
        // This is a utility function to be used after loading job codes.
        // Launch in Default.aspx.cs after loading.
        public static void CheckAllJobCodesForDefaultRate() {
            if (System.Web.Configuration.WebConfigurationManager.AppSettings["CREATE_CORP_RATES"].ToString() == "TRUE") {
                ATGDataContext db = new ATGDataContext();

                // Delete all old job rate joins.
                var oldData = from jrjs in db.Jobratejoins
                              select jrjs;
                db.Jobratejoins.DeleteAllOnSubmit(oldData);

                // Delete all rates.
                var oldRates = from rd in db.Ratedatas
                               select rd;
                db.Ratedatas.DeleteAllOnSubmit(oldRates);

                // Submit everything now.
                db.SubmitChanges();

                // Create all new blank, corp joins.
                var data = from jc in db.Jobcodes
                           select jc;

                foreach (Jobcode jc in data) {
                    // Create it.
                    ATGDB.Ratedata.MyJobRateJoinResult jrjr = new ATGDB.Ratedata.MyJobRateJoinResult();
                    // New record, create it.
                    jrjr.Jobcodeid = jc.Jobcodeid;
                    //jrjr.Rateid = rateid;
                    jrjr.Companydivisionid = null;
                    jrjr.Customerid = null;
                    jrjr.Ratedescr = "Corp Rate - " + jc.Description;
                    jrjr.Flatfeeorhourly = "H";
                    jrjr.Cost = 0.00;
                    jrjr.Rate = 0.00;
                    jrjr.Stdhours = 0.00;
                    jrjr.Stdquantity = 0.00;
                    jrjr.Upddt = DateTime.Now;
                    jrjr.Upduserid = System.Web.HttpContext.Current.User.Identity.Name;
                    // Now insert.
                    ATGDB.Ratedata.InsertNewRateAndJoin(jrjr);
                }
            }
        }

        // Method for inserting new rate record and the related join.
        public static void InsertNewRateAndJoin(MyJobRateJoinResult data) {
            ATGDataContext db = new ATGDataContext();

            // Insert the rate
            ATGDB.Ratedata r = new ATGDB.Ratedata();
            r.Isactive = true;
            r.Description = data.Ratedescr;
            r.Flatfeeorhourly = data.Flatfeeorhourly;
            r.Cost = data.Cost;
            r.Rate = data.Rate;
            r.Stdhours = data.Stdhours;
            r.Stdquantity = data.Stdquantity;
            r.Upddt = data.Upddt;
            r.Upduserid = data.Upduserid;

            // Change history row
            r.Changehistory = "INSERT " + DateTime.Now.ToString() + 
                              " Descr: " + r.Description + " Active: " + r.Isactive + " Cost: " + r.Cost + " Rate: " + r.Rate +
                              " Std Hours: " + r.Stdhours + " Std Qty: " + r.Stdquantity + " Upd Dt: " + r.Upddt + " Upd User: " + r.Upduserid;
            
            db.Ratedatas.InsertOnSubmit(r);
            db.SubmitChanges();

            // Insert the join 
            ATGDB.Jobratejoin jbj = new ATGDB.Jobratejoin();
            jbj.Jobcodeid = data.Jobcodeid;
            jbj.Rateid = r.Rateid;
            jbj.Ratedata = r;
            jbj.Companydivisionid = data.Companydivisionid;
            jbj.Customerid = data.Customerid;
            db.Jobratejoins.InsertOnSubmit(jbj);
            db.SubmitChanges();
        }
        // Method for inserting new rate record and the related join.
        protected static void UpdateActiveRate(MyJobRateJoinResult data) {
            ATGDataContext db = new ATGDataContext();

            // Insert the rate
            ATGDB.Ratedata r = db.Ratedatas.Single(x => x.Rateid == data.Rateid);
            r.Isactive = true;
            r.Description = data.Ratedescr;
            r.Flatfeeorhourly = data.Flatfeeorhourly;
            r.Cost = data.Cost;
            r.Rate = data.Rate;
            r.Stdhours = data.Stdhours;
            r.Stdquantity = data.Stdquantity;
            r.Upddt = DateTime.Now;
            r.Upduserid = data.Upduserid;

            // Change history row
            r.Changehistory = r.Changehistory + "\n" +
                              "UPDATE " + DateTime.Now.ToString() +
                              " Descr: " + r.Description + " Active: " + r.Isactive + " Cost: " + r.Cost + " Rate: " + r.Rate +
                              " Std Hours: " + r.Stdhours + " Std Qty: " + r.Stdquantity + " Upd Dt: " + r.Upddt + " Upd User: " + r.Upduserid;

            db.SubmitChanges();
        }
        //if (curOption == "4") {
        // Changing to corporate rates.
        // Any company rate, or customer rate, or company/cust rate must be disabled.
        // This is a total downgrade to corporate rates. Any other rates must be disabled.
        // So, any rates for this jobcode get IsActive = false, except corporate.
        public static void DisableAllRatesForJobCodeExceptCorporate(string keys, MyJobRateJoinResult data) {
            // Break up the keys.
            string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
            string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);

            // Update the data first.
            UpdateActiveRate(data);

            // See if there is a company rate override record.
            ATGDataContext db = new ATGDataContext();
            var data1 = from jrj in db.Jobratejoins
                        where jrj.Jobcodeid == data.Jobcodeid
                        where jrj.Companydivisionid == int.Parse(companyDivisionId)
                        where jrj.Customerid == null
                        select jrj;

            if (data1.Count() == 1) {
                // Record exists, set active = true
                data1.ElementAt(0).Ratedata.Isactive = false;
                db.SubmitChanges();
            }

            // See if there is a customer rate override record.
            data1 = from jrj in db.Jobratejoins
                    where jrj.Jobcodeid == data.Jobcodeid
                    where jrj.Companydivisionid == null
                    where jrj.Customerid == int.Parse(customerId)
                    select jrj;

            if (data1.Count() == 1) {
                // Record exists, set active = true
                data1.ElementAt(0).Ratedata.Isactive = false;
                db.SubmitChanges();
            }

            // See if there is a company/customer rate override record.
            data1 = from jrj in db.Jobratejoins
                    where jrj.Jobcodeid == data.Jobcodeid
                    where jrj.Companydivisionid == int.Parse(companyDivisionId)
                    where jrj.Customerid == int.Parse(customerId)
                    select jrj;

            if (data1.Count() == 1) {
                // Record exists, set active = true
                data1.ElementAt(0).Ratedata.Isactive = false;
                db.SubmitChanges();
            }
        }
        //if (curOption == "3") {
        // This is a company/cust rate override. 
        // In this case, just create the override record, or set to active if it exists.
        // This affects just the company and customer that is selected.
        // This is highly custom to the current selected comp/cust.
        public static void CreateCompanyCustomerRateOverrideForJobCode(string keys, MyJobRateJoinResult data) {
            // Break up the keys.
            //string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
            //string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);

            // See if there is a comp/cust rate override record.
            ATGDataContext db = new ATGDataContext();
            var data1 = from jrj in db.Jobratejoins
                        where jrj.Jobcodeid == data.Jobcodeid
                        where jrj.Companydivisionid == data.Companydivisionid
                        where jrj.Customerid == data.Customerid
                        select jrj;

            if (data1.Count() == 1) {
                // Record exists, set active = true
                UpdateActiveRate(data);
            } else {
                // No record, create a new one.
                InsertNewRateAndJoin(data);
            }
        }
        //if (curOption == "2") {
        // This is a customer override. 
        // In this scenario, the customer is upgrading or downgrading to a customer override.
        // Create the override record, or set to active if it exists.
        // There may be a comp/cust record that needs to be disabled.
        public static void CreateCustomerRateOverrideForJobCode(string keys, MyJobRateJoinResult data) {
            // Break up the keys.
            string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
            string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);

            // See if there is a comp/cust rate override record.
            ATGDataContext db = new ATGDataContext();
            var data1 = from jrj in db.Jobratejoins
                        where jrj.Jobcodeid == data.Jobcodeid
                        where jrj.Companydivisionid == int.Parse(companyDivisionId)
                        where jrj.Customerid == int.Parse(customerId)
                        select jrj;

            if (data1.Count() == 1) {
                // Record exists, set active = false
                data1.ElementAt(0).Ratedata.Isactive = false;
                db.SubmitChanges();
            }

            // Now check for the customer override and activate if found, create if not.
            data1 = from jrj in db.Jobratejoins
                    where jrj.Jobcodeid == data.Jobcodeid
                    where jrj.Companydivisionid == data.Companydivisionid // Should be null if ChangeRate.aspx.cs save button is right.
                    where jrj.Customerid == data.Customerid
                    select jrj;

            if (data1.Count() == 1) {
                // Record exists, set active = true
                UpdateActiveRate(data);
            } else {
                // No record, create a new one.
                InsertNewRateAndJoin(data);
            }
        }
        //if (curOption == "1") {
        // This is a company override. Look for company/cust override, and/or cust override, and make inactive.
        // In this scenario, the user is essentially downgrading either a cust override, or a comp/cust override,
        // if they exist. It could also be upgrading a corporate standard rate. 
        public static void CreateCompanyRateOverrideForJobCode(string keys, MyJobRateJoinResult data) {
            // Break up the keys.
            string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
            string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);

            // See if there is a comp/cust rate override record.
            ATGDataContext db = new ATGDataContext();
            var data1 = from jrj in db.Jobratejoins
                        where jrj.Jobcodeid == data.Jobcodeid
                        where jrj.Companydivisionid == int.Parse(companyDivisionId)
                        where jrj.Customerid == int.Parse(customerId)
                        select jrj;

            if (data1.Count() == 1) {
                // Record exists, set active = false
                data1.ElementAt(0).Ratedata.Isactive = false;
                db.SubmitChanges();
            }

            // Now check for customer override and desctive if found.
            data1 = from jrj in db.Jobratejoins
                    where jrj.Jobcodeid == data.Jobcodeid
                    where jrj.Companydivisionid == null
                    where jrj.Customerid == int.Parse(customerId)
                    select jrj;

            if (data1.Count() == 1) {
                // Record exists, set active = true
                data1.ElementAt(0).Ratedata.Isactive = false;
                db.SubmitChanges();
            }

            // Now check for the company override and activate if found, create if not.
            data1 = from jrj in db.Jobratejoins
                    where jrj.Jobcodeid == data.Jobcodeid
                    where jrj.Companydivisionid == data.Companydivisionid
                    where jrj.Customerid == data.Customerid // Should be null if ChangeRate.aspx.cs save button is right.
                    select jrj;

            if (data1.Count() == 1) {
                // Record exists, set active = true
                UpdateActiveRate(data);
            } else {
                // No record, create a new one.
                InsertNewRateAndJoin(data);
            }
        }

    }
}