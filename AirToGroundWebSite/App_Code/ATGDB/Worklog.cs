﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
/// <summary>
/// Summary description for WorkLog
/// </summary>

namespace ATGDB
{
    public partial class Worklog
    {
        public static ATGDB.Worklog GetWorkLog(string workLogId) {
            if (workLogId != "") {
                ATGDataContext db = new ATGDataContext();
                return db.Worklogs.SingleOrDefault(x => x.Worklogid == int.Parse(workLogId));
            } else {
                return null;
            }
        }
        public static int CreateWorkLog(ATGDB.Serviceschedule ss, string userId, string userName,
                                         DateTime workDate, DateTime? startTime, DateTime? endTime,
                                         double wQty, string notes, string exSelections, string exNotes) {

            ATGDataContext db = new ATGDataContext();

            // Get the Serviceschedule.
            //ATGDB.Serviceschedule ss = db.Serviceschedules.Single(x => x.Servicescheduleid == int.Parse(ssId));

            // Get the job code.
            ATGDB.Jobcode jc = Jobcode.GetJobCodeFromServiceEquipmentTypeJoin(ss.Serviceid, ss.Equipment.Equipmenttypeid);
            if (jc == null) {
                throw new Exception("A Job Code has not been set up for service: " + ss.Service.Description +
                    " and equipment type: " + ss.Equipment.Equipmenttype.Description + ". " + 
                    "Cancelling Action.");
            }

            // It is possible the CompanyCustJobJoin isn't set up yet. Check it.
            ATGDB.Companycustomerjobjoin ccjj = db.Companycustomerjobjoins.SingleOrDefault(x => x.Jobcodeid == jc.Jobcodeid &&
                x.Companydivisionid == (int)ss.Companydivisionid && x.Customerid == ss.Service.Customerid);
            if (ccjj == null) {
                // Didn't find one. Need to insert it because it means the user is trying to use this
                // job code for this service, and no one has set it up yet. Do it now.
                ccjj = new Companycustomerjobjoin();
                ccjj.Companydivisionid = (int)ss.Companydivisionid;
                ccjj.Customerid = ss.Service.Customerid;
                ccjj.Jobcodeid = jc.Jobcodeid;
                db.Companycustomerjobjoins.InsertOnSubmit(ccjj);
                db.SubmitChanges();
            }

            // Create the worklog.
            ATGDB.Worklog wl = new ATGDB.Worklog();
            wl.Workeddate = workDate;
            wl.Userid = userId;
            wl.Jobcodeid = jc.Jobcodeid;
            wl.Companydivisionid = (int)ss.Companydivisionid;
            wl.Customerid = ss.Service.Customerid;
            wl.Starttime = startTime;
            wl.Endtime = endTime;
            wl.Hours = ((startTime == null) || (endTime == null)) ? 0.00f :
                        float.Parse(((endTime.Value - startTime.Value).Hours + ((endTime.Value - startTime.Value).Minutes / 60)).ToString());
            wl.Quantity = wQty < 0.00d ? 0.00f : float.Parse(wQty.ToString());
            wl.Notes = notes;

            // Get the rest of the rate data and denormalize it.
            ATGDB.Ratedata r = ATGDB.Ratedata.GetRatedataByVals((int)wl.Jobcodeid, (int)wl.Companydivisionid, (int)wl.Customerid);
            wl.Rateid = r.Rateid;
            wl.Flatfeeorhourly = r.Flatfeeorhourly;
            wl.Cost = r.Cost;
            wl.Rate = r.Rate;
            wl.Stdhours = r.Stdhours;
            wl.Stdquantity = r.Stdquantity;

            //wl.Serviceschedules.Add(ss);

            // Complete the Serviceschedule.
            //ss.Worklog = wl;
            //ss.Worklogid = wl.Worklogid;
            //ss.Comments = notes;
            //ss.Completeddate = workDate;
            //ss.Completedtime = workDate;

            // Insert it now. Need the p-key.
            db.Worklogs.InsertOnSubmit(wl);

            // Save changes.
            db.SubmitChanges();

            // Set the Worklog Id now.
            ss.Worklogid = wl.Worklogid;

            // Create any Serviceexceptions.
            ATGDB.Serviceexception sex;
            if (exSelections != "") {

                // Create exceptions for each one in the list.
                StringTokenizer st = new StringTokenizer(exSelections, ':');
                foreach (string s in st) {
                    // Create the exception.
                    sex = new ATGDB.Serviceexception();
                    sex.Exceptionby = userName;
                    sex.Exceptiondate = workDate;
                    sex.Exceptionnotes = exNotes;
                    sex.Originalworklogid = wl.Worklogid;
                    sex.Sectionid = int.Parse(s);
                    sex.Serviceid = ss.Serviceid;
                    sex.Servicescheduleid = ss.Servicescheduleid;
                    sex.Equipmentid = ss.Equipmentid;
                    // Insert it.
                    db.Serviceexceptions.InsertOnSubmit(sex);
                }

                // The service schedule has an active exception.
                ss.Hasactiveexception = true;
                ss.Hadexception = true;

                // Submit all inserts.
                db.SubmitChanges();
            }

            // Return the worklog id.
            return wl.Worklogid;
        }
        public static DataSet GetWorkLogDS(DateTime workDate, Boolean unapprovedOnly) {
            ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
            IEnumerable data;
            if (unapprovedOnly == true) {
                data = from wl in dc.Worklogs
                       where wl.Approvaldate == null
                       orderby wl.Workeddate ascending
                       select wl;
            } else {
                data = from wl in dc.Worklogs
                       where wl.Workeddate.Date == workDate.Date
                       orderby wl.Workeddate ascending
                       select wl;
            }
            DataSet ds = new DataSet();
            DataTable tbl = ds.Tables.Add("Worklogs");
            tbl.Columns.Add("Worklogid", typeof(Int32));
            tbl.Columns.Add("Customerid", typeof(Int32));
            tbl.Columns.Add("Jobcodeid", typeof(Int32));
            tbl.Columns.Add("Jobcodecode", typeof(string));
            tbl.Columns.Add("Tailnumber", typeof(string));
            tbl.Columns.Add("Workeddate", typeof(DateTime));
            tbl.Columns.Add("Starttime", typeof(DateTime));
            tbl.Columns.Add("Endtime", typeof(DateTime));
            tbl.Columns.Add("Flatfeeorhourly", typeof(string));
            tbl.Columns.Add("Quantity", typeof(double));
            tbl.Columns.Add("Cost", typeof(double));
            tbl.Columns.Add("Rate", typeof(double));
            tbl.Columns.Add("Stdhours", typeof(double));
            tbl.Columns.Add("Stdquantity", typeof(double));
            tbl.Columns.Add("Userid", typeof(string));
            tbl.Columns.Add("Notes", typeof(string));
            tbl.Columns.Add("Hasex", typeof(bool));
            tbl.Columns.Add("Servicedescr", typeof(string));
            tbl.Columns.Add("Approvaldate", typeof(DateTime));
            tbl.Columns.Add("Approvaluserid", typeof(string));
            tbl.Columns.Add("ChangeHistory", typeof(string));
            tbl.Columns.Add("UpdDt", typeof(DateTime));
            tbl.Columns.Add("UpdUserId", typeof(string));
            foreach (Worklog wl in data) {
                tbl.Rows.Add(new object[] {
                    wl.Worklogid,
                    wl.Customerid,
                    wl.Jobcodeid,
                    wl.Jobcode.Code,
                    wl.Serviceschedules.Count > 0 ? wl.Serviceschedules.ElementAt(0).Equipment.Tailnumber : "", // In reality, worklog and serviceschedule are one to one, not one to many. This relationship should be fixed.
                    wl.Workeddate,
                    wl.Starttime,
                    wl.Endtime,
                    wl.Flatfeeorhourly,
                    wl.Quantity,
                    wl.Cost,
                    wl.Rate,
                    wl.Stdhours,                               
                    wl.Stdquantity,
                    wl.Userid,
                    wl.Notes,
                    wl.Serviceexceptions_Originalworklogid.Count > 0,
                    wl.Serviceschedules.Count > 0 ? wl.Serviceschedules.ElementAt(0).Service.Description : "",   // PROBLEM: only one service schedule per Worklog?????
                    wl.Approvaldate,
                    wl.Approvaluserid,
                    wl.ChangeHistory,
                    wl.UpdDt,
                    wl.UpdUserId
                });
            }
            return ds;
        }
        public static void UpdateWorkLog(Worklog wl) {
            throw new NotImplementedException("Worklog.UpdateWorkLog Has Not Been Implemented");
        }
        public static void DeleteWorkLog(Worklog wl) {
            //throw new NotImplementedException("Worklog.DeleteWorkLog Has Not Been Implemented");
            // Worklog actually shares a 1:1 with Serviceschedule. This should be fixed. This method will delete the 
            // data in the right order, finally deleting the work log.
            ATGDataContext dc = new ATGDataContext();
            var data = from ss in dc.Serviceschedules
                       where ss.Worklogid == wl.Worklogid
                       select ss;
            foreach (ATGDB.Serviceschedule ss in data) {
                Serviceschedule.DeleteServiceSchedule(ss.Servicescheduleid);
            }
        }
    }
}