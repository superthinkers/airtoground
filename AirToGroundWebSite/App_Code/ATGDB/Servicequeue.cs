﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

/// <summary>
/// Summary description for Servicequeue
/// </summary>
namespace ATGDB
{
    public partial class Servicequeue
    {
        public class ServiceQueueItemResult
        {
            public int Servicequeueitemid { get; set; }
            public int Servicequeueid { get; set; }
            public int Servicescheduleid { get; set; }
            public int Serviceid { get; set; }
            public int Customerid { get; set; }
            public DateTime Queuedate { get; set; }
            public DateTime? Queuecloseddate { get; set; }
            public string Tailnumber { get; set; }
            public string Eqtype { get; set; }
            public DateTime Servicedate { get; set; }
            public string Servicedescr { get; set; }
            public DateTime? Inboundtime { get; set; }
            public DateTime? Outboundtime { get; set; }
            public string Ramp { get; set; }
            public Boolean Isspare { get; set; }
            public Boolean Isdone { get; set; }
            public DateTime? Closeddate { get; set; }
            public string Notdonereason { get; set; }
            public DateTime Upddt { get; set; }
            public string Upduserid { get; set; }
        }
        public const string NOT_DONE_REASON_CANCELLED = "Scheduled Service Was Cancelled";
        public const string NOT_DONE_REASON_NORAMP_NOTHERE = "No Ramp Assigned, Not Here";
        public const string NOT_DONE_REASON_SYSTEM = "System Closed - Work Not Logged";
        // Custom Query for worker Dialogs
        public static List<ServiceQueueItemResult> GetServiceQueueItemsByCompCustAndDateAndTailAndService(string keys, DateTime queueDate, string tail, string svcId) {
            // Break up the keys. Similar idea in Timesheets
            string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
            string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);

            ATGDataContext dc = new ATGDataContext();
            var data = from sq in dc.Servicequeueitems
                       where sq.Servicequeue.Companydivisionid == int.Parse(companyDivisionId)
                       where sq.Servicequeue.Customerid == int.Parse(customerId)
                       where sq.Servicequeue.Queuedate.Date == queueDate.Date
                       where sq.Serviceschedule.Equipment.Tailnumber == tail
                       where sq.Serviceschedule.Serviceid == int.Parse(svcId)
                       orderby sq.Serviceschedule.Equipment.Tailnumber ascending,
                               sq.Serviceschedule.Servicedate ascending,
                               sq.Serviceschedule.Service.Description ascending
                       select new ServiceQueueItemResult {
                           Servicequeueitemid = sq.Servicequeueitemid,
                           Servicequeueid = sq.Servicequeueid,
                           Servicescheduleid = sq.Servicescheduleid,
                           Serviceid = sq.Serviceschedule.Serviceid,
                           Customerid = sq.Serviceschedule.Equipment.Customerid,
                           Queuedate = sq.Servicequeue.Queuedate,
                           Queuecloseddate = sq.Servicequeue.Closeddate,
                           Tailnumber = sq.Serviceschedule.Equipment.Tailnumber,
                           Eqtype = sq.Serviceschedule.Equipment.Equipmenttype.Description,
                           Servicedate = sq.Serviceschedule.Servicedate,
                           Servicedescr = sq.Serviceschedule.Service.Description,
                           Inboundtime = sq.Inboundtime,
                           Outboundtime = sq.Outboundtime,
                           Ramp = sq.Ramp,
                           Isspare = sq.Isspare,
                           Isdone = sq.Isdone,
                           Closeddate = sq.Closeddate,
                           Notdonereason = sq.Notdonereason,
                           Upddt = sq.Upddt,
                           Upduserid = sq.Upduserid
                       };
            return data.ToList();
        }
        // Called from Home page UI for summary box
        public static List<ServiceQueueItemResult> GetServiceQueueItemsByCompCustAndDate(string keys, DateTime queueDate) {
            // Break up the keys. Similar idea in Timesheets
            if ((keys != ":") && (keys.Length >= 3)) {
                string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
                string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);

                ATGDataContext dc = new ATGDataContext();
                var data = from sq in dc.Servicequeueitems
                           where sq.Servicequeue.Companydivisionid == int.Parse(companyDivisionId)
                           where sq.Servicequeue.Customerid == int.Parse(customerId)
                           where sq.Servicequeue.Queuedate.Date == queueDate.Date
                           orderby sq.Serviceschedule.Equipment.Tailnumber ascending,
                                   sq.Serviceschedule.Servicedate ascending,
                                   sq.Serviceschedule.Service.Description ascending
                           select new ServiceQueueItemResult {
                               Servicequeueitemid = sq.Servicequeueitemid,
                               Servicequeueid = sq.Servicequeueid,
                               Servicescheduleid = sq.Servicescheduleid,
                               Serviceid = sq.Serviceschedule.Serviceid,
                               Customerid = sq.Serviceschedule.Equipment.Customerid,
                               Queuedate = sq.Servicequeue.Queuedate,
                               Queuecloseddate = sq.Servicequeue.Closeddate,
                               Tailnumber = sq.Serviceschedule.Equipment.Tailnumber,
                               Eqtype = sq.Serviceschedule.Equipment.Equipmenttype.Description,
                               Servicedate = sq.Serviceschedule.Servicedate,
                               Servicedescr = sq.Serviceschedule.Service.Description,
                               Inboundtime = sq.Inboundtime,
                               Outboundtime = sq.Outboundtime,
                               Ramp = sq.Ramp,
                               Isspare = sq.Isspare,
                               Isdone = sq.Isdone,
                               Closeddate = sq.Closeddate,
                               Notdonereason = sq.Notdonereason,
                               Upddt = sq.Upddt,
                               Upduserid = sq.Upduserid
                           };
                return data.ToList();
            } else {
                return new List<ServiceQueueItemResult>();
            }
        }
        // CALL THIS ONE FROM MAIN ServiceQueue UI for Databinding
        public static DataSet GetSynchronizedServiceQueueItemsByCompCustAndDateDS(string keys, DateTime queueDate, bool hideRamp, bool hideClosed) {
            List<ServiceQueueItemResult> lst = GetSynchronizedServiceQueueItemsByCompCustAndDate(keys, queueDate, hideRamp, hideClosed);
            return ConvertToSQRDataSet(lst);
        }
        // MAIN WORKER METHOD. Also called from InboundSheetEntryDialog UI
        public static List<ServiceQueueItemResult> GetSynchronizedServiceQueueItemsByCompCustAndDate(string keys, DateTime queueDate, bool hideRamp, bool hideClosed) {
            // Every time data is requested, process for expired queue items first.
            CloseExpiredQueueItems();     
            
            // Get the requested queue items.
            var data = GetServiceQueueItemsByCompCustAndDate(keys, queueDate);

            if ((data.Count() == 0) && (queueDate.Date == DateTime.Now.Date)) {
                if (SecurityUtils.GetATGMode() != SecurityUtils.ATG_MODE_CUSTOMER) {
                    // There isn't a queue for today. Create one.
                    int sqId = GetCurrentServiceQueueId(queueDate, keys);
                    // Now synchronize the items.
                    SynchronizeQueueItems(sqId);
                }
            } else if (data.Count() > 0) {
                // Just synchronize the items.
                // All rows in "data" are for the same service queue.
                SynchronizeQueueItems(data.ElementAt(0).Servicequeueid);
            } else {
                // No queue, and we don't want to create one either. This
                // Would happen in the case the user picked a date in the 
                // past that never had a queue before.
                return null;
            }

            // Now, reselect the data and return it. Should be sync'd and current for passed queueDate, 
            // and past/expired service queues should be fully closed.
            if ((hideRamp == true) && (hideClosed == false)) {
                data = GetServiceQueueItemsByCompCustAndDate(keys, queueDate).Where(x => x.Ramp != null && x.Ramp != "").ToList<ServiceQueueItemResult>();
            } else if ((hideRamp == false) && (hideClosed == true)) {
                data = GetServiceQueueItemsByCompCustAndDate(keys, queueDate).Where(x => x.Closeddate == null).ToList<ServiceQueueItemResult>();
            } else if ((hideRamp == true) && (hideClosed == true)) {
                data = GetServiceQueueItemsByCompCustAndDate(keys, queueDate).Where(x => x.Ramp != null && x.Ramp != "" && x.Closeddate == null).ToList<ServiceQueueItemResult>();
            } else {
                data = GetServiceQueueItemsByCompCustAndDate(keys, queueDate);
            }
            return data;
        }
        // Returns unique Servicequeueid over a servicequeueitem collection.
        class UniqueServiceQueueComparer : IEqualityComparer<ServiceQueueItemResult>
        {
            public bool Equals(ServiceQueueItemResult item1, ServiceQueueItemResult item2) {
                if (item1 == null && item2 == null) {
                    return true;
                } else if ((item1 != null && item2 == null) ||
                           (item1 == null && item2 != null)) {
                    return false;
                } else if (
                    // Matching ServiceQueueId
                           (item1.Servicequeueid == item2.Servicequeueid)
                          ) {
                    return true;
                } else {
                    return false;
                }
            }
            public int GetHashCode(ServiceQueueItemResult item) {
                return item.Servicequeueid.GetHashCode();
            }
        }
        // "Expired" queue items are over 60 hours old...2.5 days.
        protected static void CloseExpiredQueueItems() {
            if (SecurityUtils.GetATGMode() != SecurityUtils.ATG_MODE_CUSTOMER) {
                // Take all past items and make sure all are closed.
                ATGDataContext dc = new ATGDataContext();

                // Get all expired Service Queues - and NOT today
                var expSq = from sq in dc.Servicequeues
                            where sq.Closeddate == null
                            where sq.Queuedate.Date != DateTime.Now.Date
                            select sq;

                // Process each "Expired" service queue and close it
                bool needsCommit = false;
                foreach (ATGDB.Servicequeue sq in expSq) {
                    // Is it expired?
                    if ((DateTime.Now.Date - sq.Queuedate.Date).TotalHours > (24 + 24 + 12)) {
                        // If expired, close every item.
                        foreach (ATGDB.Servicequeueitem item in sq.Servicequeueitems) {
                            if (item.Closeddate == null) {
                                item.Closeddate = DateTime.Now.Date;
                                item.Isdone = false;
                                // Why did it close?
                                if (String.IsNullOrEmpty(item.Ramp)) {
                                    item.Notdonereason = NOT_DONE_REASON_NORAMP_NOTHERE;
                                } else {
                                    item.Notdonereason = NOT_DONE_REASON_SYSTEM;
                                }
                                item.Upddt = DateTime.Now;
                                item.Upduserid = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                            }
                        }
                        sq.Closeddate = DateTime.Now;
                        sq.Upddt = DateTime.Now;
                        sq.Upduserid = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                        needsCommit = true;
                    }
                }
                if (needsCommit == true) {
                    // Submit
                    dc.SubmitChanges();
                }
            }
        }
        protected static void SynchronizeQueueItems(int serviceQueueId) {
            // Take Today's items and make sure every service schedule
            // for today is represented.
            if (SecurityUtils.GetATGMode() != SecurityUtils.ATG_MODE_CUSTOMER) {
                ATGDataContext dc = new ATGDataContext();
                ATGDB.Servicequeue sq = dc.Servicequeues.SingleOrDefault(x => x.Servicequeueid == serviceQueueId);
                if (sq != null) {
                    // If the ServiceQueue is not closed...
                    //if (sq.Closeddate == null) {
                        bool needsSubmit = false;

                        // FIRST, check each queue item for SS changes.
                        foreach (ATGDB.Servicequeueitem qitem in sq.Servicequeueitems) {
                            ATGDB.Serviceschedule ss = dc.Serviceschedules.SingleOrDefault(x => x.Servicescheduleid == qitem.Servicescheduleid);
                            if ((ss != null) && (qitem.Closeddate == null)) {
                                // Did the information change? Does the item need updating?
                                // There is one.
                                // It may not be immediately obvious, but a user could have
                                // updated the service schedule from another screen. For example, 
                                // they could have cancelled it or completed it (or deleted it -
                                // which means any queue items need to be deleted it a ss is deleted).
                                // So, check for updated and respond accordingly.
                                if ((ss.Completeddate == null) && (ss.Cancelleddate != null)) {
                                    // It was cancelled. So, cancel the queue item.
                                    if (qitem.Closeddate == null) {
                                        qitem.Isdone = false;
                                        qitem.Closeddate = DateTime.Now;
                                        qitem.Notdonereason = ss.Comments == null ? NOT_DONE_REASON_CANCELLED : ss.Comments;
                                        qitem.Upddt = DateTime.Now;
                                        qitem.Upduserid = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                                        needsSubmit = true;
                                    }
                                } else if ((ss.Completeddate != null) && (ss.Cancelleddate == null)) {
                                    // It was completed. So, complete the queue item.
                                    if (qitem.Closeddate == null) {
                                        qitem.Isdone = true;
                                        qitem.Closeddate = DateTime.Now;
                                        qitem.Notdonereason = "";
                                        qitem.Upddt = DateTime.Now;
                                        qitem.Upduserid = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                                        needsSubmit = true;
                                    }
                                } else if ((ss.Completeddate == null) && (ss.Cancelleddate == null)) {
                                    // It was re-opened. So, re-open the queue item.
                                    if (qitem.Closeddate != null) {
                                        qitem.Isdone = false;
                                        qitem.Closeddate = null;
                                        qitem.Notdonereason = qitem.Notdonereason == null ? "Reopened" : "Reopened: " + qitem.Notdonereason;
                                        qitem.Upddt = DateTime.Now;
                                        qitem.Upduserid = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                                        needsSubmit = true;
                                    }
                                }
                            }
                        }

                        // Submit
                        if (needsSubmit == true) {
                            dc.SubmitChanges();
                            needsSubmit = false;
                        }

                        // If we are on today, sync new scheduled items.
                        if (sq.Queuedate.Date == DateTime.Now.Date) {
                         
                            // Get all schedules that are open.
                            var ssch = from ss in dc.Serviceschedules
                                       where ss.Companydivisionid == sq.Companydivisionid || ss.Companydivisionid == null
                                       where ss.Equipment.Customerid == sq.Customerid
                                       where ss.Servicedate.Date <= sq.Queuedate.Date
                                       where ss.Completeddate == null
                                       where ss.Cancelleddate == null
                                       select ss;

                            // For every serviceSchedule that doesn't have a queue item, create a new queue item.
                            foreach (ATGDB.Serviceschedule ss in ssch) {
                                // Only create new one if there aren't ones that are open on another date
                                var openCheckData = ss.Servicequeueitems.Where(x => x.Closeddate == null);
                                if (openCheckData.Count() == 0) {
                                    // Is there a queue item for the current service queue? If not, create one.
                                    ATGDB.Servicequeueitem qitem = sq.Servicequeueitems.SingleOrDefault(x => x.Servicescheduleid == ss.Servicescheduleid);
                                    if (qitem == null) {
                                        // Not found, create a new queue item.
                                        needsSubmit = true;
                                        qitem = new ATGDB.Servicequeueitem();
                                        qitem.Servicequeueid = serviceQueueId;
                                        qitem.Servicescheduleid = ss.Servicescheduleid;
                                        qitem.Upddt = DateTime.Now;
                                        qitem.Upduserid = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                                        sq.Servicequeueitems.Add(qitem);
                                        dc.Servicequeueitems.InsertOnSubmit(qitem);
                                    }
                                }
                            }

                            // Submit
                            if (needsSubmit == true) {
                                dc.SubmitChanges();
                            }
                        }
                    //}
                }
            }
        }
        public static void UpdateServiceQueueItem(ServiceQueueItemResult item) {
            ATGDataContext dc = new ATGDataContext();
            // Get the Queue Item.
            ATGDB.Servicequeueitem qi = dc.Servicequeueitems.SingleOrDefault(x => x.Servicequeueitemid == item.Servicequeueitemid);
            if (qi != null) {
                // Update vals.
                if (item.Inboundtime != null) {
                    qi.Inboundtime = DateTime.Parse(DateTime.Now.Date.ToShortDateString() + " " + item.Inboundtime.Value.ToShortTimeString());
                } else {
                    qi.Inboundtime = null;
                }
                if (item.Inboundtime != null) {
                    qi.Outboundtime = DateTime.Parse(DateTime.Now.Date.ToShortDateString() + " " + item.Outboundtime.Value.ToShortTimeString());
                } else {
                    qi.Outboundtime = null;
                }
                qi.Ramp = item.Ramp;
                qi.Isspare = item.Isspare;
                qi.Upddt = DateTime.Now.Date;
                qi.Upduserid = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                // Submit
                dc.SubmitChanges();
            }
        }
        // CloseServiceQueueItem
        public static void CloseServiceQueueItem(int queueItemId, bool isDone, 
            DateTime closeDate, string notDoneReason, string updUserId) {
            ATGDataContext dc = new ATGDataContext();
            ATGDB.Servicequeueitem qi = dc.Servicequeueitems.SingleOrDefault(x => x.Servicequeueitemid == queueItemId);
            if (qi != null) {
                qi.Isdone = isDone;
                qi.Closeddate = closeDate;
                qi.Notdonereason = notDoneReason;
                qi.Upduserid = updUserId;
                // Commit.
                dc.SubmitChanges();
            }
        }

        private static DataSet GetSQRDataSet() {        // Create a dataset.
            DataSet ds = new DataSet();
            // Add a table.
            DataTable tbl = ds.Tables.Add("ServiceQueueResults");
            // Create the columns.
            DataColumn col = new DataColumn("Servicequeueitemid", typeof(Int32));
            tbl.Columns.Add(col);
            col = new DataColumn("Servicequeueid", typeof(Int32));
            tbl.Columns.Add(col);
            col = new DataColumn("Servicescheduleid", typeof(Int32));
            tbl.Columns.Add(col);
            col = new DataColumn("Serviceid", typeof(Int32));
            tbl.Columns.Add(col);
            col = new DataColumn("Customerid", typeof(Int32));
            tbl.Columns.Add(col);
            col = new DataColumn("Queuedate", typeof(DateTime));
            tbl.Columns.Add(col);
            col = new DataColumn("Queuecloseddate", typeof(DateTime));
            tbl.Columns.Add(col);
            col = new DataColumn("Tailnumber", typeof(string));
            tbl.Columns.Add(col);
            col = new DataColumn("Eqtype", typeof(string));
            tbl.Columns.Add(col);
            col = new DataColumn("Servicedate", typeof(DateTime));
            tbl.Columns.Add(col);
            col = new DataColumn("Servicedescr", typeof(string));
            tbl.Columns.Add(col);
            col = new DataColumn("Inboundtime", typeof(DateTime));
            tbl.Columns.Add(col);
            col = new DataColumn("Outboundtime", typeof(DateTime));
            tbl.Columns.Add(col);
            col = new DataColumn("Ramp", typeof(string));
            tbl.Columns.Add(col);
            col = new DataColumn("Isspare", typeof(bool));
            tbl.Columns.Add(col);
            col = new DataColumn("Isdone", typeof(bool));
            tbl.Columns.Add(col);
            col = new DataColumn("Closeddate", typeof(DateTime));
            tbl.Columns.Add(col);
            col = new DataColumn("Notdonereason", typeof(string));
            tbl.Columns.Add(col);
            col = new DataColumn("Upddt", typeof(DateTime));
            tbl.Columns.Add(col);
            col = new DataColumn("Upduserid", typeof(string));
            tbl.Columns.Add(col);
            return ds;
        }
        private static DataSet ConvertToSQRDataSet(List<ServiceQueueItemResult> lst) {
            DataSet ds = GetSQRDataSet();
            if (lst != null) {
                DataTable tbl = ds.Tables[0];
                // Add the data.
                foreach (ServiceQueueItemResult sq in lst) {
                    tbl.Rows.Add(new object[] {
                    sq.Servicequeueitemid,
                    sq.Servicequeueid,
                    sq.Servicescheduleid,
                    sq.Serviceid,
                    sq.Customerid,
                    sq.Queuedate,
                    sq.Queuecloseddate,
                    sq.Tailnumber,
                    sq.Eqtype,
                    sq.Servicedate,
                    sq.Servicedescr,
                    sq.Inboundtime,
                    sq.Outboundtime,
                    sq.Ramp,
                    sq.Isspare,
                    sq.Isdone,
                    sq.Closeddate,
                    sq.Notdonereason,
                    sq.Upddt,
                    sq.Upduserid
                });
                }
            }
            // Return it.
            return ds;
        }
        public static void InsertServiceQueueItem(int serviceQueueId, int compId, int custId, int ssId, 
            DateTime serviceDate, DateTime? inTime, DateTime? outTime, string ramp, bool isSpare) {
            
            ATGDataContext dc = new ATGDataContext();
            
            // Get current service queue.
            ATGDB.Servicequeue sq;
            sq = dc.Servicequeues.SingleOrDefault(x => x.Servicequeueid == serviceQueueId);

            if (sq != null) {
                ATGDB.Servicequeueitem item = new ATGDB.Servicequeueitem();
                item.Servicequeueid = sq.Servicequeueid;
                item.Servicescheduleid = ssId;
                item.Inboundtime = inTime;
                item.Outboundtime = outTime;
                item.Ramp = ramp;
                item.Isspare = isSpare;
                item.Isdone = false;
                item.Notdonereason = "";
                item.Upddt = DateTime.Now;
                item.Upduserid = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                dc.Servicequeueitems.InsertOnSubmit(item);
                sq.Servicequeueitems.Add(item);
                dc.SubmitChanges();
            }
        }
        public static ATGDB.Servicequeue GetCurrentServiceQueue(DateTime queueDt, int compId, int custId) {
            ATGDataContext dc = new ATGDataContext();
            ATGDB.Servicequeue sq = dc.Servicequeues.SingleOrDefault(x => x.Queuedate.Date == queueDt.Date &&
                                                                     x.Companydivisionid == compId &&
                                                                     x.Customerid == custId);
            return sq;
        }
        public static int GetCurrentServiceQueueId(DateTime queueDt, string keys) {
            // Break up the keys. Similar idea in Timesheets
            string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
            string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);

            ATGDataContext dc = new ATGDataContext();
            ATGDB.Servicequeue sq = dc.Servicequeues.SingleOrDefault(x => x.Queuedate.Date == queueDt.Date &&
                                                                     x.Companydivisionid == int.Parse(companyDivisionId) &&
                                                                     x.Customerid == int.Parse(customerId));
            if (sq != null) {
                // Return found one.
                return sq.Servicequeueid;
            } else {
                if (SecurityUtils.GetATGMode() != SecurityUtils.ATG_MODE_CUSTOMER) {
                    // Insert a new one.
                    sq = new Servicequeue();
                    sq.Queuedate = queueDt;
                    sq.Companydivisionid = int.Parse(companyDivisionId);
                    sq.Customerid = int.Parse(customerId);
                    sq.Upddt = DateTime.Now.Date;
                    sq.Upduserid = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                    dc.Servicequeues.InsertOnSubmit(sq);
                    try {
                        dc.SubmitChanges();
                    } catch {
                        // unhandled for now.
                    }
                    return sq.Servicequeueid;
                } else {
                    return -1;
                }
            }
        }
    }
}