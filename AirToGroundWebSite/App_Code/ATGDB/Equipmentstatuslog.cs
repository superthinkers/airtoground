﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Equipmentstatuslog
/// </summary>
namespace ATGDB
{
    public partial class Equipmentstatuslog
    {
        public static void PutOnHold(string eqId, string logId, string status, string reason,
                                     DateTime statusDate, string statusBy, DateTime returndate, string statusNotes) {
            ATGDataContext db = new ATGDataContext();
            bool doInsert = false;
            if ((logId != "") && (int.Parse(logId) > 0)) {
                // This is an update.
                ATGDB.Equipmentstatuslog log = db.Equipmentstatuslogs.SingleOrDefault(x => x.Equipmentstatuslogid == int.Parse(logId));
                if ((log != null) && (log.Unholddate == null)) {
                    // Update.
                    log.Status = status;
                    log.Statusreason = reason;
                    log.Statusdate = statusDate;
                    log.Statusby = statusBy;
                    log.Returndate = returndate;
                    log.Statusnotes = statusNotes;
                    log.Unholdby = "";
                    log.Unholddate = null;
                    log.Unholdnotes = "";
                    db.SubmitChanges();
                } else {
                    doInsert = true;
                }
            } else {
                doInsert = true;
            }

            if (doInsert == true) {
                // Should have been found. Insert anyway.
                ATGDB.Equipmentstatuslog log2 = new ATGDB.Equipmentstatuslog();
                log2.Equipmentid = int.Parse(eqId);
                log2.Status = status;
                log2.Statusreason = reason;
                log2.Statusdate = statusDate;
                log2.Statusby = statusBy;
                log2.Returndate = returndate;
                log2.Statusnotes = statusNotes;
                db.Equipmentstatuslogs.InsertOnSubmit(log2);
                db.SubmitChanges();

                // Get the equipment to create the inverse relation.
                ATGDB.Equipment eq = db.Equipments.SingleOrDefault(x => x.Equipmentid == int.Parse(eqId));
                if (eq != null) {
                    eq.Equipmentstatuslogid = log2.Equipmentstatuslogid;
                    db.SubmitChanges();
                }
            }

            // Need to cancel any scheduled items LESS THAN RETURN DATE.
            var data = from ss in db.Serviceschedules
                       where ss.Equipmentid == int.Parse(eqId)
                       where ss.Servicedate.Date < returndate.Date
                       where ss.Completeddate == null
                       where ss.Cancelleddate == null
                       select ss;
            bool doCommit = false;
            foreach (ATGDB.Serviceschedule ss in data) {
                doCommit = true;
                ss.Cancelleddate = statusDate;
                if (ss.Comments.Length > 0) {
                    ss.Comments = ss.Comments + "\n On Hold: " + statusNotes;
                } else {
                    ss.Comments = statusNotes;
                }
            }
            if (doCommit == true) {
                db.SubmitChanges();
            }
        }
        public static void ReleaseHold(string eqId, string logId, string services, string companyDivisionId,
                                       DateTime unHoldDate, string unHoldBy, string unHoldNotes) {
            ATGDataContext db = new ATGDataContext();
            // This is an update.
            ATGDB.Equipmentstatuslog log = db.Equipmentstatuslogs.SingleOrDefault(x => x.Equipmentstatuslogid == int.Parse(logId));
            if (log != null) {
                // Update.
                log.Unholddate = unHoldDate;
                log.Unholdby = unHoldBy;
                log.Unholdnotes = unHoldNotes;
                db.SubmitChanges();
            }

            // Now, create the schedules for each service.
            StringTokenizer tokens = new StringTokenizer(services, ";");
            bool doCommit = false;
            foreach (string value in tokens) {
                string svcId = value.Substring(0, value.IndexOf("|"));
                string svcDt = value.Substring(value.IndexOf("|") + 1, value.Length - value.IndexOf("|") - 1);

                doCommit = true;
                ATGDB.Serviceschedule ss = new ATGDB.Serviceschedule();
                ss.Equipmentid = int.Parse(eqId);
                ss.Serviceid = int.Parse(svcId);
                ss.Servicedate = Convert.ToDateTime(svcDt);
                ss.Servicetime = Convert.ToDateTime(svcDt);
                ss.Comments = unHoldNotes;
                ss.Companydivisionid = int.Parse(companyDivisionId);
                db.Serviceschedules.InsertOnSubmit(ss);
            }
            if (doCommit == true) {
                db.SubmitChanges();
            }

        }
        public class SelectedService
        {
            public int ServiceId { get; set; }
            public string Description { get; set; }
            public DateTime? ServiceDate { get; set; }
            public string Comments { get; set; }
        }
        public static string EQ_STATUS_LOG_DATA = "EQ_STATUS_LOG_DATA";
        public List<SelectedService> GetServices(int eqId) {
            HttpContext context = HttpContext.Current;
            if ((context.Session[EQ_STATUS_LOG_DATA] != null) && (context.Session[EQ_STATUS_LOG_DATA].ToString() != "")) {
                return (List<SelectedService>)context.Session[EQ_STATUS_LOG_DATA];
            } else {
                // Service list.
                ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
                ATGDB.Equipment eq = db.Equipments.SingleOrDefault(x => x.Equipmentid == eqId);
                var services = from svcs in db.Services
                               where svcs.Customerid == eq.Customerid
                               orderby svcs.Sequence, svcs.Description
                               select svcs;
                List<SelectedService> data = new List<SelectedService>();
                foreach (ATGDB.Service svc in services) {
                    SelectedService ss = new SelectedService();
                    ss.ServiceId = svc.Serviceid;
                    ss.Description = svc.Description;
                    ss.ServiceDate = null;
                    ss.Comments = "";
                    data.Add(ss);
                }
                context.Session.Add(EQ_STATUS_LOG_DATA, data);
                return data;
            }
        }
        public void UpdateService(int ServiceId, DateTime ServiceDate) {
            HttpContext context = HttpContext.Current;
            List<SelectedService> logs = (List<SelectedService>)context.Session[EQ_STATUS_LOG_DATA];
            SelectedService ss = logs.Find(x => x.ServiceId == ServiceId);
            ss.ServiceDate = ServiceDate;
        }
    }
}