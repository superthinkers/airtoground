﻿using System.Linq;

/// <summary>
/// Summary description for Divisionarea
/// </summary>
namespace ATGDB
{
    public partial class Divisionarea
    {                         
        public static IQueryable GetAllDivisionareas() {
            ATGDataContext dc = new ATGDataContext();
            var data = from a in dc.Divisionareas
                       orderby a.Description
                       select a;
            return data.AsQueryable();
        }           
        
    }
}