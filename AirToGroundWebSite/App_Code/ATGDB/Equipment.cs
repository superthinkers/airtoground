﻿using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Equipment
/// </summary>
namespace ATGDB
{
    public partial class Equipment
    {
        public static IEnumerable<ATGDB.Equipment> GetEquipmentFiltered(int customerId) {
            ATGDataContext dc = new ATGDataContext();
            if (customerId > 0) {
                var data = from eq in dc.Equipments
                           where eq.Customerid == customerId
                           orderby eq.Tailnumber
                           select eq;
                return data.ToList();
            } else {
                var data = from eq in dc.Equipments
                           orderby eq.Tailnumber
                           select eq;
                return data.ToList();
            }
        }
        public static int GetEquipmentIdByTailNumber(int customerId, string tailNumber) {
            ATGDataContext dc = new ATGDataContext();
            ATGDB.Equipment e = dc.Equipments.SingleOrDefault(x => x.Customerid == customerId && x.Tailnumber.Trim().Equals(tailNumber.Trim()));
            if (e == null) {
                return -1;
            } else {
                return e.Equipmentid;
            }
        }
        public static Equipment GetEquipmentByTailNumber(int customerId, string tailNumber) {
            ATGDataContext dc = new ATGDataContext();
            ATGDB.Equipment e = dc.Equipments.SingleOrDefault(x => x.Customerid == customerId && x.Tailnumber.Trim().Equals(tailNumber.Trim()));
            if (e == null) {
                return null;
            } else {
                return e;
            }
        }
        public static IEnumerable<ATGDB.Equipment> GetEquipmentNotOnHold(int customerId) {
            ATGDataContext dc = new ATGDataContext();
            var data = from e in dc.Equipments
                       where e.Customerid == customerId
                       where ((e.Equipmentstatuslog == null) || (e.Equipmentstatuslog.Unholddate != null))
                       orderby e.Tailnumber
                       select e;
            return data;
        }

    }
}