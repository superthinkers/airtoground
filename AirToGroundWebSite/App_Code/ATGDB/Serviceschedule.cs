﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Telerik.Web.UI;

/// <summary>
/// Summary description for Serviceschedule
/// </summary>
namespace ATGDB
{
    public partial class Serviceschedule
    {
        public static ATGDB.Serviceschedule GetServiceSchedule(int serviceScheduleId) {
            ATGDataContext dc = new ATGDataContext();
            ATGDB.Serviceschedule ss = dc.Serviceschedules.SingleOrDefault(x => x.Servicescheduleid == serviceScheduleId);
            return ss;
        }
        public static IEnumerable GetServiceSchedulesByRange(int equipmentId, DateTime dtFromDate) {
            ATGDataContext db = new ATGDataContext();
            // Look for similar service schedules. EXACT DATE
            var data = from s in db.Serviceschedules
                       where s.Equipmentid == equipmentId
                       where s.Servicedate >= dtFromDate.AddDays(-14)
                       where s.Servicedate <= dtFromDate.AddDays(14)
                       where s.Completeddate == null
                       where s.Cancelleddate == null
                       orderby s.Servicedate, s.Serviceid
                       select new {
                           Servicescheduleid = s.Servicescheduleid,
                           Equipmentid = s.Equipmentid,
                           Equipmentdescr = s.Equipment.Description,
                           TailNumber = s.Equipment.Tailnumber,
                           Servicedescr = s.Service.Description,
                           Location = s.Companydivision.Location,
                           Servicedate = s.Servicedate,
                           Servicetime = s.Servicetime,
                           Completeddate = s.Completeddate,
                           Completedtime = s.Completedtime,
                           Comments = s.Comments
                       };
            return data.ToList();
        }
        public static IEnumerable GetServiceSchedulesByDate(int equipmentId, DateTime dtFromDate) {
            ATGDataContext db = new ATGDataContext();
            // Look for similar service schedules. EXACT DATE
            var data = from s in db.Serviceschedules
                       where s.Equipmentid == equipmentId
                       where s.Servicedate == dtFromDate
                       orderby s.Servicedate, s.Serviceid
                       select new {
                           Servicescheduleid = s.Servicescheduleid,
                           Equipmentid = s.Equipmentid,
                           Equipmentdescr = s.Equipment.Description,
                           TailNumber = s.Equipment.Tailnumber,
                           Servicedescr = s.Service.Description,
                           Location = s.Companydivision.Location,
                           Servicedate = s.Servicedate,
                           Servicetime = s.Servicetime,
                           Completeddate = s.Completeddate,
                           Completedtime = s.Completedtime,
                           Comments = s.Comments
                       };
            return data.ToList();
        }
        public static IEnumerable GetServiceSchedulesByDate(DateTime dtFromDate, Int32 equipmentId) {
            ATGDataContext db = new ATGDataContext();
            // Look for similar service schedules. EXACT DATE
            var data = from s in db.Serviceschedules
                       where s.Equipmentid == equipmentId
                       where ((s.Servicedate == dtFromDate) && (s.Completeddate == null) && (s.Cancelleddate == null))
                       orderby s.Servicedate, s.Serviceid
                       select new {
                           Servicescheduleid = s.Servicescheduleid,
                           Equipmentid = s.Equipmentid,
                           Equipmentdescr = s.Equipment.Description,
                           TailNumber = s.Equipment.Tailnumber,
                           Servicedescr = s.Service.Description,
                           Location = s.Companydivision.Location,
                           Servicedate = s.Servicedate,
                           Servicetime = s.Servicetime,
                           Completeddate = s.Completeddate,
                           Completedtime = s.Completedtime,
                           Hasactiveexception = s.Hasactiveexception,
                           Comments = s.Comments
                       };
            return data.ToList();
        }

        public static IEnumerable GetServiceSchedulesByDates(int equipmentId, DateTime dtFromDate, DateTime dtToDate) {
            ATGDataContext db = new ATGDataContext();
            // Look for similar service schedules. EXACT DATE
            var data = from s in db.Serviceschedules
                       where s.Equipmentid == equipmentId
                       where s.Servicedate >= dtFromDate
                       where s.Servicedate <= dtToDate
                       orderby s.Servicedate, s.Serviceid
                       select new {
                           Servicescheduleid = s.Servicescheduleid,
                           Equipmentid = s.Equipmentid,
                           Equipmentdescr = s.Equipment.Description,
                           TailNumber = s.Equipment.Tailnumber,
                           Servicedescr = s.Service.Description,
                           Location = s.Companydivision.Location,
                           Servicedate = s.Servicedate,
                           Servicetime = s.Servicetime,
                           Completeddate = s.Completeddate,
                           Completedtime = s.Completedtime,
                           Comments = s.Comments
                       };
            return data.ToList();
        }
        public static IEnumerable GetOpenServiceSchedulesByDates(int equipmentId, DateTime dtFromDate, DateTime dtToDate) {
            ATGDataContext db = new ATGDataContext();
            // Look for similar service schedules. EXACT DATE
            var data = from s in db.Serviceschedules
                       where s.Equipmentid == equipmentId
                       where s.Servicedate >= dtFromDate
                       where s.Servicedate <= dtToDate
                       where s.Completeddate == null
                       where s.Cancelleddate == null
                       orderby s.Servicedate, s.Serviceid
                       select new {
                           Servicescheduleid = s.Servicescheduleid,
                           Equipmentid = s.Equipmentid,
                           Equipmentdescr = s.Equipment.Description,
                           TailNumber = s.Equipment.Tailnumber,
                           Servicedescr = s.Service.Description,
                           Location = s.Companydivision.Location,
                           Servicedate = s.Servicedate,
                           Servicetime = s.Servicetime,
                           Completeddate = s.Completeddate,
                           Completedtime = s.Completedtime,
                           Comments = s.Comments
                       };
            return data.ToList();
        }
        public class MyServiceSchedule
        {
            public int Servicescheduleid { get; set; }
            public int Serviceid { get; set; }
            public int Equipmentid { get; set; }
            public int Equipmenttypeid { get; set; }
            public string Equipmenttypedescr { get; set; }
            public string Equipmentdescr { get; set; }
            public string CustomerName { get; set; }
            public string TailNumber { get; set; }
            public string Servicedescr { get; set; }
            public string Area { get; set; }
            public string Shortdescr { get; set; } // Service Short Description.
            public string Location { get; set; }
            public int Interval { get; set; }
            public int ReportInterval { get; set; }
            public int Sequence { get; set; }
            public DateTime Servicedate { get; set; }
            public DateTime Servicetime { get; set; }
            public DateTime Completeddate { get; set; }
            public DateTime Completedtime { get; set; }
            public DateTime Cancelleddate { get; set; }
            public bool Hasactiveexception { get; set; }
            public bool Hadexception { get; set; }
            public string Comments { get; set; }
            public string Status { get; set; }
            public int TheGroup { get; set; }
            public string OnHold { get; set; }
            public DateTime? UpdDt { get; set; }
            public string UpdUserId { get; set; }
            public DateTime? Approvaldate { get; set; }
            public string Approvaluserid { get; set; }
        }
        public static DataView GetServiceSchedulesByDatesAndEquipOrServiceType(int customerId, int equipmentId,
                                                                              DateTime dtFromDate, DateTime dtToDate,
                                                                              bool futureOption, string services, string eqTypes) {
            return Serviceschedule.GetServiceSchedulesByDatesAndEquipOrServiceType(customerId, equipmentId, dtFromDate, dtToDate, futureOption, services, eqTypes);
        }
        class MyEqualityComparer : IEqualityComparer<MyServiceSchedule>
        {
            public bool Equals(MyServiceSchedule item1, MyServiceSchedule item2) {
                if (item1 == null && item2 == null) {
                    return true;
                } else if ((item1 != null && item2 == null) ||
                           (item1 == null && item2 != null)) {
                    return false;
                } else if (item1.Equipmentid == item2.Equipmentid &&
                           item1.Serviceid == item2.Serviceid &&
                           (item1.Servicedate > item2.Servicedate.AddDays(-1 * item2.Interval) &&
                           item1.Servicedate < item2.Servicedate.AddDays(item2.Interval))/* ||
                       (item2.Servicedate > item1.Servicedate.AddDays(-1 * item1.Interval) &&
                       item2.Servicedate < item1.Servicedate.AddDays(item1.Interval))*/
                          ) {
                    return true;
                } else {
                    return false;
                }
            }
            public int GetHashCode(MyServiceSchedule item) {
                return item.Equipmentid.GetHashCode() ^
                       item.Serviceid.GetHashCode() ^
                       item.Interval.GetHashCode();
            }
        }
        // Builds the filter string for a DataView.
        // Note: ViewSchedules.SetChecksInListBox() depends on this format.
        public static string GetServicesDataViewFilter(RadListBox lstCustServices) {
            string svs = "";
            if (lstCustServices.CheckedItems.Count > 0) {
                foreach (RadListBoxItem item in lstCustServices.CheckedItems) {
                    if (svs == "") {
                        svs = item.Value;
                    } else {
                        svs = svs + ", " + item.Value;
                    }
                }
                svs = "Serviceid IN (" + svs + ")";
            }
            return svs;
        }
        // Builds the filter string for a DataView.
        // Note: ViewSchedules.SetChecksInListBox() depends on this format.
        public static string GetEqTypesDataViewFilter(RadListBox lstCustEqTypes) {
            string eqtypes = "";
            if (lstCustEqTypes.CheckedItems.Count > 0) {
                foreach (RadListBoxItem item in lstCustEqTypes.CheckedItems) {
                    if (eqtypes == "") {
                        eqtypes = item.Value;
                    } else {
                        eqtypes = eqtypes + ", " + item.Value;
                    }
                }
                eqtypes = "Equipmenttypeid IN (" + eqtypes + ")";
            }
            return eqtypes;
        }
        /* 
         * unScheduledOption 
         * 0 = Scheduled Only
         * 1 = UnScheduled Only
         * 2 = Both
         */
        public const string DEFAULT_LOCATION = "[unassigned]";
        public const string VIEW_SCHEDULE_CACHE = "VIEW_SCHEDULE_CACHE";
        public static DataView GetServiceSchedulesByDatesAndEquipOrServiceType(int customerId, int equipmentId,
            DateTime dtFromDate, DateTime dtToDate, bool futureOption, 
            string services, string eqTypes, bool masterScheduleMode) {

            // Apply caching.
            if (System.Web.HttpContext.Current.Session[VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey] != "") {
                return (DataView)System.Web.HttpContext.Current.Session[VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey];
            } else {
                ATGDataContext db = new ATGDataContext();
                DataSet ds = GetSSDataSet();
                DataTable tbl = ds.Tables[0];

                // Get a list of Services.
                var svs = from s in db.Services
                          where customerId > 0 ? s.Customerid == customerId : true
                          //where s.Serviceinterval > 0 // 02/22/2012 Removed. Need non recurring items to be able to be scheduled.
                          //                               this does not affect the masterschedule mode, nor future items. It simply
                          //                               allows the view schedule mode to have a record for scheduling, for today's
                          //                               date all the time.
                          where ((services != "") && (services != null)) ? services.IndexOf(s.Serviceid.ToString()) > 0 : true
                          where s.Showinmasterschedule == true
                          select s;

                // If the services aren't set up yet, return an empty dataview.
                if ((svs.Count() == 0) || 
                    (dtToDate == DateTime.MinValue) || 
                    (dtFromDate == DateTime.MinValue)) {
                    return ds.Tables[0].AsDataView();
                }

                // Get the main data to work with.
                #region GET MAIN DATA
                var schSS = (from ss in db.Serviceschedules
                             where customerId > 0 ? ss.Equipment.Customerid == customerId : true
                             where equipmentId > 0 ? ss.Equipmentid == equipmentId : true
                             where // Open Scheduled Items
                                   (
                                   (ss.Servicedate != null) && (ss.Completeddate == null) && (ss.Cancelleddate == null)
                                   ) ||
                                   // Completed, Last Completed
                                   ( 
                                       ((ss.Completeddate != null) && (ss.Cancelleddate == null)) && // Must be completed
                                        ((ss.Completeddate >= dtFromDate.Date && ss.Completeddate <= dtToDate.Date) || // in date range or last date
                                         (ss.Completeddate >= (db.Serviceschedules.Where(x =>
                                                               x.Serviceid == ss.Serviceid &&
                                                               x.Equipmentid == ss.Equipmentid &&
                                                               x.Completeddate != null &&
                                                               x.Cancelleddate == null
                                                               ).Max(y => y.Completeddate) ) ))
                                   ) ||
                                   // Cancelled items
                                   (
                                        (ss.Completeddate == null) && (ss.Cancelleddate != null) && // Must be cancelled
                                        (ss.Cancelleddate >= dtFromDate.Date) && (ss.Cancelleddate <= dtToDate.Date)
                                   )
                             select ss);
                #endregion

                // Start building the MAIN RESULT DATASET a bit at a time.

                #region ADD SCHEDULED ITEMS TO RESULTS
                // Add the SCHEDULED items. We only want to show
                // SS that are within the range of the User's
                // criteria date range.
                foreach (ATGDB.Serviceschedule ss in schSS) {
                    // Add the record.
                    tbl.Rows.Add(new object[] {
                    ss.Servicescheduleid,
                    ss.Serviceid,
                    ss.Equipmentid,
                    ss.Equipment.Equipmenttypeid,
                    ss.Equipment.Equipmenttype.Description,
                    ss.Equipment.Description,
                    ss.Equipment.Customer.Description,
                    ss.Equipment.Tailnumber,
                    ss.Service.Description,
                    ss.Divisionarea == null ? "" : ss.Divisionarea.Description,
                    ss.Service.Serviceshortdescr.Description,
                    ss.Companydivision == null ? DEFAULT_LOCATION : ss.Companydivision.Location,
                    ss.Service.Serviceinterval,
                    ss.Service.Reportinterval,
                    ss.Service.Sequence,
                    ss.Servicedate,
                    ss.Servicetime,
                    ss.Completeddate,
                    ss.Completedtime,
                    ss.Cancelleddate,
                    ss.Hasactiveexception,
                    ss.Hadexception,
                    ss.Comments,
                    ss.Hasactiveexception == true ? "RED" : 
                        ss.Completeddate == null && ss.Cancelleddate == null ? "GREEN" : 
                        (ss.Completeddate != null && ss.Cancelleddate == null ? "YELLOW1" : "YELLOW2"),
                    0,
                    ss.Equipment.Equipmentstatuslogid <= 0 ? false : (ss.Equipment.Equipmentstatuslog != null ? ss.Equipment.Equipmentstatuslog.Unholddate == null : false),
                    ss.UpdDt,
                    ss.UpdUserId,
                    ss.Approvaldate,
                    ss.Approvaluserid
                    });
                }
                #endregion 

                #region ADD UNSCHEDULED ITEMS TO RESULTS
                // We need an "unscheduled" row for every service in the results
                // for users to be able to schedule new service. Note that some
                // services may already be in the results, so when computing
                // service dates, those last scheduled dates should be used.
                // If the service interval is zero, it is easy - just create the row.

                // Get a list of the equipment.
                var eq = from e in db.Equipments
                         where customerId > 0 ? e.Customerid == customerId : true
                         where equipmentId > 0 ? e.Equipmentid == equipmentId : true
                         where ((eqTypes != "") && (eqTypes != null)) ? eqTypes.IndexOf(e.Equipmenttypeid.ToString()) > 0 : true
                         select e;

                // Create rows for each service.
                int key = -1;
                foreach (ATGDB.Service s in svs) {
                    // Get the Serviceschedules for the current service being processed.
                    var data = schSS.Where(x => x.Serviceid == s.Serviceid).OrderBy(y => y.Servicedate);

                    if (data.Count() > 0) {
                        foreach (ATGDB.Serviceschedule ss2 in data) {
                            // Primary key counter.
                            key--;

                            // For each service schedule, add the unscheduled item.
                            // Top of the list should be the last completed item for this service.
                            // Add the record.
                            tbl.Rows.Add(new object[] {
                            key, 
                            s.Serviceid, 
                            ss2.Equipmentid,
                            ss2.Equipment.Equipmenttypeid,
                            ss2.Equipment.Equipmenttype.Description,
                            ss2.Equipment.Description,
                            ss2.Equipment.Customer.Description,
                            ss2.Equipment.Tailnumber,
                            s.Description,
                            ss2.Divisionarea == null ? "" : ss2.Divisionarea.Description,
                            s.Serviceshortdescr.Description,
                            DEFAULT_LOCATION,
                            s.Serviceinterval,
                            s.Reportinterval,
                            s.Sequence,
                            // If the service interval is zero, set the next date to the current (today's) date.
                            // If the service interval is > 0 then:
                            //      1. If the service date plus the interval in days is less than today, the service date is today.
                            //      2. If the service date plus the interval in days is greater than today, e.g. the future,
                            //         set the date to the service date plus interval.
                            s.Serviceinterval == 0 ? DateTime.Now.Date : 
                                DateTime.Now.Date > ss2.Servicedate.AddDays(s.Serviceinterval).Date ? DateTime.Now.Date :
                                    ss2.Servicedate.AddDays(s.Serviceinterval),
                            s.Serviceinterval == 0 ? DateTime.Now.Date : 
                                DateTime.Now.Date > ss2.Servicedate.AddDays(s.Serviceinterval).Date ? DateTime.Now.Date :
                                    ss2.Servicedate.AddDays(s.Serviceinterval),
                            null,
                            null,
                            null,
                            false,
                            false,
                            "",
                            "BLUE",
                            0,
                            ss2.Equipment.Equipmentstatuslogid <= 0 ? false : (ss2.Equipment.Equipmentstatuslog != null ? ss2.Equipment.Equipmentstatuslog.Unholddate == null : false),
                            null,
                            "",
                            null,
                            ""
                            });
                        }
                    } else {
                        // There isn't an existing matching service(s) in our results, so we
                        // need to insert one for each piece of equipment.
                        foreach (ATGDB.Equipment e in eq) {
                            // Primary key counter.
                            key--;

                            // Add the row for this equipment
                            tbl.Rows.Add(new object[] {
                            key,
                            s.Serviceid, 
                            e.Equipmentid,
                            e.Equipmenttypeid,
                            e.Equipmenttype.Description,
                            e.Description,
                            e.Customer.Description,
                            e.Tailnumber,
                            s.Description,
                            "", // Area
                            s.Serviceshortdescr.Description,
                            DEFAULT_LOCATION,
                            s.Serviceinterval,
                            s.Reportinterval,
                            s.Sequence,
                            // Set the service date to today since there isn't any other SS to use for reference.
                            DateTime.Now.Date,
                            DateTime.Now.Date,
                            null,
                            null,
                            null,
                            false,
                            false,
                            "",
                            "BLUE",
                            0,
                            e.Equipmentstatuslogid <= 0 ? false : (e.Equipmentstatuslog != null ? e.Equipmentstatuslog.Unholddate == null : false),
                            null,
                            "",
                            null,
                            ""
                            });
                        }
                    }
                }
                #endregion UNSCHEDULED ITEMS

                #region ADD FUTURE OPTION ITEMS TO RESULTS
                // Now, for the "future casting". At this point, the general
                // behavior for scheduled items, and unscheduled items is working.
                // However, unscheduled items are only one date into the future. We need
                // to project within the scope of the date range.
                if (futureOption == true) {
                    bool mergeTables = false;
                    bool loop = true;
                    DataSet ds2 = GetSSDataSet();
                    DataTable tbl2 = ds2.Tables[0];

                    foreach (DataRow row in tbl.Rows) {
                        if ((row["Status"].ToString() == "BLUE") &&
                            (int.Parse(row["Interval"].ToString()) > 0)) {
                            // Vars
                            loop = true;
                            mergeTables = true;
                            DateTime theDt = DateTime.Parse(row["Servicedate"].ToString()).Date;
                            int iDaysToAdd = int.Parse(row["Interval"].ToString());

                            // Add the future items based on the initial service date.
                            while (loop == true) {
                                DateTime computedDate = theDt.AddDays(iDaysToAdd);
                                if ((computedDate < dtToDate.Date) &&
                                    (computedDate >= dtFromDate.Date)) {
                                    loop = true;
                                
                                    // Add the record.
                                    tbl2.Rows.Add(new object[] {
                                    row["Servicescheduleid"],
                                    row["Serviceid"],
                                    row["Equipmentid"],             
                                    row["Equipmenttypeid"],
                                    row["Equipmenttypedescr"],
                                    row["Equipmentdescr"],
                                    row["CustomerName"],
                                    row["TailNumber"],
                                    row["Servicedescr"],
                                    row["Area"],
                                    row["Shortdescr"],
                                    DEFAULT_LOCATION,
                                    row["Interval"],
                                    row["ReportInterval"],
                                    row["Sequence"],
                                    computedDate,
                                    computedDate,
                                    null,
                                    null,
                                    null,
                                    false,
                                    false,
                                    "",
                                    "GRAY",
                                    0,
                                    row["OnHold"],
                                    null,
                                    "",
                                    null,
                                    ""
                                    });

                                    // Update orig date holder
                                    theDt = computedDate;
                                } else {
                                    // This means we have exceeded our boundary.
                                    loop = false;
                                }
                            }
                        }
                    } // for

                    // If we actually added future items to the future table holder, tbl2, merge the results.
                    if (mergeTables == true) {
                        DataTableReader reader = tbl2.CreateDataReader();
                        tbl.Load(reader);
                        tbl.EndLoadData();
                        reader.Close();
                    }

                } // future option
                #endregion FUTURE OPTION ITEMS

                // Grab the view for sorting and filtering.
                DataView view = ds.Tables[0].AsDataView();
                
                // Services and EQ Types Filter.
                if (!string.IsNullOrEmpty(services) && string.IsNullOrEmpty(eqTypes)) {
                    view.RowFilter = services;
                } else if (string.IsNullOrEmpty(services) && !string.IsNullOrEmpty(eqTypes)) {
                    view.RowFilter = eqTypes;
                } else if (!string.IsNullOrEmpty(services) && !string.IsNullOrEmpty(eqTypes)) {
                    view.RowFilter = services + " AND " + eqTypes;
                }

                // Finally, sort the dataset.
                view.Sort = "Servicedescr, Servicedate ASC";
                view.ApplyDefaultSort = true;
                view.AllowDelete = true;
                view.AllowEdit = true;
                view.AllowNew = true;

                // Cache the results.
                System.Web.HttpContext.Current.Session.Add(VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey, view);
                return view;
            }
        }
        private static DataSet GetSSDataSet() {        // Create a dataset.
            DataSet ds = new DataSet();
            // Add a table.
            DataTable tbl = ds.Tables.Add("ServiceSchedules");
            // Create the columns.
            DataColumn pk0 = tbl.Columns.Add("Servicescheduleid", typeof(Int32));
            DataColumn pk1 = tbl.Columns.Add("Serviceid", typeof(Int32));
            DataColumn pk2 = tbl.Columns.Add("Equipmentid", typeof(Int32));
            tbl.Columns.Add("Equipmenttypeid", typeof(Int32));
            tbl.Columns.Add("Equipmenttypedescr", typeof(string));
            tbl.Columns.Add("Equipmentdescr", typeof(string));
            tbl.Columns.Add("CustomerName", typeof(string));
            tbl.Columns.Add("TailNumber", typeof(string));
            tbl.Columns.Add("Servicedescr", typeof(string));
            tbl.Columns.Add("Area", typeof(string));
            tbl.Columns.Add("Shortdescr", typeof(string));
            tbl.Columns.Add("Location", typeof(string));
            tbl.Columns.Add("Interval", typeof(Int32));
            tbl.Columns.Add("ReportInterval", typeof(Int32));
            tbl.Columns.Add("Sequence", typeof(Int32));
            DataColumn pk3 = tbl.Columns.Add("Servicedate", typeof(DateTime));
            tbl.Columns.Add("Servicetime", typeof(DateTime));
            tbl.Columns.Add("Completeddate", typeof(DateTime));
            tbl.Columns.Add("Completedtime", typeof(DateTime));
            tbl.Columns.Add("Cancelleddate", typeof(DateTime));
            tbl.Columns.Add("Hasactiveexception", typeof(bool));
            tbl.Columns.Add("Hadexception", typeof(bool));
            tbl.Columns.Add("Comments", typeof(string));
            tbl.Columns.Add("Status", typeof(string));
            tbl.Columns.Add("TheGroup", typeof(Int32));
            tbl.Columns.Add("OnHold", typeof(bool));
            tbl.Columns.Add("UpdDt", typeof(DateTime));
            tbl.Columns.Add("UpdUserId", typeof(string));
            tbl.Columns.Add("Approvaldate", typeof(DateTime));
            tbl.Columns.Add("Approvaluserid", typeof(string));
            // Set primary key.
            tbl.PrimaryKey = new[] {pk0, pk1, pk2, pk3};
            return ds;
        }
        private static DataSet ConvertToDataSet(List<MyServiceSchedule> lst) {
            DataSet ds = GetSSDataSet();
            DataTable tbl = ds.Tables[0];
            // Add the data.
            foreach (MyServiceSchedule ss in lst) {
                tbl.Rows.Add(new object[] {
                ss.Servicescheduleid,
                ss.Serviceid,
                ss.Equipmentid,
                ss.Equipmenttypeid,
                ss.Equipmenttypedescr,
                ss.Equipmentdescr,
                ss.CustomerName,
                ss.TailNumber,
                ss.Servicedescr,
                ss.Area,
                ss.Shortdescr,
                ss.Location,
                ss.Interval,
                ss.ReportInterval,
                ss.Sequence,
                ss.Servicedate,
                ss.Servicetime,
                ss.Completeddate,
                ss.Completedtime,
                ss.Cancelleddate,
                ss.Hasactiveexception,
                ss.Hadexception,
                ss.Comments,
                ss.Status,
                ss.TheGroup,
                ss.OnHold,
                ss.UpdDt,
                ss.UpdUserId,
                ss.Approvaldate,
                ss.Approvaluserid
            });
            }
            // Return it.
            return ds;
        }
        /*
         * Returns a 120-Day range of current schedules for a given customer.
         */
        public static DataView GetAllServiceSchedules(int customerId, int range) {
            // Get a list of Scheduled and Completed items.
            ATGDataContext dc = new ATGDataContext();
            var data = (from ss in dc.Serviceschedules
                       where ss.Equipment.Customerid == customerId
                       //where ss.Servicedate >= DateTime.Now.Date
                       where ss.Servicedate <= DateTime.Now.Date.AddDays(range)
                       where ss.Completeddate == null
                       where ss.Cancelleddate == null
                       select ss)
                       .Union
                       (from ss in dc.Serviceschedules
                        where ss.Equipment.Customerid == customerId
                        where ss.Completeddate != null
                        where ss.Completeddate >= DateTime.Now.Date
                        where ss.Completeddate <= DateTime.Now.Date.AddDays(range)
                        where ss.Cancelleddate == null
                        select ss);

            // The Summary dataset.
            DataSet sumDs = GetSummaryDataSet();
            DataTable sumTbl = sumDs.Tables[0];
            DataRow sumRow;

            // Spin through the services for the customer, create a new dataset
            // grouping / counting by service id, interval, scheduled or not.
            var svcs = Service.GetMSorVSServicesByCustomerId(customerId);

            // Spin through each row and inspect count it / group it, etc.
            //for (int lcv = 0; lcv < dv.Table.Rows.Count; lcv++) {
            foreach (ATGDB.Serviceschedule ss in data) {
                //ssRow = dv.Table.Rows[lcv];

                ATGDB.Service svc = svcs.FirstOrDefault(x => x.Serviceid == ss.Serviceid);//int.Parse(ssRow["Serviceid"].ToString()));
                //foreach (ATGDB.Service svc in svcs) {
                if (svc != null) {
                    // Match the Service first.
                    //if (int.Parse(ssRow["Serviceid"].ToString()) == svc.Serviceid) {
                        // Eliminate completed items.
                        //if (ssRow["Completeddate"].ToString() == "") {
                            // Is it Scheduled or not?
                            if (ss.Completeddate == null) {//(int.Parse(ssRow["Servicescheduleid"].ToString()) > 0) {
                                // Scheduled items
                                // Do we have a summary row already?
                                sumRow = sumTbl.Rows.Find(new object[] { ss.Serviceid, true });
                                if (sumRow != null) {
                                    sumRow["Count"] = (int.Parse(sumRow["Count"].ToString()) + 1).ToString();
                                } else {
                                    // Add the row.
                                    sumTbl.Rows.Add(new object[] 
                                    {
                                    svc.Serviceid,
                                    svc.Sequence,
                                    svc.Description,
                                    svc.Serviceinterval,
                                    true,
                                    1
                                    });
                                }
                            } else {
                                // Completed items.
                                // Do we have a summary row already?
                                sumRow = sumTbl.Rows.Find(new object[] { ss.Serviceid, false });
                                if (sumRow != null) {
                                    sumRow["Count"] = (int.Parse(sumRow["Count"].ToString()) + 1).ToString();
                                } else {
                                    // Add the row.
                                    sumTbl.Rows.Add(new object[] 
                                    {
                                    svc.Serviceid,
                                    svc.Sequence,
                                    svc.Description,
                                    svc.Serviceinterval,
                                    false,
                                    1
                                    });
                                }
                            }
                        //}
                    //}
                }
            }

            // Create a view of the summary, and sort it.
            DataView view = sumTbl.AsDataView();
            view.Sort = "Scheduled DESC, Sequence";
            view.ApplyDefaultSort = true;

            return view;
        }
        private static DataSet GetSummaryDataSet() {        // Create a dataset.
            DataSet ds = new DataSet();
            // Add a table.
            DataTable tbl = ds.Tables.Add("Summary");
            // Create the columns.
            tbl.Columns.Add("Serviceid", typeof(Int32));
            tbl.Columns.Add("Sequence", typeof(Int32));
            tbl.Columns.Add("Description", typeof(string));
            tbl.Columns.Add("Interval", typeof(Int32));
            tbl.Columns.Add("Scheduled", typeof(bool));
            tbl.Columns.Add("Count", typeof(Int32));
            // Primary key.
            tbl.PrimaryKey = new DataColumn[] { tbl.Columns["Serviceid"], tbl.Columns["Scheduled"] };

            return ds;
        }
        public static void UpdateServiceSchedule(int serviceScheduleId, DateTime serviceDate, DateTime serviceTime, string comments) {
            ATGDataContext db = new ATGDataContext();

            ATGDB.Serviceschedule ss = db.Serviceschedules.SingleOrDefault(s => s.Servicescheduleid == serviceScheduleId);
            if (ss != null) {
                ss.Servicedate = serviceDate;
                ss.Servicetime = serviceTime;
                ss.Comments = comments;
                db.SubmitChanges();
            }
        }
        public static int InsertServiceSchedule(int? companyDivisionId, int equipmentId, int serviceId, DateTime serviceDate, DateTime? serviceTime, string comments) {
            ATGDataContext db = new ATGDataContext();

            ATGDB.Serviceschedule ss = new ATGDB.Serviceschedule();
            ss.Companydivisionid = companyDivisionId;
            ss.Equipmentid = equipmentId;
            ss.Serviceid = serviceId;
            ss.Servicedate = serviceDate;
            ss.Servicetime = serviceTime;
            ss.Completeddate = null;
            ss.Completedtime = null;
            ss.Cancelleddate = null;
            ss.Comments = comments;
            db.Serviceschedules.InsertOnSubmit(ss);
            db.SubmitChanges();
            return ss.Servicescheduleid;
        }
        public static void DeleteServiceSchedule(int serviceScheduleId) {
            ATGDataContext db = new ATGDataContext();

            ATGDB.Serviceschedule ss = db.Serviceschedules.SingleOrDefault(s => s.Servicescheduleid == serviceScheduleId);
            if (ss != null) {
                //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
                //m.IsActive = true;

                // Delete related service exceptions
                if (ss.Serviceexceptions.Count > 0) {
                db.SubmitChanges();
                    db.Serviceexceptions.DeleteAllOnSubmit(ss.Serviceexceptions);
                    db.SubmitChanges();
                }

                // Delete related service queue items
                if (ss.Servicequeueitems.Count > 0) {
                    db.Servicequeueitems.DeleteAllOnSubmit(ss.Servicequeueitems);
                    db.SubmitChanges();
                }

                db.Serviceschedules.DeleteOnSubmit(ss);

                // Delete related work log items
                if (ss.Worklog != null) {
                    db.Worklogs.DeleteOnSubmit(ss.Worklog);
                    ss.Worklog = null;
                }

                // Now look for an delete an un-logged service schedule
                // if it was auto scheduled as a result of this one.
                var data = from x in db.Servicesectionrules
                           where x.Serviceid == ss.Serviceid
                           select x;
                if ((data.Count() > 0) && (data.ElementAt(0).Sectionid == 999999)) { // Do not Change - used in Servicesectionrule.cs also
                    // This service says to auto schedule the next one.
                    // Find that next one and if it is still non-logged or cancelled just delete it. It is not valid.
                    // By virtue of being completed or cancelled it is considered valid, so don't delete it.
                    var data2 = from xss in db.Serviceschedules
                               where xss.Equipmentid == ss.Equipmentid
                               where xss.Serviceid == ss.Serviceid
                               where xss.Servicedate == ss.Servicedate.AddDays(ss.Service.Serviceinterval)
                               where xss.Servicetime == ss.Servicetime
                               where xss.Completeddate == null
                               where xss.Cancelleddate == null
                               select xss;
                    if (data2.Count() > 0) {
                        // Found one or more. delete them.
                        db.Serviceschedules.DeleteAllOnSubmit(data2);
                    }
                }

                db.SubmitChanges();
            }
        }

        /*
         * For creating and displaying the "Master Schedule Array"
         * 
         */
        public const string MASTER_SCHEDULE_CACHE = "MASTER_SCHEDULE_CACHE";
        public static DataView GetMasterSchedulesByDatesAndEquipOrServiceType(int customerId, int equipmentId,
                                                                          DateTime dtFromDate, DateTime dtToDate,
                                                                          bool futureOption, string services, string eqTypes) {
            // Apply caching.
            if (System.Web.HttpContext.Current.Session[MASTER_SCHEDULE_CACHE + ScheduleHelper.Instance.MasterPageKey] != "") {
                return (DataView)System.Web.HttpContext.Current.Session[MASTER_SCHEDULE_CACHE + ScheduleHelper.Instance.MasterPageKey];
            } else {
                                 
                // Clear/Create the cache first.
                System.Web.HttpContext.Current.Session.Add(VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey, "");

                // Get the schedule information first - all tails (tail = 0).
                // MasterScheduleMode, the last argument, true.
                DataView origData = GetServiceSchedulesByDatesAndEquipOrServiceType(customerId, 0, dtFromDate, dtToDate, futureOption, services, eqTypes, true);
               
                // Get current profile.
                ProfileCommon profile = (ProfileCommon)System.Web.HttpContext.Current.Profile;

                // Build a new cross-tab dataset, expanding columns based on the date range, rows by tail.
                DataSet newData = new DataSet();
                // Add a table.
                DataTable tbl = newData.Tables.Add("CrossTab");

                // Create the columns.        
                // Tail number column is the first, primary key.
                DataColumn c = tbl.Columns.Add("Tailnumber", typeof(string));
                c.Unique = true;
                c.Caption = "Tailnumber";

                c = tbl.Columns.Add("Taildesc", typeof(string));
                c.Unique = false;
                c.Caption = "Taildesc";

                c = tbl.Columns.Add("TaildescUrl", typeof(string));
                c.Unique = false;
                c.Caption = "TaildescUrl";

                c = tbl.Columns.Add("Equipmenttypedescr", typeof(string));
                c.Unique = false;
                c.Caption = "Equipmenttypedescr";

				// Primary key.
				tbl.PrimaryKey = new DataColumn[] { tbl.Columns["Tailnumber"] };
				
				if ((dtToDate != DateTime.MinValue) && (dtFromDate != DateTime.MinValue)) {

                    // Vars
                    DataRow row = null;
                    string sUrl = "";
                    string sExtraUrl = "";
                    if (SecurityUtils.GetATGMode() == SecurityUtils.ATG_MODE_CUSTOMER) {
                        sExtraUrl = "/Customers/" + SecurityHelper.Instance.ProviderRoot;
                    }

                    // Sort first.
                    origData.ApplyDefaultSort = false;
                    origData.Sort = "Servicedate Asc";
                    origData.RowFilter = "";

                    // Get a unique list of dates to process.
                    var dates = new List<DateTime>();
                    foreach (DataRowView dr in origData) {
						if (DateTime.Parse(dr["Servicedate"].ToString()).Date >= dtFromDate.Date) {
							dates.Add(DateTime.Parse(dr["Servicedate"].ToString()).Date);
						}
                    }

					// Now make distinct list.
					dates = dates.Distinct().OrderBy(xxx => xxx.Date).ToList<DateTime>();
					
					// Add all the columns to the result table
					int cnt = 0;
					foreach (var ddd in dates) {
						// Dynamic column name.
						c = tbl.Columns.Add("col" + cnt, typeof(string));
						c.Unique = false;
						// Set Caption to the date.
						c.Caption = ddd.ToShortDateString();

						// URL COLUMN
						// Dynamic column name.
						c = tbl.Columns.Add("url" + cnt, typeof(string));
						c.Unique = false;
						// Set Caption to the date.
						c.Caption = "Url Hidden" + cnt;
						cnt++;
					}	  

					// Some Vars.
					// Process the list of dates looking for them in the origData results.
					// Dynamically add columns across the date range for columns that have data.
					int iColIndex = 0;
					string theColor = "";
					string theBackColor = "Transparent";
					string spacer = "&nbsp;";

					// Populate the result table
					foreach (DataRow ss in origData.Table.Rows) {
						// What column are we working on?
						iColIndex = dates.IndexOf(DateTime.Parse(ss["Servicedate"].ToString()).Date);
						
						if (iColIndex >= 0) {
							// Determine the color to use
							if (ss["Status"].ToString() == "YELLOW1") {
								theColor = ColorUtils.GetSS_CompletedColorString(profile.SiteTheme);
							} else if (ss["Status"].ToString() == "YELLOW2") {
								theColor = ColorUtils.GetSS_CancelledColorString(profile.SiteTheme);
							} else if (ss["Status"].ToString() == "GREEN") {
								theColor = ColorUtils.GetSS_ScheduledColorString(profile.SiteTheme);
								if (Convert.ToDateTime(ss["Servicedate"]).Date < DateTime.Now.Date) {
									theBackColor = ColorUtils.GetSS_LateBackColor(profile.SiteTheme);
								}
							} else if (ss["Status"].ToString() == "RED") {
								theColor = ColorUtils.GetSS_CompletedExColorString(profile.SiteTheme);
							} else if ((ss["Status"].ToString() == "GRAY") || (ss["Status"].ToString() == "BLUE")) {
								theColor = ColorUtils.GetSS_FutureColorString(profile.SiteTheme);
							}

                            // Look for an existing row in the new data first,
                            // if found, update the correct column. If not
                            // found, insert a new row and update the correct column.
                            row = tbl.Rows.Find(ss["TailNumber"]);
                            // If the column exists...it may not exist for future items.
                            if (row != null) {
                                // We have a row, update the column info
                                // Now add it.
                                sUrl = "~" + sExtraUrl + "/Schedules/ViewSchedules.aspx?Mode=3&tail=" + ss["TailNumber"].ToString() +
										"&dt=" + DateTime.Parse(ss["Servicedate"].ToString()).ToShortDateString() +
                                        "&cid=" + customerId +
                                        "&services=" + services +
                                        "&eqTypes=" + eqTypes;

								if (String.IsNullOrEmpty(row["col" + iColIndex].ToString())) {
                                    row["col" + iColIndex] = "<span style=\"font-weight: bold; font-size: 12px; color: " + theColor +
                                                        "; background-color: " + theBackColor + "\">" + ss["Shortdescr"] + "</span>";
                                } else {
                                    row["col" + iColIndex] = row["col" + iColIndex] + spacer +
                                                        "<span style=\"font-weight: bold; font-size: 12px; color: " + theColor +
                                                        "; background-color: " + theBackColor + "\">" + ss["Shortdescr"] + "</span>";
                                }
							} else {
								// Insert a new row with the PK value of Tailnumber.
								row = tbl.Rows.Add(new object[] { ss["TailNumber"] });

								// The URL
								sUrl = "~" + sExtraUrl + "/Schedules/ViewSchedules.aspx?Mode=2&tail=" + ss["TailNumber"].ToString() +
										"&dtFrom=" + dtFromDate.ToShortDateString() +
										"&dtTo=" + dtToDate.ToShortDateString() +
										"&cid=" + customerId +
										"&services=" + services +
										"&eqTypes=" + eqTypes;

								// Update the tail number desc with the URL
								row["Taildesc"] = ss["TailNumber"].ToString();
								row["TaildescUrl"] = sUrl;
								row["Equipmenttypedescr"] = ss["Equipmenttypedescr"].ToString();

								// Update the specific column information for the row.
								sUrl = "~/" + sExtraUrl + "Schedules/ViewSchedules.aspx?Mode=3&tail=" + ss["TailNumber"].ToString() +
										"&dt=" + DateTime.Parse(ss["Servicedate"].ToString()).ToShortDateString() +
										"&cid=" + customerId +
										"&services=" + services +
										"&eqTypes=" + eqTypes;

								row["col" + iColIndex] = "<span style=\"font-weight: bold; font-size: 12px; color: " + theColor +
													"; background-color: " + theBackColor + "\">" + ss["Shortdescr"] + "</span>";
							}
							row["url" + iColIndex] = sUrl;
						}
					}
                }

                // Create a view of the data, and sort it.
                DataView newDataView = tbl.AsDataView();
                // Sort.
                //newDataView.ApplyDefaultSort = true;
                System.Web.HttpContext.Current.Session.Add(MASTER_SCHEDULE_CACHE + ScheduleHelper.Instance.MasterPageKey, newDataView);
                return newDataView;
            }
        }

        public static void CancelServiceSchedule(string ssId, DateTime cancelledDt, DateTime? rescheduleDt, string notes) {
            // Cancel the service schedule.
            ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
            ATGDB.Serviceschedule ss = db.Serviceschedules.SingleOrDefault(x => x.Servicescheduleid == int.Parse(ssId));
            if (ss != null) {
                ss.Cancelleddate = cancelledDt;
                if (ss.Comments != "") {
                    ss.Comments = ss.Comments + " :: " + notes;
                } else {
                    ss.Comments = notes;
                }
                db.SubmitChanges();
            }

            // Create the new one.
            if (rescheduleDt != null) {
                ATGDB.Serviceschedule newSS = new ATGDB.Serviceschedule();
                newSS.Serviceid = ss.Serviceid;
                newSS.Servicedate = rescheduleDt.Value;
                newSS.Comments = "Rescheduled: " + ss.Comments;
                //newSS.Companydivisionid = ss.Companydivisionid;
                newSS.Equipmentid = ss.Equipmentid;
                newSS.Servicetime = ss.Servicetime;
                db.Serviceschedules.InsertOnSubmit(newSS);
                db.SubmitChanges();
            }
        }

        public static DataSet GetSSDS(DateTime serviceDate, Boolean unapprovedOnly, Int32 companyDivisionId, Int32 customerId) {
            ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
            IEnumerable data;
            if (unapprovedOnly == true) {
                data = from ss in dc.Serviceschedules
                       where ((ss.Companydivisionid == companyDivisionId) || (ss.Companydivisionid == null))
                       where ss.Equipment.Customerid == customerId
                       where ss.Approvaldate == null
                       orderby ss.Servicedate ascending
                       select ss;
            } else {
                data = from ss in dc.Serviceschedules
                       where ss.Servicedate.Date == serviceDate.Date
                       where ((ss.Companydivisionid == companyDivisionId) || (ss.Companydivisionid == null))
                       where ss.Equipment.Customerid == customerId
                       orderby ss.Servicedate ascending
                       select ss;
            }
            DataSet ds = new DataSet();
            DataTable tbl = ds.Tables.Add("ServiceSchedules");
            tbl.Columns.Add("Servicescheduleid", typeof(Int32));
            tbl.Columns.Add("Companydivisionid", typeof(Int32));
            tbl.Columns.Add("Customerid", typeof(Int32));
            tbl.Columns.Add("Equipmentid", typeof(Int32));
            tbl.Columns.Add("Tailnumber", typeof(string));
            tbl.Columns.Add("Serviceid", typeof(Int32));
            tbl.Columns.Add("Servicedescr", typeof(string));
            tbl.Columns.Add("Area", typeof(string));
            tbl.Columns.Add("Servicedate", typeof(DateTime));
            tbl.Columns.Add("Servicetime", typeof(DateTime));
            tbl.Columns.Add("Completeddate", typeof(DateTime));
            tbl.Columns.Add("Completedtime", typeof(DateTime));
            tbl.Columns.Add("Cancelleddate", typeof(DateTime));
            tbl.Columns.Add("Comments", typeof(string));
            tbl.Columns.Add("Approvaldate", typeof(DateTime));
            tbl.Columns.Add("Approvaluserid", typeof(string));
            tbl.Columns.Add("ChangeHistory", typeof(string));
            tbl.Columns.Add("UpdDt", typeof(DateTime));
            tbl.Columns.Add("UpdUserId", typeof(string));
            foreach (Serviceschedule ss in data) {
                tbl.Rows.Add(new object[] {
                    ss.Servicescheduleid,
                    ss.Companydivisionid,
                    ss.Equipment.Customerid,
                    ss.Equipmentid,
                    ss.Equipment.Tailnumber,
                    ss.Serviceid,
                    ss.Service.Description,
                    ss.Divisionarea == null ? "" : ss.Divisionarea.Description,
                    ss.Servicedate,
                    ss.Servicetime,
                    ss.Completeddate,
                    ss.Completedtime,
                    ss.Cancelleddate,
                    ss.Comments,
                    ss.Approvaldate,
                    ss.Approvaluserid,
                    ss.ChangeHistory,
                    ss.UpdDt,
                    ss.UpdUserId
                });
            }
            return ds;
        }
        public static void UpdateSS(Serviceschedule ss) {
            throw new NotImplementedException("Serviceschedule.UpdateSS Has Not Been Implemented");
        }
        public static void DeleteSS(Serviceschedule ss) {
            //throw new NotImplementedException("Serviceschedule.DeleteSS Has Not Been Implemented");
            DeleteServiceSchedule(ss.Servicescheduleid);
        }
    }

}