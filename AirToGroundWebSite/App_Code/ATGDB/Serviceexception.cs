﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Serviceexception
/// </summary>
namespace ATGDB
{
    public partial class Serviceexception
    {
        public static IEnumerable<ATGDB.Serviceexception> GetServiceexceptionsByServiceScheduleId(Int32 serviceScheduleId) {
            ATGDataContext dc = new ATGDataContext();
            var data = from ze in dc.Serviceexceptions
                       where ze.Servicescheduleid == serviceScheduleId
                       select ze;
            return data.ToList<ATGDB.Serviceexception>();
        }
        public static IEnumerable<ATGDB.Serviceexception> GetServiceexceptionsByWorkLogId(Int32 workLogId) {
            ATGDataContext dc = new ATGDataContext();
            var data = from ze in dc.Serviceexceptions
                       where ze.Originalworklogid == workLogId
                       select ze;
            return data.ToList<ATGDB.Serviceexception>();
        }
        public static IEnumerable<ATGDB.Serviceexception> GetOutstandingExceptionsByServiceScheduleId(Int32 serviceScheduleId) {
            ATGDataContext db = new ATGDataContext();
                                    
            // Get the current SS.
            ATGDB.Serviceschedule ss = db.Serviceschedules.Single(s => s.Servicescheduleid == serviceScheduleId);

            // Get the rule and find the last sequence.
            ATGDB.Service lastSvc;
            if ((ss.Service.Sequence != null) && (ss.Service.Sequence > 0)) {
                lastSvc = Service.GetLastService(ss.Service.Sequence, ss.Equipment.Customerid);
            } else {
                lastSvc = ss.Service;
            }

            var data = GetOpenServiceExceptions(ss.Equipment.Equipmentid, lastSvc.Serviceid);
            return data.ToList<ATGDB.Serviceexception>();
        }
        public class ServiceExceptionEqualityComparer : IEqualityComparer<ATGDB.Serviceexception>
        {
            public bool Equals(ATGDB.Serviceexception item1, ATGDB.Serviceexception item2) {
                if (item1 == null && item2 == null) {
                    return true;
                } else if ((item1.Serviceexceptionid > 0 && item2.Serviceexceptionid <= 0) ||
                           (item1.Serviceexceptionid <= 0 && item2.Serviceexceptionid > 0)) {
                    return false;
                } else if (item1.Serviceexceptionid == item2.Serviceexceptionid) {
                    return true;
                } else {
                    return false;
                }
            }
            public int GetHashCode(ATGDB.Serviceexception item) {
                return item.Serviceexceptionid.GetHashCode();
            }
        }
        public static void CreateServiceException(Worklog wl, Serviceschedule ss, Int32 sectionId, string exNotes) {
            ATGDataContext db = new ATGDataContext();
            ATGDB.Serviceexception sex = new ATGDB.Serviceexception();
            sex.Servicescheduleid = ss.Servicescheduleid;
            sex.Serviceid = ss.Serviceid;
            sex.Sectionid = sectionId;
            sex.Originalworklogid = wl.Worklogid;
            sex.Exceptionnotes = exNotes;
            sex.Exceptiondate = wl.Workeddate;
            sex.Exceptionby = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            sex.Equipmentid = ss.Equipmentid;

            // This is where it gets tricky. If the exceptions for service schedule were completed on another worklog,
            // it needs to be updated here, otherwise the exceptions will remain open and will appear at the closing
            // of the next scheduled item.
            // Look at all the exceptions for the SS, and grab closed/completed information if it exists. This is 
            // NOT a perfect situation. It is possible for exceptions to be split between two work logs. But, without
            // user interaction, this is as close as we can get.
            // *** DEPRECATED - Do not allow editing of closed/completed Service Exceptions
            //foreach (ATGDB.Serviceexception sex2 in ss.Serviceexceptions) {
            //    if (sex2.Closeddate != null) {
            //        sex.Closeddate = sex2.Closeddate;
            //        sex.Closedby = sex2.Closedby;
            //        sex.Closednotes = sex2.Closednotes;
            //        break;
            //    } else if (sex2.Completeddate != null) {
            //        sex.Completeddate = sex2.Completeddate;
            //        sex.Completedby = sex2.Completedby;
            //        sex.Completednotes = sex2.Completednotes;
            //        break;
            //    }
            //}
            // Insert the new exception.
            db.Serviceexceptions.InsertOnSubmit(sex);
            db.SubmitChanges();
        }
        public static IEnumerable<ATGDB.Serviceexception> GetOpenServiceExceptions(int equipmentId, int serviceId) {
            ATGDataContext db = new ATGDataContext();
            var data = from sex in db.Serviceexceptions
                       where sex.Equipment.Equipmentid == equipmentId
                       where sex.Serviceid == serviceId
                       where sex.Closeddate == null
                       where sex.Completeddate == null
                       select sex;
            return data;
        }
        public static List<ATGDB.Section> GetOutstandingExceptionSections(int curSsId) {
            ATGDataContext db = new ATGDataContext();

            // Get the current SS.
            ATGDB.Serviceschedule ss = db.Serviceschedules.Single(s => s.Servicescheduleid == curSsId);

            // Get the rule and find the last sequence.
            ATGDB.Service lastSvc;
            if ((ss.Service.Sequence != null) && (ss.Service.Sequence > 0)) {
                lastSvc = Service.GetLastService(ss.Service.Sequence, ss.Equipment.Customerid);
            } else {
                lastSvc = ss.Service;
            }

            var data = GetOpenServiceExceptions(ss.Equipment.Equipmentid, lastSvc.Serviceid);
            List<ATGDB.Section> sections = new List<ATGDB.Section>();
            ATGDB.Section section;
            foreach (ATGDB.Serviceexception ex in data) {
                section = new ATGDB.Section();
                section.Sectionid = ex.Sectionid;
                section.Customerid = ex.Section.Customerid;
                section.Description = ex.Section.Description;
                sections.Add(section);
            }
            return sections;
        }
        public static List<ATGDB.Section> GetCurrentExceptionSections(int curSsId, out string notes) {
            ATGDataContext db = new ATGDataContext();

            // Get the current SS.
            ATGDB.Serviceschedule ss = db.Serviceschedules.Single(s => s.Servicescheduleid == curSsId);

            // Get exceptions for current SS.
            var data = GetOpenServiceExceptions(ss.Equipment.Equipmentid, ss.Serviceid);
            List<ATGDB.Section> sections = new List<ATGDB.Section>();
            ATGDB.Section section;
            notes = "";
            foreach (ATGDB.Serviceexception ex in data) {
                notes = ex.Exceptionnotes;
                section = new ATGDB.Section();
                section.Sectionid = ex.Sectionid;
                section.Customerid = ex.Section.Customerid;
                section.Description = ex.Section.Description;
                sections.Add(section);
            }
            return sections;
        }
        public static List<ATGDB.Section> GetAllExceptionSections(int curSsId, out string notes) {
            ATGDataContext db = new ATGDataContext();

            // Get the current SS.
            ATGDB.Serviceschedule ss = db.Serviceschedules.Single(s => s.Servicescheduleid == curSsId);

            // Get exceptions for current SS.
            var data = GetServiceexceptionsByServiceScheduleId(ss.Servicescheduleid);
            List<ATGDB.Section> sections = new List<ATGDB.Section>();
            ATGDB.Section section;
            notes = "";
            foreach (ATGDB.Serviceexception ex in data) {
                notes = ex.Exceptionnotes;
                section = new ATGDB.Section();
                section.Sectionid = ex.Sectionid;
                section.Customerid = ex.Section.Customerid;
                section.Description = ex.Section.Description;
                sections.Add(section);
            }
            return sections;
        }
        public void UpdateServiceException(Int32 Servicescheduleid, Int32 Serviceid,
                                              Int32 Sectionid, Int32 Equipmentid,
                                              string Username,
                                              DateTime Closeddate, string Closednotes,
                                              DateTime Completeddate, string Completednotes,
                                              Int32 Serviceexceptionid) {
            ATGDataContext db = new ATGDataContext();
            // Get the exception and udpate it.
            ATGDB.Serviceexception sex = db.Serviceexceptions.Single(s => s.Serviceexceptionid == Serviceexceptionid);
            if (Closeddate > DateTime.Parse("1/1/0001 12:00:00 AM")) {
                sex.Closeddate = Closeddate;
                sex.Closednotes = Closednotes;
                sex.Closedby = Username;
                sex.Completeddate = null;
                sex.Completednotes = null;
                sex.Completedby = null;
            }
            if (Completeddate > DateTime.Parse("1/1/0001 12:00:00 AM")) {
                sex.Completeddate = Completeddate;
                sex.Completednotes = Completednotes;
                sex.Completedby = Username;
                sex.Closeddate = null;
                sex.Closednotes = null;
                sex.Closedby = null;
            }
            // Commit the changes.
            db.SubmitChanges();

            // Check to see if all exceptions have been either completed or closed.
            var data = Serviceexception.GetServiceexceptionsByServiceScheduleId(Servicescheduleid);
            bool allClosed = false;
            foreach (ATGDB.Serviceexception sx in data) {
                allClosed = sx.Closeddate != null || sx.Completeddate != null;
                if (allClosed == false) {
                    break;
                }
            }

            // If everything closed, need to clear the HasActiveException flag on the SS.
            if (allClosed == true) {
                ATGDB.Serviceschedule ss = db.Serviceschedules.Single(s => s.Servicescheduleid == Servicescheduleid);
                ss.Hasactiveexception = false;
                db.SubmitChanges();
            }
        }
    }
}