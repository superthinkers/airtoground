﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Section
/// </summary>
namespace ATGDB
{
    public partial class Section
    {
		public static IEnumerable<ATGDB.Section> GetSectionsByServiceId(Int32 Serviceid) {
			ATGDataContext dc = new ATGDataContext();
			var data = from sz in dc.Servicesectionjoins
					   join z in dc.Sections on sz.Sectionid equals z.Sectionid
					   where sz.Serviceid == Serviceid
					   select z;
			return data;
		}
		public static IEnumerable<ATGDB.Section> GetSectionsByCustomerId(Int32 Customerid) {
			ATGDataContext dc = new ATGDataContext();
			var data = from z in dc.Sections
					   where z.Customerid == Customerid
					   select z;
			return data;
		}
		public static IEnumerable<ATGDB.Section> GetSectionsByServiceScheduleId(Int32 serviceScheduleId) {
			ATGDataContext dc = new ATGDataContext();
			ATGDB.Serviceschedule ss = Serviceschedule.GetServiceSchedule(serviceScheduleId);
			var data = from sz in dc.Servicesectionjoins
					   join z in dc.Sections on sz.Sectionid equals z.Sectionid
					   where sz.Serviceid == ss.Serviceid
					   select z;
			return data;
		}
    }
}