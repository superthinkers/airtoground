﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Service
/// </summary>
namespace ATGDB
{
    public partial class Service
    {
        public class MyInterval
        {
            public string Interval { get; set; }
            public string Description { get; set; }
            public MyInterval(string interval) {
                Interval = interval;
                Description = interval;
            }
        }
        public List<MyInterval> GetIntervalData() {
            List<MyInterval> data = new List<MyInterval>();
            data.Add(new MyInterval("0"));
            data.Add(new MyInterval("1"));
            data.Add(new MyInterval("3"));
            data.Add(new MyInterval("7"));
            data.Add(new MyInterval("14"));
            data.Add(new MyInterval("15"));
            data.Add(new MyInterval("21"));
            data.Add(new MyInterval("28"));
            data.Add(new MyInterval("30"));
            data.Add(new MyInterval("45"));
            data.Add(new MyInterval("60"));
            data.Add(new MyInterval("75"));
            data.Add(new MyInterval("90"));
            data.Add(new MyInterval("120"));
            data.Add(new MyInterval("150"));
            data.Add(new MyInterval("180"));
            data.Add(new MyInterval("360"));
            return data;
        }
        public class MyRate
        {
            public string RateKind { get; set; }
            public string Description { get; set; }
            public MyRate(string ratekind, string description) {
                RateKind = ratekind;
                Description = description;
            }
        }
        public List<MyRate> GetRateData() {
            List<MyRate> data = new List<MyRate>();
            data.Add(new MyRate("F", "Flat Fee"));
            data.Add(new MyRate("H", "Hourly"));
            return data;
        }
        public class MyShortDescription
        {
            public string ShortDescription { get; set; }
            public string Description { get; set; }
            public MyShortDescription(string key, string descr) {
                ShortDescription = key;
                Description = descr;
            }
        }
        public List<MyShortDescription> GetShortDescriptionData() {
            List<MyShortDescription> data = new List<MyShortDescription>();
            data.Add(new MyShortDescription("", ""));
            data.AddRange(GetShortDescriptionData2());
            return data;
        }
        public List<MyShortDescription> GetShortDescriptionData2() {
            List<MyShortDescription> data = new List<MyShortDescription>();
            data.Add(new MyShortDescription("0", "No Interval"));
            data.Add(new MyShortDescription("1", "1 Day"));
            data.Add(new MyShortDescription("3", "3 Day"));
            data.Add(new MyShortDescription("7", "7 Day"));
            data.Add(new MyShortDescription("14", "14 Day"));
            data.Add(new MyShortDescription("15", "15 Day"));
            data.Add(new MyShortDescription("21", "21 Day"));
            data.Add(new MyShortDescription("28", "28 Day"));
            data.Add(new MyShortDescription("30", "30 Day"));
            data.Add(new MyShortDescription("45", "45 Day"));
            data.Add(new MyShortDescription("60", "60 Day"));
            data.Add(new MyShortDescription("75", "75 Day"));
            data.Add(new MyShortDescription("90", "90 Day"));
            data.Add(new MyShortDescription("120", "120 Day"));
            data.Add(new MyShortDescription("150", "150 Day"));
            data.Add(new MyShortDescription("180", "180 Day"));
            data.Add(new MyShortDescription("360", "360 Day"));
            // These typically used when interval is 0 days
            data.Add(new MyShortDescription("BM", "Ball Mats"));
            data.Add(new MyShortDescription("BW", "Bi-Weekly"));
            data.Add(new MyShortDescription("CC", "Complete Cleaning"));
            data.Add(new MyShortDescription("CP", "Cockpit"));
            data.Add(new MyShortDescription("CS", "Crew Service"));
            data.Add(new MyShortDescription("LV", "Lavatory"));
            data.Add(new MyShortDescription("MX", "Miscellaneous"));
            data.Add(new MyShortDescription("MS", "Meal Service"));
            data.Add(new MyShortDescription("MO", "Monthly"));
            data.Add(new MyShortDescription("NR", "Non-Routine"));
            data.Add(new MyShortDescription("PW", "Potable Water"));
            data.Add(new MyShortDescription("SP", "Special"));
            data.Add(new MyShortDescription("SR", "Snow Removal"));
            data.Add(new MyShortDescription("TR", "Trash Removal"));
            data.Add(new MyShortDescription("TM", "Twice Monthly"));
            data.Add(new MyShortDescription("WI", "Water Incursion"));
            data.Add(new MyShortDescription("WK", "Weekly"));
            // Interval > 0 days
            data.Add(new MyShortDescription("Z1", "Zone 1"));
            data.Add(new MyShortDescription("Z2", "Zone 2"));
            data.Add(new MyShortDescription("Z3", "Zone 3"));
            data.Add(new MyShortDescription("Z4", "Zone 4"));
            data.Add(new MyShortDescription("Z5", "Zone 5"));
            data.Add(new MyShortDescription("Z6", "Zone 6"));
            return data;
        }
        public static IEnumerable<ATGDB.Service> GetServicesByEquipmentId(Int32 equipmentid) {
            ATGDataContext dc = new ATGDataContext();
            ATGDB.Equipment e = dc.Equipments.SingleOrDefault(x => x.Equipmentid.Equals(equipmentid));
            var data = from st in dc.Services 
                       where st.Customerid == e.Customerid
                       select st;
            return data;
        }
        public static IEnumerable<ATGDB.Service> GetServicesByCustomerId(Int32 customerId) {
            ATGDataContext dc = new ATGDataContext();
            var data = from st in dc.Services
                       where st.Customerid == customerId
                       //where st.Showinmasterschedule == true
                       select st;
            return data;
        }
        public static IEnumerable<ATGDB.Service> GetMSorVSServicesByCustomerId(Int32 customerId) {
            ATGDataContext dc = new ATGDataContext();
            var data = from st in dc.Services
                       where st.Customerid == customerId
                       where st.Showinmasterschedule == true
                       orderby st.Description
                       select st;
            return data;
        }
        public static IEnumerable<ATGDB.Service> GetServiceQueueServicesByCustomerId(Int32 customerId) {
            ATGDataContext dc = new ATGDataContext();
            var data = from st in dc.Services
                       where st.Customerid == customerId
                       where st.Showinservicequeue == true
                       select st;
            return data;
        }
        public static ATGDB.Service GetServiceByScheduleId(int serviceScheduleId) {
            ATGDataContext db = new ATGDataContext();
            ATGDB.Serviceschedule ss = Serviceschedule.GetServiceSchedule(serviceScheduleId);
            ATGDB.Service st = db.Services.SingleOrDefault(x => x.Serviceid == ss.Serviceid);
            return st;
        }
        public static ATGDB.Service GetLastService(int? curSeq, int customerId) {
            ATGDataContext db = new ATGDataContext();

            // Get a list of Services for the customer.
            var svs = from s in db.Services
                      where s.Customerid == customerId
                      where s.Sequence != null
                      orderby s.Sequence descending
                      select s;

            // What is the max sequence?
            int? maxSeq = svs.ElementAt(0).Sequence;

            // Get a reference to the last Service.
            ATGDB.Service lastSvc;
            if (curSeq > 1) {
                lastSvc = svs.Where(x => x.Sequence == curSeq - 1).ElementAt(0);
            } else {
                lastSvc = svs.Where(x => x.Sequence == maxSeq).ElementAt(0);
            }
            return lastSvc;
        }
    }
}