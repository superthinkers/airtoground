﻿using System.Collections.Generic;
using System.Linq;
using System.Data;

/// <summary>
/// Summary description for Jobcode
/// </summary>
namespace ATGDB
{
    public partial class Jobcode
    {
        public static ATGDB.Jobcode GetJobcodeById(string jobCodeId) {
            if (jobCodeId != "") {
                ATGDataContext dc = new ATGDataContext();
                var jc = dc.Jobcodes.SingleOrDefault(x => x.Jobcodeid == int.Parse(jobCodeId));
                return jc;
            } else {
                return null;
            }
        }

        public static ATGDB.Jobcode GetJobCodeFromServiceEquipmentTypeJoin(int serviceId, int equipTypeId) {
            // Get the job code.
            ATGDataContext db = new ATGDataContext();
            ATGDB.Serviceequipmenttypejobcodejoin setjcj = db.Serviceequipmenttypejobcodejoins.SingleOrDefault(
                x => x.Serviceid == serviceId && x.Equipmenttypeid == equipTypeId);
            ATGDB.Jobcode jc = null;
            if (setjcj != null) {
                jc = setjcj.Jobcode;
            }
            return jc;
        }

        public static int GetJobCodeIdFromServiceEquipmentTypeJoin(int serviceId, int equipTypeId) {
            // Get the job code.
            ATGDataContext db = new ATGDataContext();
            ATGDB.Serviceequipmenttypejobcodejoin setjcj = db.Serviceequipmenttypejobcodejoins.SingleOrDefault(
                x => x.Serviceid == serviceId && x.Equipmenttypeid == equipTypeId);
            int jcId = -1;
            if (setjcj != null) {
                jcId = setjcj.Jobcodeid;
            }
            return jcId;
        }

        public static List<ATGDB.Jobcode> GetUniqueJobsByCompanyCustomer(string companyDivisionId, string customerId) {
            ATGDataContext dc = new ATGDataContext();
            var data = from ccj in dc.Companycustomerjobjoins
                       from j in dc.Jobcodes
                       where j.Jobcodeid == ccj.Jobcodeid
                       where ccj.Companydivisionid == int.Parse(companyDivisionId)
                       where ccj.Customerid == int.Parse(customerId)
                       where ccj.Jobcode.Isactive == true
                       select j;
            return data.ToList<ATGDB.Jobcode>();
        }

        public static List<ATGDB.Jobcode> GetUniqueJobsByCustomer(string customerId) {
            ATGDataContext dc = new ATGDataContext();
            var data = (from ccj in dc.Companycustomerjobjoins
                       from j in dc.Jobcodes
                       where j.Jobcodeid == ccj.Jobcodeid
                       where customerId == "-1" ? true == true : ccj.Customerid == int.Parse(customerId)
                       where ccj.Jobcode.Isactive == true
                       select j).Distinct();
            return data.ToList<ATGDB.Jobcode>();
        }

        public static List<ATGDB.Companycustomerjobjoin> GetCompanyCustomerJobJoins(string companyDivisionId, string customerId) {
            ATGDataContext dc = new ATGDataContext();
            var data = from ccj in dc.Companycustomerjobjoins
                       where ccj.Companydivisionid == int.Parse(companyDivisionId)
                       where ccj.Customerid == int.Parse(customerId)
                       where ccj.Jobcode.Isactive == true
                       select ccj;
            return data.ToList<ATGDB.Companycustomerjobjoin>();
        }
        public static DataSet GetCompanyCustomerJobJoinsDS(string companyDivisionId, string customerId) {
            List<ATGDB.Companycustomerjobjoin> data = GetCompanyCustomerJobJoins(companyDivisionId, customerId);
            DataSet ds = new DataSet("ccjjs");
            DataTable dt = ds.Tables.Add("ccjj");
            dt.Columns.Add("Companydivisionid", typeof(int));
            dt.Columns.Add("Customerid", typeof(int));
            dt.Columns.Add("Jobcodeid", typeof(int));
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Jobcodedescr", typeof(string));
            dt.Columns.Add("Requiresworkcard", typeof(bool));
            dt.Columns.Add("Requirestailnumber", typeof(bool));
            foreach (ATGDB.Companycustomerjobjoin ccjj in data) {
                dt.Rows.Add(new object[] { ccjj.Companydivisionid, ccjj.Customerid,  
                    ccjj.Jobcodeid, ccjj.Jobcode.Code, ccjj.Jobcode.Description, ccjj.Jobcode.Requiresworkcard, ccjj.Jobcode.Requirestailnumber });
            }
            return ds;
        }
    }
}