﻿using System.Collections.Specialized;

/// <summary>
/// Summary description for ATGRoleProvider
/// </summary>
/// 
public class ATGRoleProvider : Devart.Data.MySql.Web.Providers.MySqlRoleProvider
{
    public override void Initialize(string name, NameValueCollection config) {
        // Pass doctored config to base classes        
        base.Initialize(name, config);
    }

}