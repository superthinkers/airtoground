﻿using System.Collections.Specialized;

/// <summary>
/// Summary description for ATGProfileProvider
/// </summary>
public class ATGProfileProvider : Devart.Data.MySql.Web.Providers.MySqlProfileProvider
{
    public override void Initialize(string name, NameValueCollection config) {
        // DEPRECATED. Using "ApplicationName" switching in web.config providers.
        // Update the security database connection string.
        //SecurityUtils.UpdateSecurityDBConnectionString(config);
        
        // Pass doctored config to base classes        
        base.Initialize(name, config);
    }

}