﻿using System.Collections.Specialized;

/// <summary>
/// Summary description for ATGMembershipProvider
/// </summary>
public class ATGMembershipProvider : Devart.Data.MySql.Web.Providers.MySqlMembershipProvider
{
    public override void Initialize(string name, NameValueCollection config) {
        //// Remap the provider to the right one.
        //string mapName = "";
        //if (name.Contains("Admin_AspNetMySqlMembershipProvider")) {
        //    mapName = "Admin_AspNetMySqlMembershipProvider_" + SecurityHelper.Instance.ProviderRoot;
        //} else {
        //    mapName = SecurityHelper.Instance.ProviderRoot + "_MembershipProvider";
        //}
        //// Only initialize the right provider.
        //if (name.Contains(SecurityHelper.Instance.ProviderRoot)) {
            base.Initialize(name, config);
        //}
    }
}