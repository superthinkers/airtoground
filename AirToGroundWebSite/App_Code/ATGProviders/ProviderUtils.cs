﻿using System;
using System.Web.Security;
using System.Reflection;
using System.Configuration.Provider;

/// <summary>
/// Summary description for ProviderUtils
/// </summary>
public static class ProviderUtils
{

    //public static void SetupATGProvider() {
    //    var field = typeof(ProviderCollection).GetField("_ReadOnly", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField | BindingFlags.GetField);
    //    lock (Membership.Providers.SyncRoot) {
    //        field.SetValue(Membership.Providers["624_MembershipProvider"], false);
    //        Membership.Providers.Remove("624_MembershipProvider");
    //        field.SetValue(Membership.Providers, true);

    //        field.SetValue(Membership.Providers["Admin_AspNetMySqlMembershipProvider_624"], false);
    //        Membership.Providers.Remove("Admin_AspNetMySqlMembershipProvider_624");
    //        field.SetValue(Membership.Providers, true);

    //        field.SetValue(Membership.Providers["201619_MembershipProvider"], false);
    //        Membership.Providers.Remove("201619_MembershipProvider");
    //        field.SetValue(Membership.Providers, true);

    //        field.SetValue(Membership.Providers["Admin_AspNetMySqlMembershipProvider_201619"], false);
    //        Membership.Providers.Remove("Admin_AspNetMySqlMembershipProvider_201619");
    //        field.SetValue(Membership.Providers, true);
    //    }
    //}

    //public static void Setup624Provider() {
    //    var field = typeof(ProviderCollection).GetField("_ReadOnly", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField | BindingFlags.GetField);
    //    lock (Membership.Providers.SyncRoot) {
    //        field.SetValue(Membership.Providers["ATG_MembershipProvider"], false);
    //        Membership.Providers.Remove("ATG_MembershipProvider");
    //        field.SetValue(Membership.Providers, true);

    //        field.SetValue(Membership.Providers["Admin_AspNetMySqlMembershipProvider_ATG"], false);
    //        Membership.Providers.Remove("Admin_AspNetMySqlMembershipProvider_ATG");
    //        field.SetValue(Membership.Providers, true);

    //        field.SetValue(Membership.Providers["201619_MembershipProvider"], false);
    //        Membership.Providers.Remove("201619_MembershipProvider");
    //        field.SetValue(Membership.Providers, true);

    //        field.SetValue(Membership.Providers["Admin_AspNetMySqlMembershipProvider_201619"], false);
    //        Membership.Providers.Remove("Admin_AspNetMySqlMembershipProvider_201619");
    //        field.SetValue(Membership.Providers, true);
    //    }
    //}

    //public static void Setup201619Provider() {
    //    var field = typeof(ProviderCollection).GetField("_ReadOnly", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField | BindingFlags.GetField);
    //    lock (Membership.Providers.SyncRoot) {
    //        field.SetValue(Membership.Providers["ATG_MembershipProvider"], false);
    //        Membership.Providers.Remove("ATG_MembershipProvider");
    //        field.SetValue(Membership.Providers, true);

    //        field.SetValue(Membership.Providers["Admin_AspNetMySqlMembershipProvider_ATG"], false);
    //        Membership.Providers.Remove("Admin_AspNetMySqlMembershipProvider_ATG");
    //        field.SetValue(Membership.Providers, true);

    //        field.SetValue(Membership.Providers["624_MembershipProvider"], false);
    //        Membership.Providers.Remove("624_MembershipProvider");
    //        field.SetValue(Membership.Providers, true);

    //        field.SetValue(Membership.Providers["Admin_AspNetMySqlMembershipProvider_624"], false);
    //        Membership.Providers.Remove("Admin_AspNetMySqlMembershipProvider_624");
    //        field.SetValue(Membership.Providers, true);
    //    }
    //}

    // MEMBERSHIP
    public static MembershipUser GetUser(string userName) {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        if ((!String.IsNullOrEmpty(userName)) &&
            (Membership.Providers[providerRoot + "_MembershipProvider"] != null)) {
            return Membership.Providers[providerRoot + "_MembershipProvider"].GetUser(userName, true);
        } else {
            return null;
        }
    }
    public static MembershipUser CreateUser(string userName, string password, string email, string ques, string ans, bool appr, out MembershipCreateStatus status) {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        return Membership.Providers[providerRoot + "_MembershipProvider"].CreateUser(userName, password, email, ques, ans, appr, null, out status);
    }
    public static void UpdateUser(MembershipUser user) {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        Membership.Providers[providerRoot + "_MembershipProvider"].UpdateUser(user);
    }
    public static void DeleteUser(string user) {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        Membership.Providers[providerRoot + "_MembershipProvider"].DeleteUser(user, true);
    }
    public static MembershipUserCollection GetAllUsers() {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        int x;
        return Membership.Providers[providerRoot + "_MembershipProvider"].GetAllUsers(0, 999, out x);
    }
    public static bool ChangePassword(string userName, string oldPw, string newPw) {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        return Membership.Providers[providerRoot + "_MembershipProvider"].ChangePassword(userName, oldPw, newPw);        
    }
    public static bool ChangePasswordQuestionAndAnswer(string userName, string curPw, string newQues, string newAns) {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        return Membership.Providers[providerRoot + "_MembershipProvider"].ChangePasswordQuestionAndAnswer(userName, curPw, newQues, newAns);
    }

    // ROLES
    public static void CreateRole(string roleName) {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        Roles.Providers[providerRoot + "_RoleProvider"].CreateRole(roleName);
    }
    public static void AddUsersToRoles(string[] users, string[] roles) {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        Roles.Providers[providerRoot + "_RoleProvider"].AddUsersToRoles(users, roles);
    }
    public static void RemoveUsersFromRoles(string[] users, string[] roles) {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        Roles.Providers[providerRoot + "_RoleProvider"].RemoveUsersFromRoles(users, roles);
    }
    public static void AddUserToRole(string user, string role) {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        Roles.Providers[providerRoot + "_RoleProvider"].AddUsersToRoles(new string[] { user }, new string[] { role });
    }
    public static bool IsUserInRole(string user, string role) {
        try {
            string providerRoot = SecurityHelper.Instance.ProviderRoot;
            return Roles.Providers[providerRoot + "_RoleProvider"].IsUserInRole(user, role);
        } catch {
            return false;
        }
    }
    public static string[] GetAllRoles() {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        return Roles.Providers[providerRoot + "_RoleProvider"].GetAllRoles();
    }
    public static string[] GetRolesForUser(string user) {
        string providerRoot = SecurityHelper.Instance.ProviderRoot;
        return Roles.Providers[providerRoot + "_RoleProvider"].GetRolesForUser(user);
    }

    // PROFILE
    // Called from Default.cs
    //public static void InitializeProfile(ProfileCommon profile, string user) {
    //    profile.Providers[providerRoot + "_ProfileProvider"].Initialize(user, );
    //}
}