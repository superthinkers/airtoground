using System;
using System.Web;
using System.Web.UI;
using Telerik.Web.UI;

public static class PageUtils
{
	public static Control FindControlRecursive(Control Root, string Id) {
		if (Root.ID == Id)
			return Root;     

		foreach (Control Ctl in Root.Controls) {
			Control FoundCtl = FindControlRecursive(Ctl, Id);
			if (FoundCtl != null)
				return FoundCtl;
		}
		return null;
	}

    // Adds necessary script to master page to use a session keep alive pulse.
    public static void RegisterSessionTimeoutPulse(Page page) {
        // Clear the counter var.
        HttpContext.Current.Session.Add("KeepSessionAliveCounter", "");
        // Enable the pulse in the page.
        string pulseEnabled = System.Web.Configuration.WebConfigurationManager.AppSettings["SESSION_PULSE_ENABLED"].ToUpper();
        if (pulseEnabled.Equals("TRUE")) {
            string sAppPath = "";//(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath == "/" ? "" : System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            if (System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath == "/") {
                // DEV
                if (SecurityHelper.Instance.ProviderRoot == "ATG") {
                    sAppPath = "/localhost:25265";
                } else {
                    sAppPath = "/localhost:25265/Customers/" + SecurityHelper.Instance.ProviderRoot;
                }
            } else {
                // PROD
                if (SecurityHelper.Instance.ProviderRoot == "ATG") {
                    sAppPath = "https://www.airtogroundtrack.com"; 
                } else {
                    sAppPath = "https://www.airtogroundtrack.com/Customers/" + SecurityHelper.Instance.ProviderRoot;
                }
            }
            string pulse = System.Web.Configuration.WebConfigurationManager.AppSettings["SESSION_PULSE"];
            string script =
            "$(function () { " +
            "    setInterval(KeepSessionAlive, " + pulse + "); " +
            "}); " +
            "function KeepSessionAlive() { " +
            //"    $.post(\"" + sAppPath + "/KeepSessionAlive.ashx\", " +
            "    $.post(\"/KeepSessionAlive.ashx\", " +
            "           null, " +
            "           function (data) { " +
            "           if (data == \"SessionTimedOut\") { " + // Value set in KeepSessionAlive.ashx
            "               $(window).attr(\"location\",\"" + sAppPath + "/Security/SessionTimeout.aspx\");" + // Redirect to timeout page
            "           }" +
            "           }); " +
            "}";
            page.ClientScript.RegisterStartupScript(page.GetType(), "Pulse_Script", script, true);
        }
    }
    /*public static class CacheHelper
    {
        const int _seconds = 600;
        public static void CacheThis(HttpResponse response) {
            // Only cache for X seconds.
            response.Cache.SetExpires(DateTime.Now.AddSeconds(_seconds));
            response.Cache.SetMaxAge(new TimeSpan(0, 0, _seconds));
            response.Cache.SetCacheability(HttpCacheability.Public);
            response.Cache.SetValidUntilExpires(true);

            // Sliding expiration means we reset the X seconds after each request.
            // SetETag is required to work around one aspect of sliding expiration.
            response.Cache.SetSlidingExpiration(true);
            response.Cache.SetETagFromFileDependencies();
        }
    }*/

    public static void SetRadGridImagePathAllControls(Control cPageRoot, string sTheme) {
        foreach (Control ctrl in cPageRoot.Controls) {
            if (ctrl is RadGrid) {
                ((RadGrid)ctrl).ImagesPath = "~/App_Themes/" + sTheme + "/Grid/";
            } else {
                SetRadGridImagePathAllControls(ctrl, sTheme);
            }
        }
    }

    public static void RegisterAllSkins(Page page, RadStyleSheetManager mgr) {
        string assembly = "";
        if (page.Theme == "Corp") {
            assembly = "ATGCorpSkin";
        } else if (page.Theme == "624") {
            assembly = "ATGFedExSkin";
        } else if (page.Theme == "201619") {
            assembly = "ATGUPSSkin";
        }
        StyleSheetReference ss;
        //ss = new StyleSheetReference(assembly + ".Button.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".Calendar.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".ColorPicker.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".ComboBox.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".DataPager.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".Dock.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".Editor.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".FileExplorer.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".Filter.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".FormDecorator.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".Grid.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".ImageEditor.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".Input.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".ListBox.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".ListView.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".Menu.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".Notification.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".OrgChart.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".PanelBar.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".Rating.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".RibbonBar.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".Rotator.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".Scheduler.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".SiteMap.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".Slider.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".SocialShare.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".Splitter.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".TabStrip.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".TagCloud.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".ToolBar.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".ToolTip.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".TreeList.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".TreeView.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        //ss = new StyleSheetReference(assembly + ".Upload.ATGSkin.css", assembly);
        //mgr.StyleSheets.Add(ss);
        ss = new StyleSheetReference(assembly + ".Window.ATGSkin.css", assembly);
        mgr.StyleSheets.Add(ss);
    }

    public static DateTime LoadDateChars(string chars) {
        string year = "";
        string month = "";
        string day = "";
        Byte[] timebytes = System.Text.ASCIIEncoding.ASCII.GetBytes(chars);
        int cnt = 0;
        foreach (char c in timebytes) {
			if ((c.ToString() != "?") && (c.ToString() != "/")) {
				if (cnt < 2) {
					month += c.ToString();
				} else if (cnt < 5) {
					day += c.ToString();
				} else if (cnt < 9) {
					year += c.ToString();
				}
			} else {
				cnt++;
			}
		}
        return DateTime.Parse(month + "/" + day + "/" + year + " " + DateTime.MinValue.Hour + ":" + DateTime.MinValue.Minute + ":" + DateTime.MinValue.Second);
    }

    public static DateTime LoadDateTimeChars(string chars) {
        string year = "";
        string month = "";
        string day = "";
        string hour = "";
        string min = "";
        string sec = "";
		string meridian = "";
        Byte[] timebytes = System.Text.ASCIIEncoding.ASCII.GetBytes(chars);
        int cnt = 0;
        foreach (char c in timebytes) {
            if ((c.ToString() != "?") && (c.ToString() != "/")) {
                if (cnt < 2) {
                    month += c.ToString();
                } else if (cnt < 5) {
                    day += c.ToString();
                } else if (cnt < 9) {
                    year += c.ToString();
                }
            } else {
                cnt++;
            }
        }
        cnt = 0;
        foreach (char c in timebytes) {
			if ((c.ToString() != "?") && (c.ToString() != ":")) {
				if ((cnt > 6) && (cnt < 9)) {
					hour += c.ToString();
                } else if ((cnt > 9) && (cnt < 12)) {
					min += c.ToString();
                } else if ((cnt > 12) && (cnt < 15)) {
					sec += c.ToString();
                } else if ((cnt > 14) && (cnt < 17)) {
					meridian += c.ToString();
					if (meridian == "PM") {
						hour = ((int.Parse(hour) + 12).ToString());
					}
				}
			} else {
				cnt++;
			}
		}
        return DateTime.Parse(month + "/" + day + "/" + year + " " + hour + ":" + min + ":" + sec + " " + meridian);
    }

}
