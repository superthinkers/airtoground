﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Security;
//using Devart.Data.MySql.Web.Providers;
using ATGDB;

/// <summary>
/// Summary description for MembershipHelper
/// </summary>
public class MembershipHelper
{

    public class CheckedRole
    {
        public bool IsChecked { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public CheckedRole() {
        }
        public CheckedRole(bool isChecked, string roleName, string userName) {
            IsChecked = isChecked;
            RoleName = roleName;
            UserName = userName;
        }
    }

    public static ProfileCommon GetProfile(string userName) {
        return new ProfileCommon().GetProfile(userName);
    }

    public DataSet GetAllUsers() {
        MembershipUserCollection users = ProviderUtils.GetAllUsers();

        DataSet d = new DataSet();
        DataTable t = d.Tables.Add("Users");
        t.Columns.Add("Userid", typeof(String));
        t.Columns.Add("UserName", typeof(String));
        t.Columns.Add("Email", typeof(String));
        t.Columns.Add("Password", typeof(String));
        t.Columns.Add("IsApproved", typeof(Boolean));
        t.Columns.Add("SiteTheme", typeof(String));
        t.Columns.Add("Companydivisionid", typeof(String));
        t.Columns.Add("Companydivisiondescription", typeof(String));
        t.Columns.Add("Customerid", typeof(String));
        t.Columns.Add("Customerdescription", typeof(String));

        foreach (MembershipUser u in users) {
            // Get the security user.
            ProfileCommon pc = new ProfileCommon().GetProfile(u.UserName);
            t.Rows.Add(new object[] {u.ProviderUserKey.ToString(), u.UserName, u.Email, "", u.IsApproved, pc.SiteTheme, 
                                  pc.Companydivisionid, pc.Companydivisiondescription,
                                  pc.Customerid, pc.Customerdescription});
        }
        return d;
    }

    public DataView GetAllUsersEx() {
        MembershipUserCollection users = ProviderUtils.GetAllUsers();

        DataSet d = new DataSet();
        DataTable t = d.Tables.Add("Users");
        t.Columns.Add("Userid", typeof(String));
        t.Columns.Add("UserName", typeof(String));
        t.Columns.Add("Email", typeof(String));
        t.Columns.Add("Password", typeof(String));
        t.Columns.Add("IsApproved", typeof(Boolean));
        t.Columns.Add("SiteTheme", typeof(String));
        t.Columns.Add("Companydivisionid", typeof(String));
        t.Columns.Add("Companydivisiondescription", typeof(String));
        t.Columns.Add("Customerid", typeof(String));
        t.Columns.Add("Customerdescription", typeof(String));
        // From Extended User
        t.Columns.Add("Employeenumber", typeof(String));
        t.Columns.Add("Lastname", typeof(String));
        t.Columns.Add("Firstname", typeof(String));
        t.Columns.Add("Updusername", typeof(String));
        t.Columns.Add("Salaryamount", typeof(float));
        t.Columns.Add("Hourlyrate", typeof(float));
        t.Columns.Add("Useca24hourrule", typeof(Boolean));

        ATGDataContext dc = new ATGDataContext();

        ATGDB.Extendeduser exu;
        ProfileCommon pc;
        foreach (MembershipUser u in users) {
            // Get the security user.
            pc = new ProfileCommon().GetProfile(u.UserName);
            // Get the extended user.
            exu = dc.Extendedusers.SingleOrDefault<Extendeduser>(x => x.Userid.Equals(u.ProviderUserKey.ToString()));
            if (exu != null) {
                t.Rows.Add(new object[] {u.ProviderUserKey.ToString(), u.UserName, u.Email, "", u.IsApproved, pc.SiteTheme, 
                                  pc.Companydivisionid, pc.Companydivisiondescription,
                                  pc.Customerid, pc.Customerdescription,
                                  // Extended user fields.
                                  exu.Employeenumber, exu.Lastname, exu.Firstname, exu.Updusername, 
                                  exu.Salaryamount, exu.Hourlyrate, exu.Useca24hourrule});
            }
        }
        
        DataView view = d.Tables[0].AsDataView();
        view.Sort = "Lastname ASC";
        view.AllowDelete = true;
        view.AllowEdit = true;
        view.AllowNew = true;
        view.ApplyDefaultSort = true;
        
        return view;
    }

    public List<CheckedRole> GetCheckedRolesForUser(string userName) {
        List<CheckedRole> CheckedRoles = new List<CheckedRole>();
        if (!String.IsNullOrEmpty(userName)) {
            string[] userroles = ProviderUtils.GetRolesForUser(userName);
            string[] roles = ProviderUtils.GetAllRoles();
            bool found = false;
            foreach (string role in roles) {
                found = false;
                foreach (string userrole in userroles) {
                    found = role == userrole;
                    if (found) break;
                }
                // Never show Customer role. Manage under covers.
                if (role != "Customer") {
                    CheckedRoles.Add(new CheckedRole(found, role, userName));
                }
            }
        }
        return CheckedRoles;
    }

    public string[] GetRolesForUser(string userName) {
        string[] roles = ProviderUtils.GetRolesForUser(userName);
        return roles;
    }

    public string[] GetAllRoles() {
        string[] roles = ProviderUtils.GetAllRoles();
        return roles;         
    }

    public void UpdateUserRoles(bool isChecked, string roleName, string userName) {
        if (isChecked) {
            if (!ProviderUtils.IsUserInRole(userName, roleName)) {
                // Add the role.
                ProviderUtils.AddUsersToRoles(new string[] { userName }, new string[] { roleName });
            }
        } else {
            if (ProviderUtils.IsUserInRole(userName, roleName)) {
                // Remove the role.
                ProviderUtils.RemoveUsersFromRoles(new string[] { userName }, new string[] { roleName });
            }
        }
    }

    public void UpdateUser(string userId, string userName, string email, bool isApproved) {
        // Update the user.
        MembershipUser u = ProviderUtils.GetUser(userName);
        u.Email = email;
        u.IsApproved = isApproved;
        ProviderUtils.UpdateUser(u);
        
        // Make sure user is unlocked.
        if (u.IsApproved) {
            u.UnlockUser();
        }
    }
    public void UpdateUserEx(string userId, string userName, string email, bool isApproved,
                             /*string employeeNumber,*/ string lastName, string firstName,
                             float salaryAmount, float hourlyRate, bool useCa24hourRule) {
        // Update the user.
        MembershipUser u = ProviderUtils.GetUser(userName);
        u.Email = email;
        u.IsApproved = isApproved;
        ProviderUtils.UpdateUser(u);

        // Update extended user record.
        ATGDataContext dc = new ATGDataContext();
        ATGDB.Extendeduser exu = dc.Extendedusers.SingleOrDefault(x => x.Userid == u.ProviderUserKey.ToString());
        if (exu != null) {
            //exu.Username = u.UserName;
            //if (exu.Employeenumber != employeeNumber) {
            //    exu.Employeenumber = employeeNumber;
            //}
            exu.Updusername = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            exu.Firstname = firstName;
            exu.Lastname = lastName;
            exu.Salaryamount = salaryAmount;
            exu.Hourlyrate = hourlyRate;
            exu.Useca24hourrule = useCa24hourRule;
            dc.SubmitChanges();
        }

        // Make sure user is unlocked.
        if (u.IsApproved) {
            u.UnlockUser();
        }
    }

    public static string NO_QUESTION = "no question";
    public static string NO_ANSWER = "no answer";
    public bool InsertUser(string userName, string currentUserName, string email, string password, bool isApproved) {
        // Create a new user.
        MembershipCreateStatus status;
        MembershipUser u = ProviderUtils.CreateUser(userName, password, email, NO_QUESTION, NO_ANSWER, isApproved, out status);
        if (status != MembershipCreateStatus.Success) {
            //throw new Exception(status.ToString());
            return false;
        }

        // Customers get the "Customer" Role as a default.
        if (SecurityUtils.GetATGMode() == SecurityUtils.ATG_MODE_CUSTOMER) {
            ProviderUtils.AddUserToRole(u.UserName, "Customer");
        }

        // Create the user's custom profile.
        ProfileCommon pc = new ProfileCommon().GetProfile(userName);
        pc.Userid = u.ProviderUserKey.ToString();
        pc.SiteTheme = "";
        pc.Companydivisionid = "";
        pc.Companydivisiondescription = "";
        pc.Companydivisionlocation = "";
        pc.Customerid = "";
        pc.Customerdescription = "";
        pc.ProviderRoot = SecurityHelper.Instance.ProviderRoot;
        pc.Save();

        // Email user with new pw.
        EmailUtils.SendPasswordChangedMail(HttpContext.Current.Server, userName, currentUserName, password);
        return true;
    }
    public void InsertUserEx(string userName, string currentUserName, string email, string password, bool useCa24hourRule,
                             string firstName, string lastName, string employeeNumber) {
        InsertUserEx(userName, currentUserName, email, password, false, employeeNumber, lastName, firstName,
            0.00f, 0.00f, useCa24hourRule);
    }
    public bool InsertUserEx(string userName, string currentUserName, string email, string password, bool isApproved,
                             string employeeNumber, string lastName, string firstName,
                             float salaryAmount, float hourlyRate, bool useCa24hourRule) {
        // Create a new user.
        MembershipCreateStatus status;
        MembershipUser u = ProviderUtils.CreateUser(userName, password, email, NO_QUESTION, NO_ANSWER, isApproved, out status);
        if (status != MembershipCreateStatus.Success) {
            if (status.ToString() == "InvalidPassword") {
                throw new Exception("You may not use that password");
                //return false;
            } else if (status.ToString() == "DuplicateUserName") {
                throw new Exception("That user name has been used already");
            } else {
                throw new Exception(status.ToString());
            }
        }

        // Customers get the "Customer" Role as a default.
        if (SecurityUtils.GetATGMode() == SecurityUtils.ATG_MODE_CUSTOMER) {
            SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_CUSTOMER, u.UserName);
        }

        // Create the user's custom profile.
        ProfileCommon pc = new ProfileCommon().GetProfile(userName);
        pc.Userid = u.ProviderUserKey.ToString();
        pc.SiteTheme = "";
        pc.Companydivisionid = "";
        pc.Companydivisiondescription = "";
        pc.Companydivisionlocation = "";
        pc.Customerid = "";
        pc.Customerdescription = "";
        pc.ProviderRoot = SecurityHelper.Instance.ProviderRoot;
        pc.Save();

        // Add extended user record.
        ATGDB.Extendeduser exu = new ATGDB.Extendeduser();
        exu.Userid = u.ProviderUserKey.ToString();
        exu.Employeenumber = employeeNumber;
        exu.Updusername = currentUserName;
        exu.Firstname = firstName;
        exu.Lastname = lastName;
        exu.Salaryamount = salaryAmount;
        exu.Hourlyrate = hourlyRate;
        exu.Useca24hourrule = useCa24hourRule;
        ATGDataContext dc = new ATGDataContext();
        dc.Extendedusers.InsertOnSubmit(exu);
        dc.SubmitChanges();

        // Email user with new pw.
        try {
            EmailUtils.SendPasswordChangedMail(HttpContext.Current.Server, userName, currentUserName, password);
        } catch {
            // Silent exception. Only occurs during development as the mail server doesn't send mail.
        }
        return true;
    }

    public static void DeleteUser(string userName) {
        MembershipUser user = ProviderUtils.GetUser(userName);
        string userId = user.ProviderUserKey.ToString();

        // Delete joins
        ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
        var data = from ccuj in db.Companycustomeruserjoins
                   where ccuj.Userid == userId
                   select ccuj;
        bool doSubmit = false;
        foreach (ATGDB.Companycustomeruserjoin x in data) {
            db.Companycustomeruserjoins.DeleteOnSubmit(x);
            doSubmit = true;
        }
        if (doSubmit) {
            db.SubmitChanges();
        }

        // Delete audit trail
        var data2 = from at in db.Audittrails
                    where at.Userid == userId
                    select at;
        doSubmit = false;
        foreach (ATGDB.Audittrail a in data2) {
            db.Audittrails.DeleteOnSubmit(a);
            doSubmit = true;
        }
        if (doSubmit) {
            db.SubmitChanges();
        }

        // Delete any extended user records. (if not customer because customers never created any)
        var users = from u in db.Extendedusers
                    where u.Userid == userId
                    select u;
        doSubmit = false;
        foreach (ATGDB.Extendeduser exu in users) {
            db.Extendedusers.DeleteOnSubmit(exu);
            doSubmit = true;
        }
        if (doSubmit) {
            db.SubmitChanges();
        }
        
        // Delete the user and all associated records.
        ProviderUtils.DeleteUser(userName);
    }

    public List<CheckedCCUJoin> CheckedCCUJoins = new List<CheckedCCUJoin>();
    public class CheckedCCUJoin
    {
        public bool IsChecked { get; set; }
        public bool IsDefault { get; set; }
        public int CompanyDivisionId { get; set; }
        public string CompanyDescr { get; set; }
        public int CustomerId { get; set; }
        public string CustomerDescr { get; set; }
        public CheckedCCUJoin() {
        }
        public CheckedCCUJoin(bool isChecked, bool isDefault, int companyDivisionId, 
                string companyDescr, int customerId, string customerDescr) {
            IsChecked = isChecked;
            IsDefault = isDefault;
            CompanyDivisionId = companyDivisionId;
            CompanyDescr = companyDescr;
            CustomerId = customerId;
            CustomerDescr = customerDescr;
        }
    }

    //public const string MH_CHECKED_CCUJ = "MH_CHECKED_CCUJ";
    public List<CheckedCCUJoin> GetCheckedCCUJoinsForUser(string userName) {
        //HttpContext context = HttpContext.Current;
        //if ((context.Session[MH_CHECKED_CCUJ] != null) && (context.Session[MH_CHECKED_CCUJ] != "")) {
        //    return (List<CheckedCCUJoin>)context.Session[MH_CHECKED_CCUJ];
        //} else {
            if (!String.IsNullOrEmpty(userName)) {
                MembershipUser user = ProviderUtils.GetUser(userName);
                string userId = user.ProviderUserKey.ToString();
                var allCCJ = GetCompanyCustomers();
                var allCCUJ = GetCompanyCustomerUserJoins(userId);
                ProfileCommon pc = new ProfileCommon().GetProfile(userName);
                bool found = false;
                bool isdefault = false;
                foreach (ATGDB.Companycustomerjoin ccj in allCCJ) {
                    found = false;
                    foreach (ATGDB.Companycustomeruserjoin ccuj in allCCUJ) {
                        //if (SecurityUtils.GetATGMode() != SecurityUtils.ATG_MODE_CUSTOMER) {
                            found = ((ccuj.Userid == userId) &&
                                     (ccj.Companydivisionid == ccuj.Companydivisionid) &&
                                     (ccj.Customerid == ccuj.Customerid));

                        //} else {
                        //    found = ((ccuj.Userid == userId) &&
                        //             (ccj.Companydivisionid == ccuj.Companydivisionid) &&
                        //             (ccj.Customerid.Equals(SecurityUtils.GetCustId())));
                        //}
                        isdefault = false;
                        if (found) {
                            // If this matches the profile, it is the default.
                            if (((pc.Companydivisionid != "") && (pc.Customerid != "")) &&
                                (ccuj.Companydivisionid == int.Parse(pc.Companydivisionid)) &&
                                (ccuj.Customerid == int.Parse(pc.Customerid))) {
                                isdefault = true;
                            }
                            break;
                        }
                    }
                    CheckedCCUJoins.Add(new CheckedCCUJoin(found, isdefault, ccj.Companydivisionid, ccj.Companydivision.Description,
                                                           ccj.Customerid, ccj.Customer.Description));
                }
            }
            // Assign the cache.
            //context.Session[MH_CHECKED_CCUJ] = CheckedCCUJoins;
            return CheckedCCUJoins;
        //}
    }

    // for IsAssigned Updates
    public void UpdateCCUJoins(bool isChecked, string userId, string userName, int companyDivisionId, int customerId, out string errMsg) {
        errMsg = "";
        ProfileCommon profile;
        // Fetch it.
        profile = new ProfileCommon().GetProfile(userName);
        ATGDataContext db = new ATGDataContext();
        if (isChecked) {
            // Add the join.
            ATGDB.Companycustomeruserjoin ccuj = db.Companycustomeruserjoins.SingleOrDefault(x => x.Userid == userId &&
                                                                                                  x.Companydivisionid == companyDivisionId &&
                                                                                                  x.Customerid == customerId);
            // If not there, insert it.
            if (ccuj == null) {
                // Insert it.
                ccuj = new Companycustomeruserjoin();
                ccuj.Userid = userId;
                ccuj.Companydivisionid = companyDivisionId;
                ccuj.Customerid = customerId;
                db.Companycustomeruserjoins.InsertOnSubmit(ccuj);
                db.SubmitChanges();

                ATGDB.Companydivision cd = Companydivision.GetCompanyDivisionById(companyDivisionId.ToString());
                ATGDB.Customer c = Customer.GetCustomerById(customerId.ToString());

                // If profile unassigned, assign it.
                if (profile.Companydivisionid == "") {
                    errMsg = "Division for User: " + userName + " Updated";
                    profile.Companydivisionid = cd.Companydivisionid.ToString();
                    profile.Companydivisiondescription = cd.Description;
                    profile.Save();
                }
                
                if (profile.Customerid == "") {
                    if (errMsg != "") {
                        errMsg = errMsg + " and ";
                    }
                    errMsg = errMsg + "Customer for User: " + userName + " Updated";
                    profile.Customerid = c.Customerid.ToString();
                    profile.Customerdescription = c.Description;
                    profile.Save();
                }
            }
        } else {
            // Locate and delete the join.
            ATGDB.Companycustomeruserjoin ccuj = db.Companycustomeruserjoins.SingleOrDefault(x => x.Userid == userId &&
                                                                                                  x.Companydivisionid == companyDivisionId &&
                                                                                                  x.Customerid == customerId);
            // If there, delete it.
            if (ccuj != null) {
                // Does this also happen to be the default?
                if ((profile.Companydivisionid != "") && (profile.Customerid != "") &&
                    (int.Parse(profile.Companydivisionid) == companyDivisionId) &&
                    (int.Parse(profile.Customerid) == customerId)) {
                    errMsg = "Warning! User: " + userName + " has no Default Division or Customer";
                    profile.Companydivisionid = "";
                    profile.Companydivisiondescription = "";
                    profile.Customerid = "";
                    profile.Customerdescription = "";
                    profile.Save();
                }

                // Now delete.
                db.Companycustomeruserjoins.DeleteOnSubmit(ccuj);
                db.SubmitChanges();
            }
        }
        // Finally, update the cache.
        //UpdateCCCUJCache(profile, isChecked, companyDivisionId, customerId);
    }

    // for IsAssigned and IsDefaults Cache Updates
    /*protected void UpdateCCCUJCache(ProfileCommon profile, bool isChecked, int companyDivisionId, int customerId) {
        HttpContext context = HttpContext.Current;
        List<CheckedCCUJoin> data = null;
        //if ((context.Session[MH_CHECKED_CCUJ] != null) && (context.Session[MH_CHECKED_CCUJ] != "")) {
        //    data = (List<CheckedCCUJoin>)context.Session[MH_CHECKED_CCUJ];
        //}
        if (data != null) {
            foreach (CheckedCCUJoin ccuj in data) {
                // This loop has no break in it.
                // If they match check it.
                if ((ccuj.CompanyDivisionId == companyDivisionId) &&
                    (ccuj.CustomerId == customerId)) {
                    // Set checked value.
                    ccuj.IsChecked = isChecked;
                }
                // If it matches the profile, check it as default.
                if (((profile.Companydivisionid != "") && (profile.Customerid != "")) &&
                    ((int.Parse(profile.Companydivisionid) == ccuj.CompanyDivisionId) &&
                    (int.Parse(profile.Customerid) == ccuj.CustomerId))) {
                    ccuj.IsDefault = true;
                } else {
                    ccuj.IsDefault = false;
                }
            }
        }
    }*/

    // for IsDefault Updates
    public static string INVALID_SELECTION = "INVALID SELECTION - Assign First.";
    public void UpdateProfileDefaults(bool isDefault, string userId, string userName, int companyDivisionId, int customerId, out string errMsg) {
        errMsg = "";
        ProfileCommon profile;
        // Fetch it.
        profile = new ProfileCommon().GetProfile(userName);
        ATGDataContext db = new ATGDataContext();
        if (isDefault) {
            // Find the record.
            ATGDB.Companycustomeruserjoin ccuj = db.Companycustomeruserjoins.SingleOrDefault(x => x.Userid == userId &&
                                                                                                  x.Companydivisionid == companyDivisionId &&
                                                                                                  x.Customerid == customerId);
            if (ccuj == null) {
                // Insert it and set the new defaults. This happens when the user clicks directly to the default check box on unassigned record
                ccuj = new Companycustomeruserjoin();
                ccuj.Userid = userId;
                ccuj.Companydivisionid = companyDivisionId;
                ccuj.Customerid = customerId;
                db.Companycustomeruserjoins.InsertOnSubmit(ccuj);
                db.SubmitChanges();
            }
            // Update the profile's defaults
            ATGDB.Companydivision cd = Companydivision.GetCompanyDivisionById(companyDivisionId.ToString());
            ATGDB.Customer c = Customer.GetCustomerById(customerId.ToString());

            profile.Companydivisionid = companyDivisionId.ToString();
            profile.Companydivisiondescription = cd.Description;
            profile.Customerid = customerId.ToString();
            profile.Customerdescription = c.Description;
            profile.Save();
        } else { 
            // Find the record.
            ATGDB.Companycustomeruserjoin ccuj = db.Companycustomeruserjoins.SingleOrDefault(x => x.Userid == userId &&
                                                                                                  x.Companydivisionid == companyDivisionId &&
                                                                                                  x.Customerid == customerId);
            if (ccuj != null) {
                // Does this also happen to be the default for the profile (should be)?
                if (((profile.Companydivisionid != "") && (profile.Customerid != "")) &&
                    ((int.Parse(profile.Companydivisionid) == companyDivisionId) &&
                    (int.Parse(profile.Customerid) == customerId))) {
                    errMsg = "Warning! User: " + userName + " has no Default Division or Customer";
                    profile.Companydivisionid = "";
                    profile.Companydivisiondescription = "";
                    profile.Customerid = "";
                    profile.Customerdescription = "";
                    profile.Save();
                }
            }
        }
        // Finally, update the cache.
        //if (errMsg != INVALID_SELECTION) {
        //    ATGDB.Companycustomeruserjoin ccuj = db.Companycustomeruserjoins.SingleOrDefault(x => x.Userid == userId &&
        //                                                                              x.Companydivisionid == companyDivisionId &&
        //                                                                              x.Customerid == customerId);

        //    // Update cache 1.
        //    UpdateCCCUJCache(profile, ccuj != null, companyDivisionId, customerId);
        //    // Update cache 2.
        //    UpdateCCCUJCache2(isDefault, companyDivisionId, customerId);
        //}
    }

    // for IsDefaults Cache Updates
    /*protected void UpdateCCCUJCache2(bool isDefault, int companyDivisionId, int customerId) {
        HttpContext context = HttpContext.Current;
        List<CheckedCCUJoin> data = null;
        //if ((context.Session[MH_CHECKED_CCUJ] != null) && (context.Session[MH_CHECKED_CCUJ] != "")) {
        //    data = (List<CheckedCCUJoin>)context.Session[MH_CHECKED_CCUJ];
        //}
        if (data != null) {
            foreach (CheckedCCUJoin ccuj in data) {
                // All get set default to false.
                // This loop has no break in it.
                // Makes the default checkbox in the UI a single-check/radio feature.
                ccuj.IsDefault = false;
                // It they match, and checking it true...
                if ((isDefault == true) && 
                    (ccuj.CompanyDivisionId == companyDivisionId) &&
                    (ccuj.CustomerId == customerId)) {
                    // New default.
                    ccuj.IsDefault = true;
                }
            }
        }
    }*/

    public static IEnumerable<ATGDB.Companycustomerjoin> GetCompanyCustomers() {
        ATGDataContext dc = new ATGDataContext();
        if (SecurityUtils.GetATGMode() != SecurityUtils.ATG_MODE_CUSTOMER) {
            var data = from u in dc.Companycustomerjoins
                       orderby u.Companydivision.Description, u.Customer.Description
                       select u;
            return data;
        } else {
            var data = from u in dc.Companycustomerjoins
                       where u.Customerid.Equals(SecurityUtils.GetCustId())
                       orderby u.Companydivision.Description
                       select u;
            return data;
        }
    }

    public static IEnumerable<ATGDB.Companycustomeruserjoin> GetCompanyCustomerUserJoins(string userId) {
        ATGDataContext dc = new ATGDataContext();
        var data = from u in dc.Companycustomeruserjoins
                   where u.Userid == userId
                   select u;
        return data;
    }

    public static IQueryable<ATGDB.Companydivision> GetCompanyDivisionsForUser(string userId) {
        ATGDataContext dc = new ATGDataContext();
        var data = from u in dc.Companycustomeruserjoins
                   from d in dc.Companydivisions
                   where u.Userid == userId
                   where d.Companydivisionid == u.Companydivisionid
                   select d;
        return data.Distinct().AsQueryable();
    }

    public static IQueryable<ATGDB.Customer> GetCustomersForCompanyDivisionAndUser(string userId, int companyDivisionId) {
        ATGDataContext dc = new ATGDataContext();
        var data = from u in dc.Companycustomeruserjoins
                   from c in dc.Customers
                   where u.Userid == userId
                   where u.Companydivisionid == companyDivisionId
                   where c.Customerid == u.Customerid
                   select c;
        return data.Distinct().AsQueryable();
    }

    public static IQueryable<ATGDB.Customer> GetCustomersForCompanyDivision(int companyDivisionId) {
        ATGDataContext dc = new ATGDataContext();
        var data = from ccj in dc.Companycustomerjoins
                   from c in dc.Customers
                   where ccj.Companydivisionid == companyDivisionId
                   where c.Customerid == ccj.Customerid
                   select c;
        return data.Distinct().AsQueryable();
    }

    public static void ResetPassword(HttpServerUtility server, string userName, string changeUserName, string answer) {
        MembershipUser u = ProviderUtils.GetUser(userName);
        if (u != null) {
            // Reset Password.
            string newPw = "";
            if (answer != "") {
                newPw = u.ResetPassword(answer);
            } else {
                // Get the admin provider - need different security.
                MembershipUser u2 = Membership.Providers["Admin_AspNetMySqlMembershipProvider_" + SecurityHelper.Instance.ProviderRoot].GetUser(userName, false);
                newPw = u2.ResetPassword();
            }
            // Email user with new pw.
            EmailUtils.SendPasswordChangedMail(server, userName, changeUserName, newPw);
        }
    }

    public static ATGDB.Extendeduser GetExtendedUserById(string userId) {
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        Extendeduser exu = dc.Extendedusers.SingleOrDefault(x => x.Userid == userId);
        return exu;
    }

    public static ATGDB.AspnetUser GetUserById(string userId) {
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        AspnetUser u = dc.AspnetUsers.SingleOrDefault(x => x.Userid == userId);
        return u;
    }

    public static ATGDB.Extendeduser GetExtendedUserByEmpNum(string empNum) {
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        Extendeduser exu = dc.Extendedusers.SingleOrDefault(x => x.Employeenumber == empNum);
        return exu;
    }

    public static void CreateAdministrator() {
        if (ProviderUtils.GetUser("Administrator") == null) {
            // Create the Administrator
            MembershipHelper mh = new MembershipHelper();
            string email = System.Web.Configuration.WebConfigurationManager.AppSettings["WEBADMIN"].ToString();
            mh.InsertUserEx("Administrator", "Administrator", email, "password!", false, "Administrator", "Administrator", "A" + new Random().Next(10000000, 99999999));

            // Approve the user.
            MembershipUser u = ProviderUtils.GetUser("Administrator");
            u.IsApproved = true;
            ProviderUtils.UpdateUser(u);

            // Set up the standard security question and answer.
            ProviderUtils.ChangePasswordQuestionAndAnswer(u.UserName, "password!", "What is your Mother's maiden name?", "ware");

            // Auto-add some important roles.
            SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_ADMIN, "Administrator");
            SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_CODETABLES, "Administrator");
            if (SecurityUtils.GetATGMode() == SecurityUtils.ATG_MODE_CORP) {
                SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_CORP, "Administrator");
                ProviderUtils.RemoveUsersFromRoles(new string[] { u.UserName }, new string[] { SecurityUtils.ROLE_CUSTOMER });
            } else {
                SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_CUSTOMER, "Administrator");
                ProviderUtils.RemoveUsersFromRoles(new string[] { u.UserName }, new string[] { SecurityUtils.ROLE_CORP });
            }
            SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_USERSECURITY, "Administrator");
        }
    }

    // Update every user's userId profile property. This method is
    // intended to fix bad data and isn't intended to be enabled permanently. Look at Default.cs
    public static void UpdateAllUserIds(ProfileCommon curProfile) {
        if (System.Web.Configuration.WebConfigurationManager.AppSettings["UPDATE_PROFILE_USERIDS"].ToString() == "TRUE") {
            ProfileCommon profile = new ProfileCommon();
            MembershipUserCollection users = ProviderUtils.GetAllUsers();
            foreach (MembershipUser user in users) {
                profile = profile.GetProfile(user.UserName);
                profile.Initialize(user.UserName, true);
                profile.Userid = user.ProviderUserKey.ToString();
                profile.Save();
                // Update the currently logged in profile also.
                if (profile.UserName == curProfile.UserName) {
                    curProfile.Userid = profile.Userid;
                    curProfile.Save();
                }
            }
        }
    }
}