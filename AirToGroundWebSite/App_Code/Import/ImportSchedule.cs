﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.OleDb;
using System.Web;
using Telerik.Web.UI;
using System.IO;

/// <summary>
/// Summary description for ImportSchedule
/// </summary>
public class ImportSchedule: AbstractImport 
{
	static ImportSchedule instance = new ImportSchedule();
	public static ImportSchedule getInstance() {
		if (instance == null) {
			instance = new ImportSchedule();
		}
		return instance;
	}
	public override DataSet ReadFile(RadAsyncUpload upload)
	{
		//base.ReadFile(upload);
		//upload.UploadedFiles[0].InputStream.Dispose(); // release previous file locks.
		string targetfilepath = Path.Combine(HttpContext.Current.Server.MapPath(upload.TargetFolder), upload.UploadedFiles[0].GetName());
		//upload.UploadedFiles[0].SaveAs(targetfilepath);

		string strConn = "";
	    if (upload.UploadedFiles[0].GetExtension().ToString() == ".xls") {
			strConn = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + targetfilepath + ";Extended Properties=Excel 8.0;HDR=Yes;";
		} else if (upload.UploadedFiles[0].GetExtension() == ".xlsx") {
		    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + targetfilepath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
		}

		// create your excel connection object using the connection string
		using (OleDbConnection conn = new OleDbConnection(strConn)) {
			conn.Open();

			// use a SQL Select command to retrieve the data from the Excel Spreadsheet
			// the "table name" is the name of the worksheet within the spreadsheet
			// in this case, the worksheet name is "Sheet1" and is expressed as: [Sheet1$]
			using (OleDbCommand command = new OleDbCommand("SELECT * FROM [Sheet1$]", conn)) {

				// Create a new Adapter
				using (OleDbDataAdapter adapter = new OleDbDataAdapter()) {
					// retrieve the Select command for the Spreadsheet
					adapter.SelectCommand = command;

					// Create a DataSet
					using (DataSet ds = new DataSet()) {
						// Populate the DataSet with the spreadsheet worksheet data
						adapter.Fill(ds);
						conn.Close();
						conn.Dispose();
						adapter.Dispose();

						// return the data
						return ds;
					}
				}	
			}
		}			
	}
	public override void Import(DataKey ids)
	{
		base.Import(ids);
	}
	public override void Rollback(DataKey ids) {
		base.Rollback(ids);
	}
	public override void Rollforward(DataKey ids) {
		base.Rollforward(ids);
	}
}