﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using Telerik.Web.UI;

/// <summary>
/// Summary description for ImportInterface
/// </summary>
//namespace ATG.Import { 
	public interface ImportInterface
	{
		DataSet ReadFile(RadAsyncUpload upload);
		void Import(DataKey ids);
		void Rollback(DataKey ids);
		void Rollforward(DataKey ids);
	}
//}