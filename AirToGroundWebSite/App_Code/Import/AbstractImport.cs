﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using Telerik.Web.UI;

/// <summary>
/// Summary description for AbstractImport
/// </summary>
public abstract class AbstractImport : ImportInterface
{
	public virtual DataSet ReadFile(RadAsyncUpload upload) {
		if (upload != null) {
			UploadedFile file = upload.UploadedFiles[0];
			byte[] fileData = new byte[file.InputStream.Length];
			file.InputStream.Read(fileData, 0, (int)file.InputStream.Length);
		} else {
			throw new Exception("RadAsyncUpload control is required in ImportInterface. Please contact technical support");
		}
		return null;
	}
	public virtual void Import(DataKey ids) {

	}
	public virtual void Rollback(DataKey ids) {

	}
	public virtual void Rollforward(DataKey ids) {

	}
}