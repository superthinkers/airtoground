using System;

public static class DateUtils
{
	public static int GetDaysFromToday(DateTime inDt) {
        return (DateTime.Now - inDt).Days;
    }
    public static string SubtractDates(DateTime dtLeft, DateTime dtRight) {
        TimeSpan ts = dtRight.Subtract(dtLeft);
        return ts.ToString();
    }


}
