﻿using System;
using System.Linq;

/// <summary>
/// Summary description for IAuditTrailPage
/// </summary>
public interface IAuditTrailPage
{
    string GetAuditPageName();
}