﻿using System;
using System.Linq;
using System.Web.Configuration;
using ATGDB;

/// <summary>
/// Summary description for AuditUtils
/// </summary>
public static class AuditUtils
{
    public static string PAGE_AUDIT_TRAILS = "Utility: Online Audit Trail Review";

    // Click Home Link
    public static string PAGE_HOME = "Home Page";
    
    // Service Schedules Menu
    public static string PAGE_SCHEDULE_MASTER_SCHEDULE = "Schedule: Master Schedule";
    public static string PAGE_SCHEDULE_TAIL_SCHEDULE = "Schedule: Tail Schedule";
    public static string PAGE_SCHEDULE_SERVICE_QUEUE = "Schedule: Service Queue";
    public static string PAGE_SCHEDULE_SCHEDULE_WIZ = "Schedule: Schedule Aircraft Wizard";
    public static string PAGE_SCHEDULE_RESCHEDULE_WIZ = "Schedule: Reschedule Aircraft Wizard";
    public static string PAGE_SCHEDULE_CANCEL_EX = "Schedule: Cancel Exception Dialog";
    public static string PAGE_SCHEDULE_CANCEL = "Schedule: Cancel a Service Schedule";
    public static string PAGE_SCHEDULE_EX_DETAILS = "Schedule: Exception Details Dialog";
    public static string PAGE_SCHEDULE_LOG_WORK = "Schedule: Log Work Dialog";
    public static string PAGE_SCHEDULE_CLOSE_SQITEM = "Schedule: Close Service Queue Item Dialog";
    public static string PAGE_SCHEDULE_INBOUND_ENTRY = "Schedule: Inbound Sheet Entry Dialog";
    public static string PAGE_SCHEDULE_INBOUND_SINGLE_ENTRY = "Schedule: Inbound Sheet Single Entry Dialog";
    public static string PAGE_SCHEDULE_SQ_QUICKLOG = "Schedule: Service Queue Quick Log Dialog";
    public static string PAGE_SCHEDULE_MANAGER = "Schedule: Manager";

    // WorkLogs Menu
    public static string PAGE_WORKLOG_MANAGER = "Worklog: Worklog Manager";
    public static string PAGE_ENTER_MY_TIME_WIZ = "Worklog: Enter My Time Wizard";
    public static string PAGE_WORKLOG_MODIFY_EX = "Worklog: Modify Exceptions Dialog";

    // Timesheets Menu
    public static string PAGE_TIMESHEETS = "Timesheet: Timesheet Data-entry / Search Screen";
    public static string PAGE_WORKCARD_MANAGER = "WorkCard: Work Card Manager";

    // Reports Menu
    // Click Reports Link
    public static string PAGE_REPORT_HOME = "Report: Report Home Page";

    // Aircraft
    public static string PAGE_REPORT_OPEN_EXCEPTIONS = "Report: Open Exceptions";
    public static string PAGE_REPORT_AIRCRAFT_LIST = "Report: Aircraft List";
    public static string PAGE_REPORT_AIRCRAFT_STATUS_LIST = "Report: Aircraft Status List";
    public static string PAGE_REPORT_AIRCRAFT_SERVICE_ROLLUP = "Report: Service Rollup";
    public static string PAGE_REPORT_SERVICE_PERFORMED = "Report: Service Performed";
    public static string PAGE_REPORT_RECENT_SERVICE_PERFORMED = "Report: Service Performed (Recent)";
    
    // Revenue
    public static string PAGE_REPORT_SERVICE_LOC = "Report: Service-Location";
    public static string PAGE_REPORT_WORK_REVENUE = "Report: Work Revenue Detail Summary";
    
    // Work
    public static string PAGE_REPORT_WORK_AGING = "Report: Work Aging Report";
    public static string PAGE_REPORT_DAILY_WRAP_UP = "Report: Daily Wrap-up Sheet";
    public static string PAGE_REPORT_DAILY_WRAP_UP_SUM = "Report: Daily Wrap-up Sheet Summary";
    public static string PAGE_REPORT_DAILY_WORK_SHEET = "Report: Daily Work Sheet";
    
    // Job
    public static string PAGE_REPORT_MONTH_TS_COST_BY_JOB = "Report: Monthly Timesheet Cost by Job";
    public static string PAGE_REPORT_MONTH_TS_HOURS_BY_JOB = "Report: Monthly Timesheet Hours by Job";
    public static string PAGE_REPORT_MONTH_WL_REVENUE_BY_JOB = "Report: Monthly Worklog REVENUE by Job";
    public static string PAGE_REPORT_MONTH_WL_HOURS_BY_JOB = "Report: Monthly Worklog Hours by Job";
    public static string PAGE_REPORT_WORK_CARD_DETAIL = "Report: Work Card with Detail";
    public static string PAGE_REPORT_WORK_CARD_BUDGETED_HOURS = "Report: Work Card Budgeted Hours";
    public static string PAGE_REPORT_WORK_CARD_BUDGETED_AMOUNT = "Report: Work Card Budgeted Amount";

    // Careers
    public static string PAGE_CAREERS_JOBAPP_MGR = "Careers: Job App Manager";

    // Help Menu
    public static string PAGE_HELP_INDEX_FAQ = "Help: Index/FAQ Help Section";
    public static string PAGE_HELP_SUPPORT = "Help: Website Support and Error Submission";
    public static string PAGE_HELP_RFS = "Help: Request for Service";
    public static string PAGE_HELP_CHANGE_PASSWORD = "Help: Change Password";
    public static string PAGE_HELP_CHANGE_SECURITY_QA = "Help: Change Security Q&A";
    public static string PAGE_HELP_CHANGE_DEFAULTS = "Help: Change Default Customer/Division";

    // Administration Menu
    // Click Administration Link
    public static string PAGE_ADMIN_HOME = "Admin: Administration Home Page";
    public static string PAGE_ADMIN_COMPANY_DIVISIONS = "Admin: Company Divisions";
    public static string PAGE_ADMIN_CUST_SERV_SEC = "Admin: Customers, Services, Sections";
    public static string PAGE_ADMIN_DIVISION_AREAS = "Admin: Division Areas";
    public static string PAGE_ADMIN_AIRCRAFT = "Admin: Aircraft";
    public static string PAGE_ADMIN_AIRCRAFT_HOLD = "Admin: Aircraft Hold Dialog";
    public static string PAGE_ADMIN_AIRCRAFT_STATUS = "Admin: Aircraft Status Dialog";
    public static string PAGE_ADMIN_AIRCRAFT_UNHOLD = "Admin: Aircraft UnHold Dialog";
    public static string PAGE_ADMIN_AIRCRAFT_TYPE = "Admin: Aircraft Types";
    public static string PAGE_ADMIN_SERVICES_RULES = "Admin: Services and Service Rules";
    public static string PAGE_ADMIN_SERVICE_SHORT_DESCR = "Admin: Service Short Descriptions";
    public static string PAGE_ADMIN_SERVICE_CAT = "Admin: Service Categories";
    public static string PAGE_ADMIN_SERVICE_SECT = "Admin: Service Sections";
    public static string PAGE_ADMIN_SERVICE_EX_DESCR = "Admin: Service Extended Description Dialog";
    public static string PAGE_ADMIN_SERVICE_EX_DESCR_VIEW = "Admin: Service Extended Description Dialog (View Only)";
    public static string PAGE_ADMIN_JOBS = "Admin: Job Codes";
    public static string PAGE_ADMIN_RATE_SCHEDULE = "Admin: Rate Schedule";
    public static string PAGE_ADMIN_CHANGE_RATE = "Admin: Change a Rate";
    public static string PAGE_ADMIN_USER_SECURITY = "Admin: User Security and Roles";

	// Importing
	public static string IMPORT_MANAGER = "Import Manager";
    // Intended Use:
    /*
    // Audit Trail
    AuditUtils.AuditTrail.Log(Profile.Userid, AuditUtils.PAGE_HOME);
    */
    public static _AuditTrail AuditTrail = new _AuditTrail();
    public class _AuditTrail
    {
        // If the user aborted the connection, a log could be left open.
        public void CloseOpenLogs(string userId) {
            if (!String.IsNullOrEmpty((WebConfigurationManager.AppSettings["AUDIT_TRAIL"])) &&
                (WebConfigurationManager.AppSettings["AUDIT_TRAIL"].ToUpper() == "TRUE")) {
                ATGDataContext dc = new ATGDataContext();
                var logs = from l in dc.Audittrails
                           where l.Userid == userId
                           where l.Enddatetime == null
                           select l;
                if (logs.Count() > 0) {
                    // Close them all.
                    foreach (Audittrail at in logs) {
                        at.Enddatetime = at.Startdatetime;
                    }
                    dc.SubmitChanges();
                }
            }
        }
        // Log method. Logs page interactions. Closes last open page log entry.
        public void Log(string userId, string pageName) {
            if (!String.IsNullOrEmpty((WebConfigurationManager.AppSettings["AUDIT_TRAIL"])) &&
                (WebConfigurationManager.AppSettings["AUDIT_TRAIL"].ToUpper() == "TRUE")) {
                
                // Update the last log entry for this user first.
                // The difference between this record's StartDateTime and EndDateTime is presumably
                // the amount of time they spent on the page.
                ATGDataContext dc = new ATGDataContext();
                var logs = from l in dc.Audittrails
                           where l.Userid == userId
                           where l.Enddatetime == null
                           select l;
                var needsSave = false;
                if (logs.Count() > 0) {
                    // Close them all.
                    foreach (Audittrail at in logs) {
                        // Close it.
                        needsSave = true;
                        at.Enddatetime = DateTime.Now;
                    }
                }

                // Now insert and start a new one if the page name is not empty.
                // An empty page name would indicate the user is logging out.
                if (!String.IsNullOrEmpty(pageName)) {
                    needsSave = true;
                    Audittrail log = new Audittrail();
                    log.Userid = userId;
                    log.Pagename = pageName;
                    log.Startdatetime = DateTime.Now;
                    dc.Audittrails.InsertOnSubmit(log);
                }

                // Save updates.
                if (needsSave == true) {
                    try {
                        dc.SubmitChanges();
                    } catch {
                        // unhandled.
                    }
                }
            }
        }
    }

}