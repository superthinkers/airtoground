﻿using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

/// <summary>
/// This validator looks at a radio, and if not checked,
/// will look at the RadTextBox, and if empty will require
/// it to have text in it.
/// </summary>
/// 
namespace Validators
{
    public class RadioRadTextBoxValidator : BaseValidator
    {
        public string RadioControlToValidate { get; set; }
        public string RadTextBoxToValidate { get; set; }

        public RadioRadTextBoxValidator() {
            // Don't want to use this.
            ControlToValidate = "";
        }
        // Validate the properties that are used.
        protected override bool ControlPropertiesValid() {
            Control ctrl1 = FindControl(RadioControlToValidate);
            Control ctrl2 = FindControl(RadTextBoxToValidate);
            // The "ControlToValidate" property should go unused.
            bool valid = ((ctrl1 != null) && (ctrl2 != null)) &&
                         ((ControlToValidate == "") || (ControlToValidate == null));
            return valid;
        }

        protected override bool EvaluateIsValid() {
            return this.CheckValid();
        }

        protected bool CheckValid() {
            RadioButton radio = ((RadioButton)this.FindControl(this.RadioControlToValidate));
            RadTextBox rtb = ((RadTextBox)this.FindControl(this.RadTextBoxToValidate));
            return ((radio.Checked == true) && (rtb.Text != "")) ||
                   (radio.Checked == false);
        }

    }
}