﻿using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// This validator looks at a radio, and if not checked,
/// will look at the RadTextBox, and if empty will require
/// it to have text in it.
/// </summary>
/// 
namespace Validators
{
    public class RadioTextBoxValidator : BaseValidator
    {
        public string RadioControlToValidate { get; set; }
        public string TextBoxToValidate { get; set; }
        public string TextBoxWatermarkExtender { get; set; }

        public RadioTextBoxValidator() {
            // Don't want to use this.
            ControlToValidate = "";
        }
        // Validate the properties that are used.
        protected override bool ControlPropertiesValid() {
            Control ctrl1 = FindControl(RadioControlToValidate);
            Control ctrl2 = FindControl(TextBoxToValidate);

            bool valid1 = true;
            if (TextBoxWatermarkExtender != null) {
                Control ctrl3 = FindControl(TextBoxWatermarkExtender);
                valid1 = ctrl3 != null;
            }

            // The "ControlToValidate" property should go unused.
            bool valid2 = ((ctrl1 != null) && (ctrl2 != null)) &&
                          ((ControlToValidate == "") || (ControlToValidate == null));
            return valid1 && valid2;
        }

        protected override bool EvaluateIsValid() {
            return this.CheckValid();
        }

        protected bool CheckValid() {
            RadioButton radio = ((RadioButton)this.FindControl(this.RadioControlToValidate));
            TextBox rtb = ((TextBox)this.FindControl(this.TextBoxToValidate));
            AjaxControlToolkit.TextBoxWatermarkExtender waterEx = null;
            if (this.TextBoxWatermarkExtender != null) {
                waterEx = ((AjaxControlToolkit.TextBoxWatermarkExtender)this.FindControl(this.TextBoxWatermarkExtender));
            }

            bool isV = ((radio.Checked == true) && (rtb.Text != "")) || (radio.Checked == false);

            if (waterEx != null) {
                waterEx.Enabled = !isV;
            }

            return isV;
        }

    }
}