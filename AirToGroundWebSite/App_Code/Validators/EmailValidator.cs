﻿using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

/// <summary>
/// Email Validator
/// </summary>
/// 
namespace Validators
{
    public class EmailValidator : BaseValidator
    {
        public EmailValidator() {
            // Don't want to use this.
            ControlToValidate = "";
            ToolTip = "The Email Address is Invalid";
        }
        // Validate the properties that are used.
        protected override bool ControlPropertiesValid() {
            Control ctrl = FindControl(ControlToValidate);
            return (ctrl != null);
        }

        protected override bool EvaluateIsValid() {
            return this.CheckValid();
        }

        protected bool CheckValid() {
            TextBox ctrl = ((TextBox)this.FindControl(this.ControlToValidate));
            return (Regex.IsMatch(ctrl.Text, "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*") == true);
        }

    }
}