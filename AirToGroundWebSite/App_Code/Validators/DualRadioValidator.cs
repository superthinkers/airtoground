﻿using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// What this validator does for us is take to radio buttons,
/// and ensure that at least one of them is set to true.
/// usually one of them is bound to a column, the first,
/// and one is not. This allows the user to set either
/// to true or false, and the bound one to properly 
/// reflect a true or false condition. It's acts
/// like a nullable checkbox, forcing the user to 
/// pick yes or no.
/// </summary>
/// 
namespace Validators
{
    public class DualRadioValidator : BaseValidator
    {
        public string RadioControl1ToValidate { get; set; }
        public string RadioControl2ToValidate { get; set; }

        public DualRadioValidator() {
            // Don't want to use this.
            ControlToValidate = "";
            this.IsValid = false;
        }
        // Validate the properties that are used.
        protected override bool ControlPropertiesValid() {
            Control ctrl1 = FindControl(RadioControl1ToValidate);
            Control ctrl2 = FindControl(RadioControl2ToValidate);
            // The "ControlToValidate" property should go unused.
            bool valid = ((ctrl1 != null) && (ctrl2 != null)) &&
                         ((ControlToValidate == "") || (ControlToValidate == null));
            return valid;
        }

        protected override bool EvaluateIsValid() {
            return this.CheckValid();
        }

        protected bool CheckValid() {
            RadioButton radio1 = ((RadioButton)this.FindControl(this.RadioControl1ToValidate));
            RadioButton radio2 = ((RadioButton)this.FindControl(this.RadioControl2ToValidate));
            return !((radio1.Checked == false) && (radio2.Checked == false));
        }

    }
}