﻿using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// This validator uses a single radio button, when if true,
/// will take an array of radio buttons and make sure
/// at least one of them is checked. 
/// </summary>
/// 
namespace Validators
{
    public class RadioRadioArrayValidator : BaseValidator
    {
        public string RadioControlToValidate { get; set; }
        // String of radio controls. Any delim, ,;:| is ok.
        public string RadioControlsToValidate { get; set; }

        public RadioRadioArrayValidator() {
            // Don't want to use this.
            ControlToValidate = "";
            this.IsValid = false;
        }
        // Validate the properties that are used.
        protected override bool ControlPropertiesValid() {
            Control ctrl1 = FindControl(RadioControlToValidate);
            StringTokenizer st = new StringTokenizer(RadioControlsToValidate, new char[]{',',';',':','|'});
            st.Reset();
            Control ctrl2 = null;
            for (int lcv = 0; lcv < st.Count; lcv++) {
                ctrl2 = FindControl(st.NextToken);
                if (ctrl2 == null) {
                    break;
                }
            }

            // The "ControlToValidate" property should go unused.
            bool valid = ((ctrl1 != null) && (ctrl2 != null)) &&
                         ((ControlToValidate == "") || (ControlToValidate == null));
            return valid;
        }

        protected override bool EvaluateIsValid() {
            return this.CheckValid();
        }

        protected bool CheckValid() {
            RadioButton radio1 = ((RadioButton)this.FindControl(this.RadioControlToValidate));

            StringTokenizer st = new StringTokenizer(RadioControlsToValidate, new char[] { ',', ';', ':', '|' });
            st.Reset();
            RadioButton radio2 = null;
            bool chkd = false;
            for (int lcv = 0; lcv < st.Count; lcv++) {
                radio2 = (RadioButton)FindControl(st.NextToken);
                chkd = radio2.Checked;
                if (chkd == true) {
                    break;
                }
            }

            return (radio1.Checked == false) ||
                   ((radio1.Checked == true) && (chkd == true));
        }

    }
}