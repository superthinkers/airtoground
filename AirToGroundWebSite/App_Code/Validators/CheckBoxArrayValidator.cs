﻿using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Take an array of check boxes and make sure
/// at least one of them is checked. 
/// </summary>
/// 
namespace Validators
{
    public class CheckBoxArrayValidator : BaseValidator
    {
        // String of radio controls. Any delim, ,;:| is ok.
        public string CheckBoxControlsToValidate { get; set; }

        public CheckBoxArrayValidator() {
            // Don't want to use this.
            ControlToValidate = "";
            this.IsValid = false;
        }
        // Validate the properties that are used.
        protected override bool ControlPropertiesValid() {
            StringTokenizer st = new StringTokenizer(CheckBoxControlsToValidate, new char[]{',',';',':','|'});
            st.Reset();
            Control ctrl = null;
            for (int lcv = 0; lcv < st.Count; lcv++) {
                ctrl = FindControl(st.NextToken);
                if (ctrl == null) {
                    break;
                }
            }

            // The "ControlToValidate" property should go unused.
            bool valid = (ctrl != null) &&
                         ((ControlToValidate == "") || (ControlToValidate == null));
            return valid;
        }

        protected override bool EvaluateIsValid() {
            return this.CheckValid();
        }

        protected bool CheckValid() {
            StringTokenizer st = new StringTokenizer(CheckBoxControlsToValidate, new char[] { ',', ';', ':', '|' });
            st.Reset();
            CheckBox chk = null;
            bool chkd = false;
            for (int lcv = 0; lcv < st.Count; lcv++) {
                chk = (CheckBox)FindControl(st.NextToken);
                chkd = chk.Checked;
                if (chkd == true) {
                    break;
                }
            }

            return (chkd == true);
        }

    }
}