﻿using System.Collections.Generic;
using System.Security.Cryptography;

public class AESWrapper
{
    public static string EncryptString(string plainSourceStringToEncrypt) {
        byte[] key = System.Text.ASCIIEncoding.ASCII.GetBytes("a5h7G3fI8h1w3R55O9hsWd4T");
        return AESWrapper.EncryptString(plainSourceStringToEncrypt, key);
    }

    public static string DecryptString(string base64StringToDecrypt) {
        byte[] key = System.Text.ASCIIEncoding.ASCII.GetBytes("a5h7G3fI8h1w3R55O9hsWd4T");
        return AESWrapper.DecryptString(base64StringToDecrypt, key);
    }

    ///
    /// Encrpyts the sourceString, returns this result as an AES encrpyted, BASE64 encoded string
    ///
    public static string EncryptString(string plainSourceStringToEncrypt, byte[] key) {
        // Set up the encryption objects
        AesCryptoServiceProvider acsp = AESWrapper.GetProvider(key);
        byte[] sourceBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(plainSourceStringToEncrypt.Trim());
        ICryptoTransform ictE = acsp.CreateEncryptor();

        // Set up stream to contain the encryption
        System.IO.MemoryStream msS = new System.IO.MemoryStream();

        // Perform the encrpytion, storing output into the stream
        CryptoStream csS = new CryptoStream(msS, ictE, CryptoStreamMode.Write);
        csS.Write(sourceBytes, 0, sourceBytes.Length);
        csS.FlushFinalBlock();

        // sourceBytes are now encrypted as an array of secure bytes
        byte[] encryptedBytes = msS.ToArray(); //.ToArray() is important, don't mess with the buffer

        // return the encrypted bytes as a BASE64 encoded string
        return System.Convert.ToBase64String(encryptedBytes);
    }

    ///
    /// Decrypts a BASE64 encoded string of encrypted data, returns a plain string
    ///
    /// an AES encrypted AND base64 encoded string
    /// any byte array (16 bytes long is optimal)
    /// returns a plain string
    public static string DecryptString(string base64StringToDecrypt, byte[] key) {
        // Set up the encryption objects
        AesCryptoServiceProvider acsp = AESWrapper.GetProvider(key);
        byte[] RawBytes = System.Convert.FromBase64String(base64StringToDecrypt.Trim());
        ICryptoTransform ictD = acsp.CreateDecryptor();
        // RawBytes now contains original byte array, still in Encrypted state

        // Decrypt into stream
        System.IO.MemoryStream msD = new System.IO.MemoryStream(RawBytes, 0, RawBytes.Length);
        CryptoStream csD = new CryptoStream(msD, ictD, CryptoStreamMode.Read);
        // csD now contains original byte array, fully decrypted

        // return the content of msD as a regular string
        return (new System.IO.StreamReader(csD)).ReadToEnd();
    }

    private static AesCryptoServiceProvider GetProvider(byte[] key) {
        // Set up the encryption objects
        AesCryptoServiceProvider result = new AesCryptoServiceProvider();
        byte[] RealKey = AESWrapper.GetKey(key, result);
        result.Key = RealKey;
        result.IV = RealKey;
        return result;
    }

    // Just a key handy-dandy.
    private static byte[] GetKey(byte[] suggestedKey, AesCryptoServiceProvider p) {
        byte[] kRaw = suggestedKey;
        List<byte> kList = new List<byte>();
        for (int i = 0; i < p.LegalKeySizes[0].MinSize; i += 8 ) {
            kList.Add(kRaw[i % kRaw.Length]);
        }
        byte[] k = kList.ToArray();
        return k;
    }

}

