﻿using System;

public class SecurityHelper
{
    //private static SecurityHelper instance = new SecurityHelper();
    private SecurityHelper() { }

    public static SecurityHelper Instance {
        get {
            if (System.Web.HttpContext.Current.Session["SESSION_SECURITY_HELPER"] == null) {
                System.Web.HttpContext.Current.Session["SESSION_SECURITY_HELPER"] = new SecurityHelper();
            }
            //if (instance == null) {
            //    instance = new SecurityHelper();
            //}
            return (SecurityHelper)System.Web.HttpContext.Current.Session["SESSION_SECURITY_HELPER"];// instance;
        }
    }

    private string providerRoot = null;
    public string ProviderRoot { 
        get {
            if (!String.IsNullOrEmpty(providerRoot)) {
                // We have the provider root, use it.
                return providerRoot;
            } else if (((ProfileCommon)System.Web.HttpContext.Current.Profile).IsAnonymous == false) {
                // User is logged in, do we have a provider root in the user's profile?
                providerRoot = ((ProfileCommon)System.Web.HttpContext.Current.Profile).ProviderRoot;
                if (System.String.IsNullOrEmpty(providerRoot)) {
                    // No provider root, try to get from Session.
                    providerRoot = (String)System.Web.HttpContext.Current.Session["ProviderRoot"];
                }
                if (String.IsNullOrEmpty(providerRoot)) {
                    // No provider root, try to get from Web.config
                    providerRoot = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(
                        System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath + "/web.config").AppSettings.Settings["ProviderRoot"].ToString();
                }
                // Final check
                if (System.String.IsNullOrEmpty(providerRoot)) {
                    throw new Exception("Provider Root was Empty");
                } else {
                    return providerRoot;
                }
            } else {
                // No provider set yet, user is anonymous (not logged in), get provider root from web.config
                providerRoot = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(
                    System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath + "/web.config").AppSettings.Settings["ProviderRoot"].ToString();
                return providerRoot;
            }
        } 
        set {
            // Set the static variable
            providerRoot = value;
            // Set the session variable
            System.Web.HttpContext.Current.Session.Add("ProviderRoot", value);
            // Does the profile need updating? User must be logged in.
            //if ((((ProfileCommon)System.Web.HttpContext.Current.Profile).IsAnonymous == false) && 
            //    (((ProfileCommon)System.Web.HttpContext.Current.Profile).ProviderRoot != providerRoot)) {
            //    // Set on the user's profile, it needs updating.
            //    ((ProfileCommon)System.Web.HttpContext.Current.Profile).ProviderRoot = providerRoot;
            //    System.Web.HttpContext.Current.Profile.Save();
            //}
        } 
    }
}
