using System;
using Telerik.Reporting;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for WorkCardWithDetail.
/// </summary>
public class WorkCardWithDetail : Report
{
	private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
	private Telerik.Reporting.DetailSection detail;
	private Telerik.Reporting.PageFooterSection pageFooterSection1;

    private int Customerid = 0;
    private string Customername = "";
    private int Companyid = 0;
    private string Companyname = "";
    private DateTime FromDt = DateTime.Now.Date;
    private DateTime ToDt = DateTime.Now.AddDays(15).Date;
    private TextBox txtFooter;
    private TextBox textBox6;
    private TextBox textBox2;
    private TextBox textBox8;
    private TextBox textBox9;
    private TextBox textBox10;
    private TextBox textBox11;
    private TextBox txtReportDate;
    private TextBox textBox12;
    private TextBox txtUserName;
    private TextBox txtCustName;
    private TextBox textBox18;
    private ReportFooterSection reportFooterSection1;
    private TextBox txtTo;
    private TextBox textBox19;
    private TextBox txtFrom;
    private Group group1;
    private GroupFooterSection groupFooterSection1;
    private GroupHeaderSection groupHeaderSection1;
    private TextBox textBox14;
    private TextBox textBox23;
    private TextBox textBox24;
    private TextBox textBox25;
    private TextBox textBox26;
    private TextBox textBox27;
    private TextBox textBox28;
    private TextBox textBox29;
    private TextBox textBox31;
    private TextBox textBox32;
    private TextBox textBox33;
    private TextBox textBox34;
    private TextBox textBox1;
    private TextBox textBox3;
    private CheckBox checkBox1;
    private TextBox textBox4;
    string UserName = "";

    public WorkCardWithDetail(int custId, string custName, int compId, string compName, DateTime frmDt, DateTime toDt, string userNm) {
        Customerid = custId;
        Customername = custName;
        Companyid = compId;
        Companyname = compName;
        FromDt = frmDt;
        ToDt = toDt;
        UserName = userNm;
        
        // Create.
        InitializeComponent();

        // Some vals.
        txtCustName.Value = Companyname + " / " + Customername;
        txtTo.Value = ToDt.Date.ToShortDateString();
        txtFrom.Value = FromDt.Date.ToShortDateString();
        txtReportDate.Value = DateTime.Now.Date.ToLongDateString();
        txtUserName.Value = UserName;
        txtFooter.Value = Resources.Resource.ReportFooterText;

        // Load the data.
        LoadData();
    }

    class MyReportData
    {
        public int Workcardid { get; set; }
        public string Cardnumber { get; set; }
        public string Description { get; set; }
        public double Hourlyrate { get; set; }
        public bool Completed { get; set; }
        public string Employeenumber { get; set; }
        public string Fullname { get; set; }
        public DateTime Workeddate { get; set; }
        public DateTime Starttime { get; set; }
        public DateTime Endtime { get; set; }
    }
    protected void LoadData() {
        ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();

        List<MyReportData> data = (from ts in db.Timesheets
                                   from wc in db.Workcards
                                   from u in db.Extendedusers
                                   where Customerid > 0 ? ts.Customerid == Customerid : true
                                   //where Companyid > 0 ? ts.Companydivisionid == Companyid : true
                                   where ts.Workeddate.Date >= FromDt.Date
                                   where ts.Workeddate.Date <= ToDt.Date
                                   where wc.Workcardid == ts.Workcardid
                                   where u.Userid == ts.Userid
                                   orderby ts.Workeddate, u.Lastname
                                   select new MyReportData() { 
                                        Workcardid = wc.Workcardid,
                                        Cardnumber = wc.Cardnumber,
                                        Description = wc.Description,
                                        Hourlyrate = wc.Hourlyrate,
                                        Completed = wc.Completed,
                                        Employeenumber = u.Employeenumber,
                                        Fullname = u.Firstname + " " + u.Lastname, 
                                        Workeddate = ts.Workeddate,
                                        Starttime = ts.Starttime,
                                        Endtime = ts.Endtime
                                   }).ToList<MyReportData>();

        // Set the datasource.
        Report.DataSource = data;
    }

    protected WorkCardWithDetail()
	{
		//
		// Required for telerik Reporting designer support
		//
		InitializeComponent();

		//
		// TODO: Add any constructor code after InitializeComponent call
		//
	}

	#region Component Designer generated code
	/// <summary>
	/// Required method for telerik Reporting designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent()
	{
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.txtReportDate = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.txtUserName = new Telerik.Reporting.TextBox();
            this.txtCustName = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.txtTo = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.txtFrom = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.txtFooter = new Telerik.Reporting.TextBox();
            this.reportFooterSection1 = new Telerik.Reporting.ReportFooterSection();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.group1 = new Telerik.Reporting.Group();
            this.groupFooterSection1 = new Telerik.Reporting.GroupFooterSection();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.groupHeaderSection1 = new Telerik.Reporting.GroupHeaderSection();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.checkBox1 = new Telerik.Reporting.CheckBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox6,
            this.textBox2,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.txtReportDate,
            this.textBox12,
            this.txtUserName,
            this.txtCustName,
            this.textBox18,
            this.txtTo,
            this.textBox19,
            this.txtFrom});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.pageHeaderSection1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pageHeaderSection1.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.19996054470539093D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Arial Unicode MS";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox6.Style.Font.Strikeout = false;
            this.textBox6.Value = "Air to Ground Services, Inc.";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Arial Unicode MS";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox2.Style.Font.Strikeout = false;
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Value = "Work Card Report";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.5997638702392578D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.299999862909317D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox8.Value = "= PageNumber";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.299999862909317D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox9.Value = "= PageCount";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1996855735778809D), Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox10.Value = "Page";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.8998427391052246D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.20007896423339844D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox11.Value = "of";
            // 
            // txtReportDate
            // 
            this.txtReportDate.Format = "{0:g}";
            this.txtReportDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.70007866621017456D));
            this.txtReportDate.Name = "txtReportDate";
            this.txtReportDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7999998331069946D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.txtReportDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtReportDate.Value = "textBox12";
            // 
            // textBox12
            // 
            this.textBox12.CanGrow = false;
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.700078547000885D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.29999956488609314D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox12.Style.Color = System.Drawing.Color.Gray;
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox12.Value = "By:";
            // 
            // txtUserName
            // 
            this.txtUserName.CanGrow = false;
            this.txtUserName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.9000792503356934D), Telerik.Reporting.Drawing.Unit.Inch(0.70007866621017456D));
            this.txtUserName.Multiline = false;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5998420715332031D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.txtUserName.Style.Color = System.Drawing.Color.Gray;
            this.txtUserName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtUserName.TextWrap = false;
            this.txtUserName.Value = "textBox13";
            // 
            // txtCustName
            // 
            this.txtCustName.CanGrow = true;
            this.txtCustName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.30841159820556641D), Telerik.Reporting.Drawing.Unit.Inch(0.55240887403488159D));
            this.txtCustName.Multiline = false;
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5915882587432861D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.txtCustName.Style.Color = System.Drawing.Color.Black;
            this.txtCustName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtCustName.TextWrap = false;
            this.txtCustName.Value = "textBox13";
            // 
            // textBox18
            // 
            this.textBox18.CanGrow = false;
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.552408754825592D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.29999956488609314D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox18.Style.Color = System.Drawing.Color.Black;
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox18.Value = "For:";
            // 
            // txtTo
            // 
            this.txtTo.CanGrow = false;
            this.txtTo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1000787019729614D), Telerik.Reporting.Drawing.Unit.Inch(0.75248748064041138D));
            this.txtTo.Multiline = false;
            this.txtTo.Name = "txtTo";
            this.txtTo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.txtTo.Style.Color = System.Drawing.Color.Black;
            this.txtTo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtTo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtTo.TextWrap = false;
            this.txtTo.Value = "textBox13";
            // 
            // textBox19
            // 
            this.textBox19.CanGrow = false;
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.800078809261322D), Telerik.Reporting.Drawing.Unit.Inch(0.75248759984970093D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.29992112517356873D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox19.Style.Color = System.Drawing.Color.Black;
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox19.Value = "To";
            // 
            // txtFrom
            // 
            this.txtFrom.CanGrow = false;
            this.txtFrom.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(0.75248748064041138D));
            this.txtFrom.Multiline = false;
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.txtFrom.Style.Color = System.Drawing.Color.Black;
            this.txtFrom.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtFrom.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtFrom.TextWrap = false;
            this.txtFrom.Value = "textBox13";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20011790096759796D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34});
            this.detail.Name = "detail";
            this.detail.PageBreak = Telerik.Reporting.PageBreak.None;
            // 
            // textBox31
            // 
            this.textBox31.CanGrow = false;
            this.textBox31.Format = "";
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1000787019729614D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1229177713394165D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox31.StyleName = "";
            this.textBox31.Value = "=Fields.Employeenumber";
            // 
            // textBox32
            // 
            this.textBox32.CanGrow = false;
            this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.4229171276092529D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox32.StyleName = "";
            this.textBox32.Value = "=Fields.Fullname";
            // 
            // textBox33
            // 
            this.textBox33.CanGrow = false;
            this.textBox33.Format = "{0:d}";
            this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.027161471545696259D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.94984179735183716D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox33.StyleName = "";
            this.textBox33.Value = "=Fields.Workeddate";
            // 
            // textBox34
            // 
            this.textBox34.CanGrow = false;
            this.textBox34.Format = "{0:C2}";
            this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7247999906539917D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox34.StyleName = "";
            this.textBox34.Value = "=Fields.Hourlyrate";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtFooter});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.pageFooterSection1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            // 
            // txtFooter
            // 
            this.txtFooter.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.txtFooter.Name = "txtFooter";
            this.txtFooter.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D));
            this.txtFooter.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.txtFooter.Style.Color = System.Drawing.Color.Gray;
            this.txtFooter.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtFooter.Style.Font.Strikeout = false;
            this.txtFooter.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtFooter.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // reportFooterSection1
            // 
            this.reportFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20007896423339844D);
            this.reportFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox24,
            this.textBox25});
            this.reportFooterSection1.Name = "reportFooterSection1";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3583312034606934D), Telerik.Reporting.Drawing.Unit.Inch(7.8837074397597462E-05D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0416688919067383D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox24.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.Value = "Grand Total";
            // 
            // textBox25
            // 
            this.textBox25.CanGrow = false;
            this.textBox25.Format = "{0:C2}";
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4663858413696289D), Telerik.Reporting.Drawing.Unit.Inch(7.8837074397597462E-05D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.058414101600647D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox25.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.Value = "=Sum(Fields.Hourlyrate)";
            // 
            // group1
            // 
            this.group1.GroupFooter = this.groupFooterSection1;
            this.group1.GroupHeader = this.groupHeaderSection1;
            this.group1.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping("=Fields.Workcardid")});
            this.group1.Name = "group1";
            // 
            // groupFooterSection1
            // 
            this.groupFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20003969967365265D);
            this.groupFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox14,
            this.textBox23});
            this.groupFooterSection1.Name = "groupFooterSection1";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(3.9736431062920019E-05D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7000001072883606D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.Value = "Sub Total";
            // 
            // textBox23
            // 
            this.textBox23.CanGrow = false;
            this.textBox23.Format = "{0:C2}";
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4663858413696289D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.058414101600647D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.Value = "=Sum(Fields.Hourlyrate)";
            // 
            // groupHeaderSection1
            // 
            this.groupHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.40003946423530579D);
            this.groupHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox1,
            this.textBox3,
            this.checkBox1,
            this.textBox4});
            this.groupHeaderSection1.Name = "groupHeaderSection1";
            // 
            // textBox26
            // 
            this.textBox26.CanGrow = false;
            this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1000787019729614D), Telerik.Reporting.Drawing.Unit.Inch(0.19999994337558746D));
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1229177713394165D), Telerik.Reporting.Drawing.Unit.Inch(0.19988124072551727D));
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "Emp Number";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.54367160797119141D), Telerik.Reporting.Drawing.Unit.Inch(0.19988124072551727D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox27.StyleName = "";
            this.textBox27.Value = "Name";
            // 
            // textBox28
            // 
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.027161534875631332D), Telerik.Reporting.Drawing.Unit.Inch(0.20011876523494721D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.94984167814254761D), Telerik.Reporting.Drawing.Unit.Inch(0.19988124072551727D));
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox28.StyleName = "";
            this.textBox28.Value = "Work Date";
            // 
            // textBox29
            // 
            this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7247999906539917D), Telerik.Reporting.Drawing.Unit.Inch(0.19988124072551727D));
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "Rate";
            // 
            // textBox1
            // 
            this.textBox1.CanGrow = false;
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.97708219289779663D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7228387594223023D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.StyleName = "";
            this.textBox1.Value = "=Fields.Cardnumber";
            // 
            // textBox3
            // 
            this.textBox3.CanGrow = false;
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0229182243347168D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox3.StyleName = "";
            this.textBox3.Value = "=Fields.Description";
            // 
            // checkBox1
            // 
            this.checkBox1.CanGrow = false;
            this.checkBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.7229962348937988D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0769648551940918D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.checkBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox1.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox1.Text = "Completed";
            this.checkBox1.Value = "=Fields.Completed";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.97696393728256226D), Telerik.Reporting.Drawing.Unit.Inch(0.19988124072551727D));
            this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.StyleName = "";
            this.textBox4.Value = "Work Card:";
            // 
            // WorkCardWithDetail
            // 
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.group1});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.groupHeaderSection1,
            this.groupFooterSection1,
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1,
            this.reportFooterSection1});
            this.Name = "MasterSchedule";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}
	#endregion

}