using System;
using Telerik.Reporting;
using System.Linq;

/// <summary>
/// Summary description for TimeSheetCostDD.
/// </summary>
public class TimeSheetCostDDLvl1 : Report
{
	private Telerik.Reporting.DetailSection detail;
	private Telerik.Reporting.PageFooterSection pageFooterSection1;

    private int Customerid = 0;
    private int Companydivisionid = 0;
    private string Customername = "";
    private DateTime dtFrom;
    private DateTime dtTo;
    private TextBox txtFooter;
    private PageHeaderSection pageHeaderSection1;
    private TextBox textBox6;
    private TextBox textBox2;
    private TextBox textBox8;
    private TextBox textBox9;
    private TextBox textBox10;
    private TextBox textBox11;
    private TextBox txtReportDate;
    private TextBox textBox12;
    private TextBox txtUserName;
    private TextBox txtCustName;
    private TextBox textBox18;
    private TextBox txtTo;
    private TextBox textBox19;
    private TextBox txtFrom;
    private Crosstab crosstab1;
    private TextBox txtValue;
    private TextBox txtMonth;
    private TextBox textBox1;
    private TextBox txtCompanyDescr;
    private TextBox textBox13;
    private TextBox txtJobCode;
    private TextBox txtTotalRate;
    private TextBox textBox3;
    private TextBox textBox7;
    private TextBox textBox14;
    private TextBox textBox5;
    private TextBox textBox4;
    private TextBox textBox15;
    private TextBox textBox16;
    private TextBox textBox17;
    private TextBox textBox21;
    private TextBox textBox22;
    private TextBox textBox20;
    private TextBox textBox24;
    private TextBox textBox25;
    private TextBox textBox23;
    string UserName = "";

    class MyReportData
    {
        public int Companydivisionid { get; set; }
        public int Customerid { get; set; }
        public DateTime Workeddate { get; set; }
        public float Hourlyrate { get; set; }
        public double Cost { get; set; }
        public double Rate { get; set; }
        public string Jobcode { get; set; }
        public string Companydescr { get; set; }
    }
    protected void LoadData() {
        ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
        //ProfileCommon profile = (ProfileCommon)ProfileCommon.Create(UserName);

        var data = from ts in db.Timesheets
                   where ((ts.Workeddate >= dtFrom.Date) && (ts.Workeddate <= dtTo.Date))
                   where ts.Customerid == Customerid
                   //where ts.Rate > 0.00
                   //where ts.Companydivisionid == Companydivisionid
                   orderby ts.Workeddate ascending
                   select new MyReportData() {
                       Companydivisionid = ts.Companydivisionid,
                       Customerid = ts.Customerid,
                       Workeddate = ts.Workeddate,
                       Hourlyrate = ts.Hourlyrate,
                       Cost = ts.Cost,
                       Rate = ts.Rate,
                       Jobcode = ts.Jobcode.Code,
                       Companydescr = ts.Companydivision.Description
                   };

        //Report.DataSource = data;
        crosstab1.DataSource = data;
    }
    public static string GetFormattedMonthString(int month, int year) {
        switch (month) {
            case 1: return "Jan " + year.ToString();
            case 2: return "Feb " + year.ToString();
            case 3: return "Mar " + year.ToString();
            case 4: return "Apr " + year.ToString();
            case 5: return "May " + year.ToString();
            case 6: return "June " + year.ToString();
            case 7: return "July " + year.ToString();
            case 8: return "Aug " + year.ToString();
            case 9: return "Sept " + year.ToString();
            case 10: return "Oct " + year.ToString();
            case 11: return "Nov " + year.ToString();
            case 12: return "Dec " + year.ToString();
            default: return "";
        }
    }
	public TimeSheetCostDDLvl1()
	{
		//
		// Required for telerik Reporting designer support
		//
        InitializeComponent();
    }

	#region Component Designer generated code
	/// <summary>
	/// Required method for telerik Reporting designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent()
	{
            Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.NavigateToReportAction navigateToReportAction1 = new Telerik.Reporting.NavigateToReportAction();
            Telerik.Reporting.NavigateToReportAction navigateToReportAction2 = new Telerik.Reporting.NavigateToReportAction();
            Telerik.Reporting.NavigateToReportAction navigateToReportAction3 = new Telerik.Reporting.NavigateToReportAction();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter4 = new Telerik.Reporting.ReportParameter();
            this.txtMonth = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.txtJobCode = new Telerik.Reporting.TextBox();
            this.txtCompanyDescr = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.crosstab1 = new Telerik.Reporting.Crosstab();
            this.txtValue = new Telerik.Reporting.TextBox();
            this.txtTotalRate = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.txtFooter = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.txtReportDate = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.txtUserName = new Telerik.Reporting.TextBox();
            this.txtCustName = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.txtTo = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.txtFrom = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // txtMonth
            // 
            this.txtMonth.Angle = -90D;
            this.txtMonth.Format = "{0}";
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.56666648387908936D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.79999983310699463D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtMonth.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMonth.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtMonth.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtMonth.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMonth.Style.Font.Bold = true;
            this.txtMonth.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(9D, Telerik.Reporting.Drawing.UnitType.Point);
            this.txtMonth.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtMonth.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtMonth.Value = "=GetFormattedMonthString(Fields.Workeddate.Month, Fields.Workeddate.Year)";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.53333336114883423D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.79999983310699463D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox5.Style.BackgroundColor = System.Drawing.Color.Khaki;
            this.textBox5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.Color = System.Drawing.Color.Navy;
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox5.StyleName = "";
            this.textBox5.Value = "Total";
            // 
            // txtJobCode
            // 
            this.txtJobCode.Name = "txtJobCode";
            this.txtJobCode.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1333333253860474D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.68500024080276489D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtJobCode.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.txtJobCode.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.txtJobCode.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtJobCode.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtJobCode.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtJobCode.Style.Color = System.Drawing.Color.Black;
            this.txtJobCode.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtJobCode.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtJobCode.StyleName = "";
            this.txtJobCode.Value = "=Fields.Jobcode";
            // 
            // txtCompanyDescr
            // 
            this.txtCompanyDescr.BookmarkId = "";
            formattingRule1.Filters.AddRange(new Telerik.Reporting.Filter[] {
            new Telerik.Reporting.Filter("= RowNumber() Mod 2", Telerik.Reporting.FilterOperator.Equal, "0")});
            formattingRule1.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(166)))));
            this.txtCompanyDescr.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
            this.txtCompanyDescr.Format = "{0}";
            this.txtCompanyDescr.Name = "txtCompanyDescr";
            this.txtCompanyDescr.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.2333335876464844D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.68500024080276489D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtCompanyDescr.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.txtCompanyDescr.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.txtCompanyDescr.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtCompanyDescr.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtCompanyDescr.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtCompanyDescr.Style.Color = System.Drawing.Color.Black;
            this.txtCompanyDescr.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCompanyDescr.Style.Visible = true;
            this.txtCompanyDescr.Value = "=Fields.Companydescr";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.3666670322418213D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23333333432674408D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox3.Style.BackgroundColor = System.Drawing.Color.Khaki;
            this.textBox3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox3.Style.Color = System.Drawing.Color.Navy;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.StyleName = "";
            this.textBox3.Value = "Total Hourly Rates";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.3666670322418213D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23000000417232513D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox23.Style.BackgroundColor = System.Drawing.Color.Khaki;
            this.textBox23.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox23.Style.Color = System.Drawing.Color.Navy;
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "Total Std Rates";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.3666670322418213D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23000000417232513D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox20.Style.BackgroundColor = System.Drawing.Color.Khaki;
            this.textBox20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox20.Style.Color = System.Drawing.Color.Navy;
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "Total Cost";
            // 
            // detail
            // 
            this.detail.Height = new Telerik.Reporting.Drawing.Unit(3.0183334350585938D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.crosstab1});
            this.detail.Name = "detail";
            this.detail.PageBreak = Telerik.Reporting.PageBreak.None;
            // 
            // crosstab1
            // 
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.56666642427444458D, Telerik.Reporting.Drawing.UnitType.Inch)));
            this.crosstab1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.53333336114883423D, Telerik.Reporting.Drawing.UnitType.Inch)));
            this.crosstab1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.225000262260437D, Telerik.Reporting.Drawing.UnitType.Inch)));
            this.crosstab1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.23000001907348633D, Telerik.Reporting.Drawing.UnitType.Inch)));
            this.crosstab1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.23000001907348633D, Telerik.Reporting.Drawing.UnitType.Inch)));
            this.crosstab1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.23333345353603363D, Telerik.Reporting.Drawing.UnitType.Inch)));
            this.crosstab1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.23000000417232513D, Telerik.Reporting.Drawing.UnitType.Inch)));
            this.crosstab1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.23000000417232513D, Telerik.Reporting.Drawing.UnitType.Inch)));
            this.crosstab1.Body.SetCellContent(0, 0, this.txtValue);
            this.crosstab1.Body.SetCellContent(3, 0, this.txtTotalRate);
            this.crosstab1.Body.SetCellContent(0, 1, this.textBox7);
            this.crosstab1.Body.SetCellContent(3, 1, this.textBox14);
            this.crosstab1.Body.SetCellContent(1, 0, this.textBox4);
            this.crosstab1.Body.SetCellContent(1, 1, this.textBox15);
            this.crosstab1.Body.SetCellContent(2, 0, this.textBox16);
            this.crosstab1.Body.SetCellContent(2, 1, this.textBox17);
            this.crosstab1.Body.SetCellContent(5, 0, this.textBox21);
            this.crosstab1.Body.SetCellContent(5, 1, this.textBox22);
            this.crosstab1.Body.SetCellContent(4, 0, this.textBox24);
            this.crosstab1.Body.SetCellContent(4, 1, this.textBox25);
            tableGroup1.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping("=Fields.Workeddate.Month")});
            tableGroup1.Name = "Month";
            tableGroup1.ReportItem = this.txtMonth;
            tableGroup2.Name = "Group2";
            tableGroup2.ReportItem = this.textBox5;
            this.crosstab1.ColumnGroups.Add(tableGroup1);
            this.crosstab1.ColumnGroups.Add(tableGroup2);
            this.crosstab1.Corner.SetCellContent(0, 0, this.textBox1);
            this.crosstab1.Corner.SetCellContent(0, 1, this.textBox13);
            this.crosstab1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtValue,
            this.txtTotalRate,
            this.textBox7,
            this.textBox14,
            this.textBox4,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox21,
            this.textBox22,
            this.textBox24,
            this.textBox25,
            this.txtMonth,
            this.textBox5,
            this.textBox1,
            this.textBox13,
            this.txtCompanyDescr,
            this.txtJobCode,
            this.textBox3,
            this.textBox20,
            this.textBox23});
            this.crosstab1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.1000000610947609D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.crosstab1.Name = "crosstab1";
            tableGroup5.Name = "Group3";
            tableGroup6.Name = "Group4";
            tableGroup7.Name = "Group5";
            tableGroup4.ChildGroups.Add(tableGroup5);
            tableGroup4.ChildGroups.Add(tableGroup6);
            tableGroup4.ChildGroups.Add(tableGroup7);
            tableGroup4.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping("=Fields.Jobcode")});
            tableGroup4.Name = "Jobcode1";
            tableGroup4.ReportItem = this.txtJobCode;
            tableGroup4.Sortings.AddRange(new Telerik.Reporting.Sorting[] {
            new Telerik.Reporting.Sorting("=Fields.Jobcode", Telerik.Reporting.SortDirection.Asc)});
            tableGroup3.ChildGroups.Add(tableGroup4);
            tableGroup3.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping("=Fields.Companydescr")});
            tableGroup3.Name = "Company";
            tableGroup3.ReportItem = this.txtCompanyDescr;
            tableGroup8.Name = "Group1";
            tableGroup8.ReportItem = this.textBox3;
            tableGroup9.Name = "Group7";
            tableGroup9.ReportItem = this.textBox23;
            tableGroup10.Name = "Group6";
            tableGroup10.ReportItem = this.textBox20;
            this.crosstab1.RowGroups.Add(tableGroup3);
            this.crosstab1.RowGroups.Add(tableGroup8);
            this.crosstab1.RowGroups.Add(tableGroup9);
            this.crosstab1.RowGroups.Add(tableGroup10);
            this.crosstab1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.4666666984558105D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(2.1783337593078613D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.crosstab1.ItemDataBound += new System.EventHandler(this.crosstab1_ItemDataBound);
            // 
            // txtValue
            // 
            navigateToReportAction1.Parameters.Add(new Telerik.Reporting.Parameter("companyId", "=Fields.Companydivisionid"));
            navigateToReportAction1.Parameters.Add(new Telerik.Reporting.Parameter("customerId", "=Fields.Customerid"));
            navigateToReportAction1.Parameters.Add(new Telerik.Reporting.Parameter("workedDate", "=Fields.Workeddate"));
            navigateToReportAction1.ReportDocumentType = "TimeSheetCostDDLvl2, App_Code.7tje_sqi, Version=0.0.0.0, Culture=neutral, PublicK" +
    "eyToken=null";
            this.txtValue.Action = navigateToReportAction1;
            this.txtValue.Format = "{0:C0}";
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.56666648387908936D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.22500008344650269D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtValue.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.txtValue.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtValue.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtValue.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtValue.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtValue.Style.Color = System.Drawing.Color.Navy;
            this.txtValue.Style.Font.Bold = false;
            this.txtValue.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtValue.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtValue.Value = "= IIf(Sum(Fields.Hourlyrate) <= 0, \"\", Sum(Fields.Hourlyrate))";
            // 
            // txtTotalRate
            // 
            this.txtTotalRate.CanGrow = true;
            this.txtTotalRate.Format = "{0:C0}";
            this.txtTotalRate.Name = "txtTotalRate";
            this.txtTotalRate.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.56666648387908936D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23333333432674408D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtTotalRate.Style.BackgroundColor = System.Drawing.Color.Khaki;
            this.txtTotalRate.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtTotalRate.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtTotalRate.Style.Color = System.Drawing.Color.Navy;
            this.txtTotalRate.Style.Font.Bold = true;
            this.txtTotalRate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtTotalRate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtTotalRate.StyleName = "";
            this.txtTotalRate.Value = "= IIf(Sum(Fields.Hourlyrate) <= 0, \"\", Sum(Fields.Hourlyrate))";
            // 
            // textBox7
            // 
            this.textBox7.Format = "{0:C0}";
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.53333336114883423D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.22500008344650269D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox7.Style.BackgroundColor = System.Drawing.Color.Khaki;
            this.textBox7.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.Color = System.Drawing.Color.Navy;
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.StyleName = "";
            this.textBox7.Value = "= IIf(Sum(Fields.Hourlyrate) <= 0, \"\", Sum(Fields.Hourlyrate))";
            // 
            // textBox14
            // 
            this.textBox14.Format = "{0:C0}";
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.53333336114883423D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23333333432674408D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox14.Style.BackgroundColor = System.Drawing.Color.Tan;
            this.textBox14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.Color = System.Drawing.Color.Green;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "= IIf(Sum(Fields.Hourlyrate) <= 0, \"\", Sum(Fields.Hourlyrate))";
            // 
            // textBox4
            // 
            navigateToReportAction2.Parameters.Add(new Telerik.Reporting.Parameter("companyId", "=Fields.Companydivisionid"));
            navigateToReportAction2.Parameters.Add(new Telerik.Reporting.Parameter("customerId", "=Fields.Customerid"));
            navigateToReportAction2.Parameters.Add(new Telerik.Reporting.Parameter("workedDate", "=Fields.Workeddate"));
            navigateToReportAction2.ReportDocumentType = "TimeSheetCostDDLvl2, App_Code.7tje_sqi, Version=0.0.0.0, Culture=neutral, PublicK" +
    "eyToken=null";
            this.textBox4.Action = navigateToReportAction2;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.56666642427444458D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23000000417232513D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox4.Style.Color = System.Drawing.Color.Navy;
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.StyleName = "";
            this.textBox4.Value = "= IIf(Sum(Fields.Rate) <= 0, \"\", Sum(Fields.Rate))";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.53333336114883423D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23000000417232513D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox15.Style.BackgroundColor = System.Drawing.Color.Khaki;
            this.textBox15.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.Color = System.Drawing.Color.Navy;
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.StyleName = "";
            this.textBox15.Value = "= IIf(Sum(Fields.Rate) <= 0, \"\", Sum(Fields.Rate))";
            // 
            // textBox16
            // 
            navigateToReportAction3.Parameters.Add(new Telerik.Reporting.Parameter("companyId", "=Fields.Companydivisionid"));
            navigateToReportAction3.Parameters.Add(new Telerik.Reporting.Parameter("customerId", "=Fields.Customerid"));
            navigateToReportAction3.Parameters.Add(new Telerik.Reporting.Parameter("workedDate", "=Fields.Workeddate"));
            navigateToReportAction3.ReportDocumentType = "TimeSheetCostDDLvl2, App_Code.7tje_sqi, Version=0.0.0.0, Culture=neutral, PublicK" +
    "eyToken=null";
            this.textBox16.Action = navigateToReportAction3;
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.56666642427444458D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23000000417232513D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.Style.Color = System.Drawing.Color.Navy;
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "= IIf(Sum(Fields.Cost) <= 0, \"\", Sum(Fields.Cost))";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.53333336114883423D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23000000417232513D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox17.Style.BackgroundColor = System.Drawing.Color.Khaki;
            this.textBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.Color = System.Drawing.Color.Navy;
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "= IIf(Sum(Fields.Cost) <= 0, \"\", Sum(Fields.Cost))";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.56666642427444458D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23000000417232513D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox21.Style.BackgroundColor = System.Drawing.Color.Khaki;
            this.textBox21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox21.Style.Color = System.Drawing.Color.Navy;
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.StyleName = "";
            this.textBox21.Value = "= IIf(Sum(Fields.Cost) <= 0, \"\", Sum(Fields.Cost))";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.53333336114883423D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23000000417232513D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox22.Style.BackgroundColor = System.Drawing.Color.Tan;
            this.textBox22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.Color = System.Drawing.Color.Green;
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.StyleName = "";
            this.textBox22.Value = "= IIf(Sum(Fields.Cost) <= 0, \"\", Sum(Fields.Cost))";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.56666642427444458D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23000000417232513D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox24.Style.BackgroundColor = System.Drawing.Color.Khaki;
            this.textBox24.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox24.Style.Color = System.Drawing.Color.Navy;
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "= IIf(Sum(Fields.Rate) <= 0, \"\", Sum(Fields.Rate))";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.53333336114883423D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23000000417232513D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox25.Style.BackgroundColor = System.Drawing.Color.Tan;
            this.textBox25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.Color = System.Drawing.Color.Green;
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.StyleName = "";
            this.textBox25.Value = "= IIf(Sum(Fields.Rate) <= 0, \"\", Sum(Fields.Rate))";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.2333335876464844D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.79999983310699463D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox1.Value = "Company";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1333333253860474D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.79999983310699463D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "\r\n[1. Hourly Rate]\r\n[2. Std Rate      ]\r\n[3. Cost            ]\r\nJob Code";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.60000008344650269D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtFooter});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Pixel);
            this.pageFooterSection1.Style.Padding.Right = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Pixel);
            // 
            // txtFooter
            // 
            this.txtFooter.Docking = Telerik.Reporting.DockingStyle.Fill;
            this.txtFooter.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtFooter.Name = "txtFooter";
            this.txtFooter.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.60000008344650269D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtFooter.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.txtFooter.Style.Color = System.Drawing.Color.Gray;
            this.txtFooter.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(7D, Telerik.Reporting.Drawing.UnitType.Point);
            this.txtFooter.Style.Font.Strikeout = false;
            this.txtFooter.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtFooter.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox6,
            this.textBox2,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.txtReportDate,
            this.textBox12,
            this.txtUserName,
            this.txtCustName,
            this.textBox18,
            this.txtTo,
            this.textBox19,
            this.txtFrom});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.1000000610947609D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999986588954926D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.2000000476837158D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19996054470539093D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Arial Unicode MS";
            this.textBox6.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
            this.textBox6.Style.Font.Strikeout = false;
            this.textBox6.Value = "Air to Ground Services, Inc.";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.2333333492279053D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.7999999523162842D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Arial Unicode MS";
            this.textBox2.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            this.textBox2.Style.Font.Strikeout = false;
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Value = "Time Sheet Cost By Job Code - Year View";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(9.1000003814697266D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(7.9472862068996619E-08D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.299999862909317D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox8.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            this.textBox8.Value = "= PageNumber";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(9.6002359390258789D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.299999862909317D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox9.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            this.textBox9.Value = "= PageCount";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(8.699920654296875D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox10.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            this.textBox10.Value = "Page";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(9.4000787734985352D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(7.9472862068996619E-08D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.20007896423339844D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox11.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            this.textBox11.Value = "of";
            // 
            // txtReportDate
            // 
            this.txtReportDate.Format = "{0:g}";
            this.txtReportDate.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.1999998092651367D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.70007878541946411D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtReportDate.Name = "txtReportDate";
            this.txtReportDate.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.7999998331069946D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtReportDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtReportDate.Value = "textBox12";
            // 
            // textBox12
            // 
            this.textBox12.CanGrow = false;
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.7524874210357666D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.29999956488609314D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox12.Style.Color = System.Drawing.Color.Gray;
            this.textBox12.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
            this.textBox12.Value = "By:";
            // 
            // txtUserName
            // 
            this.txtUserName.CanGrow = false;
            this.txtUserName.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(8.3000774383544922D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.7524874210357666D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtUserName.Multiline = false;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.5998420715332031D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtUserName.Style.Color = System.Drawing.Color.Gray;
            this.txtUserName.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
            this.txtUserName.TextWrap = false;
            this.txtUserName.Value = "textBox13";
            // 
            // txtCustName
            // 
            this.txtCustName.CanGrow = false;
            this.txtCustName.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.55240887403488159D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtCustName.Multiline = false;
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8000000715255737D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtCustName.Style.Color = System.Drawing.Color.Black;
            this.txtCustName.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
            this.txtCustName.TextWrap = false;
            this.txtCustName.Value = "textBox13";
            // 
            // textBox18
            // 
            this.textBox18.CanGrow = false;
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.091588400304317474D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.552408754825592D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.29999956488609314D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox18.Style.Color = System.Drawing.Color.Black;
            this.textBox18.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
            this.textBox18.Value = "For:";
            // 
            // txtTo
            // 
            this.txtTo.CanGrow = false;
            this.txtTo.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.1916670799255371D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.75248748064041138D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtTo.Multiline = false;
            this.txtTo.Name = "txtTo";
            this.txtTo.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.699999988079071D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtTo.Style.Color = System.Drawing.Color.Black;
            this.txtTo.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
            this.txtTo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtTo.Style.Visible = true;
            this.txtTo.TextWrap = false;
            this.txtTo.Value = "textBox13";
            // 
            // textBox19
            // 
            this.textBox19.CanGrow = false;
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.89166724681854248D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.75248759984970093D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.29992112517356873D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.textBox19.Style.Color = System.Drawing.Color.Black;
            this.textBox19.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox19.Style.Visible = true;
            this.textBox19.Value = "To";
            // 
            // txtFrom
            // 
            this.txtFrom.CanGrow = false;
            this.txtFrom.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.19158846139907837D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.75248748064041138D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtFrom.Multiline = false;
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.699999988079071D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.txtFrom.Style.Color = System.Drawing.Color.Black;
            this.txtFrom.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
            this.txtFrom.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtFrom.Style.Visible = true;
            this.txtFrom.TextWrap = false;
            this.txtFrom.Value = "textBox13";
            // 
            // TimeSheetCostDDLvl1
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "MasterSchedule";
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.AllowBlank = false;
            reportParameter1.Name = "customerId";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter2.AllowBlank = false;
            reportParameter2.Name = "companyId";
            reportParameter2.Type = Telerik.Reporting.ReportParameterType.Integer;
            reportParameter3.AllowBlank = false;
            reportParameter3.Name = "dtFromDt";
            reportParameter3.Type = Telerik.Reporting.ReportParameterType.DateTime;
            reportParameter4.AllowBlank = false;
            reportParameter4.Name = "dtToDt";
            reportParameter4.Type = Telerik.Reporting.ReportParameterType.DateTime;
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.ReportParameters.Add(reportParameter4);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.NeedDataSource += new System.EventHandler(this.TimeSheetCostDDLvl1_NeedDataSource);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}
	#endregion

    private void TimeSheetCostDDLvl1_NeedDataSource(object sender, EventArgs e) {
        UpdateVals();
        // Load the data.
        LoadData();
    }
    private void crosstab1_ItemDataBound(object sender, EventArgs e) {
        UpdateVals();
    }
    protected void UpdateVals() {
        if (Report.ReportParameters.Count > 0) {
            Customerid = Convert.ToInt32(Report.ReportParameters["customerId"].Value);
            Companydivisionid = Convert.ToInt32(Report.ReportParameters["companyId"].Value);
            DateTime dtFromDt = Convert.ToDateTime(Report.ReportParameters["dtFromDt"].Value);
            DateTime dtToDt = Convert.ToDateTime(Report.ReportParameters["dtToDt"].Value);
            Customername = ATGDB.Customer.GetCustomerById(Customerid.ToString()).Description;
            UserName = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            // Rework the dates to be the very beginning and very ending of the months.
            // Beginning of month.
            dtFrom = DateTime.Parse(dtFromDt.Month.ToString() + "/1/" + dtFromDt.Year.ToString());
            // Ending of month.
            dtTo = DateTime.Parse(dtToDt.Month.ToString() + "/" +
                DateTime.DaysInMonth(dtToDt.Year, dtToDt.Month).ToString() + "/" +
                dtToDt.Year.ToString());

            // Some vals.
            txtCustName.Value = Customername;
            txtReportDate.Value = DateTime.Now.Date.ToLongDateString();
            txtUserName.Value = UserName;
            txtFooter.Value = Resources.Resource.ReportFooterText;
            txtFrom.Value = dtFrom.Date.ToShortDateString();
            txtTo.Value = dtTo.Date.ToShortDateString();
        }
    }
    

}