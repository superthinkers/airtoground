using System;
using Telerik.Reporting;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for CustomerEquipmentStatusList.
/// </summary>
public class CustomerEquipmentStatusList : Report
{
    private int Customerid = 0;
    string Customername = "";
    string UserName = "";

	private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
    private Telerik.Reporting.DetailSection detail;
    private TextBox textBox1;
    private TextBox textBox2;
    private TextBox textBox3;
    private TextBox textBox4;
    private TextBox textBox5;
    private TextBox textBox6;
    private TextBox textBox7;
    private TextBox textBox9;
    private ReportHeaderSection reportHeaderSection1;
    private TextBox txtUserName;
    private TextBox textBox12;
    private TextBox txtReportDate;
    private TextBox textBox11;
    private TextBox textBox13;
    private TextBox textBox14;
    private TextBox textBox15;
    private TextBox textBox16;
    private TextBox textBox17;
    private TextBox txtFooter;
    private TextBox textBox18;
    private TextBox txtCustName;
    private TextBox textBox8;
    private TextBox textBox10;
    private TextBox textBox19;
    private TextBox textBox20;
    private TextBox textBox21;
    private TextBox textBox22;
    private TextBox textBox23;
    private TextBox textBox24;
	private Telerik.Reporting.PageFooterSection pageFooterSection1;

    class MyReportData
    {
        public int Equipmentid { get; set; }
        public string Tailnumber { get; set; }
        public string Description { get; set; }
        public string Equipmenttypedescr { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }
        public string Statusreason { get; set; }
        public string Statusnotes { get; set; }
        public string Statusby { get; set; }
        public DateTime Statusdate { get; set; }
        public DateTime? Returndate { get; set; }
    }
    public CustomerEquipmentStatusList(int custId, string custName, string userNm) {
        Customerid = custId;
        Customername = custName;
        UserName = userNm;
        
        // Create.
        InitializeComponent();

        // Some vals.
        txtCustName.Value = Customername;
        txtReportDate.Value = DateTime.Now.Date.ToLongDateString();
        txtUserName.Value = UserName;
        txtFooter.Value = Resources.Resource.ReportFooterText;

        // Load the data.
        LoadData();
    }

    protected void LoadData() {
        ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
        List<MyReportData> data = (from e in db.Equipments
                                   from s in db.Equipmentstatuslogs
                                   where s.Equipmentstatuslogid == e.Equipmentstatuslogid
                                   where s.Unholddate == null
                                   where e.Customerid == Customerid
                                   orderby e.Tailnumber
                                   select new MyReportData() {
                                       Equipmentid = e.Equipmentid,
                                       Tailnumber = e.Tailnumber,
                                       Description = e.Description,
                                       Equipmenttypedescr = e.Equipmenttype.Description,
                                       Notes = e.Notes,
                                       Status = s.Status,
                                       Statusby = s.Statusby,
                                       Statusreason = s.Statusreason,
                                       Statusnotes = s.Statusnotes,
                                       Statusdate = s.Statusdate,
                                       Returndate = s.Returndate
                                   }).ToList<MyReportData>();
        this.DataSource = data;
    }
    protected CustomerEquipmentStatusList()
	{
		//
		// Required for telerik Reporting designer support
		//
		InitializeComponent();

		//
		// TODO: Add any constructor code after InitializeComponent call
		//
	}

	#region Component Designer generated code
	/// <summary>
	/// Required method for telerik Reporting designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent()
	{
        Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
        Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
        Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
        Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
        Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
        this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
        this.txtUserName = new Telerik.Reporting.TextBox();
        this.textBox12 = new Telerik.Reporting.TextBox();
        this.txtReportDate = new Telerik.Reporting.TextBox();
        this.textBox11 = new Telerik.Reporting.TextBox();
        this.textBox13 = new Telerik.Reporting.TextBox();
        this.textBox14 = new Telerik.Reporting.TextBox();
        this.textBox15 = new Telerik.Reporting.TextBox();
        this.textBox16 = new Telerik.Reporting.TextBox();
        this.textBox17 = new Telerik.Reporting.TextBox();
        this.textBox18 = new Telerik.Reporting.TextBox();
        this.txtCustName = new Telerik.Reporting.TextBox();
        this.textBox5 = new Telerik.Reporting.TextBox();
        this.textBox6 = new Telerik.Reporting.TextBox();
        this.textBox4 = new Telerik.Reporting.TextBox();
        this.detail = new Telerik.Reporting.DetailSection();
        this.textBox3 = new Telerik.Reporting.TextBox();
        this.textBox21 = new Telerik.Reporting.TextBox();
        this.textBox22 = new Telerik.Reporting.TextBox();
        this.textBox23 = new Telerik.Reporting.TextBox();
        this.textBox24 = new Telerik.Reporting.TextBox();
        this.textBox1 = new Telerik.Reporting.TextBox();
        this.textBox2 = new Telerik.Reporting.TextBox();
        this.textBox9 = new Telerik.Reporting.TextBox();
        this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
        this.txtFooter = new Telerik.Reporting.TextBox();
        this.textBox7 = new Telerik.Reporting.TextBox();
        this.reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
        this.textBox8 = new Telerik.Reporting.TextBox();
        this.textBox10 = new Telerik.Reporting.TextBox();
        this.textBox19 = new Telerik.Reporting.TextBox();
        this.textBox20 = new Telerik.Reporting.TextBox();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // pageHeaderSection1
        // 
        this.pageHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
        this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtUserName,
            this.textBox12,
            this.txtReportDate,
            this.textBox11,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.txtCustName});
        this.pageHeaderSection1.Name = "pageHeaderSection1";
        // 
        // txtUserName
        // 
        this.txtUserName.CanGrow = false;
        this.txtUserName.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.2002353668212891D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.69791668653488159D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.txtUserName.Multiline = false;
        this.txtUserName.Name = "txtUserName";
        this.txtUserName.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.5998420715332031D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.txtUserName.Style.Color = System.Drawing.Color.Gray;
        this.txtUserName.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
        this.txtUserName.TextWrap = false;
        this.txtUserName.Value = "textBox13";
        // 
        // textBox12
        // 
        this.textBox12.CanGrow = false;
        this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.9001574516296387D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.69791668653488159D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox12.Name = "textBox12";
        this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.29999956488609314D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox12.Style.Color = System.Drawing.Color.Gray;
        this.textBox12.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox12.Value = "By:";
        // 
        // txtReportDate
        // 
        this.txtReportDate.Format = "{0:g}";
        this.txtReportDate.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.1000795364379883D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.69791668653488159D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.txtReportDate.Name = "txtReportDate";
        this.txtReportDate.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.7999998331069946D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.txtReportDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
        this.txtReportDate.Value = "textBox12";
        // 
        // textBox11
        // 
        this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(9.4021625518798828D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox11.Name = "textBox11";
        this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.20007896423339844D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox11.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox11.Value = "of";
        // 
        // textBox13
        // 
        this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(8.6999998092651367D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox13.Name = "textBox13";
        this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox13.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox13.Value = "Page";
        // 
        // textBox14
        // 
        this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(9.602320671081543D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox14.Name = "textBox14";
        this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.299999862909317D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox14.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox14.Value = "= PageCount";
        // 
        // textBox15
        // 
        this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(9.1000795364379883D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox15.Name = "textBox15";
        this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.299999862909317D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox15.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox15.Value = "= PageNumber";
        // 
        // textBox16
        // 
        this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.4000003337860107D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.49783784151077271D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox16.Name = "textBox16";
        this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.7000000476837158D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox16.Style.Font.Bold = true;
        this.textBox16.Style.Font.Name = "Arial Unicode MS";
        this.textBox16.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox16.Style.Font.Strikeout = false;
        this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
        this.textBox16.Value = "Aircraft Status List";
        // 
        // textBox17
        // 
        this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.1000000610947609D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999986588954926D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox17.Name = "textBox17";
        this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.2000000476837158D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19996054470539093D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox17.Style.Font.Bold = true;
        this.textBox17.Style.Font.Name = "Arial Unicode MS";
        this.textBox17.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox17.Style.Font.Strikeout = false;
        this.textBox17.Value = "Air to Ground Services, Inc.";
        // 
        // textBox18
        // 
        this.textBox18.CanGrow = false;
        this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.69791668653488159D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox18.Name = "textBox18";
        this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.29999956488609314D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox18.Style.Color = System.Drawing.Color.Black;
        this.textBox18.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox18.Value = "For:";
        // 
        // txtCustName
        // 
        this.txtCustName.CanGrow = false;
        this.txtCustName.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.30000004172325134D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.69791668653488159D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.txtCustName.Multiline = false;
        this.txtCustName.Name = "txtCustName";
        this.txtCustName.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8000000715255737D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.txtCustName.Style.Color = System.Drawing.Color.Black;
        this.txtCustName.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Point);
        this.txtCustName.TextWrap = false;
        this.txtCustName.Value = "textBox13";
        // 
        // textBox5
        // 
        this.textBox5.CanGrow = false;
        this.textBox5.CanShrink = false;
        this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.90003949403762817D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox5.Name = "textBox5";
        this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.89999991655349731D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox5.Style.Font.Bold = true;
        this.textBox5.Value = "Description";
        // 
        // textBox6
        // 
        this.textBox6.CanGrow = false;
        this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.9000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox6.Name = "textBox6";
        this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox6.Style.Font.Bold = true;
        this.textBox6.Value = "Status Notes";
        // 
        // textBox4
        // 
        this.textBox4.CanGrow = false;
        this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9339065551757812E-05D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox4.Name = "textBox4";
        this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.89992135763168335D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox4.Style.Font.Bold = true;
        this.textBox4.Value = "Tail Number";
        // 
        // detail
        // 
        this.detail.Height = new Telerik.Reporting.Drawing.Unit(0.20003955066204071D, Telerik.Reporting.Drawing.UnitType.Inch);
        this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox3,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox1,
            this.textBox2,
            this.textBox9});
        this.detail.Name = "detail";
        // 
        // textBox3
        // 
        this.textBox3.CanGrow = true;
        this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.9000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox3.Name = "textBox3";
        this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.0042450428009033D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000012218952179D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox3.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox3.Value = "=Fields.Notes";
        // 
        // textBox21
        // 
        this.textBox21.CanGrow = true;
        this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.9000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox21.Name = "textBox21";
        this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000012218952179D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox21.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox21.Value = "=Fields.Status";
        // 
        // textBox22
        // 
        this.textBox22.CanGrow = true;
        this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.4000003337860107D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox22.Name = "textBox22";
        this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3999999761581421D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000012218952179D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox22.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox22.Value = "=Fields.Statusreason";
        // 
        // textBox23
        // 
        this.textBox23.CanGrow = false;
        this.textBox23.Format = "{0:d}";
        this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.9000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox23.Name = "textBox23";
        this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.84992194175720215D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000012218952179D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox23.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox23.Value = "=Fields.Statusdate";
        // 
        // textBox24
        // 
        this.textBox24.CanGrow = false;
        this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.8000001907348633D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox24.Name = "textBox24";
        this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0520836114883423D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000012218952179D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox24.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox24.Value = "=Fields.Statusby";
        // 
        // textBox1
        // 
        this.textBox1.CanGrow = false;
        this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox1.Name = "textBox1";
        this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.89992135763168335D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000012218952179D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox1.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox1.Value = "=Fields.TailNumber";
        // 
        // textBox2
        // 
        this.textBox2.CanGrow = true;
        this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.90003949403762817D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox2.Name = "textBox2";
        this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.89999991655349731D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000012218952179D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox2.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox2.Value = "=Fields.Description";
        // 
        // textBox9
        // 
        this.textBox9.CanGrow = false;
        this.textBox9.Format = "{0:d}";
        this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(6.89992094039917D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox9.Name = "textBox9";
        this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.90007859468460083D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000012218952179D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox9.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        this.textBox9.Value = "=Fields.Returndate";
        // 
        // pageFooterSection1
        // 
        this.pageFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.60000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch);
        this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtFooter});
        this.pageFooterSection1.Name = "pageFooterSection1";
        // 
        // txtFooter
        // 
        this.txtFooter.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.txtFooter.Name = "txtFooter";
        this.txtFooter.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.60000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.txtFooter.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
        this.txtFooter.Style.Color = System.Drawing.Color.Gray;
        this.txtFooter.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(7D, Telerik.Reporting.Drawing.UnitType.Point);
        this.txtFooter.Style.Font.Strikeout = false;
        this.txtFooter.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
        this.txtFooter.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
        // 
        // textBox7
        // 
        this.textBox7.CanGrow = false;
        this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(6.89992094039917D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox7.Name = "textBox7";
        this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.90007883310317993D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox7.Style.Font.Bold = true;
        this.textBox7.Value = "Return Date";
        // 
        // reportHeaderSection1
        // 
        this.reportHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.20003938674926758D, Telerik.Reporting.Drawing.UnitType.Inch);
        this.reportHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox5,
            this.textBox7,
            this.textBox6,
            this.textBox8,
            this.textBox10,
            this.textBox19,
            this.textBox20});
        this.reportHeaderSection1.Name = "reportHeaderSection1";
        // 
        // textBox8
        // 
        this.textBox8.CanGrow = false;
        this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.9000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox8.Name = "textBox8";
        this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.59999990463256836D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox8.Style.Font.Bold = true;
        this.textBox8.Value = "Status";
        // 
        // textBox10
        // 
        this.textBox10.CanGrow = false;
        this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.4000003337860107D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox10.Name = "textBox10";
        this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.64791566133499146D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox10.Style.Font.Bold = true;
        this.textBox10.Value = "Reason";
        // 
        // textBox19
        // 
        this.textBox19.CanGrow = false;
        this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.9000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox19.Name = "textBox19";
        this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.500079333782196D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox19.Style.Font.Bold = true;
        this.textBox19.Value = "Date";
        // 
        // textBox20
        // 
        this.textBox20.CanGrow = false;
        this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.8000001907348633D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox20.Name = "textBox20";
        this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
        this.textBox20.Style.Font.Bold = true;
        this.textBox20.Value = "By";
        // 
        // CustomerEquipmentStatusList
        // 
        this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1,
            this.reportHeaderSection1});
        this.Name = "CustomerEquipmentList";
        this.PageSettings.Landscape = true;
        this.PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
        this.PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch);
        this.PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch);
        this.PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
        this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
        this.Style.BackgroundColor = System.Drawing.Color.White;
        this.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
        this.Style.Padding.Left = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
        this.Style.Padding.Right = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
        styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Civic.TableNormal")});
        styleRule1.Style.BackgroundColor = System.Drawing.Color.White;
        styleRule1.Style.BorderColor.Default = System.Drawing.Color.Black;
        styleRule1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
        styleRule1.Style.BorderWidth.Default = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Pixel);
        styleRule1.Style.Color = System.Drawing.Color.Black;
        styleRule1.Style.Font.Name = "Georgia";
        styleRule1.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
        descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Civic.TableHeader")});
        styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
        styleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(174)))), ((int)(((byte)(173)))));
        styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
        styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
        styleRule2.Style.BorderWidth.Default = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Pixel);
        styleRule2.Style.Color = System.Drawing.Color.White;
        styleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
        descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Civic.TableBody")});
        styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
        styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
        styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
        styleRule3.Style.BorderWidth.Default = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Pixel);
        this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3});
        this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Pixel;
        this.Width = new Telerik.Reporting.Drawing.Unit(10D, Telerik.Reporting.Drawing.UnitType.Inch);
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}
	#endregion
}