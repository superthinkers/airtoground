using System;
using Telerik.Reporting;
using System.Linq;
using ATGDB;
using Telerik.Web.UI;


/// <summary>
/// Summary description for CustomerEquipmentList.
/// </summary>
public class RecentServicePerformed : Report
{
    private int Customerid = 0;
    string Customername = "";
    string UserName = "";
    //DateTime dtFrom;
    //DateTime dtTo;
    bool SortByEqTypes = false;
    bool SortByTail = false;
    RadListBox lstEqTypes = null;
    RadListBox lstServices = null;
    private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
    private Telerik.Reporting.DetailSection detail;
    private TextBox textBox1;
    private TextBox textBox2;
    private TextBox textBox3;
    private TextBox textBox4;
    private TextBox textBox5;
    private TextBox textBox6;
    private TextBox textBox7;
    private TextBox textBox8;
    private TextBox textBox9;
    private TextBox textBox10;
    private ReportHeaderSection reportHeaderSection1;
    private TextBox txtUserName;
    private TextBox textBox12;
    private TextBox txtReportDate;
    private TextBox textBox11;
    private TextBox textBox13;
    private TextBox textBox14;
    private TextBox textBox15;
    private TextBox textBox16;
    private TextBox textBox17;
    private TextBox txtFooter;
    private TextBox textBox18;
    private TextBox txtCustName;
    private TextBox textBox20;
    private TextBox textBox19;
    private TextBox txtDates;
    private TextBox textBox22;
    private TextBox textBox23;
    private TextBox textBox21;
    private TextBox textBox25;
    private TextBox textBox24;
	private Telerik.Reporting.PageFooterSection pageFooterSection1;

    public RecentServicePerformed(int custId, string custName, string userNm, /*DateTime fromDt, DateTime toDt,*/
        bool sortEQ, bool sortTN, RadListBox lstEqTypesBox, RadListBox lstServicesBox) {
        Customerid = custId;
        Customername = custName;
        UserName = userNm;
        //dtFrom = fromDt;
        //dtTo = toDt;
        SortByEqTypes = sortEQ;
        SortByTail = sortTN;
        lstEqTypes = lstEqTypesBox;
        lstServices = lstServicesBox;
        
        // Create.
        InitializeComponent();

        // Some vals.
        txtCustName.Value = Customername;
        txtReportDate.Value = DateTime.Now.ToString();
        txtUserName.Value = UserName;
        txtFooter.Value = Resources.Resource.ReportFooterText;
        //txtDates.Value = dtFrom.ToShortDateString() + " to " + dtTo.ToShortDateString();
    }
    protected override void OnNeedDataSource(object sender, EventArgs e) {
        // Load the data.
        LoadData();
    }
    class MyReportData
    {
        public string Tailnumber { get; set; }
        public string Description { get; set; }
        public string Area { get; set; }
        public string Equipmenttypedescr { get; set; }
        public DateTime Completeddate { get; set; }
        public string Location { get; set; }
        public string Nextdate { get; set; }
        public bool IsNextScheduled { get; set; }
        public bool IsHold { get; set; }
    }
    protected void LoadData() {
        string services = Serviceschedule.GetServicesDataViewFilter(lstServices);
        string eqTypes =Serviceschedule.GetEqTypesDataViewFilter(lstEqTypes);

        ATGDataContext db = new ATGDataContext();
        // Get all next scheduled dates for the customer
        var next = (from x in db.Serviceschedules
                   where x.Equipment.Customerid == Customerid
                   where ((eqTypes != "") && (eqTypes != null)) ? eqTypes.IndexOf(x.Equipment.Equipmenttypeid.ToString()) > 0 : true
                   where x.Servicedate != null
                   //where x.Servicedate.Date >= dtFrom.Date
                   //where x.Servicedate.Date <= dtTo.Date
                   where x.Completeddate == null
                   where x.Cancelleddate == null
                   where x.Service.Showinmasterschedule == true
                   where ((services != "") && (services != null)) ? services.IndexOf(x.Serviceid.ToString()) > 0 : true
                   select x);

        var hold = (from x in db.Equipmentstatuslogs
                    where x.Unholddate == null
                    select x);

        // Get all last completed schedule dates for each tail number
        var data = (from x in db.Serviceschedules
                   where x.Equipment.Customerid == Customerid
                   where ((eqTypes != "") && (eqTypes != null)) ? eqTypes.IndexOf(x.Equipment.Equipmenttypeid.ToString()) > 0 : true
                   where x.Completeddate != null
                   where x.Completeddate == (db.Serviceschedules.Where(w => w.Equipmentid == x.Equipmentid && w.Serviceid == x.Serviceid).Max(r => r.Completeddate))
                   where x.Service.Showinmasterschedule == true
                   where ((services != "") && (services != null)) ? services.IndexOf(x.Serviceid.ToString()) > 0 : true
                   select new MyReportData() {
                        Tailnumber = x.Equipment.Tailnumber,
                        Description = x.Service.Description,
                        Area = x.Divisionarea == null ? "" : x.Divisionarea.Description,
                        Equipmenttypedescr = x.Equipment.Equipmenttype.Description,
                        Completeddate = x.Completeddate.Value.Date,
                        Location = x.Companydivision != null ? x.Companydivision.Description : "<unassigned>",
                        // What is the next date?
                        Nextdate = (x.Service.Serviceinterval <= 0 ? null :
                                       ((next.Where(y => y.Equipment.Tailnumber == x.Equipment.Tailnumber &&
                                                    y.Serviceid == x.Serviceid).Count() <= 0 ?
                                                    // Does NOT have scheduled dates, compute next
                                                    x.Completeddate.Value.AddDays(x.Service.Serviceinterval) :
                                                    // Has future scheudled dates, so get the minimum next scheduled date and show it as the next scheudled date
                                                    next.Where(y => y.Equipment.Tailnumber == x.Equipment.Tailnumber &&
                                                    y.Serviceid == x.Serviceid).Min(z => z.Servicedate))).ToString()
                                   ), 
                        IsNextScheduled = (next.Where(y => y.Equipment.Tailnumber == x.Equipment.Tailnumber &&
                                                      y.Serviceid == x.Serviceid).Count() > 0) && x.Service.Serviceinterval > 0,
                        IsHold = (hold.Where(y => y.Equipmentid == x.Equipmentid).Count() > 0) == true ? true : false
              });

        if (SortByEqTypes == true) {
            var data2 = data.OrderBy(a => a.Equipmenttypedescr +  a.Tailnumber + a.Completeddate);
            this.DataSource = data2;
        } else if (SortByTail == true) {
            var data2 = data.OrderBy(a => a.Tailnumber + a.Equipmenttypedescr + a.Completeddate);
            this.DataSource = data2;
        } else {
            this.DataSource = data;
        }                          
    }
    protected RecentServicePerformed()
	{
		//
		// Required for telerik Reporting designer support
		//
		InitializeComponent();

		//
		// TODO: Add any constructor code after InitializeComponent call
		//
	}

	#region Component Designer generated code
	/// <summary>
	/// Required method for telerik Reporting designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent()
	{
            Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule2 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule3 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.txtUserName = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.txtReportDate = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.txtCustName = new Telerik.Reporting.TextBox();
            this.txtDates = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.txtFooter = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtUserName,
            this.textBox12,
            this.txtReportDate,
            this.textBox11,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.txtCustName,
            this.txtDates,
            this.textBox22});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // txtUserName
            // 
            this.txtUserName.CanGrow = false;
            this.txtUserName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.9020829200744629D), Telerik.Reporting.Drawing.Unit.Inch(0.69791668653488159D));
            this.txtUserName.Multiline = false;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5998420715332031D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.txtUserName.Style.Color = System.Drawing.Color.Gray;
            this.txtUserName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtUserName.TextWrap = false;
            this.txtUserName.Value = "textBox13";
            // 
            // textBox12
            // 
            this.textBox12.CanGrow = false;
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.69791668653488159D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.29999956488609314D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox12.Style.Color = System.Drawing.Color.Gray;
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox12.Value = "By:";
            // 
            // txtReportDate
            // 
            this.txtReportDate.Format = "{0:G}";
            this.txtReportDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.70007866621017456D));
            this.txtReportDate.Name = "txtReportDate";
            this.txtReportDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7999998331069946D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.txtReportDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtReportDate.Value = "textBox12";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9020829200744629D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.20007896423339844D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox11.Value = "of";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.199920654296875D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox13.Value = "Page";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.102241039276123D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.299999862909317D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox14.Value = "= PageCount";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.5999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.299999862909317D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox15.Value = "= PageNumber";
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.4000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.49999982118606567D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Arial Unicode MS";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox16.Style.Font.Strikeout = false;
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox16.Value = "Recent Service Performed Report";
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.19996054470539093D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Arial Unicode MS";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox17.Style.Font.Strikeout = false;
            this.textBox17.Value = "Air to Ground Services, Inc.";
            // 
            // textBox18
            // 
            this.textBox18.CanGrow = false;
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.49999967217445374D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.29999956488609314D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox18.Style.Color = System.Drawing.Color.Black;
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox18.Value = "For:";
            // 
            // txtCustName
            // 
            this.txtCustName.CanGrow = false;
            this.txtCustName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D), Telerik.Reporting.Drawing.Unit.Inch(0.49999967217445374D));
            this.txtCustName.Multiline = false;
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8000000715255737D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.txtCustName.Style.Color = System.Drawing.Color.Black;
            this.txtCustName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtCustName.TextWrap = false;
            this.txtCustName.Value = "textBox13";
            // 
            // txtDates
            // 
            this.txtDates.CanGrow = false;
            this.txtDates.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.400078684091568D), Telerik.Reporting.Drawing.Unit.Inch(0.69791668653488159D));
            this.txtDates.Multiline = false;
            this.txtDates.Name = "txtDates";
            this.txtDates.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.3998427391052246D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.txtDates.Style.Color = System.Drawing.Color.Black;
            this.txtDates.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtDates.Style.Visible = false;
            this.txtDates.TextWrap = false;
            this.txtDates.Value = "textBox13";
            // 
            // textBox22
            // 
            this.textBox22.CanGrow = false;
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0083333654329180717D), Telerik.Reporting.Drawing.Unit.Inch(0.70007866621017456D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39166665077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox22.Style.Color = System.Drawing.Color.Black;
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox22.Style.Visible = false;
            this.textBox22.Value = "From:";
            // 
            // textBox5
            // 
            this.textBox5.CanGrow = false;
            this.textBox5.CanShrink = false;
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.5D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89999991655349731D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Value = "Service";
            // 
            // textBox6
            // 
            this.textBox6.CanGrow = false;
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.699920654296875D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Value = "Location";
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.299960196018219D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Value = "Tail";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20003955066204071D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox2,
            this.textBox9,
            this.textBox10,
            this.textBox3,
            this.textBox20,
            this.textBox23});
            this.detail.Name = "detail";
            // 
            // textBox1
            // 
            this.textBox1.CanGrow = false;
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49992132186889648D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox1.Value = "=Fields.TailNumber";
            // 
            // textBox2
            // 
            this.textBox2.CanGrow = false;
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.5D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox2.Value = "=Fields.Description";
            // 
            // textBox9
            // 
            this.textBox9.CanGrow = false;
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.2000787258148193D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.899921178817749D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox9.Value = "=Fields.Equipmenttypedescr";
            // 
            // textBox10
            // 
            this.textBox10.CanGrow = false;
            this.textBox10.Format = "{0:d}";
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000048875808716D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.Value = "=Fields.Completeddate";
            // 
            // textBox3
            // 
            this.textBox3.CanGrow = true;
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox3.Value = "=Fields.Location";
            // 
            // textBox20
            // 
            this.textBox20.CanGrow = false;
            formattingRule1.Filters.Add(new Telerik.Reporting.Filter("=Fields.IsNextScheduled", Telerik.Reporting.FilterOperator.Equal, "true"));
            formattingRule1.Style.Font.Bold = true;
            formattingRule2.Filters.Add(new Telerik.Reporting.Filter("=Fields.IsNextScheduled", Telerik.Reporting.FilterOperator.Equal, "false"));
            formattingRule2.Style.Font.Italic = true;
            formattingRule3.Filters.Add(new Telerik.Reporting.Filter("=Fields.IsHold", Telerik.Reporting.FilterOperator.Equal, "true"));
            formattingRule3.Style.Font.Bold = true;
            this.textBox20.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1,
            formattingRule2,
            formattingRule3});
            this.textBox20.Format = "{0}";
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1020822525024414D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1979176998138428D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox20.Value = "=IIf(CBool(Fields.IsHold), \"(On Hold)\", IIf(IsNull(Fields.Nextdate, \"(no interval" +
    ")\") = \"(no interval)\", \"(no interval)\", Format(\"{0:d}\", CDate(Fields.Nextdate)))" +
    ")";
            // 
            // textBox23
            // 
            this.textBox23.CanGrow = true;
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69999945163726807D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox23.Value = "=Fields.Area";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.60000002384185791D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtFooter,
            this.textBox25,
            this.textBox24});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // txtFooter
            // 
            this.txtFooter.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.19992104172706604D));
            this.txtFooter.Name = "txtFooter";
            this.txtFooter.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.501924991607666D), Telerik.Reporting.Drawing.Unit.Inch(0.40007898211479187D));
            this.txtFooter.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.txtFooter.Style.Color = System.Drawing.Color.Gray;
            this.txtFooter.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtFooter.Style.Font.Strikeout = false;
            this.txtFooter.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtFooter.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox25
            // 
            this.textBox25.CanGrow = false;
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.497917652130127D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox25.Style.Font.Bold = false;
            this.textBox25.Style.Font.Italic = true;
            this.textBox25.Value = "ITALICS = Nothing Scheduled, This is the due date";
            // 
            // textBox24
            // 
            this.textBox24.CanGrow = false;
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.6998820304870605D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Value = "BOLD = This is the Next Scheduled Date";
            // 
            // textBox7
            // 
            this.textBox7.CanGrow = false;
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.2000787258148193D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.899921178817749D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Value = "Aircraft Type";
            // 
            // textBox8
            // 
            this.textBox8.CanGrow = false;
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000048875808716D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Value = "Completed";
            // 
            // reportHeaderSection1
            // 
            this.reportHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20003938674926758D);
            this.reportHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox5,
            this.textBox7,
            this.textBox8,
            this.textBox6,
            this.textBox19,
            this.textBox21});
            this.reportHeaderSection1.Name = "reportHeaderSection1";
            // 
            // textBox19
            // 
            this.textBox19.CanGrow = false;
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1000781059265137D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3998825550079346D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Value = "Next Scheduled Date";
            // 
            // textBox21
            // 
            this.textBox21.CanGrow = false;
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.699920654296875D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Value = "Area";
            // 
            // RecentServicePerformed
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1,
            this.reportHeaderSection1});
            this.Name = "CustomerEquipmentList";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.5D), Telerik.Reporting.Drawing.Unit.Inch(0.5D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Civic.TableNormal")});
            styleRule1.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule1.Style.Color = System.Drawing.Color.Black;
            styleRule1.Style.Font.Name = "Georgia";
            styleRule1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Civic.TableHeader")});
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(174)))), ((int)(((byte)(173)))));
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.White;
            styleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Civic.TableBody")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3});
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Pixel;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.501924991607666D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}
	#endregion
}