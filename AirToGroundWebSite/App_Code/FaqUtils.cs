﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for FaqUtil
/// </summary>
public class FaqUtils
{
    public class Faq {
        public int FaqId { get; set; }
        public string FaqText { get; set; }
        public string FaqDescr { get; set; }
        public string FaqUrl { get; set; }
        public Faq (int id, string text, string descr, string url) {
            FaqId = id;
            FaqText = text;
            FaqDescr = descr;
            FaqUrl = url;
        }
    }

    private static void AddFaq(int faqIndex, List<Faq> list, string title, string descr, string url) {
        list.Add(new Faq(faqIndex, title, descr, url));
        faqIndex++;
    }
    public static List<Faq> GetFaqs() {
        List<Faq> faqs = new List<Faq>();

        // Add the faqs.
        int faqIndex = 0;
        AddFaq(faqIndex, faqs, "ALL DOCUMENTS", "(.zip File)", "~/Help/All/All.zip");
        AddFaq(faqIndex, faqs, "Training", "General (MS Word.docx)", "~/Help/TrainingGuide/TrainingGuide_General1.docx");
        AddFaq(faqIndex, faqs, "Training", "Home Page (MS Word.docx)", "~/Help/TrainingGuide/TrainingGuide_HomePage.docx");
        AddFaq(faqIndex, faqs, "Training", "Service Queue (MS Word.docx)", "~/Help/TrainingGuide/TrainingGuide_ServiceQueue.docx");
        AddFaq(faqIndex, faqs, "Training", "Tail Schedule (MS Word.docx)", "~/Help/TrainingGuide/TrainingGuide_TailSchedule.docx");
        AddFaq(faqIndex, faqs, "Training", "Time Sheet (MS Word.docx)", "~/Help/TrainingGuide/TrainingGuide_TimeSheet.docx");
        AddFaq(faqIndex, faqs, "Admin - Aircraft", "Administration (MS Word.docx)", "~/Help/Administrative/Aircraft.docx");
        AddFaq(faqIndex, faqs, "Admin - Customers, Services, Sections", "Administration (MS Word.docx)", "~/Help/Administrative/CustomersServicesSections.docx");
        AddFaq(faqIndex, faqs, "Admin - Job Codes", "Administration (MS Word.docx)", "~/Help/Administrative/JobCodes.docx");
        AddFaq(faqIndex, faqs, "Admin - Rate Schedules", "General Documentation (MS Word.docx)", "~/Help/Administrative/RateSchedule.docx");
        AddFaq(faqIndex, faqs, "Admin - Sections", "Administration (MS Word.docx)", "~/Help/Administrative/Sections.docx");
        AddFaq(faqIndex, faqs, "Admin - Services, Sections, Rules", "Administration (MS Word.docx)", "~/Help/Administrative/ServicesSectionsRules.docx");
        AddFaq(faqIndex, faqs, "Admin - Users and Roles", "Administration (MS Word.docx)", "~/Help/Administrative/UsersAndRoles.docx");
        AddFaq(faqIndex, faqs, "General Info", "General Documentation (MS Word.docx)", "~/Help/General/General.docx");
        AddFaq(faqIndex, faqs, "Master Schedule", "General Documentation (MS Word.docx)", "~/Help/Schedule/MasterSchedule.docx");
        AddFaq(faqIndex, faqs, "Reports", "General Documentation (MS Word.docx)", "~/Help/Reports/GeneralReports.docx");
        AddFaq(faqIndex, faqs, "Service Queue Manager", "General Documentation (MS Word.docx)", "~/Help/ServiceQueue/ServiceQueue.docx");
        //AddFaq(faqIndex, faqs, "Service Schedule Wizards", "General Documentation (MS Word.docx)", "~/Help/Schedule/ScheduleWizards.docx");
        AddFaq(faqIndex, faqs, "Time Sheets and Data Entry", "General Documentation (MS Word.docx)", "~/Help/TimeSheets/TimeSheets.docx");
        AddFaq(faqIndex, faqs, "View Tail Schedule", "General Documentation (MS Word.docx)", "~/Help/Schedule/ViewSchedule.docx");

        // Return
        return faqs;
    }

    public static List<Faq> GetCustomerFaqs() {
        List<Faq> faqs = new List<Faq>();

        // Add the faqs.
        int faqIndex = 0;
        AddFaq(faqIndex, faqs, "ALL DOCUMENTS", "(.zip File)", "~/Help/All/All.zip");
        AddFaq(faqIndex, faqs, "Training", "General (MS Word.docx)", "~/Help/TrainingGuide/TrainingGuide_General1.docx");
        AddFaq(faqIndex, faqs, "Training", "Home Page (MS Word.docx)", "~/Help/TrainingGuide/TrainingGuide_HomePage.docx");
        //AddFaq(faqIndex, faqs, "Training", "Service Queue (MS Word.docx)", "~/Help/TrainingGuide/TrainingGuide_ServiceQueue.docx");
        AddFaq(faqIndex, faqs, "Training", "Tail Schedule (MS Word.docx)", "~/Help/TrainingGuide/TrainingGuide_TailSchedule.docx");
        //AddFaq(faqIndex, faqs, "Training", "Time Sheet (MS Word.docx)", "~/Help/TrainingGuide/TrainingGuide_TimeSheet.docx");
        //AddFaq(faqIndex, faqs, "Admin - Aircraft", "Administration (MS Word.docx)", "~/Help/Administrative/Aircraft.docx");
        //AddFaq(faqIndex, faqs, "Admin - Customers, Services, Sections", "Administration (MS Word.docx)", "~/Help/Administrative/CustomersServicesSections.docx");
        //AddFaq(faqIndex, faqs, "Admin - Job Codes", "Administration (MS Word.docx)", "~/Help/Administrative/JobCodes.docx");
        //AddFaq(faqIndex, faqs, "Admin - Rate Schedules", "General Documentation (MS Word.docx)", "~/Help/Administrative/RateSchedule.docx");
        //AddFaq(faqIndex, faqs, "Admin - Sections", "Administration (MS Word.docx)", "~/Help/Administrative/Sections.docx");
        //AddFaq(faqIndex, faqs, "Admin - Services, Sections, Rules", "Administration (MS Word.docx)", "~/Help/Administrative/ServicesSectionsRules.docx");
        AddFaq(faqIndex, faqs, "Admin - Users and Roles", "Administration (MS Word.docx)", "~/Help/Administrative/UsersAndRoles.docx");
        AddFaq(faqIndex, faqs, "General Info", "General Documentation (MS Word.docx)", "~/Help/General/General.docx");
        AddFaq(faqIndex, faqs, "Master Schedule", "General Documentation (MS Word.docx)", "~/Help/Schedule/MasterSchedule.docx");
        AddFaq(faqIndex, faqs, "Reports", "General Documentation (MS Word.docx)", "~/Help/Reports/GeneralReports.docx");
        //AddFaq(faqIndex, faqs, "Service Queue Manager", "General Documentation (MS Word.docx)", "~/Help/ServiceQueue/ServiceQueue.docx");
        //AddFaq(faqIndex, faqs, "Service Schedule Wizards", "General Documentation (MS Word.docx)", "~/Help/Schedule/ScheduleWizards.docx");
        //AddFaq(faqIndex, faqs, "Time Sheets and Data Entry", "General Documentation (MS Word.docx)", "~/Help/TimeSheets/TimeSheets.docx");
        AddFaq(faqIndex, faqs, "View Tail Schedule", "General Documentation (MS Word.docx)", "~/Help/Schedule/ViewSchedule.docx");

        // Return
        return faqs;
    }
}