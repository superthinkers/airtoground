﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

/// <summary>
/// Summary description for TextBoxFilterGridTemplateColumn
/// </summary>
namespace ATG.Filters
{

    public class TextBoxFilterGridTemplateColumn : GridTemplateColumn
    {
        //public TextBoxFilterGridTemplateColumn() : base() {
            
        //}
        protected override void SetupFilterControls(TableCell cell) {
            RadTextBox txtBox = new RadTextBox();
            txtBox.Width = this.FilterControlWidth;
            //txtBox.Width = 80;
            txtBox.ID = "txt" + this.DataField + "Filter";
            txtBox.AutoPostBack = true;
            txtBox.TextChanged += txtBox_TextChanged;
            cell.Controls.Add(txtBox);
        }

        protected override void SetCurrentFilterValueToControl(TableCell cell) {
            if (!(this.CurrentFilterValue == "")) {
                ((RadTextBox)cell.Controls[0]).Text = this.CurrentFilterValue;
            }
        }

        protected override string GetCurrentFilterValueFromControl(TableCell cell) {
            string currentValue = ((RadTextBox)cell.Controls[0]).Text;
            this.CurrentFilterFunction = (currentValue != "") ? GridKnownFunction.EqualTo : GridKnownFunction.NoFilter;
            return currentValue;
        }

        private void txtBox_TextChanged(object sender, EventArgs e) {
            this.CurrentFilterValue = ((RadTextBox)sender).Text;
            ((GridFilteringItem)(((RadTextBox)sender).Parent.Parent)).FireCommandEvent("Do" + this.DataField + "Filter", new Pair(((RadTextBox)sender).Text, ""));
        }
    }
}