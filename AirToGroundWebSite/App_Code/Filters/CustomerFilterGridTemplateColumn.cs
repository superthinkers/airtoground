﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using ATGDB;

/// <summary>
/// Summary description for CustomerFilterGridTemplateColumn
/// </summary>
namespace ATG.Filters
{

    public class CustomerFilterGridTemplateColumn : GridTemplateColumn
    {
        public CustomerFilterGridTemplateColumn() : base() {
            
        }

        public string DataValueField { get; set; }
        protected override void SetupFilterControls(TableCell cell) {
            RadComboBox rcBox = new RadComboBox();
            rcBox.Width = this.FilterControlWidth;
            rcBox.ID = "lstCustomerFilter";
            rcBox.AutoPostBack = true;
            rcBox.DataTextField = "Description";
            if (String.IsNullOrEmpty(DataValueField)) {
                rcBox.DataValueField = "Customerid";
            } else {
                rcBox.DataValueField = DataValueField;
            }
            rcBox.SelectedIndexChanged += rcBox_SelectedIndexChanged;
            rcBox.DataBound += rcBox_DataBound;

            ATGDataContext dc = new ATGDataContext();
            var data = from c in dc.Customers
                       orderby c.Description
                       select c;

            rcBox.DataSource = data;
            cell.Controls.Add(rcBox);
        }

        protected override void SetCurrentFilterValueToControl(TableCell cell) {
            if (!(this.CurrentFilterValue == "")) {
                ((RadComboBox)cell.Controls[0]).Items.FindItemByValue(this.CurrentFilterValue).Selected = true;
            }
        }

        protected override string GetCurrentFilterValueFromControl(TableCell cell) {
            string currentValue = ((RadComboBox)cell.Controls[0]).SelectedItem.Value;
            this.CurrentFilterFunction = (currentValue != "") ? GridKnownFunction.EqualTo : GridKnownFunction.NoFilter;
            return currentValue;
        }

        private void rcBox_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e) {
            this.CurrentFilterValue = ((RadComboBox)sender).SelectedValue;
            ((GridFilteringItem)(((RadComboBox)sender).Parent.Parent)).FireCommandEvent("DoCustomerFilter", new Pair(((RadComboBox)sender).SelectedValue, ""));
        }
        protected void rcBox_DataBound(object sender, EventArgs e) {
            RadComboBox lst = (RadComboBox)sender;
            if (lst.Items[0].Value != "") {
                RadComboBoxItem item = new RadComboBoxItem("", "-1");
                lst.Items.Insert(0, item);
            }
        }
     }
}