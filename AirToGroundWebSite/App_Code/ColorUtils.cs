﻿using System;
using System.Linq;
using System.Drawing;

/// <summary>
/// Summary description for ColorUtils
/// </summary>
public static class ColorUtils
{
    // Service Queues.
    public static Color SERVICE_QUEUE_DONE_CLOSED = System.Drawing.Color.Silver;//Color.LightBlue;
    public static Color SERVICE_QUEUE_NDONE_NCLOSED = Color.MediumSpringGreen;
    public static Color SERVICE_QUEUE_NDONE_CLOSED = System.Drawing.Color.LightSalmon;//Color.FromArgb(169, 120, 191); // Light Purple  #A978BF

    public static string GetSS_LateBackColor(string aTheme) {
        /*if (aTheme == "Corp") {
            return "Silver";
        } else if (aTheme == "624") {
            return "Silver";
        } else if (aTheme == "201619") {
            return "Silver";
        } else {*/
        return "Yellow";
        //}
    }

    // Color versions. See ViewSchedules or MasterSchedules
    public static Color GetSS_CompletedColor(string aTheme) {
        if (aTheme == "Corp") {
            return Color.FromArgb(192, 156, 210); // Light Purple
        } else if (aTheme == "624") {
            return Color.FromArgb(192, 156, 210); // Light Purple
        } else if (aTheme == "201619") {
            return Color.FromArgb(136, 98, 97); // Light Brown
        } else {
            return Color.Purple;
        }
    }
    public static Color GetSS_CompletedExColor(string aTheme) {
        if (aTheme == "Corp") {
            return Color.Red;
        } else if (aTheme == "624") {
            return Color.Red;
        } else if (aTheme == "201619") {
            return Color.Red;
        } else {
            return Color.Red;
        }
    }
    public static Color GetSS_CancelledColor(string aTheme) {
        if (aTheme == "Corp") {
            return Color.FromArgb(192, 156, 210); // Light Purple
        } else if (aTheme == "624") {
            return Color.FromArgb(192, 156, 210); // Light Purple
        } else if (aTheme == "201619") {
            return Color.FromArgb(136, 98, 97); // Light Brown
        } else {
            return Color.Purple;
        }
    }
    public static Color GetSS_ScheduledColor(string aTheme) {
        if (aTheme == "Corp") {
            return Color.FromArgb(95, 185, 71); // Light Green
        } else if (aTheme == "624") {
            return Color.FromArgb(95, 185, 71); // Light Green
        } else if (aTheme == "201619") {
            return Color.FromArgb(95, 185, 71); // Light Green
        } else {
            return Color.FromArgb(95, 185, 71); // Light Green
        }
    }
    public static Color GetSS_NonScheduledColor(string aTheme) {
        if (aTheme == "Corp") {
            return Color.LightBlue;
        } else if (aTheme == "624") {
            return Color.LightBlue;
        } else if (aTheme == "201619") {
            return Color.LightBlue;
        } else {
            return Color.LightBlue;
        }
    }
    public static Color GetSS_FutureColor(string aTheme) {
        if (aTheme == "Corp") {
            return Color.DarkGray;
        } else if (aTheme == "624") {
            return Color.DarkGray;
        } else if (aTheme == "201619") {
            return Color.DarkGray;
        } else {
            return Color.DarkGray;
        }
    }

    // String Versions. See Serviceschedule.cs

    // MasterSchedule cell colors.
    public static string GetSS_CompletedColorString(string aTheme) {
        if (aTheme == "Corp") {
            return "Purple";
            //return "#9A5CB9"; // Light Purple
        } else if (aTheme == "624") {
            return "Purple";
            //return "#9A5CB9"; // Light Purple
        } else if (aTheme == "201619") {
            return "#886261"; // Light Brown
        } else {
            return "Purple";
        }
    }
    public static string GetSS_CompletedExColorString(string aTheme) {
        if (aTheme == "Corp") {
            return "Red";
        } else if (aTheme == "624") {
            return "Red";
        } else if (aTheme == "201619") {
            return "Red";
        } else {
            return "Red";
        }
    }
    public static string GetSS_CancelledColorString(string aTheme) {
        if (aTheme == "Corp") {
            return "Black";
        } else if (aTheme == "624") {
            return "Black";
        } else if (aTheme == "201619") {
            return "Black";
        } else {
            return "Black";
        }
    }
    public static string GetSS_ScheduledColorString(string aTheme) {
        if (aTheme == "Corp") {
            return "Green";
        } else if (aTheme == "624") {
            return "Green";
        } else if (aTheme == "201619") {
            return "Green";
        } else {
            return "Green";
        }
    }
    public static string GetSS_FutureColorString(string aTheme) {
        if (aTheme == "Corp") {
            return "Gray";
        } else if (aTheme == "624") {
            return "Gray";
        } else if (aTheme == "201619") {
            return "Gray";
        } else {
            return "Gray";
        }
    }
}