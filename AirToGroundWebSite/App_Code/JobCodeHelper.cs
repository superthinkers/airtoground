﻿using System.Collections.Generic;
using System.Linq;
using System.Data;
using ATGDB;

/// <summary>
/// Summary description for Jobcode
/// </summary>
public class JobCodeHelper
{

    public class CheckedJob
    {
        public bool IsChecked { get; set; }
        public int Companydivisionid { get; set; }
        public int Customerid { get; set; }
        public int Jobcodeid { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public CheckedJob() {
        }
        public CheckedJob(bool isChecked, int companyDivisionId, int customerId, int jobCodeId, string code, string description) {
            IsChecked = isChecked;
            Companydivisionid = companyDivisionId;
            Customerid = customerId;
            Jobcodeid = jobCodeId;
            Code = code;
            Description = description;
        }
    }

    public static List<CheckedJob> GetCheckedJobs(int companyDivisionId, int customerId) {
        List<CheckedJob> CheckedJobs = new List<CheckedJob>();
        ATGDataContext dc = new ATGDataContext();
        var jobs = from j in dc.Jobcodes
                   where j.Isactive == true
                   orderby j.Description
                   select j;
        var custjobs = from ccj in dc.Companycustomerjobjoins
                       where ccj.Companydivisionid == companyDivisionId
                       where ccj.Customerid == customerId
                       select ccj;

        bool found = false;
        foreach (ATGDB.Jobcode j in jobs) {
            found = false;
            foreach (Companycustomerjobjoin ccj in custjobs) {
                found = j.Jobcodeid == ccj.Jobcodeid;
                if (found) break;
            }
            CheckedJobs.Add(new CheckedJob(found, companyDivisionId, customerId, j.Jobcodeid, j.Code, j.Description));
        }        
        return CheckedJobs;
    }

    public static DataSet GetCheckedJobsDS(int companyDivisionId, int customerId) {
        List<CheckedJob> data = GetCheckedJobs(companyDivisionId, customerId);
        DataSet ds = new DataSet("checkedJobs");
        DataTable dt = ds.Tables.Add("checkedJob");
        dt.Columns.Add("IsChecked", typeof(bool));
        dt.Columns.Add("Companydivisionid", typeof(int));
        dt.Columns.Add("Customerid", typeof(int));
        dt.Columns.Add("Jobcodeid", typeof(int));
        dt.Columns.Add("Code", typeof(string));
        dt.Columns.Add("Description", typeof(string));
        foreach (CheckedJob job in data) {
            dt.Rows.Add(new object[]{ job.IsChecked, job.Companydivisionid, job.Customerid, job.Jobcodeid, job.Code, job.Description });
        }
        return ds;
    }

    public class CheckedCompanyCustomerJob
    {
        public bool IsChecked { get; set; }
        public int Companydivisionid { get; set; }
        public int Customerid { get; set; }
        public int Jobcodeid { get; set; }
        public string Companydescr { get; set; }
        public string Customerdescr { get; set; }
        public string Jobdescr { get; set; }
        public string Description { get; set; }
        public CheckedCompanyCustomerJob() {
        }
        public CheckedCompanyCustomerJob(bool isChecked, int companyDivisionId, int customerId, 
                int jobCodeId, string companyDescr, string custDescr, string jobDescr) {
            IsChecked = isChecked;
            Companydivisionid = companyDivisionId;
            Customerid = customerId;
            Jobcodeid = jobCodeId;
            Companydescr = companyDescr;
            Customerdescr = custDescr;
            Jobdescr = jobDescr;
            Description = companyDescr + " / " + custDescr;
        }
    }

    class CCComparer : IEqualityComparer<ATGDB.Companycustomerjoin>
    {
        public bool Equals(Companycustomerjoin x, Companycustomerjoin y) {
            return (x.Companydivisionid.Equals(y.Companydivisionid)) &&
                   (x.Customerid.Equals(y.Customerid));
        }
        public int GetHashCode(Companycustomerjoin obj) {
            return obj.Companydivisionid.GetHashCode() & obj.Customerid.GetHashCode();
        }
    }
    public static List<CheckedCompanyCustomerJob> GetCheckedCompanyCustomerJobs(int jobCodeId, string description) {
        List<CheckedCompanyCustomerJob> CheckedCompanyCustomerJobs = new List<CheckedCompanyCustomerJob>();
        ATGDataContext dc = new ATGDataContext();
        var ccu = (from y in dc.Companycustomerjoins
                  orderby y.Companydivision.Description
                  orderby y.Customer.Description
                  select y).ToList().Distinct(new CCComparer());

        var ccj = from x in dc.Companycustomerjobjoins
                  orderby x.Companycustomerjoin.Companydivision.Description
                  orderby x.Companycustomerjoin.Customer.Description
                  select x;

        bool found = false;
        foreach (ATGDB.Companycustomerjoin cc in ccu) {
            found = false;
            foreach (ATGDB.Companycustomerjobjoin j in ccj) {
                if ((cc.Companydivisionid == j.Companydivisionid) &&
                   (cc.Customerid == j.Customerid) &&
                   (j.Jobcodeid == jobCodeId)) {
                    // Found it.
                    found = true;
                }
                if (found == true) {
                    break;
                }
            }
            CheckedCompanyCustomerJobs.Add(new CheckedCompanyCustomerJob(found, cc.Companydivisionid, cc.Customerid, jobCodeId,
                cc.Companydivision.Description, cc.Customer.Description, description));
        }
        return CheckedCompanyCustomerJobs;
    }

    public static DataSet GetCheckedCompanyCustomerJobsDS(int jobCodeId, string description) {
        List<CheckedCompanyCustomerJob> data = GetCheckedCompanyCustomerJobs(jobCodeId, description);
        DataSet ds = new DataSet("checkedJobs");
        DataTable dt = ds.Tables.Add("checkedJob");
        dt.Columns.Add("IsChecked", typeof(bool));
        dt.Columns.Add("Companydivisionid", typeof(int));
        dt.Columns.Add("Customerid", typeof(int));
        dt.Columns.Add("Jobcodeid", typeof(int));
        dt.Columns.Add("Companydescr", typeof(string));
        dt.Columns.Add("Customerdescr", typeof(string));
        dt.Columns.Add("Jobdescr", typeof(string));
        dt.Columns.Add("Description", typeof(string));
        foreach (CheckedCompanyCustomerJob job in data) {
            dt.Rows.Add(new object[] { job.IsChecked, job.Companydivisionid, job.Customerid, job.Jobcodeid, 
                job.Companydescr, job.Customerdescr, job.Jobdescr, job.Description });
        }
        return ds;
    }

    public static void UpdateCompanyCustomerJob(bool isChecked, int companyDivisionId, int custId, int jobCodeId) {
        ATGDataContext dc = new ATGDataContext();
        if (isChecked) {
            // Add the job.
            var data = from x in dc.Companycustomerjobjoins
                       where x.Companydivisionid == companyDivisionId
                       where x.Customerid == custId
                       where x.Jobcodeid == jobCodeId
                       select x;
            if (data.Count() == 0) {
                Companycustomerjobjoin ccj = new Companycustomerjobjoin();
                ccj.Companydivisionid = companyDivisionId;
                ccj.Customerid = custId;
                ccj.Jobcodeid = jobCodeId;
                dc.Companycustomerjobjoins.InsertOnSubmit(ccj);
                dc.SubmitChanges();
            }
        } else {
            // Remove the job(s).
            var data = from x in dc.Companycustomerjobjoins
                       where x.Companydivisionid == companyDivisionId
                       where x.Customerid == custId
                       where x.Jobcodeid == jobCodeId
                       select x;
            if (data.Count() > 0) {
                foreach (Companycustomerjobjoin ccj in data) {
                    // Should only be one.
                    dc.Companycustomerjobjoins.DeleteOnSubmit(ccj);
                }
                dc.SubmitChanges();
            }
        }

        // EXTRA: Let's make sure AT LEAST the default CORP RATE Join is there. 
        var rates = from r in dc.Jobratejoins
                    where r.Companydivisionid == null
                    where r.Customerid == null
                    where r.Jobcodeid == jobCodeId
                    select r;
        if (rates.Count() == 0) {
            // Need to insert the CORP rate.
            ATGDB.Jobcode jc = Jobcode.GetJobcodeById(jobCodeId.ToString());
            
            // The rate.
            ATGDB.Ratedata rd = new Ratedata();
            rd.Cost = 0.00;
            rd.Description = "Corp Rate - " + jc.Code;
            rd.Flatfeeorhourly = "F";
            rd.Isactive = true;
            rd.Rate = 0.00;
            rd.Stdhours = 0;
            rd.Stdquantity = 0;
            rd.Upddt = System.DateTime.Now;
            rd.Upduserid = "System";
            dc.Ratedatas.InsertOnSubmit(rd);
            dc.SubmitChanges();
            
            // The join.
            ATGDB.Jobratejoin jrj = new Jobratejoin();
            jrj.Companydivisionid = companyDivisionId;
            jrj.Customerid = custId;
            jrj.Jobcodeid = jobCodeId;
            jrj.Rateid = rd.Rateid;
            dc.Jobratejoins.InsertOnSubmit(jrj);
            dc.SubmitChanges();
        }
    }

}