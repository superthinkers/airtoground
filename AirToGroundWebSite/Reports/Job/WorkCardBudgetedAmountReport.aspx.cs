﻿using System;
using System.Linq;

public partial class Reports_WorkCardBudgetedAmountReport : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_REPORT_WORK_CARD_BUDGETED_AMOUNT;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            dtFrom.SelectedDate = DateTime.Now.Date;
            dtTo.SelectedDate = DateTime.Now.AddDays(15).Date;
        }
    }
    protected void btnRunReport_Click(object sender, EventArgs e) {
        // Run the report.
        Telerik.Reporting.InstanceReportSource rs = new Telerik.Reporting.InstanceReportSource();
        rs.ReportDocument = new WorkCardBudgetedAmount(int.Parse(Profile.Customerid),//lstCustomerSelector.SelectedCustomerId),
            Profile.Customerdescription,//lstCustomerSelector.SelectedCustomerDescr, 
            int.Parse(Profile.Companydivisionid), Profile.Companydivisiondescription,
            dtFrom.SelectedDate.Value.Date, dtTo.SelectedDate.Value.Date, Profile.UserName);
        ReportViewer1.ReportSource = rs;
    }
}