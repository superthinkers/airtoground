﻿using System;

public partial class Reports_Job_TSCostDDReport : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_REPORT_MONTH_TS_COST_BY_JOB;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            dtFrom.SelectedDate = DateTime.Now.Date.AddDays(-30); // Past 30 days.
            dtTo.SelectedDate = DateTime.Now.Date;
        }
    }
    protected void btnRunReport_Click(object sender, EventArgs e) {
        // Run the report.
        TimeSheetCostDDLvl1 rpt = new TimeSheetCostDDLvl1();
        rpt.ReportParameters["companyId"].Value = Profile.Companydivisionid;
        rpt.ReportParameters["customerId"].Value = Profile.Customerid;// lstCustomerSelector.SelectedCustomerId;
        rpt.ReportParameters["dtFromDt"].Value = dtFrom.SelectedDate;
        rpt.ReportParameters["dtToDt"].Value = dtTo.SelectedDate;
        Telerik.Reporting.InstanceReportSource rs = new Telerik.Reporting.InstanceReportSource();
        rs.ReportDocument = rpt;
        ReportViewer1.ReportSource = rs;
    }
}