﻿using System;

public partial class Reports_EndOfDayRapSheetSummaryReport : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_REPORT_DAILY_WRAP_UP_SUM;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            dtFrom.SelectedDate = DateTime.Now.Date;
            dtTo.SelectedDate = DateTime.Now.Date;
        }
    }
    protected void btnRunReport_Click(object sender, EventArgs e) {
        // Run the report.
        Telerik.Reporting.InstanceReportSource rs = new Telerik.Reporting.InstanceReportSource();
        rs.ReportDocument = new EndOfDayRapSheetSummary(int.Parse(Profile.Customerid),//lstCustomerSelector.SelectedCustomerId),
            Profile.Customerdescription,//lstCustomerSelector.SelectedCustomerDescr,
            dtFrom.SelectedDate.Value.Date,
            dtTo.SelectedDate.Value.Date, Profile.UserName);
        ReportViewer1.ReportSource = rs;
    }
}