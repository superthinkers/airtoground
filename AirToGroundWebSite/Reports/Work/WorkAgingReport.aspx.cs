﻿using System;
using System.Linq;

public partial class Reports_WorkAgingReport : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_REPORT_WORK_AGING;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
            //m.IsActive = true;
            //dtFrom.SelectedDate = DateTime.Now.Date;
            //dtTo.SelectedDate = DateTime.Now.Date;
        }
    }
    protected void btnRunReport_Click(object sender, EventArgs e) {
        // Run the report.
        //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
        //m.IsActive = true; 
        Telerik.Reporting.InstanceReportSource rs = new Telerik.Reporting.InstanceReportSource();
        rs.ReportDocument = new WorkAging(int.Parse(Profile.Customerid),//lstCustomerSelector.SelectedCustomerId),
            Profile.Customerdescription,//lstCustomerSelector.SelectedCustomerDescr,
            int.Parse(lstServiceCategories.SelectedValue)/*,
            dtFrom.SelectedDate.Value.Date,
            dtTo.SelectedDate.Value.Date*/, Profile.UserName);
        ReportViewer1.ReportSource = rs;
    }
}