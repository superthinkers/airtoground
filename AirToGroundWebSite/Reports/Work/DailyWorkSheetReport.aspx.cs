﻿using System;

public partial class Reports_DailyWorkSheetReport : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_REPORT_DAILY_WORK_SHEET;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            dtFrom.SelectedDate = DateTime.Now.Date;
            dtTo.SelectedDate = DateTime.Now.Date;
        }
    }
    protected void btnRunReport_Click(object sender, EventArgs e) {
        // Run the report.
        Telerik.Reporting.InstanceReportSource rs = new Telerik.Reporting.InstanceReportSource();
        rs.ReportDocument = new DailyWorkSheet(int.Parse(Profile.Customerid),//lstCustomerSelector.SelectedCustomerId),
            Profile.Customerdescription,//lstCustomerSelector.SelectedCustomerDescr,
            int.Parse(lstServiceCategories.SelectedValue),
            dtFrom.SelectedDate.Value.Date,
            dtTo.SelectedDate.Value.Date, Profile.UserName);
        ReportViewer1.ReportSource = rs;
    }
}