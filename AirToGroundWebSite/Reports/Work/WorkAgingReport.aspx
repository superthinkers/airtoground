﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="WorkAgingReport.aspx.cs" Inherits="Reports_WorkAgingReport" Buffer="true"
    Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <devart:DbLinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Servicecategory" OrderBy="Description" TableName="Servicecategories"
        EnableInsert="true" EnableUpdate="true" EnableDelete="true">
    </devart:DbLinqDataSource>
    <br />
    <div class="div_container">
        <div class="div_header">Work Aging - Report Options</div>
        <%--<div style="position: relative; left: 35%; margin: 25px 0px 0px 0px; width: 30%">
            <atg:CustomerSelector ID="lstCustomerSelector" runat="server" />
        </div>--%>

        <br />
        <div style="position: relative; left: 28%; width: 60%">
            <asp:Label ID="lblServiceCategory" runat="server" SkinID="InfoLabel" Text="Service Category (Required)"></asp:Label>
            <telerik:RadComboBox ID="lstServiceCategories" runat="server" Width="300px" DataSourceID="LinqDataSource2"
                DataTextField="Description" DataValueField="Servicecategoryid">
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" CssClass="stdValidator"
                ErrorMessage="*" ControlToValidate="lstServiceCategories" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>

        <br />
        <%--<div style="position: relative; left: 20%; width: 60%">
            <asp:Label ID="lblFromDate" runat="server" SkinID="InfoLabel" Text="From Date (Required)"></asp:Label>
            <telerik:RadDatePicker ID="dtFrom" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="stdValidator" ErrorMessage="*"
                ControlToValidate="dtFrom" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:Label ID="lblToDate" runat="server" SkinID="InfoLabel" Text="To Date (Required)"></asp:Label>
            <telerik:RadDatePicker ID="dtTo" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="stdValidator" ErrorMessage="*"
                ControlToValidate="dtTo" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>

        <br />--%>
        <div style="position: relative; left: 40%; width: 20%; text-align: center">
            <asp:Button ID="btnRunReport" runat="server" Text="Run Report" OnClick="btnRunReport_Click" />
        </div>
        <br />
        <telerik:ReportViewer ID="ReportViewer1" runat="server" Height="500px" Width="100%">
        </telerik:ReportViewer>
    </div>
</asp:Content>

