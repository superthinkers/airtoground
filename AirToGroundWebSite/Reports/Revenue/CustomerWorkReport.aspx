﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="CustomerWorkReport.aspx.cs" Inherits="Reports_CustomerWorkReport" Buffer="true"
    Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <devart:DbLinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="" OrderBy="Tailnumber" Select="new (Description, Equipmentid, Tailnumber)"
        TableName="Equipments" Where="Customerid == Convert.ToInt32(@Customerid)">
        <WhereParameters>
            <asp:ProfileParameter Name="Customerid" PropertyName="Customerid" Type="Int32" />
        </WhereParameters>
    </devart:DbLinqDataSource>
    <script type="text/javascript">
        function ValidateDateRange(sender, e) {
            var dtFromDate = $find("<%= dtFrom.ClientID %>");
            var dtToDate = $find("<%= dtTo.ClientID %>");

            if ((dtFromDate.get_selectedDate() == null) && (dtToDate.get_selectedDate() != null)) {
                alert("Please select the From/End Date");
            } else if ((dtFromDate.get_selectedDate() > dtToDate.get_selectedDate()) && (dtToDate.get_selectedDate() != null)) {
                alert("The To/Start Date must be less than or equal to than the From/Begin Date");
                dtFromDate.set_selectedDate(dtToDate.get_selectedDate());
            }
            return false;
        }
    </script>
    <br />
    <div class="div_container">
        <div class="div_header">Work Revenue Detail Summary - Report Options</div>
        <%--<div style="position: relative; left: 35%; margin: 25px 0px 0px 0px; width: 30%">
            <atg:CustomerSelector ID="lstCustomerSelector" runat="server" AutoPostBack="true"
                OnSelectedIndexChanged="lstCustomerSelector_SelectedIndexChanged" />
        </div>--%>

        <br />
        <div style="position: relative; left: 30%; width: 50%">
            <asp:Label ID="lblEquipment" runat="server" SkinID="InfoLabel" Text="Tail Number (Optional)"></asp:Label>
            <telerik:RadComboBox ID="lstEquipment" runat="server" Width="300px" DataSourceID="LinqDataSource2"
                DataTextField="Tailnumber" DataValueField="Equipmentid" MarkFirstMatch="true" OnDataBound="lstEquipment_DataBound"
                HighlightTemplatedItems="true" EnableAutomaticLoadOnDemand="true" LoadingMessage="Loading...">
                <ItemTemplate>
                    <ul>
                        <li class="cbCol1">
                            <%# DataBinder.Eval(Container.DataItem, "Tailnumber") %></li>
                        <li class="cbCol2">
                            <%# DataBinder.Eval(Container.DataItem, "Description") %></li>
                    </ul>
                </ItemTemplate>
            </telerik:RadComboBox>
        </div>

        <br />
        <div style="position: relative; text-align: center; left: 15%; width: 70%">
            <asp:Label ID="lblFromDate" runat="server" SkinID="InfoLabel" Text="From Date (Required)"></asp:Label>
            <telerik:RadDatePicker ID="dtFrom" runat="server" Width="100px">
                <ClientEvents OnDateSelected="ValidateDateRange" />
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="stdValidator"
                ErrorMessage="*" ControlToValidate="dtFrom" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:Label ID="lblToDate" runat="server" SkinID="InfoLabel" Text="To Date (Required)"></asp:Label>
            <telerik:RadDatePicker ID="dtTo" runat="server" Width="100px">
                <ClientEvents OnDateSelected="ValidateDateRange" />
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="stdValidator"
                ErrorMessage="*" ControlToValidate="dtTo" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>

        <br />
        <div style="position: relative; left: 40%; width: 20%; text-align: center">
            <asp:Button ID="btnRunReport" runat="server" Text="Run Report" OnClick="btnRunReport_Click" />
        </div>
        <br />
        <telerik:ReportViewer ID="ReportViewer1" runat="server" Height="450px" Width="100%">
        </telerik:ReportViewer>
    </div>
</asp:Content>

