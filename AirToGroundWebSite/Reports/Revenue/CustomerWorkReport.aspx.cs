﻿using System;
using System.Linq;
using Telerik.Web.UI;

public partial class Reports_CustomerWorkReport : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_REPORT_WORK_REVENUE;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            dtFrom.SelectedDate = DateTime.Now.Date;
            dtTo.SelectedDate = DateTime.Now.AddDays(120).Date;
        }
    }
    protected void lstEquipment_DataBound(object sender, EventArgs e) {
        RadComboBox lst = (RadComboBox)sender;
        if (lst.Items.Count > 0) {
            if (lst.Items[0].Value != "") {
                RadComboBoxItem item = new RadComboBoxItem("", "");
                lst.Items.Insert(0, item);
            }
        }
    }
    protected void lstCustomerSelector_SelectedIndexChanged(object sender, EventArgs e) {
        lstEquipment.Items.Clear();
        lstEquipment.Text = "";
    }
    protected void btnRunReport_Click(object sender, EventArgs e) {
        // Run the report.
        if (lstEquipment.SelectedValue != "") {
            Telerik.Reporting.InstanceReportSource rs = new Telerik.Reporting.InstanceReportSource();
            rs.ReportDocument = new CustomerWork(int.Parse(Profile.Customerid),//lstCustomerSelector.SelectedCustomerId),
                Profile.Customerdescription,//lstCustomerSelector.SelectedCustomerDescr,
                int.Parse(lstEquipment.SelectedValue),
                dtFrom.SelectedDate.Value.Date,
                dtTo.SelectedDate.Value.Date, Profile.UserName);
            ReportViewer1.ReportSource = rs;
        } else {
            Telerik.Reporting.InstanceReportSource rs = new Telerik.Reporting.InstanceReportSource();
            rs.ReportDocument = new CustomerWork(int.Parse(Profile.Customerid),//lstCustomerSelector.SelectedCustomerId),
                Profile.Customerdescription,//lstCustomerSelector.SelectedCustomerDescr,
                int.Parse("0"),
                dtFrom.SelectedDate.Value.Date,
                dtTo.SelectedDate.Value.Date, Profile.UserName);
            ReportViewer1.ReportSource = rs;
        }
    }
}