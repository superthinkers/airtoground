﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="RecentServicePerformedReport.aspx.cs" Inherits="Reports_RecentServicePerformedReport"
    Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <script type="text/javascript">
        function ValidateDateRange(sender, e) {
            //var dtFromDate = $find("< %= dtFrom.ClientID %>");
            //var dtToDate = $find("< %= dtTo.ClientID %>");

            if ((dtFromDate.get_selectedDate() == null) && (dtToDate.get_selectedDate() != null)) {
                alert("Please select the From/End Date");
            } else if ((dtFromDate.get_selectedDate() > dtToDate.get_selectedDate()) && (dtToDate.get_selectedDate() != null)) {
                alert("The To/Start Date must be less than or equal to than the From/Begin Date");
                dtFromDate.set_selectedDate(dtToDate.get_selectedDate());
            }
            return false;
        }
    </script>
    <br />
    <div class="div_container">
        <div class="div_header">Recent Service Performed Report</div>
        <!-- Services -->
        <div style="position: relative; left: 25%; width: 49%;">
            <br />
            <%--<div style="text-align: left !important; position: relative; float: left; width: 99%;">
                <br />
                <asp:Label ID="lblFromDate" runat="server" SkinID="InfoLabel" Text="From Date (Required)"></asp:Label>
                <telerik:RadDatePicker ID="dtFrom" runat="server" Width="100px">
                    <ClientEvents OnDateSelected="ValidateDateRange" />
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="stdValidator"
                    ErrorMessage="*" ControlToValidate="dtFrom" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:Label ID="lblToDate" runat="server" SkinID="InfoLabel" Text="To Date (Required)"></asp:Label>
                <telerik:RadDatePicker ID="dtTo" runat="server" Width="100px">
                    <ClientEvents OnDateSelected="ValidateDateRange" />
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="stdValidator"
                    ErrorMessage="*" ControlToValidate="dtTo" Display="Dynamic"></asp:RequiredFieldValidator>
                <br />
                <br />
            </div>--%>
            <div style="text-align: left !important; position: relative; float: left; width: 50%;">
                <asp:Label ID="lblHeader1" runat="server" SkinID="InfoLabel" Text="Aircraft Types" Width="49%"></asp:Label>
                <asp:Panel ID="pnlScroll1" runat="server" BorderWidth="0" ScrollBars="Auto" Width="100%"
                    Height="101px">
                    <telerik:RadListBox ID="lstCustEqTypes" runat="server" DataKeyField="Equipmenttypeid"
                        BorderWidth="0" BorderColor="Transparent"
                        DataValueField="Equipmenttypeid" DataTextField="Description" CssClass="lblSmall"
                        CheckBoxes="true"
                        SelectionMode="Multiple" Width="98%" EnableViewState="true">
                    </telerik:RadListBox>
                </asp:Panel>
            </div>
            <div style="text-align: left !important; position: relative; float: left; width: 49%;">
                <asp:Label ID="lblHeader2" runat="server" SkinID="InfoLabel" Text="Services"></asp:Label>
                <asp:Panel ID="pnlScroll2" runat="server" BorderWidth="0" ScrollBars="Auto" Width="100%"
                    Height="101px">
                    <telerik:RadListBox ID="lstCustServices" runat="server" DataKeyField="Serviceid"
                        BorderWidth="0" BorderColor="Transparent"
                        DataValueField="Serviceid" DataTextField="Description" CssClass="lblSmall" CheckBoxes="true"
                        SelectionMode="Multiple" Width="98%" EnableViewState="true">
                    </telerik:RadListBox>
                </asp:Panel>
            </div>
            <div style="text-align: left !important; position: relative; float: left; width: 99%;">
                <br />
                <asp:RadioButton ID="btnSort1" runat="server" Checked="true" Text="Sort By Aircraft Type" GroupName="SORT" />
                <asp:RadioButton ID="btnSort2" runat="server" Text="Sort By Tail number" GroupName="SORT" />
            </div>
            <div style="text-align: left !important; position: relative; float: left; width: 99%;">
                <br />
                <asp:Button ID="btnRunReport" runat="server" Text="Run Report" OnClick="btnRunReport_Click" />
                <br />
                <br />
            </div>
        </div>
    </div>
    <!-- Services -->
    <div style="position: relative; left: 10%; float: left; width: 80%;">
        <telerik:ReportViewer ID="ReportViewer1" runat="server" Height="1150px" Width="100%">
        </telerik:ReportViewer>
    </div>
</asp:Content>

