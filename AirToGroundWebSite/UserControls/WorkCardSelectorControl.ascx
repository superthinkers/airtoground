﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WorkCardSelectorControl.ascx.cs" Inherits="UserControls_WorkCardSelectorControl" %>

<%--<telerik:RadScriptBlock ID="WorkCardRadCodeBlock1" runat="server">
    <script type="text/javascript">
        function WorkCardOnClientSelectedIndexChanged(sender, args) {
            // Don't need this anymore....Managed through ajax manager.
            //__doPostBack('WorkCardRadComboBox1', '');
        }
    </script>
</telerik:RadScriptBlock>--%>
<table style="width: 100%; border-collapse: collapse; border: 0px; text-align: left;">
    <tr>
        <td style="width: 90%">
            <telerik:RadComboBox ID="WorkCardRadComboBox1" Width="100%" runat="server" MarkFirstMatch="true"
                DataTextField="Cardnumber" DataValueField="Description" AutoPostBack="true"
                AllowCustomText="true" ShowToggleImage="false" ExpandAnimation-Type="None" Filter="StartsWith"
                IsCaseSensitive="false" EnableTextSelection="true" AppendDataBoundItems="false"
                HighlightTemplatedItems="true" EnableLoadOnDemand="true" CollapseAnimation-Type="None"
                DropDownWidth="300%" CausesValidation="false">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text=" " Value=" "></telerik:RadComboBoxItem>
                </Items>
                <HeaderTemplate>
                    <table width="100%">
                        <tr style="background-color: Silver; border: 1px solid Black">
                            <td align="left" style="width: 33%">Card #
                            </td>
                            <td align="left" style="width: 67%">Description
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table width="100%">
                        <tr>
                            <td style="width: 33%">
                                <%# Eval("Cardnumber") %>
                            </td>
                            <td style="width: 67%">
                                <%# Eval("Description") %>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </telerik:RadComboBox>
        </td>
        <td align="left">
            <asp:RequiredFieldValidator ID="reqWorkCardNumber1" runat="server" CssClass="stdValidator"
                Enabled="true" ControlToValidate="WorkCardRadComboBox1" Display="Static"
                ErrorMessage="*" SetFocusOnError="true">
            </asp:RequiredFieldValidator>
        </td>
    </tr>
</table>
