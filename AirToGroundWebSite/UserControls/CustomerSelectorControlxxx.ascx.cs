﻿using System;
using Telerik.Web.UI;
using ATGDB;

public partial class UserControls_CustomerSelectorControl : System.Web.UI.UserControl
{
    public bool VerticalLabelMode {
        get {
            if (ViewState["_CustomerSelectorControl_VerticalLabelMode"] != null) {
                string mode = ViewState["_CustomerSelectorControl_VerticalLabelMode"].ToString();
                return Convert.ToBoolean(mode);
            } else {
                return false;
            }
        }
        set {
            ViewState["_CustomerSelectorControl_VerticalLabelMode"] = value;
        } 
    }
    public string SelectedCustomerId {
        get {
            if (ViewState["_CustomerSelectorControl_CustomerId"] != null) {
                return ViewState["_CustomerSelectorControl_CustomerId"].ToString();
            } else {
                return "";
            }
        }
        set {
            ViewState["_CustomerSelectorControl_CustomerId"] = value;
        }
    }
    public string SelectedCustomerDescr {
        get {
            if (ViewState["_CustomerSelectorControl_CustomerDescr"] != null) {
                return ViewState["_CustomerSelectorControl_CustomerDescr"].ToString();
            } else {
                return "";
            }
        }
        set {
            ViewState["_CustomerSelectorControl_CustomerDescr"] = value;
        }
    }
    public bool AutoPostBack { 
        get {
            return lstCustomerSelector.AutoPostBack;    
        }

        set {
            lstCustomerSelector.AutoPostBack = value;
        }
    }
    public bool CausesValidation {
        get {
            return lstCustomerSelector.CausesValidation;
        }

        set {
            lstCustomerSelector.CausesValidation = value;
        }
    }
    public event RadComboBoxSelectedIndexChangedEventHandler OnSelectedIndexChanged = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            // UI Layout
            if (VerticalLabelMode == false) {
                lblCustomerSelector1.Visible = true;
                lblCustomerSelector1.Width = new System.Web.UI.WebControls.Unit(100);
                colCustomerSelector.Visible = true;
                lblCustomerSelector2.Visible = false;
            } else {
                lblCustomerSelector1.Visible = false;
                lblCustomerSelector1.Width = new System.Web.UI.WebControls.Unit(0);
                colCustomerSelector.Visible = false;
                lblCustomerSelector2.Visible = true;
            }

            // Load up the customers.
            var data = Customer.GetCustomersByUserid(Profile.Userid);
            lstCustomerSelector.DataSource = data;
            lstCustomerSelector.DataBind();

            if (string.IsNullOrEmpty(SelectedCustomerId)) {
                // Line up the default based on the user's profile.
                RadComboBoxItem item = lstCustomerSelector.FindItemByValue(Profile.Customerid);
                if (item != null) {
                    item.Selected = true;
                }
                SelectedCustomerId = Profile.Customerid;
                SelectedCustomerDescr = Profile.Customerdescription;
            } else {
                // Use the passed in value.
                RadComboBoxItem item = lstCustomerSelector.FindItemByValue(SelectedCustomerId);
                if (item != null) {
                    item.Selected = true;
                }
                SelectedCustomerDescr = item.Text;
            }
            // If this is a customer, just show a label.
            //if (ProviderUtils.IsUserInRole(Security.ROLE_CUSTOMER)) {
            //    lstCustomerSelector.Visible = false;
            //    CustomerSelectorRequiredFieldValidator.Enabled = false;
            //    txtCustomer.Visible = true;
            //    txtCustomer.Text = SelectedCustomerDescr;
            //}
        }
    }
    protected void lstCustomerSelector_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e) {
        // Set the selected customer.
        SelectedCustomerId = lstCustomerSelector.SelectedValue;
        SelectedCustomerDescr = lstCustomerSelector.SelectedItem.Text;
        if (OnSelectedIndexChanged != null) {
            OnSelectedIndexChanged(sender, e);
        }
    }
}