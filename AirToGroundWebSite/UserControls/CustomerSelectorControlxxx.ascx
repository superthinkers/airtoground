﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerSelectorControlxxx.ascx.cs" Inherits="UserControls_CustomerSelectorControl" %>
<table width="100%" style="border-collapse: collapse; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px">
    <tr>
        <td id="colCustomerSelector" runat="server" align="right" valign="middle">
            <div style="margin-right: 9px">
              <asp:Label ID="lblCustomerSelector1" runat="server" SkinID="InfoLabel"
                 Text="Customer&nbsp;(Required)"></asp:Label>
            </div>
        </td>
        <td align="left" valign="middle">
            <div id="lblCustomerSelector2" runat="server" class="lblInfo" visible="false">Customer (Required)</div>
            <telerik:RadComboBox ID="lstCustomerSelector" runat="server" Width="200px" 
                DataTextField="Description" DataValueField="Customerid" 
                OnSelectedIndexChanged="lstCustomerSelector_SelectedIndexChanged">
            </telerik:RadComboBox>
            <%--<asp:Label ID="txtCustomer" runat="server" SkinID="InfoLabelBold" Visible="false"></asp:Label>--%>
            <asp:RequiredFieldValidator ID="CustomerSelectorRequiredFieldValidator" runat="server"
                CssClass="stdValidator" ErrorMessage="<br />Required" ControlToValidate="lstCustomerSelector"
                Display="Dynamic">
            </asp:RequiredFieldValidator>
        </td>
    </tr>
</table>

