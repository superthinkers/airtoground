﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServiceDescriptionControl.ascx.cs" Inherits="UserControls_ServiceDescriptionControl" %>
<telerik:RadScriptBlock ID="RadCodeBlock999" runat="server">
    <script type="text/javascript">
        function openExDescr(svcId) {
            radopen("../Admin/ViewServiceExDescr.aspx?ServiceId=" + svcId, "RadWindow999");
        }
    </script>
</telerik:RadScriptBlock>
<table style="border-width: 0px; padding: 0px 0px 0px 0px; border-collapse: collapse; 
    background-color: transparent; line-height:10px; height:10px; width: 100%">
    <tr>
        <td style="border-width: 0px; text-align: left; width: 100%">
            <asp:Label ID="lblCustServiceDescr" runat="server" BackColor="Transparent" SkinID="InfoLabel" Text='<%# Eval("Servicedescr") %>'></asp:Label>
        </td>
        <td style="border-width: 0px; text-align: center; width: 10px">
            <a href="#" onclick='<%# "openExDescr(" + Eval("Serviceid").ToString() + "); return false;" %>'>
                <asp:Image ID="imgCustServiceExDescr" runat="server" SkinID="Info" BackColor="Transparent" 
                    ImageAlign="AbsMiddle" />
            </a>
        </td>
    </tr>
</table>
