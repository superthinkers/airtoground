﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using Telerik.Web.UI;

public partial class UserControls_WorkCardSelectorControl : System.Web.UI.UserControl
{
    public bool Enabled {
        get {
            return WorkCardRadComboBox1.Enabled;
        }
        set {
            WorkCardRadComboBox1.Enabled = value;
        }
    }
    public string Text {
        get {
            return WorkCardRadComboBox1.Text;
        }
        set {
            WorkCardRadComboBox1.Text = value;
        }
    }
    public string Width {
        get {
            return ViewState["Width"] == null ? "88%" : ViewState["Width"].ToString();
        }
        set {
            WorkCardRadComboBox1.Width = new System.Web.UI.WebControls.Unit(ViewState["Width"] == null ? "88%" : ViewState["Width"].ToString());
        }
    }
    public string ValidationGroup {
        get {
            return ViewState["ValidationGroup"] == null ? "" : ViewState["ValidationGroup"].ToString();
        }
        set {
            WorkCardRadComboBox1.ValidationGroup = ViewState["ValidationGroup"] == null ? "" : ViewState["ValidationGroup"].ToString();
            reqWorkCardNumber1.ValidationGroup = WorkCardRadComboBox1.ValidationGroup;
        }
    }
    public bool IsValid {
        get {
            return reqWorkCardNumber1.IsValid;
        }
        set {
            reqWorkCardNumber1.IsValid = value;
        }
    }
    public bool ReqEnabled {
        get {
            return reqWorkCardNumber1.Enabled;
        }
        set {
            reqWorkCardNumber1.Enabled = value;
        }
    }
    public delegate void OnTextChangedHandler(object sender, EventArgs e);
    public event OnTextChangedHandler OnTextChanged;

    protected void Page_Load(object sender, EventArgs e) {
        RadAjaxManager mng = PageUtils.FindControlRecursive(Page, "RadAjaxManager1") as RadAjaxManager;
        mng.AjaxSettings.AddAjaxSetting(WorkCardRadComboBox1, WorkCardRadComboBox1);
        WorkCardRadComboBox1.ItemsRequested += new RadComboBoxItemsRequestedEventHandler(this.list_ItemsRequested);
        WorkCardRadComboBox1.SelectedIndexChanged += new Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventHandler(this.list_SelectedIndexChanged);
        WorkCardRadComboBox1.TextChanged += new EventHandler(this.list_TextChanged);
        WorkCardRadComboBox1.Width = new System.Web.UI.WebControls.Unit("90%");
    }                           
    public override void Focus() {
        WorkCardRadComboBox1.Focus();    
    }
    protected void list_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
        if (OnTextChanged != null) {
            OnTextChanged(sender, e);
        }
    }
    protected void list_TextChanged(object sender, EventArgs e) {
        if (OnTextChanged != null) {
            OnTextChanged(sender, e);
        }
    }
    private void list_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e) {
        if (e.Text != "") {
            WorkCardRadComboBox1.DataSource = GetDataSet(e.Text.Trim().ToUpper());
        } else {
            DataSet ds = new DataSet();
            DataTable dt = ds.Tables.Add("WorkCardTable");
            dt.Columns.Add("Cardnumber");
            dt.Columns.Add("Description");
            WorkCardRadComboBox1.DataSource = ds;
        }
        WorkCardRadComboBox1.DataBind();
    }
    class MyComparer : IEqualityComparer<ATGDB.Workcard> {
        public bool Equals(ATGDB.Workcard item1, ATGDB.Workcard item2) {
            return ((item1 != null) && (item2 != null) &&
                   (item1.Cardnumber == item2.Cardnumber));
        }
        public int GetHashCode(ATGDB.Workcard item) {
            return item.Cardnumber.GetHashCode();
        }
    }
    public static DataSet GetDataSet(string wcNum) {
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        MyComparer compare = new MyComparer();
        var data = (from wc in dc.Workcards
                    where wc.Completed == false
                    where wc.Cardnumber.ToUpper().StartsWith(wcNum)
                    orderby wc.Cardnumber ascending
                    select wc).ToList().Distinct(compare);
        DataSet ds = new DataSet();
        DataTable dt = ds.Tables.Add("WorkCardTable");
        dt.Columns.Add("Cardnumber");
        dt.Columns.Add("Description");
        foreach (ATGDB.Workcard wc in data) {
            dt.Rows.Add(new Object[] { wc.Cardnumber, wc.Description });
        }
        return ds;
    }
}