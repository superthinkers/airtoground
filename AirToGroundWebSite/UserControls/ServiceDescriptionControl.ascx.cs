﻿using System;
using Telerik.Web.UI;

public partial class UserControls_ServiceDescriptionControl : System.Web.UI.UserControl
{
    public string Text { 
        get {
            return lblCustServiceDescr.Text;
        }

        set {
            lblCustServiceDescr.Text = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Page.IsPostBack) {
            RadWindowManager man = (RadWindowManager)PageUtils.FindControlRecursive(Page, "RadWindowManager1");
            if (man != null) {
                bool found = false;
                foreach (RadWindow rw in man.Windows) {
                    if (rw.ID == "RadWindow999") {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    RadWindow win = new RadWindow();
                    win.ID = "RadWindow999";
                    win.VisibleStatusbar = false;
                    win.VisibleTitlebar = false;
                    win.EnableShadow = false;
                    win.ReloadOnShow = true;
                    win.Title = "Edit Service Extended Description";
                    win.Width = new System.Web.UI.WebControls.Unit(850);
                    win.MinWidth = new System.Web.UI.WebControls.Unit(850);
                    win.Height = new System.Web.UI.WebControls.Unit(450);
                    win.MinHeight = new System.Web.UI.WebControls.Unit(450);
                    win.Modal = true;
                    win.AutoSize = false;
                    man.Windows.Add(win);
                }
            //}
        }
    }
}