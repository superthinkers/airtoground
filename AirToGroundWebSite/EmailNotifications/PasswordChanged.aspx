﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PasswordChanged.aspx.cs" Inherits="EmailNotifications_PasswordChanged" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="cid:txtStyle" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="div_main">
            <div style="width: 100%">
                <div style="height: 76px; top: 0px">
                    <img src="cid:imgSplash" id="imgSplash" runat="server" alt="Air to Ground" style="height: 76px;
                        top: 0px" />
                </div>
                <br />
                <br />
                <table class="div_container" style="width: 300px">
                    <tr>
                        <td>
                            <div class="div_header">Administrative Alert!</div>
                            <br />
                            <asp:Label ID="lblTitle" runat="server" CssClass="lblTitle" Text="Your Password has Changed"></asp:Label>
                            <br />
                            <br />
                            <asp:Label ID="lblUser" runat="server" CssClass="lblInfoBold" Text=""></asp:Label>
                            <asp:Label ID="lblPassword" runat="server" CssClass="lblInfoBold" BackColor="Purple" Font-Italic="true" Text=""></asp:Label>
                            <br />
                            <asp:Label ID="lblDateTime" runat="server" CssClass="lblSubscript" Text=""></asp:Label>
                            <br />
                            <br />
                            <asp:HyperLink ID="lnkATG" runat="server" Text="Login" NavigateUrl="https://192.168.1.166"></asp:HyperLink>
                            <br />
                            <br />
                            <br />
                        </td>
                    </tr>
                </table>            
            </div>
        </div>
    </form>
</body>
</html>
