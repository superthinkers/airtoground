﻿using System;
using System.Linq;
using Telerik.Web.UI;

public partial class Help_RequestForService : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_HELP_RFS;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        if (Profile != null) {
            Page.Theme = Profile.SiteTheme;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            lblUserName.Text = Profile.UserName;
            lblEmail.Text = System.Web.Security.Membership.GetUser(Profile.UserName).Email;
            lblDate.Text = DateTime.Now.ToLongDateString();
            lblStatus.Visible = false;

            // Hide the menu if no profile...some serious error occurred and session has dropped.
            if (Profile == null) {
                RadMenu menu = (RadMenu)PageUtils.FindControlRecursive(Page.Master, "mnuMain");
                if (menu != null) {
                    menu.Visible = false;
                }
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e) {
        string s = "";
        s += "User: " + lblUserName.Text + "<br/>";
        s += "Phone: " + txtPhone.Text + "<br/>";
        s += "Email: " + lblEmail.Text + "<br/>";
        s += "Date: " + lblDate.Text + "<br/>";
        s += "Request: " + lstRequestType.Text + "<br/>";
        s += "Svc Date: " + dtSvcDate.SelectedDate.Value.ToShortDateString() + "<br/>";
        s += "Tail: " + txtTailNumber.Text + "<br/>";
        s += "Notes: " + txtNotes.Text + "<br/>";
        string emailAddr = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("CUSTOMER_SUPPORT").ToString();
        EmailUtils.SendSimpleHtmlMail(Page.Server, emailAddr, "ALERT: A Customer Request is Being Made", s);
        lstRequestType.ClearSelection();
        dtSvcDate.Clear();
        txtTailNumber.Text = "";
        txtNotes.Text = "";
        lblStatus.Visible = true;
    }
}