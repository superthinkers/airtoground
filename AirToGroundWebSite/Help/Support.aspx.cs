﻿using System;
using System.Linq;
using Telerik.Web.UI;

public partial class Help_Support : System.Web.UI.Page//, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_HELP_SUPPORT;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        if (Profile != null) {
            Page.Theme = Profile.SiteTheme;
        } else {
            Page.Theme = "Corp";
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            lblUserName.Text = Profile.UserName;
            lblDate.Text = DateTime.Now.ToLongDateString();
            lblStatus.Visible = false;

            // Hide the menu if no profile...some serious error occurred and session has dropped.
            if (Profile == null) {
                RadMenu menu = (RadMenu)PageUtils.FindControlRecursive(Page.Master, "mnuMain");
                if (menu != null) {
                    menu.Visible = false;
                }
            }

            // GENERAL EXCEPTIONS TRAPPED HERE
            //<error statusCode="401" redirect="~/Help/Support.aspx?ErrorCode=401" /> <!-- Unauthorized -->
            //<error statusCode="403" redirect="~/Help/Support.aspx?ErrorCode=403" /> <!-- Forbidden -->

            string err = "";
            if ((System.Web.HttpContext.Current.Session["SERVER_LAST_ERROR"] != null) &&
                (System.Web.HttpContext.Current.Session["SERVER_LAST_ERROR"].ToString() != "")) {
                err = System.Web.HttpContext.Current.Session["SERVER_LAST_ERROR"].ToString();
                // Show the error labels.
                divError1.Style.Clear();
                divError1.Style.Add("display", "inline");
                divError2.Style.Clear();
                divError2.Style.Add("display", "none");
                txtScreen.Text = Request.Url.PathAndQuery;
                txtError.Text = err;
                // Clear the error.
                System.Web.HttpContext.Current.Session.Add("SERVER_LAST_ERROR", "");
            } else {
                divError1.Style.Clear();
                divError1.Style.Add("display", "none");
                divError2.Style.Clear();
                divError2.Style.Add("text-align", "left");
                divError2.Style.Add("position", "relative");
                divError2.Style.Add("left", "20%");
                divError2.Style.Add("width", "60%");
            }
        }
    }
    protected void btnSubmit_OnClick(object sender, EventArgs e) {
        if (RadCaptcha1.IsValid == true) {
            string s = "";
            s += "User: " + lblUserName.Text + "<br/>";
            s += "Phone: " + txtPhone.Text + "<br/>";
            s += "Date: " + lblDate.Text + "<br/>";
            s += "Screen: " + txtScreen.Text + "<br/>";
            s += "Error: " + txtError.Text + "<br/>";
            s += "Task: " + txtTask + "<br/>";
            string emailAddr = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("WEBADMIN").ToString();
            EmailUtils.SendSimpleHtmlMail(Page.Server, emailAddr, "A Website Error has been Submitted", s);
            txtPhone.Enabled = false;
            txtScreen.Enabled = false;
            txtError.Enabled = false;
            txtTask.Enabled = false;
            btnSubmit.Enabled = false;
            lblStatus.Visible = true;
            RadCaptcha1.Enabled = false;
        } else {
            txtPhone.Enabled = true;
            txtScreen.Enabled = true;
            txtError.Enabled = true;
            txtTask.Enabled = true;
            btnSubmit.Enabled = true;
            lblStatus.Visible = false;
            RadCaptcha1.Enabled = true;
        }
    }
}