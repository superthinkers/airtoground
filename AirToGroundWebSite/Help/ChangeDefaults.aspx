﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="ChangeDefaults.aspx.cs" Inherits="Help_ChangeDefaults" Buffer="true"
    Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Change Your Default Customer / Company Division">
        </asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div>
                <div class="div_header">Select a Default Customer/Company Division</div>
                <br />
                <asp:DropDownList ID="lstCustDivision" runat="server" DataTextField="Description" DataValueField="Id">
                </asp:DropDownList>
                <br />
                <br />
                <asp:Button ID="btnChangeDivision" runat="server" Text="Change" OnClick="btnChangeDivision_OnClick" />
                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>

