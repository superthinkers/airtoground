﻿using System;
using System.Web.UI.WebControls;
using ATGDB;

public partial class Help_ChangeDefaults : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_HELP_CHANGE_DEFAULTS;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            lstCustDivision.DataSource = Companydivision.GetDivisionsAndCustomersByUser(Profile.Userid);
            lstCustDivision.DataBind();
            // Line up on current value.
            lstCustDivision.SelectedValue = Profile.Companydivisionid + ":" + Profile.Customerid;
        }
    }
    protected void btnChangeDivision_OnClick(object sender, EventArgs e) {
        // Get the division.
        ATGDB.Companydivision div = Companydivision.GetCompanyDivisionById(lstCustDivision.SelectedValue.Substring(0, lstCustDivision.SelectedValue.IndexOf(":")));
        // Get the customer.
        ATGDB.Customer cust = Customer.GetCustomerById(lstCustDivision.SelectedValue.Substring(lstCustDivision.SelectedValue.IndexOf(":") + 1,
            lstCustDivision.SelectedValue.Length - lstCustDivision.SelectedValue.IndexOf(":") - 1));
        // Update Profile.
        Profile.Companydivisionid = div.Companydivisionid.ToString();
        Profile.Companydivisiondescription = div.Description;
        Profile.Companydivisionlocation = div.Location;
        Profile.Customerid = cust.Customerid.ToString();
        Profile.Customerdescription = cust.Description;
        //Profile.SiteTheme = cust.Sitetheme;
        Profile.Save();
        // DEPRECATED CONCEPT.
        // Put Division in cache.
        //Cache["DIVISION"] = div;
        // Put Customer in cache.
        //Cache["CUSTOMER"] = cust;
        // Update screen label.
        Label lbl = (Label)PageUtils.FindControlRecursive(Page.Master, "lblDivision2");
        lbl.Text = Profile.Companydivisiondescription;
        Label lbl2 = (Label)PageUtils.FindControlRecursive(Page.Master, "lblCustomer2");
        lbl2.Text = Profile.Customerdescription;
        // Redirect.
        Response.Redirect("~/Home.aspx");
    }

}