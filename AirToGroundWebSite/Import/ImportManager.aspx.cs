﻿using System;
using System.Linq;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using ATGDB;

public partial class ImportManager : System.Web.UI.Page, IAuditTrailPage {
	public string GetAuditPageName() {
		return AuditUtils.IMPORT_MANAGER;	
	}	
	protected void Page_PreInit(object sender, EventArgs e) {
		Page.Theme = Profile.SiteTheme;
	}
	protected void Page_Load(object sender, EventArgs e) {
		if (!Page.IsPostBack) {
			DbLinqDataSource1.WhereParameters[0].DefaultValue = System.DateTime.Now.AddDays(-30).ToShortDateString();
			uploadValidator.IsValid = true;
			btnImport.Enabled = false;
			btnRead.Enabled = true;
			btnRollback.Enabled = false;
			btnRollforward.Enabled = false;


			//RadGrid1.Items[4].SelectableMode = GridItemSelectableMode.None;
			//RadGrid1.Items[5].SelectableMode = GridItemSelectableMode.None;
		}
	}
	private ImportInterface getImportInterface() {
		if (rdbSchedules.Enabled == true) {
			return ImportSchedule.getInstance();
		} else {
			return null;			
		}
	}
	protected void btnRead_OnClick(object sender, EventArgs e) {
		ImportInterface ii = getImportInterface();
		if (ii != null) {
			if (ctrlUpload.UploadedFiles.Count > 0) {
				uploadValidator.IsValid = true;
				RadGrid1.DataSource = ii.ReadFile(ctrlUpload);
				RadGrid1.DataBind();
			} else {
				uploadValidator.IsValid = false;
			}
		} else {
			throw new Exception("The import interface could not be found. Please contact technical support.");
		}
	}
	protected void btnImport_OnClick(object sender, EventArgs e) {
		ImportInterface ii = getImportInterface();
		if (ii != null) {
			if (RadGrid1.SelectedValues.Count > 0) {
				uploadValidator.IsValid = true;
				ii.Import(RadGrid1.SelectedValues);
			} else {
				uploadValidator.IsValid = false;
			}
		} else {
			throw new Exception("The import interface could not be found. Please contact technical support.");
		}
	}
	protected void btnRollback_OnClick(object sender, EventArgs e) {
		ImportInterface ii = getImportInterface();
		if (RadGrid2.SelectedValues.Count > 0) {
			uploadValidator.IsValid = true;
			ii.Rollback(RadGrid2.SelectedValues);
		} else {
			uploadValidator.IsValid = false;
		}
	}
	protected void btnRollforward_OnClick(object sender, EventArgs e) {
		ImportInterface ii = getImportInterface();
		if (RadGrid2.SelectedValues.Count > 0) {
			uploadValidator.IsValid = true;
			ii.Rollforward(RadGrid2.SelectedValues);
		} else {
			uploadValidator.IsValid = false;
		}
	}

	protected void lstPastUploads_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
		if (lstPastUploads.SelectedValue != "") {
			DbLinqDataSource2.DataBind();
			//if (RadGrid2.MasterTableView.Items.Count > 0) {
				RadGrid1.MasterTableView.DataSource = null;
				RadGrid1.MasterTableView.DataBind();
				btnImport.Enabled = false;
				btnRead.Enabled = false;
				btnRollback.Enabled = true;
				btnRollforward.Enabled = true;
			//}
		}
	}

	protected void ctrlUpload_FileUploaded(object sender, FileUploadedEventArgs e) {
		throw new Exception("fred");
	}
}