﻿<%@ Page Title="Import Manager" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
	EnableEventValidation="false" CodeFile="ImportManager.aspx.cs" Inherits="ImportManager"
	Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
	<devart:DbLinqDataSource ID="DbLinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
		EntityTypeName="ATGDB.Importheaders" OrderBy="Importheaderid"
		TableName="Importheaders"
		Where='Uploaddate > Convert.ToDateTime(@Uploaddate)'>
		<WhereParameters>
			<asp:Parameter Name="Uploaddate" />
		</WhereParameters>
	</devart:DbLinqDataSource>
	<devart:DbLinqDataSource ID="DbLinqDataSource2" runat="server" ContextTypeName="ATGDB.ATGDataContext"
		EntityTypeName="ATGDB.Importdetails" OrderBy="Importdetailid"
		TableName="Importdetails"
		Where='iif(@Importdetailid == "-1", true == true, Importdetailid == Convert.ToInt32(@Importdetailid))'>
		<WhereParameters>
			<asp:ControlParameter Name="Importdetailid" ControlID="lstPastUploads" Type="String" DefaultValue="-99" />
		</WhereParameters>
	</devart:DbLinqDataSource>
	<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
		<AjaxSettings>
			<%--<telerik:AjaxSetting AjaxControlID="ctrlUpload">
				<UpdatedControls>
					<telerik:AjaxUpdatedControl ControlID="btnRead" />
					<telerik:AjaxUpdatedControl ControlID="btnImport" />
					<telerik:AjaxUpdatedControl ControlID="btnRollback" />
					<telerik:AjaxUpdatedControl ControlID="btnRollforward" />
				</UpdatedControls>
			</telerik:AjaxSetting>--%>
			<telerik:AjaxSetting AjaxControlID="RadGrid1">
				<UpdatedControls>
					<telerik:AjaxUpdatedControl ControlID="RadGrid1" />
				</UpdatedControls>
			</telerik:AjaxSetting>
			<telerik:AjaxSetting AjaxControlID="RadGrid2">
				<UpdatedControls>
					<telerik:AjaxUpdatedControl ControlID="RadGrid2" />
				</UpdatedControls>
			</telerik:AjaxSetting>
			<telerik:AjaxSetting AjaxControlID="btnRead">
				<UpdatedControls>
					<telerik:AjaxUpdatedControl ControlID="RadGrid1" />
				</UpdatedControls>
			</telerik:AjaxSetting>
			<telerik:AjaxSetting AjaxControlID="btnImport">
				<UpdatedControls>
					<telerik:AjaxUpdatedControl ControlID="RadGrid2" />
				</UpdatedControls>
			</telerik:AjaxSetting>
			<telerik:AjaxSetting AjaxControlID="btnRollback">
				<UpdatedControls>
					<telerik:AjaxUpdatedControl ControlID="RadGrid2" />
				</UpdatedControls>
			</telerik:AjaxSetting>
			<telerik:AjaxSetting AjaxControlID="btnRollforward">
				<UpdatedControls>
					<telerik:AjaxUpdatedControl ControlID="RadGrid2" />
				</UpdatedControls>
			</telerik:AjaxSetting>

		</AjaxSettings>
	</telerik:RadAjaxManager>
	<telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
		ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
		Overlay="true" ReloadOnShow="false" runat="server" EnableShadow="false"
		Modal="true" VisibleTitlebar="false" Width="250" Height="25" EnableViewState="false">
	</telerik:RadWindowManager>
	<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="true" Skin="Office2007">
	</telerik:RadAjaxLoadingPanel>
	<telerik:RadScriptBlock runat="server">
		<script type="text/javascript">
			//<![CDATA[
			function onClientFileUploaded(sender, args) {
				// Clear grid data
				var view = $find("<%= RadGrid1.ClientID %>").get_masterTableView();
				if (view) {
					view.set_dataSource([]);
					view.dataBind();
				}
				view = $find("<%= RadGrid2.ClientID %>").get_masterTableView();
				if (view) {
					view.set_dataSource([]);
					view.dataBind();
				}

				// Set header combo to item 0
				var combo = $find("<%= lstPastUploads.ClientID %>");
				if (combo) {
					combo.clearSelection();
				}

				// Enable/Disable
				var btnRead = $find("<%=btnRead.ClientID %>");
               	var btnImport = $find("<%=btnImport.ClientID %>");
				var btnRollback = $find("<%=btnRollback.ClientID %>");
				var btnRollforward = $find("<%=btnRollforward.ClientID %>");
				// Enable button a few seconds delayed to there aren't problems with the asynchronous upload control.
				setTimeout(function () {
					btnRead.set_enabled(true);
				}, 1000);
				btnImport.set_enabled(false);
				btnRollback.set_enabled(false);
				btnRollforward.set_enabled(false);
			}
			function rg1RowSelected(sender, args) {
				var btnImport = $find("<%=btnImport.ClientID %>");
				btnImport.set_enabled(true);
			}
			function rg1RowDeselected(sender, args) {
				if (sender.get_selectedItems().length === 0) {
					var btnImport = $find("<%=btnImport.ClientID %>");
					btnImport.set_enabled(false);
				}
			}
            //]]>
		</script>
	</telerik:RadScriptBlock>
	<div id="div_main">
		<asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Import Manager"></asp:Label>
		<br />
		<br />
		<div class="div_container">
			<div class="div_header">Import Manager</div>
			<div style="margin: 10px 10px 10px 10px">
				<table style="width: 99%;border-collapse:collapse;">
					<tr>
						<td>
							<fieldset style="width: 99%">
								<legend>Import Setup</legend>
								<table style="width: 100%;">
									<tr>
										<td rowspan="4" style="width: 350px">
											<ol style="font-size: 10px; color: green; text-align: left;">
												<li>Select a file to upload</li>
												<li>Click 'Read File Contents' to load the file for viewing</li>
												<li>Select the records you wish to import</li>
												<li>Import the files"</li>
											</ol>
											<p>- OR -</p>
											<ol style="font-size: 10px; color: green; text-align: left;">
												<li>Select a "Past Upload" to Roll backward or foward the imported records</li>
											</ol>
										</td>
									</tr>
									<tr>
										<td style="width: 100px; padding:10px;font-weight:bold;vertical-align:top;">Select an import file or drop an import file here:
										</td>
										<td style="background-color: khaki;padding:10px;">
                                            <telerik:RadAsyncUpload ID="ctrlUpload" runat="server" 
												TargetFolder="~/Import/Upload" TemporaryFolder="~/Import/UploadTemp" 
												EnableInlineProgress="false"	 
												MaxFileInputsCount="1" ChunkSize="1048576" OnClientFileUploaded="onClientFileUploaded">
                                            </telerik:RadAsyncUpload>
											<telerik:RadProgressManager runat="server" ID="RadProgressManager1" />
											<script type="text/javascript">
												//<![CDATA[
												function onClientProgressBarUpdating(progressArea, args) {
													//progressArea.updateVerticalProgressBar(args.get_progressBarElement(), args.get_progressValue());
													//args.set_cancel(true);
												}
												//]]>
											</script>
											<telerik:RadProgressArea runat="server" ID="RadProgressArea1" Width="100%"
												OnClientProgressBarUpdating="onClientProgressBarUpdating">
												<ProgressTemplate>
													<div class="rpaInner">
														<!-- THIS IS THE PROGRESS INDICATOR -->
														<div class="qsf-demo-canvas"></div>
														<div class="rpaBody">
															<table style="width: 100%">
																<tr>
																	<td style="width:30%">
																		<label class="rpaStatFirst">Total Progress Percent</label>
																		<label class="rpaStatFirst"><span runat="server" id="PrimaryPercent"></span>%</label>
																	</td>
																	<td>
																		<label>Selected Files Count:</label>
																		<label><span runat="server" id="SecondaryTotal"></span></label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<label>Total Progress:</label>
																		<label><span runat="server" id="PrimaryValue"></span></label>
																	</td>
																	<td>
																		<label>Current File Name:</label>
																		<label><span runat="server" id="CurrentOperation"></span></label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<label>Files Count:</label>
																		<label><span runat="server" id="SecondaryValue"></span></label>
																	</td>
																	<td>
																		<label>Time Elapsed:</label>
																		<label><span runat="server" id="TimeElapsed"></span></label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<label>Files Count Percent:</label>
																		<label><span runat="server" id="SecondaryPercent"></span></label>
																	</td>
																	<td>
																		<label>Time Estimated:</label>
																		<label><span runat="server" id="TimeEstimated"></span></label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<label>Request Size:</label>
																		<label><span runat="server" id="PrimaryTotal"></span></label>
																	</td>
																	<td>
																		<label>Transfer Speed:</label>
																		<label><span runat="server" id="Speed"></span></label>
																	</td>
																</tr>
															</table>
														</div>
													</div>
												</ProgressTemplate>
											</telerik:RadProgressArea>
											<asp:CustomValidator ID="uploadValidator" runat="server" CssClass="stdValidator" Display="Dynamic" ErrorMessage="<br/>Please Select a File to Read"></asp:CustomValidator>
										</td>
									</tr>
									<tr>
										<td style="width: 100px;padding: 10px; font-weight: bold;">Upload Type:
										</td>
										<td>
											<asp:RadioButton ID="rdbSchedules" runat="server" Checked="true" Text="Schedules" ToolTip="Will Upload File Contents to Schedules" />
										</td>
									</tr>
									<tr>
										<td style="text-align: center;">
											<telerik:RadButton ID="btnRead" runat="server" Text="Read File Contents" OnClick="btnRead_OnClick" />
										</td>
									</tr>
								</table>
							</fieldset>
						</td>
					</tr>
				</table>
				<br />
				<div class="lblInfoBold">File Contents:</div>
				<!-- Import Results -->
				<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
					<telerik:RadGrid ID="RadGrid1" GridLines="None" Width="100%" runat="server"
						AllowAutomaticDeletes="false" AllowAutomaticInserts="false" AllowAutomaticUpdates="false"
						AllowMultiRowEdit="false" AllowMultiRowSelection="true" ShowGroupPanel="false"
						AllowPaging="false" AllowSorting="true" AutoGenerateColumns="true" ShowHeader="true">
						<MasterTableView Width="100%" CommandItemDisplay="None"
							AllowAutomaticUpdates="false" AllowAutomaticInserts="false" AllowAutomaticDeletes="false"
							NoMasterRecordsText="No Data Read from Import File" AllowSorting="false" AllowMultiColumnSorting="false"
							CommandItemSettings-ShowRefreshButton="false" CommandItemSettings-ShowAddNewRecordButton="false"
							TableLayout="Fixed" RowIndicatorColumn-Display="false" AutoGenerateColumns="true">
							<Columns>
								<telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
									<ItemStyle Width="40" />
									<HeaderStyle Width="40" />
								</telerik:GridClientSelectColumn>
							</Columns>
						</MasterTableView>
						<ClientSettings ClientEvents-OnRowSelected="rg1RowSelected" ClientEvents-OnRowDeselected="rg1RowDeselected">
							<Selecting AllowRowSelect="true" />
							<Scrolling AllowScroll="true" SaveScrollPosition="true" UseStaticHeaders="true" />
						</ClientSettings>
					</telerik:RadGrid>
				</telerik:RadAjaxPanel>
				<br />
				<table style="width: 100%">
					<tr>
						<td style="width: 250px;">
							<telerik:RadButton ID="btnImport" runat="server" Text="Import Selected Records (from above)" OnClick="btnImport_OnClick" />
							<asp:CustomValidator ID="importValidator" runat="server" CssClass="stdValidator" Display="Dynamic" ErrorMessage="<br/>Please Select Files to Import"></asp:CustomValidator>
						</td>
						<td>
							<p>- OR SELECT A PAST UPLOAD (30-day history only) -</p>
						</td>
						<td>							
							<telerik:RadComboBox ID="lstPastUploads" runat="server" DataSourceID="DbLinqDataSource1"
							    DataValueField="Importheaderid"	AppendDataBoundItems="true" HighlightTemplatedItems="true"	
								Enabled="true" AutoPostBack="true" OnSelectedIndexChanged="lstPastUploads_SelectedIndexChanged" Width="98%">
								<Items>
									<telerik:RadComboBoxItem Text="" Value="" />
								</Items>
								<ItemTemplate>
									<table style="width:100%;border-collapse: collapse;">
										<tr>
											<td style="width: 50%;">
												<asp:Label ID="lblFileName" runat="server" Text='<%# Eval("Filename") %>'></asp:Label>
											</td>
											<td style="width: 150px;">
												<asp:Label ID="lblImportType" runat="server" Text='<%# Eval("Importtype") %>'></asp:Label>
											</td>
											<td>
												<asp:Label ID="lblUploadDate" runat="server" Text='<%# Eval("Uploaddate") %>'></asp:Label>
											</td>
										</tr>
									</table>
								</ItemTemplate>
							</telerik:RadComboBox>
							<br />
							<br />
							<table style="width: 100%;">
								<tr>
									<td>
										<telerik:RadButton ID="btnRollback" runat="server" Text="Roll Back Selected Records (below)" OnClick="btnRollback_OnClick" />
										<asp:CustomValidator ID="rollbackValidator" runat="server" CssClass="stdValidator" Display="Dynamic" ErrorMessage="<br/>Please Select Files to Roll Back"></asp:CustomValidator>
									</td>
									<td>
										<telerik:RadButton ID="btnRollforward" runat="server" Text="Roll Forward Selected Records (below)" OnClick="btnRollforward_OnClick" />
										<asp:CustomValidator ID="rollforwardValidator" runat="server" CssClass="stdValidator" Display="Dynamic" ErrorMessage="<br/>Please Select Files to Roll Forward"></asp:CustomValidator>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br />
				<div class="lblInfoBold">Imported Records:</div>
				<!-- Imported Files -->
				<telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" EnableAJAX="true">
					<telerik:RadGrid ID="RadGrid2" DataSourceID="DbLinqDataSource2" GridLines="None" Width="100%" runat="server" ShowHeader="true"
						AllowAutomaticDeletes="false" AllowAutomaticInserts="false" AllowAutomaticUpdates="false"
						AllowMultiRowEdit="false" AllowMultiRowSelection="true" ShowGroupPanel="false"
						AllowPaging="false" AllowSorting="false" AutoGenerateColumns="true">
						<MasterTableView Width="100%" CommandItemDisplay="None" DataKeyNames="Importdetailid"
							AllowAutomaticUpdates="false" AllowAutomaticInserts="false" AllowAutomaticDeletes="false"
							NoMasterRecordsText="No Data Imported or Past Import Selected" AllowSorting="false" AllowMultiColumnSorting="false"
							CommandItemSettings-ShowRefreshButton="false" CommandItemSettings-ShowAddNewRecordButton="false"
							TableLayout="Fixed" RowIndicatorColumn-Display="false" AutoGenerateColumns="true">
							<Columns>
								<telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
									<ItemStyle Width="40" />
									<HeaderStyle Width="40" />
								</telerik:GridClientSelectColumn>
							</Columns>
						</MasterTableView>
						<ClientSettings>
							<Selecting AllowRowSelect="true" />
							<Scrolling AllowScroll="true" SaveScrollPosition="true" UseStaticHeaders="true" />
						</ClientSettings>
					</telerik:RadGrid>
				</telerik:RadAjaxPanel>
				<br />
			</div>
		</div>
	</div>
</asp:Content>

