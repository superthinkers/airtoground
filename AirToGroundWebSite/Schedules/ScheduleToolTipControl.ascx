﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ScheduleToolTipControl.ascx.cs"
    Inherits="ScheduleToolTipControl" ViewStateMode="Disabled" WarningLevel="0" %>
<div class="div_container">
    <div class="div_header">Service Quick View</div>
    <div style="text-align: left; width: 98%">
        <telerik:RadAjaxPanel ID="RadAjaxPanelScheduleToolTip" runat="server" EnableAJAX="true" Width="100%">
            <telerik:RadGrid ID="RadGridScheduleToolTip" runat="server" AutoGenerateColumns="false"
                Width="100%" Height="300px" OnItemDataBound="RadGridScheduleToolTip_ItemDataBound">
                <MasterTableView Name="MainView" runat="server" AutoGenerateColumns="false" RowIndicatorColumn-Display="false"
                    GridLines="Both" CellPadding="0" CellSpacing="0" RowIndicatorColumn-Visible="false"
                    ShowHeadersWhenNoRecords="true" DataKeyNames="Tailnumber" Font-Names="Arial"
                    Font-Size="10px" HorizontalAlign="Left" AllowPaging="false" AllowSorting="false"
                    AllowFilteringByColumn="false" CommandItemDisplay="None" CommandItemSettings-ShowAddNewRecordButton="false"
                    CommandItemSettings-ShowRefreshButton="false" AlternatingItemStyle-BackColor="Silver">
                    <Columns>
                        <telerik:GridBoundColumn HeaderText="Tail" DataField="Tailnumber" UniqueName="TailnumberColumn"
                            ReadOnly="true" AllowSorting="false" AllowFiltering="false">
                            <HeaderStyle Width="40px" HorizontalAlign="Center" />
                            <ItemStyle Width="40px" HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Type" DataField="Equipmenttypedescr" UniqueName="EqTypeDescrColumn"
                            ReadOnly="true" AllowSorting="false" AllowFiltering="false">
                            <HeaderStyle Width="60px" HorizontalAlign="Center" />
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Service" DataField="Servicedescr" UniqueName="ServicedescrColumn"
                            ReadOnly="true" AllowSorting="true" AllowFiltering="false">
                            <HeaderStyle Width="200px" HorizontalAlign="Left" />
                            <ItemStyle Width="200px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Area" UniqueName="AreaColumn"
                            HeaderText="Div area" SortExpression="Area" AllowFiltering="false" ShowFilterIcon="false">
                            <%--<EditItemTemplate>
                                    <telerik:RadComboBox ID="cmbArea1" runat="server" DataSourceID="DBLinqDataSource2"
                                        AppendDataBoundItems="true" Width="150px" DataTextField="Description"
                                        DataValueField="Description" MarkFirstMatch="true">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="reqArea1" runat="server" Enabled="false" CssClass="stdValidator"
                                        ErrorMessage="*" ControlToValidate="cmbArea1" Display="Dynamic"></asp:RequiredFieldValidator>
                                </EditItemTemplate>--%>
                            <HeaderStyle Width="80px" HorizontalAlign="Left" />
                            <ItemStyle Width="80px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Interval" UniqueName="IntervalColumn" HeaderText="Interval"
                            ReadOnly="true" AllowFiltering="false" AllowSorting="false">
                            <HeaderStyle Width="50px" HorizontalAlign="Center" />
                            <ItemStyle Width="50px" HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn DataField="Servicedate" UniqueName="ServicedateColumn"
                            DataFormatString="{0:d}" HeaderText="Service Date" ReadOnly="true" MinDate="1/1/0001 12:00:00 AM"
                            AllowSorting="false" AllowFiltering="false">
                            <ItemStyle Width="80px" />
                            <HeaderStyle Width="80px" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn DataField="Servicetime" UniqueName="ServicetimeColumn"
                            DataFormatString="{0:t}" HeaderText="Start Time" ReadOnly="true" MinDate="1/1/0001 12:00:00 AM" 
                            AllowFiltering="false" AllowSorting="false">
                            <HeaderStyle Width="80px" />
                            <ItemStyle Width="80px" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn DataField="Completeddate" UniqueName="CompleteddateColumn"
                            DataFormatString="{0:d}" HeaderText="Compl Date" ReadOnly="true" MinDate="1/1/0001 12:00:00 AM"
                            AllowFiltering="false" AllowSorting="false">
                            <HeaderStyle Width="80px" />
                            <ItemStyle Width="80px" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn DataField="Completedtime" UniqueName="CompletedtimeColumn"
                            DataFormatString="{0:t}" HeaderText="Compl Time" ReadOnly="true" MinDate="1/1/0001 12:00:00 AM"
                            AllowFiltering="false" AllowSorting="false">
                            <HeaderStyle Width="80px" />
                            <ItemStyle Width="80px" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn DataField="Location" UniqueName="LocationColumn" HeaderText="Location"
                            ReadOnly="true" AllowFiltering="false" AllowSorting="false">
                            <HeaderStyle Width="100px" HorizontalAlign="Center" />
                            <ItemStyle Width="100px" HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Comments" UniqueName="CommentsColumn" HeaderText="Comments"
                            ReadOnly="true" AllowFiltering="false" AllowSorting="false">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Status" UniqueName="StatusColumn" HeaderText="x"
                            ReadOnly="true" Visible="false" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="ssHeader" />
                <ItemStyle HorizontalAlign="Center" />
                <AlternatingItemStyle HorizontalAlign="Center" />
                <ClientSettings>
                    <Selecting AllowRowSelect="true" />
                    <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                </ClientSettings>
            </telerik:RadGrid>
            <br />
        </telerik:RadAjaxPanel>
    </div>
</div>
