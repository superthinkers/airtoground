﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExceptionDetailsView.aspx.cs"
    Inherits="Schedules_ExceptionDetailsView" Buffer="true" Strict="true" Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function doComplete() {
                // Get a reference to the current RadWindow
                var oWnd = GetRadWindow();
                oWnd.close();
            }
        </script>
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server" AsyncPostBackTimeout="600"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin" ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <div style="width:98%; margin: 10px 10px 10px 10px; width: 600px; height: 400px;">
            <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Service Exception Details"></asp:Label>
            <br />
            <br />
            <div style="position: relative; float: left; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 100%">
                <table style="border-collapse: separate; width: 100%">
                    <tr>
                        <td style="text-align: right; width: 15%">
                            <asp:Label ID="lblCustomerLbl" runat="server" SkinID="InfoLabel" Text="Customer:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCustomer" runat="server" SkinID="InfoLabelBold" Text="Customer"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 15%">
                            <asp:Label ID="lblTailNumberLbl" runat="server" SkinID="InfoLabel" Text="Tail Number:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblTailNumber" runat="server" SkinID="InfoLabelBold" Text="Tail Number"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 15%">
                            <asp:Label ID="lblServiceDescrLbl" runat="server" SkinID="InfoLabel" Text="Service:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblServiceDescr" runat="server" SkinID="InfoLabelBold" Text="Service"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 15%">
                            <asp:Label ID="lblServiceDateLbl" runat="server" SkinID="InfoLabel" Text="Service Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblServiceDate" runat="server" SkinID="InfoLabelBold" Text="Service Date"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: relative; float: left; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 100%">
                <div class="stdLabel" style="text-align: center; width: 98%">
                    Completed / Closed Exceptions
                </div>
                <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False"
                    Width="99%" OnSelectedIndexChanged="RadGrid1_SelectedIndexChanged" 
                    Height="150px" GridLines="None">
                    <MasterTableView Name="MainView" runat="server"
                        AutoGenerateColumns="false" RowIndicatorColumn-Display="false" GridLines="Both"
                        CellPadding="0" CellSpacing="0" RowIndicatorColumn-Visible="false" ShowHeadersWhenNoRecords="true"
                        DataKeyNames="Serviceexceptionid" Font-Names="Arial" HorizontalAlign="Left" AllowAutomaticDeletes="false"
                        AllowPaging="false" EditMode="InPlace" AllowSorting="false" AllowFilteringByColumn="false"
                        Width="100%" AllowAutomaticUpdates="true">
                        <Columns>
                            <%--<telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                <HeaderStyle Width="60px" />
                                <ItemStyle Width="60px" />
                            </telerik:GridEditCommandColumn>--%>
                            <telerik:GridBoundColumn DataField="Serviceexceptionid" UniqueName="ServiceexceptionidColumn"
                                HeaderText="Ex Id" ReadOnly="false" Visible="false" AllowFiltering="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Servicescheduleid" UniqueName="ServicescheduleidColumn"
                                HeaderText="SS Id" ReadOnly="false" Visible="false" AllowFiltering="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Equipmentid" UniqueName="EquipmentidColumn" HeaderText="Eq Id"
                                ReadOnly="false" Visible="false" AllowSorting="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Serviceid" UniqueName="ServiceidColumn" HeaderText="Service Id"
                                ReadOnly="false" Visible="false" AllowFiltering="false" AllowSorting="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Sectionid" UniqueName="SectionidColumn" HeaderText="Section Id"
                                ReadOnly="false" Visible="false" AllowFiltering="false" AllowSorting="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Exceptiondate" UniqueName="Exceptiondateolumn"
                                HeaderText="Ex Date" ReadOnly="true" AllowSorting="false" AllowFiltering="false"
                                DataFormatString="{0:d}">
                                <HeaderStyle Width="90px" />
                                <ItemStyle Width="90px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Exceptionby" UniqueName="ExceptionbyColumn" HeaderText="Ex By"
                                ReadOnly="true" AllowSorting="false" AllowFiltering="false">
                                <HeaderStyle Width="50px" />
                                <ItemStyle Width="50px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Exceptionnotes" UniqueName="ExceptionnotesColumn"
                                HeaderText="Ex Notes" ReadOnly="true" AllowSorting="false" AllowFiltering="false">
                                <HeaderStyle Width="150px" />
                                <ItemStyle Width="150px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn DataField="Completeddate" UniqueName="Completeddateolumn"
                                HeaderText="Compl. Date" ReadOnly="false" AllowSorting="false" AllowFiltering="false"
                                DataFormatString="{0:d}" MinDate="1/1/0001 12:00:00 AM" ColumnEditorID="CompleteddateColumnEditor">
                                <HeaderStyle Width="120px" />
                                <ItemStyle Width="120px" />
                            </telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn DataField="Completednotes" UniqueName="CompletednotesColumn"
                                HeaderText="Compl. Notes" ReadOnly="false" AllowSorting="false" AllowFiltering="false">
                                <HeaderStyle Width="150px" />
                                <ItemStyle Width="150px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn DataField="Closeddate" UniqueName="CloseddateColumn"
                                HeaderText="Cls. Date" ReadOnly="false" AllowSorting="false" AllowFiltering="false"
                                DataFormatString="{0:d}" MinDate="1/1/0001 12:00:00 AM" ColumnEditorID="CloseddateColumnEditor">
                                <HeaderStyle Width="120px" />
                                <ItemStyle Width="120px" />
                            </telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn DataField="Closednotes" UniqueName="ClosednotesColumn" HeaderText="Cls. Notes"
                                ReadOnly="false" AllowSorting="false" AllowFiltering="false">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <NoRecordsTemplate>
                            <span class="lblInfo">No Exceptions</span>
                        </NoRecordsTemplate>
                        <AlternatingItemStyle BackColor="#F2F0F2" />
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="true" />
                        <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                    </ClientSettings>
                </telerik:RadGrid>
                <telerik:GridDateTimeColumnEditor ID="CompleteddateColumnEditor" runat="server">
                    <TextBoxStyle Width="98%" />
                </telerik:GridDateTimeColumnEditor>
                <telerik:GridDateTimeColumnEditor ID="CloseddateColumnEditor" runat="server">
                    <TextBoxStyle Width="98%" />
                </telerik:GridDateTimeColumnEditor>
            </div>
            <div style="position: relative; float: left; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 100%">
                <table style="border-collapse: separate; width: 100%">
                    <tr>
                        <td style="text-align: right; width: 15%">
                            <asp:Label ID="lblExNotes" runat="server" SkinID="InfoLabel" Text="Exception Notes:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtExNotes" runat="server" SkinID="PopBox" Width="98%" MaxLength="255"
                                Rows="10" Height="50px" ReadOnly="true" Wrap="true" TextMode="MultiLine">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 15%">
                            <asp:Label ID="lblCompletedNotes" runat="server" SkinID="InfoLabel" Text="Completed Notes:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtCompletedNotes" runat="server" SkinID="PopBox" Width="98%" MaxLength="255"
                                Rows="10" Height="50px" ReadOnly="true" Wrap="true" TextMode="MultiLine">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 15%">
                            <asp:Label ID="lblClosedNotes" runat="server" SkinID="InfoLabel" Text="Closed Notes:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtClosedNotes" runat="server" SkinID="PopBox" Width="98%" MaxLength="255"
                                Rows="10" Height="50px" ReadOnly="true" Wrap="true" TextMode="MultiLine">
                            </asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div style="text-align: center; width:100%">
                <button title="Done" id="Submit" onclick="doComplete(); return false;">
                    Done</button>
            </div>
        </div>
    </form>
</body>
</html>
