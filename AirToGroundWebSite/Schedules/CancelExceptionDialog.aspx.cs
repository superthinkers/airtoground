﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using ATGDB;

public partial class Schedules_CancelExceptionDialog : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_CANCEL_EX;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);
        
        // Update RadGrid.ImagePaths
        PageUtils.SetRadGridImagePathAllControls(this, Page.Theme);

        if (!Page.IsPostBack) {
            if (Request.QueryString["ssId"] != null) {

                // For refreshing ViewSchedules.aspx.
                Session.Add("REFRESH_VIEW_SCHEDULES", "TRUE");

                // Header summary
                ATGDB.Serviceschedule ss = Serviceschedule.GetServiceSchedule(int.Parse(Request.QueryString["ssId"].ToString()));
                lblCustomer.Text = ss.Service.Customer.Description;
                lblTailNumber.Text = ss.Equipment.Tailnumber;
                lblServiceDescr.Text = ss.Service.Description;
                lblServiceDate.Text = ss.Servicedate.ToShortDateString();
            }
        }
    }
    protected void RadGrid1_SelectedIndexChanged(object sender, EventArgs e) {
        Int32 exId = Convert.ToInt32(RadGrid1.SelectedValue);

        // Get the exception.
        ATGDataContext db = new ATGDataContext();
        ATGDB.Serviceexception ex = db.Serviceexceptions.Single(x => x.Serviceexceptionid == exId);

        // Set the notes fields.
        txtExNotes.Text = ex.Exceptionnotes;
        txtCompletedNotes.Text = ex.Completednotes;
        txtClosedNotes.Text = ex.Closednotes;
    }
    protected void RadGrid1_DataBound(object sender, EventArgs e) {
        // Default selection.
        if ((RadGrid1.SelectedValue == null) && (RadGrid1.Items.Count > 0)) {
            RadGrid1.Items[0].Selected = true;
        }
        RadGrid1_SelectedIndexChanged(null, null);
    }
    protected void CustomValidator1_OnServerValidate(object sender, ServerValidateEventArgs e) {
        //GridEditableItem item = (GridEditableItem)RadGrid1.SelectedItems[0];
        //System.Collections.Specialized.ListDictionary vals = new System.Collections.Specialized.ListDictionary();
        //item.ExtractValues(vals);
        //string[] myKeys = new string[vals.Count];
        //vals.Keys.CopyTo(myKeys, 0);
        //string dtCompleted = vals["CompletedDate"].ToString();
        e.IsValid = true;
    }
}