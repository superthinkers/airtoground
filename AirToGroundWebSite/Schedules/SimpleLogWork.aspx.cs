﻿using System;
using Telerik.Web.UI;
using System.Linq;
using ATGDB;

public partial class Schedules_SimpleLogWork : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_LOG_WORK;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);

        if (!Page.IsPostBack) {
            if (Request.QueryString["ssId"] != null) {
                Session.Add("LAST_SSID", null);

                // Set a hidden field.
                hiddenSsId.Value = Request.QueryString["ssId"].ToString();
                if (Request.QueryString["sqitemId"] != null) {
                    hiddenSQItemId.Value = Request.QueryString["sqitemId"];
                }

                // Track height.
                int theHeight = 610;

                // Header summary
                ATGDB.Serviceschedule ss = Serviceschedule.GetServiceSchedule(int.Parse(Request.QueryString["ssId"].ToString()));
                lblCustomer.Text = ss.Service.Customer.Description;
                lblTailNumber.Text = ss.Equipment.Tailnumber;
                lblServiceDescr.Text = ss.Service.Description;
                lblServiceDate.Text = ss.Servicedate.ToShortDateString();

                //LoadJobCodes(ss);

                // Hide UI based on service flat-fee/hourly.
                /*if (ss.Service.Flatfeeorhourly == "F") {
                    st1.Visible = false;
                    //st2.Visible = false;
                    et1.Visible = false;
                    //et2.Visible = false;
                    ex1.Visible = true;
                    ex2.Visible = true;
                    ex3.Visible = true;
                    ex4.Visible = true;
                    q1.Visible = true;
                    //q2.Visible = true;
                    // Set height change.
                    theHeight = theHeight - 250;
                } else {
                    st1.Visible = true;
                    //st2.Visible = true;
                    et1.Visible = true;
                    //et2.Visible = true;
                    ex1.Visible = true;
                    ex2.Visible = true;
                    ex3.Visible = true;
                    ex4.Visible = true;
                    q1.Visible = false;
                    //q2.Visible = false;
                    // Set height change.
                    theHeight = theHeight - 200;
                }*/

                // Other defaults.
                txtWorkDate.SelectedDate = ss.Servicedate;
                txtStartTime.SelectedDate = ss.Servicedate;
                txtEndTime.SelectedDate = ss.Servicedate;

                // Load up any outstanding exceptions.
                var data = Serviceexception.GetOutstandingExceptionSections(int.Parse(Request.QueryString["ssId"].ToString()));
                if (data.Count > 0) {
                    lstCompletedSections.DataSource = data;
                    lstCompletedSections.DataBind();
                    // Check all of them.
                    string s = "";
                    foreach (RadListBoxItem item in lstCompletedSections.Items) {
                        item.Checked = true;
                        if (s == "") {
                            s = item.Value;
                        } else {
                            s = s + ":" + item.Value;
                        }
                    }
                    // Set the completed sections now.
                    hiddenCompletedSections.Value = s;
                } else {
                    // No Exceptions, don't show UI.
                    ex1.Visible = false;
                    ex4.Visible = false;
                    // Set height change.
                    theHeight = theHeight - 100;
                }

                // Set the height.
                mainDiv.Style.Add("height", theHeight.ToString() + "px");
                mainDiv.Style.Add("width", "600px");

                // Load up the Exception Sections
                var data2 = Section.GetSectionsByServiceId(int.Parse(Request.QueryString["serviceId"].ToString()));
                // Bind.
                lstSections.DataSource = data2;
                lstSections.DataBind();

                // Get the service.
                ATGDB.Service svc = Service.GetServiceByScheduleId(Convert.ToInt32(hiddenSsId.Value));

                // Load Division areas
                ATGDataContext db = new ATGDataContext();
                var data3 = from x in db.Divisionareas
                           orderby x.Description
                           select x;
                cmbArea.DataSource = data3;
                cmbArea.DataBind();

                // Enable the validation
                reqArea.Enabled = svc.Requiresdivisionarea;
                if (reqArea.Enabled == true) {
                    reqArea.IsValid = !svc.Requiresdivisionarea || !String.IsNullOrEmpty(cmbArea.SelectedValue);
                }
            }
        }
    }
    protected void cmbArea_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
        // Get the service.
        ATGDB.Service svc = Service.GetServiceByScheduleId(Convert.ToInt32(hiddenSsId.Value));

        // Enable the validation
        reqArea.IsValid = true;
        if (svc.Requiresdivisionarea == true) {
            reqArea.IsValid = !String.IsNullOrEmpty(e.Value);        
        }
    }
    protected void lstSections_ItemCheck(object sender, RadListBoxItemEventArgs e) {
        string s = "";
        for (int lcv = 0; lcv < lstSections.Items.Count; lcv++) {
            if (lstSections.Items[lcv].Checked) {
                if (s == "") {
                    s = lstSections.Items[lcv].Value;
                } else {
                    s = s + ":" + lstSections.Items[lcv].Value;
                }
            }
        }
        hiddenSelectedSections.Value = s;
    }
    protected void lstCompletedSections_ItemCheck(object sender, RadListBoxItemEventArgs e) {
        string s = "";
        for (int lcv = 0; lcv < lstCompletedSections.Items.Count; lcv++) {
            if (lstCompletedSections.Items[lcv].Checked) {
                if (s == "") {
                    s = lstCompletedSections.Items[lcv].Value;
                } else {
                    s = s + ":" + lstCompletedSections.Items[lcv].Value;
                }
            }
        }
        hiddenCompletedSections.Value = s;
    }
    //protected void lstJobCodes_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
    //    ATGDB.Jobcode jc = ATGDB.Jobcode.GetJobcodeById(e.Value);
    //    if (jc != null) {
    //        lblJobCodeName.Text = jc.Description;
    //    } else {
    //        lblJobCodeName.Text = "None Selected";
    //    }
    //}
    //protected void LoadJobCodes(ATGDB.Serviceschedule ss) {
    //    //ATGDB.Serviceschedule ss = Serviceschedule.GetServiceSchedule(int.Parse(Request.QueryString["ssId"].ToString()));
    //    string companyDivisionId = ss.Companydivisionid.ToString();
    //    string customerId = ss.Service.Customerid.ToString();
    //    if (customerId != "") {
    //        DataSet data = ATGDB.Jobcode.GetCompanyCustomerJobJoinsDS(companyDivisionId, customerId);
    //        lstJobCodes.DataSource = data;
    //        lstJobCodes.DataBind();
    //        //RadComboBoxItem item = new RadComboBoxItem("- Required -", "");
    //        //lstJobCodes.Items.Insert(0, item);
    //        // Now set the job code.
    //        lstJobCodes.SelectedIndex = lstJobCodes.FindItemIndexByValue(ss.Service.Jobcodeid.ToString());
    //    }
    //}
}