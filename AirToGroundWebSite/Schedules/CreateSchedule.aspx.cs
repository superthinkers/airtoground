﻿using System;
using System.Text;
using System.Linq;
using System.Web.UI.WebControls;
using ATGDB;

public partial class Schedules_CreateSchedule : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_SCHEDULE_WIZ;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            lstEquipment.DataSource = Equipment.GetEquipmentNotOnHold(int.Parse(Profile.Customerid));
            lstEquipment.DataBind();
        }
    }
    protected void CustomValidator1_OnServerValidate(object sender, ServerValidateEventArgs e) {
        e.IsValid = false;
        for (int lcv = 0; lcv < lstServices.Items.Count(); lcv++) {
            e.IsValid = lstServices.Items[lcv].Checked;
            if (e.IsValid) {
                break;
            }
        }
    }
    protected void Wizard1_OnActiveStepChanged(object sender, EventArgs e) {
        if (Wizard1.ActiveStepIndex == 3) {
            // Select similar recently schedule equipment.
            RadGrid1.DataSourceID = "ObjectDataSource1";
            RadGrid1.DataBind();
        } else if (Wizard1.ActiveStepIndex == 4) {
            
            // Build a summary.
            StringBuilder s = new StringBuilder();
            s.Append("<div style=\"text-align:left\">");
            s.Append("<div class=\"lblTitle\">Aircraft</div>");
            s.Append("<div class=\"lblInfo\">" + lstEquipment.Text + "</div>");
            s.Append("<br />");

            //string location = Companydivision.GetCompanyDivisionById(Profile.Companydivisionid).Location;
            //s.Append("<div class=\"lblTitle\">Location</div>");
            //s.Append("<div class=\"lblInfo\">" + location + "</div>");
            //s.Append("<br />");

            //s.Append("<br />");
            s.Append("<div class=\"lblTitle\">Date/Time</div>");
            s.Append("<div class=\"lblInfo\">" + dtServiceDate.SelectedDate.Value.ToShortDateString() + "/" + dtServiceTime.SelectedDate.Value.ToShortTimeString() + "</div>");
            s.Append("<br />");

            // List of Services.
            s.Append("<div class=\"lblTitle\">Services</div>");
            for (int lcv = 0; lcv < lstServices.Items.Count(); lcv++) {
                if (lstServices.Items[lcv].Checked) {
                    s.Append("<div class=\"lblInfo\">" + lstServices.Items[lcv].Text + "</div>");
                }
            }
            s.Append("<br />");

            // Finish up.
            s.Append("<div class=\"lblTitle\">Comments</div>");
            s.Append("<div class=\"lblInfo\">" + txtComments.Text ?? "None" +"</div>");
            s.Append("<br />");
            s.Append("</div>");

            // Write it.
            lblSummary.Text = s.ToString();
        }
    }
    protected void Wizard1_OnFinishButtonClick(object sender, EventArgs e) {
        // Made it this far, create the schedules.

        ATGDataContext db = new ATGDataContext();
        
        // Create one for each service.
        for (int lcv = 0; lcv < lstServices.Items.Count(); lcv++) {
            if (lstServices.Items[lcv].Checked) {
                int serviceId = Int32.Parse(lstServices.Items[lcv].Value);

                // Create it.
                ATGDB.Serviceschedule s = new ATGDB.Serviceschedule();
                s.Serviceid = serviceId;
                //s.Companydivisionid = Int32.Parse(Profile.Companydivisionid);
                s.Equipmentid = Int32.Parse(lstEquipment.SelectedValue);
                s.Servicedate = dtServiceDate.SelectedDate.Value;
                s.Servicetime = dtServiceTime.SelectedDate.Value;
                s.Comments = txtComments.Text;

                // Insert it.
                db.Serviceschedules.InsertOnSubmit(s);
                db.SubmitChanges();

                // Reset Wizard.
                Wizard1.ActiveStepIndex = 5;
            }
        }
    }
    protected void RadGrid1_DataBinding(object sender, EventArgs e) {
        ObjectDataSource1.SelectParameters["equipmentId"].DefaultValue = lstEquipment.SelectedValue;
        ObjectDataSource1.SelectParameters["dtFromDate"].DefaultValue = dtServiceDate.SelectedDate.Value.ToShortDateString();
    }
}