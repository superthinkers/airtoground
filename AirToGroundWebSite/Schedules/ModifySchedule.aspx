﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="ModifySchedule.aspx.cs" Inherits="Schedules_ModifySchedule" Buffer="true"
    Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <%--<devart:DbLinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="" OrderBy="Tailnumber" Select="new (Description, Equipmentid, Tailnumber)"
        TableName="Equipments" Where="Customerid == Convert.ToInt32(@Customerid)">
        <WhereParameters>
            <asp:ProfileParameter Name="Customerid" PropertyName="Customerid" Type="Int32" />
        </WhereParameters>
    </devart:DbLinqDataSource>--%>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DeleteMethod="DeleteServiceSchedule"
        SelectMethod="GetOpenServiceSchedulesByDates" TypeName="ATGDB.Serviceschedule"
        UpdateMethod="UpdateServiceSchedule">
        <DeleteParameters>
            <asp:Parameter Name="serviceScheduleId" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:Parameter Name="equipmentId" Type="Int32" />
            <asp:Parameter Name="dtFromDate" Type="DateTime" />
            <asp:Parameter Name="dtToDate" Type="DateTime" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="serviceScheduleId" Type="Int32" />
            <asp:Parameter Name="serviceDate" Type="DateTime" />
            <asp:Parameter Name="serviceTime" Type="DateTime" />
            <asp:Parameter Name="comments" Type="String" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="true" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" Width="250" Height="75">
    </telerik:RadWindowManager>
    <script type="text/javascript">
        function ValidateDateRange(sender, e) {
            var dtFromDate = $find("<%= dtFromServiceDate.ClientID %>");
            var dtToDate = $find("<%= dtToServiceDate.ClientID %>");

            if ((dtFromDate.get_selectedDate() == null) && (dtToDate.get_selectedDate() != null)) {
                alert("Please select the From/End Date");
            } else if ((dtFromDate.get_selectedDate() > dtToDate.get_selectedDate()) && (dtToDate.get_selectedDate() != null)) {
                alert("The To/Start Date must be less than or equal to than the From/Begin Date");
                dtFromDate.set_selectedDate(dtToDate.get_selectedDate());
            }
            return false;
        }
    </script>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Modify a Scheduled Service"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">
                Modify a Scheduled Service</div>
            <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" DisplayCancelButton="true"
                BackColor="#F2F0F2" DisplaySideBar="false" OnActiveStepChanged="Wizard1_OnActiveStepChanged"
                StepStyle-HorizontalAlign="Center" Width="100%" CancelDestinationPageUrl="~/Schedules/ModifySchedule.aspx">
                <WizardSteps>
                    <asp:WizardStep ID="WizardStep1" runat="server" Title="Step 1">
                        <br />
                        <div class="lblInfo">
                            From Date<br />
                            <telerik:RadDatePicker ID="dtFromServiceDate" runat="server">
                                <ClientEvents OnDateSelected="ValidateDateRange" />
                            </telerik:RadDatePicker>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="stdValidator"
                                ErrorMessage="<br/>Required" ControlToValidate="dtFromServiceDate"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                        <div class="lblInfo">
                            To Date (Optional)<br />
                            <telerik:RadDatePicker ID="dtToServiceDate" runat="server">
                                <ClientEvents OnDateSelected="ValidateDateRange" />
                            </telerik:RadDatePicker>
                        </div>
                        <br />
                        <div class="lblInfo">
                            Tail Number Being Rescheduled<br />
                            <telerik:RadComboBox ID="lstEquipment" runat="server" Width="200px"
                                DataTextField="Tailnumber" DataValueField="Equipmentid" MarkFirstMatch="true"
                                HighlightTemplatedItems="true">
                                <%--<ItemTemplate>
                                    <ul>
                                        <li class="cbCol1">
                                            <%# DataBinder.Eval(Container.DataItem, "Tailnumber") %></li>
                                        <li class="cbCol2">
                                            <%# DataBinder.Eval(Container.DataItem, "Description") %></li>
                                    </ul>
                                </ItemTemplate>--%>
                            </telerik:RadComboBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="stdValidator"
                                ErrorMessage="<br/>Required" ControlToValidate="lstEquipment"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep2" runat="server" Title="Step 2" StepType="Step">
                        <br />
                        <div class="lblTitle" style="width: 45%">
                            These Services are scheduled for this Aircraft for the selected Date(s).</div>
                        <br />
                        <div style="text-align: left">
                            <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="false" HorizontalAlign="NotSet"
                                AutoGenerateColumns="false" OnDataBinding="RadGrid1_DataBinding">
                                <MasterTableView Name="MainView" runat="server" AutoGenerateColumns="false" RowIndicatorColumn-Display="false"
                                    GridLines="Both" RowIndicatorColumn-Visible="false"
                                    ShowHeadersWhenNoRecords="false" DataKeyNames="Servicescheduleid" Font-Names="Arial"
                                    HorizontalAlign="Left" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true"
                                    EditMode="InPlace">
                                    <Columns>
                                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                            <HeaderStyle Width="60px" />
                                            <ItemStyle Width="60px" />
                                        </telerik:GridEditCommandColumn>
                                        <telerik:GridBoundColumn DataField="TailNumber" UniqueName="TailNumberColumn" HeaderText="Tail"
                                            ReadOnly="true">
                                            <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                            <ItemStyle Width="40px" HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Equipmentdescr" UniqueName="EquipmentdescrColumn"
                                            HeaderText="Aircraft" ReadOnly="true">
                                            <HeaderStyle Width="80px" />
                                            <ItemStyle Width="80px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Servicedescr" UniqueName="ServicedescrColumn"
                                            HeaderText="Service Description" ReadOnly="true">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridDateTimeColumn DataField="Servicedate" UniqueName="ServicedateColumn"
                                            DataFormatString="{0:d}" HeaderText="Service Date" ReadOnly="false" 
                                            MinDate="1/1/0001 12:00:00 AM" ColumnEditorID="ServicedateColumnEditor">
                                            <ItemStyle Width="120px" />
                                            <HeaderStyle Width="120px" />
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridDateTimeColumn DataField="Servicetime" UniqueName="ServicetimeColumn"
                                            DataFormatString="{0:t}" PickerType="TimePicker" HeaderText="Service Time" ReadOnly="false"
                                            MinDate="1/1/0001 12:00:00 AM" ColumnEditorID="ServicetimeColumnEditor">
                                            <ItemStyle Width="120px" />
                                            <HeaderStyle Width="120px" />
                                        </telerik:GridDateTimeColumn>
                                        <%--<telerik:GridDateTimeColumn DataField="Completeddate" UniqueName="CompleteddateColumn"
                                            DataFormatString="{0:d}" HeaderText="Completed Date" ReadOnly="true" MinDate="1/1/0001 12:00:00 AM">
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridDateTimeColumn DataField="Completedtime" UniqueName="CompletedtimeColumn"
                                            DataFormatString="{0:t}" PickerType="TimePicker" HeaderText="Completed Time"
                                            ReadOnly="true" MinDate="1/1/0001 12:00:00 AM">
                                        </telerik:GridDateTimeColumn>--%>
                                        <telerik:GridBoundColumn DataField="Location" UniqueName="LocationColumn" HeaderText="Location"
                                            ReadOnly="true">
                                            <HeaderStyle Width="90px" />
                                            <ItemStyle Width="90px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Comments" UniqueName="CommentsColumn" HeaderText="Comments"
                                            ReadOnly="false" MaxLength="255" ColumnEditorID="CommentsColumnEditor">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record?"
                                            ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                                            UniqueName="DeleteCommandColumn" CommandName="Delete">
                                            <HeaderStyle Width="40px" />
                                            <ItemStyle Width="40px" />
                                        </telerik:GridButtonColumn>
                                    </Columns>
                                    <NoRecordsTemplate>
                                        <div class="lblInfo">
                                            No matching Aircraft. Please continue.</div>
                                    </NoRecordsTemplate>
                                    <AlternatingItemStyle BackColor="#F2F0F2" />
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                    <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <telerik:GridDateTimeColumnEditor ID="ServicedateColumnEditor" runat="server">
                                <TextBoxStyle Font-Names="Arial" Font-Size="10px" Width="98%" />
                            </telerik:GridDateTimeColumnEditor>
                            <telerik:GridDateTimeColumnEditor ID="ServicetimeColumnEditor" runat="server">
                                <TextBoxStyle Font-Names="Arial" Font-Size="10px" Width="98%" />
                            </telerik:GridDateTimeColumnEditor>
                            <telerik:GridTextBoxColumnEditor ID="CommentsColumnEditor" runat="server">
                                <TextBoxStyle Font-Names="Arial" Width="98%" />
                            </telerik:GridTextBoxColumnEditor>
                        </div>
                        <br />
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep3" runat="server" Title="Step 3" StepType="Complete">
                        <br />
                        <br />
                        <div class="lblTitle" style="width: 25%">
                            Your Services were Successfully Rescheduled!</div>
                        <br />
                        <br />
                        <asp:HyperLink ID="lnkRescheduleWizard" runat="server" NavigateUrl="~/Schedules/ModifySchedule.aspx"
                            Text="Reschedule Another Service"></asp:HyperLink>
                        <br />
                        <br />
                    </asp:WizardStep>
                </WizardSteps>
            </asp:Wizard>
        </div>
    </div>
</asp:Content>
