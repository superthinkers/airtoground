﻿using System;
using System.Linq;
using ATGDB;

public partial class Schedules_ServiceSchedule_Updater : System.Web.UI.Page
{
    [System.Web.Services.WebMethod]
    public static string ApproveServiceSchedule(string ssId) {
        ATGDataContext dc = new ATGDataContext();
        Serviceschedule ss = dc.Serviceschedules.SingleOrDefault(x => x.Servicescheduleid == int.Parse(ssId));
        if (ss != null) {
            ss.Approvaldate = DateTime.Now;
            ss.Approvaluserid = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            dc.SubmitChanges();

            return "Success";
        } else {
            return "Fail";
        }
    }
}