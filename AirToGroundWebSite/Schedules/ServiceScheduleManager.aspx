﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="ServiceScheduleManager.aspx.cs" Inherits="Schedules_ServiceScheduleManager"
    Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetSSDS"
        UpdateMethod="UpdateSS" DeleteMethod="DeleteSS" DataObjectTypeName="ATGDB.Serviceschedule"
        TypeName="ATGDB.Serviceschedule" EnableCaching="false">
        <SelectParameters>
            <asp:Parameter Name="serviceDate" Type="DateTime" />
            <asp:Parameter Name="unapprovedOnly" Type="Boolean" DefaultValue="false" />
            <asp:ProfileParameter Name="companyDivisionId" PropertyName="Companydivisionid" Type="Int32" DefaultValue="-1" />
            <asp:ProfileParameter Name="customerId" PropertyName="Customerid" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="lnkApprove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="false" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" Width="250" Height="25" EnableViewState="false">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" NavigateUrl="ModifyExceptionsDialog.aspx"
                Behaviors="Close,Resize" OnClientClose="onClientClose1" ShowContentDuringLoad="false"
                VisibleStatusbar="false" VisibleTitlebar="false" EnableShadow="false" ReloadOnShow="true"
                Title="Log Work Done" Width="700" Height="620" Modal="true" AutoSize="false">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>     
    <telerik:RadCodeBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            // This fixes problems when exporting.
            function onRequestStart(sender, args) {
                if ((args.get_eventTarget().indexOf("btnExportToExcel") >= 0) || 
                    (args.get_eventTarget().indexOf("xxx") >= 0)) {
                    // Disable ajax
                    args.set_enableAjax(false);
                }
            }
            function onClientClose1(oWnd, args) {
                // Force a page postback. Causes refresh.
                <%= PostBackString %>
            }
            function openWin1(workLogId) {
                radopen("ModifyExceptionsDialog.aspx?workLogId=" + workLogId, "RadWindow1");
            }
            function DoApprove(ssId) {
                //alert("DoApprove: " + ssId);

                var loc = "ServiceSchedule_Updater.aspx";
                var args = '"ssId":"' + ssId + '"';
                $.ajax({
                    type: "POST",
                    url: loc + "/ApproveServiceSchedule",
                    data: "{" + args + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        // Replace the div's content with the page method's return.
                        //alert(msg.d);
                        // Force a search click. Causes grid refresh.
                        //< %= SearchPostBackString %>
                        $find("<%= RadGrid1.ClientID %>").get_masterTableView().rebind();
                    },
                    fail: function (msg) {
                        // Replace the div's content with the page method's return.
                        alert("The Service Schedule Could not be Verified");
                    }
                });
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="true" Skin="Office2007">
    </telerik:RadAjaxLoadingPanel>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Verify Scheduled Services"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Verify Scheduled Services</div>
            <div style="margin: 10px 10px 10px 10px">
                <!-- SEARCH -->
                <table style="width: 97%;">
                    <tr>
                        <td style="width: 70%">
                            <!-- SEARCH -->
                            <fieldset style="width: 500px">
                                <legend>Search Criteria:</legend>
                                <div class="lblInfo" style="margin: 10px 10px 10px 10px">
                                    <table style="width: 99%;">
                                        <tr>
                                            <asp:Label ID="lblCompCustInfo" runat="server" CssClass="lblInfo"></asp:Label>
                                        </tr>
                                        <tr>
                                            <td style="width: 33%;">
                                                Service Date:
                                            </td>
                                            <td rowspan="2" style="width: 33%;">
                                                &nbsp;-&nbsp;OR&nbsp;-&nbsp;
                                                <asp:CheckBox ID="chkUnapprovedOnly" runat="server" Checked="false" Text="Un-verified Only" />
                                            </td>
                                            <td rowspan="2" style="text-align: right; vertical-align: central;">
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_OnClick" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <telerik:RadDatePicker ID="dtServiceDate" runat="server" Width="120px">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </fieldset>
                        </td>                
                    </tr>
                </table>
                <br />
                <div class="lblInfoBold">Selected Service Schedules:</div>
                <!-- Work Log Results -->
                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
                    <telerik:RadGrid ID="RadGrid1" GridLines="None" Width="100%" runat="server"
                        AllowAutomaticDeletes="true" AllowAutomaticInserts="false" AllowAutomaticUpdates="false"
                        AllowMultiRowEdit="false" AllowMultiRowSelection="false" ShowGroupPanel="false"
                        AllowPaging="false" PageSize="10" AllowSorting="false" AutoGenerateColumns="false"
                        OnItemDataBound="RadGrid1_ItemDataBound">
                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="Servicescheduleid"
                            AllowAutomaticUpdates="false" AllowAutomaticInserts="false" AllowAutomaticDeletes="true"
                            NoMasterRecordsText="No Scheduled Services Found" AllowSorting="true" AllowMultiColumnSorting="false"
                            CommandItemSettings-ShowRefreshButton="false" CommandItemSettings-ShowAddNewRecordButton="false"
                            TableLayout="Fixed" Font-Size="12px" HorizontalAlign="NotSet" RowIndicatorColumn-Display="false"
                            AutoGenerateColumns="false" EditMode="InPlace">
                            <PagerStyle AlwaysVisible="true" Mode="NumericPages" Position="Bottom" />
                            <Columns>
                                <%--<telerik:GridTemplateColumn HeaderText="" UniqueName="ModifyExceptionsDialogColumn" AllowFiltering="false">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <a href="#" onclick='<%# "openWin1(" + Eval("Servicescheduleid").ToString() + "); return false;" %>'>
                                            <asp:ImageButton ID="btnEditRed" runat="server" SkinID="Edit" />
                                        </a>
                                    </ItemTemplate>
                                    <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                    <ItemStyle Width="50px" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>--%>
                                <%--<telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                    <HeaderStyle Width="60px" />
                                    <ItemStyle Width="60px" />
                                </telerik:GridEditCommandColumn>--%>
                                <telerik:GridTemplateColumn HeaderText="" UniqueName="ApproveColumn" AllowFiltering="false">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkApprove" runat="server" OnClientClick='<%# "DoApprove(" + Eval("Servicescheduleid").ToString() + "); return false;" %>'>
                                            Verify
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                    <ItemStyle Width="50px" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="Servicescheduleid" HeaderText="Id" AllowSorting="false"
                                    UniqueName="WorklogidColumn" ReadOnly="true" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Tailnumber" HeaderText="Tail" AllowSorting="true"
                                    UniqueName="TailnumberColumn" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="Servicedate" HeaderText="Service Date" AllowSorting="true"
                                    UniqueName="ServicedateColumn" ReadOnly="false" Visible="true" DataFormatString="{0:d}"
                                    PickerType="DatePicker" DefaultInsertValue="<%@ System.DateTime.Now.Date.ToShortDateString() %>" ColumnEditorID="ServicedateEditor">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="Servicedescr" HeaderText="Service Descr" AllowSorting="true"
                                    UniqueName="ServicedescrColumn" ReadOnly="true">
                                    <HeaderStyle Width="250px" />
                                    <ItemStyle Width="250px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Area" UniqueName="AreaColumn"
                                    HeaderText="Div area" SortExpression="Area" AllowFiltering="false" ShowFilterIcon="false">
                                    <%--<EditItemTemplate>
                                    <telerik:RadComboBox ID="cmbArea1" runat="server" DataSourceID="DBLinqDataSource2"
                                        AppendDataBoundItems="true" Width="150px" DataTextField="Description"
                                        DataValueField="Description" MarkFirstMatch="true">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="reqArea1" runat="server" Enabled="false" CssClass="stdValidator"
                                        ErrorMessage="*" ControlToValidate="cmbArea1" Display="Dynamic"></asp:RequiredFieldValidator>
                                </EditItemTemplate>--%>
                                    <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                    <ItemStyle Width="80px" HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="Servicetime" HeaderText="Service Time" AllowSorting="true"
                                    UniqueName="ServicetimeColumn" ReadOnly="false" Visible="true" DataFormatString="{0:t}"
                                    PickerType="TimePicker" DefaultInsertValue="08:00:00 AM" ColumnEditorID="ServicetimeEditor">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="Completeddate" HeaderText="Completed Date" AllowSorting="true"
                                    UniqueName="CompleteddateColumn" ReadOnly="true" Visible="true" DataFormatString="{0:d}">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="Completedtime" HeaderText="Completed Time" AllowSorting="true"
                                    UniqueName="CompletedtimeColumn" ReadOnly="true" Visible="true" DataFormatString="{0:t}">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="Cancelleddate" HeaderText="Cancelled Date" AllowSorting="true"
                                    UniqueName="CancelleddateColumn" ReadOnly="true" Visible="true" DataFormatString="{0:d}">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="Comments" HeaderText="Comments"
                                    AllowSorting="false" UniqueName="CommentsColumn" ReadOnly="false" Visible="true" ColumnEditorID="CommentsEditor">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Approvaldate" HeaderText="Verify Date" AllowSorting="true"
                                    UniqueName="ApprovaldateColumn" ReadOnly="true" Visible="true" DataFormatString="{0:d}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Approvaluserid" HeaderText="Verify UserId" AllowSorting="true"
                                    UniqueName="ApprovaluseridColumn" ReadOnly="true" Visible="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Upddt" HeaderText="Upd Dt" AllowSorting="true"
                                    UniqueName="UpddtColumn" ReadOnly="true" Visible="true" DataFormatString="{0:d}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Upduserid" HeaderText="Upd UserId" AllowSorting="true"
                                    UniqueName="UpduseridColumn" ReadOnly="true" Visible="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record? Note: This will delete all related records such as Work Logs and Service Queues."
                                    ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                                    UniqueName="DeleteCommandColumn" CommandName="Delete">
                                    <ItemStyle Width="40px" />
                                    <HeaderStyle Width="40px" />
                                </telerik:GridButtonColumn>
                                <%--<telerik:GridTemplateColumn HeaderText="" UniqueName="DeleteColumn" AllowFiltering="false">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDeleteRed" runat="server" SkinID="Delete" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                    <ItemStyle Width="50px" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>--%>
                            </Columns>
                        </MasterTableView>
                        <ExportSettings FileName="ServiceScheduleManager" OpenInNewWindow="true" ExportOnlyData="true"
                            IgnorePaging="true" HideStructureColumns="true">
                        </ExportSettings>
                        <ClientSettings>
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <telerik:GridDateTimeColumnEditor ID="ServicedateEditor" runat="server" TextBoxStyle-Width="98%">
                    </telerik:GridDateTimeColumnEditor>
                    <telerik:GridDateTimeColumnEditor ID="ServicetimeEditor" runat="server" TextBoxStyle-Width="98%">
                    </telerik:GridDateTimeColumnEditor>
                    <telerik:GridTextBoxColumnEditor ID="CommentsEditor" runat="server" TextBoxStyle-Width="98%" TextBoxMaxLength="255">
                    </telerik:GridTextBoxColumnEditor>
                </telerik:RadAjaxPanel>
                <br />
            </div>
        </div>
    </div>
</asp:Content>

