﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="CreateSchedule.aspx.cs" Inherits="Schedules_CreateSchedule" Buffer="true"
    Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <%--<devart:DbLinqDataSource ID="LinqDataSource1" runat="server" 
        ContextTypeName="ATGDB.ATGDataContext" EntityTypeName="" OrderBy="Tailnumber" 
        Select="new (Description, Equipmentid, Tailnumber)" TableName="Equipments" 
        Where="Customerid == Convert.ToInt32(@Customerid)">
        <WhereParameters>
            <asp:ProfileParameter Name="Customerid" PropertyName="Customerid" 
                Type="Int32" />
        </WhereParameters>
    </devart:DbLinqDataSource>--%>
    <devart:DbLinqDataSource ID="LinqDataSource2" runat="server" 
        ContextTypeName="ATGDB.ATGDataContext" EntityTypeName="" OrderBy="Description" 
        Select="new (Description, Serviceid)" TableName="Services" 
        Where="Customerid == Convert.ToInt32(@Customerid)">
        <WhereParameters>
            <asp:ProfileParameter Name="Customerid" PropertyName="Customerid" 
                Type="Int32" />
        </WhereParameters>
    </devart:DbLinqDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DeleteMethod="DeleteServiceSchedule"
        SelectMethod="GetServiceSchedulesByRange" TypeName="ATGDB.Serviceschedule" UpdateMethod="UpdateServiceSchedule">
        <DeleteParameters>
            <asp:Parameter Name="serviceScheduleId" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:Parameter Name="equipmentId" Type="Int32" />
            <asp:Parameter Name="dtFromDate" Type="DateTime" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="serviceScheduleId" Type="Int32" />
            <asp:Parameter Name="serviceDate" Type="DateTime" />
            <asp:Parameter Name="serviceTime" Type="DateTime" />
            <asp:Parameter Name="comments" Type="String" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="true" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" Width="250" Height="75">
    </telerik:RadWindowManager>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Schedule a Service"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Schedule a Service</div>
            <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" DisplayCancelButton="true"
                DisplaySideBar="false" BackColor="#F2F0F2" OnActiveStepChanged="Wizard1_OnActiveStepChanged"
                StepStyle-HorizontalAlign="Center" Width="100%" OnFinishButtonClick="Wizard1_OnFinishButtonClick"
                CancelDestinationPageUrl="~/Schedules/CreateSchedule.aspx">
                <WizardSteps>
                    <asp:WizardStep ID="WizardStep1" runat="server" Title="Step 1">
                        <br />
                        <div class="lblTitle" style="width: 30%">Please select the Service Date and Time</div>
                        <br />
                        <div class="lblInfo">Service Date<br />
                            <telerik:RadDatePicker ID="dtServiceDate" runat="server">
                            </telerik:RadDatePicker>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="stdValidator"
                                ErrorMessage="<br />Required" ControlToValidate="dtServiceDate"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                        <div class="lblInfo">Service Time<br />
                            <telerik:RadTimePicker ID="dtServiceTime" runat="server">
                            </telerik:RadTimePicker>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="stdValidator"
                                ErrorMessage="<br />Required" ControlToValidate="dtServiceTime"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                        <div class="lblInfo">Tail Number Being Serviced<br />
                            <telerik:RadComboBox ID="lstEquipment" runat="server" Width="200px"
                                DataTextField="Tailnumber" DataValueField="Equipmentid" MarkFirstMatch="true"
                                HighlightTemplatedItems="true">
                                <%--<ItemTemplate>
                                    <ul>
                                        <li class="cbCol1">
                                            <%# DataBinder.Eval(Container.DataItem, "Tailnumber") %></li>
                                        <li class="cbCol2">
                                            <%# DataBinder.Eval(Container.DataItem, "Description") %></li>
                                    </ul>
                                </ItemTemplate>--%>
                            </telerik:RadComboBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="stdValidator"
                                ErrorMessage="<br />Required" ControlToValidate="lstEquipment"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep2" runat="server" Title="Step 2">
                        <div style="position:relative; width: 30%; left: 0%; text-align:left">
                            <br />
                            <div class="lblTitle">
                                Please select all the Services being performed</div>
                            <br />
                            <telerik:RadListBox ID="lstServices" runat="server" CheckBoxes="true" 
                                DataSourceID="LinqDataSource2" Width="100%"
                                DataKeyField="Serviceid" DataTextField="Description" DataValueField="Serviceid">
                            </telerik:RadListBox>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" CssClass="stdValidator"
                                ErrorMessage="<br />Required" OnServerValidate="CustomValidator1_OnServerValidate"
                                Display="Dynamic"></asp:CustomValidator>
                        </div>
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep3" runat="server" Title="Step 3">
                        <br />
                        <div class="lblTitle">Are there any Comments you would like to add?</div>
                        <br />
                        <asp:TextBox ID="txtComments" runat="server" Width="400px"
                            MaxLength="255" Rows="10" Height="150px" Wrap="true" TextMode="MultiLine"></asp:TextBox>
                        <br />
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep4" runat="server" Title="Step 4">
                        <br />
                        <div class="lblTitle" style="width: 35%">These Services are already scheduled for this Aircraft.</div>
                        <br />
                        <div class="lblInfo">Do you need to adjust your parameters? If yes, click "Previous".</div>
                        <br />
                        <div style="text-align: left">
                            <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="false" HorizontalAlign="NotSet"
                                AutoGenerateColumns="false" OnDataBinding="RadGrid1_DataBinding">
                                <MasterTableView Name="MainView" runat="server" AutoGenerateColumns="false" 
                                    RowIndicatorColumn-Display="false" GridLines="Both" CellPadding="0" CellSpacing="0"
                                    RowIndicatorColumn-Visible="false" ShowHeadersWhenNoRecords="false"
                                    DataKeyNames="Servicescheduleid" HorizontalAlign="Left"
                                    AllowAutomaticDeletes="true" AllowAutomaticUpdates="true" EditMode="InPlace">
                                    <Columns>
                                        <%--<telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                            <HeaderStyle Width="60px" />
                                            <ItemStyle Width="60px" />
                                        </telerik:GridEditCommandColumn>--%>
                                        <telerik:GridBoundColumn DataField="TailNumber" UniqueName="TailNumberColumn" HeaderText="Tail"
                                            ReadOnly="true">
                                            <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                            <ItemStyle Width="40px" HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Equipmentdescr" UniqueName="EquipmentdescrColumn"
                                            HeaderText="Aircraft" ReadOnly="true">
                                            <HeaderStyle Width="80px" />
                                            <ItemStyle Width="80px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Servicedescr" UniqueName="ServicedescrColumn"
                                            HeaderText="Service Description" ReadOnly="true">
                                            <HeaderStyle Width="300px" />
                                            <ItemStyle Width="300px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridDateTimeColumn DataField="Servicedate" UniqueName="ServicedateColumn"
                                            DataFormatString="{0:d}" HeaderText="Service Date" ReadOnly="false">
                                            <ItemStyle Width="120px" />
                                            <HeaderStyle Width="120px" />
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridDateTimeColumn DataField="Servicetime" UniqueName="ServicetimeColumn"
                                            DataFormatString="{0:t}" PickerType="TimePicker" HeaderText="Service Time" ReadOnly="false">
                                            <ItemStyle Width="120px" />
                                            <HeaderStyle Width="120px" />
                                        </telerik:GridDateTimeColumn>
                                        <%--<telerik:GridDateTimeColumn DataField="Completeddate" UniqueName="CompleteddateColumn"
                                            DataFormatString="{0:d}" HeaderText="Completed Date" ReadOnly="true">
                                        </telerik:GridDateTimeColumn>
                                        <telerik:GridDateTimeColumn DataField="Completedtime" UniqueName="CompletedtimeColumn"
                                            DataFormatString="{0:t}" PickerType="TimePicker" HeaderText="Completed Time"
                                            ReadOnly="true">
                                        </telerik:GridDateTimeColumn>--%>
                                        <telerik:GridBoundColumn DataField="Location" UniqueName="LocationColumn" HeaderText="Location"
                                            ReadOnly="true">
                                            <HeaderStyle Width="90px" />
                                            <ItemStyle Width="90px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Comments" UniqueName="CommentsColumn" HeaderText="Comments"
                                            ReadOnly="false" MaxLength="255">
                                        </telerik:GridBoundColumn>
                                        <%--<telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record?"
                                            ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                                            UniqueName="DeleteCommandColumn" CommandName="Delete">
                                            <HeaderStyle Width="40px" />
                                            <ItemStyle Width="40px" />
                                        </telerik:GridButtonColumn>--%>
                                    </Columns>
                                    <NoRecordsTemplate>
                                        <div class="lblInfo">No matching Services. You may continue scheduling your Service.</div>
                                    </NoRecordsTemplate>
                                    <AlternatingItemStyle BackColor="#F2F0F2" />
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                    <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <br />
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep5" runat="server" Title="Step 5" StepType="Finish">
                        <br />
                        <div class="lblTitle">Summary</div>
                        <br />
                        <asp:Label ID="lblSummary" runat="server" Width="300px"></asp:Label>
                        <br />
                    </asp:WizardStep>
                    <asp:WizardStep ID="WizardStep7" runat="server" Title="Step 7" StepType="Complete">
                        <br />
                        <br />
                        <div class="lblTitle">Your Services were Successfully Created!</div>
                        <br />
                        <br />
                        <asp:HyperLink ID="lnkCreateWizard" runat="server" NavigateUrl="~/Schedules/CreateSchedule.aspx"
                            Text="Schedule Another Service"></asp:HyperLink>
                        <br />
                        <br />
                    </asp:WizardStep>
                </WizardSteps>
            </asp:Wizard>  
        </div>
    </div>
</asp:Content>

