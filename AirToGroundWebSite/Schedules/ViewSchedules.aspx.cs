﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using ATGDB;

public partial class Schedules_ViewSchedules : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_TAIL_SCHEDULE;
    }
    // For postbacks in the JavaScript in the page.
    protected string PostBackString;
    protected string SearchPostBackString;

    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e) {
        // For postbacks in the JavaScript in the page.
        PostBackString = Page.ClientScript.GetPostBackEventReference(this, "");
        SearchPostBackString = Page.ClientScript.GetPostBackEventReference(btnSearch, "OnClick");

        if (!Page.IsPostBack) {
            // Clear the cache.
            hiddenPageKey.Value = new Random().Next(10000000, 999999999).ToString();
			ScheduleHelper.Instance.ViewPageKey = hiddenPageKey.Value;
			System.Web.HttpContext.Current.Session.Add(Serviceschedule.VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey, "");

            // Security.
            if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_SCH)) {
                btnExportToExcel.Visible = false;
                //RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                //RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                //RadGrid1.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                //RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
            }

            // UI defaults.
            // Date boxes.
            dtFrom.SelectedDate = DateTime.Now;
            dtTo.SelectedDate = DateTime.Now.AddDays(7);
            
            // More Selection criteria.
            //lstCustomerSelector.SelectedCustomerId = Profile.Customerid;
            lstCustServices.DataSource = Service.GetMSorVSServicesByCustomerId(int.Parse(Profile.Customerid));//lstCustomerSelector.SelectedCustomerId));
            //lstCustEqTypes.DataSource = Equipmenttype.GetEquipmentTypesByCustomerId(int.Parse(lstCustomerSelector.SelectedCustomerId));
            lstCustServices.DataBind();
            //lstCustEqTypes.DataBind();
            
            //Devart.Data.MySql.MySqlMonitor m = new MySqlMonitor();
            //m.IsActive = true;
            // If parameters are passed in, set up the form and perform the search.
            //if (!String.IsNullOrEmpty(Request.QueryString["DAYS"])) {
            //    int days = int.Parse(Request.QueryString["DAYS"]);

            //    // Date boxes.
            //    dtFrom.SelectedDate = DateTime.Now;
            //    dtTo.SelectedDate = DateTime.Now.AddDays(days);

            //    // Performs search.
            //    RadGrid1.DataSourceID = "ObjectDataSource1";
            //    SetParams();
            //    RadGrid1.DataBind();
            //}

            // Mode = 1 : Date was clicked, parm=dt
            //            Return all tails, specific date.
            // Mode = 2 : Tail Number was clicked, parm=tail,dtFrom,dtTo
            //            Return all schedules given date range, specific tail.
            // Mode = 3 : A cell was clicked (Tail/Date combination), parm=dt,tail
            //            Return all schedules, specific tail, specific date
            if ((Request.QueryString["Mode"] != null) && (Request.QueryString["Mode"].ToString() != "")) {
                // Get the mode.
                string mode = Request.QueryString["Mode"].ToString();

                // Customer passed for all modes.
                //string custId = Request.QueryString["cid"].ToString();
                //lstCustomerSelector.SelectedCustomerId = custId;

                if (mode == "1") {
                    string dt = Request.QueryString["dt"].ToString();
                    dtFrom.SelectedDate = Convert.ToDateTime(dt);
                    dtTo.SelectedDate = Convert.ToDateTime(dt);
                    txtTailNumber.Text = "0";
                } else if (mode == "2") {
                    string dt1 = Request.QueryString["dtFrom"].ToString();
                    string dt2 = Request.QueryString["dtTo"].ToString();
                    string tail = Request.QueryString["tail"].ToString();
                    dtFrom.SelectedDate = Convert.ToDateTime(dt1);
                    dtTo.SelectedDate = Convert.ToDateTime(dt2);
                    txtTailNumber.Text = tail;
                } else if (mode == "3") {
                    string dt = Request.QueryString["dt"].ToString();
                    string tail = Request.QueryString["tail"].ToString();
                    dtFrom.SelectedDate = Convert.ToDateTime(dt);
                    dtTo.SelectedDate = Convert.ToDateTime(dt);
                    txtTailNumber.Text = tail;
                }

                string serviceParms = Request.QueryString["services"] == null ? "" : Request.QueryString["services"].ToString();
                //string eqTypesParms = Request.QueryString["eqTypes"] == null ? "" : Request.QueryString["eqTypes"].ToString();
                SetChecksInListBox(lstCustServices, "Serviceid IN (", serviceParms);
                //SetChecksInListBox(lstCustEqTypes, "Equipmenttypeid IN (", eqTypesParms);

                // Load it up.
                btnSearch_OnClick(null, null);

            }
        } else {
            ScheduleHelper.Instance.ViewPageKey = hiddenPageKey.Value;
            if ((Session["REFRESH_VIEW_SCHEDULES"] != null) && (Session["REFRESH_VIEW_SCHEDULES"].ToString() != "")) {
                // Clear the refresh flag first.
                Session.Add("REFRESH_VIEW_SCHEDULES", "");
                // Rebind the grid.
                btnSearch_OnClick(null, null);
            }
        }
    }
    //protected void lstCustomerSelector_SelectedIndexChanged(object sender, EventArgs e) {
    //    lstCustServices.DataSource = Service.GetMSorVSServicesByCustomerId(int.Parse(lstCustomerSelector.SelectedCustomerId));
    //    //lstCustEqTypes.DataSource = Equipmenttype.GetEquipmentTypesByCustomerId(int.Parse(lstCustomerSelector.SelectedCustomerId));
    //    reqTailNumber.Enabled = false;
    //    lstCustServices.DataBind();
    //    //lstCustEqTypes.DataBind();
    //    reqTailNumber.Enabled = true;
    //}
    protected void SetChecksInListBox(RadListBox lst, string sLeaderText, string sText) {
        if (sText != "") {
            string vals = sText.Substring(sLeaderText.Length, sText.Length - sLeaderText.Length - 1);
            StringTokenizer tokens = new StringTokenizer(vals, ", ");
            foreach (string token in tokens) {
                RadListBoxItem item = lst.FindItemByValue(token);
                if (item != null) {
                    item.Checked = true;
                }
            }
        }
    }
    //protected void lstCustomer_SelectedIndexChanged(object sender, EventArgs e) {
    //    lstEquipment.Items.Clear();
    //    lstEquipment.Text = "";
    //}
    //protected void lstCustomer_DataBound(object sender, EventArgs e) {
    //    RadComboBox lst = (RadComboBox)sender;
    //    if (lst.Items[0].Value != "") {
    //        RadComboBoxItem item = new RadComboBoxItem("", "-1");
    //        lst.Items.Insert(0, item);
    //    }
    //}
    //protected void lstEquipment_DataBound(object sender, EventArgs e) {
    //    // Insert a blank space.
    //    RadComboBox lst = (RadComboBox)sender;
    //    if ((lst.Items.Count > 0) && (lst.Items[0].Value != "-1")) {
    //        RadComboBoxItem item = new RadComboBoxItem("", "-1");
    //        lst.Items.Insert(0, item);
    //    }
    //}
    //protected void lstEquipment_ItemDataBound(object sender, RadComboBoxItemEventArgs e) {
    //    // Set the Text and Value property of every item
    //    e.Item.Text = ((ATGDB.Equipment)e.Item.DataItem).Tailnumber;
    //    e.Item.Value = ((ATGDB.Equipment)e.Item.DataItem).Equipmentid.ToString();
    //}
    protected void SetParams() {
        // Set the rest of the parameters.
        ObjectDataSource1.SelectParameters["customerId"].DefaultValue = Profile.Customerid;// lstCustomerSelector.SelectedCustomerId;
        ObjectDataSource1.SelectParameters["equipmentId"].DefaultValue = 
            Equipment.GetEquipmentIdByTailNumber(int.Parse(Profile.Customerid/*lstCustomerSelector.SelectedCustomerId*/), txtTailNumber.Text).ToString();
        ObjectDataSource1.SelectParameters["dtFromDate"].DefaultValue = dtFrom.SelectedDate.Value.ToShortDateString();
        if (dtTo.SelectedDate == null) {
            ObjectDataSource1.SelectParameters["dtToDate"].DefaultValue = dtFrom.SelectedDate.Value.ToShortDateString();
        } else {
            ObjectDataSource1.SelectParameters["dtToDate"].DefaultValue = dtTo.SelectedDate.Value.ToShortDateString();
        }
        ObjectDataSource1.SelectParameters["futureOption"].DefaultValue = chkFuture.Checked.ToString();
        ObjectDataSource1.SelectParameters["services"].DefaultValue = Serviceschedule.GetServicesDataViewFilter(lstCustServices);
        ObjectDataSource1.SelectParameters["eqTypes"].DefaultValue = "";// Serviceschedule.GetEqTypesDataViewFilter(lstCustEqTypes);
    }
    protected void RadGrid1_DataBinding(object sender, EventArgs e) {
        SetParams();
    }
    protected void btnSearch_OnClick(object sender, EventArgs e) {
        RadAjaxLoadingPanel1.Enabled = true;
        if (dtTo.SelectedDate == null) {
            dtTo.SelectedDate = dtFrom.SelectedDate;
        }
        if ((txtTailNumber.Text != "") && (txtTailNumber.Text != "0") && (txtTailNumber.Text.IndexOf("-") == -1) &&
            (Equipment.GetEquipmentIdByTailNumber(int.Parse(Profile.Customerid/*lstCustomerSelector.SelectedCustomerId*/), txtTailNumber.Text) != -1)) {
            // Validation
            reqTailNumber.IsValid = true;
            reqTailNumber.ErrorMessage = "Please Enter a Tail Number";
            // Datasource
            RadGrid1.DataSourceID = "ObjectDataSource1";
            // Show Grid
            //RadGrid1.Visible = true;
            // Clear cache.
            ScheduleHelper.Instance.ViewPageKey = hiddenPageKey.Value;
            System.Web.HttpContext.Current.Session.Add(Serviceschedule.VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey, "");
            //ObjectDataSource1.DataBind();
            RadGrid1.DataBind();
        } else {
            // Validation
            reqTailNumber.IsValid = false;
            reqTailNumber.ErrorMessage = "Invalid Tail Number";
            // Hide Grid
            //RadGrid1.Visible = false;
        }
        RadAjaxLoadingPanel1.Enabled = false;
    }
    // For coloring the rows.
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e) {
        if (e.Item is GridDataItem) {
            if (!e.Item.IsInEditMode) {
                GridDataItem row = e.Item as GridDataItem;
                var dataRow = (e.Item.DataItem as System.Data.DataRowView).Row;

                // COLOR THE ROWS, HIDE THE DELETE BUTTON.
                TableCell cell = row["StatusColumn"];
                TableCell logCell = row["SimpleLogColumn"];
                TableCell holdCell = row["OnHoldColumn"];

                // First, is it VERIFIED?
                if ((dataRow[23].ToString().ToUpper() != "BLUE") && (dataRow[23].ToString().ToUpper() != "GRAY")) {
                    TableCell verifyCell = row["ApprovaluseridColumn"];
                    bool verified = verifyCell.Text != "&nbsp;";
                    if (verified == false) {
                        // Make the font yellow
                        row.ForeColor = System.Drawing.Color.White;
                    }
                }

                if (holdCell.Text.ToUpper() == "TRUE") {
                    // Readonly mode - ON HOLD.
                    holdCell.Text = "On Hold";
                    cell = row["DeleteCommandColumn"];
                    cell.Controls.Clear();
                    cell = row["EditCommandColumn"];
                    cell.Controls.Clear();
                    cell = row["CancelColumn"];
                    cell.Controls.Clear();
                    cell = row["EX1Column"];
                    cell.Controls.Clear();
                    // Hide the simple log option.
                    logCell.Controls.Clear();
                } else {
                    holdCell.Text = "";
                }
                //cell = row["StatusColumn"];

                if (dataRow[23].ToString().ToUpper() == "BLUE") {
                    // NON-SCHEDULED - NEXT ONE
                    row.BackColor = ColorUtils.GetSS_NonScheduledColor(Profile.SiteTheme);
                    cell = row["DeleteCommandColumn"];
                    cell.Controls.Clear();
                    cell = row["EX1Column"];
                    cell.Controls.Clear();
                    cell = row["CancelColumn"];
                    cell.Controls.Clear();
                    // Hide the simple log option.
                    logCell.Controls.Clear();
                } else if (dataRow[23].ToString().ToUpper() == "GRAY") {
                    // NON-SCHEDULE - FUTURE CAST
                    row.BackColor = ColorUtils.GetSS_FutureColor(Profile.SiteTheme);
                    // Can't edit or Delete.
                    cell = row["DeleteCommandColumn"];
                    cell.Controls.Clear();
                    cell = row["EditCommandColumn"];
                    cell.Controls.Clear();
                    cell = row["EX1Column"];
                    cell.Controls.Clear();
                    cell = row["CancelColumn"];
                    cell.Controls.Clear();
                    // Hide the simple log option.
                    logCell.Controls.Clear();
                } else if (dataRow[23].ToString().ToUpper() == "RED") {
                    // COMPLETED, ZONE EXCEPTION
                    row.BackColor = ColorUtils.GetSS_CompletedExColor(Profile.SiteTheme);
                    if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_DELETE_LOGGED_SS)) {
                        // Can't edit or Delete completed SS records without security role.
                        cell = row["DeleteCommandColumn"];
                        cell.Controls.Clear();
                    }
                    cell = row["EditCommandColumn"];
                    cell.Controls.Clear();
                    cell = row["CancelColumn"];
                    cell.Controls.Clear();
                    // Hide the simple log option.
                    logCell.Controls.Clear();
                } else if (dataRow[23].ToString().ToUpper() == "GREEN") {
                    // SCHEDULED
                    row.BackColor = ColorUtils.GetSS_ScheduledColor(Profile.SiteTheme);
                    //cell = row["DeleteCommandColumn"];
                    cell = row["EX1Column"];
                    cell.Controls.Clear();
                } else if (dataRow[23].ToString().ToUpper() == "YELLOW1") {
                    // Completed Schedules - NON EDITABLE - YELLOW
                    row.BackColor = ColorUtils.GetSS_CompletedColor(Profile.SiteTheme);
                    if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_DELETE_LOGGED_SS)) {
                        // Can't edit or Delete completed SS records without security role.
                        cell = row["DeleteCommandColumn"];
                        cell.Controls.Clear();
                    }
                    cell = row["EditCommandColumn"];
                    cell.Controls.Clear();
                    cell = row["EX1Column"];
                    cell.Controls.Clear();
                    cell = row["CancelColumn"];
                    cell.Controls.Clear();
                    // Hide the simple log option.
                    logCell.Controls.Clear();
                } else if (dataRow[23].ToString().ToUpper() == "YELLOW2") {
                    // Canceled Schedules - NON EDITABLE - YELLOW
                    row.BackColor = ColorUtils.GetSS_CompletedColor(Profile.SiteTheme);
                    // Strike out columns.
                    //row.Font.Strikeout = true;
                    TableCell strikeCell = row["TailnumberColumn"];
                    strikeCell.Font.Strikeout = true;
                    //strikeCell = row["CustomerNameColumn"];
                    //strikeCell.Font.Strikeout = true;
                    strikeCell = row["EquipmentdescrColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["ServicedescrColumn2"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["LocationColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["IntervalColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["ServicedateColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["ServicetimeColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["CompleteddateColumn"];
                    strikeCell.Font.Strikeout = true;

                    if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_DELETE_LOGGED_SS)) {
                        // Can't edit or Delete completed SS records without security role.
                        cell = row["DeleteCommandColumn"];
                        cell.Controls.Clear();
                    }
                    cell = row["EditCommandColumn"];
                    cell.Controls.Clear();
                    cell = row["EX1Column"];
                    cell.Controls.Clear();
                    cell = row["CancelColumn"];
                    cell.Controls.Clear();
                    // Hide the simple log option.
                    logCell.Controls.Clear();
                }                
            }
        }
    }
    protected void btnEdit_OnClick(object sender, EventArgs e) {
        GridDataItem item = (GridDataItem)((ImageButton)sender).NamingContainer;
        item.Edit = true;
        RadGrid1.DataBind();
    }
    protected void btnSave_OnClick(object sender, EventArgs e) {
        GridEditableItem item = (GridEditableItem)((ImageButton)sender).NamingContainer;
        item.Edit = false;
        ATGDB.Serviceschedule ss = new ATGDB.Serviceschedule();
        item.UpdateValues(ss);

        if (ss.Servicescheduleid < 0) {
            // Inserting new one
            Serviceschedule.InsertServiceSchedule(null, 
                ss.Equipmentid, ss.Serviceid, 
                ss.Servicedate, Convert.ToDateTime(ss.Servicetime), ss.Comments);
        } else {
            // Updating existing one
            Serviceschedule.UpdateServiceSchedule(ss.Servicescheduleid, 
                ss.Servicedate, Convert.ToDateTime(ss.Servicetime), ss.Comments);
        }
        // Clear cache.
        System.Web.HttpContext.Current.Session.Add(Serviceschedule.VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey, "");
        RadGrid1.DataBind();
        RadAjaxManager1.RaisePostBackEvent("");
    }
    protected void btnDelete_OnClick(object sender, EventArgs e) {
        //RadWindowManager1.RadConfirm("Delete Schedule?", "confirmCallBackFn", 200, 180, null, "Delete?");

        GridEditableItem item = (GridEditableItem)((ImageButton)sender).NamingContainer;
        //ATGDB.Serviceschedule ss = new ATGDB.Serviceschedule();
        //item.UpdateValues(ss);
        item.Selected = true;
        int ssId = int.Parse(RadGrid1.SelectedValue.ToString());

        // Delete
        if (ssId > 0) {
            Serviceschedule.DeleteServiceSchedule(ssId);

            // Clear cache.
            System.Web.HttpContext.Current.Session.Add(Serviceschedule.VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey, "");
            RadGrid1.DataBind();
        }
    }
    protected void btnCancel_OnClick(object sender, EventArgs e) {
        GridDataItem item = (GridDataItem)((ImageButton)sender).NamingContainer;
        item.Edit = false;
        RadGrid1.DataBind();
    }
    protected void SimpleLogWork_ValueChanged(object sender, EventArgs e) {
        // Create the Work Log from the hidden fields.
        string ssId = hiddenSimpleLogWorkSsId.Value;
        if ((Session["LAST_SSID"] == null) || (Session["LAST_SSID"].ToString() != ssId)) {
            //Devart.Data.MySql.MySqlMonitor m = new MySqlMonitor();
            //m.IsActive = true;
            Session.Add("LAST_SSID", ssId);
            //string jobCodeId = hiddenSimpleLogWorkJobcodeid.Value;

            // !!!! There is a RARE case where this hiddel "WorkWorkDate" field somehow is not
            // allowing conversion of a perfectly valid date. Default to today in these cases.
            DateTime wkDate;
            if (!DateTime.TryParse(hiddenSimpleLogWorkWorkDate.Value, out wkDate)) {
                wkDate = PageUtils.LoadDateTimeChars(hiddenSimpleLogWorkWorkDate.Value);
            }
            DateTime startTime;
            if (!DateTime.TryParse(hiddenSimpleLogWorkStartTime.Value, out startTime)) {
                startTime = PageUtils.LoadDateTimeChars(hiddenSimpleLogWorkStartTime.Value);
            }
            DateTime endTime;
            if (!DateTime.TryParse(hiddenSimpleLogWorkEndTime.Value, out endTime)) {
                endTime = PageUtils.LoadDateTimeChars(hiddenSimpleLogWorkEndTime.Value);
            }
                                       
            double wQty = -1.00; 
            string area = ""; 
            if (hiddenSimpleLogWorkArea.Value != "") {
                area = hiddenSimpleLogWorkArea.Value;
            }
            if (hiddenSimpleLogWorkQuantity.Value != "") {
                wQty = double.Parse(hiddenSimpleLogWorkQuantity.Value);
            }
            string notes = hiddenSimpleLogWorkNotes.Value;
            string exSelections = hiddenSimpleLogWorkExceptions.Value;
            string exCompletedSelections = hiddenSimpleLogWorkCompletedExceptions.Value;
            string exNotes = hiddenSimpleLogWorkExNotes.Value;
            string completedNotes = hiddenSimpleLogWorkCompletedNotes.Value;

            try {
                // Process the Service Schedule. This is a very important procedure!
                Servicesectionrule.ProcessServiceSchedule(ssId, Profile.UserName, wkDate, startTime, endTime, wQty, area, notes, exSelections, exNotes, exCompletedSelections, completedNotes);
            } catch (Exception ex) {
                RadWindowManager1.RadAlert(ex.Message, null ,null, "Error", "");
            }
            // Finally, refresh.
            btnSearch_OnClick(null, null);
        }
    }
    protected void CancelSS_ValueChanged(object sender, EventArgs e) {
        string ssId = hiddenCancelSS_SsId.Value;
        string notes = hiddenCancelSS_Notes.Value;

        DateTime cancelledDt = DateTime.Now.Date;
        DateTime rescheduleDt;
        if (!DateTime.TryParse(hiddenCancelSS_RescheduleDate.Value, out rescheduleDt)) {
            rescheduleDt = PageUtils.LoadDateTimeChars(hiddenCancelSS_RescheduleDate.Value);
        }   

        if ((Session["LAST_CANCEL_SSID"] == null) || (Session["LAST_CANCEL_SSID"].ToString() != ssId)) {
            Session.Add("LAST_CANCEL_SSID", ssId);
            // Cancel the service schedule.
            ATGDB.Serviceschedule.CancelServiceSchedule(ssId, cancelledDt.Date, rescheduleDt, notes);

            // Finally, refresh.
            btnSearch_OnClick(null, null);
        }
    }
    protected bool GetEX2Visible() {
        if ((Convert.ToBoolean(Eval("Hasactiveexception")) == false) && 
            (Convert.ToBoolean(Eval("Hadexception")) == true)) {
            return true;
        } else {
            return false;
        }
    }
    protected void btnExportToExcel_OnClick(object sender, EventArgs e) {
        if (RadGrid1.MasterTableView.Items.Count > 0) {
            RadGrid1.MasterTableView.Columns.FindByUniqueName("EditCommandColumn").Display = false;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("SimpleLogColumn").Display = false;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("CancelColumn").Display = false;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("EX1Column").Display = false;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("EX2Column").Display = false;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("DeleteCommandColumn").Display = false;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ServicedescrColumn1").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ServicedescrColumn1").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ServicedescrColumn2").Display = false;
            RadGrid1.Rebind();
            RadGrid1.MasterTableView.ExportToExcel();
        }
    }
  
}