﻿using System;
using System.Linq;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Schedules_ServiceScheduleManager : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_MANAGER;
    }
    // For postbacks in the JavaScript in the page.
    protected string PostBackString;
    protected string SearchPostBackString;

    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // For postbacks in the JavaScript in the page.
        PostBackString = Page.ClientScript.GetPostBackEventReference(this, "");
        SearchPostBackString = Page.ClientScript.GetPostBackEventReference(btnSearch, "OnClick");
        //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
        //m.IsActive = true;

        if (!Page.IsPostBack) {
            // Load some defaults. 
            Session.Add("REFRESH_SERVICESCHEDULE_MANAGER_DIALOG", "");
            // Work date is today.
            dtServiceDate.SelectedDate = DateTime.Now.Date;
            //dtWorkDate.MaxDate = DateTime.Now.Date;

            lblCompCustInfo.Text = "**Selected Division/Customer: <strong>" + Profile.Companydivisiondescription + " / " + Profile.Customerdescription + "</strong>";
        } else {
            if ((Session["REFRESH_SERVICESCHEDULE_MANAGER_DIALOG"] != null) && (Session["REFRESH_SERVICESCHEDULE_MANAGER_DIALOG"].ToString() != "")) {
                LoadServiceSchedules();
            }
        }
    }
    protected void btnSearch_OnClick(object sender, EventArgs e) {
        LoadServiceSchedules();
    }
    protected void LoadServiceSchedules() {
        RadGrid1.DataSourceID = "ObjectDataSource1";
        if (dtServiceDate.SelectedDate == null) {
            dtServiceDate.SelectedDate = DateTime.Now.Date;
        }
        ObjectDataSource1.SelectParameters["serviceDate"].DefaultValue = dtServiceDate.SelectedDate.Value.Date.ToShortDateString();
        ObjectDataSource1.SelectParameters["unapprovedOnly"].DefaultValue = chkUnapprovedOnly.Checked.ToString();
        RadGrid1.DataBind();
        if (RadGrid1.Items.Count > 0) {
            RadGrid1.Items[0].Selected = true;
        }
    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e) {
        if (e.Item is GridDataItem) {
            if (!e.Item.IsInEditMode) {
                // COLOR THE ROWS, HIDE THE DELETE BUTTON.
                GridDataItem row = e.Item as GridDataItem;
                TableCell apprCell = row["ApproveColumn"];

                // Handy vars.

                DateTime? apprDt = null;
                if (((DataRowView)(e.Item.DataItem)).Row["Approvaldate"].ToString() != "") {
                    apprDt = Convert.ToDateTime(((DataRowView)(e.Item.DataItem)).Row["Approvaldate"].ToString());
                }

                if (apprDt == null) {
                    // Modify label is visible
                    // So, do nothing
                } else {
                    // Clear the modify cell
                    apprCell.Controls.Clear();
                    apprCell.Text = "";
                }
            }
        }
    }
    protected void btnExportToExcel_OnClick(object sender, EventArgs e) {
        if (RadGrid1.MasterTableView.Items.Count > 0) {
            RadGrid1.MasterTableView.Columns.FindByUniqueName("TailColumn1").Display = false;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("TailColumn2").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("EqtypeColumn").Display = true;
            RadGrid1.Rebind();
            RadGrid1.MasterTableView.ExportToExcel();
        }
    }
}