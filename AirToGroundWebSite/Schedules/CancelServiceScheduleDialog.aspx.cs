﻿using System;
using ATGDB;

public partial class Schedules_CancelServiceScheduleDialog : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_CANCEL;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);

        if (!Page.IsPostBack) {
            if (Request.QueryString["ssId"] != null) {
                hiddenSsId.Value = Request.QueryString["ssId"];
                if (Request.QueryString["sqitemId"] != null) {
                    hiddenSQItemId.Value = Request.QueryString["sqitemId"];
                }
                // Header summary
                ATGDB.Serviceschedule ss = Serviceschedule.GetServiceSchedule(int.Parse(Request.QueryString["ssId"].ToString()));
                lblCustomer.Text = ss.Service.Customer.Description;
                lblTailNumber.Text = ss.Equipment.Tailnumber;
                lblServiceDescr.Text = ss.Service.Description;
                lblServiceDate.Text = ss.Servicedate.ToShortDateString();
                // Defaults
                txtCancelledDate.Text = DateTime.Now.Date.ToShortDateString();
                //txtCancelledDate.MinDate = DateTime.Now.Date;
                txtRescheduleDate.MinDate = DateTime.Now.Date;
                //txtRescheduleDate.SelectedDate = DateTime.Now.Date.AddDays(1);
                //txtRescheduleDate.Focus();
                //txtCancelledNotes.Focus();
            }
        }
    }
}