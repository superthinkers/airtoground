﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SimpleLogWork.aspx.cs" Inherits="Schedules_SimpleLogWork"
    Buffer="true" Strict="true" Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
        function doComplete() {
            // Create the argument that will be returned to the parent page
            var oArg = new Object();

            // Let's the calling form know the user is committing the changes, versus cancelling.
            oArg.OK = "TRUE";

            // Get the various fields.
            oArg.SsId = document.getElementById("hiddenSsId").value;

            oArg.SQItemId = document.getElementById("hiddenSQItemId").value;

            //var lstJobCodes = $find("<       %= lstJobCodes.ClientID %>");
            //if (lstJobCodes.get_value()) {
            //    oArg.Jobcodeid = lstJobCodes.get_value();
            //} else {
            //    oArg.Jobcodeid = "-1";
            //}

            var txtWorkDate = $find("<%= txtWorkDate.ClientID %>");
            if (txtWorkDate.get_selectedDate()) {
                oArg.WorkDate = txtWorkDate.get_selectedDate().toLocaleString();
            } else {
                oArg.WorkDate = "";
            }

            var txtStartTime = $find("<%= txtStartTime.ClientID %>");
            if (txtStartTime && txtStartTime.get_selectedDate()) {
                //var time = txtStartTime.get_timeView().getTime().toLocaleTimeString().slice(0, txtStartTime.get_timeView().getTime().toLocaleTimeString().lastIndexOf("00") - 3) +
                //    " " + txtStartTime.get_timeView().getTime().toLocaleTimeString().slice(txtStartTime.get_timeView().getTime().toLocaleTimeString().lastIndexOf("00") + 4);
                oArg.StartTime = txtStartTime.get_selectedDate().toLocaleString();
            } else {
                oArg.StartTime = "";
            }

            var txtEndTime = $find("<%= txtEndTime.ClientID %>");
            if (txtEndTime && txtEndTime.get_selectedDate()) {
                //var time = txtEndTime.get_timeView().getTime().toLocaleTimeString().slice(0, txtEndTime.get_timeView().getTime().toLocaleTimeString().lastIndexOf("00") - 3) +
                //    " " + txtEndTime.get_timeView().getTime().toLocaleTimeString().slice(txtEndTime.get_timeView().getTime().toLocaleTimeString().lastIndexOf("00") + 4);
                oArg.EndTime = txtEndTime.get_selectedDate().toLocaleString();
            } else {
                oArg.EndTime = "";
            }

            if (document.getElementById("txtQuantity")) {
                oArg.Quantity = document.getElementById("txtQuantity").value;
            } else {
                oArg.Quantity = "";
            }

            var cmbArea1 = $find("<%= cmbArea.ClientID %>");
            var reqArea1 = document.getElementById("reqArea"); // Validator
            if (cmbArea1 && cmbArea1.get_value()) {
                oArg.Area = cmbArea1.get_value();
            } else {
                oArg.Area = "";
            }

            oArg.Notes = document.getElementById("txtNotes").value;

            if (document.getElementById("hiddenSelectedSections")) {
                oArg.Exceptions = document.getElementById("hiddenSelectedSections").value;
            } else {
                oArg.Exceptions = "";
            }

            if (document.getElementById("txtExNotes")) {
                oArg.ExNotes = document.getElementById("txtExNotes").value;
            } else {
                oArg.ExNotes = "";
            }

            if (document.getElementById("hiddenCompletedSections")) {
                oArg.CompletedExceptions = document.getElementById("hiddenCompletedSections").value;
                oArg.CompletedNotes = document.getElementById("txtCompletedNotes").value;
            } else {
                oArg.CompletedExceptions = "";
                oArg.CompletedNotes = "";
            }

            // Get a reference to the current RadWindow
            var oWnd = GetRadWindow();

            var msg = "";

            // VALIDATION

            // Work Date is Required
            if (oArg.WorkDate == "") {
                msg = "The Worked Date is Required.\n";
            }
            // At least Start/End Time AND/OR Quantity is Required
            if (oArg.StartTime == "" && oArg.EndTime == "" && oArg.Quantity == "") {
                msg = msg + "A Start Time and End Time, AND/OR Quantity is Required.\n";
            }
            // Missing the End Time
            if (oArg.StartTime != "" && oArg.EndTime == "") {
                msg = msg + "The End Time is Required.\n";
            }
            // Missing the Start Time
            if (oArg.StartTime == "" && oArg.EndTime != "") {
                msg = msg + "The Start Time is Required.\n";
            }
            // Missing the Quantity
            if (oArg.Quantity == "") {
                msg = msg + "The Quantity is Required. May be Zero.\n";
            }
            // Area for this service is required
            if ((oArg.Area == "") && (reqArea1.isvalid == false)) {
                msg = msg + "The Division Area is Required.\n";
            }
            // One or the other.
            //if ((oArg.StartTime != "" && oArg.EndTime == "" && oArg.Quantity != "") || 
            //    (oArg.StartTime == "" && oArg.EndTime != "" && oArg.Quantity != "") || 
            //    (oArg.StartTime != "" && oArg.EndTime != "" && oArg.Quantity != "")) {
            //    msg = msg + "Choose Either a Start Time and End Time, OR Quantity\n";
            //}
            // Validate exception fields.
            if ((oArg.Exceptions != "" && oArg.ExNotes == "") || 
                (oArg.ExNotes != "" && oArg.Exceptions == "")) {
                msg = msg + "Exceptions Require Ex Notes, and Ex Notes Require Exceptions.\n";
            }
            
            if (msg == "") {
                oWnd.close(oArg);
            } else {
                alert(msg);
            }

            return false;
        }
        function doCancel() {
            var oWnd = GetRadWindow();
            var oArg = new Object();
            oArg.OK = "FALSE";
            oWnd.close(oArg);

            return false;
        }
    </script>
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server" AsyncPostBackTimeout="600"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin" ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator2" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <asp:HiddenField ID="hiddenSQItemId" runat="server" />
        <asp:HiddenField ID="hiddenSsId" runat="server" Value="" />
        <div id="mainDiv" runat="server" style="margin: 10px 10px 10px 10px; width: 450px; height: 530px;">
            <div style="text-align:center;width:100%">
                <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Work Log Entry Dialog"></asp:Label>
            </div>
            <div style="position: relative; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 98%">
                <table style="border-collapse: separate; width: 100%">
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblCustomerLbl" runat="server" SkinID="InfoLabel" Text="Customer:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCustomer" runat="server" SkinID="InfoLabelBold" Text="Customer"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblTailNumberLbl" runat="server" SkinID="InfoLabel" Text="Tail Number:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblTailNumber" runat="server" SkinID="InfoLabelBold" Text="Tail Number"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblServiceDescrLbl" runat="server" SkinID="InfoLabel" Text="Service:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblServiceDescr" runat="server" SkinID="InfoLabelBold" Text="Service"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblServiceDateLbl" runat="server" SkinID="InfoLabel" Text="Service Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblServiceDate" runat="server" SkinID="InfoLabelBold" Text="Service Date"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: relative; text-align: left; margin: 5px 0px 5px 0px;
                border: 1px solid Navy; width: 98%; background-color: #F2F0F2">
                <table id="tblFields" runat="server" style="border-collapse: separate; width: 100%">
                    <%--<tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblJobCode" runat="server" SkinID="InfoLabel" Text="Job Code:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadComboBox ID="lstJobCodes" runat="server" DataValueField="Jobcodeid" DataTextField="Code" Width="78%"
                                HighlightTemplatedItems="true" AutoPostBack="true" OnSelectedIndexChanged="lstJobCodes_SelectedIndexChanged">
                                <HeaderTemplate>
                                    <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                        width: 30%">
                                        Code</div>
                                    <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                        width: 70%">
                                        Description</div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="lblInfo" style="position: relative; float: left; width: 30%">
                                        <%# Eval("Code") %>
                                    </div>
                                    <div class="lblInfo" style="position: relative; float: left; width: 70%">
                                        <%# Eval("Jobcodedescr")%>
                                    </div>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                            <br />
                            <asp:Label ID="lblJobCodeName" runat="server" SkinID="SmallLabel" Text=""></asp:Label>
                        </td>
                    </tr>--%>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblWorkDate" runat="server" SkinID="InfoLabel" Text="Work Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadDatePicker ID="txtWorkDate" runat="server" DateInput-DateFormat="M/d/yyyy" DateInput-DisplayDateFormat="M/d/yyyy">
                            </telerik:RadDatePicker>
                        </td>
                    </tr>
                    <tr id="st1" runat="server">
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblStartTime" runat="server" SkinID="InfoLabel" Text="Start Time:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadTimePicker ID="txtStartTime" runat="server">
                            </telerik:RadTimePicker>
                        </td>
                    </tr>
                    <tr id="st2" runat="server">
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblEndTime" runat="server" SkinID="InfoLabel" Text="End Time:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadTimePicker ID="txtEndTime" runat="server">
                            </telerik:RadTimePicker>
                        </td>
                    </tr>
                    <tr id="q1" runat="server">
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblQuantity" runat="server" SkinID="InfoLabel" Text="Quantity:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadNumericTextBox ID="txtQuantity" runat="server"
                                MinValue="0.00" NumberFormat-DecimalDigits="2" Value="0.00">
                            </telerik:RadNumericTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblArea" runat="server" SkinID="InfoLabel" Text="Division Area:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadComboBox ID="cmbArea" runat="server" 
                                AppendDataBoundItems="true" Width="150px" AutoPostBack="false"
                                OnSelectedIndexChanged="cmbArea_SelectedIndexChanged" 
                                DataTextField="Description" DataValueField="Divisionareaid">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox>
                            <asp:RequiredFieldValidator ID="reqArea" runat="server" ControlToValidate="cmbArea"
                                ErrorMessage="*" CssClass="stdValidator" SetFocusOnError="true">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblNotes" runat="server" SkinID="InfoLabel" Text="Notes:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtNotes" runat="server" Width="98%" MaxLength="255" Rows="10"
                                Height="40px" Wrap="true" TextMode="MultiLine">
                            </asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <div id="ex1" runat="server" style="text-align: left; width: 98%; background-color: #F2F0F2; margin: 0px 10px 0px 10px">
                    <div class="stdLabel" style="text-align: left">
                        Did you COMPLETE These Unfinished Exceptions?
                    </div>
                    <asp:HiddenField ID="hiddenCompletedSections" runat="server" Value="" />
                    <telerik:RadListBox ID="lstCompletedSections" runat="server" CheckBoxes="true" Width="98%"
                        DataKeyField="Sectionid" DataTextField="Description" DataValueField="Sectionid"
                        AutoPostBack="true" OnItemCheck="lstCompletedSections_ItemCheck" Height="50px" >
                    </telerik:RadListBox>
                    <br />
                </div>
                <div id="ex4" runat="server" style="text-align: left; width: 98%; background-color: #F2F0F2; margin: 0px 10px 0px 10px">
                    <div class="stdLabel" style="text-align: left">
                        Exception Complete / Close Notes (required if you are Completing / Closing Exceptions)
                    </div>
                    <asp:TextBox ID="txtCompletedNotes" runat="server" Width="98%" MaxLength="255" Rows="10"
                        Height="40px" Wrap="true" TextMode="MultiLine">
                    </asp:TextBox>
                    <br />
                </div>
                <div id="ex2" runat="server" style="text-align: left; width: 98%; background-color: #F2F0F2; margin: 0px 10px 0px 10px">
                    <div class="stdLabel" style="text-align: left">
                        Are there any New Exceptions? (optional)
                    </div>
                    <asp:HiddenField ID="hiddenSelectedSections" runat="server" Value="" />
                    <telerik:RadListBox ID="lstSections" runat="server" CheckBoxes="true" Width="99%"
                        DataKeyField="Sectionid" DataTextField="Description" DataValueField="Sectionid"
                        AutoPostBack="true" OnItemCheck="lstSections_ItemCheck" Height="50px">
                    </telerik:RadListBox>
                    <br />
                </div>
                <div id="ex3" runat="server" style="text-align: left; width: 98%; background-color: #F2F0F2; margin: 0px 10px 0px 10px">
                    <div class="stdLabel" style="text-align: left">
                        Exception Notes (required if you had an Exception)
                    </div>
                    <asp:TextBox ID="txtExNotes" runat="server" Width="98%" MaxLength="255" Rows="10"
                        Height="40px" Wrap="true" TextMode="MultiLine">
                    </asp:TextBox>
                </div>
                <br />
                <div style="text-align: center;width:100%">
                    <asp:Button ID="Submit" runat="server" Text="Submit" OnClientClick="doComplete(); return false;" CausesValidation="true" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Cancel" runat="server" Text="Cancel" OnClientClick="doCancel(); return false;" CausesValidation="false" />
                </div>
                <br />
            </div>
        </div>
    </form>
</body>
</html>
