﻿using System;
using ATGDB;

public partial class Schedules_ModifySchedule : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_RESCHEDULE_WIZ;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            lstEquipment.DataSource = Equipment.GetEquipmentNotOnHold(int.Parse(Profile.Customerid));
            lstEquipment.DataBind();
        }
    }
    protected void Wizard1_OnActiveStepChanged(object sender, EventArgs e) {
        if (Wizard1.ActiveStepIndex == 1) {
            RadGrid1.DataSourceID = "ObjectDataSource1";
            RadGrid1.DataBind();
        }
    }
    protected void RadGrid1_DataBinding(object sender, EventArgs e) {
        ObjectDataSource1.SelectParameters["equipmentId"].DefaultValue = lstEquipment.SelectedValue;
        ObjectDataSource1.SelectParameters["dtFromDate"].DefaultValue = dtFromServiceDate.SelectedDate.Value.ToShortDateString();
        if (dtToServiceDate.SelectedDate == null) {
            ObjectDataSource1.SelectParameters["dtToDate"].DefaultValue = dtFromServiceDate.SelectedDate.Value.ToShortDateString();
        } else {
            ObjectDataSource1.SelectParameters["dtToDate"].DefaultValue = dtToServiceDate.SelectedDate.Value.ToShortDateString();
        }
    }
}