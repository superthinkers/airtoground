﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/Default.master"
    AutoEventWireup="true" CodeFile="ViewSchedules.aspx.cs" Inherits="Schedules_ViewSchedules" Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetServiceSchedulesByDatesAndEquipOrServiceType"
        TypeName="ATGDB.Serviceschedule" EnableCaching="false">
        <SelectParameters>
            <asp:Parameter Name="customerId" Type="Int32" />
            <asp:Parameter Name="equipmentId" Type="Int32" />
            <asp:Parameter Name="dtFromDate" Type="DateTime" />
            <asp:Parameter Name="dtToDate" Type="DateTime" />
            <asp:Parameter Name="futureOption" Type="Boolean" />
            <asp:Parameter Name="services" Type="String" DefaultValue="" />
            <asp:Parameter Name="eqTypes" Type="String" DefaultValue="" />
            <asp:Parameter Name="masterScheduleMode" Type="Boolean" DefaultValue="false" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <%--<devart:DbLinqDataSource ID="DBLinqDataSource2" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="" OrderBy="Description"
        TableName="Divisionareas">
    </devart:DbLinqDataSource>--%>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="true"
        Skin="Office2007">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="DoRequestStart" OnResponseEnd="DoResponseEnd"/>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1"/>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1"/>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="lstCustomerSelector">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lstCustServices" />
                    <telerik:AjaxUpdatedControl ControlID="lstCustEqTypes" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnExportToExcel">
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ctrlServiceExDescr">
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSaveRed">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1"/>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancelRed">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1"/>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" ReloadOnShow="false" runat="server" EnableShadow="true" Modal="true"
        VisibleTitlebar="false" EnableViewState="false" AutoSize="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" NavigateUrl="SimpleLogWork.aspx"
                Behaviors="Close" OnClientClose="onClientClose1" ShowContentDuringLoad="false" VisibleStatusbar="false" Title="Log Work Done">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" NavigateUrl="CancelExceptionDialog.aspx"
                Behaviors="Close" OnClientClose="onClientClose2" ShowContentDuringLoad="false" VisibleStatusbar="false" Title="Exception Details">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" NavigateUrl="ExceptionDetailsView.aspx"
                ShowContentDuringLoad="false" VisibleStatusbar="false" Title="Exception Details">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow4" runat="server" NavigateUrl="CancelServiceScheduleDialog.aspx"
                Behaviors="Close" OnClientClose="onClientClose3" ShowContentDuringLoad="false" VisibleStatusbar="false" Title="Cancel Service Schedule">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow999" runat="server" ShowContentDuringLoad="false" VisibleStatusbar="false" Title="">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>    
    <script type="text/javascript">
        //MAKE SURE THAT THE FOLLOWING SCRIPT IS PLACED AFTER THE RADWINDOWMANAGER DECLARATION
        //Replace old radconfirm with a changed version.   
        var oldConfirm = radconfirm;
        //TELERIK
        //window.radconfirm = function(text, mozEvent)
        //We will change the radconfirm function so it takes all the original radconfirm attributes
        window.radconfirm = function (text, mozEvent, oWidth, oHeight, callerObj, oTitle) {
            var ev = mozEvent ? mozEvent : window.event; //Moz support requires passing the event argument manually   
            //Cancel the event   
            ev.cancelBubble = true;
            ev.returnValue = false;
            if (ev.stopPropagation) ev.stopPropagation();
            if (ev.preventDefault) ev.preventDefault();

            //Determine who is the caller   
            var callerObj = ev.srcElement ? ev.srcElement : ev.target;

            //Call the original radconfirm and pass it all necessary parameters   
            if (callerObj) {
                //Show the confirm, then when it is closing, if returned value was true, automatically call the caller's click method again.   
                var callBackFn = function (arg) {
                    if (arg) {
                        callerObj["onclick"] = "";
                        if (callerObj.click) callerObj.click(); //Works fine every time in IE, but does not work for links in Moz   
                        else if (callerObj.tagName == "A") //We assume it is a link button!   
                        {
                            try {
                                eval(callerObj.href)
                            }
                            catch (e) { }
                        }
                    }
                }
                //TELERIK
                //oldConfirm(text, callBackFn, 300, 100, null, null);       
                //We will need to modify the oldconfirm as well                
                oldConfirm(text, callBackFn, oWidth, oHeight, callerObj, oTitle);
            }
            return false;
        }
    </script>                      
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
        	function DoRequestStart(sender, args) {
        		var currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");

     			if (args.get_eventTarget() === "<%= btnSearch.UniqueID %>") {
     				currentUpdatedControl = "<%= RadGrid1.ClientID %>";
                }
     			//show the loading panel over the updated control
				currentLoadingPanel.show(currentUpdatedControl);
			}
        	function DoResponseEnd(sender, args) {
				var currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");

            	//hide the loading panel and clean up the global variables
            	if (currentLoadingPanel !== null)
            		currentLoadingPanel.hide(currentUpdatedControl);
            	currentUpdatedControl = null;
            	currentLoadingPanel = null;

            	if (args.get_eventTarget().indexOf("btnExportToExcel") >= 0 ||
                    args.get_eventTarget().indexOf("ctrlServiceExDescr") >= 0) {
            		args.set_enableAjax(false);
            	}
			}
        	function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }
            function openWin1(ssId, serviceId) {
                radopen("SimpleLogWork.aspx?ssId=" + ssId + "&serviceId=" + serviceId, "RadWindow1");
            }
            function openWin2(ssId, hasEx) {
                if ((ssId > 0) && (hasEx == "1")) {
                    radopen("CancelExceptionDialog.aspx?ssId=" + ssId, "RadWindow2");
                } else {
                    alert("No Exceptions");
                }
            }
            function openWin3(ssId, hasEx) {
                if ((ssId > 0) && (hasEx == "1")) {
                    radopen("ExceptionDetailsView.aspx?ssId=" + ssId, "RadWindow3");
                } else {
                    alert("No Exceptions");
                }
            }
            function openWin4(ssId) {
                if (ssId > 0) {
                    radopen("CancelServiceScheduleDialog.aspx?ssId=" + ssId, "RadWindow4");
                }
            }
            function onClientClose1(oWnd, args) {
                oWnd.set_width(100);
                oWnd.set_height(100);
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg && (arg.OK == "TRUE")) {
                    // Update the hidden fields. Will trigger value changed event.
                    var hidden = $get("<%= hiddenSimpleLogWorkSsId.ClientID %>");
                    hidden.value = arg.SsId;

                    // By setting the trigger field here to ssId, it shouldn't fire too often.
                    hidden = $get("<%= hiddenSimpleLogWorkTrigger.ClientID %>");
                    hidden.value = arg.SsId;

                    //hidden = $get("<%= hiddenSimpleLogWorkJobcodeid.ClientID %>");
                    //hidden.value = arg.Jobcodeid;

                    hidden = $get("<%= hiddenSimpleLogWorkWorkDate.ClientID %>");
                    hidden.value = arg.WorkDate;

                    hidden = $get("<%= hiddenSimpleLogWorkStartTime.ClientID %>");
                    hidden.value = arg.StartTime;

                    hidden = $get("<%= hiddenSimpleLogWorkEndTime.ClientID %>");
                    hidden.value = arg.EndTime;

                    hidden = $get("<%= hiddenSimpleLogWorkQuantity.ClientID %>");
                    hidden.value = arg.Quantity;

                    hidden = $get("<%= hiddenSimpleLogWorkArea.ClientID %>");
                    hidden.value = arg.Area;

                    hidden = $get("<%= hiddenSimpleLogWorkNotes.ClientID %>");
                    hidden.value = arg.Notes;

                    hidden = $get("<%= hiddenSimpleLogWorkExceptions.ClientID %>");
                    hidden.value = arg.Exceptions;

                    hidden = $get("<%= hiddenSimpleLogWorkCompletedExceptions.ClientID %>");
                    hidden.value = arg.CompletedExceptions;

                    hidden = $get("<%= hiddenSimpleLogWorkExNotes.ClientID %>");
                    hidden.value = arg.ExNotes;

                    hidden = $get("<%= hiddenSimpleLogWorkCompletedNotes.ClientID %>");
                    hidden.value = arg.CompletedNotes;                

                    // For debugging.
                    //alert(arg.SsId + " :: Dates / Times :: " +
                    //      arg.WorkDate + " -> " + arg.StartTime + " -> " + arg.EndTime + " :: Qty :: " +
                    //      arg.Quantity + " :: Notes :: " + arg.Notes + " :: Exception Info :: " + 
                    //      arg.Exceptions + " -> " + arg.ExNotes);

                    // Force a page postback. Causes hidden's OnValueChanged to fire.
                    <%= PostBackString %>
                }
            }
            function onClientClose2(oWnd, args) {
                <%= PostBackString %>
            }
            function onClientClose3(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg && (arg.OK == "TRUE")) {
                    // Update the hidden fields. Will trigger value changed event.
                    var hidden = $get("<%= hiddenCancelSS_SsId.ClientID %>");
                    hidden.value = arg.SsId;

                    hidden = $get("<%= hiddenCancelSS_RescheduleDate.ClientID %>");
                    hidden.value = arg.RescheduleDate;

                    hidden = $get("<%= hiddenCancelSS_Notes.ClientID %>");
                    hidden.value = arg.Notes;

                    // Force a page postback. Causes hidden's OnValueChanged to fire.
                    <%= PostBackString %>
                }
            }
            function ValidateDateRange(sender, e) {
                var dtFromDate = $find("<%= dtFrom.ClientID %>");
                var dtToDate = $find("<%= dtTo.ClientID %>");
  
                if ((dtFromDate.get_selectedDate() == null) && (dtToDate.get_selectedDate() != null)) {
                    alert("Please select the From/End Date");
                } else if ((dtFromDate.get_selectedDate() > dtToDate.get_selectedDate()) && (dtToDate.get_selectedDate() != null)) {
                    alert("The To/Start Date must be less than or equal to than the From/Begin Date");
                    dtFromDate.set_selectedDate(dtToDate.get_selectedDate());
                }
                return false;
            }
            function clickButton(e) {
                if (e.keyCode == 13) {
                    <%= SearchPostBackString %>
                    return false;
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <asp:HiddenField ID="hiddenPageKey" runat="server" Value="" ClientIDMode="Static" />
    <!-- SIMPLE LOG WORK -->
    <asp:HiddenField ID="hiddenSimpleLogWorkTrigger" runat="server" Value="&nbsp;" ClientIDMode="Static" OnValueChanged="SimpleLogWork_ValueChanged" />
    <asp:HiddenField ID="hiddenSimpleLogWorkSsId" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenSimpleLogWorkJobcodeid" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenSimpleLogWorkWorkDate" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenSimpleLogWorkStartTime" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenSimpleLogWorkEndTime" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenSimpleLogWorkQuantity" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenSimpleLogWorkArea" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenSimpleLogWorkNotes" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenSimpleLogWorkExceptions" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenSimpleLogWorkCompletedExceptions" runat="server" Value="&nbsp;" />
    <asp:HiddenField ID="hiddenSimpleLogWorkExNotes" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenSimpleLogWorkCompletedNotes" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <!-- Cancel SS NOTES -->
    <asp:HiddenField ID="hiddenCancelSS_SsId" runat="server" Value="&nbsp;" OnValueChanged="CancelSS_ValueChanged" />
    <asp:HiddenField ID="hiddenCancelSS_RescheduleDate" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenCancelSS_Notes" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Tail Service Schedule Browser"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div id="headerDiv" class="div_header">Search / Browse Tail Service Schedules</div>
            <div style="position: relative; margin: 4px; width: 99%;">
                <div style="position: relative; float: left; width: 70%">
                    <fieldset>
                        <legend>Search Criteria</legend>
                        <table width="100%">
                            <tr>
                                <td style="width: 30%">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 50%">
                                                <div class="lblInfo">
                                                    From Date<br />
                                                    <telerik:RadDatePicker ID="dtFrom" runat="server" Font-Names="Arial" MinDate="1/1/0001 12:00:00 AM"
                                                        Width="98%">
                                                        <ClientEvents OnDateSelected="ValidateDateRange" />
                                                    </telerik:RadDatePicker>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="stdValidator"
                                                        ErrorMessage="<br />Required" ControlToValidate="dtFrom"
                                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                            </td>
                                            <td style="width: 50%">
                                                <div class="lblInfo">
                                                    To Date (opt)<br />
                                                    <telerik:RadDatePicker ID="dtTo" runat="server" Font-Names="Arial" MinDate="1/1/0001 12:00:00 AM"
                                                        Width="98%">
                                                        <ClientEvents OnDateSelected="ValidateDateRange" />
                                                    </telerik:RadDatePicker>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 15%">
                                    <%--<atg:CustomerSelector ID="lstCustomerSelector" runat="server" VerticalLabelMode="true"
                                        AutoPostBack="true" OnOnSelectedIndexChanged="lstCustomerSelector_SelectedIndexChanged"
                                        CausesValidation="false" />--%>
                                </td>
                                <%--<td rowspan="3" style="width: 35%">
                                    <!-- Equipment Types -->
                                    <div>
                                        <asp:Label ID="lblHeader1" runat="server" SkinID="InfoLabel" Text="Aircraft Types"></asp:Label>
                                        <asp:Panel ID="pnlScroll1" runat="server" ScrollBars="Vertical" Width="100%" Height="101px">
                                            <div style="background-color: Navy; margin: 1px 0px 0px 0px; border: 1px solid Navy">
                                                <telerik:RadListBox ID="lstCustEqTypes" runat="server" DataKeyField="Equipmenttypeid"
                                                    DataValueField="Equipmenttypeid" DataTextField="Description" CheckBoxes="true"
                                                    SelectionMode="Multiple" Width="98%">
                                                </telerik:RadListBox>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </td>--%>
                                <td rowspan="3" style="width: 35%">
                                    <!-- Services -->
                                    <div>
                                        <asp:Label ID="lblHeader2" runat="server" SkinID="InfoLabel" Text="Services"></asp:Label>
                                        <asp:Panel ID="pnlScroll2" runat="server" BorderWidth="0" ScrollBars="Auto" Width="100%" Height="101px">
                                            <telerik:RadListBox ID="lstCustServices" runat="server" DataKeyField="Serviceid"
                                                DataValueField="Serviceid" DataTextField="Description" CheckBoxes="true" SelectionMode="Multiple"
                                                BorderWidth="0" BorderColor="Transparent" Width="98%">
                                            </telerik:RadListBox>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%">
                                    <div class="lblInfo">
                                        Tail Number (required)</div>
                                    <asp:TextBox ID="txtTailNumber" runat="server" Width="100px" AutoCompleteType="None" onkeypress="return clickButton(event)"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqTailNumber" runat="server" CssClass="stdValidator"
                                        ErrorMessage="<br />Required" ControlToValidate="txtTailNumber"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 15%">
                                    <asp:CheckBox ID="chkFuture" runat="server" Font-Size="10px" Text="Future Items?"
                                        Checked="false" Visible="true" />
                                    <%--<br />
                            <asp:Label ID="lblFutureWarning" runat="server" SkinID="SubscriptLabel" Text="(considerably slower)"></asp:Label>--%>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" style="width: 40%">
                                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_OnClick" Text="Search"/>
                                    &nbsp;&nbsp;<asp:Button ID="btnExportToExcel" runat="server" OnClick="btnExportToExcel_OnClick"
                                        Text="Export to Excel" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="position: relative; float: left; width: 30%">
                    <fieldset>
                        <legend>Legend</legend>
                        <div>
                            <label class="legendVSCompleted">
                                COMPLETED ITEMS /
                            </label>
                            <label class="legendVSCancelled">
                                CANCELLED ITEMS</label>
                            <br />
                            <label class="legendVSCompletedEx">
                                COMPLETED ITEMS W/OPEN EXCEPTION</label>
                            <br />
                            <label class="legendVSScheduled">
                                SCHEDULED ITEMS</label>
                            <br />
                            <label class="legendVSScheduled2">
                                NON-VERIFIED SCHEDULED ITEMS</label>
                            <br />
                            <label class="legendVSNonScheduled">
                                NON-SCHEDULED ITEMS</label>
                            <br />
                            <label class="legendVSFuture">
                                NON-SCHEDULED, FUTURE ITEMS</label>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="div_container" style="height: 500px;">
            <telerik:RadGrid ID="RadGrid1" runat="server" HorizontalAlign="NotSet" AutoGenerateColumns="false"
                OnDataBinding="RadGrid1_DataBinding" Width="100%" OnItemDataBound="RadGrid1_ItemDataBound">
                <MasterTableView Name="MainView" runat="server" Width="100%" AutoGenerateColumns="false" RowIndicatorColumn-Display="false"
                    GridLines="Both" CellPadding="0" CellSpacing="0" RowIndicatorColumn-Visible="false"
                    ShowHeadersWhenNoRecords="true" DataKeyNames="Servicescheduleid" TableLayout="Fixed"
                    Font-Names="Arial" Font-Size="10px" HorizontalAlign="Left" AllowPaging="false"
                    CommandItemDisplay="None" CommandItemSettings-ShowAddNewRecordButton="false" CommandItemSettings-ShowRefreshButton="false"
                    EditMode="InPlace" AllowSorting="true" AllowFilteringByColumn="true">
                    <Columns>
                        <telerik:GridTemplateColumn UniqueName="EditCommandColumn" AllowFiltering="false">
                            <ItemTemplate>
                                <div style="width: 20px">
                                    <asp:ImageButton ID="btnEditRed" runat="server" SkinID="Edit" OnClick="btnEdit_OnClick" />
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div style="width: 60px">
                                    <asp:ImageButton ID="btnSaveRed" runat="server" SkinID="Save" OnClick="btnSave_OnClick" />
                                    <asp:ImageButton ID="btnCancelRed" runat="server" SkinID="Cancel" OnClick="btnCancel_OnClick" />
                                </div>
                            </EditItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn UniqueName="SimpleLogColumn" AllowFiltering="false">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <a href="#" onclick='<%# "openWin1(" + Eval("Servicescheduleid").ToString() + "," + Eval("Serviceid").ToString() + "); return false;" %>'>Log</a>
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle Width="30px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn UniqueName="CancelColumn" AllowFiltering="false">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <a href="#" onclick='<%# "openWin4(" + Eval("Servicescheduleid").ToString() + "); return false;" %>'>Cancel </a>
                            </ItemTemplate>
                            <HeaderStyle Width="45px" />
                            <ItemStyle Width="45px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="EX1Column">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <a href="#" onclick='<%# "openWin2(" + Eval("Servicescheduleid").ToString() + "," + (Convert.ToBoolean(Eval("Hasactiveexception")) == true ? "1" : "0") + "); return false;" %>'>
                                    <asp:Image ID="imgEx1" runat="server" SkinID="Exception" />
                                </a>
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle Width="30px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="Hadexception" AllowFiltering="false" UniqueName="EX2Column">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <div id="divEx2" runat="server" visible='<%# GetEX2Visible() %>'>
                                    <a href="#" onclick='<%# "openWin3(" + Eval("Servicescheduleid").ToString() + "," + (Convert.ToBoolean(Eval("Hadexception")) == true ? "1" : "0") + "); return false;" %>'>
                                        <asp:Image ID="imgEx2" runat="server" SkinID="Exception" />
                                    </a>
                                </div>
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle Width="30px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Servicescheduleid" UniqueName="ServicescheduleidColumn"
                            HeaderText="Id" ReadOnly="false" Visible="false" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Equipmentid" UniqueName="EquipmentidColumn" HeaderText="EqId"
                            ReadOnly="false" Visible="false" AllowSorting="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Serviceid" UniqueName="ServicesidColumn" HeaderText="Service Id"
                            ReadOnly="false" Visible="false" AllowFiltering="false" AllowSorting="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Tail" DataField="Tailnumber" UniqueName="TailnumberColumn"
                            ReadOnly="true" AllowSorting="true" AllowFiltering="false">
                            <HeaderStyle Width="40px" HorizontalAlign="Center" />
                            <ItemStyle Width="40px" HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Equipmenttypedescr" UniqueName="EquipmentdescrColumn"
                            HeaderText="Type" ReadOnly="true" AllowSorting="true" AllowFiltering="false">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Servicedescr" UniqueName="ServicedescrColumn1"
                            HeaderText="Service Description" ReadOnly="true" AllowSorting="true" AllowFiltering="false"
                            Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="Servicedescr" UniqueName="ServicedescrColumn2"
                            HeaderText="Service Description" ReadOnly="true" SortExpression="Servicedescr"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                            FilterControlWidth="98%">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <atg:ServiceExDescr ID="ctrlServiceExDescr" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle Width="250px" HorizontalAlign="Left" />
                            <ItemStyle Width="250px" HorizontalAlign="Left" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Area" UniqueName="AreaColumn"
                            HeaderText="Div area" SortExpression="Area" AllowFiltering="false" ShowFilterIcon="false">
                            <%--<EditItemTemplate>
                                    <telerik:RadComboBox ID="cmbArea1" runat="server" DataSourceID="DBLinqDataSource2"
                                        AppendDataBoundItems="true" Width="150px" DataTextField="Description"
                                        DataValueField="Description" MarkFirstMatch="true">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="reqArea1" runat="server" Enabled="false" CssClass="stdValidator"
                                        ErrorMessage="*" ControlToValidate="cmbArea1" Display="Dynamic"></asp:RequiredFieldValidator>
                                </EditItemTemplate>--%>
                            <HeaderStyle Width="80px" HorizontalAlign="Left" />
                            <ItemStyle Width="80px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Location" UniqueName="LocationColumn" HeaderText="Location"
                            ReadOnly="true" SortExpression="Location" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                            AutoPostBackOnFilter="true">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Interval" UniqueName="IntervalColumn" HeaderText="Interval"
                            ReadOnly="true" AllowFiltering="true" SortExpression="Interval" CurrentFilterFunction="Contains"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" FilterControlWidth="98%">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn DataField="Servicedate" UniqueName="ServicedateColumn"
                            DataFormatString="{0:d}" HeaderText="Service Date" ReadOnly="false" MinDate="1/1/0001 12:00:00 AM"
                            ColumnEditorID="ServicedateColumnEditor" AllowSorting="true" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" FilterControlWidth="98%">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn DataField="Servicetime" UniqueName="ServicetimeColumn"
                            ColumnEditorID="ServicetimeColumnEditor" DataFormatString="{0:t}" PickerType="TimePicker"
                            HeaderText="Service Time" ReadOnly="false" MinDate="1/1/0001 12:00:00 AM" AllowFiltering="false"
                            AllowSorting="false">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn DataField="Completeddate" UniqueName="CompleteddateColumn"
                            DataFormatString="{0:d}" HeaderText="Compl Date" ReadOnly="true" MinDate="1/1/0001 12:00:00 AM"
                            ColumnEditorID="CompleteddateColumnEditor" AllowSorting="true" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" FilterControlWidth="98%">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn DataField="Comments" UniqueName="CommentsColumn" HeaderText="Notes"
                            ReadOnly="false" MaxLength="255" AllowFiltering="false" AllowSorting="false">
                            <HeaderStyle Width="200px" HorizontalAlign="Left" />
                            <ItemStyle Width="200px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Status" UniqueName="StatusColumn" ReadOnly="true"
                            Visible="false" AllowFiltering="false">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn DataField="Approvaldate" UniqueName="ApprovaldateColumn" AllowFiltering="false"
                            DataFormatString="{0:d}" HeaderText="Verify Date" ReadOnly="true" MinDate="1/1/0001 12:00:00 AM" AllowSorting="true">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn DataField="Approvaluserid" HeaderText="Verify User" ReadOnly="true"
                            AllowFiltering="false" UniqueName="ApprovaluseridColumn">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn DataField="Upddt" UniqueName="UpddtColumn" AllowFiltering="false"
                            DataFormatString="{0:d}" HeaderText="Upd Date" ReadOnly="true" MinDate="1/1/0001 12:00:00 AM" AllowSorting="true">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn DataField="Upduserid" HeaderText="Upd User" ReadOnly="true"
                            AllowFiltering="false" UniqueName="UpduseridColumn">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="OnHold" UniqueName="OnHoldColumn" ReadOnly="true"
                            Visible="true" AllowFiltering="false">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn UniqueName="DeleteCommandColumn" AllowFiltering="false">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <div style="width: 20px">
                                    <asp:ImageButton ID="btnDelete" runat="server" SkinID="Delete" OnClientClick="radconfirm('Delete Schedule?', event, 200, 140, null,'Delete?'); return false;" OnClick="btnDelete_OnClick" />
                                    <%--<ajaxToolkit:ConfirmButtonExtender ID="confirmDelete" runat="server" TargetControlID="btnDelete"
                                            ConfirmText="Delete Schedule?">
                                        </ajaxToolkit:ConfirmButtonExtender>--%>
                                </div>
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle Width="40px" />
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <NoRecordsTemplate>
                        <span class="lblInfo">No matching Aircraft. Change your Search Criteria, or Filters, and try again.</span>
                    </NoRecordsTemplate>
                    <%--<CommandItemSettings ShowExportToCsvButton="false" ShowExportToExcelButton="true"
                            ShowExportToPdfButton="false" />--%>
                </MasterTableView>
                <ExportSettings FileName="Schedule" OpenInNewWindow="true" ExportOnlyData="true" IgnorePaging="true"
                    HideStructureColumns="true">
                    <Excel Format="Html" />
                </ExportSettings>
                <ClientSettings>
                    <ClientEvents OnRowDblClick="RowDblClick" />
                    <Selecting AllowRowSelect="true" />
                    <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                </ClientSettings>
            </telerik:RadGrid>
            <telerik:GridDateTimeColumnEditor ID="ServicedateColumnEditor" runat="server">
                <TextBoxStyle Font-Names="Arial" Font-Size="10px" Width="98%" />
            </telerik:GridDateTimeColumnEditor>
            <telerik:GridDateTimeColumnEditor ID="ServicetimeColumnEditor" runat="server">
                <TextBoxStyle Font-Names="Arial" Font-Size="10px" Width="98%" />
            </telerik:GridDateTimeColumnEditor>
            <telerik:GridDateTimeColumnEditor ID="CompleteddateColumnEditor" runat="server">
                <TextBoxStyle Font-Names="Arial" Font-Size="10px" Width="98%" />
            </telerik:GridDateTimeColumnEditor>
        </div>
    </div>
</asp:Content>
