﻿using System;
using System.Data;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

namespace Job
{
    public partial class Job_TimeSheetResearch : System.Web.UI.Page, IAuditTrailPage
    {
        public string GetAuditPageName() {
            return AuditUtils.PAGE_TIMESHEETS;
        }
        protected void Page_PreInit(object sender, EventArgs e) {
            Page.Theme = Profile.SiteTheme;
        }
        protected void Page_Load(object sender, EventArgs e) {
            // This must be set here every time the page refreshes.
            cmbDE_WorkCardNumber.OnTextChanged += cmbDE_WorkCardnumber_OnTextChanged;
            if (!Page.IsPostBack) {
                // Initialize.
                Session.Add("TIMESHEET_RESEARCH_GRID_SELECTED_INDEX", "");
                // UI Setup.
                lblDE_WorkCardHourlyRate0.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                lblDE_WCBudgetHours0.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                lblDE_WCBudgetAmount0.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                txtDE_WCHourlyRate.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                txtDE_WCBudgetHours.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                txtDE_WCBudgetAmount.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                dtFrom.SelectedDate = DateTime.Now.Date;
                dtTo.SelectedDate = DateTime.Now.Date;
                dtDE_Workeddate.SelectedDate = DateTime.Now.Date;
                dtDE_Starttime.SelectedDate = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + "08:00:00 AM");
                dtDE_Endtime.SelectedDate = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + "05:00:00 PM");
                txtDE_Hours.Text = "8.00";
                lstUpdUsers.DataBind();
                if (lstUpdUsers.FindItemIndexByValue(Profile.Userid) > 0) {
                    lstUpdUsers.SelectedValue = Profile.Userid;
                }

                // Load the companies.
                LoadCompanys();
                if (lstCompanys.Items.Count > 0) {
                    LoadCustomers(lstCompanys.SelectedValue);

                    if (lstCustomers.Items.Count > 0) {
                        LoadJobCodes(lstCompanys.SelectedValue.ToString() + ":" + lstCustomers.SelectedValue.ToString());
                        LoadDE_JobCodes(lstCompanys.SelectedValue.ToString() + ":" + lstCustomers.SelectedValue.ToString());
                    }
                }
            } else {
                // Reset previously selected values.
                //if (Session["TIMESHEET_COMPANY"] != null) {
                //    lstCompanys.SelectedValue = Session["TIMESHEET_COMPANY"].ToString();
                //}
                //if (Session["TIMESHEET_CUSTOMER"] != null) {
                //    lstCustomers.SelectedValue = Session["TIMESHEET_CUSTOMER"].ToString();
                //}
                //if (Session["TIMESHEET_JOBCODE"] != null) {
                //    lstJobCodes.SelectedValue = Session["TIMESHEET_JOBCODE"].ToString();
                //}
         
                // Set the search params.
                SetSearchParams();
            }
        }
        protected void SetSearchParams() {
            // Build the Where statement.
            string sWhere = "";
            LinqDataSource0.Where = "";
            LinqDataSource0.WhereParameters.Clear();

            if (dtFrom.SelectedDate != null) {
                sWhere = "((Workeddate >= Convert.ToDateTime(@dtFrom)) and (Workeddate <= Convert.ToDateTime(@dtTo)))";
                LinqDataSource0.WhereParameters.Add("dtFrom", dtFrom.SelectedDate.Value.ToShortDateString());
                LinqDataSource0.WhereParameters.Add("dtTo", dtTo.SelectedDate.Value.ToShortDateString());
            }

            if (!string.IsNullOrEmpty(lstCompanys.SelectedValue)) {
                sWhere = sWhere + " and (Companydivisionid == Convert.ToInt32(@Companydivisionid))";
                LinqDataSource0.WhereParameters.Add("Companydivisionid", lstCompanys.SelectedValue.ToString());
            }

            if (!string.IsNullOrEmpty(lstCustomers.SelectedValue)) {
                sWhere = sWhere + " and (Customerid == Convert.ToInt32(@Customerid))";
                LinqDataSource0.WhereParameters.Add("Customerid", lstCustomers.SelectedValue.ToString());
            }

            if (!string.IsNullOrEmpty(lstEmployees.SelectedValue)) {
                sWhere = sWhere + " and (Userid == @Userid)";
                LinqDataSource0.WhereParameters.Add("Userid", lstEmployees.SelectedValue.ToString());
            }

            if (!string.IsNullOrEmpty(txtWorkCardNumber.Text)) {
                sWhere = sWhere + " and (Workcardnumber == @Workcardnumber)";
                LinqDataSource0.WhereParameters.Add("Workcardnumber", txtWorkCardNumber.Text);
            }

            if (!string.IsNullOrEmpty(lstJobCodes.SelectedValue)) {
                sWhere = sWhere + " and (Jobcodeid == Convert.ToInt32(@Jobcodeid))";
                LinqDataSource0.WhereParameters.Add("Jobcodeid", lstJobCodes.SelectedValue);
            }

            if (lstUpdUsers.SelectedIndex > 0) {
                sWhere = sWhere + " and (UpdUserId == @UpdUserId)";
                string sUserId = lstUpdUsers.SelectedValue.ToString();
                string sUserName = MembershipHelper.GetUserById(sUserId).Username;
                LinqDataSource0.WhereParameters.Add("UpdUserId", sUserName);
            }
            // Set the Where.
            LinqDataSource0.Where = sWhere;

            // ORDER
            string sOrder = "";
            if (lstSort.SelectedValue.ToString() == "NONE") {
                sOrder = "";
            } else if (lstSort.SelectedValue.ToString() == "EMPNUM") {
                sOrder = "Extendeduser.Employeenumber";
            } else if (lstSort.SelectedValue.ToString() == "EMPNAME") {
                sOrder = "Extendeduser.Lastname";
            } else if (lstSort.SelectedValue.ToString() == "WORKEDDT") {
                sOrder = "Workeddate";
            } else if (lstSort.SelectedValue.ToString() == "TAILNUM") {
                sOrder = "Equipment.Tailnumber";
            } else if (lstSort.SelectedValue.ToString() == "JOBCODE") {
                sOrder = "Jobcode.Code";
            } else if (lstSort.SelectedValue.ToString() == "WCNUM") {
                sOrder = "Workcardnumber";
            } else if (lstSort.SelectedValue.ToString() == "UPDUSERID") {
                sOrder = "UpdUserId";
            }
            // Ascending or Descending
            string sOrderDir = "";
            if (btnDesc.Checked == true) {
                sOrderDir = " DESC";
            } else if (btnAsc.Checked == true) {
                sOrderDir = " ASC";
            }
            // Apply the sort.
            if (sOrder != "") {
                LinqDataSource0.OrderBy = sOrder + sOrderDir;
            }

        }
        protected void Sort_CheckChanged(object sender, EventArgs e) {
            // Just fire search again.
            btnSearch_OnClick(sender, e);
        }
        protected void lstSortSelectedIndexChanged(object sender, EventArgs e) {
            // Just fire search again.
            btnSearch_OnClick(sender, e);
        }
        protected void btnSearch_OnClick(object sender, EventArgs e) {
            lblAdded.Text = "0";
            RadGrid1.DataSourceID = "LinqDataSource0";
            SetSearchParams();
            RadGrid1.DataBind();
            lblCount.Text = "&nbsp;Found:&nbsp;" + RadGrid1.Items.Count.ToString() + "&nbsp;";
        }
        protected void LoadCompanys() {
            DataSet data = ATGDB.Companydivision.GetCompanyCustomerUserJoinsDS(Profile.Userid, true);
            lstCompanys.DataSource = data;
            lstCompanys.DataBind();
            if (lstCompanys.Items.Count > 0) {
                if (lstCompanys.Items.FindItemByValue(Profile.Companydivisionid) != null) {
                    lstCompanys.SelectedValue = Profile.Companydivisionid;
                }
                //LoadCustomers(Profile.Companydivisionid);
            }
        }
        protected void LoadCustomers(string companyDivisionId) {
            DataSet data = ATGDB.Companydivision.GetCompanyCustomerUserJoinsDS(Profile.Userid, false);
            DataView view = data.Tables[0].AsDataView();
            view.RowFilter = "Companydivisionid = " + companyDivisionId;
            lstCustomers.Items.Clear();
            lstCustomers.DataSource = view;
            lstCustomers.DataBind();

            // Insert empty row here.
            RadComboBoxItem item = new RadComboBoxItem("- None -", "");
            lstJobCodes.Items.Insert(0, item);
            
            // Default index.
            if (lstCustomers.Items.Count > 0) {
                lstCustomers.SelectedValue = ""; // forces refresh;

                if (lstCustomers.Items.FindItemByValue(Profile.Customerid) != null) {
                    lstCustomers.SelectedValue = Profile.Customerid;
                }

                if (lstCustomers.SelectedValue != "") {
                    Session.Add("TIMESHEET_CUSTOMER", lstCustomers.SelectedValue);
                    LoadJobCodes(lstCompanys.SelectedValue + ":" + lstCustomers.SelectedValue);
                    LoadDE_JobCodes(lstCompanys.SelectedValue + ":" + lstCustomers.SelectedValue);
                }
            }
        }
        protected void LoadJobCodes(string keys) {
            string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
            string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);
            lstCompanys.SelectedValue = companyDivisionId;
            lstCustomers.SelectedValue = customerId;
            if (customerId != "") {
                DataSet data = ATGDB.Jobcode.GetCompanyCustomerJobJoinsDS(companyDivisionId, customerId);
                lstJobCodes.DataSource = data;
                lstJobCodes.DataBind();
                RadComboBoxItem item = new RadComboBoxItem("- None -", "");
                lstJobCodes.Items.Insert(0, item);
                lblJobCodeName.Text = "";
            }
        }
        protected void LoadDE_JobCodes(string keys) {
            string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
            string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);
            lstCompanys.SelectedValue = companyDivisionId;
            lstCustomers.SelectedValue = customerId;
            if (customerId != "") {
                DataSet data = ATGDB.Jobcode.GetCompanyCustomerJobJoinsDS(companyDivisionId, customerId);
                lstDE_Jobcodes.Items.Clear();
                lstDE_Jobcodes.Items.Add(new RadComboBoxItem("- None -", ""));
                lstDE_Jobcodes.DataSource = data;
                lstDE_Jobcodes.DataBind();

                // Refresh UI states for WC and Tail
                if (lstDE_Jobcodes.Items.Count > 0) {
                    lstDE_Jobcodes.Items[0].Selected = true;
                    EnableDiableWCandTail(lstDE_Jobcodes.SelectedItem.Text);
                    ATGDB.Jobcode jc = ATGDB.Jobcode.GetJobcodeById(lstDE_Jobcodes.Items[0].Value);
                    if (jc != null) {
                        lblDE_JobcodeName.Text = jc.Description;
                    } else {
                        lblDE_JobcodeName.Text = "";
                    }
                } else {
                    lblDE_JobcodeName.Text = "";
                }
            }
            CheckWCRateAndDescr();
        }
        protected void lstCompanys_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
            //Session["TIMESHEET_COMPANY"] = lstCompanys.SelectedValue;
            LoadCustomers(lstCompanys.SelectedValue);
        }
        protected void lstCustomers_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
            //Session["TIMESHEET_CUSTOMER"] = lstCustomers.SelectedValue;
            LoadJobCodes(lstCompanys.SelectedValue + ":" + lstCustomers.SelectedValue);
            LoadDE_JobCodes(lstCompanys.SelectedValue + ":" + lstCustomers.SelectedValue);
        }
        protected void lstEmployees_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
            ATGDB.Extendeduser exu = ATGDB.Extendeduser.GetExtendedUserByEmpNum(e.Text);
            if (exu != null) {
                lblEmployeeName.Text = exu.Fullname;
            } else {
                lblEmployeeName.Text = "";
            }
        }
        protected void txtDE_Employees_OnTextChanged(object sender, EventArgs e) {
            string val = txtDE_Employees.Text.Trim();
            if (val != "") {
                ATGDB.Extendeduser exu = ATGDB.Extendeduser.GetExtendedUserByEmpNum(val);
                if (exu != null) {
                    lblDE_EmployeeName.Text = exu.Fullname;
                    reqDE_Employees.IsValid = true;
                } else {
                    lblDE_EmployeeName.Text = "";
                    reqDE_Employees.IsValid = false;
                    txtDE_Employees.Focus();
                }
                dtDE_Workeddate.Focus();
                //ajaxPanelDE.RaisePostBackEvent("");
            }
        }
        protected void lstJobCodes_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
            ATGDB.Jobcode jc = ATGDB.Jobcode.GetJobcodeById(e.Value);
            if (jc != null) {
                lblJobCodeName.Text = jc.Description;
            } else {
                lblJobCodeName.Text = "";
            }
        }
        protected void lstDE_Jobcodes_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
            //LoadDE_JobCodes(lstCompanys.SelectedValue + ":" + lstCustomers.SelectedValue);
            EnableDiableWCandTail(lstDE_Jobcodes.Text);
            ATGDB.Jobcode jc = ATGDB.Jobcode.GetJobcodeById(e.Value);
            if (jc != null) {
                lblDE_JobcodeName.Text = jc.Description;
            } else {
                lblDE_JobcodeName.Text = "";
            }
            txtDE_TailNumber.Text = "";
            lblDE_TailDescr.Text = "";
            dtDE_Starttime.Focus();
        }
        //protected void lstCustomers_ItemsRequested(object o, RadComboBoxItemsRequestedEventArgs e) {
        //    LoadCustomers(e.Text);
        //}
        //protected void lstJobcodes_ItemsRequested(object o, RadComboBoxItemsRequestedEventArgs e) {
        //    LoadJobCodes(e.Text);
        //}
        protected void lstDE_Jobcodes_ItemsRequested(object o, RadComboBoxItemsRequestedEventArgs e) {
            LoadDE_JobCodes(e.Text);
        }
        protected void reqDE_TailNumberValidate(object sender, ServerValidateEventArgs e) {
            ATGDB.Equipment eq = null;
            if (txtDE_TailNumber.Text != "") {
                eq = ATGDB.Equipment.GetEquipmentByTailNumber(int.Parse(lstCustomers.SelectedValue.Trim()), txtDE_TailNumber.Text.Trim());

                if (eq != null) {
                    lblDE_TailDescr.Text = eq.Description;
                } else {
                    lblDE_TailDescr.Text = "";
                }
            } else {
                lblDE_TailDescr.Text = "";
            }
            e.IsValid = eq != null;
        }
        protected void reqGridTailNumberValidate(object sender, ServerValidateEventArgs e) {
            RadComboBox lstGridCustomers = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lstGridCustomers");
            RadTextBox txtGridTailNumber = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridTailNumber");
            Label lblGridTailDescr = (Label)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lblGridTailDescr");
            ATGDB.Equipment eq = null;
            if (txtGridTailNumber.Text != "") {
                eq = ATGDB.Equipment.GetEquipmentByTailNumber(int.Parse(lstGridCustomers.SelectedValue), txtGridTailNumber.Text);
                if (eq != null) {
                    lblGridTailDescr.Text = eq.Description;
                } else {
                    lblGridTailDescr.Text = "";
                }
            } else {
                lblGridTailDescr.Text = "";
            }
            e.IsValid = eq != null;
        }
        protected void chkShrinkDE_CheckChanged(object sender, EventArgs e) {
            if (chkShrinkDE.Checked == true) {
                chkShrinkDE.Text = "Uncheck to Hide";
                divDE.Visible = true;
                LoadJobCodes(lstCompanys.SelectedValue.ToString() + ":" + lstCustomers.SelectedValue.ToString());
                LoadDE_JobCodes(lstCompanys.SelectedValue.ToString() + ":" + lstCustomers.SelectedValue.ToString());
            } else {
                divDE.Visible = false;
                LoadJobCodes(lstCompanys.SelectedValue.ToString() + ":" + lstCustomers.SelectedValue.ToString());
                chkShrinkDE.Text = "Check to Show";
                divDE.Visible = false;
            }
        }
        protected void txtDE_TailNumber_OnTextChanged(object sender, EventArgs e) {
            // Fire custom validator.
            reqDE_TailNumber.Validate();
            if (reqDE_TailNumber.IsValid == false) {
                txtDE_TailNumber.Focus();
            } else {
                if ((cmbDE_WorkCardNumber.Enabled == true) &&
                    (cmbDE_WorkCardNumber.Visible == true)) {
                    cmbDE_WorkCardNumber.Focus();
                } else {
                    txtDE_Notes.Focus();
                }
            }
        }
        protected void cmbDE_WorkCardnumber_OnTextChanged(object o, EventArgs e) {
            CheckWCRateAndDescr();
            // May need to focus if changing from label back to text box.
            // For instance, changing from used WC back to unused WC.
            if (txtDE_WorkCardDescr.Visible == true) {
                txtDE_WorkCardDescr.Text = "";
                txtDE_WCHourlyRate.Value = 0.00;
                chkDE_WorkCardCompleted.Checked = false;
                txtDE_WorkCardDescr.Focus();
            }
        }
        protected void txtGrid_TailNumber_OnTextChanged(object sender, EventArgs e) {
            // Fire custom validator.
            CustomValidator reqGridTailNumber = (CustomValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridTailNumber");
            RadTextBox txtGridTailNumber = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridTailNumber");
            //RadTextBox txtGridWorkCardNumber = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridWorkCardNumber");
            UserControls_WorkCardSelectorControl cmbGridWorkCardNumber = (UserControls_WorkCardSelectorControl)PageUtils.FindControlRecursive(RadGrid1, "cmbGridWorkCardNumber");
            RadTextBox txtGridNotes = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridNotes");
            reqGridTailNumber.Validate();
            if (reqGridTailNumber.IsValid == false) {
                txtGridTailNumber.Focus();
            } else {
                if ((cmbGridWorkCardNumber.Enabled == true) &&
                   (cmbGridWorkCardNumber.Visible == true)) {
                    cmbGridWorkCardNumber.Focus();
                } else {
                    txtGridNotes.Focus();
                }
            }
        }
        protected void cmbGridWorkCardnumber_OnTextChanged(object sender, EventArgs e) {
            CheckGridWCRateAndDescr();
            // May need to focus if changing from label back to text box.
            // For instance, changing from used WC back to unused WC.
            RadTextBox txtGridWorkCardDescr = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridWorkCardDescr");
            if (txtGridWorkCardDescr.Visible == true) {
                CheckBox chkGridWorkCardCompleted = (CheckBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "chkGridWorkCardCompleted");
                RadNumericTextBox txtGridWCHourlyRate = (RadNumericTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridWCHourlyRate");
                txtGridWorkCardDescr.Text = "";
                chkGridWorkCardCompleted.Checked = false;
                txtGridWCHourlyRate.Value = 0.00;
                txtGridWorkCardDescr.Focus();
            }
        }
        protected void cmbGridWorkCardNumber_OnInit(object sender, EventArgs e) {
            // This must be set here every time the grid goes into edit state.
            UserControls_WorkCardSelectorControl cmb = (UserControls_WorkCardSelectorControl)PageUtils.FindControlRecursive(sender as UserControls_WorkCardSelectorControl, "cmbGridWorkCardNumber");
            if (cmb != null) {
                cmb.OnTextChanged += cmbGridWorkCardnumber_OnTextChanged;
            }
        }
        protected void CheckWCRateAndDescr() {
            ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
            ATGDB.Workcard wc = dc.Workcards.SingleOrDefault(x => x.Cardnumber.Trim().ToUpper().Equals(cmbDE_WorkCardNumber.Text.Trim().ToUpper()));
            if (wc != null) {
                if (wc.Completed == false) {
                    lblDE_WorkCardDescr.Text = wc.Description;
                    lblDE_WorkCardDescr.ForeColor = System.Drawing.Color.Black;
                    lblDE_WorkCardDescr.Visible = true;
                    txtDE_WorkCardDescr.Text = wc.Description;
                    txtDE_WorkCardDescr.Visible = false;

                    lblDE_WorkCardHourlyRate.Text = wc.Hourlyrate.ToString("$#,##0.00");
                    lblDE_WorkCardHourlyRate.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                    txtDE_WCHourlyRate.Text = wc.Hourlyrate.ToString();
                    txtDE_WCHourlyRate.Visible = false;

                    lblDE_WCBudgetHours.Text = wc.Budgethours.ToString("#,##0.00");
                    lblDE_WCBudgetHours.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                    txtDE_WCBudgetHours.Text = wc.Budgethours.ToString();
                    txtDE_WCBudgetHours.Visible = false;

                    lblDE_WCBudgetAmount.Text = wc.Budgetamount.ToString("#,##0.00");
                    lblDE_WCBudgetAmount.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                    txtDE_WCBudgetAmount.Text = wc.Budgetamount.ToString();
                    txtDE_WCBudgetAmount.Visible = false;

                    chkDE_WorkCardCompleted.Checked = wc.Completed;

                    // It is valid.
                    //reqDE_WorkCardNumber.IsValid = true;
                    cmbDE_WorkCardNumber.IsValid = true;
                    reqDE_WorkCardDescr.IsValid = true;
                    reqDE_WCHourlyRate.IsValid = true;
                    reqDE_WCBudgetHours.IsValid = true;
                    reqDE_WCBudgetAmount.IsValid = true;
                    txtDE_Notes.Focus();
                } else {
                    // The work card is closed.
                    lblDE_WorkCardDescr.Text = "CLOSED:" + wc.Description;// "*** THIS WORK CARD IS CLOSED ***";
                    lblDE_WorkCardDescr.ForeColor = System.Drawing.Color.Red;
                    lblDE_WorkCardDescr.Visible = true;
                    txtDE_WorkCardDescr.Text = "CLOSED:" + wc.Description;// "*** THIS WORK CARD IS CLOSED ***";
                    txtDE_WorkCardDescr.Visible = false;

                    lblDE_WorkCardHourlyRate.Text = wc.Hourlyrate.ToString("$#,##0.00");
                    lblDE_WorkCardHourlyRate.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                    txtDE_WCHourlyRate.Text = wc.Hourlyrate.ToString();
                    txtDE_WCHourlyRate.Visible = false;

                    lblDE_WCBudgetHours.Text = wc.Budgethours.ToString("#,##0.00");
                    lblDE_WCBudgetHours.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                    txtDE_WCBudgetHours.Text = wc.Budgethours.ToString();
                    txtDE_WCBudgetHours.Visible = false;

                    lblDE_WCBudgetAmount.Text = wc.Budgetamount.ToString("#,##0.00");
                    lblDE_WCBudgetAmount.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                    txtDE_WCBudgetAmount.Text = wc.Budgetamount.ToString();
                    txtDE_WCBudgetAmount.Visible = false;

                    chkDE_WorkCardCompleted.Checked = wc.Completed;
                    
                    // Not valid.
                    //reqDE_WorkCardNumber.IsValid = false;
                    cmbDE_WorkCardNumber.IsValid = false;
                    cmbDE_WorkCardNumber.Focus();
                    reqDE_WorkCardDescr.IsValid = true;
                    reqDE_WCHourlyRate.IsValid = true;
                    reqDE_WCBudgetHours.IsValid = true;
                    reqDE_WCBudgetAmount.IsValid = true;
                }
            } else {
                // It is valid.
                //reqDE_WorkCardNumber.IsValid = true;
                reqDE_WorkCardDescr.IsValid = true;
                reqDE_WCHourlyRate.IsValid = true;
                cmbDE_WorkCardNumber.IsValid = true;
                lblDE_WorkCardDescr.Visible = false;
                txtDE_WorkCardDescr.Visible = true;
                lblDE_WorkCardHourlyRate.Visible = false;
                lblDE_WCBudgetHours.Visible = false;
                lblDE_WCBudgetAmount.Visible = false;
                txtDE_WCHourlyRate.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                lblDE_WCBudgetHours.Visible = false;
                txtDE_WCBudgetHours.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                lblDE_WCBudgetAmount.Visible = false;
                txtDE_WCBudgetAmount.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
            }
        }
        protected void CheckGridWCRateAndDescr() {
            Label lblGridWorkCardDescr = (Label)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lblGridWorkCardDescr");
            //RadTextBox txtGridWorkCardNumber = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridWorkCardNumber");
            UserControls_WorkCardSelectorControl cmbGridWorkCardNumber = (UserControls_WorkCardSelectorControl)PageUtils.FindControlRecursive(RadGrid1, "cmbGridWorkCardNumber");
            RadTextBox txtGridWorkCardDescr = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridWorkCardDescr");
            Label lblGridWorkCardHourlyRate = (Label)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lblGridWorkCardHourlyRate");
            RadNumericTextBox txtGridWCHourlyRate = (RadNumericTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridWCHourlyRate");
            RadTextBox txtGridNotes = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridNotes");
            CheckBox chkGridWorkCardCompleted = (CheckBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "chkGridWorkCardCompleted");
  
            ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
            ATGDB.Workcard wc = dc.Workcards.SingleOrDefault(x => x.Cardnumber.Trim().ToUpper().Equals(cmbGridWorkCardNumber.Text.Trim().ToUpper()));
            if (wc != null) {
                if (wc.Completed == false) {
                    lblGridWorkCardDescr.Text = wc.Description;
                    lblGridWorkCardDescr.ForeColor = System.Drawing.Color.Black;
                    lblGridWorkCardDescr.Visible = true;
                    txtGridWorkCardDescr.Text = wc.Description;
                    txtGridWorkCardDescr.Visible = false;

                    lblGridWorkCardHourlyRate.Text = wc.Hourlyrate.ToString("$#,##0.00");
                    lblGridWorkCardHourlyRate.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                    txtGridWCHourlyRate.Text = wc.Hourlyrate.ToString();
                    txtGridWCHourlyRate.Visible = false;

                    chkGridWorkCardCompleted.Checked = wc.Completed;
                    
                    // Its valid.
                    RequiredFieldValidator reqGridWorkCardDescr = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridWorkCardDescr");
                    reqGridWorkCardDescr.IsValid = true;
                    RequiredFieldValidator reqGridWorkCardHourlyRate = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridWorkCardHourlyRate");
                    reqGridWorkCardHourlyRate.IsValid = true;
                    cmbGridWorkCardNumber.IsValid = true;
                    txtGridNotes.Focus();
                } else {
                    // All are completed.
                    lblGridWorkCardDescr.Text = "CLOSED:" + wc.Description;// "*** THIS WORK CARD IS CLOSED ***";
                    lblGridWorkCardDescr.ForeColor = System.Drawing.Color.Red;
                    lblGridWorkCardDescr.Visible = true;
                    txtGridWorkCardDescr.Text = "CLOSED:" + wc.Description;// "*** THIS WORK CARD IS CLOSED ***";
                    txtGridWorkCardDescr.Visible = false;

                    lblGridWorkCardHourlyRate.Text = wc.Hourlyrate.ToString("$#,##0.00");
                    lblGridWorkCardHourlyRate.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                    txtGridWCHourlyRate.Text = wc.Hourlyrate.ToString();
                    txtGridWCHourlyRate.Visible = false;

                    chkGridWorkCardCompleted.Checked = wc.Completed;

                    // Its not valid.
                    //RequiredFieldValidator reqGridWorkCardNumber = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridWorkCardNumber");
                    cmbGridWorkCardNumber.IsValid = false;
                    cmbGridWorkCardNumber.Focus();
                }
            } else {
                lblGridWorkCardDescr.Visible = false;
                txtGridWorkCardDescr.Visible = true;
                lblGridWorkCardHourlyRate.Visible = false;
                txtGridWCHourlyRate.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
            }
        }
        protected void EnableDiableWCandTail(string code) {
            if (code != "") {
                lstDE_Jobcodes.FindItemByText(code).Selected = true;
                ATGDB.Jobcode jc = ATGDB.Jobcode.GetJobcodeById(lstDE_Jobcodes.SelectedValue);
                bool requiresWC = jc.Requiresworkcard;// Convert.ToBoolean(((RadComboBox)sender).SelectedItem.Attributes["Requiresworkcard"]);
                bool requiresTN = jc.Requirestailnumber;// Convert.ToBoolean(((RadComboBox)sender).SelectedItem.Attributes["Requirestailnumber"]);

                // Clear it out.
                cmbDE_WorkCardNumber.Text = "";
                txtDE_WorkCardDescr.Text = "";
                txtDE_WCHourlyRate.Text = "";
                txtDE_WCBudgetAmount.Text = "";
                txtDE_WCBudgetHours.Text = "";
                chkDE_WorkCardCompleted.Checked = false;
                txtDE_TailNumber.Text = "";

                // Reset UI
                lblDE_WorkCardDescr.Visible = false;
                lblDE_WorkCardHourlyRate.Visible = false;
                cmbDE_WorkCardNumber.Visible = true;
                txtDE_WorkCardDescr.Visible = true;
                txtDE_WCHourlyRate.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                txtDE_WCBudgetHours.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                txtDE_WCBudgetAmount.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                cmbDE_WorkCardNumber.Enabled = requiresWC;
                txtDE_WorkCardDescr.Enabled = requiresWC;
                txtDE_WCHourlyRate.Enabled = requiresWC;
                txtDE_WCBudgetHours.Enabled = requiresWC;
                txtDE_WCBudgetAmount.Enabled = requiresWC;
                chkDE_WorkCardCompleted.Enabled = requiresWC;
                //reqDE_WorkCardNumber.Enabled = requiresWC;
                cmbDE_WorkCardNumber.ReqEnabled = requiresWC;
                reqDE_WorkCardDescr.Enabled = requiresWC;
                reqDE_WCHourlyRate.Enabled = requiresWC;
                reqDE_WCHourlyRate.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE); 
                if (requiresWC == true) {
                    //reqDE_WorkCardNumber.IsValid = !requiresWC;
                    cmbDE_WorkCardNumber.IsValid = !requiresWC;
                    reqDE_WorkCardDescr.IsValid = !requiresWC;
                    reqDE_WCHourlyRate.IsValid = !requiresWC;
                    txtDE_WCHourlyRate.Text = "0.00";
                    reqDE_WCBudgetAmount.IsValid = !requiresWC;
                    txtDE_WCBudgetAmount.Text = "0.00";
                    reqDE_WCBudgetHours.IsValid = !requiresWC;
                    txtDE_WCBudgetHours.Text = "0.00";
                }

                txtDE_TailNumber.Enabled = requiresTN;
                reqDE_TailNumber.Enabled = requiresTN;
                if (requiresTN == true) {
                    reqDE_TailNumber.IsValid = !requiresTN;
                }
                //ajaxPanelDE.RaisePostBackEvent("");
            }
        }
        protected void EnableDiableGridWCandTail(string code) {
            if (code != "") {
                Label lblGridWorkCardDescr = (Label)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lblGridWorkCardDescr");
                //RadTextBox txtGridWorkCardNumber = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridWorkCardNumber");
                UserControls_WorkCardSelectorControl cmbGridWorkCardNumber = (UserControls_WorkCardSelectorControl)PageUtils.FindControlRecursive(RadGrid1, "cmbGridWorkCardNumber");
                RadTextBox txtGridWorkCardDescr = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridWorkCardDescr");
                Label lblGridWorkCardHourlyRate = (Label)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lblGridWorkCardHourlyRate");
                RadNumericTextBox txtGridWCHourlyRate = (RadNumericTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridWCHourlyRate");
                RadTextBox txtGridTailNumber = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridTailNumber");
                CheckBox chkGridWorkCardCompleted = (CheckBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "chkGridWorkCardCompleted");
                RadComboBox lstGridJobCodes = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lstGridJobCodes");
                CustomValidator reqGridTailNumber = (CustomValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridTailNumber");
                //RequiredFieldValidator reqGridWorkCardNumber = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridWorkCardNumber");
                RequiredFieldValidator reqGridWorkCardDescr = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridWorkCardDescr");
                RequiredFieldValidator reqGridWorkCardHourlyRate = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridWorkCardHourlyRate");

                lstGridJobCodes.FindItemByText(code).Selected = true;
                ATGDB.Jobcode jc = ATGDB.Jobcode.GetJobcodeById(lstGridJobCodes.SelectedValue);
                bool requiresWC = jc.Requiresworkcard;
                bool requiresTN = jc.Requirestailnumber;

                // Don't do this - editing, inserting would be empty anyway.
                // Clear it out.
                //cmbGridWorkCardNumber.Text = "";
                //txtGridWorkCardDescr.Text = "";
                //txtGridWCHourlyRate.Text = "";
                //chkGridWorkCardCompleted.Checked = false;
                //txtGridTailNumber.Text = "";

                // Reset UI
                lblGridWorkCardDescr.Visible = false;
                lblGridWorkCardHourlyRate.Visible = false;
                cmbGridWorkCardNumber.Visible = true;
                txtGridWorkCardDescr.Visible = true;
                txtGridWCHourlyRate.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
                cmbGridWorkCardNumber.Enabled = requiresWC;
                txtGridWorkCardDescr.Enabled = requiresWC;
                txtGridWCHourlyRate.Enabled = requiresWC;
                chkGridWorkCardCompleted.Enabled = requiresWC;
                //reqGridWorkCardNumber.Enabled = requiresWC;
                cmbGridWorkCardNumber.ReqEnabled = requiresWC;
                reqGridWorkCardDescr.Enabled = requiresWC;
                reqGridWorkCardHourlyRate.Enabled = requiresWC;
                reqGridWorkCardHourlyRate.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE); 
                if (requiresWC == true) {
                    //reqGridWorkCardNumber.IsValid = !requiresWC;
                    cmbGridWorkCardNumber.IsValid = !requiresWC;
                    reqGridWorkCardDescr.IsValid = !requiresWC;
                    reqGridWorkCardHourlyRate.IsValid = !requiresWC;
                    txtGridWCHourlyRate.Text = "0.00";
                }

                txtGridTailNumber.Enabled = requiresTN;
                reqGridTailNumber.Enabled = requiresTN;
                if (requiresTN == true) {
                    reqGridTailNumber.IsValid = !requiresTN;
                }
                //ajaxPanelDE.RaisePostBackEvent("");
            }
        }
        protected void RadGrid1_SelectedIndexChanged(object sender, EventArgs e) {
            if (RadGrid1.SelectedValue != null) {
                Session.Add("TIMESHEET_RESEARCH_GRID_SELECTED_INDEX", RadGrid1.SelectedValue);
            }
        }
        protected void RadGrid1_DataBound(object sender, EventArgs e) {
            // Smarter record selecting. Or perhaps dumber. Why doesn't the grid do this??
            if (RadGrid1.Items.Count > 0) {
                string lastSelectedValue = "";
                if (Session["TIMESHEET_RESEARCH_GRID_SELECTED_INDEX"] != null) {
                    lastSelectedValue = Session["TIMESHEET_RESEARCH_GRID_SELECTED_INDEX"].ToString();
                }
                if (lastSelectedValue == "") {
                    RadGrid1.Items[0].Selected = true;
                } else {
                    bool found = false;
                    foreach (GridDataItem item in RadGrid1.Items) {
                        if (((ATGDB.Timesheet)item.DataItem).Timesheetid.ToString() == lastSelectedValue) {
                            item.Selected = true;
                            found = true;
                            break;
                        }
                    }
                    if (found == false) {
                        RadGrid1.Items[0].Selected = true;
                    }
                }
            }
            
            // Enable/Disable buttons.
//            btnReconcileSelected.Enabled = RadGrid1.Items.Count > 0;
//            btnReconcileAll.Enabled = RadGrid1.Items.Count > 0;

            // If editing, load relational combos...
            if ((RadGrid1.EditItems.Count > 0) || (RadGrid1.MasterTableView.IsItemInserted)) {
                LoadGridCompanys();

                // Initial states of Tail and Work Card fields.
                RadComboBox lstGridJobCodes = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lstGridJobCodes");
                if (lstGridJobCodes.SelectedItem != null) {
                    EnableDiableGridWCandTail(lstGridJobCodes.SelectedItem.Text);
                    CheckGridWCRateAndDescr();
                }
            }
        }
        protected void LoadGridCompanys() {
            RadComboBox lstGridCompanys = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lstGridCompanys");
            if (lstGridCompanys != null) {
                DataSet data = ATGDB.Companydivision.GetCompanyCustomerUserJoinsDS(Profile.Userid, true);
                lstGridCompanys.DataSource = data;
                lstGridCompanys.DataBind();

                RadComboBoxItem item = new RadComboBoxItem("", "");
                lstGridCompanys.Items.Insert(0, item);

                // Default index.
                GridDataItem dataitem = (GridDataItem)lstGridCompanys.NamingContainer;
                if (lstGridCompanys.Items.Count > 0) {
                    string compId = ((TextBox)(dataitem["CompanydivisionidColumn"].Controls[0])).Text;
                    if (lstGridCompanys.Items.FindItemByValue(compId) != null) {
                        lstGridCompanys.FindItemByValue(compId).Selected = true;

                        // Now load the Customers.
                        LoadGridCustomers(compId);
                    }
                }
            }
        }
        protected void LoadGridCustomers(string companyDivisionId) {
            RadComboBox lstGridCustomers = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lstGridCustomers");
            if (lstGridCustomers != null) {
                if (companyDivisionId != "") {
                    DataSet data = ATGDB.Companydivision.GetCompanyCustomerUserJoinsDS(Profile.Userid, false);
                    DataView view = data.Tables[0].AsDataView();
                    view.RowFilter = "Companydivisionid = " + companyDivisionId;
                    lstGridCustomers.DataSource = view;
                    lstGridCustomers.DataBind();

                    RadComboBoxItem item = new RadComboBoxItem("", "");
                    lstGridCustomers.Items.Insert(0, item);

                    // Default index.
                    GridDataItem dataitem = (GridDataItem)lstGridCustomers.NamingContainer;
                    if (lstGridCustomers.Items.Count > 0) {
                        string custId = ((TextBox)(dataitem["CustomeridColumn"].Controls[0])).Text;
                        if (lstGridCustomers.Items.FindItemByValue(custId) != null) {
                            if (lstGridCustomers.FindItemByValue(custId) != null) {
                                lstGridCustomers.FindItemByValue(custId).Selected = true;

                                // Now load the job codes.
                                LoadGridJobCodes(companyDivisionId + ":" + custId);
                            }
                        }
                    }
                }
            }
        }
        protected void LoadGridJobCodes(string keys) {
            RadComboBox lstGridJobCodes = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lstGridJobCodes");
            if (lstGridJobCodes != null) {
                string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
                string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);
                if (customerId != "") {
                    DataSet data = ATGDB.Jobcode.GetCompanyCustomerJobJoinsDS(companyDivisionId, customerId);
                    lstGridJobCodes.DataSource = data;
                    lstGridJobCodes.DataBind();

                    RadComboBoxItem item = new RadComboBoxItem("", "");
                    lstGridJobCodes.Items.Insert(0, item);

                    // Default index.
                    GridDataItem dataitem = (GridDataItem)lstGridJobCodes.NamingContainer;
                    if (lstGridJobCodes.Items.Count > 0) {
                        string jobId = ((TextBox)(dataitem["JobcodeidColumn"].Controls[0])).Text;
                        if (lstGridJobCodes.FindItemByValue(jobId) != null) {
                            lstGridJobCodes.FindItemByValue(jobId).Selected = true;
                        }
                    }
                }
            }
        }
        protected void lstGridJobcodes_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
            RadComboBox lstGridCustomers = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lstGridCustomers");
            RadComboBox lstGridCompanys = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lstGridCompanys");
            Label lblGridJobcodeName = (Label)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lblGridJobcodeName");
            RadTextBox txtGridTailNumber = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridTailNumber");
            Label lblGridTailDescr = (Label)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lblGridTailDescr");
            RadDatePicker dtGridWorkeddate = (RadDatePicker)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "dtGridWorkeddate");
            string custId = lstGridCustomers.SelectedValue;
            LoadGridCustomers(lstGridCompanys.SelectedValue);
            lstGridCustomers.SelectedValue = custId;
            LoadGridJobCodes(lstGridCompanys.SelectedValue + ":" + lstGridCustomers.SelectedValue);
            EnableDiableGridWCandTail(e.Text);
            ATGDB.Jobcode jc = ATGDB.Jobcode.GetJobcodeById(e.Value);
            if (jc != null) {
                lblGridJobcodeName.Text = jc.Description;
            } else {
                lblGridJobcodeName.Text = "- Pick -";
            }
            txtGridTailNumber.Text = "";
            lblGridTailDescr.Text = "";
            dtGridWorkeddate.Focus();
        }
        protected void txtGridEmployees_OnTextChanged(object sender, EventArgs e) {
            RadTextBox txtGridEmployeeNum = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridEmployees");
            RequiredFieldValidator reqGridEmployee = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridEmployee");
            string val = txtGridEmployeeNum.Text.Trim();
            if (val != "") {
                ATGDB.Extendeduser exu = ATGDB.Extendeduser.GetExtendedUserByEmpNum(val);
                Label lblGridEmployeeName = (Label)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lblGridEmployeeName");
                if (exu != null) {
                    RadComboBox lstGridCompanys = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lstGridCompanys");
                    lblGridEmployeeName.Text = exu.Fullname;
                    reqGridEmployee.IsValid = true;
                    lstGridCompanys.Focus();
                } else {
                    lblGridEmployeeName.Text = "";
                    txtGridEmployeeNum.Focus();
                    reqGridEmployee.IsValid = false;
                }
            } else {
                reqGridEmployee.IsValid = false;
                txtGridEmployeeNum.Focus();
            }
        }
        protected void lstGridCompanys_ItemsRequested(object o, RadComboBoxItemsRequestedEventArgs e) {
            LoadGridCompanys();
        }
        protected void lstGridCustomers_ItemsRequested(object o, RadComboBoxItemsRequestedEventArgs e) {
            LoadGridCustomers(e.Text);
        }
        protected void lstGridJobCodes_ItemsRequested(object o, RadComboBoxItemsRequestedEventArgs e) {
            LoadGridJobCodes(e.Text);
        }
        protected void btnEdit_OnClick(object sender, EventArgs e) {
            GridDataItem item = (GridDataItem)((ImageButton)sender).NamingContainer;
            item.Edit = true;
            RadGrid1.DataBind();
        }
        protected void btnSave_OnClick(object sender, EventArgs e) {
            RequiredFieldValidator reqGridEmployee = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridEmployee");
            RequiredFieldValidator reqGridCompany = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridCompany");
            RequiredFieldValidator reqGridCustomer = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridCustomer");
            RequiredFieldValidator reqGridJob = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridJob");
            RequiredFieldValidator reqGridWorkeddate = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridWorkeddate");
            RequiredFieldValidator reqGridStarttime = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridStarttime");
            RequiredFieldValidator reqGridEndtime = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridEndtime");
            RequiredFieldValidator reqGridHours = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridHours");
            RequiredFieldValidator reqGridRate = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridRate");
            CustomValidator reqGridTailNumber = (CustomValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridTailNumber");
            //RequiredFieldValidator reqGridWorkCardNumber = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridWorkCardNumber");
            UserControls_WorkCardSelectorControl cmbGridWorkCardNumber = (UserControls_WorkCardSelectorControl)PageUtils.FindControlRecursive(RadGrid1, "cmbGridWorkCardNumber");
            RequiredFieldValidator reqGridWorkCardDescr = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridWorkCardDescr");
            RequiredFieldValidator reqGridWorkCardHourlyRate = (RequiredFieldValidator)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "reqGridWorkCardHourlyRate");
            if (reqGridEmployee.IsValid &&
              reqGridCompany.IsValid &&
              reqGridCustomer.IsValid &&
              reqGridJob.IsValid &&
              reqGridWorkeddate.IsValid &&
              reqGridStarttime.IsValid &&
              reqGridEndtime.IsValid &&
              reqGridHours.IsValid &&
              reqGridRate.IsValid &&
              reqGridTailNumber.IsValid &&
              //reqGridWorkCardNumber.IsValid &&
              cmbGridWorkCardNumber.IsValid &&
              reqGridWorkCardDescr.IsValid &&
              reqGridWorkCardHourlyRate.IsValid) {
                // Get the item.
                GridEditableItem item = (GridEditableItem)((ImageButton)sender).NamingContainer;
                // Back to Readonly Mode
                item.Edit = false;

                // Load up the other columns from the controls.
                // employee
                RadTextBox txtGridEmployees = (RadTextBox)PageUtils.FindControlRecursive(item, "txtGridEmployees");
                RadComboBox lstGridCompanys = (RadComboBox)PageUtils.FindControlRecursive(item, "lstGridCompanys");
                RadComboBox lstGridCustomers = (RadComboBox)PageUtils.FindControlRecursive(item, "lstGridCustomers");
                RadComboBox lstGridJobCodes = (RadComboBox)PageUtils.FindControlRecursive(item, "lstGridJobCodes");
                RadDatePicker dtGridWorkeddate = (RadDatePicker)PageUtils.FindControlRecursive(item, "dtGridWorkeddate");
                RadTimePicker dtGridStarttime = (RadTimePicker)PageUtils.FindControlRecursive(item, "dtGridStarttime");
                RadTimePicker dtGridEndtime = (RadTimePicker)PageUtils.FindControlRecursive(item, "dtGridEndtime");
                RadNumericTextBox txtGridHours = (RadNumericTextBox)PageUtils.FindControlRecursive(item, "txtGridHours");
                RadNumericTextBox txtGridRate = (RadNumericTextBox)PageUtils.FindControlRecursive(item, "txtGridRate");
                //RadTextBox txtGridWorkCardNumber = (RadTextBox)PageUtils.FindControlRecursive(item, "txtGridWorkCardNumber");
                RadTextBox txtGridWorkCardDescr = (RadTextBox)PageUtils.FindControlRecursive(item, "txtGridWorkCardDescr");
                RadNumericTextBox txtGridWCHourlyRate = (RadNumericTextBox)PageUtils.FindControlRecursive(item, "txtGridWCHourlyRate");
                CheckBox chkGridWorkCardCompleted = (CheckBox)PageUtils.FindControlRecursive(item, "chkGridWorkCardCompleted");
                RadTextBox txtGridNotes = (RadTextBox)PageUtils.FindControlRecursive(item, "txtGridNotes");
                RadTextBox txtGridTailNumber = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridTailNumber");
                string eqId = ATGDB.Equipment.GetEquipmentIdByTailNumber(int.Parse(lstGridCustomers.SelectedValue), txtGridTailNumber.Text).ToString();
                ATGDB.Jobcode jc = ATGDB.Jobcode.GetJobcodeById(lstGridJobCodes.SelectedValue);
                bool requiresWC = jc.Requiresworkcard;
                bool requiresTN = jc.Requirestailnumber;

                int tsId = -1;
                if (item.SavedOldValues.Count > 0) {
                    tsId = int.Parse(item.SavedOldValues["Timesheetid"].ToString());
                }
                // Get a TimeSheet - existing or new
                ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
                ATGDB.Timesheet ts = null;
                if (tsId > 0) {
                    ts = dc.Timesheets.SingleOrDefault(x => x.Timesheetid == tsId);
                    if (ts == null) {
                        ts = new ATGDB.Timesheet();
                        dc.Timesheets.InsertOnSubmit(ts);
                    }
                } else {
                    ts = new ATGDB.Timesheet();
                    dc.Timesheets.InsertOnSubmit(ts);
                }
                // Fill it up.
                ATGDB.Extendeduser exu = MembershipHelper.GetExtendedUserByEmpNum(txtGridEmployees.Text);
                if (exu != null) {
                    ts.Userid = exu.Userid;
                }
                ts.Companydivisionid = int.Parse(lstGridCompanys.SelectedValue);
                ts.Customerid = int.Parse(lstGridCustomers.SelectedValue);
                ts.Jobcodeid = int.Parse(lstGridJobCodes.SelectedValue);
                ts.Workeddate = dtGridWorkeddate.SelectedDate.Value;
                ts.Starttime = dtGridStarttime.SelectedDate.Value;
                ts.Endtime = dtGridEndtime.SelectedDate.Value;
                ts.Hours = (float)txtGridHours.Value;
                if (txtGridRate.Value > 0.00) {
                    // Use user input.
                    ts.Rate = (float)txtGridRate.Value;
                } else {
                    // Default to Employee rate.
                    if (exu != null) {
                        ts.Hourlyrate = exu.Hourlyrate;
                    } else {
                        ts.Hourlyrate = 0.00f;
                    }
                    
                    // Get the rest of the rate data and denormalize it.
                    ATGDB.Ratedata r = ATGDB.Ratedata.GetRatedataByVals(ts.Jobcodeid, ts.Companydivisionid, ts.Customerid);
                    ts.Rateid = r.Rateid;
                    ts.Flatfeeorhourly = r.Flatfeeorhourly;
                    ts.Cost = r.Cost;
                    ts.Rate = r.Rate;
                    ts.Stdhours = r.Stdhours;
                    ts.Stdquantity = r.Stdquantity;
                }

                // WORK CARD
                if (requiresWC == true) {
                    ATGDB.Workcard wc = dc.Workcards.SingleOrDefault(x => x.Cardnumber.Trim().ToUpper().Equals(cmbGridWorkCardNumber.Text.Trim().ToUpper()));
                    if (wc != null) {
                        // Assign work card.
                        ts.Workcardid = wc.Workcardid;
                        ts.Workcard = wc;
                        // Update completed status.
                        wc.Completed = chkGridWorkCardCompleted.Checked;
                    } else {
                        // Create a new work card.
                        wc = new ATGDB.Workcard();
                        wc.Cardnumber = cmbGridWorkCardNumber.Text;
                        wc.Description = txtGridWorkCardDescr.Text;
                        if (txtGridWCHourlyRate.Value > 0.00) {
                            // Use user input.
                            wc.Hourlyrate = txtGridWCHourlyRate.Value == null ? 0.00f : (float)txtGridWCHourlyRate.Value;
                        } else {
                            // Default to customer.
                            wc.Hourlyrate = ATGDB.Customer.GetCustomerById(ts.Customerid.ToString()).Nonroutinehourlyrate;
                        }
                        // Update completed status.
                        wc.Completed = chkGridWorkCardCompleted.Checked;
                        // Insert.
                        dc.Workcards.InsertOnSubmit(wc);
                        dc.SubmitChanges();
                        // Assign.
                        ts.Workcardid = wc.Workcardid;
                        ts.Workcard = wc;
                    }
                }
                
                // TAIL NUMBER
                if (requiresTN == true) {
                    if (eqId != "-1") {
                        ts.Equipmentid = int.Parse(eqId);
                    }
                }
                ts.Notes = txtGridNotes.Text;

                dc.SubmitChanges();
                RadGrid1.DataBind();
            } else {
                // Focus the appropriate control.
                GridEditableItem item = (GridEditableItem)((ImageButton)sender).NamingContainer;
                if (!reqGridEmployee.IsValid) {
                    RadTextBox txtGridEmployees = (RadTextBox)PageUtils.FindControlRecursive(item, "txtGridEmployees");
                    txtGridEmployees.Focus();
                } else if (!reqGridCompany.IsValid) {
                    RadComboBox lstGridCompanys = (RadComboBox)PageUtils.FindControlRecursive(item, "lstGridCompanys");
                    lstGridCompanys.Focus();
                } else if (!reqGridCustomer.IsValid) {
                    RadComboBox lstGridCustomers = (RadComboBox)PageUtils.FindControlRecursive(item, "lstGridCustomers");
                    lstGridCustomers.Focus();
                } else if (!reqGridJob.IsValid) {
                    RadComboBox lstGridJobCodes = (RadComboBox)PageUtils.FindControlRecursive(item, "lstGridJobCodes");
                    lstGridJobCodes.Focus();
                } else if (!reqGridWorkeddate.IsValid) {
                    RadDatePicker dtGridWorkeddate = (RadDatePicker)PageUtils.FindControlRecursive(item, "dtGridWorkeddate");
                    dtGridWorkeddate.Focus();
                } else if (!reqGridStarttime.IsValid) {
                    RadTimePicker dtGridStarttime = (RadTimePicker)PageUtils.FindControlRecursive(item, "dtGridStarttime");
                    dtGridStarttime.Focus();
                } else if (!reqGridEndtime.IsValid) {
                    RadTimePicker dtGridEndtime = (RadTimePicker)PageUtils.FindControlRecursive(item, "dtGridEndtime");
                    dtGridEndtime.Focus();
                } else if (!reqGridHours.IsValid) {
                    RadNumericTextBox txtGridHours = (RadNumericTextBox)PageUtils.FindControlRecursive(item, "txtGridHours");
                    txtGridHours.Focus();
                } else if (!reqGridRate.IsValid) {
                    RadNumericTextBox txtGridRate = (RadNumericTextBox)PageUtils.FindControlRecursive(item, "txtGridRate");
                    txtGridRate.Focus();
                } else if (!reqGridTailNumber.IsValid) {
                    RadTextBox txtGridTailNumber = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridTailNumber");
                    txtGridTailNumber.Focus();
                } else if (!cmbGridWorkCardNumber.IsValid) {
                    //RadTextBox cmbGridWorkCardNumber = (RadTextBox)PageUtils.FindControlRecursive(item, "cmbGridWorkCardNumber");
                    cmbGridWorkCardNumber.Focus();
                } else if (!reqGridWorkCardDescr.IsValid) {
                    RadTextBox txtGridWorkCardDescr = (RadTextBox)PageUtils.FindControlRecursive(item, "txtGridWorkCardDescr");
                    txtGridWorkCardDescr.Focus();
                } else if (!reqGridWorkCardHourlyRate.IsValid) {
                    RadNumericTextBox txtGridWCHourlyRate = (RadNumericTextBox)PageUtils.FindControlRecursive(item, "txtGridWCHourlyRate");
                    txtGridWCHourlyRate.Focus();
                } else {
                    // Default.
                    RadTextBox txtGridEmployees = (RadTextBox)PageUtils.FindControlRecursive(item, "txtGridEmployees");
                    txtGridEmployees.Focus();
                }
            }
        }
        protected void btnCancel_OnClick(object sender, EventArgs e) {
            GridDataItem item = (GridDataItem)((ImageButton)sender).NamingContainer;
            item.Edit = false;
            RadGrid1.DataBind();
        }
        protected void btnDelete_OnClick(object sender, EventArgs e) {
            GridEditableItem item = (GridEditableItem)((ImageButton)sender).NamingContainer;
            ATGDB.Timesheet ts = new ATGDB.Timesheet();
            item.UpdateValues(ts);

            // Delete
            if (ts.Timesheetid > 0) {
                // Delete the TimeSheet.
                ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
                ATGDB.Timesheet ts2 = dc.Timesheets.SingleOrDefault(x => x.Timesheetid == ts.Timesheetid);
                dc.Timesheets.DeleteOnSubmit(ts2);
                dc.SubmitChanges();
                RadGrid1.DataBind();
            }
        }
        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e) {
            if ((e.Item is GridDataItem) && !(e.Item is GridDataInsertItem)) {
                ATGDB.Timesheet ts = (ATGDB.Timesheet)e.Item.DataItem;
                // Line the employee dropdown back up if in edit mode.
                if (e.Item.Edit) {
                    RadTextBox txtGridEmployees = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridEmployees");
                    if (txtGridEmployees != null) {
                        txtGridEmployees.Focus();
                    }
                    RadTextBox txtGridTailNumber = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridTailNumber");
                    if (ts.Equipmentid != null) {
                        txtGridTailNumber.Text = ts.Equipment.Tailnumber;
                    } else {
                        txtGridTailNumber.Text = "";
                    }
                    Label lblGridTailDescr = (Label)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lblGridTailDescr");
                    if (ts.Equipmentid != null) {
                        lblGridTailDescr.Text = ts.Equipment.Description;
                    } else {
                        lblGridTailDescr.Text = "";
                    }
                }
            } else if (e.Item is GridDataInsertItem) {
                RadTextBox txtGridEmployees = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "txtGridEmployees");
                if (txtGridEmployees != null) {
                    txtGridEmployees.Focus();
                }
            }
        }
        protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e) {
            if (e.CommandName == "InitInsert") {
                // Cancel the default operation 
                e.Canceled = true;
                // Gather the values.
                System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();
                newValues["Username"] = Profile.UserName;
                newValues["Extendeduser"] = MembershipHelper.GetExtendedUserById(Profile.Userid);
                newValues["Jobcode"] = new ATGDB.Jobcode();
                newValues["Workeddate"] = DateTime.Now.Date;
                newValues["Starttime"] = DateTime.Now.Date;
                newValues["Endtime"] = DateTime.Now.Date;
                newValues["Hours"] = 0.00;
                newValues["Rate"] = 0.00;
                newValues["Reconciledstatus"] = "U"; // Unreconciled.
                newValues["UpdUserId"] = Profile.UserName;
                newValues["Workcard"] = new ATGDB.Workcard();
                // Set the values.
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }
        protected void btnReconcileSelected_OnClick(object sender, EventArgs e) {
            if (RadGrid1.SelectedValue != null) {
                string[] ids = new string[1];
                ids[0] = RadGrid1.SelectedValue.ToString();
                ATGDB.Timesheet.Reconcile(ids);
            }
            btnSearch_OnClick(null, null);
        }
        protected void btnReconcileAll_OnClick(object sender, EventArgs e) {
            //string[] ids = new string[RadGrid1.Items.Count];
            //foreach (GridDataItem item in RadGrid1.Items) {
                
                //item.ExtractValues(
                //ids[item.RowIndex] = ((ATGDB.Timesheet)item.DataItem).Timesheetid.ToString();
            //}
            //ATGDB.Timesheet.Reconcile(ids);
            btnSearch_OnClick(null, null);
        }
        protected void btnDE_Add_OnClick(object sender, EventArgs e) {
            //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
            //m.IsActive = true;
            // If everything is valid, add it.
            if (reqDE_Employees.IsValid &&
                reqDE_Endtime.IsValid &&
                reqDE_Hours.IsValid &&
                reqDE_Jobcode.IsValid &&
                reqDE_Starttime.IsValid &&
                reqDE_TailNumber.IsValid &&
                reqDE_WCHourlyRate.IsValid &&
                reqDE_WCBudgetAmount.IsValid &&
                reqDE_WCBudgetHours.IsValid &&
                reqDE_WorkCardDescr.IsValid &&
                //reqDE_WorkCardNumber.IsValid &&
                cmbDE_WorkCardNumber.IsValid &&
                reqDE_Workeddate.IsValid) {

                // Gather data.
                string compId = lstCompanys.SelectedValue;
                string custId = lstCustomers.SelectedValue;
                string jobId = lstDE_Jobcodes.SelectedValue;
                string empNum = txtDE_Employees.Text;
                if (empNum != "") {
                    float fRate = MembershipHelper.GetExtendedUserByEmpNum(empNum).Hourlyrate;
                    DateTime dtWd = dtDE_Workeddate.SelectedDate.Value;
                    DateTime dtSt = dtDE_Starttime.SelectedDate.Value;
                    DateTime dtEt = dtDE_Endtime.SelectedDate.Value;
                    float fHours = txtDE_Hours.Value == null ? 0.00f : (float)txtDE_Hours.Value;
                    string sNotes = txtDE_Notes.Text;
                    float fWCRate = ATGDB.Customer.GetCustomerById(custId).Nonroutinehourlyrate;
                    string sWCNum = cmbDE_WorkCardNumber.Text;
                    string sWCDesc = txtDE_WorkCardDescr.Text;
                    bool bWCDone = chkDE_WorkCardCompleted.Checked;
                    string eqId = ATGDB.Equipment.GetEquipmentIdByTailNumber(int.Parse(custId), txtDE_TailNumber.Text).ToString();
                    ATGDB.Jobcode jc = ATGDB.Jobcode.GetJobcodeById(jobId);
                    bool requiresWC = jc.Requiresworkcard;
                    bool requiresTN = jc.Requirestailnumber;

                    ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
                    
                    // Check completed status first.
                    if (sWCNum != "") {
                        ATGDB.Workcard wc = dc.Workcards.SingleOrDefault(x => x.Cardnumber.Trim().ToUpper().Equals(sWCNum.Trim().ToUpper()));
                        if ((bWCDone == true) && (wc != null) && (wc.Completed == true)) {
                            // Marked as done, existing work card, which is completed.
                            // Do not allow.
                            cmbDE_WorkCardNumber.IsValid = requiresWC;
                            cmbDE_WorkCardNumber.Focus();
                            return;
                        } else if (wc != null) {
                            // Marked as UNDONE, existing work card, which is completed.
                            // User is trying to reopen. Update work card.
                            wc.Completed = bWCDone;
                            // *submitted below.
                        }
                    }

                    // Fill new time sheet.
                    ATGDB.Timesheet ts = new ATGDB.Timesheet();
                    ts.Companydivisionid = int.Parse(compId);
                    ts.Customerid = int.Parse(custId);
                    ts.Jobcodeid = int.Parse(jobId);
                    ts.Userid = MembershipHelper.GetExtendedUserByEmpNum(empNum).Userid;
                    ts.Workeddate = dtWd;
                    ts.Starttime = dtSt;
                    ts.Endtime = dtEt;
                    ts.Hours = fHours;
                    ts.Hourlyrate = fRate;
                    ts.Notes = sNotes;
                    
                    // Get the rest of the rate data and denormalize it.
                    ATGDB.Ratedata r = ATGDB.Ratedata.GetRatedataByVals(ts.Jobcodeid, ts.Companydivisionid, ts.Customerid);
                    ts.Rateid = r.Rateid;
                    ts.Flatfeeorhourly = r.Flatfeeorhourly;
                    ts.Cost = r.Cost;
                    ts.Rate = r.Rate;
                    ts.Stdhours = r.Stdhours;
                    ts.Stdquantity = r.Stdquantity;

                    // TAIL NUMBER
                    if (requiresTN == true) {
                        if (eqId != "-1") {
                            ts.Equipmentid = int.Parse(eqId);
                        }
                    }

                    // WORK CARD
                    if (requiresWC == true) {
                        // WORK CARD
                        if (requiresWC == true) {
                            ATGDB.Workcard wc = dc.Workcards.SingleOrDefault(x => x.Cardnumber.Trim().ToUpper().Equals(cmbDE_WorkCardNumber.Text.Trim().ToUpper()));
                            if (wc != null) {
                                // Assign work card.
                                ts.Workcardid = wc.Workcardid;
                                ts.Workcard = wc;
                            } else {
                                // Create a new work card.
                                wc = new ATGDB.Workcard();
                                wc.Cardnumber = cmbDE_WorkCardNumber.Text;
                                wc.Description = sWCDesc;
                                if (txtDE_WCHourlyRate.Value > 0.00) {
                                    // Use user input.
                                    wc.Hourlyrate = txtDE_WCHourlyRate.Value == null ? 0.00f : (float)txtDE_WCHourlyRate.Value;
                                } else {
                                    // Default to customer.
                                    wc.Hourlyrate = fWCRate;// ATGDB.Customer.GetCustomerById(ts.Customerid.ToString()).Nonroutinehourlyrate;
                                }
                                wc.Budgethours = txtDE_WCBudgetHours.Value ?? 0.00;
                                wc.Budgetamount = txtDE_WCBudgetAmount.Value ?? 0.00;
                                wc.Completed = chkDE_WorkCardCompleted.Checked;
                                // Insert.
                                dc.Workcards.InsertOnSubmit(wc);
                                dc.SubmitChanges();
                                // Assign.
                                ts.Workcardid = wc.Workcardid;
                                ts.Workcard = wc;
                            }
                        }
                    }

                    // Insert it.
                    dc.Timesheets.InsertOnSubmit(ts);
                    dc.SubmitChanges();

                    // Update counter.
                    lblAdded.Text = (int.Parse(lblAdded.Text) + 1).ToString();

                    // Start again.
                    chkDE_WorkCardCompleted.Checked = false;

                    cmbDE_WorkCardNumber.Text = "";

                    lblDE_WorkCardDescr.Text = "";
                    lblDE_WorkCardDescr.Visible = false;
                    txtDE_WorkCardDescr.Text = "";
                    txtDE_WorkCardDescr.Visible = true;
                    
                    lblDE_WorkCardHourlyRate.Text = "0.00";
                    lblDE_WorkCardHourlyRate.Visible = false;
                    txtDE_WCHourlyRate.Text = "0.00";
                    txtDE_WCHourlyRate.Visible = true;

                    lblDE_WCBudgetAmount.Text = "0.00";
                    lblDE_WCBudgetAmount.Visible = false;
                    txtDE_WCBudgetAmount.Text = "0.00";
                    txtDE_WCBudgetAmount.Visible = true;
                    
                    lblDE_WCBudgetHours.Text = "0.00";
                    lblDE_WCBudgetHours.Visible = false;
                    txtDE_WCBudgetHours.Text = "0.00";
                    txtDE_WCBudgetHours.Visible = true;

                    //lblDE_EmployeeName.Text = "";
                    txtDE_Employees.Focus();
                } else {
                    // Start again.
                    chkDE_WorkCardCompleted.Checked = false;
                    txtDE_Employees.Focus();
                }
            } else {
                // Focus the appropriate control.
                if (!reqDE_Employees.IsValid) {
                    txtDE_Employees.Focus();
                } else if (!reqDE_Workeddate.IsValid) {
                    dtDE_Workeddate.Focus();
                } else if (!reqDE_Jobcode.IsValid) {
                    lstDE_Jobcodes.Focus();
                } else if (!reqDE_Starttime.IsValid) {
                    dtDE_Starttime.Focus();
                } else if (!reqDE_Endtime.IsValid) {
                    dtDE_Endtime.Focus();
                } else if (!reqDE_Hours.IsValid) {
                    txtDE_Hours.Focus();
                } else if (!reqDE_TailNumber.IsValid) {
                    txtDE_TailNumber.Focus();
                } else if (!cmbDE_WorkCardNumber.IsValid/*reqDE_WorkCardNumber.IsValid*/) {
                    cmbDE_WorkCardNumber.Focus();
                } else if (!reqDE_WorkCardDescr.IsValid) {
                    txtDE_WorkCardDescr.Focus();
                } else if (!reqDE_WCHourlyRate.IsValid) {
                    txtDE_WCHourlyRate.Focus();
                } else if (!reqDE_WCBudgetHours.IsValid) {
                    txtDE_WCBudgetHours.Focus();
                } else if (!reqDE_WCBudgetAmount.IsValid) {
                    txtDE_WCBudgetAmount.Focus();
                } else {
                    txtDE_Employees.Focus(); // default.
                }
            }
        }
        protected void btnExportToExcel_OnClick(object sender, EventArgs e) {
            if (RadGrid1.MasterTableView.Items.Count > 0) {
                // Hide the main column first.
                RadGrid1.MasterTableView.Columns.FindByUniqueName("CustomTemplateColumn").Display = false;
                // Show the ones we want.
                RadGrid1.MasterTableView.Columns.FindByUniqueName("TimesheetidColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("EmployeeNumberColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("JobcodeCodeColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("WorkeddateColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("StarttimeColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("EndtimeColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("HoursColumn").Visible = true;
                if (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE)) {
                    RadGrid1.MasterTableView.Columns.FindByUniqueName("RateColumn").Visible = true;
                }
                RadGrid1.MasterTableView.Columns.FindByUniqueName("WorkCardNumberColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("WorkCardHourlyRateColumn").Visible = true;
                RadGrid1.Rebind();
                RadGrid1.MasterTableView.ExportToExcel();
            }
        }
        protected void btnExportToADP_OnClick(object sender, EventArgs e) {
            if (RadGrid1.MasterTableView.Items.Count > 0) {
                // Hide the main column first.
                RadGrid1.MasterTableView.Columns.FindByUniqueName("CustomTemplateColumn").Display = false;
                // Show the ones we want.
                RadGrid1.MasterTableView.Columns.FindByUniqueName("TimesheetidColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("EmployeeNumberColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("JobcodeCodeColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("WorkeddateColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("StarttimeColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("EndtimeColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("HoursColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("RateColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("WorkCardNumberColumn").Visible = true;
                RadGrid1.MasterTableView.Columns.FindByUniqueName("WorkCardHourlyRateColumn").Visible = true;
                RadGrid1.Rebind();
                RadGrid1.MasterTableView.ExportToCSV();
            }
        }


    }
}