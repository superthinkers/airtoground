﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="WorkCardManager.aspx.cs" Inherits="Job_WorkCardManager"
    Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <devart:DbLinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Workcard" OrderBy="Cardnumber"
        TableName="Workcards" AutoGenerateWhereClause="True"
        EnableDelete="true" EnableInsert="true" EnableUpdate="true" OnSelecting="LinqDataSource1_Selecting"> 
    </devart:DbLinqDataSource>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="false" runat="server" EnableShadow="true"
        Modal="true" VisibleTitlebar="false" AutoSize="true">
    </telerik:RadWindowManager>     
    <telerik:RadCodeBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            // This fixes problems when exporting.
            function onRequestStart(sender, args) {
                if ((args.get_eventTarget().indexOf("btnExportToExcel") >= 0) || 
                    (args.get_eventTarget().indexOf("xxx") >= 0)) {
                    // Disable ajax
                    args.set_enableAjax(false);
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="true" Skin="Office2007">
    </telerik:RadAjaxLoadingPanel>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Work Card Manager"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Work Card Manager</div>
            <!-- SEARCH -->
            <table style="width:100%">
                <tr>
                    <td style="width: 70%;">
                        <!-- SEARCH -->
                        <fieldset>
                            <legend>Search Criteria</legend>
                            <table style="width: 40%;">
                                <tr>
                                    <td style="text-align: right; width: 10%">
                                        Card Number:
                                    </td>
                                    <td style="text-align: left; width: 23%">
                                        <telerik:RadTextBox ID="txtCardNumber" runat="server" Width="98%" MaxLength="30">
                                        </telerik:RadTextBox>
                                    </td>
                                    <td style="text-align: right; width: 10%">Description:
                                    </td>
                                    <td style="text-align: left; width: 23%">
                                        <telerik:RadTextBox ID="txtDescription" runat="server" Width="98%" MaxLength="100">
                                        </telerik:RadTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; width: 33%" colspan="4">
                                        <br />
                                        <br />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_OnClick" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </div>
        <div class="div_container">
            <div class="lblInfoBold">Selected Work Cards:</div>
            <br />
            <!-- Work Card Results -->
            <telerik:RadGrid ID="RadGrid1" GridLines="None" Width="99%" Height="200" runat="server"
                AllowAutomaticDeletes="True" AllowAutomaticUpdates="True" AllowAutomaticInserts="true" 
                AutoGenerateColumns="False" CellSpacing="0">
                <MasterTableView Width="99%" CommandItemDisplay="Top" DataKeyNames="Workcardid"
                    AllowAutomaticUpdates="true" AllowAutomaticInserts="true" AllowAutomaticDeletes="true"
                    NoMasterRecordsText="No Work Cards Found" AllowSorting="true" AllowMultiColumnSorting="false"
                    CommandItemSettings-ShowRefreshButton="false" CommandItemSettings-ShowAddNewRecordButton="true"
                    TableLayout="Fixed" Font-Size="12px" HorizontalAlign="NotSet" RowIndicatorColumn-Display="false"
                    AutoGenerateColumns="false" EditMode="InPlace" AllowFilteringByColumn="false">
                    <PagerStyle AlwaysVisible="true" Mode="NumericPages" Position="Bottom" />
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn DataField="Workcardid" HeaderText="Id" AllowSorting="false"
                            UniqueName="WorkcardidColumn" ReadOnly="true" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="Cardnumber" HeaderText="Card Number"
                            AllowFiltering="true" SortExpression="Cardnumber" CurrentFilterFunction="Contains"
                            FilterControlWidth="98%" AutoPostBackOnFilter="true" ShowFilterIcon="false">
                            <InsertItemTemplate>
                                <telerik:RadTextBox ID="txtCardNumber" runat="server" MaxLength="30"
                                    Text='<%# Bind("Cardnumber") %>' Width="94%">
                                </telerik:RadTextBox>
                                <asp:CustomValidator ID="ReqCardNumber" runat="server" CssClass="stdValidator"
                                    ControlToValidate="txtCardNumber" ErrorMessage="<br />Must Be Unique"
                                    Display="Dynamic" OnServerValidate="ReqCardNumber_Validate">
                                </asp:CustomValidator>
                                <asp:RequiredFieldValidator ID="ReqCardNumber2" runat="server" CssClass="stdValidator"
                                    ControlToValidate="txtCardNumber" ErrorMessage="<br />Required" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </InsertItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblCardNumber" runat="server" SkinID="InfoLabel" Text='<%# Eval("Cardnumber") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCardNumber" runat="server" SkinID="InfoLabel" Text='<%# Eval("Cardnumber") %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Description" HeaderText="Description" AllowSorting="true"
                            UniqueName="DescriptionColumn" ReadOnly="false" Visible="true" AllowFiltering="true" SortExpression="Cardnumber" CurrentFilterFunction="Contains"
                            FilterControlWidth="98%" AutoPostBackOnFilter="true" ShowFilterIcon="false" ColumnEditorID="DescriptionEditor">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="Completed" HeaderText="Completed" AllowSorting="true"
                            UniqueName="CompletedColumn" ReadOnly="false" Visible="true">
                            <HeaderStyle Width="7%" HorizontalAlign="Center" />
                            <ItemStyle Width="7%" HorizontalAlign="Center" />
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridNumericColumn DataField="Hourlyrate" HeaderText="Hourly Rate" UniqueName="HourlyrateColumn"
                            ReadOnly="false" Visible="true" ColumnEditorID="HourlyrateEditor" DataFormatString="{0:c}">
                            <HeaderStyle Width="7%" HorizontalAlign="Center" />
                            <ItemStyle Width="7%" HorizontalAlign="Center" />
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Budgethours" HeaderText="Budget Hours" UniqueName="BudgethoursColumn"
                            ReadOnly="false" Visible="true" ColumnEditorID="BudgethoursEditor" DataFormatString="{0:f}">
                            <HeaderStyle Width="7%" HorizontalAlign="Center" />
                            <ItemStyle Width="7%" HorizontalAlign="Center" />
                        </telerik:GridNumericColumn>
                        <telerik:GridNumericColumn DataField="Budgetamount" HeaderText="Budget Amount" UniqueName="BudgetamountColumn"
                            ReadOnly="false" Visible="true" ColumnEditorID="BudgetamountEditor" DataFormatString="{0:c}">
                            <HeaderStyle Width="7%" HorizontalAlign="Center" />
                            <ItemStyle Width="7%" HorizontalAlign="Center" />
                        </telerik:GridNumericColumn>
                        <telerik:GridBoundColumn DataField="Changehistory" HeaderText="Change History"
                            AllowSorting="false" UniqueName="ChangehistoryColumn" ReadOnly="false" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Upddt" HeaderText="Upd Dt" AllowSorting="true"
                            UniqueName="UpddtColumn" ReadOnly="true" Visible="true" DataFormatString="{0:d}">
                            <HeaderStyle Width="7%" HorizontalAlign="Center" />
                            <ItemStyle Width="7%" HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Upduserid" HeaderText="Upd UserId" AllowSorting="true"
                            UniqueName="UpduseridColumn" ReadOnly="true" Visible="true">
                            <HeaderStyle Width="7%" />
                            <ItemStyle Width="7%" />
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings FileName="WorkCardManager" OpenInNewWindow="true" ExportOnlyData="true"
                    IgnorePaging="true" HideStructureColumns="true">
                </ExportSettings>
                <ClientSettings>
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
            </telerik:RadGrid>
            <telerik:GridTextBoxColumnEditor ID="DescriptionEditor" runat="server" TextBoxStyle-Width="98%">
            </telerik:GridTextBoxColumnEditor>
            <telerik:GridNumericColumnEditor ID="HourlyrateEditor" runat="server" NumericTextBox-Width="98%"
                NumericTextBox-MinValue="0" NumericTextBox-NumberFormat-DecimalDigits="2" NumericTextBox-SelectionOnFocus="SelectAll">
            </telerik:GridNumericColumnEditor>
            <telerik:GridNumericColumnEditor ID="BudgethoursEditor" runat="server" NumericTextBox-Width="98%"
                NumericTextBox-MinValue="0" NumericTextBox-NumberFormat-DecimalDigits="2" NumericTextBox-SelectionOnFocus="SelectAll">
            </telerik:GridNumericColumnEditor>
            <telerik:GridNumericColumnEditor ID="BudgetamountEditor" runat="server" NumericTextBox-Width="98%"
                NumericTextBox-MinValue="0" NumericTextBox-NumberFormat-DecimalDigits="2" NumericTextBox-SelectionOnFocus="SelectAll">
            </telerik:GridNumericColumnEditor>
            <br />
        </div>
    </div>
</asp:Content>

