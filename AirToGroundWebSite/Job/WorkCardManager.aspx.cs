﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Job_WorkCardManager : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_WORKCARD_MANAGER;
    }

    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
        //m.IsActive = true;

        if (!Page.IsPostBack) {
            RadGrid1.MasterTableView.Columns.FindByUniqueName("HourlyrateColumn").Display = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
            RadGrid1.MasterTableView.Columns.FindByUniqueName("BudgethoursColumn").Display = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);
            RadGrid1.MasterTableView.Columns.FindByUniqueName("BudgetamountColumn").Display = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE);

            txtCardNumber.Focus();
        }
    }
    protected void btnSearch_OnClick(object sender, EventArgs e) {
        LoadWorkCards();
    }
    protected void LoadWorkCards() {
        if ((txtCardNumber.Text != "") || (txtDescription.Text != "")) {
            RadGrid1.DataSourceID = "LinqDataSource1";
            RadGrid1.MasterTableView.DataSourceID = "LinqDataSource1";
            RadGrid1.DataBind();
            if (RadGrid1.Items.Count > 0) {
                RadGrid1.Items[0].Selected = true;
            }
        }
    }
    protected void ReqCardNumber_Validate(object sender, ServerValidateEventArgs e) {
        // Card Number is REQUIRED and must be UNIQUE
        RadTextBox txtCardNum = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1, "txtCardNumber");
        //CustomValidator reqCardNum = (CustomValidator)PageUtils.FindControlRecursive(RadGrid1, "ReqCardNumber");
        string sCardNum = txtCardNum.Text == null ? "" : txtCardNum.Text.Trim();
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        var data = from wc in dc.Workcards
                   where wc.Cardnumber == sCardNum.Trim()
                   select wc;
        e.IsValid = ((sCardNum != "") && (data.Count() == 0));
        //if (string.IsNullOrEmpty(sEmpNum)) {
        //    reqCardNum.ErrorMessage = "<br />Required";    
        //} else if (data.Count() > 0) {
        //    reqCardNum.ErrorMessage = "<br />Not Unique";
        //}
    }
    protected void btnExportToExcel_OnClick(object sender, EventArgs e) {
        if (RadGrid1.MasterTableView.Items.Count > 0) {
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ChangehistoryColumn").Display = true;
            RadGrid1.Rebind();
            RadGrid1.MasterTableView.ExportToExcel();
        }
    }
    protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e) {
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        if ((txtCardNumber.Text != "") || (txtDescription.Text != "")) {
            var data = (from wc in dc.Workcards
                        where txtCardNumber.Text == "" ? true == true : wc.Cardnumber.Contains(txtCardNumber.Text.Trim())
                        where txtDescription.Text == "" ? true == true : wc.Description.Contains(txtDescription.Text.Trim())
                        orderby wc.Cardnumber
                        select wc).ToList();
            e.Result = data;
        }
    }
}