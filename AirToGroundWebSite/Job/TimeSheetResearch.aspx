﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="TimeSheetResearch.aspx.cs" Inherits="Job.Job_TimeSheetResearch" Buffer="true"
    Strict="true" Explicit="true" %>

<%@ Register Src="~/UserControls/WorkCardSelectorControl.ascx" TagName="WorkCardSelectorControl" TagPrefix="atg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <devart:DbLinqDataSource ID="LinqDataSource0" runat="server" 
        EnableDelete="true" EnableInsert="true" EnableUpdate="true"
        ContextTypeName="ATGDB.ATGDataContext" EntityTypeName="ATGDB.Timesheet" 
        TableName="Timesheets" OrderBy="Workeddate">
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Extendeduser" TableName="Extendedusers" OrderBy="Employeenumber">
    </devart:DbLinqDataSource>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("btnExportToExcel") >= 0 ||
                        args.get_eventTarget().indexOf("btnExportToADP") >= 0) {
                    args.set_enableAjax(false);
                }
            }
            function ValidateDateRange(sender, e) {
                var dtFromDate = $find("<%= dtFrom.ClientID %>");
                var dtToDate = $find("<%= dtTo.ClientID %>");

                if ((dtFromDate.get_selectedDate() == null) && (dtToDate.get_selectedDate() != null)) {
                    alert("Please select the From/End Date");
                } else if ((dtFromDate.get_selectedDate() > dtToDate.get_selectedDate()) && (dtToDate.get_selectedDate() != null)) {
                    alert("The To/Start Date must be less than or equal to than the From/Begin Date");
                    dtFromDate.set_selectedDate(dtToDate.get_selectedDate());
                }
                return false;
            }

            // COMBO BOX STUFF
            var companysCombo;
            var customersCombo;
            var jobCodesCombo;
            var DE_jobCodesCombo;
            var chkShrinkDE;
            function pageLoad() {
                // initialize the global variables
                // in this event all client objects 
                // are already created and initialized 
                companysCombo = $find("<%= lstCompanys.ClientID %>");
                customersCombo = $find("<%= lstCustomers.ClientID %>");
                jobCodesCombo = $find("<%= lstJobCodes.ClientID %>");
                DE_jobCodesCombo = $find("<%= lstDE_Jobcodes.ClientID %>");
            }
            function DE_ShrinkDE_Load(button, eventArgs) {
                chkShrinkDE = button;
            }
            // SHARED FUNCTION
            function ItemsLoaded(combo, eventArqs) {
                if (combo.get_items().get_count() > 0) {
                    // pre-select the first item
                    combo.set_text(combo.get_items().getItem(0).get_text());
                    combo.set_value(combo.get_items().getItem(0).get_value());
                    combo.get_items().getItem(0).highlight();
                }
                //combo.showDropDown();
            }

            // ***********************
            // SEARCH LOAD METHODS
            // ***********************
            function CustomerItemsLoaded(combo, eventArqs) {
                if (combo.get_items().get_count() > 0) {
                    // pre-select the first item
                    combo.set_text(combo.get_items().getItem(0).get_text());
                    combo.set_value(combo.get_items().getItem(0).get_value());
                    combo.get_items().getItem(0).highlight();
                    // this will fire the ItemsRequested event of the
                    // job codes combobox passing the companydivisionid:customerid as a parameter 
                    jobCodesCombo.requestItems(companysCombo.get_value() + ":" + customersCombo.get_value(), false);
                    if (chkShrinkDE.get_checked() == true) {
                        DE_jobCodesCombo.requestItems(companysCombo.get_value() + ":" + customersCombo.get_value(), false);
                    }
                }
                //combo.showDropDown();
            }
            function LoadCustomers(combo, eventArqs) {
                var item = eventArqs.get_item();
                // if a company is selected
                if (item.get_index() >= 0) {
                    customersCombo.set_text("Loading...");

                    jobCodesCombo.set_text(" ");
                    jobCodesCombo.clearItems();

                    if (chkShrinkDE.get_checked() == true) {
                        DE_jobCodesCombo.set_text(" ");
                        DE_jobCodesCombo.clearItems();      
                    }

                    // this will fire the ItemsRequested event of the 
                    // Customers combobox passing the Companydivisionid as a parameter 
                    customersCombo.requestItems(item.get_value(), false);
                }
            }
            // When a customer is selected...
            function LoadJobCodes(combo, eventArqs) {
                if (customersCombo.get_value()) {
                    jobCodesCombo.set_text("Loading...");
                    jobCodesCombo.clearItems();

                    if (chkShrinkDE.get_checked() == true) {
                        DE_jobCodesCombo.set_text(" ");
                        DE_jobCodesCombo.clearItems();
                    }

                    // this will fire the ItemsRequested event of the
                    // job codes combobox passing the companydivisionid:customerid as a parameter
                    jobCodesCombo.requestItems(companysCombo.get_value() + ":" + customersCombo.get_value(), false);
                    if (chkShrinkDE.get_checked() == true) {
                        DE_jobCodesCombo.requestItems(companysCombo.get_value() + ":" + customersCombo.get_value(), false);
                    }
                }
            }
          
            // ***********************
            // GRID COMBOS
            // ***********************
            var companyGridCombo;
            function DoCompanyComboLoad(combo, eventArgs) {
                companyGridCombo = combo;
            }
            var customerGridCombo;
            function DoCustomerComboLoad(combo, eventArgs) {
                customerGridCombo = combo;
            }
            var jobCodeGridCombo;
            function DoJobCodeComboLoad(combo, eventArgs) {
                jobCodeGridCombo = combo;
            }
            // When a company is selected...
            function LoadGridCustomers(combo, eventArqs) {
                var item = eventArqs.get_item();
                // if a company is selected
                if (item.get_index() > 0) {
                    customerGridCombo.set_text("Loading...");
                    // Clear the job codes, the company changed.
                    jobCodeGridCombo.clearSelection();
                    jobCodeGridCombo.set_text("");
                    jobCodeGridCombo.clearItems();
                    // this will fire the ItemsRequested event of the 
                    // customers combobox passing the companydivisionid as a parameter 
                    customerGridCombo.requestItems(item.get_value(), false);
                } else {
                    // Clear the customers, the company changed.
                    customerGridCombo.clearSelection();
                    customerGridCombo.set_text("");
                    customerGridCombo.clearItems();
                    // Clear the job codes, the company changed.
                    jobCodeGridCombo.clearSelection();
                    jobCodeGridCombo.set_text("");
                    jobCodeGridCombo.clearItems();
                }
            }
            // When a customer is selected...
            function LoadGridJobCodes(combo, eventArqs) {
                var item = eventArqs.get_item();
                if (item.get_index() > 0) {
                    jobCodeGridCombo.set_text("Loading...");
                    // this will fire the ItemsRequested event of the
                    // job codes combobox passing the companydivisionid:customerid as a parameter 
                    jobCodeGridCombo.requestItems(companyGridCombo.get_value() + ":" + customerGridCombo.get_value(), false);
                } else {
                    // Clear the job codes, the customre changed.
                    jobCodeGridCombo.clearSelection();
                    jobCodeGridCombo.set_text("");
                    jobCodeGridCombo.clearItems();
                }
            } 
            // When a job code is selected...
            function SelectGridJobCode(combo, eventArqs) {
                combo.set_text(combo.get_selectedItem().get_text());
                combo.set_value(combo.get_selectedItem().get_value());
            }
            
            // ***********************
            // Data-Entry Panel stuff.
            // ***********************
            function DE_EmployeeFocus(combo, eventArqs) {
                //combo.showDropDown();
            }
            function DE_EmployeeSelectedIndexChanged(combo, eventArqs) {
                //var next = $find("< %= dtDE_Workeddate.ClientID %>");
                //next.focused;
            }
            function DE_Time_ValueChanged(combo, eventArqs) {
                var dtStarttime = $find("<%= dtDE_Starttime.ClientID %>");
                var dtEndtime = $find("<%= dtDE_Endtime.ClientID %>");                    
                var txtHours = $find("<%= txtDE_Hours.ClientID %>");
                if ((dtStarttime.get_selectedDate() != null) &&
                   (dtEndtime.get_selectedDate() != null)) {
                    var secs = dtEndtime.get_selectedDate() - dtStarttime.get_selectedDate();
                    var hours = secs / 3600000;
                    if (hours < 0) {
                        hours = 24 + hours;
                    }
                    txtHours.set_value(hours);
                }
            }
            // ***********************
            // GRID Panel stuff.
            // ***********************
            var dtGridStart;
            function DoGridStartDtLoad(dt, eventArgs) {
                dtGridStart = dt;                                                          
            }
            var dtGridEnd;
            function DoGridEndDtLoad(dt, eventArgs) {
                dtGridEnd = dt;
            }
            var txtGridHours;
            function DoGridHoursLoad(txt, eventArgs) {
                txtGridHours = txt;
            }
            function Grid_Time_ValueChanged(dt, eventArqs) {
                if ((dtGridStart.get_selectedDate() != null) &&
                   (dtGridEnd.get_selectedDate() != null)) {
                    var secs = dtGridEnd.get_selectedDate() - dtGridStart.get_selectedDate();
                    var hours = secs / 3600000;
                    if (hours < 0) {
                        hours = 24 + hours;
                    }
                    txtGridHours.set_value(hours);
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" RequestQueueSize="3">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnDE_Add">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ajaxPanelDE" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cmbDE_WorkCardNumber">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ajaxPanelDE" />
                    <%--<telerik:AjaxUpdatedControl ControlID="lblDE_WorkCardDescr" />
                    <telerik:AjaxUpdatedControl ControlID="txtDE_WorkCardDescr" />
                    <telerik:AjaxUpdatedControl ControlID="reqDE_WorkCardDescr" />
                    <telerik:AjaxUpdatedControl ControlID="lblDE_WorkCardHourlyRate" />
                    <telerik:AjaxUpdatedControl ControlID="txtDE_WCHourlyRate" />
                    <telerik:AjaxUpdatedControl ControlID="reqDE_WCHourlyRate" />
                    <telerik:AjaxUpdatedControl ControlID="lblDE_WCBudgetHours" />
                    <telerik:AjaxUpdatedControl ControlID="txtDE_WCBudgetHours" />
                    <telerik:AjaxUpdatedControl ControlID="reqDE_WCBudgetHours" />
                    <telerik:AjaxUpdatedControl ControlID="lblDE_WCBudgetAmount" />
                    <telerik:AjaxUpdatedControl ControlID="txtDE_WCBudgetAmount" />
                    <telerik:AjaxUpdatedControl ControlID="reqDE_WCBudgetAmount" />
                    <telerik:AjaxUpdatedControl ControlID="chkDE_WorkCardCompleted" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cmbGridWorkCardNumber">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ajaxPanelDE" />
                    <%--<telerik:AjaxUpdatedControl ControlID="txtGridWorkCardDescr" />
                    <telerik:AjaxUpdatedControl ControlID="lblGridWorkCardDescr" />
                    <telerik:AjaxUpdatedControl ControlID="reqGridWorkCardDescr" />
                    <telerik:AjaxUpdatedControl ControlID="lblGridWorkCardHourlyRate" />
                    <telerik:AjaxUpdatedControl ControlID="txtGridWCHourlyRate" />
                    <telerik:AjaxUpdatedControl ControlID="reqGridWorkCardHourlyRate" />
                    <telerik:AjaxUpdatedControl ControlID="chkGridWorkCardCompleted" />--%>                    
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="lstCompanys">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ajaxPanelDE" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="lstCustomers">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ajaxPanelDE" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="lstDE_JobCodes">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ajaxPanelDE" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ajaxGridPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="true" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" Width="250" Height="75">
    </telerik:RadWindowManager>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Time Sheet Research and Data Entry"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Time Sheet Research and Data Entry</div>            
            <div style="width: 100%; margin: 5px 5px 5px 5px">
                <!-- ********* -->
                <!-- SEARCH    -->
                <!-- ********* -->
                <fieldset style="width: 96%">
                    <legend>Search Criteria</legend>
                    <telerik:RadAjaxPanel ID="ajaxSearchPanel" runat="server" EnableAJAX="true">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 25%">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFromDt" runat="server" SkinID="InfoLabelBold" Text="From Date:*"></asp:Label>
                                                <br />
                                                <telerik:RadDatePicker ID="dtFrom" runat="server" Width="92%">
                                                    <ClientEvents OnDateSelected="ValidateDateRange" />
                                                </telerik:RadDatePicker>
                                                <asp:RequiredFieldValidator ID="reqDtFrom" runat="server" CssClass="stdValidator"
                                                    ControlToValidate="dtFrom" Display="Dynamic" ErrorMessage="*"
                                                    ValidationGroup="SEARCH">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblToDt" runat="server" SkinID="InfoLabelBold" Text="To Date:*"></asp:Label>
                                                <br />
                                                <telerik:RadDatePicker ID="dtTo" runat="server" Width="92%">
                                                    <ClientEvents OnDateSelected="ValidateDateRange" />
                                                </telerik:RadDatePicker>
                                                <asp:RequiredFieldValidator ID="reqDtTo" runat="server" CssClass="stdValidator" ControlToValidate="dtTo"
                                                    Display="Dynamic" ErrorMessage="*" ValidationGroup="SEARCH">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 25%">
                                    <asp:Label ID="lblEmployee" runat="server" SkinID="InfoLabelBold" Text="Employee:"></asp:Label>
                                    <asp:Label ID="lblEmployeeName" runat="server" SkinID="SmallLabel" Text=""></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="lstEmployees" runat="server" DataSourceID="LinqDataSource1"
                                        AppendDataBoundItems="true" DataValueField="Userid" DataTextField="Employeenumber"
                                        Width="96%" HighlightTemplatedItems="true" AutoPostBack="true" 
                                        OnSelectedIndexChanged="lstEmployees_SelectedIndexChanged">
                                        <Items>
                                            <telerik:RadComboBoxItem Selected="true" Value="" Text="- None -" />
                                        </Items>
                                        <HeaderTemplate>
                                            <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                                width: 40%">
                                                Emp #</div>
                                            <%--<div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                                width: 40%">
                                                User Name</div>--%>
                                            <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                                width: 60%">
                                                Full Name</div>
                                        </HeaderTemplate>     
                                        <ItemTemplate>
                                            <div class="lblInfo" style="position: relative; float: left; width: 40%">
                                                <%# Eval("Employeenumber") %>
                                            </div>
                                            <%--<div class="lblInfo" style="position: relative; float: left; width: 40%">
                                                <%# Eval("Username") %>
                                            </div>--%>
                                            <div class="lblInfo" style="position: relative; float: left; width: 60%">
                                                <%# Eval("Fullname") %>
                                            </div>
                                        </ItemTemplate>
                                    </telerik:RadComboBox>
                                </td>
                                <td style="width: 25%">
                                    <asp:Label ID="lblWorkCardNumber" runat="server" SkinID="InfoLabelBold" Text="Card #:"></asp:Label>
                                    <br />
                                    <telerik:RadTextBox ID="txtWorkCardNumber" runat="server" MaxLength="30" SelectionOnFocus="SelectAll" Width="96%">
                                    </telerik:RadTextBox>
                                </td>
                                <td style="width: 25%">
                                    <asp:Label ID="lblUpdUsers" runat="server" SkinID="InfoLabelBold" Text="Update User:"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="lstUpdUsers" runat="server" DataSourceID="LinqDataSource1"
                                        AppendDataBoundItems="true" DataValueField="Userid" DataTextField="Fullname"
                                        Width="96%" HighlightTemplatedItems="true">
                                        <Items>
                                            <telerik:RadComboBoxItem Selected="true" Value="" Text="- None -" />
                                        </Items>
                                        <HeaderTemplate>
                                            <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                                width: 40%">
                                                Emp #</div>
                                            <%--<div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                                width: 40%">
                                                User Name</div>--%>
                                            <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                                width: 60%">
                                                Full Name</div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="lblInfo" style="position: relative; float: left; width: 40%">
                                                <%# Eval("Employeenumber") %>
                                            </div>
                                            <%--<div class="lblInfo" style="position: relative; float: left; width: 40%">
                                                <%# Eval("Username") %>
                                            </div>--%>
                                            <div class="lblInfo" style="position: relative; float: left; width: 60%">
                                                <%# Eval("Fullname") %>
                                            </div>
                                        </ItemTemplate>
                                    </telerik:RadComboBox>
                                </td>
                                <%--<td rowspan="3" align="center" style="width: 15%">
                                    <fieldset style="display: none">
                                        <legend>Reconcile Options</legend>
                                        <asp:Button ID="btnReconcileSelected" runat="server" Enabled="false" Text="Reconcile Selected" OnClick="btnReconcileSelected_OnClick" />
                                        <br />
                                        <asp:Label ID="lblInstructions1" runat="server" SkinID="SubscriptLabel" Text="(Reconcile Selected Time Sheet)"></asp:Label>
                                        <br />
                                        <asp:Button ID="btnReconcileAll" runat="server" Enabled="false" Text="Reconcile All" OnClick="btnReconcileAll_OnClick" />
                                        <br />
                                        <asp:Label ID="lblInstructions2" runat="server" SkinID="SubscriptLabel" Text="(Reconcile All Time Sheets)"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblStatus" runat="server" SkinID="InfoLabelBold" Text="Status:"></asp:Label>
                                        <asp:Label ID="lblReconcileStatus" runat="server" ForeColor="Black" SkinID="InfoLabelBold" Text="[none]"></asp:Label>
                                    </fieldset>                      
                                    Companys OnClientSelectedIndexChanged="LoadCustomers"
                                    Customers OnClientSelectedIndexChanged="LoadJobCodes" OnClientItemsRequested="CustomerItemsLoaded" OnItemsRequested="lstCustomers_ItemsRequested"
                                    JobCodes OnClientItemsRequested="ItemsLoaded" OnItemsRequested="lstJobcodes_ItemsRequested"
                                </td>--%>
                            </tr>
                            <tr>
                                <td style="width: 25%">
                                    <asp:Label ID="lblCompany" runat="server" SkinID="InfoLabelBold" Text="Company:*"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="lstCompanys" runat="server" DataValueField="Companydivisionid"
                                        DataTextField="Companydescr" Width="96%" ValidationGroup="SEARCH" 
                                        AutoPostBack="true" OnSelectedIndexChanged="lstCompanys_SelectedIndexChanged">
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="reqCompany" runat="server" CssClass="stdValidator"
                                        ControlToValidate="lstCompanys" Display="Dynamic" ErrorMessage="*"
                                        ValidationGroup="SEARCH">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 25%">
                                    <asp:Label ID="lblCustomer" runat="server" SkinID="InfoLabelBold" Text="Customer:*"></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="lstCustomers" runat="server" DataValueField="Customerid"
                                        DataTextField="Customerdescr" Width="96%" ValidationGroup="SEARCH" 
                                        AutoPostBack="true" OnSelectedIndexChanged="lstCustomers_SelectedIndexChanged">
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="reqCustomer" runat="server" CssClass="stdValidator"
                                        ControlToValidate="lstCustomers" Display="Dynamic" ErrorMessage="*"
                                        ValidationGroup="SEARCH">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 25%">
                                    <asp:Label ID="lblJobCode" runat="server" SkinID="InfoLabelBold" Text="Job Code:"></asp:Label>
                                    <asp:Label ID="lblJobCodeName" runat="server" SkinID="SmallLabel" Text=""></asp:Label>
                                    <br />
                                    <telerik:RadComboBox ID="lstJobCodes" runat="server" DataValueField="Jobcodeid" DataTextField="Code"
                                        HighlightTemplatedItems="true" 
                                        AutoPostBack="true" OnSelectedIndexChanged="lstJobCodes_SelectedIndexChanged" Width="96%">
                                        <HeaderTemplate>
                                            <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                                width: 50%">
                                                Code</div>
                                            <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                                width: 50%">
                                                Description</div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="lblInfo" style="position: relative; float: left; width: 50%">
                                                <%# Eval("Code") %>
                                            </div>
                                            <div class="lblInfo" style="position: relative; float: left; width: 50%">
                                                <%# Eval("Jobcodedescr")%>
                                            </div>
                                        </ItemTemplate>
                                    </telerik:RadComboBox>
                                </td>
                                <td style="width: 25%">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_OnClick" ValidationGroup="SEARCH" />
                                </td>
                            </tr>
                        </table>
                    </telerik:RadAjaxPanel>
                </fieldset>
                <br />
                <!-- ********* -->
                <!-- DATA ENTRY -->
                <!-- ********* -->
                <fieldset style="width: 96%">
                    <legend>Data Entry&nbsp;
                        <telerik:RadButton ID="chkShrinkDE" runat="server" ToggleType="CheckBox" AutoPostBack="true"
                            Text="Check to Show" Checked="true" Enabled="false" Visible="false" ButtonType="ToggleButton" OnClientLoad="DE_ShrinkDE_Load"
                            OnCheckedChanged="chkShrinkDE_CheckChanged" CausesValidation="false" />
                    </legend>
                    <div id="divDE" runat="server" visible="true">
                        <telerik:RadAjaxPanel ID="ajaxPanelDE" runat="server" EnableAJAX="true">
                            <table width="100%" style="margin: 5px 5px 5px 5px">
                                <tr>
                                    <td valign="top" style="width: 10%">
                                        <asp:Label ID="lblDE_Employee" runat="server" SkinID="InfoLabelBold" Text="Employee*"></asp:Label>
                                        <br />
                                        <telerik:RadTextBox ID="txtDE_Employees" runat="server" AutoPostBack="true" Width="90%" 
                                            ValidationGroup="DATAENTRY" CausesValidation="false" SelectionOnFocus="SelectAll"
                                            OnTextChanged="txtDE_Employees_OnTextChanged">
                                        </telerik:RadTextBox>
                                        <asp:RequiredFieldValidator ID="reqDE_Employees" runat="server" CssClass="stdValidator"
                                            ControlToValidate="txtDE_Employees" Display="Static" ErrorMessage="*"
                                            SetFocusOnError="true" ValidationGroup="DATAENTRY">
                                        </asp:RequiredFieldValidator>
                                        <asp:Label ID="lblDE_EmployeeName" runat="server" SkinID="SmallLabel" Text=""></asp:Label>
                                    </td>
                                    <td valign="top" style="width: 17%">
                                        <asp:Label ID="lblDE_Workeddate" runat="server" SkinID="InfoLabelBold" Text="Work Date*"></asp:Label>
                                        <br />
                                        <telerik:RadDatePicker ID="dtDE_Workeddate" runat="server" DatePopupButton-Visible="false"
                                            Width="90%" DateInput-SelectionOnFocus="SelectAll">
                                            <DateInput runat="server" SelectionOnFocus="SelectAll">
                                            </DateInput>
                                        </telerik:RadDatePicker>
                                        <asp:RequiredFieldValidator ID="reqDE_Workeddate" runat="server" CssClass="stdValidator"
                                            ControlToValidate="dtDE_Workeddate" Display="Static" ErrorMessage="*"
                                            SetFocusOnError="true" ValidationGroup="DATAENTRY">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td valign="top" style="width: 25%">
                                        <asp:Label ID="lblDE_Jobcode" runat="server" SkinID="InfoLabelBold" Text="Job Code*"></asp:Label>
                                        <br />
                                        <telerik:RadComboBox ID="lstDE_Jobcodes" runat="server" AutoPostBack="true" DataValueField="Jobcodeid"
                                            DataTextField="Code" OnClientItemsRequested="ItemsLoaded" OnItemsRequested="lstDE_Jobcodes_ItemsRequested"
                                            HighlightTemplatedItems="true" OnSelectedIndexChanged="lstDE_Jobcodes_SelectedIndexChanged" 
                                            Width="90%" ValidationGroup="DATAENTRY" CausesValidation="false">
                                            <HeaderTemplate>
                                                <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                                    width: 50%">
                                                    Code</div>
                                                <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                                    width: 50%">
                                                    Description</div>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="lblInfo" style="position: relative; float: left; width: 50%">
                                                    <%# Eval("Code") %>
                                                </div>
                                                <div class="lblInfo" style="position: relative; float: left; width: 50%">
                                                    <%# Eval("Jobcodedescr")%>
                                                </div>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>
                                        <asp:RequiredFieldValidator ID="reqDE_Jobcode" runat="server" CssClass="stdValidator"
                                            ControlToValidate="lstDE_Jobcodes" Display="Static" ErrorMessage="*"
                                            SetFocusOnError="true" ValidationGroup="DATAENTRY">
                                        </asp:RequiredFieldValidator>
                                        <asp:Label ID="lblDE_JobcodeName" runat="server" SkinID="SmallLabel" Text=""></asp:Label>
                                    </td>
                                    <td valign="top" style="width: 12%">
                                        <asp:Label ID="lblDE_Starttime" runat="server" SkinID="InfoLabelBold" Text="Time In*"></asp:Label>
                                        <br />
                                        <telerik:RadTimePicker ID="dtDE_Starttime" runat="server" DateInput-ClientEvents-OnValueChanged="DE_Time_ValueChanged"
                                            TimePopupButton-Visible="false" DateInput-SelectionOnFocus="SelectAll" Width="90%"></telerik:RadTimePicker>
                                        <asp:RequiredFieldValidator ID="reqDE_Starttime" runat="server" CssClass="stdValidator"
                                            ControlToValidate="dtDE_Starttime" Display="Static" ErrorMessage="*"
                                            SetFocusOnError="true" ValidationGroup="DATAENTRY">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td valign="top" style="width: 12%">
                                        <asp:Label ID="lblDE_Endtime" runat="server" SkinID="InfoLabelBold" Text="Time Out*"></asp:Label>
                                        <br />
                                        <telerik:RadTimePicker ID="dtDE_Endtime" runat="server" DateInput-ClientEvents-OnValueChanged="DE_Time_ValueChanged"
                                            TimePopupButton-Visible="false" DateInput-SelectionOnFocus="SelectAll" Width="90%"></telerik:RadTimePicker>
                                        <asp:RequiredFieldValidator ID="reqDE_Endtime" runat="server" CssClass="stdValidator" ControlToValidate="dtDE_Endtime"
                                            Display="Static" ErrorMessage="*" SetFocusOnError="true" ValidationGroup="DATAENTRY">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td valign="top" style="width: 12%">
                                        <asp:Label ID="lblDE_Hours" runat="server" SkinID="InfoLabelBold" Text="Hours*"></asp:Label>
                                        <br />
                                        <telerik:RadNumericTextBox ID="txtDE_Hours" runat="server" MaxValue="1000.00" MinValue="0.00"
                                            Enabled="false" EnabledStyle-ForeColor="Black" NumberFormat-DecimalDigits="2"
                                            SelectionOnFocus="SelectAll" AllowOutOfRangeAutoCorrect="true" Width="55%" ValidationGroup="DATAENTRY">
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="reqDE_Hours" runat="server" CssClass="stdValidator"
                                            ControlToValidate="txtDE_Hours" Display="Static" ErrorMessage="*"
                                            SetFocusOnError="true" ValidationGroup="DATAENTRY">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td valign="top" style="width: 12%">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:Label ID="lblDE_TailNumber" runat="server" SkinID="InfoLabelBold" Text="Tail Number"></asp:Label>
                                        <br />
                                        <telerik:RadTextBox ID="txtDE_TailNumber" runat="server" MaxLength="30" SelectionOnFocus="SelectAll"
                                            AutoPostBack="true" OnTextChanged="txtDE_TailNumber_OnTextChanged" Width="90%"
                                            ValidationGroup="DATAENTRY">
                                        </telerik:RadTextBox>
                                        <asp:CustomValidator ID="reqDE_TailNumber" runat="server" CssClass="stdValidator"
                                            Enabled="true" ControlToValidate="txtDE_TailNumber" Display="Static"
                                            ErrorMessage="*" OnServerValidate="reqDE_TailNumberValidate" ValidateEmptyText="true"
                                            ValidationGroup="DATAENTRY">
                                        </asp:CustomValidator>
                                        <asp:Label ID="lblDE_TailDescr" runat="server" SkinID="SmallLabel" Text=""></asp:Label>
                                    </td>
                                    <td valign="top">
                                        <asp:Label ID="lblDE_WorkCardNumber" runat="server" SkinID="InfoLabelBold" Text="Card #"></asp:Label>
                                        <br />
                                        <atg:WorkCardSelectorControl ID="cmbDE_WorkCardNumber" runat="server" Width="100%"
                                            OnTextChanged="cmbDE_WorkCardNumber_OnTextChanged" ValidationGroup="DATAENTRY">
                                        </atg:WorkCardSelectorControl>
                                    </td>
                                    <td valign="top">
                                        <asp:Label ID="lblDE_WorkCardDescr0" runat="server" SkinID="InfoLabelBold" Text="Work Card Description"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblDE_WorkCardDescr" runat="server" SkinID="InfoLabel" Visible="false"></asp:Label>
                                        <telerik:RadTextBox ID="txtDE_WorkCardDescr" runat="server" MaxLength="100" SelectionOnFocus="SelectAll"
                                            Width="90%" ValidationGroup="DATAENTRY">
                                        </telerik:RadTextBox>
                                        <asp:RequiredFieldValidator ID="reqDE_WorkCardDescr" runat="server" CssClass="stdValidator"
                                            Enabled="false" ControlToValidate="txtDE_WorkCardDescr" Display="Static"
                                            ErrorMessage="*" SetFocusOnError="true" ValidationGroup="DATAENTRY">                                        
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td valign="top">
                                        <asp:Label ID="lblDE_WorkCardHourlyRate0" runat="server" SkinID="InfoLabelBold" Text="WC Hourly Rate"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblDE_WorkCardHourlyRate" runat="server" Visible="false" SkinID="InfoLabel"></asp:Label>
                                        <telerik:RadNumericTextBox ID="txtDE_WCHourlyRate" runat="server" AllowOutOfRangeAutoCorrect="true"
                                            DataType="System.Currency" NumberFormat-DecimalDigits="2" MaxValue="1000.00"
                                            MinValue="0.00" SelectionOnFocus="SelectAll" Width="90%" ValidationGroup="DATAENTRY">
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="reqDE_WCHourlyRate" runat="server" CssClass="stdValidator"
                                            Enabled="false" ControlToValidate="txtDE_WCHourlyRate" Display="Static"
                                            ErrorMessage="*" SetFocusOnError="true" ValidationGroup="DATAENTRY">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td valign="top">
                                        <asp:Label ID="lblDE_WCBudgetHours0" runat="server" SkinID="InfoLabelBold" Text="WC Budget Hours"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblDE_WCBudgetHours" runat="server" Visible="false" SkinID="InfoLabel"></asp:Label>
                                        <telerik:RadNumericTextBox ID="txtDE_WCBudgetHours" runat="server" AllowOutOfRangeAutoCorrect="true"
                                            DataType="System.Decimal" NumberFormat-DecimalDigits="2" MaxValue="100000.00"
                                            MinValue="0.00" SelectionOnFocus="SelectAll" Width="90%" ValidationGroup="DATAENTRY">
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="reqDE_WCBudgetHours" runat="server" CssClass="stdValidator"
                                            Enabled="false" ControlToValidate="txtDE_WCBudgetHours" Display="Static"
                                            ErrorMessage="*" SetFocusOnError="true" ValidationGroup="DATAENTRY">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td valign="top">
                                        <asp:Label ID="lblDE_WCBudgetAmount0" runat="server" SkinID="InfoLabelBold" Text="WC Budget Amt"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblDE_WCBudgetAmount" runat="server" Visible="false" SkinID="InfoLabel"></asp:Label>
                                        <telerik:RadNumericTextBox ID="txtDE_WCBudgetAmount" runat="server" AllowOutOfRangeAutoCorrect="true"
                                            DataType="System.Currency" NumberFormat-DecimalDigits="2" MaxValue="1000000.00"
                                            MinValue="0.00" SelectionOnFocus="SelectAll" Width="90%" ValidationGroup="DATAENTRY">
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="reqDE_WCBudgetAmount" runat="server" CssClass="stdValidator"
                                            Enabled="false" ControlToValidate="txtDE_WCBudgetAmount" Display="Static"
                                            ErrorMessage="*" SetFocusOnError="true" ValidationGroup="DATAENTRY">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td valign="top" colspan="2">
                                        <asp:Label ID="lblDE_WorkCardCompleted" runat="server" SkinID="InfoLabelBold" Text="WC Completed?"></asp:Label>
                                        <br />
                                        <asp:CheckBox ID="chkDE_WorkCardCompleted" runat="server" ValidationGroup="DATAENTRY">
                                        </asp:CheckBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" colspan="5">
                                        <asp:Label ID="lblDE_Notes" runat="server" SkinID="InfoLabelBold" Text="Notes"></asp:Label>
                                        <br />
                                        <telerik:RadTextBox ID="txtDE_Notes" runat="server" SelectionOnFocus="SelectAll"
                                            Wrap="false" MaxLength="255" TextMode="SingleLine" Width="98%" ValidationGroup="DATAENTRY">
                                        </telerik:RadTextBox>
                                    </td>
                                    <td valign="bottom" colspan="2">
                                        <asp:Button ID="btnDE_Add" runat="server" Text="Add" OnClick="btnDE_Add_OnClick" ValidationGroup="DATAENTRY" />
                                        &nbsp;
                                        <asp:Label ID="lblAddedLbl" runat="server" SkinID="InfoLabelBold" Text="Added:"></asp:Label>
                                        &nbsp;
                                        <asp:Label ID="lblAdded" runat="server" ForeColor="Green" SkinID="InfoLabelBold" Text="0"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </telerik:RadAjaxPanel>
                    </div>
                </fieldset>
                <br />
                <!-- ********* -->
                <!-- RESULTS   -->
                <!-- ********* -->
                <fieldset style="width: 96%">
                    <legend>Search Results</legend>
                    <div style="border: 1px solid Black; position: relative; float: left; height: 450px;
                        width: 99%; margin: 10px 5px 5px 5px">
                        <telerik:RadAjaxPanel ID="ajaxGridPanel" runat="server" EnableAJAX="true" ClientEvents-OnRequestStart="onRequestStart"
                            Width="100%" RenderMode="Block">
                            <div style="vertical-align:middle">
                                &nbsp;&nbsp;<asp:Label ID="lblCount" runat="server" BackColor="Silver" ForeColor="Navy" Font-Bold="true" Text="&nbsp;Found:&nbsp;0"></asp:Label>
                                &nbsp;&nbsp;
                                <asp:Label ID="lblSort1" runat="server" ForeColor="Green" Text="&nbsp;Sort Ascending or Descending:&nbsp;"></asp:Label>
                                <asp:RadioButton ID="btnAsc" runat="server" Text="ASC" Checked="true" GroupName="SORT2"
                                    AutoPostBack="true" OnCheckedChanged="Sort_CheckChanged" />
                                <asp:RadioButton ID="btnDesc" runat="server" Text="DESC" GroupName="SORT2" AutoPostBack="true"
                                    OnCheckedChanged="Sort_CheckChanged" />
                                <asp:Label ID="lblSort2" runat="server" ForeColor="Green" Text="&nbsp;Sort By:&nbsp;"></asp:Label>
                                <telerik:RadComboBox ID="lstSort" runat="server" AutoPostBack="true" 
                                    OnSelectedIndexChanged="lstSortSelectedIndexChanged">
                                    <Items>
                                        <telerik:RadComboBoxItem Value="NONE" Text="No Sort / Default" Checked="true" />
                                        <telerik:RadComboBoxItem Value="EMPNAME" Text="Employee Name" Checked="false" />
                                        <telerik:RadComboBoxItem Value="EMPNUM" Text="Employee Number" Checked="false" />
                                        <telerik:RadComboBoxItem Value="JOBCODE" Text="Job Code" Checked="false" />
                                        <telerik:RadComboBoxItem Value="TAILNUM" Text="Tail Number" Checked="false" />
                                        <telerik:RadComboBoxItem Value="UPDUSERID" Text="Update User" Checked="false" />
                                        <telerik:RadComboBoxItem Value="WCNUM" Text="Work Card Number" Checked="false" />
                                        <telerik:RadComboBoxItem Value="WORKEDDT" Text="Worked Date" Checked="false" />
                                    </Items>                                
                                </telerik:RadComboBox>
                            </div>
                            <telerik:RadGrid ID="RadGrid1" GridLines="None"
                                Height="432px" runat="server" AllowMultiRowEdit="false" AllowMultiRowSelection="false"
                                AllowPaging="false" AllowSorting="false" AutoGenerateColumns="false" 
                                OnDataBound="RadGrid1_DataBound" OnItemCommand="RadGrid1_ItemCommand" 
                                OnSelectedIndexChanged="RadGrid1_SelectedIndexChanged" Width="100%" 
                                OnItemDataBound="RadGrid1_ItemDataBound">
                                <MasterTableView CommandItemDisplay="Top" DataKeyNames="Timesheetid" TableLayout="Fixed"
                                    HorizontalAlign="Left" EditMode="InPlace">
                                    <CommandItemTemplate>
                                        <%--<div style="position: relative; float: left">
                                            <asp:Button ID="btnNew" runat="server" CommandName="InitInsert" Text="Add Time Sheet"></telerik:RadButton>
                                        </div>--%>
                                        <div style="position: relative; float: right">
                                            <asp:Button ID="btnExportToExcel" runat="server" Text="Export to Excel" Visible='<%# ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_TIMESHEETS) %>'
                                                OnClick="btnExportToExcel_OnClick" />
                                            <asp:Button ID="btnExportToADP" runat="server" Text="Export to ADP" Visible='<%# ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_TIMESHEETS) %>'
                                                OnClick="btnExportToADP_OnClick" />
                                        </div>
                                    </CommandItemTemplate>
                                    <Columns>
                                        <telerik:GridTemplateColumn UniqueName="CustomTemplateColumn">
                                            <ItemTemplate>
                                                <table style="border-collapse: collapse; line-height: 12px; margin: 10px 5px 5px 5px;
                                                    width: 98%; table-layout: fixed">
                                                    <tr>
                                                        <td rowspan="4" style="width: 40px">
                                                            <asp:ImageButton ID="btnEdit" runat="server" SkinID="Edit" OnClick="btnEdit_OnClick" ValidationGroup="GRID" />
                                                        </td>
                                                        <td style="background-color: Khaki">
                                                            <asp:Label ID="lblEmployee1" runat="server" SkinID="InfoLabelBold" Text="Employee:"></asp:Label>
                                                            <asp:Label ID="lblEmployee2" runat="server" ForeColor="Green" SkinID="InfoLabelBold" Text='<%# Eval("Extendeduser.Employeenumber") %>'></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblEmployee3" runat="server" SkinID="SmallLabel" Text='<%# Eval("Extendeduser.Fullname") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCompany1" runat="server" SkinID="InfoLabelBold" Text="Company Division:"></asp:Label>
                                                            <asp:Label ID="lblCompany2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Companydivision.Description") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCustomer1" runat="server" SkinID="InfoLabelBold" Text="Customer:"></asp:Label>
                                                            <asp:Label ID="lblCustomer2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Customer.Description") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblJobcode1" runat="server" SkinID="InfoLabelBold" Text="Job Code:"></asp:Label>
                                                            <asp:Label ID="lblJobcode2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Jobcode.Code") %>'></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblGridJobcodeName" runat="server" SkinID="SmallLabel" Text='<%# Eval("Jobcode.Description") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblUpdUserName1" runat="server" SkinID="InfoLabelBold" Text="Upd User Name:"></asp:Label>
                                                            <asp:Label ID="lblUpdUserName2" runat="server" SkinID="InfoLabel" Text='<%# Eval("UpdUserId") %>'></asp:Label>
                                                        </td>
                                                        <td rowspan="4" style="width: 20px">
                                                            <asp:ImageButton ID="btnDelete" runat="server" SkinID="Delete" OnClick="btnDelete_OnClick" ValidationGroup="GRID" />
                                                            <ajaxToolkit:ConfirmButtonExtender ID="confirmDelete" runat="server" TargetControlID="btnDelete"
                                                                ConfirmText="Delete Time Sheet?">
                                                            </ajaxToolkit:ConfirmButtonExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblWorkeddate1" runat="server" SkinID="InfoLabelBold" Text="Worked Date:"></asp:Label>
                                                            <asp:Label ID="lblWorkeddate2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Workeddate", "{0:d}") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblStarttime1" runat="server" SkinID="InfoLabelBold" Text="Start Time:"></asp:Label>
                                                            <asp:Label ID="lblStarttime2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Starttime", "{0:t}") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblEndtime1" runat="server" SkinID="InfoLabelBold" Text="End Time:"></asp:Label>
                                                            <asp:Label ID="lblEndtime2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Endtime", "{0:t}") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblHours1" runat="server" SkinID="InfoLabelBold" Text="Hours:"></asp:Label>
                                                            <asp:Label ID="lblHours2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Hours", "{0:f}") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblRate1" runat="server" SkinID="InfoLabelBold" Text="Hourly Rate:" Visible='<%# ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EMPLOYEE_RATE) %>'>
                                                            </asp:Label>
                                                            <asp:Label ID="lblRate2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Hourlyrate", "{0:c}") %>'
                                                                Visible='<%# ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EMPLOYEE_RATE) %>'>
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblTailNumber1" runat="server" SkinID="InfoLabelBold" Text="Tail Number:"></asp:Label>
                                                            <asp:Label ID="lblTailNumber2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Equipment.Tailnumber") %>'></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblTailDescr1" runat="server" SkinID="SmallLabel" Text='<%# Eval("Equipment.Description") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblWorkCardNumber1" runat="server" SkinID="InfoLabelBold" Text="Work Card Number:"></asp:Label>
                                                            <asp:Label ID="lblWorkCardNumber2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Workcard.Cardnumber") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblWorkCardDescr1" runat="server" SkinID="InfoLabelBold" Text="Work Card Description:"></asp:Label>
                                                            <asp:Label ID="lblWorkCardDescr2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Workcard.Description") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblWorkCardHourlyRate1" runat="server" SkinID="InfoLabelBold" Text="Work Card Hourly Rate:"
                                                                Visible='<%# ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE) %>'>
                                                            </asp:Label>
                                                            <asp:Label ID="lblWorkCardHourlyRate2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Workcard.Hourlyrate", "{0:c}") %>'
                                                                Visible='<%# ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE) %>'>
                                                            </asp:Label>
                                                        </td>
                                                        <td valign="middle">
                                                            <asp:Label ID="lblWorkCardCompleted1" runat="server" SkinID="InfoLabelBold" Text="Work Card Completed?:"></asp:Label>
                                                            <asp:CheckBox ID="lblWorkCardCompleted2" runat="server" Enabled="false" 
                                                                Checked='<%# Eval("Workcard") == null ? false : Eval("Workcard.Completed") == DBNull.Value ? false : Eval("Workcard.Completed") %>'
                                                                Width="10px"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">
                                                            <asp:Label ID="lblNotes1" runat="server" SkinID="InfoLabelBold" Text="Notes:"></asp:Label>
                                                            <asp:Label ID="lblNotes2" runat="server" SkinID="InfoLabel" Text='<%# Eval("Notes") %>'></asp:Label>
                                                        </td>
                                                        <%--<td colspan="1">
                                                            <asp:Label ID="lblReconciledStatus1" runat="server" SkinID="InfoLabelBold" Text="Reconciled Status:"></asp:Label>
                                                            <asp:Label ID="lblReconciledStatus2" runat="server" ForeColor='<%# ATGDB.Timesheet.GetReconciledColor( Eval("ReconciledStatus").ToString() ) %>'
                                                                SkinID="InfoLabelBold" Text='<%# ATGDB.Timesheet.GetReconciledDescr( Eval("ReconciledStatus").ToString() ) %>'>
                                                            </asp:Label>
                                                        </td>--%>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <table style="border-collapse: collapse; line-height: 12px; margin: 10px 5px 5px 5px;
                                                    width: 98%; table-layout: fixed">
                                                    <tr>
                                                        <td rowspan="4" style="width: 60px">
                                                            <asp:ImageButton ID="btnSave" runat="server" SkinID="Save" OnClick="btnSave_OnClick" ValidationGroup="GRID" />
                                                            <asp:ImageButton ID="btnCancel" runat="server" SkinID="Cancel" OnClick="btnCancel_OnClick" CausesValidation="false" />
                                                        </td>
                                                        <td style="background-color:Khaki; width: 20%">
                                                            <asp:Label ID="lblGridEmployee" runat="server" SkinID="InfoLabelBold" Text="Employee:"></asp:Label>
                                                            <asp:Label ID="lblGridEmployeeName" runat="server" SkinID="SmallLabel" Text='<%# Eval("Extendeduser.Fullname") %>'></asp:Label>
                                                            <telerik:RadTextBox ID="txtGridEmployees" runat="server" AutoPostBack="true" Width="90%"
                                                                ValidationGroup="GRID" CausesValidation="false" SelectionOnFocus="SelectAll"
                                                                OnTextChanged="txtGridEmployees_OnTextChanged" Text='<%# Bind("Extendeduser.Employeenumber") %>'>
                                                            </telerik:RadTextBox>
                                                            <asp:RequiredFieldValidator ID="reqGridEmployee" runat="server" CssClass="stdValidator"
                                                                ControlToValidate="txtGridEmployees" Display="Dynamic" ErrorMessage="*"
                                                                ValidationGroup="GRID">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblGridCompany" runat="server" SkinID="InfoLabelBold" Text="Company Division:"></asp:Label>
                                                            <telerik:RadComboBox ID="lstGridCompanys" runat="server" Width="90%"
                                                                DataValueField="Companydivisionid" DataTextField="Companydescr"
                                                                OnClientLoad="DoCompanyComboLoad"
                                                                OnClientSelectedIndexChanged="LoadGridCustomers" 
                                                                OnItemsRequested="lstGridCompanys_ItemsRequested">
                                                            </telerik:RadComboBox>
                                                            <asp:RequiredFieldValidator ID="reqGridCompany" runat="server" CssClass="stdValidator"
                                                                ControlToValidate="lstGridCompanys" Display="Dynamic" ErrorMessage="*"
                                                                ValidationGroup="GRID">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td style="width: 19%">
                                                            <asp:Label ID="lblGridCustomer" runat="server" SkinID="InfoLabelBold" Text="Customer:"></asp:Label>
                                                            <telerik:RadComboBox ID="lstGridCustomers" runat="server" Width="90%"
                                                                DataValueField="Customerid" DataTextField="Customerdescr"
                                                                OnClientLoad="DoCustomerComboLoad"
                                                                OnClientSelectedIndexChanged="LoadGridJobCodes" 
                                                                OnClientItemsRequested="ItemsLoaded"
                                                                OnItemsRequested="lstGridCustomers_ItemsRequested" />
                                                            <asp:RequiredFieldValidator ID="reqGridCustomer" runat="server" CssClass="stdValidator"
                                                                ControlToValidate="lstGridCustomers" Display="Dynamic" ErrorMessage="*"
                                                                ValidationGroup="GRID">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td style="width: 19%">
                                                            <asp:Label ID="lblGridJobcode" runat="server" SkinID="InfoLabelBold" Text="Job Code:"></asp:Label>
                                                            <asp:Label ID="lblGridJobcodeName" runat="server" SkinID="SmallLabel" Text='<%# Eval("Jobcode.Description") %>'></asp:Label>
                                                            <telerik:RadComboBox ID="lstGridJobCodes" runat="server" Width="90%" DataValueField="Jobcodeid"
                                                                DataTextField="Code" AutoPostBack="true" 
                                                                HighlightTemplatedItems="true" OnSelectedIndexChanged="lstGridJobcodes_SelectedIndexChanged"
                                                                OnClientLoad="DoJobCodeComboLoad" OnClientItemsRequested="ItemsLoaded" OnItemsRequested="lstGridJobCodes_ItemsRequested">
                                                                <HeaderTemplate>
                                                                    <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                                                        width: 50%">
                                                                        Code</div>
                                                                    <div class="lblInfoBold" style="position: relative; float: left; background-color: Silver;
                                                                        width: 50%">
                                                                        Description</div>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <div class="lblInfo" style="position: relative; float: left; width: 50%">
                                                                        <%# Eval("Code") %>
                                                                    </div>
                                                                    <div class="lblInfo" style="position: relative; float: left; width: 50%">
                                                                        <%# Eval("Jobcodedescr")%>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </telerik:RadComboBox>
                                                            <asp:RequiredFieldValidator ID="reqGridJob" runat="server" CssClass="stdValidator"
                                                                ControlToValidate="lstGridJobCodes" Display="Dynamic" ErrorMessage="*" 
                                                                ValidationGroup="GRID">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td style="width: 17%">
                                                            <asp:Label ID="lblUpdUserName1" runat="server" SkinID="InfoLabelBold" Text="Upd User Name:"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblUpdUserName2" runat="server" SkinID="InfoLabel" Text='<%# Bind("UpdUserId") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblGridWorkeddate" runat="server" SkinID="InfoLabelBold" Text="Worked Date:"></asp:Label>
                                                            <br />
                                                            <telerik:RadDatePicker ID="dtGridWorkeddate" runat="server" DbSelectedDate='<%# Bind("Workeddate") %>'
                                                                Width="85%" DateInput-SelectionOnFocus="SelectAll">
                                                            </telerik:RadDatePicker>
                                                            <asp:RequiredFieldValidator ID="reqGridWorkeddate" runat="server" CssClass="stdValidator"
                                                                ControlToValidate="dtGridWorkeddate" Display="Dynamic" ErrorMessage="*"
                                                                ValidationGroup="GRID">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblGridStarttime" runat="server" SkinID="InfoLabelBold" Text="Start Time:"></asp:Label>
                                                            <br />
                                                            <telerik:RadTimePicker ID="dtGridStarttime" runat="server" DbSelectedDate='<%# Bind("Starttime") %>'
                                                                Width="85%" DateInput-SelectionOnFocus="SelectAll">
                                                                <DateInput ID="dtGridStarttimeInput" runat="server">
                                                                    <ClientEvents OnLoad="DoGridStartDtLoad" OnValueChanged="Grid_Time_ValueChanged" />
                                                                </DateInput>
                                                            </telerik:RadTimePicker>
                                                            <asp:RequiredFieldValidator ID="reqGridStarttime" runat="server" CssClass="stdValidator"
                                                                ControlToValidate="dtGridStarttime" Display="Dynamic" ErrorMessage="*"
                                                                ValidationGroup="GRID">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblGridEndtime" runat="server" SkinID="InfoLabelBold" Text="End Time:"></asp:Label>
                                                            <br />
                                                            <telerik:RadTimePicker ID="dtGridEndtime" runat="server" DbSelectedDate='<%# Bind("Endtime") %>'
                                                                Width="85%" DateInput-SelectionOnFocus="SelectAll">
                                                                <DateInput ID="dtGridEndtimeInput" runat="server">
                                                                    <ClientEvents OnLoad="DoGridEndDtLoad" OnValueChanged="Grid_Time_ValueChanged" />
                                                                </DateInput>
                                                            </telerik:RadTimePicker>
                                                            <asp:RequiredFieldValidator ID="reqGridEndtime" runat="server" CssClass="stdValidator"
                                                                ControlToValidate="dtGridEndtime" Display="Dynamic" ErrorMessage="*" 
                                                                ValidationGroup="GRID">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblGridHours" runat="server" SkinID="InfoLabelBold" Text="Hours:"></asp:Label>
                                                            <br />
                                                            <telerik:RadNumericTextBox ID="txtGridHours" runat="server" Text='<%# Bind("Hours") %>'
                                                                ClientEvents-OnLoad="DoGridHoursLoad" Enabled="false" EnabledStyle-ForeColor="Black"
                                                                AllowOutOfRangeAutoCorrect="true" NumberFormat-DecimalDigits="2" MaxValue="1000.00"
                                                                MinValue="0.00" SelectionOnFocus="SelectAll" Width="40%">
                                                            </telerik:RadNumericTextBox>
                                                            <asp:RequiredFieldValidator ID="reqGridHours" runat="server" CssClass="stdValidator"
                                                                ControlToValidate="txtGridHours" Display="Dynamic" ErrorMessage="*" 
                                                                ValidationGroup="GRID">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblGridRate" runat="server" SkinID="InfoLabelBold" Text="Hourly Rate:" Visible='<%# ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EMPLOYEE_RATE) %>'>
                                                            </asp:Label>
                                                            <br />
                                                            <telerik:RadNumericTextBox ID="txtGridRate" runat="server" Text='<%# Bind("Hourlyrate") %>'
                                                                AllowOutOfRangeAutoCorrect="true" DataType="System.Currency" NumberFormat-DecimalDigits="2"
                                                                MaxValue="1000.00" MinValue="0.00" SelectionOnFocus="SelectAll" Width="40%" Visible='<%# ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EMPLOYEE_RATE) %>'>
                                                            </telerik:RadNumericTextBox>
                                                            <asp:RequiredFieldValidator ID="reqGridRate" runat="server" CssClass="stdValidator"
                                                                ControlToValidate="txtGridRate" Display="Dynamic" ErrorMessage="*"
                                                                ValidationGroup="GRID">
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblGridTailNumber" runat="server" SkinID="InfoLabelBold" Text="Tail Number:"></asp:Label>
                                                            <asp:Label ID="lblGridTailDescr" runat="server" SkinID="SmallLabel" Text=""></asp:Label>
                                                            <br />
                                                            <telerik:RadTextBox ID="txtGridTailNumber" runat="server" MaxLength="30" AutoPostBack="true"
                                                                OnTextChanged="txtGrid_TailNumber_OnTextChanged" Width="30%" SelectionOnFocus="SelectAll">
                                                            </telerik:RadTextBox>
                                                            <asp:CustomValidator ID="reqGridTailNumber" runat="server" CssClass="stdValidator"
                                                                Enabled="true" ControlToValidate="txtGridTailNumber" Display="Dynamic"
                                                                ErrorMessage="*" OnServerValidate="reqGridTailNumberValidate" ValidateEmptyText="true"
                                                                ValidationGroup="GRID">
                                                            </asp:CustomValidator>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblGridWorkCardNumber" runat="server" SkinID="InfoLabelBold" Text="Work Card Number:"></asp:Label>
                                                            <br />
                                                            <atg:WorkCardSelectorControl ID="cmbGridWorkCardNumber" runat="server" Width="90%"
                                                                Text='<%# Eval("Workcard.Cardnumber") %>' OnInit="cmbGridWorkCardNumber_OnInit"
                                                                OnTextChanged="cmbGridWorkCardNumber_OnTextChanged" ValidationGroup="GRID">
                                                            </atg:WorkCardSelectorControl>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblGridWorkCardDescr0" runat="server" SkinID="InfoLabelBold" Text="Work Card Description:"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblGridWorkCardDescr" runat="server" SkinID="InfoLabel" Visible="false" Text=""></asp:Label>
                                                            <telerik:RadTextBox ID="txtGridWorkCardDescr" runat="server" MaxLength="100" Text='<%# Eval("Workcard.Description") %>'
                                                                SelectionOnFocus="SelectAll" Width="90%" ValidationGroup="GRID"></telerik:RadTextBox>
                                                            <asp:RequiredFieldValidator ID="reqGridWorkCardDescr" runat="server" CssClass="stdValidator"
                                                                Enabled="true" ControlToValidate="txtGridWorkCardDescr" Display="Dynamic" 
                                                                ErrorMessage="*" SetFocusOnError="true" ValidationGroup="GRID">                                        
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblGridWorkCardHourlyRate0" runat="server" SkinID="InfoLabelBold"
                                                                Text="Work Card Hourly Rate:" Visible='<%# ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE) %>'>
                                                            </asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblGridWorkCardHourlyRate" runat="server" SkinID="InfoLabel" Visible="false" Text=""></asp:Label>
                                                            <telerik:RadNumericTextBox ID="txtGridWCHourlyRate" runat="server" Text='<%# Eval("Workcard.Hourlyrate") %>'
                                                                AllowOutOfRangeAutoCorrect="true" DataType="System.Currency" NumberFormat-DecimalDigits="2"
                                                                MaxValue="1000.00" MinValue="0.00" SelectionOnFocus="SelectAll" Width="90%" Visible='<%# ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKCARD_RATE) %>'
                                                                ValidationGroup="GRID">
                                                            </telerik:RadNumericTextBox>
                                                            <asp:RequiredFieldValidator ID="reqGridWorkCardHourlyRate" runat="server" CssClass="stdValidator"
                                                                Enabled="true" ControlToValidate="txtGridWCHourlyRate" Display="Dynamic" 
                                                                ErrorMessage="*" SetFocusOnError="true" ValidationGroup="Grid">                                        
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                        <td valign="middle">
                                                            <asp:Label ID="lblGridWorkCardCompleted" runat="server" SkinID="InfoLabelBold" Text="Work Card Completed?:"></asp:Label>
                                                            <asp:CheckBox ID="chkGridWorkCardCompleted" runat="server" 
                                                                Checked='<%# Eval("Workcard.Completed") == null ? false : Eval("Workcard.Completed") == DBNull.Value ? false : Eval("Workcard.Completed") %>'>
                                                            </asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">
                                                            <asp:Label ID="lblGridNotes" runat="server" SkinID="InfoLabelBold" Text="Notes:"></asp:Label>
                                                            <telerik:RadTextBox ID="txtGridNotes" runat="server" MaxLength="255" SelectionOnFocus="SelectAll"
                                                                Text='<%# Bind(Container.DataItem, "Notes") %>' Width="98%"></telerik:RadTextBox>
                                                        </td>
                                                        <%--<td colspan="1">
                                                            <asp:Label ID="lblGridReconciledStatus" runat="server" SkinID="InfoLabelBold" Text="Reconciled Status:"></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblGridTheReconciledStatus" runat="server" ForeColor='<%# ATGDB.Timesheet.GetReconciledColor( Eval("ReconciledStatus").ToString() ) %>'
                                                                SkinID="InfoLabelBold" Text='<%# ATGDB.Timesheet.GetReconciledDescr( Eval("ReconciledStatus").ToString() ) %>'>
                                                            </asp:Label>
                                                        </td>--%>
                                                    </tr>
                                                </table>
                                            </EditItemTemplate>                                        
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="Timesheetid" HeaderText="Id" 
                                            UniqueName="TimesheetidColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Companydivisionid" HeaderText="Company Id"
                                            UniqueName="CompanydivisionidColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Companydivision.Description" HeaderText="Company" 
                                            UniqueName="CompanydescrColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Customerid" HeaderText="Customer Id" 
                                            UniqueName="CustomeridColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Customer.Description" HeaderText="Customer"
                                            UniqueName="CustomerdescrColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Username" HeaderText="Employee" 
                                            UniqueName="EmployeeColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Extendeduser.Employeenumber" HeaderText="Employee #"
                                             UniqueName="EmployeeNumberColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Jobcodeid" HeaderText="Job Code Id" 
                                            UniqueName="JobcodeidColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Jobcode.Code" HeaderText="Job Code"
                                            UniqueName="JobcodeCodeColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Workcardnumber" HeaderText="Card #"
                                            UniqueName="WorkCardNumberColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Workcarddescription" HeaderText="WC Description"
                                            UniqueName="WorkCardDescColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Workcardhourlyrate" HeaderText="WC Hourly Rate" DataFormatString="{0:f}" 
                                            UniqueName="WorkCardHourlyRateColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Workcardcompleted" HeaderText="WC Completed"
                                            UniqueName="WorkCardCompletedColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Workeddate" HeaderText="Worked Date" DataFormatString="{0:d}"
                                            UniqueName="WorkeddateColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="140px" />
                                            <ItemStyle Width="140px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Starttime" HeaderText="Start Time"
                                            UniqueName="StarttimeColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="140px" />
                                            <ItemStyle Width="140px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Endtime" HeaderText="End Time"
                                            UniqueName="EndtimeColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="140px" />
                                            <ItemStyle Width="140px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Hours" HeaderText="Hours" DataFormatString="{0:f}"
                                            UniqueName="HoursColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Hourlyrate" HeaderText="Hourly Rate" DataFormatString="{0:f}"  
                                            UniqueName="RateColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Notes" HeaderText="Notes"
                                            UniqueName="NotesColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Reconciledstatus" HeaderText="Reconciled Status"
                                            UniqueName="ReconciledstatusColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="UpdUserId" HeaderText="Upd User"
                                            UniqueName="UpdusernameColumn" ReadOnly="false" Visible="false">
                                            <HeaderStyle Width="100px" />
                                            <ItemStyle Width="100px" />
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                </MasterTableView>
                                <ExportSettings OpenInNewWindow="true" ExportOnlyData="true" IgnorePaging="true"
                                    HideStructureColumns="true">
                                    <Excel Format="Html" />
                                </ExportSettings>
                                <ClientSettings>
                                    <%--<ClientEvents OnRowDblClick="RowDblClick" />--%>
                                    <Selecting AllowRowSelect="true" />
                                    <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <br />
                        </telerik:RadAjaxPanel>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</asp:Content>


