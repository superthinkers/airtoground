﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/624/Default.master" AutoEventWireup="true"
    CodeFile="CustomerEquipmentStatusListReport.aspx.cs" Inherits="Customers_624_Reports_CustomerEquipmentStatusListReport"
    Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <br />
    <div class="div_container">
        <div class="div_header">
            Aircraft Status List - Report Options</div>
        <%--<div style="position:relative;left: 35%; margin: 25px 0px 0px 0px; width: 30%">
            <atg:CustomerSelector ID="lstCustomerSelector" runat="server" />
        </div>--%>
        <br />
        <div style="position: relative; left: 40%; width: 20%; text-align: center">
            <asp:Button ID="btnRunReport" runat="server" Text="Run Report" OnClick="btnRunReport_Click" />
        </div>
        <br />
        <telerik:ReportViewer ID="ReportViewer1" runat="server" Height="500px" Width="100%">
        </telerik:ReportViewer>
    </div>
</asp:Content>

