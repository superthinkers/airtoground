﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/624/Default.master" AutoEventWireup="true"
    CodeFile="ServiceRollupReport.aspx.cs" Inherits="Customers_624_Reports_ServiceRollupReport"
    Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <devart:DbLinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Servicecategory" OrderBy="Description" TableName="Servicecategories"
        EnableInsert="true" EnableUpdate="true" EnableDelete="true">
    </devart:DbLinqDataSource>
    <script type="text/javascript">
        function ValidateDateRange(sender, e) {
            var dtFromDate = $find("<%= dtFrom.ClientID %>");
            var dtToDate = $find("<%= dtTo.ClientID %>");

            if ((dtFromDate.get_selectedDate() == null) && (dtToDate.get_selectedDate() != null)) {
                alert("Please select the From/End Date");
            } else if ((dtFromDate.get_selectedDate() > dtToDate.get_selectedDate()) && (dtToDate.get_selectedDate() != null)) {
                alert("The To/Start Date must be less than or equal to than the From/Begin Date");
                dtFromDate.set_selectedDate(dtToDate.get_selectedDate());
            }
            return false;
        }
    </script>
    <br />
    <div class="div_container">
        <div class="div_header">Service Rollup - Report Options</div>
        <%--<div style="position: relative; left: 35%; margin: 25px 0px 0px 0px; width: 30%">
            <atg:CustomerSelector ID="lstCustomerSelector" runat="server" />
        </div>--%>

        <br />
        <div style="position: relative; left: 28%; width: 60%">
            <asp:Label ID="lblServiceCategory" runat="server" SkinID="InfoLabel" Text="Service Category (Required)"></asp:Label>
            <telerik:RadComboBox ID="lstServiceCategories" runat="server" Width="300px" DataSourceID="LinqDataSource2"
                DataTextField="Description" DataValueField="Servicecategoryid">
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" CssClass="stdValidator"
                ErrorMessage="*" ControlToValidate="lstServiceCategories" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>

        <br />
        <div style="position: relative; text-align: center; left: 15%; width: 70%">
            <asp:Label ID="lblFromDate" runat="server" SkinID="InfoLabel" Text="From Date (Required)"></asp:Label>
            <telerik:RadDatePicker ID="dtFrom" runat="server" Width="100px">
                <ClientEvents OnDateSelected="ValidateDateRange" />
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="stdValidator"
                ErrorMessage="*" ControlToValidate="dtFrom" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:Label ID="lblToDate" runat="server" SkinID="InfoLabel" Text="To Date (Required)"></asp:Label>
            <telerik:RadDatePicker ID="dtTo" runat="server" Width="100px">
                <ClientEvents OnDateSelected="ValidateDateRange" />
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="stdValidator"
                ErrorMessage="*" ControlToValidate="dtTo" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>

        <br />
        <div style="position: relative; left: 45%; width: 10%">
            <asp:Label ID="Label1" runat="server" SkinID="InfoLabel" Text="Show Details?"></asp:Label>
            <asp:CheckBox ID="checkDetails" runat="server" Checked="true">
            </asp:CheckBox>
        </div>
        <br />
        <div style="position: relative; left: 40%; width: 20%; text-align: center">
            <asp:Button ID="btnRunReport" runat="server" Text="Run Report" OnClick="btnRunReport_Click" />
        </div>
        <br />
        <telerik:ReportViewer ID="ReportViewer1" runat="server" Height="440px" Width="100%">
        </telerik:ReportViewer>
    </div>
</asp:Content>

