﻿using System;
using System.Linq;
using ATGDB;

public partial class Customers_624_Reports_ServicePerformedReport : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_REPORT_SERVICE_PERFORMED;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            dtFrom.SelectedDate = DateTime.Now.Date.AddDays(-30); // 30 days back
            dtTo.SelectedDate = DateTime.Now.Date; // Today

            // More Selection criteria.
            lstCustServices.DataSource = Service.GetMSorVSServicesByCustomerId(int.Parse(Profile.Customerid));
            lstCustEqTypes.DataSource = Equipmenttype.GetEquipmentTypesByCustomerId(int.Parse(Profile.Customerid));
            lstCustServices.DataBind();
            lstCustEqTypes.DataBind();
        }
    }
    protected void btnRunReport_Click(object sender, EventArgs e) {
        // Run the report.
        Telerik.Reporting.InstanceReportSource rs = new Telerik.Reporting.InstanceReportSource();
        rs.ReportDocument = new ServicePerformed(int.Parse(Profile.Customerid),
            Profile.Customerdescription, 
            Profile.UserName,
            dtFrom.SelectedDate.Value.Date,
            dtTo.SelectedDate.Value.Date,
            btnSort1.Checked, btnSort2.Checked, lstCustEqTypes, lstCustServices);
        ReportViewer1.ReportSource = rs;
    }
}                                        