﻿using System;

public partial class Customers_624_Reports_ReportsHome : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_REPORT_HOME;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            // Security setup.
            // Revenue Reports
            divRevenue.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_REVENUE);
            lblRevenueSecurity.Visible = !divRevenue.Visible;
            // Work Reports
            divWork.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_WORK);
            lblWorkSecurity.Visible = !divWork.Visible;
            // Equipment Reports
            divEquipment.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_EQUIP);
            lblEquipmentSecurity.Visible = !divEquipment.Visible;
            // Job Reports
            divJob.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_JOB);
            lblJobSecurity.Visible = !divJob.Visible;
        }
    }
}