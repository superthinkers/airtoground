﻿using System;
using System.Linq;
using ATGDB;

public partial class Customers_624_Schedules_ExceptionDetailsView : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_EX_DETAILS;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);

        // Update RadGrid.ImagePaths
        PageUtils.SetRadGridImagePathAllControls(this, Page.Theme);

        if (!Page.IsPostBack) {
            if (Request.QueryString["ssId"] != null) {

                // Header summary
                ATGDB.Serviceschedule ss = Serviceschedule.GetServiceSchedule(int.Parse(Request.QueryString["ssId"].ToString()));
                lblCustomer.Text = ss.Service.Customer.Description;
                lblTailNumber.Text = ss.Equipment.Tailnumber;
                lblServiceDescr.Text = ss.Service.Description;
                lblServiceDate.Text = ss.Servicedate.ToShortDateString();

                // Load up any outstanding exceptions.
                var data = Serviceexception.GetServiceexceptionsByServiceScheduleId(int.Parse(Request.QueryString["ssId"].ToString()));
                if (data.Count() > 0) {
                    RadGrid1.DataSource = data;
                    RadGrid1.DataBind();
                    if (RadGrid1.Items.Count > 0) {
                        RadGrid1.Items[0].Selected = true;
                        RadGrid1_SelectedIndexChanged(null, null);
                    }
                }
            }
        }
    }
    protected void RadGrid1_SelectedIndexChanged(object sender, EventArgs e) {
        Int32 exId = Convert.ToInt32(RadGrid1.SelectedValue);

        // Get the exception.
        ATGDataContext db = new ATGDataContext();
        ATGDB.Serviceexception ex = db.Serviceexceptions.Single(x => x.Serviceexceptionid == exId);

        // Set the notes fields.
        txtExNotes.Text = ex.Exceptionnotes;
        txtCompletedNotes.Text = ex.Completednotes;
        txtClosedNotes.Text = ex.Closednotes;
    }
}