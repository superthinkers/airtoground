﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/Customers/624/Default.master"
    AutoEventWireup="true" CodeFile="ViewSchedules.aspx.cs" Inherits="Customers_624_Schedules_ViewSchedules" Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetServiceSchedulesByDatesAndEquipOrServiceType"
        TypeName="ATGDB.Serviceschedule" EnableCaching="false">
        <SelectParameters>
            <asp:Parameter Name="customerId" Type="Int32" />
            <asp:Parameter Name="equipmentId" Type="Int32" />
            <asp:Parameter Name="dtFromDate" Type="DateTime" />
            <asp:Parameter Name="dtToDate" Type="DateTime" />
            <asp:Parameter Name="futureOption" Type="Boolean" />
            <asp:Parameter Name="services" Type="String" DefaultValue="" />
            <asp:Parameter Name="eqTypes" Type="String" DefaultValue="" />
            <asp:Parameter Name="masterScheduleMode" Type="Boolean" DefaultValue="false" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="onRequestStart"/>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnExportToExcel">
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ctrlServiceExDescr">
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" ReloadOnShow="false" runat="server" EnableShadow="true" Modal="true"
        VisibleTitlebar="false" EnableViewState="false" AutoSize="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow3" runat="server" NavigateUrl="ExceptionDetailsView.aspx"
                ShowContentDuringLoad="false" VisibleStatusbar="false" Title="Exception Details">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow999" runat="server" ShowContentDuringLoad="false" VisibleStatusbar="false" Title="">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>    
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="true"
        Skin="Office2007">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            /*
                For resizing to work:

                Pase in the two script functions. Modify the width / height settings as 
                necessary for offset1 and offset2.
                
                Then Set:

                RadGrid1.Height = null
                    MasterTableView.Height = null

                RadAjaxManager.OnResponseEnd=DoGridResize

                Then Name the div_container "resizeDiv"

                Then Name the search criteria, or other "header block" to "headerDiv".
            */
            //window.onload = function () {
                //DoGridResize();
            //}
            //window.onresize = function () {
                //DoGridResize();
            //}
            function DoGridResize(sender, eventArgs) {
                // Handles to the various div tags.
                //var divMain = document.getElementById("div");
                var divResize = document.getElementById("resizeDiv");
                var divSiteHeader = document.getElementById("divSiteHeader");
                var divSiteMenu = document.getElementById("divSiteMenu");
                var divSiteFooter = document.getElementById("divSiteFooter");
                var divPageHeader = document.getElementById("headerDiv");

                // Calc the offsite height.
                var offset1 = divSiteHeader.clientHeight + divSiteMenu.clientHeight + 5.60 * divSiteFooter.clientHeight + divPageHeader.clientHeight;
                var offset2 = divSiteHeader.clientHeight + divSiteMenu.clientHeight + 1.75 * divSiteFooter.clientHeight + divPageHeader.clientHeight;

                // Get page window height.
                var windowHeight = window.innerHeight; // || document.documentElement.clientHeight;
                var windowWidth = window.innerWidth - 100;

                // Resize main div.
                //divMain.style.height = windowHeight + 100 + "px";

                // Resize the target div tag.
                divResize.style.height = windowHeight - offset2 + "px";
                divResize.style.width = windowWidth + "px";

                // Reset grid height.
                //var grid = document.getElementById("< %= RadGrid1.ClientID %>");
                var grid = $find("<%= RadGrid1.ClientID %>");
                if (grid) {
                    grid.get_element().style.height = windowHeight - offset1 + "px";
                    grid.repaint();
                    if (grid.MasterTableView) {
                        grid.MasterTableView.get_element().clientHeight = windowHeight - offset1 + "px";
                        grid.MasterTableView.get_element().clientWidth = windowWidth + "px";
                    }
                }
            }
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("btnExportToExcel") >= 0 ||
                    args.get_eventTarget().indexOf("ctrlServiceExDescr") >= 0) {
                    args.set_enableAjax(false);
                }
            }
            function openWin3(ssId, hasEx) {
                if ((ssId > 0) && (hasEx == "1")) {
                    radopen("ExceptionDetailsView.aspx?ssId=" + ssId, "RadWindow3");
                } else {
                    alert("No Exceptions");
                }
            }
            function ValidateDateRange(sender, e) {
                var dtFromDate = $find("<%= dtFrom.ClientID %>");
                var dtToDate = $find("<%= dtTo.ClientID %>");
  
                if ((dtFromDate.get_selectedDate() == null) && (dtToDate.get_selectedDate() != null)) {
                    alert("Please select the From/End Date");
                } else if ((dtFromDate.get_selectedDate() > dtToDate.get_selectedDate()) && (dtToDate.get_selectedDate() != null)) {
                    alert("The To/Start Date must be less than or equal to than the From/Begin Date");
                    dtFromDate.set_selectedDate(dtToDate.get_selectedDate());
                }
                return false;
            }
            function clickButton(e) {
                if (e.keyCode == 13) {
                    <%= SearchPostBackString %>
                    return false;
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <asp:HiddenField ID="hiddenPageKey" runat="server" Value="" />
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Tail Service Schedule Browser"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div id="headerDiv" class="div_header">Search / Browse Tail Service Schedules</div>
            <div style="position: relative; margin: 4px; width: 99%;">
                <div style="position: relative; float: left; width: 70%">
                    <fieldset>
                        <legend>Search Criteria</legend>
                        <table width="100%">
                            <tr>
                                <td style="width: 30%">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 50%">
                                                <div class="lblInfo">
                                                    From Date<br />
                                                    <telerik:RadDatePicker ID="dtFrom" runat="server" Font-Names="Arial" MinDate="1/1/0001 12:00:00 AM"
                                                        Width="98%">
                                                        <ClientEvents OnDateSelected="ValidateDateRange" />
                                                    </telerik:RadDatePicker>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="stdValidator"
                                                        ErrorMessage="<br />Required" ControlToValidate="dtFrom"
                                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                            </td>
                                            <td style="width: 50%">
                                                <div class="lblInfo">
                                                    To Date (opt)<br />
                                                    <telerik:RadDatePicker ID="dtTo" runat="server" Font-Names="Arial" MinDate="1/1/0001 12:00:00 AM"
                                                        Width="98%">
                                                        <ClientEvents OnDateSelected="ValidateDateRange" />
                                                    </telerik:RadDatePicker>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 15%">
                                <td rowspan="3" style="width: 35%">
                                    <!-- Services -->
                                    <div>
                                        <asp:Label ID="lblHeader2" runat="server" SkinID="InfoLabel" Text="Services"></asp:Label>
                                        <asp:Panel ID="pnlScroll2" runat="server" BorderWidth="0" ScrollBars="Auto" Width="100%" Height="101px">
                                            <telerik:RadListBox ID="lstCustServices" runat="server" DataKeyField="Serviceid"
                                                DataValueField="Serviceid" DataTextField="Description" CheckBoxes="true" SelectionMode="Multiple"
                                                BorderWidth="0" BorderColor="Transparent" Width="98%">
                                            </telerik:RadListBox>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%">
                                    <div class="lblInfo">
                                        Tail Number (required)</div>
                                    <asp:TextBox ID="txtTailNumber" runat="server" Width="100px" AutoCompleteType="None" onkeypress="return clickButton(event)"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqTailNumber" runat="server" CssClass="stdValidator"
                                        ErrorMessage="<br />Required" ControlToValidate="txtTailNumber"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 15%">
                                    <asp:CheckBox ID="chkFuture" runat="server" Font-Size="10px" Text="Future Items?"
                                        Checked="false" Visible="true" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" style="width: 40%">
                                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_OnClick" Text="Search"/>
                                    &nbsp;&nbsp;<asp:Button ID="btnExportToExcel" runat="server" OnClick="btnExportToExcel_OnClick"
                                        Text="Export to Excel" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="position: relative; float: left; width: 30%">
                    <fieldset>
                        <legend>Legend</legend>
                        <div>
                            <label class="legendVSCompleted">
                                COMPLETED ITEMS /
                            </label>
                            <label class="legendVSCancelled">
                                CANCELLED ITEMS</label>
                            <br />
                            <label class="legendVSCompletedEx">
                                COMPLETED ITEMS W/OPEN EXCEPTION</label>
                            <br />
                            <label class="legendVSScheduled">
                                SCHEDULED ITEMS</label>
                            <br />
                            <label class="legendVSScheduled2">
                                NON-VERIFIED SCHEDULED ITEMS</label>
                            <br />
                            <label class="legendVSNonScheduled">
                                NON-SCHEDULED ITEMS</label>
                            <br />
                            <label class="legendVSFuture">
                                NON-SCHEDULED, FUTURE ITEMS</label>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="div_container">
            <telerik:RadGrid ID="RadGrid1" runat="server" HorizontalAlign="NotSet" AutoGenerateColumns="false"
                OnDataBinding="RadGrid1_DataBinding" Width="100%" OnItemDataBound="RadGrid1_ItemDataBound">
                <MasterTableView Name="MainView" runat="server" Width="100%" AutoGenerateColumns="false" RowIndicatorColumn-Display="false"
                    GridLines="Both" CellPadding="0" CellSpacing="0" RowIndicatorColumn-Visible="false"
                    ShowHeadersWhenNoRecords="true" DataKeyNames="Servicescheduleid" TableLayout="Fixed"
                    Font-Names="Arial" Font-Size="10px" HorizontalAlign="Left" AllowPaging="false"
                    CommandItemDisplay="None" CommandItemSettings-ShowAddNewRecordButton="false" CommandItemSettings-ShowRefreshButton="false"
                    EditMode="InPlace" AllowSorting="true" AllowFilteringByColumn="true">
                    <Columns>
                        <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="EX1Column">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <a href="#" onclick='<%# "openWin2(" + Eval("Servicescheduleid").ToString() + "," + (Convert.ToBoolean(Eval("Hasactiveexception")) == true ? "1" : "0") + "); return false;" %>'>
                                    <asp:Image ID="imgEx1" runat="server" SkinID="Exception" />
                                </a>
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle Width="30px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="Hadexception" AllowFiltering="false" UniqueName="EX2Column">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <div id="divEx2" runat="server" visible='<%# GetEX2Visible() %>'>
                                    <a href="#" onclick='<%# "openWin3(" + Eval("Servicescheduleid").ToString() + "," + (Convert.ToBoolean(Eval("Hadexception")) == true ? "1" : "0") + "); return false;" %>'>
                                        <asp:Image ID="imgEx2" runat="server" SkinID="Exception" />
                                    </a>
                                </div>
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                            <ItemStyle Width="30px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Servicescheduleid" UniqueName="ServicescheduleidColumn"
                            HeaderText="Id" ReadOnly="true" Visible="false" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Equipmentid" UniqueName="EquipmentidColumn" HeaderText="EqId"
                            ReadOnly="true" Visible="false" AllowSorting="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Serviceid" UniqueName="ServicesidColumn" HeaderText="Service Id"
                            ReadOnly="true" Visible="false" AllowFiltering="false" AllowSorting="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Tail" DataField="Tailnumber" UniqueName="TailnumberColumn"
                            ReadOnly="true" AllowSorting="true" AllowFiltering="false">
                            <HeaderStyle Width="40px" HorizontalAlign="Center" />
                            <ItemStyle Width="40px" HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Equipmenttypedescr" UniqueName="EquipmentdescrColumn"
                            HeaderText="Type" ReadOnly="true" AllowSorting="true" AllowFiltering="false">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Servicedescr" UniqueName="ServicedescrColumn1"
                            HeaderText="Service Description" ReadOnly="true" AllowSorting="true" AllowFiltering="false"
                            Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="Servicedescr" UniqueName="ServicedescrColumn2"
                            HeaderText="Service Description" ReadOnly="true" SortExpression="Servicedescr"
                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                            FilterControlWidth="98%">
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <atg:ServiceExDescr ID="ctrlServiceExDescr" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle Width="250px" HorizontalAlign="Left" />
                            <ItemStyle Width="250px" HorizontalAlign="Left" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Location" UniqueName="LocationColumn" HeaderText="Location"
                            ReadOnly="true" SortExpression="Location" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                            AutoPostBackOnFilter="true">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Interval" UniqueName="IntervalColumn" HeaderText="Interval"
                            ReadOnly="true" AllowFiltering="true" SortExpression="Interval" CurrentFilterFunction="Contains"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" FilterControlWidth="98%">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn DataField="Servicedate" UniqueName="ServicedateColumn"
                            DataFormatString="{0:d}" HeaderText="Service Date" ReadOnly="true" MinDate="1/1/0001 12:00:00 AM"
                            ColumnEditorID="ServicedateColumnEditor" AllowSorting="true" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" FilterControlWidth="98%">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn DataField="Servicetime" UniqueName="ServicetimeColumn"
                            ColumnEditorID="ServicetimeColumnEditor" DataFormatString="{0:t}" PickerType="TimePicker"
                            HeaderText="Service Time" ReadOnly="true" MinDate="1/1/0001 12:00:00 AM" AllowFiltering="false"
                            AllowSorting="false">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridDateTimeColumn DataField="Completeddate" UniqueName="CompleteddateColumn"
                            DataFormatString="{0:d}" HeaderText="Compl Date" ReadOnly="true" MinDate="1/1/0001 12:00:00 AM"
                            ColumnEditorID="CompleteddateColumnEditor" AllowSorting="true" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" FilterControlWidth="98%">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn DataField="Comments" UniqueName="CommentsColumn" HeaderText="Notes"
                            ReadOnly="true" MaxLength="255" AllowFiltering="false" AllowSorting="false">
                            <HeaderStyle Width="200px" HorizontalAlign="Left" />
                            <ItemStyle Width="200px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Status" UniqueName="StatusColumn" ReadOnly="true"
                            Visible="false" AllowFiltering="false">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="OnHold" UniqueName="OnHoldColumn" ReadOnly="true"
                            Visible="true" AllowFiltering="false">
                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                    </Columns>
                    <NoRecordsTemplate>
                        <span class="lblInfo">No matching Aircraft. Change your Search Criteria, or Filters, and try again.</span>
                    </NoRecordsTemplate>
                </MasterTableView>
                <ExportSettings FileName="Schedule" OpenInNewWindow="true" ExportOnlyData="true" IgnorePaging="true"
                    HideStructureColumns="true">
                    <Excel Format="Html" />
                </ExportSettings>
                <ClientSettings>
                    <Selecting AllowRowSelect="true" />
                    <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                </ClientSettings>
            </telerik:RadGrid>
        </div>
    </div>
</asp:Content>
