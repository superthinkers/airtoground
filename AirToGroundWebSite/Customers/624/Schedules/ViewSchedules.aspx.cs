﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using ATGDB;

public partial class Customers_624_Schedules_ViewSchedules : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_TAIL_SCHEDULE;
    }
    // For postbacks in the JavaScript in the page.
    protected string PostBackString;
    protected string SearchPostBackString;

    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e) {
        // For postbacks in the JavaScript in the page.
        PostBackString = Page.ClientScript.GetPostBackEventReference(this, "");
        SearchPostBackString = Page.ClientScript.GetPostBackEventReference(btnSearch, "OnClick");

        if (!Page.IsPostBack) {
            // Clear the cache.
            hiddenPageKey.Value = new Random().Next(10000000, 999999999).ToString();
            System.Web.HttpContext.Current.Session.Add(Serviceschedule.VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey, "");

            // Security.
            if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_SCH)) {
                btnExportToExcel.Visible = false;
            }

            // UI defaults.
            // Date boxes.
            dtFrom.SelectedDate = DateTime.Now;
            dtTo.SelectedDate = DateTime.Now.AddDays(7);
            
            // More Selection criteria.
            lstCustServices.DataSource = Service.GetMSorVSServicesByCustomerId(int.Parse(Profile.Customerid));//lstCustomerSelector.SelectedCustomerId));
            lstCustServices.DataBind();

            // Mode = 1 : Date was clicked, parm=dt
            //            Return all tails, specific date.
            // Mode = 2 : Tail Number was clicked, parm=tail,dtFrom,dtTo
            //            Return all schedules given date range, specific tail.
            // Mode = 3 : A cell was clicked (Tail/Date combination), parm=dt,tail
            //            Return all schedules, specific tail, specific date
            if ((Request.QueryString["Mode"] != null) && (Request.QueryString["Mode"].ToString() != "")) {
                // Get the mode.
                string mode = Request.QueryString["Mode"].ToString();

                // Customer passed for all modes.
                if (mode == "1") {
                    string dt = Request.QueryString["dt"].ToString();
                    dtFrom.SelectedDate = Convert.ToDateTime(dt);
                    dtTo.SelectedDate = Convert.ToDateTime(dt);
                    txtTailNumber.Text = "0";
                } else if (mode == "2") {
                    string dt1 = Request.QueryString["dtFrom"].ToString();
                    string dt2 = Request.QueryString["dtTo"].ToString();
                    string tail = Request.QueryString["tail"].ToString();
                    dtFrom.SelectedDate = Convert.ToDateTime(dt1);
                    dtTo.SelectedDate = Convert.ToDateTime(dt2);
                    txtTailNumber.Text = tail;
                } else if (mode == "3") {
                    string dt = Request.QueryString["dt"].ToString();
                    string tail = Request.QueryString["tail"].ToString();
                    dtFrom.SelectedDate = Convert.ToDateTime(dt);
                    dtTo.SelectedDate = Convert.ToDateTime(dt);
                    txtTailNumber.Text = tail;
                }

                string serviceParms = Request.QueryString["services"] == null ? "" : Request.QueryString["services"].ToString();
                SetChecksInListBox(lstCustServices, "Serviceid IN (", serviceParms);

                // Load it up.
                btnSearch_OnClick(null, null);

            }
        } else {
            ScheduleHelper.Instance.ViewPageKey = hiddenPageKey.Value;
            if ((Session["REFRESH_VIEW_SCHEDULES"] != null) && (Session["REFRESH_VIEW_SCHEDULES"].ToString() != "")) {
                // Clear the refresh flag first.
                Session.Add("REFRESH_VIEW_SCHEDULES", "");
                // Rebind the grid.
                btnSearch_OnClick(null, null);
            }
        }
    }
    protected void SetChecksInListBox(RadListBox lst, string sLeaderText, string sText) {
        if (sText != "") {
            string vals = sText.Substring(sLeaderText.Length, sText.Length - sLeaderText.Length - 1);
            StringTokenizer tokens = new StringTokenizer(vals, ", ");
            foreach (string token in tokens) {
                RadListBoxItem item = lst.FindItemByValue(token);
                if (item != null) {
                    item.Checked = true;
                }
            }
        }
    }
    protected void SetParams() {
        // Set the rest of the parameters.
        ObjectDataSource1.SelectParameters["customerId"].DefaultValue = Profile.Customerid;// lstCustomerSelector.SelectedCustomerId;
        ObjectDataSource1.SelectParameters["equipmentId"].DefaultValue = 
            Equipment.GetEquipmentIdByTailNumber(int.Parse(Profile.Customerid/*lstCustomerSelector.SelectedCustomerId*/), txtTailNumber.Text).ToString();
        ObjectDataSource1.SelectParameters["dtFromDate"].DefaultValue = dtFrom.SelectedDate.Value.ToShortDateString();
        if (dtTo.SelectedDate == null) {
            ObjectDataSource1.SelectParameters["dtToDate"].DefaultValue = dtFrom.SelectedDate.Value.ToShortDateString();
        } else {
            ObjectDataSource1.SelectParameters["dtToDate"].DefaultValue = dtTo.SelectedDate.Value.ToShortDateString();
        }
        ObjectDataSource1.SelectParameters["futureOption"].DefaultValue = chkFuture.Checked.ToString();
        ObjectDataSource1.SelectParameters["services"].DefaultValue = Serviceschedule.GetServicesDataViewFilter(lstCustServices);
        ObjectDataSource1.SelectParameters["eqTypes"].DefaultValue = "";// Serviceschedule.GetEqTypesDataViewFilter(lstCustEqTypes);
    }
    protected void RadGrid1_DataBinding(object sender, EventArgs e) {
        SetParams();
    }
    protected void btnSearch_OnClick(object sender, EventArgs e) {
        if (dtTo.SelectedDate == null) {
            dtTo.SelectedDate = dtFrom.SelectedDate;
        }
        if ((txtTailNumber.Text != "") && (txtTailNumber.Text != "0") && (txtTailNumber.Text.IndexOf("-") == -1) &&
            (Equipment.GetEquipmentIdByTailNumber(int.Parse(Profile.Customerid/*lstCustomerSelector.SelectedCustomerId*/), txtTailNumber.Text) != -1)) {
            // Validation
            reqTailNumber.IsValid = true;
            reqTailNumber.ErrorMessage = "Please Enter a Tail Number";
            // Datasource
            RadGrid1.DataSourceID = "ObjectDataSource1";
            // Show Grid
            RadGrid1.Visible = true;
            // Clear cache.
            ScheduleHelper.Instance.ViewPageKey = hiddenPageKey.Value;
            System.Web.HttpContext.Current.Session.Add(Serviceschedule.VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey, "");
            RadGrid1.DataBind();
        } else {
            // Validation
            reqTailNumber.IsValid = false;
            reqTailNumber.ErrorMessage = "Invalid Tail Number";
            // Hide Grid
            RadGrid1.Visible = false;
        }
    }
    // For coloring the rows.
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e) {
        if (e.Item is GridDataItem) {
            if (!e.Item.IsInEditMode) {
                GridDataItem row = e.Item as GridDataItem;
                var dataRow = (e.Item.DataItem as System.Data.DataRowView).Row;

                // COLOR THE ROWS, HIDE THE DELETE BUTTON.
                TableCell cell = row["StatusColumn"];
                TableCell holdCell = row["OnHoldColumn"];

                if (holdCell.Text.ToUpper() == "TRUE") {
                    // Readonly mode - ON HOLD.
                    holdCell.Text = "On Hold";
                    cell = row["EX1Column"];
                    cell.Controls.Clear();
                } else {
                    holdCell.Text = "";
                }

                if (dataRow[23].ToString().ToUpper() == "BLUE") {
                    // NON-SCHEDULED - NEXT ONE
                    row.BackColor = ColorUtils.GetSS_NonScheduledColor(Profile.SiteTheme);
                    cell = row["EX1Column"];
                    cell.Controls.Clear();
                } else if (dataRow[23].ToString().ToUpper() == "GRAY") {
                    // NON-SCHEDULE - FUTURE CAST
                    row.BackColor = ColorUtils.GetSS_FutureColor(Profile.SiteTheme);
                    // Can't edit or Delete.
                    cell = row["EX1Column"];
                    cell.Controls.Clear();
                } else if (dataRow[23].ToString().ToUpper() == "RED") {
                    // COMPLETED, ZONE EXCEPTION
                    row.BackColor = ColorUtils.GetSS_CompletedExColor(Profile.SiteTheme);
                } else if (dataRow[23].ToString().ToUpper() == "GREEN") {
                    // SCHEDULED
                    row.BackColor = ColorUtils.GetSS_ScheduledColor(Profile.SiteTheme);
                    cell = row["EX1Column"];
                    cell.Controls.Clear();
                } else if (dataRow[23].ToString().ToUpper() == "YELLOW1") {
                    // Completed Schedules - NON EDITABLE - YELLOW
                    cell = row["EX1Column"];
                    cell.Controls.Clear();
                } else if (dataRow[23].ToString().ToUpper() == "YELLOW2") {
                    // Canceled Schedules - NON EDITABLE - YELLOW
                    row.BackColor = ColorUtils.GetSS_CompletedColor(Profile.SiteTheme);
                    // Strike out columns.
                    TableCell strikeCell = row["TailnumberColumn"];
                    strikeCell.Font.Strikeout = true;
                    //strikeCell = row["CustomerNameColumn"];
                    //strikeCell.Font.Strikeout = true;
                    strikeCell = row["EquipmentdescrColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["ServicedescrColumn2"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["LocationColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["IntervalColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["ServicedateColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["ServicetimeColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["CompleteddateColumn"];
                    strikeCell.Font.Strikeout = true;

                    cell = row["EX1Column"];
                    cell.Controls.Clear();
                }                
            }
        }
    }
    protected bool GetEX2Visible() {
        if ((Convert.ToBoolean(Eval("Hasactiveexception")) == false) && 
            (Convert.ToBoolean(Eval("Hadexception")) == true)) {
            return true;
        } else {
            return false;
        }
    }
    protected void btnExportToExcel_OnClick(object sender, EventArgs e) {
        if (RadGrid1.MasterTableView.Items.Count > 0) {
            RadGrid1.MasterTableView.Columns.FindByUniqueName("EX1Column").Display = false;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("EX2Column").Display = false;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ServicedescrColumn1").Display = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ServicedescrColumn1").Visible = true;
            RadGrid1.MasterTableView.Columns.FindByUniqueName("ServicedescrColumn2").Display = false;
            RadGrid1.Rebind();
            RadGrid1.MasterTableView.ExportToExcel();
        }
    }
  
}