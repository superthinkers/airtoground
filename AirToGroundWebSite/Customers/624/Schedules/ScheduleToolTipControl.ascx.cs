﻿using System;
using System.Data;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using ATGDB;

public partial class Customers_624_ScheduleToolTipControl : System.Web.UI.UserControl
{
    public string Data {
        get {
            if (ViewState["Data"] == null) {
                return "";
            }
            return (string)ViewState["Data"];
        }
        set {
            if (this.Data != value) {

            }
            ViewState["Data"] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Data != null) {
            string tail = "";
            string dt = "";
            string hiddenPageKey = "";
            // Get the keys to filter with.
            StringTokenizer st = new StringTokenizer(Data, ":");
            int cnt = 0;
            string val = "";
            while (st.hasMoreTokens() == true) {
                cnt++;
                val = st.nextToken();
                if (cnt == 1) {
                    tail = val;
                } else if (cnt == 2) {
                    dt = val;
                } else if (cnt == 3) {
                    hiddenPageKey = val;
                }
            }
            // DataView from Cache should always be there.
            ScheduleHelper.Instance.MasterPageKey = hiddenPageKey;
            ScheduleHelper.Instance.ViewPageKey = hiddenPageKey;
            DataView data = (DataView)System.Web.HttpContext.Current.Session[Serviceschedule.VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey];
            if (data != null) {
                // Get a new view, and filter it.
                DataView filteredData = data.Table.AsDataView();
                if (dt != "Tail") {
                    // User hovered inside cell.
                    filteredData.RowFilter = "(Tailnumber = '" + tail + "') and (Servicedate = '" + dt + "')";
                } else {
                    // User hovered tail number cell.
                    filteredData.RowFilter = "Tailnumber = '" + tail + "'";
                }
                filteredData.Sort = "Servicedescr, Servicedate";
                filteredData.ApplyDefaultSort = true;
                RadGridScheduleToolTip.DataSource = filteredData;
                //RadGridScheduleToolTip.MasterTableView.DataSource = filteredData.Table;
                RadGridScheduleToolTip.DataBind();
                //RadGridScheduleToolTip.MasterTableView.DataBind();
            }
        }
    }                          
    // For coloring the rows.
    protected void RadGridScheduleToolTip_ItemDataBound(object sender, GridItemEventArgs e) {
        if (e.Item is GridDataItem) {
            if (!e.Item.IsInEditMode) {
                GridDataItem row = e.Item as GridDataItem;
                var dataRow = (e.Item.DataItem as System.Data.DataRowView).Row;
                // COLOR THE ROWS.
                //TableCell cell = row["StatusColumn"];
                if (dataRow[22].ToString().ToUpper() == "BLUE") {
                    // NON-SCHEDULED - NEXT ONE
                    row.BackColor = ColorUtils.GetSS_NonScheduledColor(Profile.SiteTheme);
                } else if (dataRow[22].ToString().ToUpper() == "GRAY") {
                    // NON-SCHEDULE - FUTURE CAST
                    row.BackColor = ColorUtils.GetSS_FutureColor(Profile.SiteTheme);
                } else if (dataRow[22].ToString().ToUpper() == "RED") {
                    // COMPLETED, ZONE EXCEPTION
                    row.BackColor = ColorUtils.GetSS_CompletedExColor(Profile.SiteTheme);
                } else if (dataRow[22].ToString().ToUpper() == "GREEN") {
                    // SCHEDULED
                    row.BackColor = ColorUtils.GetSS_ScheduledColor(Profile.SiteTheme);
                } else if (dataRow[22].ToString().ToUpper() == "YELLOW1") {
                    // Completed Schedules - NON EDITABLE - YELLOW
                    row.BackColor = ColorUtils.GetSS_CompletedColor(Profile.SiteTheme);
                } else if (dataRow[22].ToString().ToUpper() == "YELLOW2") {
                    // Canceled Schedules - NON EDITABLE - YELLOW
                    row.BackColor = ColorUtils.GetSS_CompletedColor(Profile.SiteTheme);
                    // Strike out columns.
                    //row.Font.Strikeout = true;
                    TableCell strikeCell = row["TailnumberColumn"];
                    strikeCell.Font.Strikeout = true;
                    //strikeCell = row["CustomerNameColumn"];
                    //strikeCell.Font.Strikeout = true;
                    strikeCell = row["ServicedescrColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["IntervalColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["ServicedateColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["ServicetimeColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["CompletedtimeColumn"];
                    strikeCell.Font.Strikeout = true;
                    strikeCell = row["CompleteddateColumn"];
                    strikeCell.Font.Strikeout = true;
                }
            }
        }
    }

}