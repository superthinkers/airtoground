﻿using System;
using System.Collections.Generic;
using System.Linq;

public partial class Customers_624_Home : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_HOME;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            // Default the summary range.
            if (Profile.DefaultSummaryRange.ToString() == "") {
                Profile.DefaultSummaryRange = "30";
                Profile.Save();
            }
            // Security setup.
            // Revenue Reports
            //tblRevenue.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_REVENUE) || 
            //  ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN);
            // Work Reports
            tblWork.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_WORK) ||
                ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN);
            // Equipment Reports
            tblEquipment.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_EQUIP) ||
                ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN);

            // Set up the drop down list.
            lstRange.SelectedItem.Selected = false;
            lstRange.Items.FindByValue(Profile.DefaultSummaryRange).Selected = true;

            // Load Queue Summary
            LoadQueueSummary();
        }
    }

    public class ServiceQueueSummary
    {
        public string Description { get; set; }
        public int Count { get; set; }
        public ServiceQueueSummary(string descr, int count) {
            Description = descr;
            Count = count;
        }
    }
    protected void LoadQueueSummary() {
        
        // Get today's queue for the logged in user.
        string keys = Profile.Companydivisionid + ":" + Profile.Customerid;
        var data = ATGDB.Servicequeue.GetServiceQueueItemsByCompCustAndDate(keys, DateTime.Now.Date);

        // Build a summary dataset.
        List<ServiceQueueSummary> list = new List<ServiceQueueSummary>();

        int cnt = (from x in data
                   where x.Isdone == true
                   where x.Closeddate != null
                   select x).Count();
        list.Add(new ServiceQueueSummary("Done and Closed", cnt));

        cnt = (from x in data
               where x.Isdone == false
               where x.Closeddate != null
               select x).Count();
        list.Add(new ServiceQueueSummary("Not Done, Closed", cnt));

        cnt = (from x in data
               where x.Isdone == false
               where x.Closeddate == null
               select x).Count();
        list.Add(new ServiceQueueSummary("Not Done, Not Closed", cnt));

        lstQueueSummary.DataSource = list;
        lstQueueSummary.DataBind();
    }
    protected System.Drawing.Color GetColor() {
        if (Eval("Scheduled").ToString() == "True") {
            return System.Drawing.Color.MediumSpringGreen;
        } else {
            return System.Drawing.Color.LightBlue;
        }
    }
    protected void btnRefreshSummary_Click(object sender, EventArgs e) {
        // Save the selection to the profile.
        Profile.DefaultSummaryRange = lstRange.SelectedValue.ToString();
        Profile.Save();
        // Rebind.
        ObjectDataSource1.DataBind();
        lstSummary.DataBind();
    }
}
