﻿using System;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Customers_624_Security_FirstLoginSecurityQuestion : System.Web.UI.Page
{
    //protected void Page_PreInit(object sender, EventArgs e) {
    //    Page.Theme = Profile.SiteTheme;
    //}
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            MembershipUser u = ProviderUtils.GetUser(Profile.UserName);
            lstQuestions.SelectedValue = u.PasswordQuestion;
        }
    }
    protected void reqCustPassword_ServerValidate(object sender, ServerValidateEventArgs e) {
        e.IsValid = true;
        if (txtPassword.Text != txtConfirmPassword.Text) {
            e.IsValid = false;    
        }
    }
    protected void btnOk_Click(object sender, EventArgs e) {
        try {
            lblError.Text = "";
            if (ProviderUtils.ChangePassword(Profile.UserName, txtOldPassword.Text, txtPassword.Text)) {
                if (ProviderUtils.ChangePasswordQuestionAndAnswer(Profile.UserName, txtPassword.Text, lstQuestions.Text, txtAnswer.Text)) {
                    // Go Home.
                    // Users must have a default division and customer.
                    Response.Redirect("~/Home.aspx", false);
                    return;
                } else {
                    lblError.Text = "Error Changing Question and Answer. Contact the System Administrator.";
                }
            } else {
                lblError.Text = "Old Password Invalid. New Passwords Require 7 letters, Mixed case, and One Symbol Such as !$*^#.";
            }
        } catch {
            lblError.Text = "Password Invalid";
        }
    }
}