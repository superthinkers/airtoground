﻿using System;
using System.Linq;
using System.Web;
using System.Web.Security;

public partial class Customers_624_Security_SessionTimeout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e) {
        // Kill the back button and browser cache.
        Response.Buffer = true;
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
        Response.Expires = -1;
        Response.CacheControl = "no-cache";
        Response.AddHeader("Pragma", "no-cache");
 
        if (!Page.IsPostBack) {
            // Footer.
            txtFooter.Text = Resources.Resource.WebSiteFooterText;
            // Sign out.
            FormsAuthentication.SignOut();
            // Kill the session.
            HttpContext.Current.Session.Abandon();
            // Clear the session vars for security.
            HttpContext.Current.Session.Clear();
        }
    }
    protected void lnkLogin_OnClick(object sender, EventArgs e) {
        // Redirect to the login page.
        FormsAuthentication.RedirectToLoginPage();
    }
}