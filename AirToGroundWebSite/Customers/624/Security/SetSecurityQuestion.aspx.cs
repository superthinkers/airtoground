﻿using System;
using System.Web.Security;

public partial class Customers_624_Security_SetSecurityQuestion : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_HELP_CHANGE_SECURITY_QA;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            MembershipUser u = ProviderUtils.GetUser(Profile.UserName);
            lstQuestions.SelectedValue = u.PasswordQuestion;
        }
    }
    protected void btnOk_Click(object sender, EventArgs e) {
        try {
            if (ProviderUtils.ChangePasswordQuestionAndAnswer(Profile.UserName, txtPassword.Text, lstQuestions.Text, txtAnswer.Text)) {
                // Go Home.
                // Users must have a default division and customer.
                Response.Redirect("~/Home.aspx");
            } else {
                lblError.Text = "Invalid Password.";
            }
        } catch {
            lblError.Text = "Exception. Contacted the Website Administrator.";
        }
    }
}