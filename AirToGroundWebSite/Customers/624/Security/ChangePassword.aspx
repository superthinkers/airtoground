﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/624/Default.master" AutoEventWireup="true"
    CodeFile="ChangePassword.aspx.cs" Inherits="Customers_624_Security_ChangePassword" Buffer="true"
    Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Password"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Change Your Password</div>
            <br />
            <br />
            <asp:ChangePassword ID="ChangePassword1" runat="server" Width="100%" OnChangedPassword="ChangePassword1_OnChangedPassword" >
                <ChangePasswordTemplate>
                    <table style="line-height: 25px; padding: 1px 1px 1px 1px; width: 100%">
                        <tr>
                            <td align="center">
                                <table cellpadding="0" width="100%">
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="CurrentPasswordLabel" SkinID="InfoLabel" runat="server" 
                                                AssociatedControlID="CurrentPassword">Password:</asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" CssClass="stdValidator"
                                                ControlToValidate="CurrentPassword" ErrorMessage="<br />Required." ToolTip="Password is required."
                                                ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="NewPasswordLabel" SkinID="InfoLabel" runat="server" 
                                                AssociatedControlID="NewPassword">New Password:</asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" ToolTip="Passwords require 7 characters and 1 number."></asp:TextBox>
                                            <%--<ajaxToolkit:PasswordStrength ID="PasswordStrength1" runat="server" TargetControlID="NewPassword"
                                                DisplayPosition="RightSide" PreferredPasswordLength="8" MinimumNumericCharacters="1"
                                                MinimumSymbolCharacters="0" StrengthIndicatorType="Text" TextStrengthDescriptions="Poor;Weak;Average;Good;Excellent"
                                                PrefixText="" TextCssClass="lblSubscript">
                                            </ajaxToolkit:PasswordStrength>--%>
                                            <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" CssClass="stdValidator"
                                                ControlToValidate="NewPassword" ErrorMessage="<br />Required."
                                                ToolTip="New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="ConfirmNewPasswordLabel" SkinID="InfoLabel" runat="server" 
                                                AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" CssClass="stdValidator"
                                                ControlToValidate="ConfirmNewPassword" ErrorMessage="<br />Required."
                                                ToolTip="Confirm New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" CssClass="stdValidator"
                                                ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" Display="Dynamic"
                                                ErrorMessage="The Confirm New Password must match the New Password." ValidationGroup="ChangePassword1"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2" style="color:Red;">
                                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <asp:Button ID="ChangePasswordPushButton" runat="server" 
                                                CommandName="ChangePassword" Text="Change Password" 
                                                ValidationGroup="ChangePassword1" />
                                            <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" 
                                                CommandName="Cancel" PostBackUrl="~/Home.aspx" Text="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ChangePasswordTemplate>
                <SuccessTemplate>
                    <table style="line-height: 25px; padding: 1px 1px 1px 1px; width: 100%">
                        <tr>
                            <td align="center">
                                <table cellpadding="0" width="100%">
                                    <tr>
                                        <td align="center" colspan="2">
                                            Change Password Complete</td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            Your password has been changed!</td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <asp:Button ID="ContinuePushButton" runat="server" CausesValidation="False" 
                                                CommandName="Continue" PostBackUrl="~/Home.aspx" Text="Continue" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </SuccessTemplate>
            </asp:ChangePassword>
            <br />
        </div>
    </div>
</asp:Content>

