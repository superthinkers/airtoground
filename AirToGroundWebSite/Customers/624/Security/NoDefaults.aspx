﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/624/Default.master" AutoEventWireup="true"
    CodeFile="NoDefaults.aspx.cs" Inherits="Customers_624_Security_NoDefaults" Buffer="false" Strict="true"
    Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <div style="position:relative;left:20%;width:60%;text-align:center">
        <br />
        <br />
        <div class="lblInfo">
            You do not have a default Customer / Company Division for this login.
            An Administrator must assign these for you before you can login.
            <br />
            <br />
            Please contact your Administrator.
            <br />
            <br />
            <asp:LoginStatus ID="LoginStatus1" Font-Names="Arial" Width="175px" runat="server"
                LoginText="Click Here to Log In" LogoutAction="RedirectToLoginPage" />
            <br />
            <br />
        </div>
    </div>
</asp:Content>

