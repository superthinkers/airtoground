﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/624/Default.master" AutoEventWireup="true"
    CodeFile="ManageUserRoles.aspx.cs" Inherits="Customers_624_Admin_ManageUserRoles" Buffer="true"
    Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
        SelectMethod="GetAllUsersEx"
        UpdateMethod="UpdateUserEx"
        InsertMethod="InsertUserEx"
        TypeName="MembershipHelper">
        <InsertParameters>
            <asp:ProfileParameter Name="currentUserName" PropertyName="UserName" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToPdfButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="true" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" AutoSize="true">
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="true">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnResetPassword">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnResetPassword">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnResetPassword" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="true"
        Skin="Office2007">
    </telerik:RadAjaxLoadingPanel>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Manage Users and Roles"></asp:Label>
        <br />
        <br />
        <div style="position: relative; width: 100%; float: left">
            <div class="div_container" style="width: 100%;">
                <div class="div_header">Manage Users</div>
                <%--<asp:Button ID="btnRunLoad" runat="server" Text="Run Load" OnClick="btnRunLoad_OnClick" Visible="false" />--%>
                <telerik:RadGrid ID="RadGrid1" GridLines="None"
                    runat="server" Height="440px" AllowAutomaticUpdates="True"
                    AllowPaging="True" PageSize="10"
                    AllowFilteringByColumn="true" AllowSorting="true" AutoGenerateColumns="False"
                    DataSourceID="ObjectDataSource1" OnItemUpdated="RadGrid1_ItemUpdated" OnItemInserted="RadGrid1_ItemInserted"
                    OnDataBound="RadGrid1_DataBound" AllowMultiRowSelection="false" HorizontalAlign="Left"
                    OnSelectedIndexChanged="RadGrid1_SelectedIndexChanged"
                    OnItemDataBound="RadGrid1_ItemDataBound">
                    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Userid,UserName" DataSourceID="ObjectDataSource1"
                        HorizontalAlign="Left" AllowAutomaticUpdates="true" AllowAutomaticInserts="true"
                        AutoGenerateColumns="False" EditMode="InPlace" CommandItemSettings-ShowRefreshButton="false">
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn DataField="Companydivisionid" UniqueName="CompanydivisionidColumn"
                                ReadOnly="true" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Customerid" UniqueName="CustomeridColumn"
                                ReadOnly="true" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="UserName" UniqueName="UserNameColumn" ReadOnly="true"
                                Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn DataField="Employeenumber" HeaderText="Employee Number"
                                AllowFiltering="true" SortExpression="Employeenumber" CurrentFilterFunction="Contains"
                                FilterControlWidth="98%" AutoPostBackOnFilter="true" ShowFilterIcon="false">
                                <InsertItemTemplate>
                                    <telerik:RadTextBox ID="txtEmployeeNumber" runat="server" MaxLength="20"
                                        Text='<%# Bind("Employeenumber") %>' Width="94%">
                                    </telerik:RadTextBox>
                                    <asp:CustomValidator ID="ReqEmployeeNumber" runat="server" CssClass="stdValidator"
                                        ControlToValidate="txtEmployeeNumber" ErrorMessage="<br />Must Be Unique"
                                        Display="Dynamic" OnServerValidate="ReqEmployeeNumber_Validate">
                                    </asp:CustomValidator>
                                    <asp:RequiredFieldValidator ID="ReqEmployeeNumber2" runat="server" CssClass="stdValidator"
                                        ControlToValidate="txtEmployeeNumber" ErrorMessage="<br />Required" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </InsertItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblEmployeeNumber" runat="server" SkinID="InfoLabel" Text='<%# Eval("Employeenumber") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeNumber" runat="server" SkinID="InfoLabel" Text='<%# Eval("Employeenumber") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="9%" />
                                <ItemStyle Width="9%" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="UserName" HeaderText="Login Name" AllowFiltering="true"
                                SortExpression="UserName" CurrentFilterFunction="Contains" FilterControlWidth="98%"
                                AutoPostBackOnFilter="true" ShowFilterIcon="false" UniqueName="UserNameColumn2">
                                <InsertItemTemplate>
                                    <telerik:RadTextBox ID="txtUserName" runat="server" MaxLength="255"
                                        Text='<%# Bind("UserName") %>' Width="94%">
                                    </telerik:RadTextBox>
                                    <asp:RequiredFieldValidator ID="ReqUserName" runat="server" CssClass="stdValidator"
                                        ControlToValidate="txtUserName" ErrorMessage="<br />Required" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </InsertItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblUserName" runat="server" SkinID="InfoLabel" Text='<%# Bind("UserName") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblUserName" runat="server" SkinID="InfoLabel" Text='<%# Eval("UserName") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="9%" />
                                <ItemStyle Width="9%" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="Firstname" HeaderText="First Name" SortExpression="Firstname"
                                AllowFiltering="true" CurrentFilterFunction="Contains" FilterControlWidth="98%"
                                AutoPostBackOnFilter="true" ShowFilterIcon="false">
                                <EditItemTemplate>
                                    <telerik:RadTextBox ID="txtFirstName" runat="server" MaxLength="30"
                                        Text='<%# Bind("Firstname") %>' Width="94%">
                                    </telerik:RadTextBox>
                                    <asp:RequiredFieldValidator ID="ReqFirstName" runat="server" CssClass="stdValidator"
                                        ControlToValidate="txtFirstName" ErrorMessage="<br />Required" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstName" runat="server" SkinID="InfoLabel" Text='<%# Eval("Firstname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="9%" />
                                <ItemStyle Width="9%" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="Lastname" HeaderText="Last Name" AllowFiltering="true"
                                SortExpression="Lastname" CurrentFilterFunction="Contains" FilterControlWidth="98%"
                                AutoPostBackOnFilter="true" ShowFilterIcon="false">
                                <EditItemTemplate>
                                    <telerik:RadTextBox ID="txtLastName" runat="server" MaxLength="30"
                                        Text='<%# Bind("Lastname") %>' Width="94%">
                                    </telerik:RadTextBox>
                                    <asp:RequiredFieldValidator ID="ReqLastName" runat="server" CssClass="stdValidator"
                                        ControlToValidate="txtLastName" ErrorMessage="<br />Required" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblLastName" runat="server" SkinID="InfoLabel" Text='<%# Eval("Lastname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="9%" />
                                <ItemStyle Width="9%" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="Email" HeaderText="Email" AllowFiltering="true"
                                CurrentFilterFunction="Contains" FilterControlWidth="98%" AutoPostBackOnFilter="true"
                                ShowFilterIcon="false">
                                <EditItemTemplate>
                                    <telerik:RadTextBox ID="txtEmail" runat="server" MaxLength="255"
                                        Text='<%# Bind("Email") %>' Width="94%">
                                    </telerik:RadTextBox>
                                    <asp:RequiredFieldValidator ID="ReqEmail" runat="server" CssClass="stdValidator"
                                        ControlToValidate="txtEmail" ErrorMessage="<br />Required" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmail" runat="server" SkinID="InfoLabel" Text='<%# Bind("Email") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridNumericColumn DataField="Salaryamount" HeaderText="Salary" AllowSorting="false" AllowFiltering="false"
                                DecimalDigits="2" DefaultInsertValue="0.00" NumericType="Currency" UniqueName="SalaryamountColumn">
                                <HeaderStyle HorizontalAlign="Center" Width="4%" />
                                <ItemStyle HorizontalAlign="Center" Width="4%" />
                            </telerik:GridNumericColumn>
                            <telerik:GridNumericColumn DataField="Hourlyrate" HeaderText="Hourly Rate" AllowSorting="false"
                                AllowFiltering="false" DecimalDigits="2" DefaultInsertValue="0.00" NumericType="Currency" UniqueName="HourlyrateColumn">
                                <HeaderStyle HorizontalAlign="Center" Width="4%" />
                                <ItemStyle HorizontalAlign="Center" Width="4%" />
                            </telerik:GridNumericColumn>
                            <telerik:GridCheckBoxColumn DataField="Useca24hourrule" HeaderText="CA Rule?" AllowSorting="false"
                                AllowFiltering="false" DefaultInsertValue="false" UniqueName="Useca24hourruleColumn">
                                <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridTemplateColumn DataField="Password" HeaderText="Password" AllowFiltering="false">
                                <InsertItemTemplate>
                                    <asp:TextBox ID="txtPassword" runat="server" MaxLength="50"
                                        TextMode="Password" Text='<%# Bind("Password") %>' Width="92%" ToolTip="Passwords require 7 characters and 1 number.">
                                    </asp:TextBox>
                                    <%--<ajaxToolkit:PasswordStrength ID="PasswordStrength1" runat="server" TargetControlID="txtPassword"
                                        DisplayPosition="AboveLeft" PreferredPasswordLength="8" MinimumNumericCharacters="1"
                                        MinimumSymbolCharacters="0" StrengthIndicatorType="Text" TextStrengthDescriptions="Poor;Weak;Average;Good;Excellent"
                                        PrefixText="" TextCssClass="lblSubscript">
                                    </ajaxToolkit:PasswordStrength>--%>
                                    <asp:RequiredFieldValidator ID="ReqPassword" runat="server" CssClass="stdValidator"
                                        ControlToValidate="txtPassword" ErrorMessage="<br />Required" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="ReqPassword2" runat="server" CssClass="stdValidator"
                                        ControlToValidate="txtPassword" ErrorMessage="<br />Length &gt;= 8" Display="Dynamic"
                                        OnServerValidate="ReqPassword2_OnServerValidate">
                                    </asp:CustomValidator>
                                </InsertItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblPassword" runat="server" SkinID="InfoLabel" Text="(use reset button)"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPassword" runat="server" SkinID="InfoLabel" Text="(hidden)"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="8%" />
                                <ItemStyle Width="8%" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridCheckBoxColumn DataField="IsApproved" HeaderText="Approved"
                                UniqueName="ApprovedColumn" ItemStyle-ForeColor="Black" ReadOnly="false" AllowFiltering="false">
                                <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                <ItemStyle Width="8%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridBoundColumn DataField="Updusername" HeaderText="Upd User"
                                ReadOnly="true" AllowSorting="false" AllowFiltering="false">
                                <HeaderStyle Width="8%" />
                                <ItemStyle Width="8%" />
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="DeleteCommandColumn" AllowFiltering="false">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <div style="width: 20px">
                                        <asp:ImageButton ID="btnDelete" runat="server" SkinID="Delete" OnClick="btnDelete_Click" />
                                    </div>
                                </ItemTemplate>
                                <HeaderStyle Width="30px" />
                                <ItemStyle Width="30px" />
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <CommandItemSettings ShowExportToCsvButton="true" />
                        <PagerStyle AlwaysVisible="true" Mode="NumericPages" Position="Bottom" />
                        <AlternatingItemStyle BackColor="#F2F0F2" />
                    </MasterTableView>
                    <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false"
                        Csv-FileExtension="csv" />
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <ClientEvents OnRowDblClick="RowDblClick" />
                        <Selecting AllowRowSelect="true" />
                        <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="false" />
                    </ClientSettings>
                </telerik:RadGrid>
                <div style="text-align: right; width: 100%">
                    <asp:Button ID="btnResetPassword" runat="server" OnClick="btnResetPassword_Click"
                        Text="Reset Password" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

