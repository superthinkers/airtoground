﻿using System;

public partial class Customers_624_Help_Faq : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_HELP_INDEX_FAQ;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            lstFaqs.DataSource = FaqUtils.GetCustomerFaqs();
            lstFaqs.DataBind();
        }
    }
}