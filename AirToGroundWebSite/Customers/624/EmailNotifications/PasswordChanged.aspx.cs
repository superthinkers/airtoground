﻿using System;
//using System.Web.Security;

public partial class Customers_624_EmailNotifications_PasswordChanged : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            if (Request.QueryString["Password"].ToString() == EmailUtils.DONOTSHOWPASSWORD) {
                lblUser.Text = Request.QueryString["UserName"].ToString();
                lblPassword.Text = "";
                lblPassword.Visible = false;
            } else {
                lblUser.Text = Request.QueryString["UserName"].ToString() + ":&nbsp";
                lblPassword.Text = Request.QueryString["Password"].ToString();
                lblPassword.Visible = true;
            }
            lblDateTime.Text = "On: " + DateTime.Now.ToShortDateString() + " at: " + DateTime.Now.ToShortTimeString();
            lnkATG.NavigateUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ATGLoginURL"];
        }
    }
}