﻿using System;
//using System.Web.Security;

public partial class Customers_624_EmailNotifications_AdminPasswordChanged : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            lblUser.Text = Request.QueryString["UserName"].ToString();
            lblDateTime.Text = "On: " + DateTime.Now.ToShortDateString() + " at: " + DateTime.Now.ToShortTimeString();
            lnkATG.NavigateUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ATGLoginURL"];
        }
    }
}