﻿<%@ Page Language="C#" MasterPageFile="~/Customers/624/Default.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Customers_624_Home" Title="Home"
    EnableViewState="false" Buffer="true" Explicit="true" Strict="true" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
        SelectMethod="GetAllServiceSchedules" TypeName="ATGDB.Serviceschedule">
        <SelectParameters>
            <asp:ProfileParameter Name="customerId" PropertyName="CustomerId" Type="Int32" />
            <asp:ControlParameter Name="range" PropertyName="SelectedValue" ControlID="lstRange" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <telerik:RadAjaxManager ID="AjaxManager" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="lstSummary">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lstSummary" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="LoadingPanel" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Home"></asp:Label>
        <br />
        <br />
        <div style="position: relative; left: 10%; width: 80%">
            <div style="text-align: center; position: relative; float: left; width: 48%;">
                <div class="div_container">
                    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true" Width="100%" LoadingPanelID="LoadingPanel">
                        <div class="div_header">Schedule Summary</div>
                        <div style="text-align: center; width: 100%">
                            <div style="position: relative; float: left; margin-top: 5px; width: 8%">Show</div>
                            <div style="position: relative; float: left; width: 15%">
                                <asp:DropDownList ID="lstRange" runat="server" Width="100%">
                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                    <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                    <asp:ListItem Text="45" Value="45"></asp:ListItem>
                                    <asp:ListItem Text="60" Value="60"></asp:ListItem>
                                    <asp:ListItem Text="75" Value="75"></asp:ListItem>
                                    <asp:ListItem Text="90" Value="90"></asp:ListItem>
                                    <asp:ListItem Text="120" Value="120" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div style="position: relative; float: left; margin-top: 5px; margin-left: 9px; width: 11%">Days Out</div>
                            <div style="position: relative; float: left; margin-top: 1px; width: 15%">
                                <asp:Button ID="btnRefreshSummary" runat="server" Text="Refresh" />
                            </div>
                            <div style="position: relative; float: left; width: 100%">
                                <table style="position: relative; width: 100%">
                                    <tr>
                                        <td colspan="2" style="text-align: center">
                                            <br />
                                            <label style="font-weight: bold; background-color: MediumSpringGreen">
                                                SCHEDULED ITEMS</label>
                                            <label style="font-weight: bold; background-color: LightBlue">
                                                COMPLETED ITEMS</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; width: 75%">
                                            <asp:Label ID="lblHeader1" runat="server" SkinID="InfoLabelBold" Text="Service Name"></asp:Label>
                                        </td>
                                        <td style="text-align: center; width: 25%">
                                            <asp:Label ID="lblHeader2" runat="server" SkinID="InfoLabelBold" Text="Qty"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <telerik:RadListView ID="lstSummary" runat="server" DataSourceID="ObjectDataSource1"
                                                DataKeyNames="Serviceid" AllowPaging="false">
                                                <ItemTemplate>
                                                    <div style="position: relative; float: left; text-align: left; width: 75%">
                                                        <asp:Label ID="lblDescr1" runat="server" SkinID="InfoLabelBold" BackColor='<%# GetColor() %>'
                                                            Text='<%# Eval("Description").ToString() %>' Width="100%"></asp:Label>
                                                    </div>
                                                    <div style="position: relative; float: left; text-align: center; width: 25%">
                                                        <asp:Label ID="lblCount1" runat="server" SkinID="InfoLabelBold" BackColor='<%# GetColor() %>'
                                                            Text='<%# Eval("Count").ToString() %>' Width="100%"></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </telerik:RadListView>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </div>
                        </div>
                    </telerik:RadAjaxPanel>
                </div>
                <div class="div_container">
                    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" EnableAJAX="true" Width="100%" LoadingPanelID="LoadingPanel">
                        <div class="div_header">Today's Queue</div>
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    <telerik:RadListView ID="lstQueueSummary" runat="server" AllowPaging="false">
                                        <ItemTemplate>
                                            <div style="position: relative; float: left; text-align: left; width: 75%">
                                                <asp:Label ID="lblDescr2" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Description").ToString() %>'
                                                    Width="100%"></asp:Label>
                                            </div>
                                            <div style="position: relative; float: left; text-align: center; width: 25%">
                                                <asp:Label ID="lblCount2" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Count").ToString() %>'
                                                    Width="100%"></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </telerik:RadListView>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadAjaxPanel>
                </div>
                <div id="divVerifications" runat="server" class="div_container">
                    <telerik:RadAjaxPanel ID="RadAjaxPanel3" runat="server" EnableAJAX="true" Width="100%" LoadingPanelID="LoadingPanel">
                        <div class="div_header">Verification Summary</div>
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    <telerik:RadListView ID="lstVerifications" runat="server" AllowPaging="false">
                                        <ItemTemplate>
                                            <div style="position: relative; float: left; text-align: left; width: 75%">
                                                <asp:Label ID="lblDescr3" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Description").ToString() %>'
                                                    Width="100%"></asp:Label>
                                            </div>
                                            <div style="position: relative; float: left; text-align: center; width: 25%">
                                                <asp:Label ID="lblCount3" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Count").ToString() %>'
                                                    Width="100%"></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </telerik:RadListView>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadAjaxPanel>
                </div>
            </div>
            <div style="text-align: center; position: relative; float: left; width: 49%;">
                <div class="div_container">
                    <div class="div_header">What Would You Like to do Today?</div>
                    <div style="text-align: left; width: 99%">
                        <table style="width: 98%;">
                            <tr>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="lnkViewServiceQueue" runat="server" SkinID="ImageBrowseSchedules" Width="32px"
                                        Height="32px" NavigateUrl="~/ServiceQueue/ServiceQueue2.aspx">
                                    </asp:HyperLink>
                                </td>
                                <td>
                                    <asp:HyperLink ID="lnkViewServiceQueue1" runat="server" NavigateUrl="~/ServiceQueue/ServiceQueue2.aspx"
                                        Text="Today's Queue">
                                    </asp:HyperLink>
                                </td>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="HyperLink4" runat="server" SkinID="ImageClock" Width="32px" Height="32px"
                                        NavigateUrl="~/WorkLog/WorkLogEntryWizard.aspx">
                                    </asp:HyperLink>
                                </td>
                                <td>
                                    <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/WorkLog/WorkLogEntryWizard.aspx"
                                        Text="Enter My Time">
                                    </asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="lnkViewMasterSchedule" runat="server" SkinID="ImageBrowseSchedules"
                                        Width="32px" Height="32px" NavigateUrl="~/Schedules/ViewMasterSchedule.aspx">
                                    </asp:HyperLink>
                                </td>
                                <td>
                                    <asp:HyperLink ID="lnkViewMasterSchedule2" runat="server" NavigateUrl="~/Schedules/ViewMasterSchedule.aspx"
                                        Text="View Master Schedule">
                                    </asp:HyperLink>
                                </td>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="lnkCreateSchedule" runat="server" SkinID="ImageDate1" Width="32px"
                                        Height="32px" NavigateUrl="~/Schedules/CreateSchedule.aspx">
                                    </asp:HyperLink>
                                </td>
                                <td>
                                    <asp:HyperLink ID="lnkCreateSchedule2" runat="server" NavigateUrl="~/Schedules/CreateSchedule.aspx"
                                        Text="Schedule Aircraft">
                                    </asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="lnkViewSchedules" runat="server" SkinID="ImageBrowseSchedules"
                                        Width="32px" Height="32px" NavigateUrl="~/Schedules/ViewSchedules.aspx">
                                    </asp:HyperLink>
                                </td>
                                <td>
                                    <asp:HyperLink ID="lnkViewSchedules2" runat="server" NavigateUrl="~/Schedules/ViewSchedules.aspx"
                                        Text="View Tail Schedule">
                                    </asp:HyperLink>
                                </td>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="HyperLink6" runat="server" SkinID="ImageDate2" Width="32px" Height="32px"
                                        NavigateUrl="~/Schedules/ModifySchedule.aspx">
                                    </asp:HyperLink>
                                </td>
                                <td>
                                    <asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="~/Schedules/ModifySchedule.aspx"
                                        Text="Re-schedule Aircraft">
                                    </asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="div_container">
                    <div class="div_header">Reporting and Analysis</div>
                    <div style="text-align: left; width: 99%">
                        <table id="tblEquipment" runat="server" style="width: 99%;">
                            <tr>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="HyperLink13" runat="server" SkinID="ImageReport" Width="32px"
                                        Height="32px" NavigateUrl="~/Reports/Equipment/OpenExceptionsReport.aspx"></asp:HyperLink>
                                </td>
                                <td style="width: 46%">
                                    <asp:HyperLink ID="HyperLink14" runat="server" NavigateUrl="~/Reports/Equipment/OpenExceptionsReport.aspx"
                                        Text="Open Exceptions Report"></asp:HyperLink>
                                </td>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="HyperLink3" runat="server" SkinID="ImageReport" Width="32px" Height="32px"
                                        NavigateUrl="~/Reports/Equipment/CustomerEquipmentListReport.aspx"></asp:HyperLink>
                                </td>
                                <td>
                                    <asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/Reports/Equipment/CustomerEquipmentListReport.aspx"
                                        Text="Aircraft List"></asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="HyperLink15" runat="server" SkinID="ImageReport" Width="32px"
                                        Height="32px" NavigateUrl="~/Reports/Equipment/ServiceRollupReport.aspx"></asp:HyperLink>
                                </td>
                                <td style="width: 46%">
                                    <asp:HyperLink ID="HyperLink16" runat="server" NavigateUrl="~/Reports/Equipment/ServiceRollupReport.aspx"
                                        Text="Service Rollup Report"></asp:HyperLink>
                                </td>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="HyperLink17" runat="server" SkinID="ImageReport" Width="32px"
                                        Height="32px" NavigateUrl="~/Reports/Equipment/CustomerEquipmentStatusListReport.aspx"></asp:HyperLink>
                                </td>
                                <td>
                                    <asp:HyperLink ID="HyperLink18" runat="server" NavigateUrl="~/Reports/Equipment/CustomerEquipmentStatusListReport.aspx"
                                        Text="Aircraft Status List"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                        <table id="tblWork" runat="server" style="width: 99%;">
                            <tr>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="HyperLink29" runat="server" SkinID="ImageReport" Width="32px"
                                        Height="32px" NavigateUrl="~/Reports/WorkAgingReport.aspx"></asp:HyperLink>
                                </td>
                                <td style="width: 46%">
                                    <asp:HyperLink ID="HyperLink30" runat="server" NavigateUrl="~/Reports/Work/WorkAgingReport.aspx"
                                        Text="Work Aging Report"></asp:HyperLink>
                                </td>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="HyperLink31" runat="server" SkinID="ImageReport" Width="32px"
                                        Height="32px" NavigateUrl="~/Reports/Work/EndOfDayRapSheetReport.aspx"></asp:HyperLink>
                                </td>
                                <td>
                                    <asp:HyperLink ID="HyperLink32" runat="server" NavigateUrl="~/Reports/Work/EndOfDayRapSheetReport.aspx"
                                        Text="Daily Wrap-up Sheet"></asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="HyperLink35" runat="server" SkinID="ImageReport" Width="32px"
                                        Height="32px" NavigateUrl="~/Reports/DailyWorkSheetReport.aspx"></asp:HyperLink>
                                </td>
                                <td style="width: 46%">
                                    <asp:HyperLink ID="HyperLink36" runat="server" NavigateUrl="~/Reports/Work/DailyWorkSheetReport.aspx"
                                        Text="Daily Work Sheet"></asp:HyperLink>
                                </td>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="HyperLink10" runat="server" SkinID="ImageReport" Width="32px"
                                        Height="32px"
                                        NavigateUrl="~/Reports/Equipment/ServicePerformedReport.aspx"></asp:HyperLink>
                                </td>
                                <td>
                                    <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="~/Reports/Equipment/ServicePerformedReport.aspx"
                                        Text="Service Performed"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                        <%--<table id="tblRevenue" runat="server" style="width: 99%;">
                            <tr>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="HyperLink51" runat="server" SkinID="ImageReport" Width="32px"
                                        Height="32px" NavigateUrl="~/Reports/CustomerWorkReport.aspx"></asp:HyperLink>
                                </td>
                                <td style="width: 46%">
                                    <asp:HyperLink ID="HyperLink52" runat="server" NavigateUrl="~/Reports/Revenue/CustomerWorkReport.aspx"
                                        Text="Work Revenue Detail Summary"></asp:HyperLink>
                                </td>
                                <td style="width: 32px">
                                    <asp:HyperLink ID="HyperLink1" runat="server" SkinID="ImageReport" Width="32px" Height="32px"
                                        NavigateUrl="~/Reports/Revenue/ServiceLocationReport.aspx"></asp:HyperLink>
                                </td>
                                <td>
                                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Reports/Revenue/ServiceLocationReport.aspx"
                                        Text="Service - Location Report"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

