﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/201619/Default.master" AutoEventWireup="true"
    CodeFile="RequestForService.aspx.cs" Inherits="Customers_201619_Help_RequestForService" Buffer="true"
    Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Customer Service Requests"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Request for Service</div>
            <div style="text-align: left; position: relative; left: 20%; width: 60%">
                <p>
                    If you have questions or requests that are not suited to this form, 
                    you may email <a href="mailto:customersupport@airtogroundtrack.com">Customer Support</a>
                    and we will return to you as soon as possible.
                </p>
                <p>
                    This is fairly easy to use...just select your request type and fill in the fields.
                    When you are finished, click "Submit Request". If there are any special instructions,
                    be sure to type them into the notes area.
                </p>
            </div>
            <div class="lblTitle">Request for Service</div>
            <table style="width: 85%">
                <tr>
                    <td align="right" style="width: 30%">
                        <asp:Label ID="lblUserName1" runat="server" SkinID="InfoLabel" Text="User Name:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblUserName" runat="server" SkinID="InfoLabel" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 30%">
                        <asp:Label ID="lblEmail1" runat="server" SkinID="InfoLabel" Text="Email Address:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblEmail" runat="server" SkinID="InfoLabel" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblDate1" runat="server" SkinID="InfoLabel" Text="Today's Date:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblDate" runat="server" SkinID="InfoLabel" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblPhone1" runat="server" SkinID="InfoLabel" Text="Is there a Phone Number We May Call?:"></asp:Label>
                    </td>
                    <td align="left">
                        <telerik:RadMaskedTextBox ID="txtPhone" runat="server" SkinID="PopBox" Text="" Width="15%"
                            AutoCompleteType="BusinessPhone" DisplayMask="(###) ###-####" ResetCaretOnFocus="true"
                            SelectionOnFocus="SelectAll" ReadOnly="false" Enabled="true" 
                            Mask="(###) ###-####">
                        </telerik:RadMaskedTextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblReqType1" runat="server" SkinID="InfoLabel" Text="Request Type:"></asp:Label>
                    </td>
                    <td align="left">
                        <telerik:RadComboBox ID="lstRequestType" runat="server" Width="50%">
                            <Items>
                                <telerik:RadComboBoxItem Value="1" Text="Schedule Aircraft" />
                                <telerik:RadComboBoxItem Value="2" Text="Cancel Scheduled Aircraft" />
                                <telerik:RadComboBoxItem Value="3" Text="Suspend Aircraft Service" />
                                <telerik:RadComboBoxItem Value="4" Text="Return Aircraft to Revenue" />
                                <telerik:RadComboBoxItem Value="5" Text="Add New Aircraft w/Service" />
                                <telerik:RadComboBoxItem Value="6" Text="Unlisted Request" />
                            </Items>
                        </telerik:RadComboBox>
                        <asp:RequiredFieldValidator ID="reqReqType" runat="server" CssClass="stdValidator"
                            ControlToValidate="lstRequestType" ErrorMessage="*" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblTail1" runat="server" SkinID="InfoLabel" Text="Tail Number:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtTailNumber" runat="server" SkinID="PopBox" Text="" MaxLength="10" Width="15%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqTailNumber" runat="server" ControlToValidate="txtTailNumber"
                            ErrorMessage="*" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblSvcDate1" runat="server" SkinID="InfoLabel" Text="Service Date:"></asp:Label>
                    </td>
                    <td align="left">
                        <telerik:RadDatePicker ID="dtSvcDate" runat="server" Width="20%"></telerik:RadDatePicker>
                        <asp:RequiredFieldValidator ID="reqSvcDate" runat="server" ControlToValidate="dtSvcDate"
                            ErrorMessage="*" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblNotes1" runat="server" SkinID="InfoLabel" Text="Notes / Instructions:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtNotes" runat="server" SkinID="PopBox" Text="" TextMode="MultiLine"
                            Height="200px" Wrap="true" Rows="40" MaxLength="500" Width="94%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit Request" OnClick="btnSubmit_Click" />    
                    </td>
                </tr>
                <tr>
                    <td colspan="2"> 
                        <asp:Label ID="lblStatus" runat="server" ForeColor="Green" SkinID="InfoLabel" 
                            Text="The Request has been Submitted" Visible="false"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

