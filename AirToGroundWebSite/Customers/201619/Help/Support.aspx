﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/201619/Default.master" AutoEventWireup="true"
    CodeFile="Support.aspx.cs" Inherits="Customers_201619_Help_Support" Buffer="true" Strict="true"
    Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSubmit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadCaptcha1" />
                    <telerik:AjaxUpdatedControl ControlID="txtPhone" />
                    <telerik:AjaxUpdatedControl ControlID="txtScreen" />
                    <telerik:AjaxUpdatedControl ControlID="txtTask" />
                    <telerik:AjaxUpdatedControl ControlID="txtError" />
                    <telerik:AjaxUpdatedControl ControlID="lblStatus" />
                    <telerik:AjaxUpdatedControl ControlID="btnSubmit" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="RadCaptcha1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadCaptcha1" />
                    <telerik:AjaxUpdatedControl ControlID="lblStatus" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Website Support"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Website Support</div>
            <br />
            <div class="lblTitle">Error Submission</div>
            <br />
            <div id="divError1" runat="server" style="display: none">
                <asp:Label ID="lblError1" runat="server" ForeColor="Red" Font-Size="X-Large" Text="OOPS! An Error has Occurred."></asp:Label>
                <br />
                <asp:Label ID="lblError2" runat="server" SkinID="SubscriptLabel" Text="(Please do not submit the same error again)"></asp:Label>
            </div>
            <div id="divError2" runat="server" style="display: none">
                <p>
                    If you have questions about how a particular screen functions, please
                    consult the online <a href="./Faq.aspx">Help</a> system. If you need
                    additional assistance, you may email the <a href="mailto:websiteadmin@airtogroundtrack.com">Website Administration</a>
                    and they will respond to you as soon as possible.
                </p>
                <p>
                    If you experience technical difficulty with the website, use this error
                    submittal form to send this critical, but minimal information to the
                    Website Administration. Please be as clear as possible, and include as
                    many details as you can. Note that sending multiple reports does not
                    help unless you have new information to report, in which case please do!
                </p>
            </div>
            <br />
            <table id="tblMain" runat="server" style="width: 100%">
                <tr>
                    <td align="right" style="width: 20%">
                        <asp:Label ID="lbl1" runat="server" SkinID="InfoLabel" Text="User Name:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblUserName" runat="server" SkinID="InfoLabel" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lbl2a" runat="server" SkinID="InfoLabel" Text="What Phone May You Be Contacted At?:"></asp:Label>
                    </td>
                    <td align="left">
                        <telerik:RadMaskedTextBox ID="txtPhone" runat="server" Text="" Width="98%"
                            AutoCompleteType="BusinessPhone" DisplayMask="(###) ###-####" ResetCaretOnFocus="true"
                            SelectionOnFocus="SelectAll" ReadOnly="false" Enabled="true"
                            Mask="(###) ###-####" ValidationGroup="VAL">
                        </telerik:RadMaskedTextBox>
                        <%--<asp:RequiredFieldValidator ID="reqPhone" runat="server" CssClass="stdValidator"
                            ControlToValidate="txtPhone" ErrorMessage="(Required)" Display="Dynamic" ValidationGroup="VAL">
                        </asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lbl2" runat="server" SkinID="InfoLabel" Text="Date:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblDate" runat="server" SkinID="InfoLabel" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lbl3" runat="server" SkinID="InfoLabel" Text="What Screen Were You On?:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtScreen" runat="server" Text="" Width="98%" ValidationGroup="VAL"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqScreen" runat="server" CssClass="stdValidator"
                            ControlToValidate="txtScreen" ErrorMessage="(Required)" Display="Static" ValidationGroup="VAL">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lbl4" runat="server" SkinID="InfoLabel" Text="What Were You Doing?:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtTask" runat="server" Text="" TextMode="MultiLine"
                            Height="100px" Wrap="true" Rows="20" MaxLength="500" Width="98%" ValidationGroup="VAL"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqTask" runat="server" CssClass="stdValidator" ControlToValidate="txtTask"
                            ErrorMessage="(Required)" Display="Static" ValidationGroup="VAL">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lbl5" runat="server" SkinID="InfoLabel" Text="What Was the Error?:"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtError" runat="server" Text="" TextMode="MultiLine"
                            Height="100px" Wrap="true" Rows="20" MaxLength="500" Width="98%" ValidationGroup="VAL"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqError" runat="server" CssClass="stdValidator"
                            ControlToValidate="txtError" ErrorMessage="(Required)" Display="Static" ValidationGroup="VAL">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <br />
                        <div style="position: relative; float: left; width: 100%">
                            <telerik:RadCaptcha ID="RadCaptcha1" runat="server"
                                Display="Static" ClientIDMode="AutoID"
                                Width="190px" ErrorMessage="Wrong Letters. Try Again." IgnoreCase="true"
                                EnableRefreshImage="true" ProtectionMode="Captcha" CaptchaTextBoxLabel="" ValidationGroup="VAL">
                                <CaptchaImage LineNoise="High" UseRandomFont="True" FontWarp="Medium" />
                            </telerik:RadCaptcha>
                        </div>
                        <br />
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit Error"
                            CausesValidation="true" OnClick="btnSubmit_OnClick" ValidationGroup="VAL" />    
                    </td>
                </tr>
                <tr>
                    <td colspan="2"> 
                        <asp:Label ID="lblStatus" runat="server" ForeColor="Green" SkinID="InfoLabel" 
                            Text="The Error has been Submitted" Visible="false"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </div>
    </div>
</asp:Content>

