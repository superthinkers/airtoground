﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/201619/Default.master" AutoEventWireup="true"
    CodeFile="Faq.aspx.cs" Inherits="Customers_201619_Help_Faq" Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Online Help Information"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Online Help Information</div>
            <telerik:RadListBox ID="lstFaqs" runat="server" DataKeyField="FaqId" Width="100%">
                <ItemTemplate>
                    <asp:HyperLink ID="lnlFaq" runat="server" Text='<%# Eval("FaqText") %>' NavigateUrl='<%# Eval("FaqUrl") %>'></asp:HyperLink>                    
                    &nbsp;-&nbsp;
                    <asp:Label ID="lblDescr" runat="server" Text='<%# Eval("FaqDescr") %>'></asp:Label>
                </ItemTemplate>
            </telerik:RadListBox>
        </div>
    </div>
</asp:Content>

