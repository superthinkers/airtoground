﻿using System;
using System.Data;
//using Devart.Data.Linq;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;
using ATGDB;
//using Telerik.Web.UI.GridExcelBuilder;
//using System.Web.UI.WebControls;

public partial class Customers_201619_Schedules_ViewMasterSchedule : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_SCHEDULE_MASTER_SCHEDULE;
    }
    // For postbacks in the JavaScript in the page.
    protected string PostBackString;

    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    public class MySSD
    {
        public Int32 Serviceshortdescrid { get; set; }
        public string Description { get; set; }
        public ATGDB.Service Service { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e) {
        // For postbacks in the JavaScript in the page.
        PostBackString = Page.ClientScript.GetPostBackEventReference(this, "");

        if (!Page.IsPostBack) {
            // Set the key first time in. New cache.
            hiddenPageKey.Value = new Random().Next(10000000, 999999999).ToString();
            ScheduleHelper.Instance.MasterPageKey = hiddenPageKey.Value;
            ScheduleHelper.Instance.ViewPageKey = hiddenPageKey.Value;
            // Create/Clear the cache.
            System.Web.HttpContext.Current.Session.Add(Serviceschedule.MASTER_SCHEDULE_CACHE + ScheduleHelper.Instance.MasterPageKey, "");
            System.Web.HttpContext.Current.Session.Add(Serviceschedule.VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey, "");

            // Security.
            if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_SCH)) {
                btnExportToExcel.Visible = false;
                //RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                //RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                //RadGrid1.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                //RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
            }

            // UI Defaults.
            dtFrom.SelectedDate = DateTime.Now.Date;
            dtTo.SelectedDate = DateTime.Now.Date.AddDays(30);
            chkFuture.Checked = false;
            lblLate.Style.Add("background-color", ColorUtils.GetSS_LateBackColor(Profile.SiteTheme));

            // UI settings. Gets rid of horizontal scroll problems.
            RadGrid1.Style.Add("overflow", "hidden");

            // More Selection criteria.
            //lstCustomerSelector.SelectedCustomerId = Profile.Customerid;
            lstCustServices.DataSource = Service.GetMSorVSServicesByCustomerId(int.Parse(Profile.Customerid));//lstCustomerSelector.SelectedCustomerId));
            lstCustEqTypes.DataSource = Equipmenttype.GetEquipmentTypesByCustomerId(int.Parse(Profile.Customerid));//lstCustomerSelector.SelectedCustomerId));
            lstCustServices.DataBind();
            lstCustEqTypes.DataBind();

            // Service Short Descr Legend
            ATGDataContext dc = new ATGDataContext();
            var data = (from ssd in dc.Serviceshortdescrs
                        from svc in dc.Services
                        where svc.Serviceshortdescrid == ssd.Serviceshortdescrid
                        where svc.Customerid == int.Parse(Profile.Customerid)//lstCustomerSelector.SelectedCustomerId)
                        where svc.Showinmasterschedule == true
                        select new MySSD {
                            Serviceshortdescrid = ssd.Serviceshortdescrid,
                            Description = ssd.Description,
                            Service = svc
                            }).OrderBy(x => x.Description).Distinct();
            lstServiceDescr.DataSource = data;
            lstServiceDescr.DataBind();

        } else {
            // Ensure the current key is set to this page's key
            ScheduleHelper.Instance.MasterPageKey = hiddenPageKey.Value;
            ScheduleHelper.Instance.ViewPageKey = hiddenPageKey.Value;
        }
    }
    protected void OnAjaxUpdate(object sender, ToolTipUpdateEventArgs args) {
        ScheduleHelper.Instance.MasterPageKey = hiddenPageKey.Value;
        ScheduleHelper.Instance.ViewPageKey = hiddenPageKey.Value;
        this.UpdateToolTip(args.Value, args.UpdatePanel);
    }
    protected void UpdateToolTip(string data, UpdatePanel panel) {
		Control ctrl = Page.LoadControl("~/Customers/201619/Schedules/ScheduleToolTipControl.ascx");
        panel.ContentTemplateContainer.Controls.Add(ctrl);
		Customers_201619_ScheduleToolTipControl tt = (Customers_201619_ScheduleToolTipControl)ctrl;
        tt.Data = data;           
    }
    //protected void lstCustomerSelector_SelectedIndexChanged(object sender, EventArgs e) {
    //    lstCustServices.DataSource = Service.GetMSorVSServicesByCustomerId(int.Parse(lstCustomerSelector.SelectedCustomerId));
    //    lstCustEqTypes.DataSource = Equipmenttype.GetEquipmentTypesByCustomerId(int.Parse(lstCustomerSelector.SelectedCustomerId));
    //    lstCustServices.DataBind();
    //    lstCustEqTypes.DataBind();
    //}
    string[] headerDates = null;
    protected void SetupToolTips(GridItem item) {
        DataRowView curRow = (DataRowView)item.DataItem;
        Control ctrl;
        GridTableCell cell;
        GridTableHeaderCell cell2;
        for (int i = 0; i < item.Cells.Count; i++) {
            if (item.Cells[i] is GridTableHeaderCell) {
                if (headerDates == null) {
                    headerDates = new string[item.Cells.Count];
                }
                cell2 = (GridTableHeaderCell)item.Cells[i];
                if (cell2.Controls.Count > 0) {
                    headerDates[i] = cell2.Text;
                }
            } else if (item.Cells[i] is GridTableCell) {
                cell = (GridTableCell)item.Cells[i];
                if (cell.Controls.Count > 0) {
                    ctrl = cell.Controls[0];

                    // Add the control to the tooltip manager.
                    if (ctrl != null) {
                        this.RadToolTipManager1.TargetControls.Add(ctrl.ClientID,
                            curRow.Row["Tailnumber"].ToString() + ":" + headerDates[i] + ":" + hiddenPageKey.Value, true);
                    }
                }
            }
        }
    }
    protected void btnSearch_OnClick(object sender, EventArgs e) {
        // User must select a service or eq type if using future casting.
        /*if (chkFuture.Checked) {
            if ((lstCustServices.CheckedItems.Count == 0) &&
               (lstCustEqTypes.CheckedItems.Count == 0)) {
                RadWindowManager1.RadAlert("You must select a Service and/or an Aircraft Type", 400, 90, "Oops!", "");
                ObjectDataSource1.DataBind();
                RadGrid1.DataBind();
                return;
            }
        }*/
        
        if (dtTo.SelectedDate == null) {
            dtTo.SelectedDate = dtFrom.SelectedDate;
        }
        
        // Ensure the current key is this page's key.
        ScheduleHelper.Instance.MasterPageKey = hiddenPageKey.Value;
        ScheduleHelper.Instance.ViewPageKey = hiddenPageKey.Value;
        
        // Clear the cache for this key.
        // Create/Clear the cache.
        System.Web.HttpContext.Current.Session.Add(Serviceschedule.MASTER_SCHEDULE_CACHE + ScheduleHelper.Instance.MasterPageKey, "");
        System.Web.HttpContext.Current.Session.Add(Serviceschedule.VIEW_SCHEDULE_CACHE + ScheduleHelper.Instance.ViewPageKey, "");
        
        // Now, load up the results
        SetParams();
        DataView data = Serviceschedule.GetMasterSchedulesByDatesAndEquipOrServiceType(int.Parse(Profile.Customerid/*lstCustomerSelector.SelectedCustomerId*/),
            0, dtFrom.SelectedDate.Value.Date, dtTo.SelectedDate.Value.Date, chkFuture.Checked,
            Serviceschedule.GetServicesDataViewFilter(lstCustServices),
            Serviceschedule.GetEqTypesDataViewFilter(lstCustEqTypes));

        // Create the columns.
        CreateColumns(data);

        // Reset the tooltips.
        this.RadToolTipManager1.TargetControls.Clear();

        // Bind the data.
        this.RadToolTipManager1.TargetControls.Clear();
        RadGrid1.CurrentPageIndex = 0;
        RadGrid1.DataBind();
    }
    protected void SetParams() {
        ObjectDataSource1.SelectParameters["customerId"].DefaultValue = Profile.Customerid;// lstCustomerSelector.SelectedCustomerId;
        ObjectDataSource1.SelectParameters["equipmentId"].DefaultValue = "0";
        ObjectDataSource1.SelectParameters["dtFromDate"].DefaultValue = dtFrom.SelectedDate.Value.Date.ToShortDateString();
        ObjectDataSource1.SelectParameters["dtToDate"].DefaultValue = dtTo.SelectedDate.Value.Date.ToShortDateString();
        ObjectDataSource1.SelectParameters["futureOption"].DefaultValue = chkFuture.Checked.ToString();
        ObjectDataSource1.SelectParameters["services"].DefaultValue = Serviceschedule.GetServicesDataViewFilter(lstCustServices);
        ObjectDataSource1.SelectParameters["eqTypes"].DefaultValue = Serviceschedule.GetEqTypesDataViewFilter(lstCustEqTypes);
    }
    protected void RadGrid1_PageIndexChanged(object sender, GridPageChangedEventArgs e) {
        // Ensure the current key is this page's key.
        ScheduleHelper.Instance.MasterPageKey = hiddenPageKey.Value;
        ScheduleHelper.Instance.ViewPageKey = hiddenPageKey.Value;

        // Get current data.
        DataView data = Serviceschedule.GetMasterSchedulesByDatesAndEquipOrServiceType(int.Parse(Profile.Customerid/*lstCustomerSelector.SelectedCustomerId*/),
            0, dtFrom.SelectedDate.Value.Date, dtTo.SelectedDate.Value.Date, chkFuture.Checked,
            Serviceschedule.GetServicesDataViewFilter(lstCustServices),
            Serviceschedule.GetEqTypesDataViewFilter(lstCustEqTypes));

        // Re-Create the columns.
        CreateColumns(data);

        if (e != null) {
            RadGrid1.CurrentPageIndex = e.NewPageIndex;
        }

        // Reset the tooltips.
        this.RadToolTipManager1.TargetControls.Clear();
    }
    protected void RadGrid1_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e) {
        RadGrid1_PageIndexChanged(sender, null);
    }
    protected void RadGrid1_DataBound(object sender, EventArgs e) {
    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e) {
        // Now setup the tooltips.
        SetupToolTips(e.Item);
    }
    protected void RadGrid1_PreRender(object sender, EventArgs e) {
        // Relabel the date columns.        
        GridColumn[] cols = RadGrid1.MasterTableView.RenderColumns;
        for (int i = 0; i < cols.Length; i++) { 
            if (cols[i].HeaderText.Contains("col")) {
                int day = int.Parse(cols[i].HeaderText.Substring(3, cols[i].HeaderText.Length - 3));
                DateTime dt = dtFrom.SelectedDate.Value.AddDays(day);
                // Set the HeaderText
                cols[i].HeaderText = dt.ToShortDateString();
            } else if (cols[i].HeaderText.Contains("Taildesc")) {
                // Set the HeaderText
                cols[i].HeaderText = "Tail";
            } else if (cols[i].HeaderText.Contains("Tailnumber")) {
                // Hide the pk column.
                cols[i].Visible = false;
            } else if (cols[i].HeaderText.Contains("url")) {
                // Hide the url columns.
                cols[i].Visible = false;
            }
        }
    }
    protected GridHyperLinkColumn GetHyperlinkCol(string sDataField, string sUrlField, string sHdr) {
        GridHyperLinkColumn col = new GridHyperLinkColumn();
        col.DataNavigateUrlFields = new string[]{ sUrlField };
        col.DataTextField = sDataField;
        col.UniqueName = sDataField + "Column";
        col.Target = "_blank";
        col.HeaderText = sHdr;
        col.Text = "";
        col.HeaderTooltip = " ";
        return col;                      
    }
    protected GridBoundColumn GetBoundCol(string sDataField, string sHdr) {
        GridBoundColumn col = new GridBoundColumn();
        col.DataField = sDataField;
        col.UniqueName = sDataField + "Column";
        col.HeaderText = sHdr;
        col.HeaderTooltip = " ";
        return col;
    }
    protected void CreateColumns(DataView data) {
        RadGrid1.MasterTableView.Columns.Clear();
        RadGrid1.MasterTableView.Columns.Add(GetHyperlinkCol("Taildesc", "TaildescUrl", "Tail"));
        RadGrid1.MasterTableView.Columns.Add(GetBoundCol("Equipmenttypedescr", "Equip_Type"));
        foreach (DataColumn col in data.Table.Columns) {
            if (col.ColumnName.Contains("col")) {
                string colId = col.ColumnName.Substring(3, col.ColumnName.Length - 3);
                RadGrid1.MasterTableView.Columns.Add(GetHyperlinkCol(col.ColumnName, "url" + colId, col.Caption));
            }
        }
    }
    protected void AddExportRow() {
        // Get the data from the cache.
        DataView data = (DataView)System.Web.HttpContext.Current.Session[Serviceschedule.MASTER_SCHEDULE_CACHE + ScheduleHelper.Instance.MasterPageKey];
        // Add a row that looks like the header.
        DataRow row = data.Table.NewRow();
        for (int lcv = 0; lcv < data.Table.Columns.Count; lcv++) {
            if (data.Table.Columns[lcv].Caption.Contains("Taildesc")) {
                row[data.Table.Columns[lcv].ColumnName] = "Tail";
            } else if (data.Table.Columns[lcv].Caption.Contains("Tailnumber")) {
                row[data.Table.Columns[lcv].ColumnName] = "-1";
            } else if (data.Table.Columns[lcv].Caption.Contains("Equip")) {
                row[data.Table.Columns[lcv].ColumnName] = "Equip_Type";
            } else if (data.Table.Columns[lcv].ColumnName.Contains("url")) {
                // Blank the URL
                row[data.Table.Columns[lcv].ColumnName] = "";
            } else {
                // The Service Schedule Dates
                row[data.Table.Columns[lcv].ColumnName] = data.Table.Columns[lcv].Caption;
            }
        }
        data.Table.Rows.InsertAt(row, 0);
    }
    protected void btnExportToExcel_OnClick(object sender, EventArgs e) {
        if (RadGrid1.MasterTableView.Items.Count > 0) {
            // Ensure the current key is this page's key.
            ScheduleHelper.Instance.MasterPageKey = hiddenPageKey.Value;
            ScheduleHelper.Instance.ViewPageKey = hiddenPageKey.Value;
            // Get the data from the cache.
            DataView data = (DataView)System.Web.HttpContext.Current.Session[Serviceschedule.MASTER_SCHEDULE_CACHE + ScheduleHelper.Instance.MasterPageKey];
            // Add export header row.
            AddExportRow();
            // Create grid columns.
            CreateColumns(data);
            // Rebind grid
            RadGrid1.Rebind();
            // Export
            RadGrid1.MasterTableView.ExportToExcel();
            // Delete export header row.
            data.Table.Rows[0].Delete();
        }
    }
}