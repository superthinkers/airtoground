﻿<%@ Page Title="Master Schedule" EnableEventValidation="false" Language="C#" MasterPageFile="~/Customers/201619/Default.master"
    AutoEventWireup="true" CodeFile="ViewMasterSchedule.aspx.cs" Inherits="Customers_201619_Schedules_ViewMasterSchedule"
    Buffer="true" Strict="true" Explicit="true" AsyncTimeout="0" ValidateRequest="false" %>

<%@ Register TagPrefix="atg" TagName="ScheduleToolTip" Src="~/Customers/201619/Schedules/ScheduleToolTipControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
        SelectMethod="GetMasterSchedulesByDatesAndEquipOrServiceType" TypeName="ATGDB.Serviceschedule">
        <SelectParameters>
            <asp:Parameter Name="customerId" Type="Int32" />
            <asp:Parameter Name="equipmentId" Type="Int32" />
            <asp:Parameter Name="dtFromDate" Type="DateTime" />
            <asp:Parameter Name="dtToDate" Type="DateTime" />
            <asp:Parameter Name="futureOption" Type="Boolean" />
            <asp:Parameter Name="services" Type="String" />
            <asp:Parameter Name="eqTypes" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            /*
            For resizing to work:

            Pase in the two script functions. Modify the width / height settings as 
            necessary for offset1 and offset2.
                
            Then Set:

            RadGrid1.Height = null
            MasterTableView.Height = null

            RadAjaxManager.OnResponseEnd=DoGridResize

            Then Name the div_container "resizeDiv"

            Then Name the search criteria, or other "header block" to "headerDiv".
            */
            window.onload = function () {
                DoGridResize();
            }
            window.onresize = function () {
                DoGridResize();
            }
            function DoGridResize(sender, eventArgs) {
                // Handles to the various div tags.
                //var divMain = document.getElementById("div");
                var divResize = document.getElementById("resizeDiv");
                var divSiteHeader = document.getElementById("divSiteHeader");
                var divSiteMenu = document.getElementById("divSiteMenu");
                var divSiteFooter = document.getElementById("divSiteFooter");
                var divPageHeader = document.getElementById("headerDiv");

                // Calc the offsite height.
                var offset1 = divSiteHeader.clientHeight + divSiteMenu.clientHeight + 5.60 * divSiteFooter.clientHeight + divPageHeader.clientHeight;
                var offset2 = divSiteHeader.clientHeight + divSiteMenu.clientHeight + 1.75 * divSiteFooter.clientHeight + divPageHeader.clientHeight;

                // Get page window height.
                var windowHeight = window.innerHeight; // || document.documentElement.clientHeight;
                var windowWidth = window.innerWidth - 100;

                // Resize main div.
                //divMain.style.height = windowHeight + 100 + "px";

                // Resize the target div tag.
                divResize.style.height = windowHeight - offset2 + "px";
                divResize.style.width = windowWidth + "px";

                // Reset grid height.
                //var grid = document.getElementById("< %= RadGrid1.ClientID %>");
                var grid = $find("<%= RadGrid1.ClientID %>");
                if (grid) {
                    grid.get_element().style.height = windowHeight - offset1 + "px";
                    grid.repaint();
                    if (grid.MasterTableView) {
                        grid.MasterTableView.get_element().clientHeight = windowHeight - offset1 + "px";
                        grid.MasterTableView.get_element().clientWidth = windowWidth + "px";
                    }
                }
            }
            // **********************************
            // LOADING PANEL
            //var currentLoadingPanel = null;
            //var currentUpdatedControl = null;
            function DoRequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");

                if (args.get_eventTarget() == "<%= btnSearch.UniqueID %>") {
                    currentUpdatedControl = "<%= RadGrid1.ClientID %>";
                }
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }
            function DoResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
                DoGridResize();
            }
            // **********************************
            // Date Validation
            function ValidateDateRange(sender, e) {
                var dtFromDate = $find("<%= dtFrom.ClientID %>");
                var dtToDate = $find("<%= dtTo.ClientID %>");

                if ((dtFromDate.get_selectedDate() == null) && (dtToDate.get_selectedDate() != null)) {
                    alert("Please select the From/End Date");
                } else if ((dtFromDate.get_selectedDate() > dtToDate.get_selectedDate()) && (dtToDate.get_selectedDate() != null)) {
                    alert("The To/Start Date must be less than or equal to than the From/Begin Date");
                    dtFromDate.set_selectedDate(dtToDate.get_selectedDate());
                }
                return false;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="DoRequestStart" OnResponseEnd="DoResponseEnd" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadToolTipManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch"> 
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadToolTipManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="lstCustomerSelector">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lstCustServices" />
                    <telerik:AjaxUpdatedControl ControlID="lstCustEqTypes" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="true" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false">
    </telerik:RadWindowManager>
    <telerik:RadToolTipManager ID="RadToolTipManager1" runat="server" AnimationDuration="0"
        ShowDelay="1500" EnableShadow="false" HideDelay="200" Width="1000px"
        Modal="true" AutoTooltipify="false" AutoCloseDelay="0"
        HideEvent="LeaveTargetAndToolTip" RegisterWithScriptManager="true" RenderInPageRoot="true"
        Height="320px" RelativeTo="Element" Animation="Slide" Position="BottomCenter"
        ShowEvent="OnMouseOver" OnAjaxUpdate="OnAjaxUpdate" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="true"
        Skin="Office2007">
    </telerik:RadAjaxLoadingPanel>
    <asp:HiddenField ID="hiddenPageKey" runat="server" Value="" />
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Master Schedule Browser"></asp:Label>
        <br />
        <br />
        <div id="resizeDiv" class="div_container" style="width: 98%; position: relative; left: 1%; height: 100%">
            <div id="headerDiv" class="div_header">Master Schedule</div>
            <div style="position: relative; float: left; margin: 5px 5px 0px 10px; width: 100%">
                <div style="position: relative; float: left; width: 70%">
                    <fieldset>
                        <legend>Search Criteria</legend>
                        <table>
                            <tr>
                                <td style="width: 20%">
                                    <div class="lblInfo" style="position: relative; float: left; margin-left: 2px">
                                        From Date<br />
                                        <telerik:RadDatePicker ID="dtFrom" runat="server" Font-Names="Arial" MinDate="1/1/0001 12:00:00 AM"
                                            Width="98%">
                                            <ClientEvents OnDateSelected="ValidateDateRange" />
                                        </telerik:RadDatePicker>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="stdValidator"
                                            ErrorMessage="<br />Required" ControlToValidate="dtFrom"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </td>
                                <td style="width: 20%">
                                    <%--<atg:CustomerSelector ID="lstCustomerSelector" runat="server" VerticalLabelMode="true"
                                        AutoPostBack="true" OnOnSelectedIndexChanged="lstCustomerSelector_SelectedIndexChanged" />--%>
                                </td>
                                <td rowspan="3" style="width: 20%">
                                    <!-- Equipment Types -->
                                    <div>
                                        <asp:Label ID="lblHeader1" runat="server" SkinID="InfoLabel" Text="Aircraft Types"></asp:Label>
                                        <asp:Panel ID="pnlScroll1" runat="server" BorderWidth="0" ScrollBars="Auto" Width="100%"
                                            Height="101px">
                                            <telerik:RadListBox ID="lstCustEqTypes" runat="server" DataKeyField="Equipmenttypeid"
                                                BorderWidth="0" BorderColor="Transparent"
                                                DataValueField="Equipmenttypeid" DataTextField="Description" CssClass="lblSmall"
                                                CheckBoxes="true"
                                                SelectionMode="Multiple" Width="98%" EnableViewState="true">
                                            </telerik:RadListBox>
                                        </asp:Panel>
                                    </div>
                                </td>
                                <td rowspan="3" style="width: 40%">
                                    <!-- Services -->
                                    <div>
                                        <asp:Label ID="lblHeader2" runat="server" SkinID="InfoLabel" Text="Services"></asp:Label>
                                        <asp:Panel ID="pnlScroll2" runat="server" BorderWidth="0" ScrollBars="Auto" Width="100%"
                                            Height="101px">
                                            <telerik:RadListBox ID="lstCustServices" runat="server" DataKeyField="Serviceid"
                                                BorderWidth="0" BorderColor="Transparent"
                                                DataValueField="Serviceid" DataTextField="Description" CssClass="lblSmall" CheckBoxes="true"
                                                SelectionMode="Multiple" Width="98%" EnableViewState="true">
                                            </telerik:RadListBox>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%">
                                    <div class="lblInfo" style="position: relative; float: left">
                                        To Date (optional)<br />
                                        <telerik:RadDatePicker ID="dtTo" runat="server" Font-Names="Arial" MinDate="1/1/0001 12:00:00 AM"
                                            Width="98%">
                                            <ClientEvents OnDateSelected="ValidateDateRange" />
                                        </telerik:RadDatePicker>
                                    </div>
                                </td>
                                <td style="width: 20%">
                                    <asp:CheckBox ID="chkFuture" runat="server" Font-Size="10px" Text="Future Items?"
                                        Checked="false" Visible="true" />
                                    <!--<br />
                                <asp:Label ID="lblFutureWarning" runat="server" SkinID="SubscriptLabel"
                                    Text="(considerably slower)"></asp:Label>-->
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" style="width: 40%">
                                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_OnClick" Text="Search" />
                                    &nbsp;&nbsp;<asp:Button ID="btnExportToExcel" runat="server" OnClick="btnExportToExcel_OnClick"
                                        Text="Export to Excel" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="position: relative; float: left; width: 29%">
                    <fieldset>
                        <legend>Legend</legend>
                        <table>
                            <tr>
                                <td style="width: 40%">
                                    <div>
                                        <label class="legendMSCompleted">
                                            COMPLETED ITEMS</label>
                                        <br />
                                        <label class="legendMSCompletedEx">
                                            COMPLETED ITEMS W/EXCEPTION</label>
                                        <br />
                                        <label class="legendMSScheduled">
                                            SCHEDULED ITEMS</label>
                                        <br />
                                        <label class="legendMSCancelled">
                                            CANCELLED ITEMS</label>
                                        <br />
                                        <label class="legendMSFuture">
                                            FUTURE ITEMS</label>
                                        <br />
                                        <label class="legendMSScheduled" id="lblLate" runat="server">LATE ITEMS</label>
                                        <br />
                                    </div>
                                </td>
                                <td style="width: 59%">
                                    <asp:Panel ID="pnlScroll" runat="server" BorderWidth="0" ScrollBars="Auto" Width="98%"
                                        Height="101px">
                                        <telerik:RadListView ID="lstServiceDescr" runat="server"
                                            DataKeyNames="Serviceshortdescrid" Width="100%">
                                            <ItemTemplate>
                                                <table width="98%">
                                                    <tr>
                                                        <td style="text-align: right; width: 30px">
                                                            <asp:Label ID="lblKey" runat="server" SkinID="SmallLabelBold" Text='<%# Eval("Description") %>'></asp:Label>
                                                        </td>
                                                        <td style="text-align: left">
                                                            <asp:Label ID="lblDescription" runat="server" SkinID="SmallLabel" Text='<%# Eval("Service.Description") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </telerik:RadListView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
            <div style="position: relative; float: left; margin-top: 5px; width: 100%">
                <telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="ObjectDataSource1" EnableViewState="false"
                    AutoGenerateColumns="false" Height="465px" Width="100%" OnPreRender="RadGrid1_PreRender"
                    OnDataBound="RadGrid1_DataBound" OnItemDataBound="RadGrid1_ItemDataBound"
                    OnPageIndexChanged="RadGrid1_PageIndexChanged" OnPageSizeChanged="RadGrid1_PageSizeChanged">
                    <MasterTableView Name="MainView" runat="server" DataSourceID="ObjectDataSource1"
                        AutoGenerateColumns="false" RowIndicatorColumn-Display="false"
                        GridLines="Both" CellPadding="0" CellSpacing="0"
                        DataKeyNames="Tailnumber" Font-Names="Arial"
                        Font-Size="10px" HorizontalAlign="Left" AllowSorting="false" AllowFilteringByColumn="false"
                        CommandItemDisplay="None" AllowPaging="true" PageSize="14">
                        <AlternatingItemStyle BackColor="Silver" HorizontalAlign="Center" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="ssHeader" />
                        <ItemStyle HorizontalAlign="Center" />
                        <PagerStyle AlwaysVisible="true" Mode="NumericPages" HorizontalAlign="Left"
                            Position="Bottom" Width="35%" />
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="false">
                        <Selecting AllowRowSelect="true" />
                        <Scrolling AllowScroll="true" SaveScrollPosition="true" UseStaticHeaders="true" FrozenColumnsCount="2" />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
</asp:Content>
