﻿using System;
using System.Web.Security;
using System.Collections.Generic;
using System.Security;

public partial class Customers_201619_Default : System.Web.UI.Page
{
    protected void Page_LoadComplete(object sender, EventArgs e) {
        // Populate the login controls.
        System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)PageUtils.FindControlRecursive(this, "RememberMe");
        System.Web.UI.WebControls.TextBox txtPassword = (System.Web.UI.WebControls.TextBox)PageUtils.FindControlRecursive(this, "Password");
        Login1.UserName = SecurityUtils.GetLoginUserName(chk.Checked);
        txtPassword.Attributes.Add("value", SecurityUtils.GetLoginPassWord(chk.Checked));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // Hook LoadComplete event.
        Page.LoadComplete += Page_LoadComplete;
            
        if (!Page.IsPostBack) {
        }

        // Footer.
        txtFooter.Text = Resources.Resource.WebSiteFooterText;
    }
    protected void ResetProviderRoot() {
        // This is the CUSTOMER
        SecurityUtils.SetATGMode(SecurityUtils.ATG_MODE_CUSTOMER);

        // Set the login's membership provider here.
        string providerRoot = "201619";
        SecurityHelper.Instance.ProviderRoot = providerRoot;
    }
    protected void Login1_LoggingIn(object sender, System.Web.UI.WebControls.LoginCancelEventArgs e) {
        // Set up some application classes. NO SESSION YET.
        ResetProviderRoot();  
    }
    protected void Login1_OnLoggedIn(object sender, EventArgs e) {
        // Set up some application classes. HAVE SESSION
        ResetProviderRoot();

        // You must have an administrator.
        //MembershipHelper.CreateAdministrator();

        // Create the "remember me" cookie.
        System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)PageUtils.FindControlRecursive(this, "RememberMe");
        SecurityUtils.SetLoginCookie(chk.Checked, Login1.UserName, Login1.Password);

        // Initialize the profile.
        var needsSave = false;
        Profile.Initialize(Login1.UserName, true);

        // Update profile in case the provider root gets messed up.
        Profile.ProviderRoot = "201619";
        Profile.SiteTheme = "201619";
        Profile.Save();

        // Utility method to fix all profile userids.
        //MembershipHelper.UpdateAllUserIds(Profile);

        // This is a security check to prevent a user from another site (UPS, FedEx) from logging into this site.
        if (!String.IsNullOrEmpty(Profile.ProviderRoot) && (Profile.ProviderRoot != SecurityHelper.Instance.ProviderRoot)) {
            // Someone is logging into the wrong site with the wrong credentials.
            // No exceptions, signout.
            //FormsAuthentication.GetAuthCookie("", true).Expires = DateTime.Now;
            FormsAuthentication.SignOut();
            // Redirect to customer site.
            Response.Redirect("~/Customers/201619/Default.aspx", false);
            return;
        } else if (String.IsNullOrEmpty(Profile.ProviderRoot)) {
            needsSave = true;
            Profile.ProviderRoot = SecurityHelper.Instance.ProviderRoot;
        }

        // Update the site theme. Stop-gap measure to prevent possible problems.
        if (Profile.SiteTheme != SecurityUtils.GetSiteThemeFromProviderRoot(SecurityHelper.Instance.ProviderRoot)) {
            needsSave = true;
            Profile.SiteTheme = SecurityUtils.GetSiteThemeFromProviderRoot(SecurityHelper.Instance.ProviderRoot);
        }
         
        // Make sure there is a default company and customer, if possible.
        if (String.IsNullOrEmpty(Profile.Companydivisionid) || String.IsNullOrEmpty(Profile.Customerid)) {
            List<ATGDB.Companycustomeruserjoin> data = ATGDB.Companydivision.GetCompanyCustomerUserJoins(Profile.Userid, true);
            if (data.Count > 0) {
                // They have one or more assignments, so set it to the one at the top of the list.
                Profile.Companydivisionid = data[0].Companydivisionid.ToString();
                Profile.Companydivisiondescription = data[0].Companydivision.Description;
                Profile.Customerid = data[0].Customerid.ToString();
                Profile.Customerdescription = data[0].Customer.Description;
            } else {
                Profile.Customerid = "2"; // UPS
                Profile.Customerdescription = "United Parcel Service (UPS)";
                Profile.Companydivisionid = "5"; // LOUISVILLE
                Profile.Companydivisiondescription = "Louisville (SDF)";
                Profile.Save();

                // Users must have a default division and customer. Administrators can always log in.
                //if (Profile.UserName != "Administrator") {
                //    if (needsSave == true) {
                //        Profile.Save();
                //    }
                //    // No exceptions, signout.
                //    FormsAuthentication.SignOut();
                //    // Send to error page.
                //    Response.Redirect("~/Customers/201619/Security/NoDefaults.aspx", false);
                //    return;
                //}
            }
        }

        // Update the profile
        if (needsSave == true) {
            Profile.Save();
        }

        // All users of this location will have the CORPORATE ROLE.
        if (SecurityUtils.GetATGMode() == SecurityUtils.ATG_MODE_CORP) {
            SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_CORP, Profile.UserName);
            ProviderUtils.RemoveUsersFromRoles(new string[] { Profile.UserName }, new string[] { SecurityUtils.ROLE_CUSTOMER });
        } else {
            SecurityUtils.AutoCreateSecurityRoleAndOptionallyAssignToUser(SecurityUtils.ROLE_CUSTOMER, Profile.UserName);
            ProviderUtils.RemoveUsersFromRoles(new string[] { Profile.UserName }, new string[] { SecurityUtils.ROLE_CORP });
        }

        // If the user has not set their question/answer, make them do that.
        MembershipUser u = ProviderUtils.GetUser(Profile.UserName);
        if ((u != null) && (u.PasswordQuestion == MembershipHelper.NO_QUESTION)) {
            // Users must have a security question.
            // Send to error page.
            Response.Redirect("~/Customers/201619/Security/FirstLoginSecurityQuestion.aspx", false);
            return;
        }

        // Finish up the audit trails, clear the cookies.
        var profile = (ProfileCommon)System.Web.HttpContext.Current.Profile;
        if (profile != null) {
            // Audit Trail - close last entry.
            AuditUtils.AuditTrail.CloseOpenLogs(Profile.Userid);
        }
    }
}
