﻿using System;
using Telerik.Web.UI;
using ATGDB;

public partial class Customers_201619_Default_Master : System.Web.UI.MasterPage
{
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = "201619";
    }
    protected void Page_LoadComplete(object sender, EventArgs e) {
        // Update RadGrid.ImagePaths
        // Omit the ServiceQueue2 Page.
        if (!(Page.AppRelativeVirtualPath.Contains("ServiceQueue2"))) {
            PageUtils.SetRadGridImagePathAllControls(this, Page.Theme);
        }
        // Audit Trail
        if (Page is IAuditTrailPage) {
            AuditUtils.AuditTrail.Log(Profile.Userid, ((IAuditTrailPage)Page).GetAuditPageName());
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // Hook LoadComplete event.
        Page.LoadComplete += Page_LoadComplete;

        // Dynamically register the PULSE SCRIPT
        // All pages with this master will keep the session alive.
        PageUtils.RegisterSessionTimeoutPulse(Page);

        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManagerMasterPage);

        if (!Page.IsPostBack) {
            // Reinitialize profile if necessary.
            if (Profile.IsAnonymous == true) {
                throw new Exception("Default.master.cs Profile is Anonymous");
            }

            // Menu Setup
            if (SecurityUtils.GetATGMode() == SecurityUtils.ATG_MODE_CUSTOMER) {
                if (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN) ||
                    ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_CODETABLES) ||
                    ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_USERSECURITY)) {
                    RadMenu menu = (RadMenu)Page.Master.FindControl("mnuMain");
                    if (menu != null) {
                        RadMenuItem menuitem = new RadMenuItem("Administration");
                        menu.Items.Add(menuitem);
                        // Subitems          
                        if (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_CODETABLES) ||
                            ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN)) {
                            RadMenuItem subitem;
                            if ((ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_USERSECURITY)) ||
                                ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN)) {
                                subitem = new RadMenuItem("Users and User Roles", "~/Customers/201619/Admin/ManageUserRoles.aspx");
                                subitem.ImageUrl = "~/App_Themes/201619/Images/Users.bmp";
                                menuitem.Items.Add(subitem);
                            }
                        }
                    }
                }

                // Reports.
                RadMenu menu2 = (RadMenu)Page.Master.FindControl("mnuMain");

                // Equipment Reports
                RadMenuItem rpt = menu2.FindItemByValue("_ReportEquip");
                if (rpt != null) {
                    rpt.Visible = (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_EQUIP) ||
                        ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN));
                }

                // Audit Trails
                rpt = menu2.FindItemByValue("_ReportAuditing");
                if (rpt != null) {
                    rpt.Visible = ((SecurityUtils.GetATGMode() != SecurityUtils.ATG_MODE_CUSTOMER) &&
                        ((Profile.UserName.ToUpper() == "ADMINISTRATOR") || (Profile.UserName.ToUpper() == "CNILEST")));
                }
            }

            // Footer.
            txtFooter.Text = Resources.Resource.WebSiteFooterText;
        }
    }    
}
