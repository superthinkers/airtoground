﻿<%@ WebHandler Language="C#" Class="KeepSessionAlive" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Configuration;
using System.Net;
using System.IO;
using System.Net.Mail;
using System.Linq;

public class KeepSessionAlive : IHttpHandler, IRequiresSessionState
{
    public void ProcessRequest(HttpContext context) {
        // Don't use the passed context. Use the current one.
        HttpContext.Current.Session.Add("KeepSessionAlive", DateTime.Now);
        
        // Start counter at zero
        if ((HttpContext.Current.Session["KeepSessionAliveCounter"] == null) ||
            (HttpContext.Current.Session["KeepSessionAliveCounter"].ToString() == "")) {
            HttpContext.Current.Session.Add("KeepSessionAliveCounter", "1");
        } else {
            // Increment the counter.
            HttpContext.Current.Session.Add("KeepSessionAliveCounter", 
                (int.Parse(HttpContext.Current.Session["KeepSessionAliveCounter"].ToString()) + 1).ToString());
        }
        
        // Pulse in milliseconds
        int pulse = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["SESSION_PULSE"].ToString());

        // Session timeout in minutes.
        int timeout = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["SESSION_TIMEOUT"].ToString());
           
        // Current pulse count.
        int pulseCount = int.Parse(HttpContext.Current.Session["KeepSessionAliveCounter"].ToString());
             
        // If the timeout is reached, logout and display the session timeout screen.
        if (pulse * pulseCount >= timeout * 60 * 1000) {
            HttpContext.Current.Session.Add("KeepSessionAliveCounter", "");

            context.Response.Clear();
            context.Response.Write("SessionTimedOut"); // This value used in PageUtils jQuery call KeepSessionAlive()
        }    
    }

    public bool IsReusable {
        get {
            return false;
        }
    }
}