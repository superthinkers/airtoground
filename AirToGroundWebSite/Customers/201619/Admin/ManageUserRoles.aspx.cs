﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;   
using System.Linq;
using Telerik.Web.UI;

public partial class Customers_201619_Admin_ManageUserRoles : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_USER_SECURITY;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            // Security.
            if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_USERS)) {
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
            }

            // Rate Columns.
            if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EMPLOYEE_RATE)) {
                RadGrid1.Columns.FindByUniqueName("SalaryamountColumn").Visible = false;
                RadGrid1.Columns.FindByUniqueName("HourlyrateColumn").Visible = false;
                RadGrid1.Columns.FindByUniqueName("Useca24hourruleColumn").Visible = false;
            }
        }
    }
    protected void RadGrid1_SelectedIndexChanged(object source, EventArgs e) {
        if (RadGrid1.SelectedValues != null) {
            btnResetPassword.Text = "Reset Password: " + RadGrid1.SelectedValues["UserName"].ToString();
            btnResetPassword.Enabled = !RadGrid1.SelectedValues["UserName"].ToString().ToUpper().Contains("ADMIN");
        } else {
            btnResetPassword.Text = "";
            btnResetPassword.Enabled = false;
        }
    }
    protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("UserName").ToString();
        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;

            if ((e.Exception.InnerException != null) &&
               (e.Exception.InnerException.InnerException != null) &&
               (e.Exception.InnerException.InnerException.Message.Contains("Duplicate entry"))) {
                SetMessage("User " + id + " cannot be updated. Reason: Duplicate Employee Number or User name");
            } else {
                SetMessage("User " + id + " cannot be updated. Reason: " + e.Exception.InnerException.Message);
            }
            DisplayMessage(gridMessage);
        } else {
            SetMessage("User " + id + " is updated!");
        }
    }
    protected void RadGrid1_ItemInserted(object source, Telerik.Web.UI.GridInsertedEventArgs e) {
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("User cannot be inserted. Reason: " + e.Exception.InnerException.Message);
            DisplayMessage(gridMessage);
        } else {
            SetMessage("New User is created!");
        }
    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e) {
        if ((e.Item is GridDataItem) && (e.Item.IsInEditMode == false)) {
            GridDataItem dataItem = e.Item as GridDataItem;
            int col1Idx = dataItem.OwnerTableView.GetColumn("UserNameColumn2").OrderIndex;
            TableCell cell1 = dataItem.Cells[col1Idx];
            if (!String.IsNullOrEmpty(((Label)cell1.Controls[1]).Text)) {
                // Edit col
                int col2Idx = dataItem.OwnerTableView.GetColumn("EditCommandColumn").OrderIndex;
                TableCell cell2 = dataItem.Cells[col2Idx];
                // Delete col
                int col3Idx = dataItem.OwnerTableView.GetColumn("DeleteCommandColumn").OrderIndex;
                TableCell cell3 = dataItem.Cells[col3Idx];
                if ((((Label)cell1.Controls[1]).Text.ToUpper().Contains("ADMIN")) &&
                     (!Profile.UserName.ToUpper().Contains("ADMIN"))) {
                    cell2.Text = "&nbsp;";
                    cell3.Text = "&nbsp;";
                }
            }
        }
    }
    protected void RadGrid1_DataBound(object sender, EventArgs e) {
        if (!string.IsNullOrEmpty(gridMessage)) {
            DisplayMessage(gridMessage);
        }
        if ((RadGrid1.SelectedValue == null) || (RadGrid1.SelectedValue.ToString() == "")) {
            if (RadGrid1.Items.Count > 0) {
                RadGrid1.Items[0].Selected = true;
                btnResetPassword.Text = "Reset Password: " + RadGrid1.SelectedValues["UserName"].ToString();
                btnResetPassword.Enabled = !RadGrid1.SelectedValues["UserName"].ToString().ToUpper().Contains("ADMIN");
            }
        }
        // Must refresh these - when filtering cols needs refresh.
        RadGrid1_SelectedIndexChanged(null, null);
    }
    private string gridMessage = null;
    private void DisplayMessage(string text) {
        gridMessage = "";
        RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "RadGrid1");
        grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage(string message) {
        gridMessage = message;
    }
    protected void ReqEmployeeNumber_Validate(object sender, ServerValidateEventArgs e) {
        // Employee Number is REQUIRED and must be UNIQUE
        RadTextBox txtEmpNum = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1, "txtEmployeeNumber");
        //CustomValidator reqEmpNum = (CustomValidator)PageUtils.FindControlRecursive(RadGrid1, "ReqEmployeeNumber");
        string sEmpNum = txtEmpNum.Text == null ? "" : txtEmpNum.Text.Trim();
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        var data = from exu in dc.Extendedusers
                   where exu.Employeenumber == sEmpNum.Trim()
                   select exu;
        e.IsValid = ((sEmpNum != "") && (data.Count() == 0));
    }
    protected void ReqPassword2_OnServerValidate(object sender, ServerValidateEventArgs e) {
        e.IsValid = e.Value != null && e.Value.Length >= 8;
    }
    protected void btnResetPassword_Click(object sender, EventArgs e) {
        string un = RadGrid1.SelectedValues["UserName"].ToString();
        if ((un != null) && (un != "")) {
            if (!un.ToUpper().Contains("ADMIN")) {
                //MembershipUser u = ProviderUtils.GetUser(un);
                try {
                    MembershipHelper.ResetPassword(Server, un, Profile.UserName, "");
                    RadAjaxManager1.ResponseScripts.Add(@"radalert('Password Reset and Email Sent to: " + un + "', 330, 110);");
                } catch (Exception ex) {
                    throw ex;
                }
            } else {
                RadWindowManager1.RadAlert("You May Not Reset an Administrator's Password", null, null, "Alert", "");
            }
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e) {
        GridDataItem item = (sender as ImageButton).NamingContainer as GridDataItem;
        string userName = ((Label)item["UserNameColumn2"].Controls[1]).Text;
        if ((userName != "Administrator") && (userName != "&nbsp;")) {
            MembershipHelper.DeleteUser(userName);
            ObjectDataSource1.DataBind();
            RadGrid1.DataBind();
        }
    }
}