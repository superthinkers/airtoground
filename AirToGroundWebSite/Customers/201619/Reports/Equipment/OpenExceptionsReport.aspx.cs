﻿using System;
using System.Linq;

public partial class Customers_201619_Reports_OpenExceptionsReport : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_REPORT_OPEN_EXCEPTIONS;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            //dtFrom.SelectedDate = DateTime.Now.Date;
            //dtTo.SelectedDate = DateTime.Now.Date;
        }
    }
    protected void btnRunReport_Click(object sender, EventArgs e) {
        // Run the report.
        Telerik.Reporting.InstanceReportSource rs = new Telerik.Reporting.InstanceReportSource();
        rs.ReportDocument = new OpenExceptions(int.Parse(Profile.Customerid),//lstCustomerSelector.SelectedCustomerId),
            Profile.Customerdescription,//lstCustomerSelector.SelectedCustomerDescr,
            int.Parse(lstServiceCategories.SelectedValue),
            txtTailNumber.Text, Profile.UserName);
        ReportViewer1.ReportSource = rs;
    }
}