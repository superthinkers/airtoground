﻿using System;

public partial class Customers_201619_Reports_CustomerEquipmentListReport : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_REPORT_AIRCRAFT_LIST;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
        }
    }
    protected void btnRunReport_Click(object sender, EventArgs e) {
        // Run the report.
        Telerik.Reporting.InstanceReportSource rs = new Telerik.Reporting.InstanceReportSource();
        rs.ReportDocument = new CustomerEquipmentList(int.Parse(Profile.Customerid),//lstCustomerSelector.SelectedCustomerId),
            Profile.Customerdescription, Profile.UserName);//lstCustomerSelector.SelectedCustomerDescr, Profile.UserName);
        ReportViewer1.ReportSource = rs;
    }
}