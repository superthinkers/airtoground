﻿using System;

public partial class Customers_201619_Reports_Audit_UserAuditTrailsReport : System.Web.UI.Page, IAuditTrailPage 
{   
    public string GetAuditPageName() {
        return AuditUtils.PAGE_AUDIT_TRAILS;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            dtFrom.SelectedDate = DateTime.Now.Date.AddDays(-30); // Past 30 days.
            dtTo.SelectedDate = DateTime.Now.Date;
        }
    }
    protected void btnRunReport_Click(object sender, EventArgs e) {
        // Run the report.
        Telerik.Reporting.InstanceReportSource rs = new Telerik.Reporting.InstanceReportSource();
        rs.ReportDocument = new UserAuditTrails(Profile.UserName, lstEmployees.SelectedValue, lstEmployees.Text, dtFrom.SelectedDate.Value, dtTo.SelectedDate.Value);
        ReportViewer1.ReportSource = rs;
    }
}