﻿<%@ Page ViewStateMode="Disabled" Title="" Language="C#" MasterPageFile="~/Customers/201619/Default.master"
    AutoEventWireup="true" CodeFile="ReportsHome.aspx.cs" Inherits="Customers_201619_Reports_ReportsHome"
    Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Reporting and Analysis"></asp:Label>
        <br />
        <br />
        <div style="position:relative; left:10%; width:80%">
            <!-- EQUIPMENT -->
            <div class="div_container" style="width: 25%;">
                <div class="div_header">
                    Aircraft</div>
                <asp:Label ID="lblEquipmentSecurity" runat="server" SkinID="SmallLabel" Text="<br />You Must Have the &quot;Run Aircraft Reports&quot; Security Role to Run These Reports"
                    Visible="false"></asp:Label>
                <div id="divEquipment" runat="server" style="text-align: left; position: relative;
                    margin: 10px 0px 0px 10px; width: 98%">
                    <table width="97%">
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink39" runat="server" SkinID="ImageReport" Width="32px"
                                    Height="32px" NavigateUrl="~/Reports/Equipment/OpenExceptionsReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink40" runat="server" NavigateUrl="~/Reports/Equipment/OpenExceptionsReport.aspx"
                                    Text="Open Exceptions Report"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink41" runat="server" SkinID="ImageReport" Width="32px"
                                    Height="32px" NavigateUrl="~/Reports/Equipment/CustomerEquipmentListReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink42" runat="server" NavigateUrl="~/Reports/Equipment/CustomerEquipmentListReport.aspx"
                                    Text="Aircraft List"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink51" runat="server" SkinID="ImageReport" Width="32px"
                                    Height="32px" NavigateUrl="~/Reports/Equipment/CustomerEquipmentStatusListReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink52" runat="server" NavigateUrl="~/Reports/Equipment/CustomerEquipmentStatusListReport.aspx"
                                    Text="Aircraft Status List"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink43" runat="server" SkinID="ImageReport" Width="32px"
                                    Height="32px" NavigateUrl="~/Reports/Equipment/ServiceRollupReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink44" runat="server" NavigateUrl="~/Reports/Equipment/ServiceRollupReport.aspx"
                                    Text="Service Rollup Report"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink11" runat="server" SkinID="ImageReport" Width="32px"
                                    Height="32px" NavigateUrl="~/Reports/Equipment/ServicePerformedReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink12" runat="server" NavigateUrl="~/Reports/Equipment/ServicePerformedReport.aspx"
                                    Text="Service Performed Report"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- WORK -->
            <div class="div_container" style="width: 25%;">
                <div class="div_header">Work</div>
                <asp:Label ID="lblWorkSecurity" runat="server" SkinID="SmallLabel" Text="<br />You Must Have the &quot;Run Work Reports&quot; Security Role to Run These Reports"
                    Visible="false"></asp:Label>
                <div id="divWork" runat="server" style="text-align: left; position: relative; margin: 10px 0px 0px 10px; width: 98%">
                    <table width="97%">
                        <%--<tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink19" runat="server" SkinID="ImageReport" Width="32px"
                                    Height="32px" NavigateUrl="~/Reports/MasterScheduleReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink20" runat="server" NavigateUrl="~/Reports/MasterScheduleReport.aspx"
                                    Text="Master Schedule"></asp:HyperLink>
                            </td>
                        </tr>--%>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink29" runat="server" SkinID="ImageReport" Width="32px"
                                    Height="32px" NavigateUrl="~/Reports/Work/WorkAgingReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink30" runat="server" NavigateUrl="~/Reports/Work/WorkAgingReport.aspx"
                                    Text="Work Aging Report"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink31" runat="server" SkinID="ImageReport" Width="32px"
                                    Height="32px" NavigateUrl="~/Reports/Work/EndOfDayRapSheetReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink32" runat="server" NavigateUrl="~/Reports/Work/EndOfDayRapSheetReport.aspx"
                                    Text="Daily Wrap-up Sheet"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink3" runat="server" SkinID="ImageReport" Width="32px" Height="32px"
                                    NavigateUrl="~/Reports/Work/EndOfDayRapSheetSummaryReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Reports/Work/EndOfDayRapSheetSummaryReport.aspx"
                                    Text="Daily Wrap-up Sheet Summary"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink35" runat="server" SkinID="ImageReport" Width="32px"
                                    Height="32px" NavigateUrl="~/Reports/Work/DailyWorkSheetReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink36" runat="server" NavigateUrl="~/Reports/Work/DailyWorkSheetReport.aspx"
                                    Text="Daily Work Sheet"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- REVENUE -->
            <div class="div_container" style="width: 25%;">
                <div class="div_header">
                    Revenue</div>
                <asp:Label ID="lblRevenueSecurity" runat="server" SkinID="SmallLabel" Text="<br />You Must Have the &quot;Run Revenue Reports&quot; Security Role to Run These Reports"
                    Visible="false"></asp:Label>
                <div id="divRevenue" runat="server" style="text-align: left; position: relative;
                    margin: 10px 0px 0px 10px; width: 98%">
                    <table width="97%">
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink4" runat="server" SkinID="ImageReport" Width="32px" Height="32px"
                                    NavigateUrl="~/Reports/Revenue/CustomerWorkReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="~/Reports/Revenue/CustomerWorkReport.aspx"
                                    Text="Work Revenue Detail Summary"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink1" runat="server" SkinID="ImageReport" Width="32px" Height="32px"
                                    NavigateUrl="~/Reports/Revenue/ServiceLocationReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Reports/Revenue/ServiceLocationReport.aspx"
                                    Text="Service - Location Report"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- JOB / TIMESHEET -->
            <div class="div_container" style="width: 25%;">
                <div class="div_header">Job</div>
                <asp:Label ID="lblJobSecurity" runat="server" SkinID="SmallLabel" Text="<br />You Must Have the &quot;Run Job Reports&quot; Security Role to Run These Reports"
                    Visible="false"></asp:Label>
                <div id="divJob" runat="server" style="text-align: left; position: relative; margin: 10px 0px 0px 10px;
                    width: 98%">
                    <table width="97%">
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkTimeSheetCost1" runat="server" SkinID="ImageReport" Width="32px" Height="32px"
                                    NavigateUrl="~/Reports/Job/TSCostDDReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkTimeSheetCost2" runat="server" NavigateUrl="~/Reports/Job/TSCostDDReport.aspx"
                                    Text="Month TS Cost By Job"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkTimeSheetHours1" runat="server" SkinID="ImageReport" Width="32px" Height="32px"
                                    NavigateUrl="~/Reports/Job/TSHoursDDReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkTimeSheetHours2" runat="server" NavigateUrl="~/Reports/Job/TSHoursDDReport.aspx"
                                    Text="Month TS Hours By Job"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkWLRevenue1" runat="server" SkinID="ImageReport" Width="32px"
                                    Height="32px" NavigateUrl="~/Reports/Job/WLRevDDReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkWLRevenue2" runat="server" NavigateUrl="~/Reports/Job/WLRevDDReport.aspx"
                                    Text="Month WL Revenue By Job"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkWLHours1" runat="server" SkinID="ImageReport" Width="32px" Height="32px"
                                    NavigateUrl="~/Reports/Job/WLHoursDDReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkWLHours2" runat="server" NavigateUrl="~/Reports/Job/WLHoursDDReport.aspx"
                                    Text="Month WL Hours By Job"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink6" runat="server" SkinID="ImageReport" Width="32px" Height="32px"
                                    NavigateUrl="~/Reports/Job/WorkCardWithDetailReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="~/Reports/Job/WorkCardWithDetailReport.aspx"
                                    Text="Work Card with Detail"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink9" runat="server" SkinID="ImageReport" Width="32px" Height="32px"
                                    NavigateUrl="~/Reports/Job/WorkCardWithLaborReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink10" runat="server" NavigateUrl="~/Reports/Job/WorkCardWithLaborReport.aspx"
                                    Text="Work Card Labor Report"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink13" runat="server" SkinID="ImageReport" Width="32px" Height="32px"
                                    NavigateUrl="~/Reports/Job/WorkCardBudgetedHoursReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink14" runat="server" NavigateUrl="~/Reports/Job/WorkCardBudgetedHoursReport.aspx"
                                    Text="Work Card Budgeted Hours Report"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="HyperLink15" runat="server" SkinID="ImageReport" Width="32px" Height="32px"
                                    NavigateUrl="~/Reports/Job/WorkCardBudgetedAmountReport.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink16" runat="server" NavigateUrl="~/Reports/Job/WorkCardBudgetedAmountReport.aspx"
                                    Text="Work Card Budgeted Amount Report"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
