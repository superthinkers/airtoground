﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="PasswordRecovery.aspx.cs"
    Inherits="Customers_201619_Security_PasswordRecovery" Buffer="true" Strict="true" Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
        .siteSplash2 
        {
	        height: 76px; 
	        top: 30px; 
	        background-image: url('../Images/SiteSplash.png');
            background-repeat: no-repeat;
        }

        .siteHeaderStub2 
        {
	        height: 76px; 
	        top: 30px; 
	        background-image: url('../Images/HeaderStub.png');
            background-repeat: repeat-x;
        }    
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" AsyncPostBackTimeout="600"
            SupportsPartialRendering="true" EnablePartialRendering="true"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <div class="siteHeaderStub2">
            <div class="siteSplash2">
                <br />
                <div id="div_siteDefault">
                    <br />
                    <br />                 
                    <div id="div_login" style="left: 32%; width: 35%; border-collapse:separate; margin: 2px 2px 2px 2px">
                        <asp:PasswordRecovery ID="PasswordRecovery1" runat="server" AnswerRequiredErrorMessage="<br />Required">
                            <QuestionTemplate>
                                <table cellpadding="1" cellspacing="0" style="border-collapse:separate; margin: 2px 2px 2px 2px">
                                    <tr>
                                        <td>
                                            <table cellpadding="0" style="border-collapse: separate; margin: 2px 2px 2px 2px">
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        <span class="lblTitle">Identity Confirmation</span></td>
                                                </tr>
                                                <tr><td colspan="2">&nbsp;</td></tr>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        Answer the following question to receive your password.</td>
                                                </tr>
                                                <tr><td colspan="2">
                                                    &nbsp;</td></tr>
                                                <tr>
                                                    <td align="right">
                                                        User Name:&nbsp;</td>
                                                    <td align="left">
                                                        <asp:Literal ID="UserName" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        Question:&nbsp;</td>
                                                    <td align="left">
                                                        <asp:Literal ID="Question" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        Answer:&nbsp;</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="Answer" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" CssClass="stdValidator"
                                                            ControlToValidate="Answer" ErrorMessage="<br />Required." ToolTip="Answer is required."
                                                            ValidationGroup="PasswordRecovery1">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2" style="color:Red;">
                                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        <br />
                                                        <asp:Button ID="SubmitButton" runat="server" CommandName="Submit" Text="Submit" 
                                                            ValidationGroup="PasswordRecovery1" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </QuestionTemplate>
                            <SuccessTemplate>
                                <table cellpadding="1" cellspacing="0" style="border-collapse:collapse;">
                                    <tr>
                                        <td>
                                            <table cellpadding="0">
                                                <tr>
                                                    <td>Your Password has been Sent to You.</td>
                                                </tr>
                                                <tr><td>
                                                    &nbsp;</td></tr>
                                                <tr>
                                                    <td>
                                                        <asp:HyperLink ID="lnkLogin" runat="server" NavigateUrl="~/Default.aspx" Text="Login"></asp:HyperLink></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>                                       
                                </table>
                            </SuccessTemplate>                        
                            <UserNameTemplate>
                                <table cellpadding="1" cellspacing="0" style="border-collapse:collapse;">
                                    <tr>
                                        <td>
                                            <table cellpadding="0">
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        <span>Enter your User Name to receive your password.</span>
                                                        <br />&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:&nbsp;</asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" CssClass="stdValidator"
                                                            ControlToValidate="UserName" ErrorMessage="<br />Required." ToolTip="User Name is required."
                                                            ValidationGroup="PasswordRecovery1">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2" style="color:Red;">
                                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                    <br />
                                                        <asp:Button ID="SubmitButton" runat="server" CommandName="Submit" Text="Submit" 
                                                            ValidationGroup="PasswordRecovery1" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </UserNameTemplate>                        
                        </asp:PasswordRecovery>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
