﻿using System;
using System.Linq;
using Telerik.Web.UI;

public partial class Customers_201619_Security_NoDefaults : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = "201619";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        RadMenu menu = (RadMenu)PageUtils.FindControlRecursive(Page.Master, "mnuMain");
        if (menu != null) {
            menu.Visible = false;
        }
    }
}