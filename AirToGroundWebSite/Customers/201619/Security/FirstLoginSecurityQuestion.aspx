﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/201619/Default.master" AutoEventWireup="true"
    CodeFile="FirstLoginSecurityQuestion.aspx.cs" Inherits="Customers_201619_Security_FirstLoginSecurityQuestion"
    Buffer="false" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <div id="div_main">
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Security Question and Answer</div>
            <br />
            <asp:Label ID="lblTitle" runat="server" SkinID="InfoLabel" Text="This is your First Login. Please Change Your Security Question and Answer"></asp:Label>
            <br />
            <br />
            <table width="100%">
                <tr>
                    <td style="text-align: right; width: 30%">
                        <asp:Label ID="Label6" runat="server" SkinID="InfoLabel" Text="Old Password"></asp:Label>
                    </td>
                    <td style="text-align: left; width: 70%">
                        <telerik:RadTextBox ID="txtOldPassword" runat="server" TextMode="Password">
                        </telerik:RadTextBox>
                        <asp:RequiredFieldValidator ID="reqTxtOldPassword" runat="server" CssClass="stdValidator"
                            ControlToValidate="txtOldPassword" Display="Dynamic" ErrorMessage="<br />Required"
                            Font-Size="10px"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:right; width:30%">
                        <asp:Label ID="Label4" runat="server" SkinID="InfoLabel" Text="New Password"></asp:Label>
                    </td>
                    <td style="text-align: left; width: 70%">
                        <telerik:RadTextBox ID="txtPassword" runat="server" TextMode="Password">
                        </telerik:RadTextBox>
                        <asp:RequiredFieldValidator ID="reqTxtPassword" runat="server" CssClass="stdValidator"
                            ControlToValidate="txtPassword" Display="Dynamic" ErrorMessage="<br />Required"
                            Font-Size="10px"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; width: 30%">
                        <asp:Label ID="Label5" runat="server" SkinID="InfoLabel" Text="Confirm Password"></asp:Label>
                    </td>
                    <td style="text-align: left; width: 70%">
                        <telerik:RadTextBox ID="txtConfirmPassword" runat="server" TextMode="Password">
                        </telerik:RadTextBox>
                        <%--<asp:RequiredFieldValidator ID="reqTxtConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                            Display="Dynamic" ErrorMessage="<br />Required" Font-Size="10px"></asp:RequiredFieldValidator>--%>
                        <asp:CustomValidator ID="reqCustPassword" runat="server" CssClass="stdValidator"
                            ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="<br />The New Password Must Match the Confirm Password"
                            OnServerValidate="reqCustPassword_ServerValidate" Font-Size="10px"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; width: 30%">
                        <asp:Label ID="Label1" runat="server" SkinID="InfoLabel" Text="Question"></asp:Label>
                    </td>
                    <td style="text-align: left; width: 70%">
                        <telerik:RadComboBox ID="lstQuestions" runat="server" Width="70%">
                            <Items>
                                <telerik:RadComboBoxItem Text="What is your Mother's maiden name?" Value="What is your Mother's maiden name?" />
                                <telerik:RadComboBoxItem Text="What was the color of your first car?" Value="What was the color of your first car?" />
                                <telerik:RadComboBoxItem Text="What is your best friend's first name?" Value="What is your best friend's first name?" />
                                <telerik:RadComboBoxItem Text="What is your first pet's name?" Value="What is your first pet's name?" />
                                <telerik:RadComboBoxItem Text="What is your favorite elementary school teacher's name?"
                                    Value="What is your favorite elementary school teacher's name?" />
                                <telerik:RadComboBoxItem Text="What was your favorite childhood toy?" Value="What was your favorite childhood toy?" />
                                <telerik:RadComboBoxItem Text="What is your favorite food?" Value="What is your favorite food?" />
                                <telerik:RadComboBoxItem Text="What is the name of your high school?" Value="What is the name of your high school?" />
                            </Items>
                        </telerik:RadComboBox>
                        <asp:RequiredFieldValidator ID="reqLstQuestions" runat="server" CssClass="stdValidator"
                            ControlToValidate="lstQuestions" Display="Dynamic" ErrorMessage="<br />Required"
                            Font-Size="10px"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; width: 30%">
                        <asp:Label ID="Label2" runat="server" SkinID="InfoLabel" Text="Answer"></asp:Label>
                    </td>
                    <td style="text-align: left; width: 70%">
                        <telerik:RadTextBox ID="txtAnswer" runat="server">
                        </telerik:RadTextBox>
                        <asp:RequiredFieldValidator ID="reqTxtAnswer" runat="server" CssClass="stdValidator"
                            ControlToValidate="txtAnswer" Display="Dynamic" ErrorMessage="<br />Required"
                            Font-Size="10px"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="Label3" runat="server" SkinID="SubscriptLabel" Text="*Be sure to remember your answer!"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblError" runat="server" SkinID="SmallLabel" Font-Size="10px" ForeColor="Red"
                            Text="" Width="60%"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="btnOk" runat="server" Text="Ok" OnClick="btnOk_Click" />
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <br />
            <br />
        </div>
    </div>
</asp:Content>

