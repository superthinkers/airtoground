﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PreScreenControl.ascx.cs" Inherits="Careers_PreScreenControl" %>
<!-- Pre-Screening Questions -->
<br />
<div class="div_container">
    <div class="div_header">
        Pre-Screening Questions</div>
    <div style="margin: 10px 10px 10px 10px">
        <br />
        <div class="lblTitle" style="position: relative; left: 20%; text-align: center; font-size: medium;
            width: 60%">
            Welcome to Air to Ground! Thank you for taking the time to fill out an online job
            application.
            <br />
            Please note that this information is strictly confidential. Be as complete and as
            accurate as you can. If your application is selected, we will contact you for more
            information.
        </div>
        <br />
        <table width="100%" style="line-height: 20px">
            <tr>
                <td class="lblTitle">
                    Today's Date:&nbsp;
                    <asp:Label ID="lblToday" runat="server" SkinID="TitleLabel" Text='<%# Bind("Createdate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    What is your name?
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 50%">
                        <td>
                            <telerik:RadTextBox ID="txtLastName" runat="server" MaxLength="30" Width="96%" Text='<%# Bind("Lastname") %>'>
                            </telerik:RadTextBox>
                            <br />
                            <asp:Label ID="lblLastName" runat="server" SkinID="SubscriptLabel" Text="Last Name"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtFirstName" runat="server" MaxLength="30" Width="96%" Text='<%# Bind("Firstname") %>'>
                            </telerik:RadTextBox>
                            <br />
                            <asp:Label ID="lblFirstName" runat="server" SkinID="SubscriptLabel" Text="First Name"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtMiddleName" runat="server" MaxLength="30" Width="96%"
                                Text='<%# Bind("Middlename") %>'>
                            </telerik:RadTextBox>
                            <br />
                            <asp:Label ID="lblMiddleName" runat="server" SkinID="SubscriptLabel" Text="Middle Name"></asp:Label>
                        </td>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    1) Are you currently employed?
                    <asp:RadioButton ID="chkCurrentlyEmployed1" runat="server" GroupName="CurEmp" Text="Yes"
                        Checked='<%# Bind("Iscurrentlyemployed") %>' />
                    <asp:RadioButton ID="chkCurrentlyEmployed2" runat="server" GroupName="CurEmp" Text="No"
                        Checked="false" />
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    2) If you are currently employed, are you looking for new employment or a 2nd job?
                    <asp:RadioButton ID="chkIsSecondJob1" runat="server" GroupName="Second" Text="This is New Employment"
                        Checked="false" />
                    <asp:RadioButton ID="chkIsSecondJob2" runat="server" GroupName="Second" Text="This is a 2nd Job"
                        Checked='<%# Bind("Issecondjob") %>' />
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    3) Can you work Monday - Saturday?
                    <asp:RadioButton ID="chkMonThruSat1" runat="server" GroupName="MonSat" Text="Yes"
                        Checked='<%# Bind("Canworkmonthrusat") %>' />
                    <asp:RadioButton ID="chkMonThruSat2" runat="server" GroupName="MonSat" Text="No"
                        Checked="false" />
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    4) Can you work First, Second, or Third Shifts?
                    <asp:CheckBox ID="chkFirst" runat="server" Text="First Shift?" Checked='<%# Bind("Canworkfirstshift") %>' />
                    <asp:CheckBox ID="chkSecond" runat="server" Text="Second Shift?" Checked='<%# Bind("Canworksecondshift") %>' />
                    <asp:CheckBox ID="chkThird" runat="server" Text="Third Shift?" Checked='<%# Bind("Canworkthirdshift") %>' />
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    5) What type of work experience do you have?
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtExperience" runat="server" Width="96%" MaxLength="255"
                        TextMode="MultiLine" Height="50" Rows="4" Text='<%# Bind("WorkExperience") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    6) Are you looking for Full or Part-time employment?
                    <telerik:RadComboBox ID="lstFullOrPT" runat="server" Width="150px" DataValueField="Fulltimeorparttime">
                        <Items>
                            <telerik:RadComboBoxItem Value="F" Text="Full-Time" />
                            <telerik:RadComboBoxItem Value="P" Text="Part-Time" />
                            <telerik:RadComboBoxItem Value="B" Text="Both" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    7) Do you attend any trade or technical schools?
                    <asp:RadioButton ID="chkAttendSchool1" runat="server" GroupName="Attend" Text="Yes"
                        Checked='<%# Bind("Isattendingschool") %>' />
                    <asp:RadioButton ID="chkAttendSchool2" runat="server" GroupName="Attend" Text="No"
                        Checked="false" />
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    8) If yes, what school and when do you attend classes?
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtSchools" runat="server" Width="96%" MaxLength="255" TextMode="MultiLine"
                        Height="50" Rows="4" Text='<%# Bind("Schoolsandschedule") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    9) Do you have any heavy equipment operating experience?
                    <asp:RadioButton ID="chkHeavyEquip1" runat="server" GroupName="Heavy" Text="Yes"
                        Checked='<%# Bind("Hasheavyequipexp") %>' />
                    <asp:RadioButton ID="chkHeavyEquip2" runat="server" GroupName="Heavy" Text="No" Checked="false" />
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    10) If yes, what types of equipment?
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtHeavyExp" runat="server" Width="96%" MaxLength="255" TextMode="MultiLine"
                        Height="50" Rows="4" Text='<%# Bind("Heavyequipexp") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    11) Do you have a valid driver's license?
                    <asp:RadioButton ID="chkValidLic1" runat="server" GroupName="Lic" Text="Yes" Checked='<%# Bind("Hasvaliddriverslicense") %>' />
                    <asp:RadioButton ID="chkValidLic2" runat="server" GroupName="Lic" Text="No" Checked="false" />
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    12) Do you have reliable transportation (i.e. own a car, bus, etc.)?
                    <asp:RadioButton ID="chkTrans1" runat="server" GroupName="Trans" Text="Yes" Checked='<%# Bind("Hastransportation") %>' />
                    <asp:RadioButton ID="chkTrans2" runat="server" GroupName="Trans" Text="No" Checked="false" />
                </td>
            </tr>
        </table>
    </div>
</div>
