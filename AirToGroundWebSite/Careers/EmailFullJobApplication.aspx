﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmailFullJobApplication.aspx.cs" Inherits="Careers_EmailFullJobApplication"  %>

<!DOCTYPE html>

<%@ Import Namespace="System.Text.RegularExpressions" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="cid:txtStyle" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
	        margin: 0px 0px 0px 0px;
	        padding: 0px 0px 0px 0px;
	        border-width: 0px;
	        border-collapse:collapse;
	        overflow:auto;
	        font-family: Arial, Helvetica, sans-serif;
	        color:#333399;
	        font-size: 12px;
        }
        p
        {
	        font-family: Arial, Helvetica, sans-serif;
	        color:#333399;
	        font-size: 12px;
        }           
        table
        {
	        border-width:0px;
	        border-collapse:collapse;
	        padding: 0px 0px 0px 0px;
        }
        .div_container
        {
            background-color: #F2F0F2; /* Light Silver */
            float:none;
	        display:block;
	        text-align:left;
	        margin: 2px 2px 2px 2px;
	        padding: 1px 1px 1px 1px;
	        border: 1px solid gray;
	        font-family: Arial, Helvetica, sans-serif;
	        font-size: 12px;
	        color: #333399;
        }
        .div_header
        {
	        font-family: Arial, Helvetica, sans-serif;
	        font-size: 12px;
	        font-weight:900;
	        text-align: center;
	        color: #333399;
	        border-collapse:collapse;
	        height:12px;
	        width:100%;
        }
        .lblInfoBold
        {
	        font-family: Arial, Helvetica, sans-serif;
	        color:#333399;
	        font-weight:900;
	        font-size: 12px;
        }
        .lblInfo
        {
	        font-family: Arial, Helvetica, sans-serif;
	        color:#333399;
	        font-size: 12px;
        }
        .lblSubscript
        {
        	font-family: Arial, Helvetica, sans-serif;
        	color:Gray;
    	    font-size: 10px;
        }
        .careersJobAppValidator
        {
            font-size: x-large;
            color: Red;
        }
        .careersLblTitle
        {
	        font-family: Arial, Helvetica, sans-serif;
	        color:#333399;
	        font-size: 12px;
	        font-weight:bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            // Stop the browser back button. This is dumb.
            $(function () {
                window.history.forward(1);
            });
        </script>
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" AsyncPostBackTimeout="600"
            SupportsPartialRendering="true" EnablePartialRendering="true"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin" ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <devart:DbLinqDataSource id="LinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
            EntityTypeName="ATGDB.Jobapplication" TableName="Jobapplications" Where="Guidkey == @id"
            EnableDelete="false" EnableUpdate="false" EnableInsert="false">
            <WhereParameters>               
                <asp:Parameter Name="id" Type="string" DefaultValue="0" />
            </WhereParameters>
        </devart:DbLinqDataSource>
        <div>
            <div id="div_main">
                <asp:Label CssClass="careersLblTitle" ID="careersLblTitle" Font-Size="Large" runat="server" 
                    Text="Air to Ground Online Job Application" Width="100%"></asp:Label>
                <br />
                <div id="divPrint" runat="server" class="lblInfo">
                    <a href="javascript:window.print();" class="lblInfo">Print</a> | <a href="javascript:window.close();" class="lblInfo">Close Window</a>
                </div>
                <br />
                <!-- Main Grid -->
                <asp:DetailsView ID="grdApplication" runat="server" DefaultMode="ReadOnly" DataSourceID="LinqDataSource1"
                    DataKeyNames="Jobapplicationid" AutoGenerateRows="False" Width="100%">
                    <Fields>
                        <asp:TemplateField ControlStyle-BorderColor="Silver">
                            <ItemTemplate>
                                <br />
                                <!-- FAA NOTES -->
                                <div class="lblInfoBold" style="text-align: left; margin: 10px 10px 10px 10px">
                                    <br />
                                    <div class="careersLblTitle" style="text-align: center; font-size: medium">
                                        AIR TO GROUND SERVICES, INC.<br />
                                        F.A.A. AND T.S.A. REGULATIONS
                                    </div>
                                    <br />
                                    <div class="careersLblTitle" style="text-align: center; border: 1px solid gray; font-weight: 900;
                                        font-size: medium">
                                        ATTENTION
                                    </div>
                                    <br />
                                    <div class="lblInfoBold" style="text-align: left; margin: 10px 10px 10px 10px">
                                        IN ORDER TO COMPLY WITH F.A.A. (Federal Aviation Administration) AND T.S.A. (Transportation
                                        Security Administration) REGULATIONS, AIR TO GROUND SERVICES, INC. IS REQUIRED TO
                                        OBTAIN THE FOLLOWING BACKGROUND INFORMATION:
                                        <ul>
                                            <li>PAST 10 YEARS OF EMPLOYMENT AND/OR EDUCATION HISTORY</li>
                                            <li>BACKGROUND CHECK (INCLUDING 10 YEAR FBI CRIMINAL CHECK AND FINGERPRINTING). PER
                                                F.A.A. AND T.S.A. REGULATIONS IF YOU HAVE BEEN <strong>CONVICTED OF A FELONY</strong>
                                                IN THE PAST 10 YEARS YOU ARE NOT ELIGIBLE TO WORK AT AIR TO GROUND SERVICES, INC.
                                            </li>
                                            <li>PER F.A.A. REGULATIONS (DOT 49 CFR PART 40 and FAA 14 CFR PART 120) YOU <strong>MUST</strong> SUBMIT TO
                                                A NON-DOT or DOT REGULATED TYPE DRUG TEST FOR PRE-EMPLOYMENT. YOU WILL BE TESTED
                                                FOR THE FOLLOWING SUBSTANCES: AMPHETAMINES, COCAINE, MARIJUANA, OPIATES AND PHENCYCLIDINE
                                                (PCP). AFTER EMPLOYED, ALL EMPLOYEES WILL BE SUBJECT TO NON-DOT OR DOT REGULATED
                                                DRUG AND/OR ALCOHOL TESTING: INCLUDING RANDOM, POST- ACCIDENT AND REASONABLE SUSPICION.</li>
                                            <li>YOU MUST BE 18 YEARS OF AGE OR OLDER.</li>
                                            <li>YOU MUST HAVE A VALID DRIVER'S LICENSE.</li>
                                        </ul>
                                        <div>
                                            <strong>Thank you for your interest in employment with our company. All applications
                                                will be reviewed, and only the selected applicants will be called in for an interview.
                                                Unfortunately, not every applicant will be called back. Please do not call to check
                                                on the status of your application. Thank you!
                                                <br />
                                                <br />
                                                Air to Ground Services, Inc.</strong>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <!-- DRUG SCREEN -->
                                <div class="lblInfoBold" style="text-align: left; margin: 10px 10px 10px 10px">
                                    <br />
                                    <div class="careersLblTitle" style="text-align: center; font-size: medium">
                                        AIR TO GROUND SERVICES, INC.<br />
                                        DRUG-FREE WORKPLACE POLICY
                                    </div>
                                    <br />
                                    <div class="careersLblTitle" style="text-align: center; border: 1px solid gray; font-weight: 900;
                                        font-size: medium">
                                        APPLICANT'S ACKNOWLEDGEMENT FORM FOR ALCOHOL AND DRUG SCREENING
                                    </div>
                                    <br />
                                    <div class="lblInfo" style="text-align: left; font-size: medium">
                                        The Omnibus Transportation employee Testing Act of 1991 requires drug and alcohol
                                        testing on all safety-sensitive positions of employees in the aviation, trucking,
                                        railroads, mass transit, pipelines, and other transportation industries. Air To
                                        Ground Services, Inc. is federally regulated by the DOT 49 CFR Part 40 and FAA 14
                                        CFR Part 120 drug and alcohol testing regulations.
                                        <br />
                                        <br />
                                        I understand it is the policy of Air To Ground Services, Inc. to conduct pre-employment
                                        DOT or NON-DOT regulated drug and/or alcohol tests of job applicants for the purpose
                                        of detecting drug and/or alcohol abuse, and that one of the requirements for consideration
                                        of employment with the company is the satisfactory passing of the drug and/or alcohol
                                        tests(s).
                                        <br />
                                        <br />
                                        For the purpose of being further considered for employment, I hereby understand that
                                        I will be submitted to DOT or NON-DOT regulated type drug and/or alcohol testing.
                                        I understand that favorable test results will not necessarily guarantee that I will
                                        be employed by Air To Ground Services, Inc.
                                        <br />
                                        <br />
                                        I hereby acknowledge that such type drug and/or alcohol testing will be conducted
                                        by an authorized collection facility to collect breath and/or urine sample from
                                        me and to conduct necessary medical tests to determine the presence of use of alcohol,
                                        drugs, or controlled substances. Further, I understand for the release of the test
                                        results and other medical information to authorized company management (Drug Program
                                        Coordinator) for appropriate review. I also understand that, if I refuse to acknowledge,
                                        I will be denied employment and may not reapply.
                                        <br />
                                        <br />
                                        If you agree, do the following and continue to the next step:
                                        <br />
                                        <br />
                                        <table width="75%">
                                            <tr>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtDrugPrintName" runat="server" Text='<%# Eval("Drugprintname") %>'
                                                        Width="300px">
                                                    </asp:Label>
                                                    <br />
                                                    <asp:Label ID="lblDrugPrintName" runat="server" CssClass="lblSubscript" Text="Name"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtDrugEmail" runat="server" Text='<%# Eval("Drugemail") %>'
                                                        Width="300px">
                                                    </asp:Label>
                                                    <br />
                                                    <asp:Label ID="lblDrugEmailAddress" runat="server" CssClass="lblSubscript" Text="Email Address"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="dtDrugDate" runat="server" Text='<%# Eval("Drugdate", "{0:d}") %>'
                                                        Width="100px">
                                                    </asp:Label>
                                                    <br />
                                                    <asp:Label ID="lblDrugDate" runat="server" CssClass="lblSubscript" Text="Date"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br />
                                <!-- PRE SCREEN -->
                                <div class="div_container">
                                    <div class="div_header">
                                        Pre-Screening Questions</div>
                                    <div style="margin: 10px 10px 10px 10px">
                                        <br />
                                        <div class="careersLblTitle" style="position: relative; left: 20%; text-align: center; font-size: medium;
                                            width: 60%">
                                            Welcome to Air to Ground! Thank you for taking the time to fill out an online job
                                            application.
                                            <br />
                                            Please note that this information is strictly confidential. Be as complete and as
                                            accurate as you can. If your application is selected, we will contact you for more
                                            information.
                                        </div>
                                        <br />
                                        <table width="100%" style="line-height: 20px">
                                            <tr>
                                                <td class="careersLblTitle">
                                                    Today's Date:&nbsp;
                                                    <asp:Label CssClass="lblInfo" ID="lblToday" runat="server" Text='<%# Eval("Createdate") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    What is your name?
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 50%">
                                                        <td>
                                                            <asp:Label CssClass="lblInfo" ID="txtLastName" runat="server" Width="96%" Text='<%# Eval("Lastname") %>'>
                                                            </asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblLastName" runat="server" CssClass="lblSubscript" Text="Last Name"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label CssClass="lblInfo" ID="txtFirstName" runat="server" Width="96%" Text='<%# Eval("Firstname") %>'>
                                                            </asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblFirstName" runat="server" CssClass="lblSubscript" Text="First Name"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label CssClass="lblInfo" ID="txtMiddleName" runat="server" Width="96%"
                                                                Text='<%# Eval("Middlename") %>'>
                                                            </asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblMiddleName" runat="server" CssClass="lblSubscript" Text="Middle Name"></asp:Label>
                                                        </td>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    1) Are you currently employed?
                                                    <asp:RadioButton ID="chkCurrentlyEmployed1" runat="server" GroupName="CurEmp" Text="Yes"
                                                        Checked='<%# Eval("Iscurrentlyemployed") %>' />
                                                    <asp:RadioButton ID="chkCurrentlyEmployed2" runat="server" GroupName="CurEmp" Text="No"
                                                        Checked='<%# !Convert.ToBoolean(Eval("Iscurrentlyemployed")) %>' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    If you are currently employed, are you looking for new employment or a 2nd job?
                                                    <asp:RadioButton ID="chkIsSecondJob1" runat="server" GroupName="Second" Text="This is New Employment"
                                                        Checked='<%# !Convert.ToBoolean(Eval("Issecondjob")) %>' />
                                                    <asp:RadioButton ID="chkIsSecondJob2" runat="server" GroupName="Second" Text="This is a 2nd Job"
                                                        Checked='<%# Eval("Issecondjob") %>' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    2) What days of the week are you available to work?
                                                    <asp:CheckBox ID="chkMon" runat="server" Text="Mon" Checked='<%# Bind("Canworkmonday") %>' />
                                                    <asp:CheckBox ID="chkTues" runat="server" Text="Tue" Checked='<%# Bind("Canworktuesday") %>' />
                                                    <asp:CheckBox ID="chkWed" runat="server" Text="Wed" Checked='<%# Bind("Canworkwednesday") %>' />
                                                    <asp:CheckBox ID="chkThurs" runat="server" Text="Thur" Checked='<%# Bind("Canworkthursday") %>' />
                                                    <asp:CheckBox ID="chkFri" runat="server" Text="Fri" Checked='<%# Bind("Canworkfriday") %>' />
                                                    <asp:CheckBox ID="chkSat" runat="server" Text="Sat" Checked='<%# Bind("Canworksaturday") %>' />
                                                    <asp:CheckBox ID="chSun" runat="server" Text="Sun" Checked='<%# Bind("Canworksunday") %>' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    3) Can you work First, Second, or Third Shifts?
                                                    <asp:CheckBox ID="chkFirst" runat="server" Text="First Shift?" Checked='<%# Eval("Canworkfirstshift") %>' />
                                                    <asp:CheckBox ID="chkSecond" runat="server" Text="Second Shift?" Checked='<%# Eval("Canworksecondshift") %>' />
                                                    <asp:CheckBox ID="chkThird" runat="server" Text="Third Shift?" Checked='<%# Eval("Canworkthirdshift") %>' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    4) What type of work experience do you have?
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    <asp:Label CssClass="lblInfo" ID="txtExperience" runat="server" Width="96%"
                                                        Text='<%# Eval("WorkExperience") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    5) Are you looking for Full or Part-time employment?
                                                    <asp:Label CssClass="lblInfo" ID="lstFullOrPT" runat="server" Width="150px" Text='<%# Eval("Fulltimeorparttime") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    6) Do you attend any trade or technical schools?
                                                    <asp:RadioButton ID="chkAttendSchool1" runat="server" GroupName="Attend" Text="Yes"
                                                        Checked='<%# Eval("Isattendingschool") %>' />
                                                    <asp:RadioButton ID="chkAttendSchool2" runat="server" GroupName="Attend" Text="No"
                                                        Checked='<%# !Convert.ToBoolean(Eval("Isattendingschool")) %>' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    If yes, what school and when do you attend classes?
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    <asp:Label CssClass="lblInfo" ID="txtSchools" runat="server" Width="96%" Text='<%# Eval("Schoolsandschedule") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    7) Do you have any heavy equipment operating experience?
                                                    <asp:RadioButton ID="chkHeavyEquip1" runat="server" GroupName="Heavy" Text="Yes"
                                                        Checked='<%# Eval("Hasheavyequipexp") %>' />
                                                    <asp:RadioButton ID="chkHeavyEquip2" runat="server" GroupName="Heavy" Text="No" 
                                                        Checked='<%# !Convert.ToBoolean(Eval("Hasheavyequipexp")) %>' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    If yes, what types of equipment?
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    <asp:Label CssClass="lblInfo" ID="txtHeavyExp" runat="server" Width="96%" Text='<%# Eval("Heavyequipexp") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    8) Do you have a valid driver's license?
                                                    <asp:RadioButton ID="chkValidLic1" runat="server" GroupName="Lic" Text="Yes" Checked='<%# Eval("Hasvaliddriverslicense") %>' />
                                                    <asp:RadioButton ID="chkValidLic2" runat="server" GroupName="Lic" Text="No" Checked='<%# !Convert.ToBoolean(Eval("Hasvaliddriverslicense")) %>' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    9) Do you have reliable transportation (i.e. own a car, bus, etc.)?
                                                    <asp:RadioButton ID="chkTrans1" runat="server" GroupName="Trans" Text="Yes" Checked='<%# Eval("Hastransportation") %>' />
                                                    <asp:RadioButton ID="chkTrans2" runat="server" GroupName="Trans" Text="No" Checked='<%# !Convert.ToBoolean(Eval("Hastransportation")) %>' />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br />
                                <!-- JOB APPLICATION -->
                                <div class="div_container">
                                    <div class="div_header">
                                        Application For Employment</div>
                                    <div style="margin: 10px 10px 10px 10px">
                                        <div class="careersLblTitle" style="text-align: center; font-size: medium">
                                            Air to Ground Job Application
                                        </div>
                                        <br />
                                        <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                            1) PERSONAL INFORMATION
                                        </div>
                                        <br />
                                        <!-- PERSONAL -->
                                        <table width="100%" style="table-layout: fixed; vertical-align: middle">
                                            <%--<tr>
                                                <td class="careersLblTitle">
                                                    Social Security Number
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    <asp:Label CssClass="lblInfo" ID="txtSSN" runat="server"
                                                        Width="80px" Text='<%# Eval("Ssn") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    Present Address
                                                    <table style="width: 70%">
                                                        <tr>
                                                            <td style="width: 60%">
                                                                <asp:Label CssClass="lblInfo" ID="txtPresAddr" runat="server" Width="96%" Text='<%# Eval("Currentaddress") %>'>
                                                                </asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblPresAddr" runat="server" CssClass="lblSubscript" Text="Street"></asp:Label>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:Label CssClass="lblInfo" ID="txtPresCity" runat="server" Width="96%" Text='<%# Eval("Currentcity") %>'>
                                                                </asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblPresCity" runat="server" CssClass="lblSubscript" Text="City"></asp:Label>
                                                            </td>
                                                            <td style="width: 5%">
                                                                <asp:Label CssClass="lblInfo" ID="txtPresSt" runat="server" Width="82%" Text='<%# Eval("Currentstate") %>'>
                                                                </asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblPresSt" runat="server" CssClass="lblSubscript" Text="State"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label CssClass="lblInfo" ID="txtPresZip" runat="server" Width="96%"
                                                                    Text='<%# Eval("Currentzip") %>' >
                                                                </asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblPresZip" runat="server" CssClass="lblSubscript" Text="Zip"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    Permanent Address
                                                    <table style="width: 70%">
                                                        <tr>
                                                            <td style="width: 60%">
                                                                <asp:Label CssClass="lblInfo" ID="txtPermAddr" runat="server" Width="96%" Text='<%# Eval("Permaddress") %>'>
                                                                </asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblPermAddr" runat="server" CssClass="lblSubscript" Text="Street"></asp:Label>
                                                            </td>
                                                            <td style="width: 25%">
                                                                <asp:Label CssClass="lblInfo" ID="txtPermCity" runat="server" Width="96%" Text='<%# Eval("Permcity") %>'>
                                                                </asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblPermCity" runat="server" CssClass="lblSubscript" Text="City"></asp:Label>
                                                            </td>
                                                            <td style="width: 5%">
                                                                <asp:Label CssClass="lblInfo" ID="txtPermState" runat="server" Width="82%" Text='<%# Eval("Permstate") %>'>
                                                                </asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblPermState" runat="server" CssClass="lblSubscript" Text="State"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label CssClass="lblInfo" ID="txtPermZip" runat="server" Width="96%"
                                                                     Text='<%# Eval("Permzip") %>' >
                                                                </asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblPermZip" runat="server" CssClass="lblSubscript" Text="Zip"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    Phone No
                                                    <asp:Label CssClass="lblInfo" ID="txtPhoneNumber" runat="server" Width="85px" 
                                                        Text='<%# Regex.Replace(Eval("Phonenumber").ToString(), @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") %>'>
                                                    </asp:Label>
                                                    &nbsp;Are you 18 years old or older?&nbsp;
                                                    <asp:RadioButton ID="chk18YearsOld1" runat="server" GroupName="18Years" Text="Yes"
                                                        Checked='<%# Eval("Is18yearsold") %>' />
                                                    <asp:RadioButton ID="chk18YearsOld2" runat="server" GroupName="18Years" Text="No"
                                                        Checked='<%# !Convert.ToBoolean(Eval("Is18yearsold")) %>' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    Do you have any physical limitations that would prevent you from doing this job?
                                                    <asp:RadioButton ID="chkPhysical1" runat="server" GroupName="Physical" Text="Yes"
                                                        Checked='<%# Eval("Hasphysicallimitations") %>' />
                                                    <asp:RadioButton ID="chkPhysical2" runat="server" GroupName="Physical" Text="No"
                                                        Checked='<%# !Convert.ToBoolean(Eval("Hasphysicallimitations")) %>' />
                                                    <br />
                                                    If Yes, please explain:
                                                    <asp:Label CssClass="lblInfo" ID="txtPhysical" runat="server" Width="96%"
                                                        Height="50" Text='<%# Eval("Physicallimitations") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                            2) EMPLOYMENT DESIRED
                                        </div>
                                        <br />
                                        <table width="100%" style="table-layout: fixed">
                                            <tr>
                                                <td class="careersLblTitle">
                                                    Position Desired
                                                    <asp:Label CssClass="lblInfo" ID="txtPosition" runat="server" Width="90%"
                                                        Text='<%# Eval("Positiondesired") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td class="careersLblTitle">
                                                    Preferred Work Location
                                                    <asp:Label CssClass="lblInfo" ID="txtPreferredLocation" runat="server" Width="90%" Text='<%# Eval("Preferredworklocation") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td class="careersLblTitle">
                                                    Alternate Work Location
                                                    <asp:Label CssClass="lblInfo" ID="txtAlternateLocation" runat="server" Width="90%" Text='<%# Eval("Alternateworklocation") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td class="careersLblTitle">
                                                    <asp:CheckBox ID="chkDay" runat="server" Text="Days" Checked='<%# Eval("Wantsday") %>' />
                                                    <asp:CheckBox ID="chkNight" runat="server" Text="Nights" Checked='<%# Eval("Wantsnight") %>' />
                                                </td>
                                                <td class="careersLblTitle">
                                                    Date You Can Start
                                                    <asp:Label CssClass="lblInfo" ID="dtEmpStartDate" runat="server" Text='<%# Eval("Datecanstart", "{0:d}") %>'
                                                        Width="100px">
                                                    </asp:Label>
                                                </td>
                                                <td class="careersLblTitle">
                                                    Salary Desired
                                                    <asp:Label CssClass="lblInfo" ID="txtSalaryReq" runat="server" 
                                                        Text='<%# Eval("Desiredsalary", "{0:c}") %>' Width="80px">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle" colspan="4">
                                                    Have You Ever Applied to This Company Before?
                                                    <asp:RadioButton ID="chkApplied1" runat="server" GroupName="Applied" Text="Yes" Checked='<%# Eval("Hasappliedbefore") %>' />
                                                    <asp:RadioButton ID="chkApplied2" runat="server" GroupName="Applied" Text="No" Checked='<%# !Convert.ToBoolean(Eval("Hasappliedbefore")) %>' />
                                                </td>
                                                <td class="careersLblTitle" colspan="2">
                                                    Referred By
                                                    <asp:Label CssClass="lblInfo" ID="txtReferredBy" runat="server" Width="150px"
                                                        Text='<%# Eval("Referredby") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle" colspan="6">
                                                    Have you ever been convicted of, plead guilty or no contest to any criminal violation
                                                    of law, including criminal traffic offences? (A conviction does not automatically
                                                    mean you cannot be hired. Provide all facts.)?
                                                    <asp:RadioButton ID="chkPleadGuilty1" runat="server" GroupName="Guilty" Text="Yes"
                                                        Checked='<%# Eval("Haspleadguilty") %>' />
                                                    <asp:RadioButton ID="chkPleadGuilty2" runat="server" GroupName="Guilty" Text="No"
                                                        Checked='<%# !Convert.ToBoolean(Eval("Haspleadguilty")) %>' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle" colspan="6">
                                                    If Yes, please explain:
                                                    <asp:Label CssClass="lblInfo" ID="txtGuiltyReason" runat="server" Width="96%"
                                                        Text='<%# Bind("Guiltyreason") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle" colspan="3">
                                                    Have You Ever Been Convicted of a Felony?
                                                    <asp:RadioButton ID="chkFelony1" runat="server" GroupName="Felony" Text="Yes" Checked='<%# Eval("Isconvictedofafelony") %>' />
                                                    <asp:RadioButton ID="chkFelony2" runat="server" GroupName="Felony" Text="No" Checked='<%# !Convert.ToBoolean(Eval("Isconvictedofafelony")) %>' />
                                                </td>
                                                <td class="careersLblTitle" colspan="3">
                                                    If Yes, When? Month
                                                    <asp:Label CssClass="lblInfo" ID="txtFelonyMonth" runat="server" 
                                                        Text='<%# Eval("Felonymonth") %>' Width="30px">
                                                    </asp:Label>
                                                    Year
                                                    <asp:Label CssClass="lblInfo" ID="txtFelonyYear" runat="server" 
                                                        Text='<%# Eval("Felonyyear") %>' Width="40px">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                            3) EDUCATION
                                        </div>
                                        <br />
                                        <table width="100%" style="table-layout: fixed; border-color: Silver" border="1">
                                            <tr>
                                                <td class="careersLblTitle" style="background-color: Silver; width: 150px">
                                                    Education
                                                </td>
                                                <td class="careersLblTitle" style="background-color: Silver; width: 50%">
                                                    Name and Address of School
                                                </td>
                                                <td class="careersLblTitle" style="background-color: Silver; width: 80px">
                                                    # of Years Attended
                                                </td>
                                                <td class="careersLblTitle" style="background-color: Silver; width: 100px">
                                                    Did You Graduate?
                                                </td>
                                                <td class="careersLblTitle" style="background-color: Silver">
                                                    Subjects Studied
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    Elementary
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtElemSchoolName" runat="server" Width="96%"
                                                        Text='<%# Eval("Elemschoolnameaddr") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtElemSchoolYears" runat="server" 
                                                        Text='<%# Eval("Elemschoolyears") %>' Width="96%">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkElemGrad1" runat="server" GroupName="ElemGrad" Text="Yes"
                                                        Checked='<%# Eval("Elemschoolgrad") %>' />
                                                    <asp:RadioButton ID="chkElemGrad2" runat="server" GroupName="ElemGrad" Text="No"
                                                        Checked='<%# !Convert.ToBoolean(Eval("Elemschoolgrad")) %>' />
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtElemSubj" runat="server" Width="96%" Text='<%# Eval("Elemschoolsubj") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    Middle School
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtMidSchoolName" runat="server" Width="96%"
                                                        Text='<%# Eval("Midschoolnameaddr") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtMidSchoolYears" runat="server" 
                                                        Text='<%# Eval("Midschoolyears") %>' Width="96%">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkMidSchoolGrad1" runat="server" GroupName="MidGrad" Text="Yes"
                                                        Checked='<%# Eval("Midschoolgrad") %>' />
                                                    <asp:RadioButton ID="chkMidSchoolGrad2" runat="server" GroupName="MidGrad" Text="No"
                                                        Checked='<%# !Convert.ToBoolean(Eval("Midschoolgrad")) %>' />
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtMidSchoolSubj" runat="server" Width="96%"
                                                        Text='<%# Eval("Midschoolsubj") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    High School
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtHighSchoolName" runat="server" Width="96%"
                                                        Text='<%# Eval("Highschoolnameaddr") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtHighSchoolYears" runat="server" 
                                                        Text='<%# Eval("Highschoolyears") %>' Width="96%">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkHighSchoolGrad1" runat="server" GroupName="HighGrad" Text="Yes"
                                                        Checked='<%# Eval("Highschoolgrad") %>' />
                                                    <asp:RadioButton ID="chkHighSchoolGrad2" runat="server" GroupName="HighGrad" Text="No"
                                                        Checked='<%# !Convert.ToBoolean(Eval("Highschoolgrad")) %>' />
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtHighSchoolSubj" runat="server" Width="96%"
                                                        Text='<%# Eval("Highschoolsubj") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    College
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtColName" runat="server" Width="96%" Text='<%# Eval("Collegenameaddr") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtCollegeYears" runat="server" 
                                                        Text='<%# Eval("Collegeyears") %>' Width="96%">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkCollegeGrad1" runat="server" GroupName="CollGrad" Text="Yes"
                                                        Checked='<%# Eval("Collegegrad") %>' />
                                                    <asp:RadioButton ID="chkCollegeGrad2" runat="server" GroupName="CollGrad" Text="No"
                                                        Checked='<%# !Convert.ToBoolean(Eval("Collegegrad")) %>' />
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtCollegeSubj" runat="server" Width="96%"
                                                        Text='<%# Eval("Collegesubj") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    Trade, Tech, Online
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtTradeName" runat="server" Width="96%"
                                                        Text='<%# Eval("Tradenameaddr") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtTradeYears" runat="server" 
                                                        Text='<%# Eval("Tradeyears") %>' Width="96%">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkTradeGrad1" runat="server" GroupName="TradeGrad" Text="Yes"
                                                        Checked='<%# Eval("Tradegrad") %>' />
                                                    <asp:RadioButton ID="chkTradeGrad2" runat="server" GroupName="TradeGrad" Text="No"
                                                        Checked='<%# !Convert.ToBoolean(Eval("Tradegrad")) %>' />
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtTradeSubj" runat="server" Width="96%"
                                                        Text='<%# Eval("Tradesubj") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                            4) GENERAL
                                        </div>
                                        <br />
                                        <table width="100%" style="table-layout: fixed">
                                            <tr>
                                                <td class="careersLblTitle">
                                                    Subjects of Special Study or Research Work
                                                    <br />
                                                    <asp:Label CssClass="lblInfo" ID="txtSpecialStudy" runat="server" Width="96%"
                                                        Text='<%# Eval("Specialstudyorresearch") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    Special Skills
                                                    <br />
                                                    <asp:Label CssClass="lblInfo" ID="txtSpecialSkills" runat="server" Width="96%"
                                                        Text='<%# Eval("Specialskills") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    Activities (Civic, Athletic, etc.)
                                                    <br />
                                                    <asp:Label ID="lblEthic" runat="server" CssClass="lblSubscript" Text="(Exclude organizations where the name of which indicate the race, creed, 
                                                                sex, age, marital status, color, or nation of origin of its members)">
                                                    </asp:Label>
                                                    <br />
                                                    <asp:Label CssClass="lblInfo" ID="txtActivities" runat="server" Width="96%"
                                                        Text='<%# Eval("Activities") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    U.S. Military Service
                                                    <asp:Label CssClass="lblInfo" ID="txtMilitary" runat="server" Width="200px"
                                                        Text='<%# Eval("Militaryservice") %>'>
                                                    </asp:Label>
                                                    Rank
                                                    <asp:Label CssClass="lblInfo" ID="txtRank" runat="server" Width="200px" Text='<%# Eval("Rank") %>'>
                                                    </asp:Label>
                                                    <br />
                                                    Present Membership in National Guard or Reserves
                                                    <asp:Label CssClass="lblInfo" ID="txtGuard" runat="server" Width="200px" Text='<%# Eval("Nationalguardorreserves") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                            5) FORMER EMPLOYERS
                                        </div>
                                        <br />
                                        <table width="100%" style="table-layout: fixed;" border="1">
                                            <tr>
                                                <td class="careersLblTitle" style="background-color: Silver; width: 160px">
                                                    Dates Employed
                                                </td>
                                                <td class="careersLblTitle" style="background-color: Silver; width: 30%">
                                                    Employer Name and Address
                                                </td>
                                                <td class="careersLblTitle" style="background-color: Silver; width: 80px">
                                                    Employer Phone
                                                </td>
                                                <td class="careersLblTitle" style="background-color: Silver">
                                                    Position
                                                </td>
                                                <td class="careersLblTitle" style="background-color: Silver; width: 7%">
                                                    Salary / Wage
                                                </td>
                                                <td class="careersLblTitle" style="background-color: Silver">
                                                    Reason for Leaving
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    From M/Y
                                                    <asp:Label CssClass="lblInfo" ID="txtEmpFromMonth" runat="server" 
                                                        Text='<%# Eval("Emp1frommonth") %>' Width="30px">
                                                    </asp:Label>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmpFromYear" runat="server" 
                                                         Text='<%# Eval("Emp1fromyear") %>' Width="40px">
                                                    </asp:Label>
                                                    <br />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                                                    <asp:Label CssClass="lblInfo" ID="txtEmpToMonth" runat="server" 
                                                        Text='<%# Eval("Emp1tomonth") %>' Width="30px">
                                                    </asp:Label>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmpToYear" runat="server" 
                                                         Text='<%# Eval("Emp1toyear") %>' Width="40px">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp1Name" runat="server" Width="96%" Text='<%# Eval("Emp1name") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp1Phone" runat="server" Width="96%" 
                                                        Text='<%# Regex.Replace(Eval("Emp1phone").ToString(), @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp1Position" runat="server" Width="96%"
                                                        Text='<%# Eval("Emp1position") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp1Salary" runat="server" Text='<%# Eval("Emp1salary", "{0:c}") %>'
                                                        Width="96%">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp1Reason" runat="server" Width="96%"
                                                        Text='<%# Eval("Emp1reason") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    From M/Y
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp2FromMonth" runat="server" 
                                                        Text='<%# Eval("Emp2frommonth") %>' Width="30px">
                                                    </asp:Label>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp2FromYear" runat="server" 
                                                         Text='<%# Eval("Emp2fromyear") %>' Width="40px">
                                                    </asp:Label>
                                                    <br />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp2ToMonth" runat="server" 
                                                        Text='<%# Eval("Emp2tomonth") %>' Width="30px">
                                                    </asp:Label>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp2ToYear" runat="server" 
                                                         Text='<%# Eval("Emp2toyear") %>' Width="40px">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp2Name" runat="server" Width="96%" Text='<%# Eval("Emp2name") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp2Phone" runat="server" Width="96%" 
                                                        Text='<%# Eval("Emp2phone") == null ? "" : Regex.Replace(Eval("Emp2phone").ToString(), @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp2Position" runat="server" Width="96%"
                                                        Text='<%# Eval("Emp2position") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp2Salary" runat="server" Text='<%# Eval("Emp2salary", "{0:c}") %>'
                                                        Width="96%">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp2Reason" runat="server" Width="96%"
                                                        Text='<%# Eval("Emp2reason") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    From M/Y
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp3FromMonth" runat="server" 
                                                        Text='<%# Eval("Emp3frommonth") %>' Width="30px">
                                                    </asp:Label>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp3FromYear" runat="server" 
                                                         Text='<%# Eval("Emp3fromyear") %>' Width="40px">
                                                    </asp:Label>
                                                    <br />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp3ToMonth" runat="server" 
                                                        Text='<%# Eval("Emp3tomonth") %>' Width="30px">
                                                    </asp:Label>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp3ToYear" runat="server" 
                                                         Text='<%# Eval("Emp3toyear") %>' Width="40px">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp3Name" runat="server" Width="96%" Text='<%# Eval("Emp3name") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp3Phone" runat="server" Width="96%" 
                                                        Text='<%# Eval("Emp3phone") == null ? "" : Regex.Replace(Eval("Emp3phone").ToString(), @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp3Position" runat="server" Width="96%"
                                                        Text='<%# Eval("Emp3position") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp3Salary" runat="server" Text='<%# Eval("Emp3salary", "{0:c}") %>'
                                                        Width="96%">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp3Reason" runat="server" Width="96%"
                                                        Text='<%# Eval("Emp3reason") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    From M/Y
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp4FromMonth" runat="server" 
                                                        Text='<%# Eval("Emp4frommonth") %>' Width="30px">
                                                    </asp:Label>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp4FromYear" runat="server" 
                                                         Text='<%# Eval("Emp4fromyear") %>' Width="40px">
                                                    </asp:Label>
                                                    <br />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp4ToMonth" runat="server" 
                                                        Text='<%# Eval("Emp4tomonth") %>' Width="30px">
                                                    </asp:Label>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp4ToYear" runat="server" 
                                                         Text='<%# Eval("Emp4toyear") %>' Width="40px">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp4Name" runat="server" Width="96%" Text='<%# Eval("Emp4name") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp4Phone" runat="server" Width="96%" 
                                                        Text='<%# Eval("Emp4phone") == null ? "" : Regex.Replace(Eval("Emp4phone").ToString(), @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp4Position" runat="server" Width="96%"
                                                        Text='<%# Eval("Emp4position") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp4Salary" runat="server" Text='<%# Eval("Emp4salary", "{0:c}") %>'
                                                        Width="96%">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp4Reason" runat="server" Width="96%"
                                                        Text='<%# Eval("Emp4reason") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    From M/Y
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp5FromMonth" runat="server" 
                                                        Text='<%# Eval("Emp5frommonth") %>' Width="30px">
                                                    </asp:Label>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp5FromYear" runat="server" 
                                                         Text='<%# Eval("Emp5fromyear") %>'
                                                        MaxText='<%# DateTime.Now.Year %>' Width="40px">
                                                    </asp:Label>
                                                    <br />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp5ToMonth" runat="server" 
                                                        Text='<%# Eval("Emp5tomonth") %>' Width="30px">
                                                    </asp:Label>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp5ToYear" runat="server" 
                                                         Text='<%# Eval("Emp5toyear") %>'
                                                        MaxText='<%# DateTime.Now.Year %>' Width="40px">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp5Name" runat="server" Width="96%" Text='<%# Eval("Emp5name") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp5Phone" runat="server" Width="96%" 
                                                        Text='<%# Eval("Emp5phone") == null ? "" : Regex.Replace(Eval("Emp5phone").ToString(), @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp5Position" runat="server" Width="96%"
                                                        Text='<%# Eval("Emp5position") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp5Salary" runat="server" Text='<%# Eval("Emp5salary", "{0:c}") %>'
                                                        Width="96%">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp5Reason" runat="server" Width="96%"
                                                        Text='<%# Eval("Emp5reason") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle">
                                                    From M/Y
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp6FromMonth" runat="server" 
                                                        Text='<%# Eval("Emp6frommonth") %>' Width="30px">
                                                    </asp:Label>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp6FromYear" runat="server" 
                                                         Text='<%# Eval("Emp6fromyear") %>'
                                                        MaxText='<%# DateTime.Now.Year %>' Width="40px">
                                                    </asp:Label>
                                                    <br />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp6ToMonth" runat="server" 
                                                        Text='<%# Eval("Emp6tomonth") %>' Width="30px">
                                                    </asp:Label>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp6ToYear" runat="server" 
                                                         Text='<%# Eval("Emp6toyear") %>'
                                                        MaxText='<%# DateTime.Now.Year %>' Width="40px">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp6Name" runat="server" Width="96%" Text='<%# Eval("Emp6name") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp6Phone" runat="server" Width="96%" 
                                                        Text='<%# Eval("Emp6phone") == null ? "" : Regex.Replace(Eval("Emp6phone").ToString(), @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp6Position" runat="server" Width="96%"
                                                        Text='<%# Eval("Emp6position") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp6Salary" runat="server" Text='<%# Eval("Emp6salary", "{0:c}") %>'
                                                        Width="96%">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtEmp6Reason" runat="server" Width="96%"
                                                        Text='<%# Eval("Emp6reason") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle" colspan="6">
                                                    Can we contact these employers?
                                                    <asp:RadioButton ID="chkContactEmp1" runat="server" GroupName="ContactEmp" Text="Yes"
                                                        Checked='<%# Eval("Cancontactemp") %>' />
                                                    <asp:RadioButton ID="chkContactEmp2" runat="server" GroupName="ContactEmp" Text="No"
                                                        Checked='<%# !Convert.ToBoolean(Eval("Cancontactemp")) %>' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="careersLblTitle" colspan="6">
                                                    If no, please list and explain:
                                                    <br />
                                                    <asp:Label CssClass="lblInfo" ID="txtContactEmpReason" runat="server" Width="96%"
                                                        Text='<%# Eval("Cantcontactreason") %>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                            6) BUSINESS / PERSONAL REFERENCES
                                        </div>
                                        <br />
                                        <table width="100%" style="table-layout: fixed;" border="1">
                                            <tr>
                                                <td class="careersLblTitle" style="background-color: Silver">
                                                    Name
                                                </td>
                                                <td class="careersLblTitle" style="background-color: Silver; width: 80px">
                                                    Phone Number
                                                </td>
                                                <td class="careersLblTitle" style="background-color: Silver">
                                                    Business
                                                </td>
                                                <td class="careersLblTitle" style="background-color: Silver; width: 80px">
                                                    Years Acquainted
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtRef1Name" runat="server" Width="96%" Text='<%# Eval("Ref1name") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtRef1Phone" runat="server" Width="96%" 
                                                        Text='<%# Regex.Replace(Eval("Ref1phone").ToString(), @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtRef1Business" runat="server" Width="96%"
                                                        Text='<%# Eval("Ref1business") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtRef1Years" runat="server" 
                                                        Text='<%# Eval("Ref1years") %>' Width="96%">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtRef2Name" runat="server" Width="96%" Text='<%# Eval("Ref2name") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtRef2Phone" runat="server" Width="96%" 
                                                        Text='<%# Regex.Replace(Eval("Ref2phone").ToString(), @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtRef2Business" runat="server" Width="96%"
                                                        Text='<%# Eval("Ref2business") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtRef2Years" runat="server" 
                                                        Text='<%# Eval("Ref2years") %>' Width="96%">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtRef3Name" runat="server" Width="96%" Text='<%# Eval("Ref3name") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtRef3Phone" runat="server" Width="96%" 
                                                        Text='<%# Eval("Ref3phone") == null ? "" : Regex.Replace(Eval("Ref3phone").ToString(), @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtRef3Business" runat="server" Width="96%"
                                                        Text='<%# Eval("Ref3business") %>'>
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtRef3Years" runat="server" 
                                                        Text='<%# Eval("Ref3years") %>' Width="96%">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <table style="table-layout: fixed; width: 90%">
                                            <tr>
                                                <td class="careersLblTitle">
                                                    In Case of Emergency Notify&nbsp;&nbsp;
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtIceName" runat="server" Width="96%" Text='<%# Eval("Icecontactname") %>'>
                                                    </asp:Label>
                                                    <br />
                                                    <asp:Label ID="lblICEName" runat="server" CssClass="lblSubscript" Text="Name"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtIceAddr" runat="server" Width="96%" Text='<%# Eval("Icecontactaddr") %>'>
                                                    </asp:Label>
                                                    <br />
                                                    <asp:Label ID="lblICEAddr" runat="server" CssClass="lblSubscript" Text="Address"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label CssClass="lblInfo" ID="txtIcePhone" runat="server" Width="75px" 
                                                        Text='<%# Regex.Replace(Eval("Icecontactphone").ToString(), @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") %>'>
                                                    </asp:Label>
                                                    <br />
                                                    <asp:Label ID="lblICEPhone" runat="server" CssClass="lblSubscript" Text="Phone Number"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <br />
                                    </div>
                                </div>
                                <br />
                                <!-- CERTIFICATION -->
                                <div class="lblInfoBold" style="text-align: center">
                                    <div class="lblInfoBold" style="text-align: left; margin: 10px 10px 10px 10px">
                                        I certify that all the information submitted by me on this application is true and
                                        complete, and I understand that if any false information, omissions, or misrepresentations
                                        are discovered, my application may be rejected and, if I am employed, my employment
                                        may be terminated at any time. In consideration of my employment, I agree to conform
                                        to the company's rules and regulations, and I agree that my employment and compensation
                                        can be terminated, with or without cause, and with or without notice, at any time
                                        at either my or the company's option. I also understand and agree that the terms
                                        and conditions of my employment may be changed, with or without cause, and with
                                        or without notice, at any time by the company. I understand that no company representative,
                                        other than it's president, and then only when in writing and signed by the president,
                                        has any authority to enter into any agreement for employment for any specific period
                                        of time, or to make any agreement contrary to the foregoing.
                                        <br />
                                        <br />
                                        Date Signed:&nbsp;&nbsp;
                                        <asp:Label CssClass="lblInfo" ID="dtSigDate" runat="server" Text='<%# Eval("Sigdate") %>'>
                                        </asp:Label>
                                        <br />
                                        Signature - please enter your Email Address:&nbsp;&nbsp;
                                        <asp:Label CssClass="lblInfo" ID="txtSignature" runat="server" Width="300px"
                                            Text='<%# Eval("Signature") %>'>
                                        </asp:Label>
                                    </div>
                                    <br />
                                    <%--<div style="float: right; text-align: center; width: 250px; margin: 10px 10px 10px 10px">
                                        <telerik:RadCaptcha ID="RadCaptcha1" runat="server" Display="Static" 
                                            Width="100%" ErrorMessage="Wrong Letters!" IgnoreCase="true"
                                            EnableRefreshImage="true" ProtectionMode="Captcha" CaptchaTextBoxLabel="" >  
                                            <CaptchaImage LineNoise="High" UseRandomFont="True" FontWarp="Medium" />
                                        </telerik:RadCaptcha>
                                    </div>--%>
                                </div>
                                <br />
                                <!-- ADMIN / RESULTS -->
                                <br />
                                <div id="divAdmin" runat="server" style="text-align: left">
                                    <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                        DO NOT WRITE BELOW THIS LINE ON THIS PAGE
                                    </div>
                                    <br />
                                    <table width="100%" style="table-layout: fixed;" border="1">
                                        <tr>
                                            <td class="careersLblTitle" colspan="3">
                                                Guid Key
                                                <asp:Label CssClass="lblInfo" ID="lblGuid" runat="server" Width="76%" Text='<%# Eval("Guidkey") %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="careersLblTitle" colspan="2">
                                                Interviewed By
                                                <asp:Label CssClass="lblInfo" ID="txtResultIntBy" runat="server" Width="76%"
                                                    Text='<%# Eval("Resultinterviewedby") %>'>
                                                </asp:Label>
                                            </td>
                                            <td class="careersLblTitle">
                                                Date
                                                <asp:Label CssClass="lblInfo" ID="dtInterviewDate" runat="server" Text='<%# Eval("Resultdate") %>'
                                                    Width="100px">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="careersLblTitle" colspan="3">
                                                Remarks
                                                <asp:Label CssClass="lblInfo" ID="txtResultRemarks" runat="server" Width="99%"
                                                    Text='<%# Eval("Resultremarks") %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="careersLblTitle" colspan="2">
                                                Neatness
                                                <asp:Label CssClass="lblInfo" ID="txtResultNeat" runat="server" Width="300px"
                                                    Text='<%# Eval("Resultneatness") %>'>
                                                </asp:Label>
                                            </td>
                                            <td class="careersLblTitle">
                                                Ability
                                                <asp:Label CssClass="lblInfo" ID="txtResultAbility" runat="server" Width="300px"
                                                    Text='<%# Eval("Resultability") %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="careersLblTitle">
                                                Hired
                                                <asp:RadioButton ID="chkHired1" runat="server" GroupName="Hired" Text="Yes" Checked="false" />
                                                <asp:RadioButton ID="chkHired2" runat="server" GroupName="Hired" Text="No" Checked="false" />
                                            </td>
                                            <td class="careersLblTitle">
                                                Position
                                                <asp:Label CssClass="lblInfo" ID="txtResultPos" runat="server" Width="150px"
                                                    Text='<%# Eval("Resultposition") %>'>
                                                </asp:Label>
                                            </td>
                                            <td class="careersLblTitle">
                                                Dept
                                                <asp:Label CssClass="lblInfo" ID="txtResultDept" runat="server" Width="150px"
                                                    Text='<%# Eval("Resultdept") %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="careersLblTitle">
                                                Salary / Wage
                                                <asp:Label CssClass="lblInfo" ID="txtResultSalary" runat="server" Text='<%# Eval("Resultsalary", "{0:c}") %>'
                                                    Width="100px">
                                                </asp:Label>
                                            </td>
                                            <td class="careersLblTitle" colspan="2">
                                                Date Reporting to Work
                                                <asp:Label CssClass="lblInfo" ID="dtStartDate" runat="server" Text='<%# Eval("Resultstartdate", "{0:d}") %>'
                                                    Width="100px">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="careersLblTitle">
                                                Approved 1)
                                                <asp:Label CssClass="lblInfo" ID="txtApproval1" runat="server" Width="150px"
                                                    Text='<%# Eval("Resultapproval1") %>'>
                                                </asp:Label>
                                                <br />
                                                <asp:Label ID="lblEmpMgr" runat="server" CssClass="lblSubscript" Text="Employment Mgr"></asp:Label>
                                            </td>
                                            <td class="careersLblTitle">
                                                Approved 2)
                                                <asp:Label CssClass="lblInfo" ID="txtApproval2" runat="server" Width="150px"
                                                    Text='<%# Eval("Resultapproval2") %>'>
                                                </asp:Label>
                                                <br />
                                                <asp:Label ID="lblDeptHead" runat="server" CssClass="lblSubscript" Text="Department Head"></asp:Label>
                                            </td>
                                            <td class="careersLblTitle">
                                                Approved 3)
                                                <asp:Label CssClass="lblInfo" ID="txtApproval3" runat="server" Width="150px"
                                                    Text='<%# Eval("Resultapproval3") %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Fields>
                </asp:DetailsView>
            </div>
        </div>
    </form>
</body>
</html>
