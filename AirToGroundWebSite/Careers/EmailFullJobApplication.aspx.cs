﻿using System;
using System.Web.UI.HtmlControls;

public partial class Careers_EmailFullJobApplication : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            // Register SESSION PULSE TIMEOUT
            PageUtils.RegisterSessionTimeoutPulse(Page);

            // StyleSheets
            PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);

            string guid = Request.QueryString["Id"];
            string admin = Request.QueryString["Admin"] ?? "";
            Session.Add("JOB_APP_GUID", guid);
            if ((guid != null) && (guid != "")) {
                LinqDataSource1.WhereParameters["id"].DefaultValue = guid;
                //LinqDataSource1.DataBind();
                grdApplication.DataBind();

                // Turn on/off admin panel.
                if ((admin == null) || (admin == "")) {
                    HtmlContainerControl div = (HtmlContainerControl)PageUtils.FindControlRecursive(grdApplication, "divAdmin");
                    if (div != null) {
                        div.Style.Add("display", "none");
                        div.Visible = false;
                    }
                } else {
                    // Turn on the admin panel.
                    HtmlContainerControl div = (HtmlContainerControl)PageUtils.FindControlRecursive(grdApplication, "divAdmin");
                    if (div != null) {
                        div.Style.Add("display", "block");
                        div.Visible = true;
                        // Hide print options when in admin mode.
                        //divPrint.Style.Add("display", "none");
                        //divPrint.Visible = false;
                    }
                }                 

                // Used for resending the job app from the url.
                // http://www.airtogroundtrack.com/Careers/EmailFullJobApplication.aspx?Id=<guid>&Admin=Y&Email=person@airtoground.com
                string email = Request.QueryString["Email"] ?? "";
                if (!String.IsNullOrEmpty(email)) {
                    // Send mail.
                    string uri = System.Web.Configuration.WebConfigurationManager.AppSettings["ATGJobAppURI"];

                    // Send the job app to the email addr. Don't format the url with the email addr or it will loop.
                    EmailUtils.SendJobApplicationMail(Server, uri, "Id=" + guid + "&Admin=Y", email, guid, false);
                }
            }
            // Force a logout.
            //FormsAuthentication.SignOut();
        }
    }
}