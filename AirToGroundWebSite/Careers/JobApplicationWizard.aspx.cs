﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.Security;

public partial class Careers_JobApplicationWizard : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = "Corp";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
			
			if ((Session["JOB_APP_GUID"] == null) ||
			    (Session["JOB_APP_GUID"].ToString() == "")) {
                Session.Add("JOB_APP_GUID", "NONE");
                // Log the user in.
                DoLogin();
            }

            LinqDataSource1.DataBind();

            // Some defaults.
            Label lblToday = (Label)PageUtils.FindControlRecursive(grdApplication3, "lblToday");
            if (lblToday != null) {
                lblToday.Text = DateTime.Now.ToString();
            }
        } else {
            //Page.Validate();
        }
    }
    protected void DoLogin() {
        //if (Profile.UserName == null) {
            // User not logged in.
            // First, validate the job application user.
            if (Membership.ValidateUser("jobappvisitor", "4ppl1c4nt!")) {
                FormsAuthentication.SetAuthCookie("jobappvisitor", false);
                Profile.Initialize("jobappvisitor", true);
                // DEPRECATED System.Web.HttpContext.Current.Session["SecurityDBName"] = "atg";
            }            
        //}
    }
    protected void Phone_OnLoad(object sender, EventArgs e) {
        RadMaskedTextBox tb = sender as RadMaskedTextBox;
        if ((tb.Text == null) || (tb.Text == "")) {
            tb.BackColor = System.Drawing.Color.FromArgb(249, 194, 187);
        } else {
            tb.BackColor = System.Drawing.Color.White;
        }
    }
    protected void reqRFV_OnInit(object sender, EventArgs e) {
        BaseValidator rfv = sender as BaseValidator;
        rfv.IsValid = false; // Page.Validate() in NextClick() and Page_Load() will override this.
    }
    protected void Wizard1_OnNextButtonClick(object sender, WizardNavigationEventArgs e) {
        Page.Validate();
        if (!Page.IsValid) {
            e.Cancel = true;    
        }
    }
    protected void Wizard1_ActiveStepChanged(object sender, EventArgs e) {
        /*if (Wizard1.ActiveStep == WizardStep1) {
            // Nothing
        } else if (Wizard1.ActiveStep == WizardStep2) {
            //grdApplication1.InsertItem(true);            
        } else if (Wizard1.ActiveStep == WizardStep3) {
            grdApplication2.InsertItem(true);
        } else if (Wizard1.ActiveStep == WizardStep4) {
            grdApplication3.InsertItem(true);
        } else if (Wizard1.ActiveStep == WizardStep5) {
            grdApplication4.InsertItem(true);
        }
        */
        Page.Validate();
    }
    protected void Wizard1_SideBarButtonClick(object sender, WizardNavigationEventArgs e) {
        /*if (e.CurrentStepIndex == 0) {
            // Nothing 
        } else if (e.CurrentStepIndex == 1) {
            grdApplication2.InsertItem(true);
        } else if (e.CurrentStepIndex == 2) {
            grdApplication3.InsertItem(true);
        } else if (e.CurrentStepIndex == 3) {
            grdApplication4.InsertItem(true);
        } else if (e.CurrentStepIndex == 4) {
            grdApplication5.InsertItem(true);
        }

        //if (!Page.IsValid) {
          //  e.Cancel = true;
        //} else {
            e.Cancel = false;
        //}
        // Validate the page.
        */
    }
    protected void Wizard1_OnFinishButtonClick(object sender, EventArgs e) {
        RadCaptcha cc = (RadCaptcha)PageUtils.FindControlRecursive(grdApplication5, "RadCaptcha1");
        if ((cc != null) && (cc.IsValid == true)) {
            // Prevent user from ever sending another application.
            //if (Session["JOB_APP_GUID"] == "NONE") {
                grdApplication2.InsertItem(false);
                //grdApplication3.InsertItem(false);
                //grdApplication4.InsertItem(false);
                //grdApplication5.InsertItem(false);
            //} else {
                // User trying to post twice.
            //    RadWindowManager1.RadAlert("You may not submit the job application twice", 225, 75, "Error", null);
            //}
        }
    }
    /*
     THIS SECTION TRIGGERED AFTER AN ASYNC PAUSE     
     */
    protected void DoEverythingAsync() {
        //Thread aThread;
        //aThread = new Thread(new ThreadStart(delegate {
            IDictionary v2 = (IDictionary)Cache[JOB_APP_VALUES_KEY2];
            IDictionary v3 = (IDictionary)Cache[JOB_APP_VALUES_KEY3];
            IDictionary v4 = (IDictionary)Cache[JOB_APP_VALUES_KEY4];
            IDictionary v5 = (IDictionary)Cache[JOB_APP_VALUES_KEY5];

            OrderedDictionary totValues = new OrderedDictionary();
            IDictionaryEnumerator en = v2.GetEnumerator();
            en.Reset();
            while (en.MoveNext()) {
                totValues.Add(en.Key, en.Value);
            }
            en = v3.GetEnumerator();
            en.Reset();
            while (en.MoveNext()) {
                totValues.Add(en.Key, en.Value);
            }
            en = v4.GetEnumerator();
            en.Reset();
            while (en.MoveNext()) {
                totValues.Add(en.Key, en.Value);
            }
            en = v5.GetEnumerator();
            en.Reset();
            while (en.MoveNext()) {
                totValues.Add(en.Key, en.Value);
            }

            // Add the guid.
            string guid = Guid.NewGuid().ToString();
            totValues.Add("Guidkey", guid);

            // Insert them all.
            LinqDataSource1.Insert(totValues);
            Session.Add("JOB_APP_GUID", guid);

            // Send mail.
            string uri = System.Web.Configuration.WebConfigurationManager.AppSettings["ATGJobAppURI"];

            // Send all the ATG staff emails.
            MembershipUserCollection users = ProviderUtils.GetAllUsers();
            foreach (MembershipUser user in users) {
                if (ProviderUtils.IsUserInRole(user.UserName, SecurityUtils.ROLE_RECEIVE_JOBAPP)) {
                    // Send the ATG users an email. Display admin panel.
                    EmailUtils.SendJobApplicationMail(Server, uri, "Id=" + guid + "&Admin=Y", user.Email, guid, false);
                }
            }

            // Send the applicant an email. No admin panel.
            EmailUtils.SendJobApplicationMail(Server, uri, "Id=" + guid, "", guid, true);

            // Clear the captcha in case the user comes back.
            RadCaptcha cc = (RadCaptcha)PageUtils.FindControlRecursive(grdApplication5, "RadCaptcha1");
            if (cc != null) {
                cc.IsValid = false;
                cc.Enabled = false;
            }
            // Go to printable view. No admin panel.
            Response.Redirect("~/Careers/EmailFullJobApplication.aspx?Id=" + guid);
        //}));
        //aThread.Start();
    }
    private string JOB_APP_VALUES_KEY2 = "JOB_APP_VALUES_KEY2";
    protected void grdApplication2_ItemInserting(object sender, System.Web.UI.WebControls.DetailsViewInsertEventArgs e) {
        Cache[JOB_APP_VALUES_KEY2] = e.Values;
        grdApplication3.InsertItem(false);
    }
    private string JOB_APP_VALUES_KEY3 = "JOB_APP_VALUES_KEY3";
    protected void grdApplication3_ItemInserting(object sender, System.Web.UI.WebControls.DetailsViewInsertEventArgs e) {
        Cache[JOB_APP_VALUES_KEY3] = e.Values;
        grdApplication4.InsertItem(false);
    }
    private string JOB_APP_VALUES_KEY4 = "JOB_APP_VALUES_KEY4";
    protected void grdApplication4_ItemInserting(object sender, System.Web.UI.WebControls.DetailsViewInsertEventArgs e) {
        Cache[JOB_APP_VALUES_KEY4] = e.Values;
        grdApplication5.InsertItem(false);
    }
    private string JOB_APP_VALUES_KEY5 = "JOB_APP_VALUES_KEY5";
    protected void grdApplication5_ItemInserting(object sender, System.Web.UI.WebControls.DetailsViewInsertEventArgs e) {
        Cache[JOB_APP_VALUES_KEY5] = e.Values;
        // This FINISH button has been pressed. Insert, Post, and Send Emails.
        DoEverythingAsync();
    }
}