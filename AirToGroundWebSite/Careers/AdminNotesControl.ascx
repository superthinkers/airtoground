﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdminNotesControl.ascx.cs" Inherits="Careers_AdminNotesControl" %>
<div class="lblTitle" style="font-size: large; background-color: Silver">
    DO NOT WRITE BELOW THIS LINE ON THIS PAGE
</div>
<br />
<table width="100%" style="table-layout: fixed;" border="0">
    <tr>
        <td class="lblTitle" colspan="2">
            Interviewed By
            <telerik:RadTextBox ID="txtResultIntBy" runat="server" MaxLength="30" Width="76%"
                Text='<%# Bind("Resultinterviewedby") %>'>
            </telerik:RadTextBox>
        </td>
        <td class="lblTitle">
            Date
            <telerik:RadDatePicker ID="dtInterviewDate" runat="server" SelectedDate='<%# Bind("Resultdate") %>'
                Width="100px">
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td class="lblTitle" colspan="3">
            Remarks
            <telerik:RadTextBox ID="txtResultRemarks" runat="server" Width="99%" MaxLength="255"
                TextMode="MultiLine" Height="50" Rows="4" Text='<%# Bind("Resultremarks") %>'>
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td class="lblTitle" colspan="2">
            Neatness
            <telerik:RadTextBox ID="txtResultNeat" runat="server" MaxLength="50" Width="300px"
                Text='<%# Bind("Resultneatness") %>'>
            </telerik:RadTextBox>
        </td>
        <td class="lblTitle">
            Ability
            <telerik:RadTextBox ID="txtResultAbility" runat="server" MaxLength="50" Width="300px"
                Text='<%# Bind("Resultability") %>'>
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td class="lblTitle">
            Hired
            <asp:RadioButton ID="chkHired1" runat="server" GroupName="Hired" Text="Yes" Checked='<%# Bind("Resultishired") %>' />
            <asp:RadioButton ID="chkHired2" runat="server" GroupName="Hired" Text="No" Checked="false" />
        </td>
        <td class="lblTitle">
            Position
            <telerik:RadTextBox ID="txtResultPos" runat="server" MaxLength="30" Width="150px"
                Text='<%# Bind("Resultposition") %>'>
            </telerik:RadTextBox>
        </td>
        <td class="lblTitle">
            Dept
            <telerik:RadTextBox ID="txtResultDept" runat="server" MaxLength="30" Width="150px"
                Text='<%# Bind("Resultdept") %>'>
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td class="lblTitle">
            Salary / Wage
            <telerik:RadNumericTextBox ID="txtResultSalary" runat="server" NumberFormat-DecimalDigits="0"
                Value='<%# Bind("Resultsalary") %>' Width="100px">
            </telerik:RadNumericTextBox>
        </td>
        <td class="lblTitle" colspan="2">
            Date Reporting to Work
            <telerik:RadDatePicker ID="dtStartDate" runat="server" SelectedDate='<%# Bind("Resultstartdate") %>'
                Width="100px">
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td class="lblTitle">
            Approved 1)
            <telerik:RadTextBox ID="txtApproval1" runat="server" MaxLength="30" Width="150px"
                Text='<%# Bind("Resultapproval1") %>'>
            </telerik:RadTextBox>
            <asp:Label ID="lblEmpMgr" runat="server" SkinID="SubscriptLabel" Text="Employment Mgr"></asp:Label>
        </td>
        <td class="lblTitle">
            Approved 2)
            <telerik:RadTextBox ID="txtApproval2" runat="server" MaxLength="30" Width="150px"
                Text='<%# Bind("Resultapproval2") %>'>
            </telerik:RadTextBox>
            <asp:Label ID="lblDeptHead" runat="server" SkinID="SubscriptLabel" Text="Department Head"></asp:Label>
        </td>
        <td class="lblTitle">
            Approved 3)
            <telerik:RadTextBox ID="txtApproval3" runat="server" MaxLength="30" Width="150px"
                Text='<%# Bind("Resultapproval3") %>'>
            </telerik:RadTextBox>
        </td>
    </tr>
</table>