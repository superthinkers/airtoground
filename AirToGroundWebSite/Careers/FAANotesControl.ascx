﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FAANotesControl.ascx.cs" Inherits="Careers_FAANotesControl" %>
<div class="lblInfoBold" style="text-align: left; margin: 10px 10px 10px 10px">
    <br />
    <div class="lblTitle" style="text-align: center; font-size: medium">
        AIR TO GROUND SERVICES, INC.<br />
        F.A.A. AND T.S.A. REGULATIONS
    </div>
    <br />
    <div class="lblTitle" style="text-align: center; border: 1px solid gray; font-weight: 900;
        font-size: medium">
        ATTENTION
    </div>
    <br />
    <div class="lblInfoBold" style="text-align: left; margin: 10px 10px 10px 10px">
        IN ORDER TO COMPLLY WITH F.A.A. (Federal Aviation Administration) AND T.S.A. (Transportation
        Security Administration) REGULATIONS, AIR TO GROUND SERVIES, INC. IS REQUIRED TO
        OBTAIN THE FOLLOWING BACKGROUND INFORMATION:
        <ul>
            <li>PAST 10 YEARS OF EMPLOYMENT AND/OR EDUCATION HISTORY</li>
            <li>BACKGROUND CHECK (INCLUDING 10 YEAR FBI CRIMINAL CHECK AND FINGERPRINTING). PER F.A.A. AND
            T.S.A. REGULATIONS IF YOU HAVE BEEN <strong>CONVICTED OF A FELONY</strong> IN THE PAST 10 YEARS
            YOU ARE NOT ELIGIBLE TO WORK AT AIR TO GROUND SERVICES, INC. </li>
            <li>PER F.A.A. REGULATIONS (DOT 49 CFR PART 40) YOU <strong>MUST</strong> SUBMIT TO A NON-DOT or DOT
            REGULATED TYPE DRUG TEST FOR PRE-EMPLOYMENT. YOU WILL BE TESTED FOR THE FOLLOWING SUBSTANCES: 
            AMPHETAMINES, COCAINE, MAJIJUANA, OPIATES AND PHENCYCLIDINE (PCP). AFTER EMPLOYED, ALL EMPLOYEES
            WILL BE SUBJECT TO NON-DOT OR DOT REGULATED DRUG AND/OR ALCOHOL TESTING: INCLUDING RANDOM, POST- ACCIDENT
            AND REASONABLE SUSPICION.</li>
            <li>YOU MUST BE 18 YEARS OF AGE OR OLDER.</li>
            <li>YOU MUST HAVE A VALID DRIVER'S LICENSE.</li>
        </ul>
        <div>
            <strong>Thank you for your interest in employment with our company. All applications
            will be reviewed, and only the selected applicants will be called in for and interview.
            Unfortunately, not every applicant will be called back. Please do not call to check
            on the status of your application. Thank you!
            <br />
            <br />
            Air to Ground Sevices, Inc.</strong>
        </div>
    </div>
</div>