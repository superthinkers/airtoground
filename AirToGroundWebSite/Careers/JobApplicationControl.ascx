﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JobApplicationControl.ascx.cs" Inherits="Careers_JobApplicationControl" %>
<!-- Application for Employment -->
<telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        function ValidateEmpStartDate(source, arguments) {
            if (DateTime(arguments.Value).Date >= DateTime.Now.Date) {
                arguments.IsValid = true;
            } else {
                arguments.IsValid = false;
            }
        }
    </script>
</telerik:RadScriptBlock>
<br />
<div class="div_container">
    <div class="div_header">Application For Employment</div>
    <div style="margin: 10px 10px 10px 10px">
        <div class="lblTitle" style="text-align: center; font-size: medium">
            Air to Ground Job Application
        </div>
        <br />
        <div class="lblTitle" style="font-size: large; background-color: Silver">
            1) PERSONAL INFORMATION
        </div>
        <br />
        <!-- PERSONAL -->
        <table width="100%" style="table-layout:fixed; vertical-align: middle">
            <tr>
                <td class="lblTitle">
                    Social Security Number
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    <telerik:RadMaskedTextBox ID="txtSSN" runat="server" MaxLength="11" Mask="###-##-####"
                        DisplayMask="###-##-####" Width="80px" Text='<%# Bind("Ssn") %>'>
                    </telerik:RadMaskedTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    Present Address
                    <table style="width: 70%">
                        <tr>
                            <td style="width: 60%">
                                <telerik:RadTextBox ID="txtPresAddr" runat="server" MaxLength="80" Width="96%" Text='<%# Bind("Currentaddress") %>'>
                                </telerik:RadTextBox>
                                <br />
                                <asp:Label ID="lblPresAddr" runat="server" SkinID="SubscriptLabel" Text="Street"></asp:Label>
                            </td>
                            <td style="width: 25%">
                                <telerik:RadTextBox ID="txtPresCity" runat="server" MaxLength="50" Width="96%" Text='<%# Bind("Currentcity") %>'>
                                </telerik:RadTextBox>
                                <br />
                                <asp:Label ID="lblPresCity" runat="server" SkinID="SubscriptLabel" Text="City"></asp:Label>
                            </td>
                            <td style="width: 5%">
                                <telerik:RadTextBox ID="txtPresSt" runat="server" MaxLength="2" Width="82%" Text='<%# Bind("Currentstate") %>'>
                                </telerik:RadTextBox>
                                <br />
                                <asp:Label ID="lblPresSt" runat="server" SkinID="SubscriptLabel" Text="State"></asp:Label>
                            </td>
                            <td>
                                <telerik:RadNumericTextBox ID="txtPresZip" runat="server" MaxLength="5" Width="96%"
                                    NumberFormat-GroupSeparator="" Text='<%# Bind("Currentzip") %>' NumberFormat-DecimalDigits="0">
                                </telerik:RadNumericTextBox>
                                <br />
                                <asp:Label ID="lblPresZip" runat="server" SkinID="SubscriptLabel" Text="Zip"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    Permanent Address
                    <table style="width: 70%">
                        <tr>
                            <td style="width: 60%">
                                <telerik:RadTextBox ID="txtPermAddr" runat="server" MaxLength="80" Width="96%" Text='<%# Bind("Permaddress") %>'>
                                </telerik:RadTextBox>
                                <br />
                                <asp:Label ID="lblPermAddr" runat="server" SkinID="SubscriptLabel" Text="Street"></asp:Label>
                            </td>
                            <td style="width: 25%">
                                <telerik:RadTextBox ID="txtPermCity" runat="server" MaxLength="50" Width="96%" Text='<%# Bind("Permcity") %>'>
                                </telerik:RadTextBox>
                                <br />
                                <asp:Label ID="lblPermCity" runat="server" SkinID="SubscriptLabel" Text="City"></asp:Label>
                            </td>
                            <td style="width: 5%">
                                <telerik:RadTextBox ID="txtPermState" runat="server" MaxLength="2" Width="82%" Text='<%# Bind("Permstate") %>'>
                                </telerik:RadTextBox>
                                <br />
                                <asp:Label ID="lblPermState" runat="server" SkinID="SubscriptLabel" Text="State"></asp:Label>
                            </td>
                            <td>
                                <telerik:RadNumericTextBox ID="txtPermZip" runat="server" MaxLength="5" Width="96%"
                                    NumberFormat-GroupSeparator="" Text='<%# Bind("Permzip") %>' NumberFormat-DecimalDigits="0">
                                </telerik:RadNumericTextBox>
                                <br />
                                <asp:Label ID="lblPermZip" runat="server" SkinID="SubscriptLabel" Text="Zip"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    Phone No
                    <telerik:RadMaskedTextBox ID="txtPhoneNumber" runat="server" Width="75px" Mask="###-###-####"
                        DisplayMask="###-###-####" Text='<%# Bind("Phonenumber") %>'>
                    </telerik:RadMaskedTextBox>
                    &nbsp;Are you 18 years old or older?&nbsp;
                    <asp:RadioButton ID="chk18YearsOld1" runat="server" GroupName="18Years" Text="Yes"
                        Checked='<%# Bind("Is18yearsold") %>' />
                    <asp:RadioButton ID="chk18YearsOld2" runat="server" GroupName="18Years" Text="No"
                        Checked="false" />
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    Do you have any physical limitations that would prevent you from doing this job?
                    <asp:RadioButton ID="chkPhysical1" runat="server" GroupName="Physical" Text="Yes"
                        Checked='<%# Bind("Hasphysicallimitations") %>' />
                    <asp:RadioButton ID="chkPhysical2" runat="server" GroupName="Physical" Text="No"
                        Checked="false" />
                    <br />
                    If Yes, please explain:
                    <telerik:RadTextBox ID="txtPhysical" runat="server" Width="96%" MaxLength="255" TextMode="MultiLine"
                        Height="50" Rows="4" Text='<%# Bind("Physicallimitations") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
        </table>
        <br />
        <div class="lblTitle" style="font-size: large; background-color: Silver">
            2) EMPLOYMENT DESIRED
        </div>
        <br />
        <table width="100%" style="table-layout: fixed">
            <tr>
                <td class="lblTitle">
                    Position Desired
                    <telerik:RadTextBox ID="txtPosition" runat="server" MaxLength="50" Width="150px"
                        Text='<%# Bind("Positiondesired") %>'>
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    Preferred Work Location
                    <telerik:RadComboBox ID="lstPreferredLocation" runat="server" Width="100px" 
                        DataValueField="Preferredworklocation" DataTextField="Preferredworklocation">
                        <Items>
                            <telerik:RadComboBoxItem Value="Indianapolis" Text="Indianapolis" />
                            <telerik:RadComboBoxItem Value="Los Angeles" Text="Los Angeles" />
                            <telerik:RadComboBoxItem Value="Louisville" Text="Louisville" />
                            <telerik:RadComboBoxItem Value="Memphis" Text="Memphis" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td class="lblTitle">
                    Alternate Work Location
                    <telerik:RadComboBox ID="lstAlternateLocation" runat="server" Width="100px" 
                        DataValueField="Alternateworklocation" DataTextField="Alternateworklocation">
                        <Items>
                            <telerik:RadComboBoxItem Value="Any" Text="Any" />
                            <telerik:RadComboBoxItem Value="Indianapolis" Text="Indianapolis" />
                            <telerik:RadComboBoxItem Value="Los Angeles" Text="Los Angeles" />
                            <telerik:RadComboBoxItem Value="Louisville" Text="Louisville" />
                            <telerik:RadComboBoxItem Value="Memphis" Text="Memphis" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td class="lblTitle">
                    <asp:CheckBox ID="chkDay" runat="server" Text="Days" Checked='<%# Bind("Wantsday") %>' />
                    <asp:CheckBox ID="chkNight" runat="server" Text="Nights" Checked='<%# Bind("Wantsnight") %>' />
                </td>
                <td class="lblTitle">
                    Date You Can Start
                    <telerik:RadDatePicker ID="dtEmpStartDate" runat="server" SelectedDate='<%# Bind("Datecanstart") %>'
                        Width="100px">
                    </telerik:RadDatePicker>
                    <asp:CustomValidator ID="reqEmpStartDate" runat="server" ControlToValidate="dtEmpStartDate" 
                        ErrorMessage="*" Display="Dynamic" SetFocusOnError="true" ValidationGroup="JOBAPP" 
                        ClientValidationFunction="ValidateEmpStartDate">
                    </asp:CustomValidator>
                </td>
                <td class="lblTitle">
                    Salary Desired
                    <telerik:RadNumericTextBox ID="txtSalaryReq" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Desiredsalary") %>' Width="80px">
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle" colspan="4">
                    Have You Ever Applied to This Company Before?
                    <asp:RadioButton ID="chkApplied1" runat="server" GroupName="Applied" Text="Yes" Checked='<%# Bind("Hasappliedbefore") %>' />
                    <asp:RadioButton ID="chkApplied2" runat="server" GroupName="Applied" Text="No" Checked="false" />
                </td>
                <td class="lblTitle" colspan="2">
                    Referred By
                    <telerik:RadTextBox ID="txtReferredBy" runat="server" MaxLength="50" Width="150px"
                        Text='<%# Bind("Referredby") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle" colspan="3">
                    Have You Ever Been Convicted of a Felony?
                    <asp:RadioButton ID="chkFelony1" runat="server" GroupName="Felony" Text="Yes" Checked='<%# Bind("Isconvictedofafelony") %>' />
                    <asp:RadioButton ID="chkFelony2" runat="server" GroupName="Felony" Text="No" Checked="false" />
                </td>
                <td class="lblTitle" colspan="3">
                    If Yes, When? Month
                    <telerik:RadNumericTextBox ID="txtFelonyMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Felonymonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    Year
                    <telerik:RadNumericTextBox ID="txtFelonyYear" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Felonyyear") %>' MinValue="1970" MaxValue='<%# DateTime.Now.Year %>'
                        Width="40px">
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
        </table>
        <br />
        <div class="lblTitle" style="font-size: large; background-color: Silver">
            3) EDUCATION
        </div>
        <br />
        <table width="100%" style="table-layout: fixed; border-color: Silver" border="1">
            <tr>
                <td class="lblTitle" style="background-color: Silver; width: 150px">
                    Education
                </td>
                <td class="lblTitle" style="background-color: Silver; width: 50%">
                    Name and Address of School
                </td>
                <td class="lblTitle" style="background-color: Silver; width: 80px">
                    # of Years Attended
                </td>
                <td class="lblTitle" style="background-color: Silver; width: 100px">
                    Did You Graduate?
                </td>
                <td class="lblTitle" style="background-color: Silver">
                    Subjects Studied
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    Elementary
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtElemSchoolName" runat="server" MaxLength="255" Width="96%"
                        Text='<%# Bind("Elemschoolnameaddr") %>'>
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadNumericTextBox ID="txtElemSchoolYears" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Elemschoolyears") %>' MinValue="0" MaxValue="12" Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <asp:RadioButton ID="chkElemGrad1" runat="server" GroupName="ElemGrad" Text="Yes"
                        Checked='<%# Bind("Elemschoolgrad") %>' />
                    <asp:RadioButton ID="chkElemGrad2" runat="server" GroupName="ElemGrad" Text="No"
                        Checked="false" />
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtElemSubj" runat="server" MaxLength="100" Width="96%" Text='<%# Bind("Elemschoolsubj") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    Middle School
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtMidSchoolName" runat="server" MaxLength="255" Width="96%"
                        Text='<%# Bind("Midschoolnameaddr") %>'>
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadNumericTextBox ID="txtMidSchoolYears" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Midschoolyears") %>' MinValue="0" MaxValue="12" Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <asp:RadioButton ID="chkMidSchoolGrad1" runat="server" GroupName="ElemGrad" Text="Yes"
                        Checked='<%# Bind("Midschoolgrad") %>' />
                    <asp:RadioButton ID="chkMidSchoolGrad2" runat="server" GroupName="ElemGrad" Text="No"
                        Checked="false" />
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtMidSchoolSubj" runat="server" MaxLength="100" Width="96%"
                        Text='<%# Bind("Midschoolsubj") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    High School
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtHighSchoolName" runat="server" MaxLength="255" Width="96%"
                        Text='<%# Bind("Highschoolnameaddr") %>'>
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadNumericTextBox ID="txtHighSchoolYears" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Highschoolyears") %>' MinValue="0" MaxValue="12" Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <asp:RadioButton ID="chkHighSchoolGrad1" runat="server" GroupName="ElemGrad" Text="Yes"
                        Checked='<%# Bind("Highschoolgrad") %>' />
                    <asp:RadioButton ID="chkHighSchoolGrad2" runat="server" GroupName="ElemGrad" Text="No"
                        Checked="false" />
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtHighSchoolSubj" runat="server" MaxLength="100" Width="96%"
                        Text='<%# Bind("Highschoolsubj") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    College
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtColName" runat="server" MaxLength="255" Width="96%" Text='<%# Bind("Collegenameaddr") %>'>
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadNumericTextBox ID="txtCollegeYears" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Collegeyears") %>' MinValue="0" MaxValue="12" Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <asp:RadioButton ID="chkCollegeGrad1" runat="server" GroupName="ElemGrad" Text="Yes"
                        Checked='<%# Bind("Collegegrad") %>' />
                    <asp:RadioButton ID="chkCollegeGrad2" runat="server" GroupName="ElemGrad" Text="No"
                        Checked="false" />
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtCollegeSubj" runat="server" MaxLength="100" Width="96%"
                        Text='<%# Bind("Collegesubj") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    Trade, Tech, Online
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtTradeName" runat="server" MaxLength="255" Width="96%"
                        Text='<%# Bind("Tradenameaddr") %>'>
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadNumericTextBox ID="txtTradeYears" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Tradeyears") %>' MinValue="0" MaxValue="12" Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <asp:RadioButton ID="chkTradeGrad1" runat="server" GroupName="ElemGrad" Text="Yes"
                        Checked='<%# Bind("Tradegrad") %>' />
                    <asp:RadioButton ID="chkTradeGrad2" runat="server" GroupName="ElemGrad" Text="No"
                        Checked="false" />
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtTradeSubj" runat="server" MaxLength="100" Width="96%"
                        Text='<%# Bind("Tradesubj") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
        </table>
        <br />
        <div class="lblTitle" style="font-size: large; background-color: Silver">
            4) GENERAL
        </div>
        <br />
        <table width="100%" style="table-layout: fixed">
            <tr>
                <td class="lblTitle">
                    Subjects of Special Study or Research Work
                    <br />
                    <telerik:RadTextBox ID="txtSpecialStudy" runat="server" Width="96%" MaxLength="255"
                        TextMode="MultiLine" Height="50" Rows="4" Text='<%# Bind("Specialstudyorresearch") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    Special Skills
                    <br />
                    <telerik:RadTextBox ID="txtSpecialSkills" runat="server" Width="96%" MaxLength="255"
                        TextMode="MultiLine" Height="50" Rows="4" Text='<%# Bind("Specialskills") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    Activities (Civic, Athletic, etc.)
                    <asp:Label ID="lblEthic" runat="server" SkinID="SubscriptLabel" 
                        Text="(Exclude organizations where the name of which indicate the race, creed, 
                            sex, age, marital status, color, or nation of origin of its members)">
                    </asp:Label>
                    <br />
                    <telerik:RadTextBox ID="txtActivities" runat="server" Width="96%" MaxLength="255"
                        TextMode="MultiLine" Height="50" Rows="4" Text='<%# Bind("Activities") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    U.S. Military Service
                    <telerik:RadTextBox ID="txtMilitary" runat="server" MaxLength="20" Width="200px"
                        Text='<%# Bind("Militaryservice") %>'>
                    </telerik:RadTextBox>
                    Rank
                    <telerik:RadTextBox ID="txtRank" runat="server" MaxLength="30" Width="200px" Text='<%# Bind("Rank") %>'>
                    </telerik:RadTextBox>
                    <br />
                    Present Membership in National Guard or Reserves
                    <telerik:RadTextBox ID="txtGuard" runat="server" MaxLength="20" Width="200px" Text='<%# Bind("Nationalguardorreserves") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
        </table>
        <br />
        <div class="lblTitle" style="font-size: large; background-color: Silver">
            5) FORMER EMPLOYERS
        </div>
        <br />
        <table width="100%" style="table-layout: fixed;" border="1">
            <tr>
                <td class="lblTitle" style="background-color: Silver; width: 160px">
                    Dates Employed
                </td>
                <td class="lblTitle" style="background-color: Silver; width: 30%">
                    Employer Name and Address
                </td>
                <td class="lblTitle" style="background-color: Silver; width: 80px">
                    Employer Phone
                </td>
                <td class="lblTitle" style="background-color: Silver">
                    Position
                </td>
                <td class="lblTitle" style="background-color: Silver; width: 7%">
                    Salary / Wage
                </td>
                <td class="lblTitle" style="background-color: Silver">
                    Reason for Leaving
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    From M/Y
                    <telerik:RadNumericTextBox ID="txtEmpFromMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Emp1frommonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="txtEmpFromYear" runat="server" NumberFormat-DecimalDigits="0"
                        NumberFormat-GroupSeparator="" Value='<%# Bind("Emp1fromyear") %>' MinValue="1970"
                        MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                    </telerik:RadNumericTextBox>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                    <telerik:RadNumericTextBox ID="txtEmpToMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Emp1tomonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="txtEmpToYear" runat="server" NumberFormat-DecimalDigits="0"
                        NumberFormat-GroupSeparator="" Value='<%# Bind("Emp1toyear") %>' MinValue="1970"
                        MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp1Name" runat="server" MaxLength="200" Width="96%" Text='<%# Bind("Emp1name") %>'
                        TextMode="MultiLine" Rows="2" Height="40px">
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadMaskedTextBox ID="txtEmp1Phone" runat="server" Width="96%" Mask="###-###-####"
                        DisplayMask="###-###-####" Text='<%# Bind("Emp1phone") %>'>
                    </telerik:RadMaskedTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp1Position" runat="server" MaxLength="30" Width="96%"
                        Text='<%# Bind("Emp1position") %>'>
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadNumericTextBox ID="txtEmp1Salary" runat="server" NumberFormat-DecimalDigits="2"
                        Value='<%# Bind("Emp1salary") %>' Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp1Reason" runat="server" MaxLength="100" Width="96%"
                        Text='<%# Bind("Emp1reason") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    From M/Y
                    <telerik:RadNumericTextBox ID="txtEmp2FromMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Emp2frommonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="txtEmp2FromYear" runat="server" NumberFormat-DecimalDigits="0"
                        NumberFormat-GroupSeparator="" Value='<%# Bind("Emp2fromyear") %>' MinValue="1970"
                        MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                    </telerik:RadNumericTextBox>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                    <telerik:RadNumericTextBox ID="txtEmp2ToMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Emp2tomonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="txtEmp2ToYear" runat="server" NumberFormat-DecimalDigits="0"
                        NumberFormat-GroupSeparator="" Value='<%# Bind("Emp2toyear") %>' MinValue="1970"
                        MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp2Name" runat="server" MaxLength="200" Width="96%" Text='<%# Bind("Emp2name") %>'
                        TextMode="MultiLine" Rows="2" Height="40px">
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadMaskedTextBox ID="txtEmp2Phone" runat="server" Width="96%" Mask="###-###-####"
                        DisplayMask="###-###-####" Text='<%# Bind("Emp2phone") %>'>
                    </telerik:RadMaskedTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp2Position" runat="server" MaxLength="30" Width="96%"
                        Text='<%# Bind("Emp2position") %>'>
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadNumericTextBox ID="txtEmp2Salary" runat="server" NumberFormat-DecimalDigits="2"
                        Value='<%# Bind("Emp2salary") %>' Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp2Reason" runat="server" MaxLength="100" Width="96%"
                        Text='<%# Bind("Emp2reason") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    From M/Y
                    <telerik:RadNumericTextBox ID="txtEmp3FromMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Emp3frommonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="txtEmp3FromYear" runat="server" NumberFormat-DecimalDigits="0"
                        NumberFormat-GroupSeparator="" Value='<%# Bind("Emp3fromyear") %>' MinValue="1970"
                        MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                    </telerik:RadNumericTextBox>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                    <telerik:RadNumericTextBox ID="txtEmp3ToMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Emp3tomonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="txtEmp3ToYear" runat="server" NumberFormat-DecimalDigits="0"
                        NumberFormat-GroupSeparator="" Value='<%# Bind("Emp3toyear") %>' MinValue="1970"
                        MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp3Name" runat="server" MaxLength="200" Width="96%" Text='<%# Bind("Emp3name") %>'
                        TextMode="MultiLine" Rows="2" Height="40px">
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadMaskedTextBox ID="txtEmp3Phone" runat="server" Width="96%" Mask="###-###-####"
                        DisplayMask="###-###-####" Text='<%# Bind("Emp3phone") %>'>
                    </telerik:RadMaskedTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp3Position" runat="server" MaxLength="30" Width="96%"
                        Text='<%# Bind("Emp3position") %>'>
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadNumericTextBox ID="txtEmp3Salary" runat="server" NumberFormat-DecimalDigits="2"
                        Value='<%# Bind("Emp3salary") %>' Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp3Reason" runat="server" MaxLength="100" Width="96%"
                        Text='<%# Bind("Emp3reason") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    From M/Y
                    <telerik:RadNumericTextBox ID="txtEmp4FromMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Emp4frommonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="txtEmp4FromYear" runat="server" NumberFormat-DecimalDigits="0"
                        NumberFormat-GroupSeparator="" Value='<%# Bind("Emp4fromyear") %>' MinValue="1970"
                        MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                    </telerik:RadNumericTextBox>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                    <telerik:RadNumericTextBox ID="txtEmp4ToMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Emp4tomonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="txtEmp4ToYear" runat="server" NumberFormat-DecimalDigits="0"
                        NumberFormat-GroupSeparator="" Value='<%# Bind("Emp4toyear") %>' MinValue="1970"
                        MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp4Name" runat="server" MaxLength="200" Width="96%" Text='<%# Bind("Emp4name") %>'
                        TextMode="MultiLine" Rows="2" Height="40px">
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadMaskedTextBox ID="txtEmp4Phone" runat="server" Width="96%" Mask="###-###-####"
                        DisplayMask="###-###-####" Text='<%# Bind("Emp4phone") %>'>
                    </telerik:RadMaskedTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp4Position" runat="server" MaxLength="30" Width="96%"
                        Text='<%# Bind("Emp4position") %>'>
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadNumericTextBox ID="txtEmp4Salary" runat="server" NumberFormat-DecimalDigits="2"
                        Value='<%# Bind("Emp4salary") %>' Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp4Reason" runat="server" MaxLength="100" Width="96%"
                        Text='<%# Bind("Emp4reason") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    From M/Y
                    <telerik:RadNumericTextBox ID="txtEmp5FromMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Emp5frommonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="txtEmp5FromYear" runat="server" NumberFormat-DecimalDigits="0"
                        NumberFormat-GroupSeparator="" Value='<%# Bind("Emp5fromyear") %>' MinValue="1970"
                        MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                    </telerik:RadNumericTextBox>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                    <telerik:RadNumericTextBox ID="txtEmp5ToMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Emp5tomonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="txtEmp5ToYear" runat="server" NumberFormat-DecimalDigits="0"
                        NumberFormat-GroupSeparator="" Value='<%# Bind("Emp5toyear") %>' MinValue="1970"
                        MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp5Name" runat="server" MaxLength="200" Width="96%" Text='<%# Bind("Emp5name") %>'
                        TextMode="MultiLine" Rows="2" Height="40px">
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadMaskedTextBox ID="txtEmp5Phone" runat="server" Width="96%" Mask="###-###-####"
                        DisplayMask="###-###-####" Text='<%# Bind("Emp5phone") %>'>
                    </telerik:RadMaskedTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp5Position" runat="server" MaxLength="30" Width="96%"
                        Text='<%# Bind("Emp5position") %>'>
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadNumericTextBox ID="txtEmp5Salary" runat="server" NumberFormat-DecimalDigits="2"
                        Value='<%# Bind("Emp5salary") %>' Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp5Reason" runat="server" MaxLength="100" Width="96%"
                        Text='<%# Bind("Emp5reason") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle">
                    From M/Y
                    <telerik:RadNumericTextBox ID="txtEmp6FromMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Emp6frommonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="txtEmp6FromYear" runat="server" NumberFormat-DecimalDigits="0"
                        NumberFormat-GroupSeparator="" Value='<%# Bind("Emp6fromyear") %>' MinValue="1970"
                        MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                    </telerik:RadNumericTextBox>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                    <telerik:RadNumericTextBox ID="txtEmp6ToMonth" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Emp6tomonth") %>' MinValue="1" MaxValue="12" Width="30px">
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="txtEmp6ToYear" runat="server" NumberFormat-DecimalDigits="0"
                        NumberFormat-GroupSeparator="" Value='<%# Bind("Emp6toyear") %>' MinValue="1970"
                        MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp6Name" runat="server" MaxLength="200" Width="96%" Text='<%# Bind("Emp6name") %>'
                        TextMode="MultiLine" Rows="2" Height="40px">
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadMaskedTextBox ID="txtEmp6Phone" runat="server" Width="96%" Mask="###-###-####"
                        DisplayMask="###-###-####" Text='<%# Bind("Emp6phone") %>'>
                    </telerik:RadMaskedTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp6Position" runat="server" MaxLength="30" Width="96%"
                        Text='<%# Bind("Emp6position") %>'>
                    </telerik:RadTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadNumericTextBox ID="txtEmp6Salary" runat="server" NumberFormat-DecimalDigits="2"
                        Value='<%# Bind("Emp6salary") %>' Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
                <td class="lblTitle">
                    <telerik:RadTextBox ID="txtEmp6Reason" runat="server" MaxLength="100" Width="96%"
                        Text='<%# Bind("Emp6reason") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="lblTitle" colspan="6">
                    Can we contact these employers?
                    <asp:RadioButton ID="chkContactEmp1" runat="server" GroupName="ContactEmp" Text="Yes"
                        Checked='<%# Bind("Cancontactemp") %>' />
                    <asp:RadioButton ID="chkContactEmp2" runat="server" GroupName="ContactEmp" Text="No"
                        Checked="false" />
                </td>
            </tr>
            <tr>
                <td class="lblTitle" colspan="6">
                    If no, please list and explain:
                    <br />
                    <telerik:RadTextBox ID="txtContactEmpReason" runat="server" MaxLength="100" Width="96%"
                        Text='<%# Bind("Cantcontactreason") %>'>
                    </telerik:RadTextBox>
                </td>
            </tr>
        </table>
        <br />
        <div class="lblTitle" style="font-size: large; background-color: Silver">
            6) BUSINESS / PERSONAL REFERENCES
        </div>
        <br />
        <table width="100%" style="table-layout: fixed;" border="1">
            <tr>
                <td class="lblTitle" style="background-color: Silver">
                    Name
                </td>
                <td class="lblTitle" style="background-color: Silver; width: 80px">
                    Phone Number
                </td>
                <td class="lblTitle" style="background-color: Silver">
                    Business
                </td>
                <td class="lblTitle" style="background-color: Silver; width: 80px">
                    Years Acquainted
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadTextBox ID="txtRef1Name" runat="server" MaxLength="30" Width="96%" Text='<%# Bind("Ref1name") %>'>
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadMaskedTextBox ID="txtRef1Phone" runat="server" Width="96%" Mask="###-###-####"
                        DisplayMask="###-###-####" Text='<%# Bind("Ref1phone") %>'>
                    </telerik:RadMaskedTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtRef1Business" runat="server" MaxLength="30" Width="96%"
                        Text='<%# Bind("Ref1business") %>'>
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="txtRef1Years" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Ref1years") %>' MinValue="1" MaxValue="100" Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadTextBox ID="txtRef2Name" runat="server" MaxLength="30" Width="96%" Text='<%# Bind("Ref2name") %>'>
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadMaskedTextBox ID="txtRef2Phone" runat="server" Width="96%" Mask="###-###-####"
                        DisplayMask="###-###-####" Text='<%# Bind("Ref2phone") %>'>
                    </telerik:RadMaskedTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtRef2Business" runat="server" MaxLength="30" Width="96%"
                        Text='<%# Bind("Ref2business") %>'>
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="txtRef2Years" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Ref2years") %>' MinValue="1" MaxValue="100" Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadTextBox ID="txtRef3Name" runat="server" MaxLength="30" Width="96%" Text='<%# Bind("Ref3name") %>'>
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadMaskedTextBox ID="txtRef3Phone" runat="server" Width="96%" Mask="###-###-####"
                        DisplayMask="###-###-####" Text='<%# Bind("Ref3phone") %>'>
                    </telerik:RadMaskedTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtRef3Business" runat="server" MaxLength="30" Width="96%"
                        Text='<%# Bind("Ref3business") %>'>
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="txtRef3Years" runat="server" NumberFormat-DecimalDigits="0"
                        Value='<%# Bind("Ref3years") %>' MinValue="1" MaxValue="100" Width="96%">
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
        </table>
        <br />
        <table width="100%" style="table-layout: fixed">
            <tr>
                <td class="lblTitle" style="width: 200px">
                    In Case of Emergency Notify
                </td>
                <td>
                    <telerik:RadTextBox ID="txtIceName" runat="server" MaxLength="60" Width="96%" Text='<%# Bind("Icecontactname") %>'>
                    </telerik:RadTextBox>
                    <br />
                    <asp:Label ID="lblICEName" runat="server" SkinID="SubscriptLabel" Text="Name"></asp:Label>
                </td>
                <td>
                    <telerik:RadTextBox ID="txtIceAddr" runat="server" MaxLength="100" Width="96%" Text='<%# Bind("Icecontactaddr") %>'>
                    </telerik:RadTextBox>
                    <br />
                    <asp:Label ID="lblICEAddr" runat="server" SkinID="SubscriptLabel" Text="Address"></asp:Label>
                </td>
                <td>
                    <telerik:RadMaskedTextBox ID="txtIcePhone" runat="server" Width="75px" Mask="###-###-####"
                        DisplayMask="###-###-####" Text='<%# Bind("Icecontactphone") %>'>
                    </telerik:RadMaskedTextBox>
                    <br />
                    <asp:Label ID="lblICEPhone" runat="server" SkinID="SubscriptLabel" Text="Phone Number"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <br />
    </div>
</div>
