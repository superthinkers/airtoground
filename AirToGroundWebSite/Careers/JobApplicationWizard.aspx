﻿<%@ Page Title="Air to Ground Careers" Language="C#" MasterPageFile="~/Careers/Default.master"
    AutoEventWireup="true" CodeFile="JobApplicationWizard.aspx.cs" Inherits="Careers_JobApplicationWizard"
    Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolderCareers" Runat="Server">
    <devart:DbLinqDataSource ID="LinqDataSource1" runat="server" 
        ContextTypeName="ATGDB.ATGDataContext" 
        EntityTypeName="ATGDB.Jobapplication" 
        TableName="Jobapplications" 
        Where="Jobapplicationid == Convert.ToInt32(@id)"
        EnableDelete="false" EnableUpdate="true" EnableInsert="true">
        <WhereParameters>
            <asp:Parameter Name="id" Type="Int32" DefaultValue="99999999" />
        </WhereParameters>
    </devart:DbLinqDataSource>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="true" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" Width="250" Height="75">
    </telerik:RadWindowManager>
    <telerik:RadScriptBlock ID="radScript1" runat="server">
        <script type="text/javascript">
            // RadNumeric, RadMasked
            function DoChangeColor(sender, args) {
                if ((sender.get_value() == null) || (sender.get_value() == "")) {
                    sender.get_styles().EnabledStyle[0] = "background-color: #F9C2BB";
                    sender.updateCssClass();
                } else {
                    sender.get_styles().EnabledStyle[0] = "background-color: White";
                    sender.updateCssClass();
                }
            }
            // RadDatePicker
            /*function DoDateChangeColor(sender, args) {
                if ((sender.get_selectedDate() == null) || (sender.get_selectedDate() == "")) {
                    sender.get_dateInput().get_styles().EnabledStyle[0] = "background-color: #F9C2BB";
                    sender.get_dateInput().updateCssClass();
                } else {
                    sender.get_dateInput().get_styles().EnabledStyle[0] = "background-color: White";
                    sender.get_dateInput().updateCssClass();
                }
            }*/
            function checkTextAreaMaxLength(textBox, e, maxLength) {
                if (!checkSpecialKeys(e)) {
                    if (textBox.value.length > maxLength - 1) {
                        if (window.event)//IE
                            e.returnValue = false;
                        else//Firefox
                            e.preventDefault();
                    }
                }
                onBlurTextCounter(textBox, maxLength);
            }
            function checkSpecialKeys(e) {
                if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
                    return false;
                else
                    return true;
            }
            function onBlurTextCounter(textBox, maxLength) {
                if (textBox.value.length > maxLength)
                    textBox.value = textBox.value.substr(0, maxLength);
            }
        </script>
    </telerik:RadScriptBlock>
    <div id="div_main">
        <br />
        <asp:Label ID="careersLblTitle" runat="server" Font-Size="Large" Text="Air to Ground Online Job Application" Width="100%"></asp:Label>
        <br />
        <!-- Main Grid -->
        <br />
        <asp:Wizard ID="Wizard1" runat="server" Visible="true" Width="100%" CancelDestinationPageUrl="http://www.airtoground.com"
            CancelButtonText="Quit" CancelButtonType="Button" DisplayCancelButton="true"
            DisplaySideBar="false" FinishCompleteButtonText="I'm Done, Submit" FinishCompleteButtonType="Button"
            OnFinishButtonClick="Wizard1_OnFinishButtonClick" BorderWidth="1px" StartNextButtonType="Button"
            SideBarStyle-Width="200px" SideBarStyle-HorizontalAlign="Left" SideBarStyle-VerticalAlign="Top"
            SideBarButtonStyle-Font-Names="Arial" SideBarButtonStyle-Font-Size="Smaller"
            SideBarStyle-Wrap="false" SideBarStyle-BackColor="Silver" SideBarStyle-BorderWidth="1px"
            OnActiveStepChanged="Wizard1_ActiveStepChanged" 
            OnSideBarButtonClick="Wizard1_SideBarButtonClick"
            OnNextButtonClick="Wizard1_OnNextButtonClick">
            <StartNavigationTemplate>
                <asp:Button ID="StartNextButton" runat="server" CommandName="MoveNext"
                    Text="Next" ValidationGroup="VAL" />
            </StartNavigationTemplate>
            <StepNavigationTemplate>
                <asp:Button ID="StepPreviousButton" runat="server" CausesValidation="false" CommandName="MovePrevious"
                    Text="Previous" />
                <asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next"
                    ValidationGroup="VAL" />
            </StepNavigationTemplate>
            <FinishNavigationTemplate>
                <asp:Button ID="FinishPreviousButton" runat="server" CausesValidation="false" CommandName="MovePrevious"
                    Text="Previous" />
                <asp:Button ID="FinishButton" runat="server" CommandName="MoveComplete" Text="I'm Done, Submit"
                    ValidationGroup="VAL" />
            </FinishNavigationTemplate>
            <WizardSteps>
                <asp:WizardStep ID="WizardStep1" runat="server" Title="Step 1: FAA Requirements" StepType="Start">
                    <!-- STEP 1 FAA NOTES -->
                    <%--<atg:FAANotes ID="ctrlFAANotes" runat="server" />--%>
                    <asp:DetailsView ID="grdApplication1" runat="server" DefaultMode="Insert"
                        DataKeyNames="Jobapplicationid" AutoGenerateRows="False" Width="100%">
                        <Fields>
                            <asp:TemplateField ControlStyle-BorderColor="Silver">
                                <InsertItemTemplate>
                                    <div class="lblInfoBold" style="text-align: left; margin: 10px 10px 10px 10px">
                                        <br />
                                        <div class="careersLblTitle" style="text-align: center; font-size: medium">
                                            AIR TO GROUND SERVICES, INC.<br />
                                            F.A.A. AND T.S.A. REGULATIONS
                                        </div>
                                        <br />
                                        <div class="careersLblTitle" style="text-align: center; border: 1px solid gray; font-weight: 900;
                                            font-size: medium">
                                            ATTENTION
                                        </div>
                                        <br />
                                        <div class="lblInfoBold" style="text-align: left; margin: 10px 10px 10px 10px">
                                            IN ORDER TO COMPLY WITH F.A.A. (Federal Aviation Administration) AND T.S.A. (Transportation
                                            Security Administration) REGULATIONS, AIR TO GROUND SERVICES, INC. IS REQUIRED TO
                                            OBTAIN THE FOLLOWING BACKGROUND INFORMATION:
                                            <ul>
                                                <li>PAST 10 YEARS OF EMPLOYMENT AND/OR EDUCATION HISTORY</li>
                                                <li>BACKGROUND CHECK (INCLUDING 10 YEAR FBI CRIMINAL CHECK AND FINGERPRINTING). PER
                                                    F.A.A. AND T.S.A. REGULATIONS IF YOU HAVE BEEN <strong>CONVICTED OF A FELONY</strong>
                                                    IN THE PAST 10 YEARS YOU ARE NOT ELIGIBLE TO WORK AT AIR TO GROUND SERVICES, INC.
                                                </li>
                                                <li>PER F.A.A. REGULATIONS (DOT 49 CFR PART 40 and FAA 14 CFR PART 120) YOU <strong>MUST</strong> SUBMIT TO
                                                    A NON-DOT or DOT REGULATED TYPE DRUG TEST FOR PRE-EMPLOYMENT. YOU WILL BE TESTED
                                                    FOR THE FOLLOWING SUBSTANCES: AMPHETAMINES, COCAINE, MARIJUANA, OPIATES AND PHENCYCLIDINE
                                                    (PCP). AFTER EMPLOYED, ALL EMPLOYEES WILL BE SUBJECT TO NON-DOT OR DOT REGULATED
                                                    DRUG AND/OR ALCOHOL TESTING: INCLUDING RANDOM, POST- ACCIDENT AND REASONABLE SUSPICION.</li>
                                                <li>YOU MUST BE 18 YEARS OF AGE OR OLDER.</li>
                                                <li>YOU MUST HAVE A VALID DRIVER'S LICENSE.</li>
                                            </ul>
                                            <div>
                                                <strong>Thank you for your interest in employment with our company. All applications
                                                    will be reviewed, and only the selected applicants will be called in for an interview.
                                                    Unfortunately, not every applicant will be called back. Please do not call to check
                                                    on the status of your application. Thank you!
                                                    <br />
                                                    <br />
                                                    Air to Ground Services, Inc.</strong>
                                            </div>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep2" runat="server" Title="Step 2: Drug / Alcohol Screening" StepType="Step">
                    <!-- STEP 2 DRUG SCREEN -->
                    <%--<atg:DrugScreenNotes ID="ctrlDrugScreenNotes" runat="server" />--%>
                    <asp:DetailsView ID="grdApplication2" runat="server" DefaultMode="Insert"
                        DataKeyNames="Jobapplicationid" AutoGenerateRows="False" Width="100%" OnItemInserting="grdApplication2_ItemInserting">
                        <Fields>
                            <asp:TemplateField ControlStyle-BorderColor="Silver">
                                <InsertItemTemplate>
                                    <div class="lblInfoBold" style="text-align: left; margin: 10px 10px 10px 10px">
                                        <br />
                                        <div class="careersLblTitle" style="text-align: center; font-size: medium">
                                            AIR TO GROUND SERVICES, INC.<br />
                                            DRUG-FREE WORKPLACE POLICY
                                        </div>
                                        <br />
                                        <div class="careersLblTitle" style="text-align: center; border: 1px solid gray; font-weight: 900;
                                            font-size: medium">
                                            APPLICANT'S ACKNOWLEDGEMENT FORM FOR ALCOHOL AND DRUG SCREENING
                                        </div>
                                        <br />
                                        <div class="lblInfo" style="text-align: left; font-size: medium">
                                            The Omnibus Transportation employee Testing Act of 1991 requires drug and alcohol
                                            testing on all safety-sensitive positions of employees in the aviation, trucking,
                                            railroads, mass transit, pipelines, and other transportation industries. Air To
                                            Ground Services, Inc. is federally regulated by the DOT 49 CFR Part 40 and FAA 14
                                            CFR Part 120 drug and alcohol testing regulations.
                                            <br />
                                            <br />
                                            I understand it is the policy of Air To Ground Services, Inc. to conduct pre-employment
                                            DOT or NON-DOT regulated drug and/or alcohol tests of job applicants for the purpose
                                            of detecting drug and/or alcohol abuse, and that one of the requirements for consideration
                                            of employment with the company is the satisfactory passing of the drug and/or alcohol
                                            tests(s).
                                            <br />
                                            <br />
                                            For the purpose of being further considered for employment, I hereby understand that
                                            I will be submitted to DOT or NON-DOT regulated type drug and/or alcohol testing.
                                            I understand that favorable test results will not necessarily guarantee that I will
                                            be employed by Air To Ground Services, Inc.
                                            <br />
                                            <br />
                                            I hereby acknowledge that such type drug and/or alcohol testing will be conducted
                                            by an authorized collection facility to collect breath and/or urine sample from
                                            me and to conduct necessary medical tests to determine the presence of use of alcohol,
                                            drugs, or controlled substances. Further, I understand for the release of the test
                                            results and other medical information to authorized company management (Drug Program
                                            Coordinator) for appropriate review. I also understand that, if I refuse to acknowledge,
                                            I will be denied employment and may not reapply.
                                            <br />
                                            <br />
                                            If you agree, do the following and continue to the next step:
                                            <br />
                                            <br />
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDrugPrintName" runat="server" MaxLength="80" Text='<%# Bind("Drugprintname") %>'
                                                            Font-Size="12px" BorderWidth="1px" Height="19px" Width="300px" ValidationGroup="VAL">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqDrugPrintName" runat="server" ControlToValidate="txtDrugPrintName"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterDrugName" runat="server" TargetControlID="txtDrugPrintName"
                                                            WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                        <br />
                                                        <asp:Label ID="lblDrugPrintName" runat="server" CssClass="lblSubscript" Text="Type Your Name"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDrugEmail" runat="server" MaxLength="80" Text='<%# Bind("Drugemail") %>'
                                                            Font-Size="12px" BorderWidth="1px" Height="19px" Width="300px" ValidationGroup="VAL" AutoPostBack="true">
                                                        </asp:TextBox>
                                                        <atg:EmailValidator CssClass="careersJobAppValidator" ID="reqDrugEmail" runat="server" ControlToValidate="txtDrugEmail"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></atg:EmailValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterDrugEmail" runat="server" TargetControlID="txtDrugEmail"
                                                            WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                        <br />
                                                        <asp:Label ID="lblDrugEmailAddress" runat="server" CssClass="lblSubscript" Text="Type Your Email Address"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <telerik:RadDatePicker ID="dtDrugDate" runat="server" SelectedDate='<%# Bind("Drugdate") %>'
                                                            Width="100px" MinDate='<%# DateTime.Now.Date %>' MaxDate='<%# DateTime.Now.Date %>'>
                                                            <DateInput ID="dtDrugDateInput" runat="server" BackColor="#F9C2BB">
                                                                <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                            </DateInput>
                                                        </telerik:RadDatePicker>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqDrugDate" runat="server" ControlToValidate="dtDrugDate"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:Label ID="lblDrugDate" runat="server" CssClass="lblSubscript" Text="Date"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep3" runat="server" Title="Step 3: Pre-Screening" StepType="Step">
                    <!-- STEP 3 PRE SCREEN -->
                    <%--<atg:PreScreen ID="ctrlPreScreen" runat="server" />--%>
                    <asp:DetailsView ID="grdApplication3" runat="server" DefaultMode="Insert"
                        DataKeyNames="Jobapplicationid" AutoGenerateRows="False" Width="100%" OnItemInserting="grdApplication3_ItemInserting">
                        <Fields>
                            <asp:TemplateField ControlStyle-BorderColor="Silver">
                                <InsertItemTemplate>
                                    <br />
                                    <div class="div_container">
                                        <div class="div_header">
                                            Pre-Screening Questions</div>
                                        <div style="margin: 10px 10px 10px 10px">
                                            <br />
                                            <div class="careersLblTitle" style="position: relative; left: 20%; text-align: center; font-size: medium;
                                                width: 60%">
                                                Welcome to Air to Ground! Thank you for taking the time to fill out an online job
                                                application.
                                                <br />
                                                Please note that this information is strictly confidential. Be as complete and as
                                                accurate as you can. If your application is selected, we will contact you for more
                                                information.
                                            </div>
                                            <br />
                                            <table width="100%" style="line-height: 20px">
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        Today's Date:&nbsp;
                                                        <asp:Label ID="lblToday" runat="server" Text='<%# Bind("Createdate") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        What is your name?
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table style="width: 50%">
                                                            <td style="width: 34%">
                                                                <asp:TextBox ID="txtLastName" runat="server" MaxLength="30" Font-Size="12px" BorderWidth="1px"
                                                                    Height="19px" Width="88%" Text='<%# Bind("Lastname") %>' ValidationGroup="VAL">
                                                                </asp:TextBox>
                                                                <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqLastName" runat="server" ControlToValidate="txtLastName"
                                                                    ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                                <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterLastName" runat="server"
                                                                    TargetControlID="txtLastName" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                                </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                                <br />
                                                                <asp:Label ID="lblLastName" runat="server" CssClass="lblSubscript" Text="Last Name"></asp:Label>
                                                            </td>
                                                            <td style="width: 33%">
                                                                <asp:TextBox ID="txtFirstName" runat="server" MaxLength="30" Font-Size="12px" BorderWidth="1px"
                                                                    Height="19px" Width="88%" Text='<%# Bind("Firstname") %>' ValidationGroup="VAL">
                                                                </asp:TextBox>
                                                                <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqFirstName" runat="server" ControlToValidate="txtFirstName"
                                                                    ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                                <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterFirstName" runat="server"
                                                                    TargetControlID="txtFirstName" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                                </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                                <br />
                                                                <asp:Label ID="lblFirstName" runat="server" CssClass="lblSubscript" Text="First Name"></asp:Label>
                                                            </td>
                                                            <td style="width: 33%">
                                                                <telerik:RadTextBox ID="txtMiddleName" runat="server" MaxLength="30" Width="88%"
                                                                    Text='<%# Bind("Middlename") %>'>
                                                                </telerik:RadTextBox>
                                                                <br />
                                                                <asp:Label ID="lblMiddleName" runat="server" CssClass="lblSubscript" Text="Middle Name"></asp:Label>
                                                            </td>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        1) Are you currently employed?
                                                        <asp:RadioButton ID="chkCurrentlyEmployed1" runat="server" GroupName="CurEmp" Text="Yes"
                                                            Checked='<%# Bind("Iscurrentlyemployed") %>' ValidationGroup="VAL" />
                                                        <asp:RadioButton ID="chkCurrentlyEmployed2" runat="server" GroupName="CurEmp" Text="No"
                                                            Checked="false" ValidationGroup="VAL" />
                                                        <atg:DualRadioValidator CssClass="careersJobAppValidator" ID="reqCurEmp" runat="server" ErrorMessage="*" ToolTip="Select Yes or No"
                                                            Display="Static" SetFocusOnError="true" ValidationGroup="VAL" 
                                                            RadioControl1ToValidate="chkCurrentlyEmployed1" RadioControl2ToValidate="chkCurrentlyEmployed2" ></atg:DualRadioValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        If you are currently employed, are you looking for new employment or a 2nd job?
                                                        <br />
                                                        <asp:RadioButton ID="chkIsSecondJob1" runat="server" GroupName="Second" Text="This is New Employment"
                                                            Checked="false" />
                                                        <asp:RadioButton ID="chkIsSecondJob2" runat="server" GroupName="Second" Text="This is a 2nd Job"
                                                            Checked='<%# Bind("Issecondjob") %>' />
                                                        <atg:RadioRadioArrayValidator CssClass="careersJobAppValidator" ID="reqSecondJob" runat="server" ErrorMessage="*"
                                                            ToolTip="Select Yes or No" Display="Static" SetFocusOnError="true"
                                                            ValidationGroup="VAL" 
                                                            RadioControlToValidate="chkCurrentlyEmployed1" 
                                                            RadioControlsToValidate="chkIsSecondJob1,chkIsSecondJob2"></atg:RadioRadioArrayValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        2) What days of the week are you available to work?
                                                        <asp:CheckBox ID="chkMon" runat="server" Text="Mon" Checked='<%# Bind("Canworkmonday") %>' />
                                                        <asp:CheckBox ID="chkTues" runat="server" Text="Tue" Checked='<%# Bind("Canworktuesday") %>' />
                                                        <asp:CheckBox ID="chkWed" runat="server" Text="Wed" Checked='<%# Bind("Canworkwednesday") %>' />
                                                        <asp:CheckBox ID="chkThurs" runat="server" Text="Thur" Checked='<%# Bind("Canworkthursday") %>' />
                                                        <asp:CheckBox ID="chkFri" runat="server" Text="Fri" Checked='<%# Bind("Canworkfriday") %>' />
                                                        <asp:CheckBox ID="chkSat" runat="server" Text="Sat" Checked='<%# Bind("Canworksaturday") %>' />
                                                        <asp:CheckBox ID="chkSun" runat="server" Text="Sun" Checked='<%# Bind("Canworksunday") %>' />
                                                        <atg:CheckBoxArrayValidator CssClass="careersJobAppValidator" ID="reqWorkDaysOfWeek"
                                                            runat="server" ErrorMessage="*" ToolTip="Select at Least One Day" Display="Static"
                                                            SetFocusOnError="true" ValidationGroup="VAL" 
                                                            CheckBoxControlsToValidate="chkMon,chkTues,chkWed,chkThurs,chkFri,chkSat,chkSun"></atg:CheckBoxArrayValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        3) Can you work First, Second, or Third Shifts?
                                                        <asp:CheckBox ID="chkFirst" runat="server" Text="First Shift?" Checked='<%# Bind("Canworkfirstshift") %>' />
                                                        <asp:CheckBox ID="chkSecond" runat="server" Text="Second Shift?" Checked='<%# Bind("Canworksecondshift") %>' />
                                                        <asp:CheckBox ID="chkThird" runat="server" Text="Third Shift?" Checked='<%# Bind("Canworkthirdshift") %>' />
                                                        <atg:CheckBoxArrayValidator CssClass="careersJobAppValidator" ID="reqShifts"
                                                            runat="server" ErrorMessage="*" ToolTip="Select at Least One Shift" Display="Static"
                                                            SetFocusOnError="true" ValidationGroup="VAL" 
                                                            CheckBoxControlsToValidate="chkFirst,chkSecond,chkThird"></atg:CheckBoxArrayValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        4) What type of work experience do you have?
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        <asp:TextBox ID="txtExperience" runat="server" Width="90%" MaxLength="255" TextMode="MultiLine"
                                                            Font-Size="12px" BorderWidth="1px" Height="50" Rows="4" Text='<%# Bind("WorkExperience") %>'
                                                            ValidationGroup="VAL" onkeyDown="checkTextAreaMaxLength(this,event,'255');" onblur="onBlurTextCounter(this,'255');">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqExperience"
                                                            runat="server" ControlToValidate="txtExperience" ErrorMessage="*" Display="Static"
                                                            ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterExperience" runat="server"
                                                            TargetControlID="txtExperience" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        5) Are you looking for Full or Part-time employment?
                                                        <telerik:RadComboBox ID="lstFullOrPT" runat="server" Width="150px" DataValueField="Fulltimeorparttime">
                                                            <Items>
                                                                <telerik:RadComboBoxItem Value="F" Text="Full-Time" />
                                                                <telerik:RadComboBoxItem Value="P" Text="Part-Time" />
                                                                <telerik:RadComboBoxItem Value="B" Text="Both" />
                                                            </Items>
                                                        </telerik:RadComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        6) Do you attend any trade or technical schools?
                                                        <asp:RadioButton ID="chkAttendSchool1" runat="server" GroupName="Attend" Text="Yes"
                                                            Checked='<%# Bind("Isattendingschool") %>' />
                                                        <asp:RadioButton ID="chkAttendSchool2" runat="server" GroupName="Attend" Text="No"
                                                            Checked="false" />
                                                        <atg:DualRadioValidator CssClass="careersJobAppValidator" ID="reqAttSchool" runat="server" ErrorMessage="*"
                                                            ToolTip="Select Yes or No" Display="Static" SetFocusOnError="true"
                                                            ValidationGroup="VAL" RadioControl1ToValidate="chkAttendSchool1" RadioControl2ToValidate="chkAttendSchool2"></atg:DualRadioValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        If yes, what school and when do you attend classes?
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        <asp:TextBox ID="txtSchools" runat="server" Width="90%" MaxLength="255" TextMode="MultiLine"
                                                            Font-Size="12px" BorderWidth="1px" Height="50" Rows="4" Text='<%# Bind("Schoolsandschedule") %>'
                                                            ValidationGroup="VAL" onkeyDown="checkTextAreaMaxLength(this,event,'255');" onblur="onBlurTextCounter(this,'255');">
                                                        </asp:TextBox>
                                                        <atg:RadioTextBoxValidator CssClass="careersJobAppValidator" ID="reqSchoolsText" runat="server" ErrorMessage="*"
                                                            Display="Static" SetFocusOnError="true" ValidationGroup="VAL" TextBoxWatermarkExtender="waterSchools" 
                                                            RadioControlToValidate="chkAttendSchool1" TextBoxToValidate="txtSchools"></atg:RadioTextBoxValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterSchools" runat="server" Enabled="false" 
                                                            TargetControlID="txtSchools" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        7) Do you have any heavy equipment operating experience?
                                                        <asp:RadioButton ID="chkHeavyEquip1" runat="server" GroupName="Heavy" Text="Yes"
                                                            Checked='<%# Bind("Hasheavyequipexp") %>' />
                                                        <asp:RadioButton ID="chkHeavyEquip2" runat="server" GroupName="Heavy" Text="No" Checked="false" />
                                                        <atg:DualRadioValidator CssClass="careersJobAppValidator" ID="reqHeavEquip" runat="server" ErrorMessage="*"
                                                            ToolTip="Select Yes or No" Display="Static" SetFocusOnError="true"
                                                            ValidationGroup="VAL" RadioControl1ToValidate="chkHeavyEquip1" RadioControl2ToValidate="chkHeavyEquip2"></atg:DualRadioValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        If yes, what types of equipment?
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        <asp:TextBox ID="txtHeavyExp" runat="server" Width="90%" MaxLength="255" TextMode="MultiLine"
                                                            Font-Size="12px" BorderWidth="1px" Height="50" Rows="4" Text='<%# Bind("Heavyequipexp") %>'
                                                            ValidationGroup="VAL" onkeyDown="checkTextAreaMaxLength(this,event,'255');" onblur="onBlurTextCounter(this,'255');">
                                                        </asp:TextBox>
                                                        <atg:RadioTextBoxValidator CssClass="careersJobAppValidator" ID="reqHeavyEquipText"
                                                            runat="server" ErrorMessage="*" Display="Static" SetFocusOnError="true"
                                                            ValidationGroup="VAL" RadioControlToValidate="chkHeavyEquip1" TextBoxToValidate="txtHeavyExp"
                                                            TextBoxWatermarkExtender="waterHeavyExp"></atg:RadioTextBoxValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterHeavyExp" runat="server" Enabled="false"
                                                            TargetControlID="txtHeavyExp" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        8) Do you have a valid driver's license?
                                                        <asp:RadioButton ID="chkValidLic1" runat="server" GroupName="Lic" Text="Yes" Checked='<%# Bind("Hasvaliddriverslicense") %>' />
                                                        <asp:RadioButton ID="chkValidLic2" runat="server" GroupName="Lic" Text="No" Checked="false" />
                                                        <atg:DualRadioValidator CssClass="careersJobAppValidator" ID="reqValidLic" runat="server" ErrorMessage="*"
                                                            ToolTip="Select Yes or No" Display="Static" SetFocusOnError="true"
                                                            ValidationGroup="VAL" RadioControl1ToValidate="chkValidLic1" RadioControl2ToValidate="chkValidLic2"></atg:DualRadioValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        9) Do you have reliable transportation (i.e. own a car, bus, etc.)?
                                                        <asp:RadioButton ID="chkTrans1" runat="server" GroupName="Trans" Text="Yes" Checked='<%# Bind("Hastransportation") %>' />
                                                        <asp:RadioButton ID="chkTrans2" runat="server" GroupName="Trans" Text="No" Checked="false" />
                                                        <atg:DualRadioValidator CssClass="careersJobAppValidator" ID="reqTrans" runat="server" ErrorMessage="*"
                                                            ToolTip="Select Yes or No" Display="Static" SetFocusOnError="true"
                                                            ValidationGroup="VAL" RadioControl1ToValidate="chkTrans1" RadioControl2ToValidate="chkTrans2"></atg:DualRadioValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep4" runat="server" Title="Step 4: Job Application" StepType="Step">
                    <!-- STEP 4 JOB APP -->
                    <%--<atg:JobApplication ID="ctrlJobApp" runat="server" />--%>
                    <asp:DetailsView ID="grdApplication4" runat="server" DefaultMode="Insert"
                        DataKeyNames="Jobapplicationid" AutoGenerateRows="False" Width="100%" OnItemInserting="grdApplication4_ItemInserting">
                        <Fields>
                            <asp:TemplateField ControlStyle-BorderColor="Silver">
                                <InsertItemTemplate>
                                    <br />
                                    <div class="div_container">
                                        <div class="div_header">
                                            Application For Employment</div>
                                        <div style="margin: 10px 10px 10px 10px">
                                            <div class="careersLblTitle" style="text-align: center; font-size: medium">
                                                Air to Ground Job Application
                                            </div>
                                            <br />
                                            <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                                1) PERSONAL INFORMATION
                                            </div>
                                            <br />
                                            <!-- PERSONAL -->
                                            <table width="100%" style="table-layout: fixed; vertical-align: middle">
                                                <%--<tr>
                                                    <td class="careersLblTitle">
                                                        Social Security Number
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadMaskedTextBox ID="txtSSN" runat="server" MaxLength="11" Mask="###-##-####"
                                                            DisplayMask="###-##-####" Width="80px" Text='<%# Bind("Ssn") %>' ValidationGroup="VAL">
                                                        </telerik:RadMaskedTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqSSN" runat="server" ControlToValidate="txtSSN"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        Present Address
                                                        <table style="width: 80%">
                                                            <tr>
                                                                <td style="width: 60%">
                                                                    <asp:TextBox ID="txtPresAddr" runat="server" MaxLength="80" Font-Size="12px" BorderWidth="1px"
                                                                        Height="19px" Width="90%" Text='<%# Bind("Currentaddress") %>' ValidationGroup="VAL">
                                                                    </asp:TextBox>
                                                                    <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqPresAddr" runat="server" ControlToValidate="txtPresAddr"
                                                                        ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                    <asp:Label ID="lblPresAddr" runat="server" CssClass="lblSubscript" Text="Street"></asp:Label>
                                                                    <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterPresAddr" runat="server" TargetControlID="txtPresAddr"
                                                                        WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                                    </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <asp:TextBox ID="txtPresCity" runat="server" MaxLength="50" Font-Size="12px" BorderWidth="1px" 
                                                                        Height="19px" Width="80%" Text='<%# Bind("Currentcity") %>'
                                                                        ValidationGroup="VAL">
                                                                    </asp:TextBox>
                                                                    <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqPresCity" runat="server" ControlToValidate="txtPresCity"
                                                                        ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                    <asp:Label ID="lblPresCity" runat="server" CssClass="lblSubscript" Text="City"></asp:Label>
                                                                    <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterPresCity" runat="server" TargetControlID="txtPresCity"
                                                                        WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                                    </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:TextBox ID="txtPresSt" runat="server" MaxLength="2" Font-Size="12px" BorderWidth="1px" Height="19px" Width="70%" 
                                                                        Text='<%# Bind("Currentstate") %>' ValidationGroup="VAL">
                                                                    </asp:TextBox>
                                                                    <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqPresSt" runat="server" ControlToValidate="txtPresSt"
                                                                        ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                    <asp:Label ID="lblPresSt" runat="server" CssClass="lblSubscript" Text="State"></asp:Label>
                                                                    <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterPresSt" runat="server"
                                                                        TargetControlID="txtPresSt" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                                    </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                                </td>
                                                                <td>
                                                                    <telerik:RadNumericTextBox ID="txtPresZip" runat="server" MinValue="10000" MaxLength="5"
                                                                        Width="70%" NumberFormat-GroupSeparator="" Text='<%# Bind("Currentzip") %>' NumberFormat-DecimalDigits="0"
                                                                        ValidationGroup="VAL" BackColor="White">
                                                                        <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor"/>
                                                                    </telerik:RadNumericTextBox>
                                                                    <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqPresZip" runat="server" ControlToValidate="txtPresZip"
                                                                        ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                    <asp:Label ID="lblPresZip" runat="server" CssClass="lblSubscript" Text="Zip"></asp:Label>
                                                                    <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterPresZip" runat="server"
                                                                        TargetControlID="txtPresZip" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                                    </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        Permanent Address
                                                        <table style="width: 80%">
                                                            <tr>
                                                                <td style="width: 60%">
                                                                    <asp:TextBox ID="txtPermAddr" runat="server" MaxLength="80" Font-Size="12px" BorderWidth="1px"
                                                                        Height="19px" Width="90%" Text='<%# Bind("Permaddress") %>' ValidationGroup="VAL">
                                                                    </asp:TextBox>
                                                                    <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqPermAddr" runat="server" ControlToValidate="txtPermAddr"
                                                                        ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                    <asp:Label ID="lblPermAddr" runat="server" CssClass="lblSubscript" Text="Street"></asp:Label>
                                                                    <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterPermAddr" runat="server"
                                                                        TargetControlID="txtPermAddr" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                                    </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <asp:TextBox ID="txtPermCity" runat="server" MaxLength="50" Font-Size="12px" BorderWidth="1px"
                                                                        Height="19px" Width="80%" Text='<%# Bind("Permcity") %>' ValidationGroup="VAL">
                                                                    </asp:TextBox>
                                                                    <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqPermCity" runat="server" ControlToValidate="txtPermCity"
                                                                        ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                    <asp:Label ID="lblPermCity" runat="server" CssClass="lblSubscript" Text="City"></asp:Label>
                                                                    <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterPermCity" runat="server"
                                                                        TargetControlID="txtPermCity" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                                    </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:TextBox ID="txtPermState" runat="server" MaxLength="2" Font-Size="12px" BorderWidth="1px"
                                                                        Height="19px" Width="70%" Text='<%# Bind("Permstate") %>' ValidationGroup="VAL">
                                                                    </asp:TextBox>
                                                                    <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqPermState" runat="server" ControlToValidate="txtPermState"
                                                                        ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                    <asp:Label ID="lblPermState" runat="server" CssClass="lblSubscript" Text="State"></asp:Label>
                                                                    <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterPermState" runat="server"
                                                                        TargetControlID="txtPermState" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                                    </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                                </td>
                                                                <td>
                                                                    <telerik:RadNumericTextBox ID="txtPermZip" runat="server" MinValue="10000" MaxLength="5"
                                                                        Width="70%" NumberFormat-GroupSeparator="" Text='<%# Bind("Permzip") %>' NumberFormat-DecimalDigits="0"
                                                                        ValidationGroup="VAL" BackColor="#F9C2BB">
                                                                        <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                                    </telerik:RadNumericTextBox>
                                                                    <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqPermZip" runat="server" ControlToValidate="txtPermZip"
                                                                        ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                    <asp:Label ID="lblPermZip" runat="server" CssClass="lblSubscript" Text="Zip"></asp:Label>
                                                                    <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterPermZip" runat="server"
                                                                        TargetControlID="txtPermZip" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                                    </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        Phone No
                                                        <telerik:RadMaskedTextBox ID="txtPhoneNumber" runat="server" Width="80px" Mask="###-###-####"
                                                            DisplayMask="###-###-####" Text='<%# Bind("Phonenumber") %>' ValidationGroup="VAL"
                                                            OnLoad="Phone_OnLoad">
                                                            <ClientEvents OnValueChanged="DoChangeColor"/>
                                                        </telerik:RadMaskedTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqPhoneNumber" runat="server" ControlToValidate="txtPhoneNumber"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        &nbsp;Are you 18 years old or older?&nbsp;
                                                        <asp:RadioButton ID="chk18YearsOld1" runat="server" GroupName="18Years" Text="Yes"
                                                            Checked='<%# Bind("Is18yearsold") %>' ValidationGroup="VAL" />
                                                        <asp:RadioButton ID="chk18YearsOld2" runat="server" GroupName="18Years" Text="No"
                                                            Checked="false" ValidationGroup="VAL" />
                                                        <atg:DualRadioValidator CssClass="careersJobAppValidator" ID="req18YearsOldRadio" runat="server" ErrorMessage="*" ToolTip="Select Yes or No"
                                                            Display="Static" SetFocusOnError="true" ValidationGroup="VAL"
                                                            RadioControl1ToValidate="chk18YearsOld1" RadioControl2ToValidate="chk18YearsOld2"></atg:DualRadioValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        Do you have any physical limitations that would prevent you from doing this job?
                                                        <asp:RadioButton ID="chkPhysical1" runat="server" GroupName="Physical" Text="Yes"
                                                            Checked='<%# Bind("Hasphysicallimitations") %>' ValidationGroup="VAL" />
                                                        <asp:RadioButton ID="chkPhysical2" runat="server" GroupName="Physical" Text="No"
                                                            Checked="false" ValidationGroup="VAL" />
                                                        <atg:DualRadioValidator CssClass="careersJobAppValidator" ID="reqPhysicalRadio" runat="server" ErrorMessage="*" ToolTip="Select Yes or No"
                                                            Display="Static" SetFocusOnError="true" ValidationGroup="VAL"
                                                            RadioControl1ToValidate="chkPhysical1" RadioControl2ToValidate="chkPhysical2"></atg:DualRadioValidator>
                                                        <br />
                                                        If Yes, please explain:
                                                        <br />
                                                        <asp:TextBox ID="txtPhysical" runat="server" Width="90%" MaxLength="255" TextMode="MultiLine"
                                                            Font-Size="12px" BorderWidth="1px" Height="50" Rows="4" Text='<%# Bind("Physicallimitations") %>'
                                                            ValidationGroup="VAL" onkeyDown="checkTextAreaMaxLength(this,event,'255');" onblur="onBlurTextCounter(this,'255');">
                                                        </asp:TextBox>
                                                        <atg:RadioTextBoxValidator CssClass="careersJobAppValidator" ID="reqPhysicalText"
                                                            runat="server" ErrorMessage="*" Display="Static" SetFocusOnError="true"
                                                            ValidationGroup="VAL" RadioControlToValidate="chkPhysical1" TextBoxToValidate="txtPhysical"
                                                            TextBoxWatermarkExtender="waterPhysical"></atg:RadioTextBoxValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterPhysical" runat="server" Enabled="false"
                                                            TargetControlID="txtPhysical" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                                2) EMPLOYMENT DESIRED
                                            </div>
                                            <br />
                                            <table style="table-layout: fixed">
                                                <tr>
                                                    <td class="careersLblTitle" style="width: 17%">
                                                        Position Desired
                                                        <br />
                                                        <asp:TextBox ID="txtPosition" runat="server" MaxLength="50" Font-Size="12px" BorderWidth="1px"
                                                            Height="19px" Width="85%" Text='<%# Bind("Positiondesired") %>' ValidationGroup="VAL">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqPosition" runat="server" ControlToValidate="txtPosition"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterPosition" runat="server"
                                                            TargetControlID="txtPosition" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                    <td class="careersLblTitle" style="width: 17%">
                                                        Preferred Work Location
                                                        <br />
                                                        <telerik:RadComboBox ID="lstPreferredLocation" runat="server" Width="85%"
                                                            Text='<%# Bind("Preferredworklocation") %>' ValidationGroup="VAL">
                                                            <Items>
                                                                <telerik:RadComboBoxItem Value="" Text="" />
                                                                <telerik:RadComboBoxItem Value="Indianapolis" Text="Indianapolis" />
                                                                <telerik:RadComboBoxItem Value="Los Angeles" Text="Los Angeles" />
                                                                <telerik:RadComboBoxItem Value="Louisville" Text="Louisville" />
                                                                <telerik:RadComboBoxItem Value="Memphis" Text="Memphis" />
                                                            </Items>
                                                        </telerik:RadComboBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqPrefLoc" runat="server" ControlToValidate="lstPreferredLocation"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="careersLblTitle" style="width: 17%">
                                                        Alternate Work Location
                                                        <br />
                                                        <telerik:RadComboBox ID="lstAlternateLocation" runat="server" Width="85%" 
                                                            Text='<%# Bind("Alternateworklocation") %>' ValidationGroup="VAL"> 
                                                            <Items>
                                                                <telerik:RadComboBoxItem Value="" Text="" />
                                                                <telerik:RadComboBoxItem Value="Any" Text="Any" />
                                                                <telerik:RadComboBoxItem Value="Indianapolis" Text="Indianapolis" />
                                                                <telerik:RadComboBoxItem Value="Los Angeles" Text="Los Angeles" />
                                                                <telerik:RadComboBoxItem Value="Louisville" Text="Louisville" />
                                                                <telerik:RadComboBoxItem Value="Memphis" Text="Memphis" />
                                                            </Items>
                                                        </telerik:RadComboBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqAltLoc" runat="server" ControlToValidate="lstAlternateLocation"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="careersLblTitle" style="width: 17%">
                                                        <asp:CheckBox ID="chkDay" runat="server" Text="Days" Checked='<%# Bind("Wantsday") %>' />
                                                        <asp:CheckBox ID="chkNight" runat="server" Text="Nights" Checked='<%# Bind("Wantsnight") %>' />
                                                        <atg:CheckBoxArrayValidator CssClass="careersJobAppValidator" ID="reqWorkDayOrNight" runat="server"
                                                            ErrorMessage="*" ToolTip="Select at Least One Day" Display="Static" 
                                                            SetFocusOnError="true" ValidationGroup="VAL" 
                                                            CheckBoxControlsToValidate="chkDay,chkNight"></atg:CheckBoxArrayValidator>
                                                    </td>
                                                    <td class="careersLblTitle" style="width: 17%">
                                                        Date You Can Start
                                                        <br />
                                                        <telerik:RadDatePicker ID="dtEmpStartDate" runat="server" SelectedDate='<%# Bind("Datecanstart") %>'
                                                            Width="100px" MinDate='<%# DateTime.Now.Date %>'>
                                                            <DateInput ID="dtEmpStartDateInput" runat="server" BackColor="White">
                                                                <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                            </DateInput>
                                                        </telerik:RadDatePicker>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqEmpStartDate" runat="server" ControlToValidate="dtEmpStartDate"
                                                            ErrorMessage="*" Display="Static" SetFocusOnError="true" ValidationGroup="VAL"
                                                            OnInit="reqRFV_OnInit">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        Salary / Wage Desired  
                                                        <br />                 
                                                        <telerik:RadNumericTextBox ID="txtSalaryReq" runat="server" NumberFormat-DecimalDigits="2"
                                                            Value='<%# Bind("Desiredsalary") %>' Width="80px" ValidationGroup="VAL" BackColor="White">
                                                            <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                        </telerik:RadNumericTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqSalaryReq" runat="server" ControlToValidate="txtSalaryReq"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle" colspan="2">
                                                        Have You Ever Applied to This Company Before?
                                                        <asp:RadioButton ID="chkApplied1" runat="server" GroupName="Applied" Text="Yes" Checked='<%# Bind("Hasappliedbefore") %>' ValidationGroup="VAL" />
                                                        <asp:RadioButton ID="chkApplied2" runat="server" GroupName="Applied" Text="No" Checked="false" ValidationGroup="VAL" />
                                                        <atg:DualRadioValidator CssClass="careersJobAppValidator" ID="reqApplied" runat="server" ErrorMessage="*" ToolTip="Select Yes or No"
                                                            Display="Static" SetFocusOnError="true" ValidationGroup="VAL"
                                                            RadioControl1ToValidate="chkApplied1" RadioControl2ToValidate="chkApplied2"></atg:DualRadioValidator>
                                                    </td>
                                                    <td class="careersLblTitle" colspan="4">
                                                        Referred By
                                                        <telerik:RadTextBox ID="txtReferredBy" runat="server" MaxLength="50" Width="150px"
                                                            Text='<%# Bind("Referredby") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle" colspan="6">
                                                        Have you ever been convicted of, plead guilty or no contest to any criminal violation
                                                        of law, including criminal traffic offences? (A conviction does not automatically
                                                        mean you cannot be hired. Provide all facts.)?
                                                        <asp:RadioButton ID="chkPleadGuilty1" runat="server" GroupName="Guilty" Text="Yes"
                                                            Checked='<%# Bind("Haspleadguilty") %>' ValidationGroup="VAL" />
                                                        <asp:RadioButton ID="chkPleadGuilty2" runat="server" GroupName="Guilty" Text="No"
                                                            Checked="false" ValidationGroup="VAL" />
                                                        <atg:DualRadioValidator CssClass="careersJobAppValidator" ID="reqGuiltyRadio" runat="server" ErrorMessage="*" ToolTip="Select Yes or No"
                                                            Display="Static" SetFocusOnError="true" ValidationGroup="VAL"
                                                            RadioControl1ToValidate="chkPleadGuilty1" RadioControl2ToValidate="chkPleadGuilty2"></atg:DualRadioValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle" colspan="6">
                                                        If Yes, please explain:
                                                        <br />
                                                        <asp:TextBox ID="txtGuiltyReason" runat="server" Width="90%" MaxLength="255" TextMode="MultiLine"
                                                            Font-Size="12px" BorderWidth="1px" Height="50" Rows="4" Text='<%# Bind("Guiltyreason") %>'
                                                            ValidationGroup="VAL" onkeyDown="checkTextAreaMaxLength(this,event,'255');" onblur="onBlurTextCounter(this,'255');">
                                                        </asp:TextBox>
                                                        <atg:RadioTextBoxValidator CssClass="careersJobAppValidator" ID="reqGuiltyText" runat="server"
                                                            ErrorMessage="*" Display="Static" SetFocusOnError="true" ValidationGroup="VAL"
                                                            RadioControlToValidate="chkPleadGuilty1" TextBoxToValidate="txtGuiltyReason"
                                                            TextBoxWatermarkExtender="waterGuiltyReason"></atg:RadioTextBoxValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterGuiltyReason" runat="server" Enabled="false"
                                                            TargetControlID="txtGuiltyReason" WatermarkCssClass="careersJobAppWatermark"
                                                            WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle" colspan="2">
                                                        Have You Ever Been Convicted of a Felony?
                                                        <asp:RadioButton ID="chkFelony1" runat="server" GroupName="Felony" Text="Yes" 
                                                            Checked='<%# Bind("Isconvictedofafelony") %>' ValidationGroup="VAL" />
                                                        <asp:RadioButton ID="chkFelony2" runat="server" GroupName="Felony" Text="No" 
                                                            Checked="false" ValidationGroup="VAL" />
                                                        <atg:DualRadioValidator CssClass="careersJobAppValidator" ID="reqFelonyRadio" runat="server" ErrorMessage="*" ToolTip="Select Yes or No"
                                                            Display="Static" SetFocusOnError="true" ValidationGroup="VAL"
                                                            RadioControl1ToValidate="chkFelony1" RadioControl2ToValidate="chkFelony2"></atg:DualRadioValidator>
                                                    </td>
                                                    <td class="careersLblTitle" colspan="4" style="text-align: left">
                                                        If Yes, When? Month
                                                        <telerik:RadNumericTextBox ID="txtFelonyMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Felonymonth") %>' MinValue="1" MaxValue="12" Width="30px">
                                                        </telerik:RadNumericTextBox>
                                                        Year
                                                        <telerik:RadNumericTextBox ID="txtFelonyYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Felonyyear") %>' MinValue="1970" MaxValue='<%# DateTime.Now.Year %>'
                                                            Width="40px" NumberFormat-GroupSeparator="">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                                3) EDUCATION
                                            </div>
                                            <br />
                                            <table width="100%" style="table-layout: fixed; border-color: Silver" border="1">
                                                <tr>
                                                    <td class="careersLblTitle" style="background-color: Silver; width: 150px">
                                                        Education
                                                    </td>
                                                    <td class="careersLblTitle" style="background-color: Silver; width: 40%">
                                                        Name and Address of School
                                                    </td>
                                                    <td class="careersLblTitle" style="background-color: Silver; width: 80px">
                                                        # of Years Attended
                                                    </td>
                                                    <td class="careersLblTitle" style="background-color: Silver; width: 120px">
                                                        Did You Graduate?
                                                    </td>
                                                    <td class="careersLblTitle" style="background-color: Silver">
                                                        Subjects Studied
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        Elementary
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtElemSchoolName" runat="server" MaxLength="255" Width="96%"
                                                            Text='<%# Bind("Elemschoolnameaddr") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadNumericTextBox ID="txtElemSchoolYears" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Elemschoolyears") %>' MinValue="1" MaxValue="12" Width="96%">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <asp:RadioButton ID="chkElemGrad1" runat="server" GroupName="ElemGrad" Text="Yes"
                                                            Checked='<%# Bind("Elemschoolgrad") %>' />
                                                        <asp:RadioButton ID="chkElemGrad2" runat="server" GroupName="ElemGrad" Text="No"
                                                            Checked="false" />
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtElemSubj" runat="server" MaxLength="100" Width="96%" Text='<%# Bind("Elemschoolsubj") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        Middle School
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtMidSchoolName" runat="server" MaxLength="255" Width="96%"
                                                            Text='<%# Bind("Midschoolnameaddr") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadNumericTextBox ID="txtMidSchoolYears" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Midschoolyears") %>' MinValue="1" MaxValue="12" Width="96%">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <asp:RadioButton ID="chkMidSchoolGrad1" runat="server" GroupName="MidGrad" Text="Yes"
                                                            Checked='<%# Bind("Midschoolgrad") %>' />
                                                        <asp:RadioButton ID="chkMidSchoolGrad2" runat="server" GroupName="MidGrad" Text="No"
                                                            Checked="false" />
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtMidSchoolSubj" runat="server" MaxLength="100" Width="96%"
                                                            Text='<%# Bind("Midschoolsubj") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        High School
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <asp:TextBox ID="txtHighSchoolName" runat="server" MaxLength="255" Font-Size="12px"
                                                            BorderWidth="1px" Height="19px" Width="92%" Text='<%# Bind("Highschoolnameaddr") %>'
                                                            ValidationGroup="VAL">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqHighSchoolName" runat="server" ControlToValidate="txtHighSchoolName"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterHighSchoolName" runat="server"
                                                            TargetControlID="txtHighSchoolName" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadNumericTextBox ID="txtHighSchoolYears" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Highschoolyears") %>' MinValue="1" MaxValue="12" Width="75%"
                                                            ValidationGroup="VAL" BackColor="White">
                                                            <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                        </telerik:RadNumericTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqHighSchoolYears" runat="server" ControlToValidate="txtHighSchoolYears"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <asp:RadioButton ID="chkHighSchoolGrad1" runat="server" GroupName="HighGrad" Text="Yes"
                                                            Checked='<%# Bind("Highschoolgrad") %>' ValidationGroup="VAL" />
                                                        <asp:RadioButton ID="chkHighSchoolGrad2" runat="server" GroupName="HighGrad" Text="No"
                                                            Checked="false" ValidationGroup="VAL" />
                                                        <atg:DualRadioValidator CssClass="careersJobAppValidator" ID="reqHighSchoolGradOpt" runat="server" ErrorMessage="*"
                                                            ToolTip="Select Yes or No" Display="Static" SetFocusOnError="true"
                                                            ValidationGroup="VAL" RadioControl1ToValidate="chkHighSchoolGrad1" RadioControl2ToValidate="chkHighSchoolGrad2"></atg:DualRadioValidator>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <asp:TextBox ID="txtHighSchoolSubj" runat="server" MaxLength="100" Font-Size="12px"
                                                            BorderWidth="1px" Height="19px" Width="90%" Text='<%# Bind("Highschoolsubj") %>'
                                                            ValidationGroup="VAL">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqHighSchoolSubj" runat="server" ControlToValidate="txtHighSchoolSubj"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterHighSchoolSubj" runat="server"
                                                            TargetControlID="txtHighSchoolSubj" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        College
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtColName" runat="server" MaxLength="255" Width="96%" Text='<%# Bind("Collegenameaddr") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadNumericTextBox ID="txtCollegeYears" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Collegeyears") %>' MinValue="1" MaxValue="12" Width="96%">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <asp:RadioButton ID="chkCollegeGrad1" runat="server" GroupName="CollGrad" Text="Yes"
                                                            Checked='<%# Bind("Collegegrad") %>' />
                                                        <asp:RadioButton ID="chkCollegeGrad2" runat="server" GroupName="CollGrad" Text="No"
                                                            Checked="false" />
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtCollegeSubj" runat="server" MaxLength="100" Width="96%"
                                                            Text='<%# Bind("Collegesubj") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        Trade, Tech, Online
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtTradeName" runat="server" MaxLength="255" Width="96%"
                                                            Text='<%# Bind("Tradenameaddr") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadNumericTextBox ID="txtTradeYears" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Tradeyears") %>' MinValue="1" MaxValue="12" Width="96%">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <asp:RadioButton ID="chkTradeGrad1" runat="server" GroupName="TradeGrad" Text="Yes"
                                                            Checked='<%# Bind("Tradegrad") %>' />
                                                        <asp:RadioButton ID="chkTradeGrad2" runat="server" GroupName="TradeGrad" Text="No"
                                                            Checked="false" />
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtTradeSubj" runat="server" MaxLength="100" Width="96%"
                                                            Text='<%# Bind("Tradesubj") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                                4) GENERAL
                                            </div>
                                            <br />
                                            <table width="100%" style="table-layout: fixed">
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        Subjects of Special Study or Research Work
                                                        <br />
                                                        <telerik:RadTextBox ID="txtSpecialStudy" runat="server" Width="96%" MaxLength="255"
                                                            TextMode="MultiLine" Height="50" Rows="4" Text='<%# Bind("Specialstudyorresearch") %>'
                                                            onkeyDown="checkTextAreaMaxLength(this,event,'255');" onblur="onBlurTextCounter(this,'255');">
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        Special Skills
                                                        <br />
                                                        <telerik:RadTextBox ID="txtSpecialSkills" runat="server" Width="96%" MaxLength="255"
                                                            TextMode="MultiLine" Height="50" Rows="4" Text='<%# Bind("Specialskills") %>'
                                                            onkeyDown="checkTextAreaMaxLength(this,event,'255');" onblur="onBlurTextCounter(this,'255');">
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        Activities (Civic, Athletic, etc.)
                                                        <br />
                                                        <asp:Label ID="lblEthic" runat="server" CssClass="lblSubscript" Text="(Exclude organizations where the name of which indicate the race, creed, 
                                                            sex, age, marital status, color, or nation of origin of its members)">
                                                        </asp:Label>
                                                        <br />
                                                        <telerik:RadTextBox ID="txtActivities" runat="server" Width="96%" MaxLength="255"
                                                            TextMode="MultiLine" Height="50" Rows="4" Text='<%# Bind("Activities") %>' onkeyDown="checkTextAreaMaxLength(this,event,'255');"
                                                            onblur="onBlurTextCounter(this,'255');">
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        U.S. Military Service
                                                        <telerik:RadTextBox ID="txtMilitary" runat="server" MaxLength="20" Width="200px"
                                                            Text='<%# Bind("Militaryservice") %>'>
                                                        </telerik:RadTextBox>
                                                        Rank
                                                        <telerik:RadTextBox ID="txtRank" runat="server" MaxLength="30" Width="200px" Text='<%# Bind("Rank") %>'>
                                                        </telerik:RadTextBox>
                                                        <br />
                                                        Present Membership in National Guard or Reserves
                                                        <telerik:RadTextBox ID="txtGuard" runat="server" MaxLength="20" Width="200px" Text='<%# Bind("Nationalguardorreserves") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                                5) FORMER EMPLOYERS
                                            </div>
                                            <br />
                                            <table width="100%" style="table-layout: fixed;" border="1">
                                                <tr>
                                                    <td class="careersLblTitle" style="background-color: Silver; width: 190px">
                                                        Dates Employed
                                                    </td>
                                                    <td class="careersLblTitle" style="background-color: Silver; width: 35%">
                                                        Employer Name and Address
                                                    </td>
                                                    <td class="careersLblTitle" style="background-color: Silver; width: 100px">
                                                        Employer Phone
                                                    </td>
                                                    <td class="careersLblTitle" style="background-color: Silver">
                                                        Position
                                                    </td>
                                                    <td class="careersLblTitle" style="background-color: Silver; width: 8%">
                                                        Salary / Wage
                                                    </td>
                                                    <td class="careersLblTitle" style="background-color: Silver">
                                                        Reason for Leaving
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        From M/Y
                                                        <telerik:RadNumericTextBox ID="txtEmp1FromMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Emp1frommonth") %>' MinValue="1" MaxValue="12" Width="30px"
                                                            ValidationGroup="VAL" BackColor="White">
                                                            <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                        </telerik:RadNumericTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqEmp1FromMonth" runat="server" ControlToValidate="txtEmp1FromMonth"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <telerik:RadNumericTextBox ID="txtEmp1FromYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            NumberFormat-GroupSeparator="" Value='<%# Bind("Emp1fromyear") %>' MinValue="1970"
                                                            MaxValue='<%# DateTime.Now.Year %>' Width="40px" ValidationGroup="VAL" BackColor="#F9C2BB">
                                                            <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                        </telerik:RadNumericTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqEmpFromYear" runat="server" ControlToValidate="txtEmp1FromYear"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <br />
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                                                        <telerik:RadNumericTextBox ID="txtEmp1ToMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Emp1tomonth") %>' MinValue="1" MaxValue="12" Width="30px" ValidationGroup="VAL"
                                                            BackColor="#F9C2BB">
                                                            <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                        </telerik:RadNumericTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqEmpToMonth" runat="server" ControlToValidate="txtEmp1ToMonth"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <telerik:RadNumericTextBox ID="txtEmp1ToYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            NumberFormat-GroupSeparator="" Value='<%# Bind("Emp1toyear") %>' MinValue="1970"
                                                            MaxValue='<%# DateTime.Now.Year %>' Width="40px" ValidationGroup="VAL" BackColor="#F9C2BB">
                                                            <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                        </telerik:RadNumericTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqEmpToYear" runat="server" ControlToValidate="txtEmp1ToYear"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <asp:TextBox ID="txtEmp1Name" runat="server" MaxLength="200" Width="90%" Text='<%# Bind("Emp1name") %>'
                                                            TextMode="MultiLine" Rows="2" Font-Size="12px" BorderWidth="1px" Height="40px"
                                                            ValidationGroup="VAL" onkeyDown="checkTextAreaMaxLength(this,event,'200');" onblur="onBlurTextCounter(this,'200');">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqEmp1Name" runat="server" ControlToValidate="txtEmp1Name"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterEmp1Name" runat="server"
                                                            TargetControlID="txtEmp1Name" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadMaskedTextBox ID="txtEmp1Phone" runat="server" Width="80%" Mask="###-###-####"
                                                            DisplayMask="###-###-####" Text='<%# Bind("Emp1phone") %>' ValidationGroup="VAL"
                                                            OnLoad="Phone_OnLoad">
                                                            <ClientEvents OnValueChanged="DoChangeColor"/>
                                                        </telerik:RadMaskedTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqEmp1Phone" runat="server" ControlToValidate="txtEmp1Phone"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <asp:TextBox ID="txtEmp1Position" runat="server" MaxLength="30" Font-Size="12px"
                                                            BorderWidth="1px" Height="19px" Width="75%" Text='<%# Bind("Emp1position") %>'>
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqEmp1Pos" runat="server" ControlToValidate="txtEmp1Position"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterEmp1Position" runat="server"
                                                            TargetControlID="txtEmp1Position" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadNumericTextBox ID="txtEmp1Salary" runat="server" NumberFormat-DecimalDigits="2"
                                                            Value='<%# Bind("Emp1salary") %>' Width="75%" ValidationGroup="VAL" BackColor="#F9C2BB">
                                                            <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                        </telerik:RadNumericTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqEmp1Sal" runat="server" ControlToValidate="txtEmp1Salary"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <asp:TextBox ID="txtEmp1Reason" runat="server" MaxLength="100" Font-Size="12px" BorderWidth="1px"
                                                            Height="19px" Width="75%" Text='<%# Bind("Emp1reason") %>' ValidationGroup="VAL">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqEmp1Reason" runat="server" ControlToValidate="txtEmp1Reason"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterEmp1Reason" runat="server"
                                                            TargetControlID="txtEmp1Reason" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        From M/Y
                                                        <telerik:RadNumericTextBox ID="txtEmp2FromMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Emp2frommonth") %>' MinValue="1" MaxValue="12" Width="30px">
                                                        </telerik:RadNumericTextBox>
                                                        <telerik:RadNumericTextBox ID="txtEmp2FromYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            NumberFormat-GroupSeparator="" Value='<%# Bind("Emp2fromyear") %>' MinValue="1970"
                                                            MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                                                        </telerik:RadNumericTextBox>
                                                        <br />
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                                                        <telerik:RadNumericTextBox ID="txtEmp2ToMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Emp2tomonth") %>' MinValue="1" MaxValue="12" Width="30px">
                                                        </telerik:RadNumericTextBox>
                                                        <telerik:RadNumericTextBox ID="txtEmp2ToYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            NumberFormat-GroupSeparator="" Value='<%# Bind("Emp2toyear") %>' MinValue="1970"
                                                            MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp2Name" runat="server" MaxLength="200" Width="96%" Text='<%# Bind("Emp2name") %>'
                                                            TextMode="MultiLine" Rows="2" Height="40px" onkeyDown="checkTextAreaMaxLength(this,event,'200');"
                                                            onblur="onBlurTextCounter(this,'200');">
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadMaskedTextBox ID="txtEmp2Phone" runat="server" Width="96%" Mask="###-###-####"
                                                            DisplayMask="###-###-####" Text='<%# Bind("Emp2phone") %>'>
                                                        </telerik:RadMaskedTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp2Position" runat="server" MaxLength="30" Width="96%"
                                                            Text='<%# Bind("Emp2position") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadNumericTextBox ID="txtEmp2Salary" runat="server" NumberFormat-DecimalDigits="2"
                                                            Value='<%# Bind("Emp2salary") %>' Width="96%">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp2Reason" runat="server" MaxLength="100" Width="96%"
                                                            Text='<%# Bind("Emp2reason") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        From M/Y
                                                        <telerik:RadNumericTextBox ID="txtEmp3FromMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Emp3frommonth") %>' MinValue="1" MaxValue="12" Width="30px">
                                                        </telerik:RadNumericTextBox>
                                                        <telerik:RadNumericTextBox ID="txtEmp3FromYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            NumberFormat-GroupSeparator="" Value='<%# Bind("Emp3fromyear") %>' MinValue="1970"
                                                            MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                                                        </telerik:RadNumericTextBox>
                                                        <br />
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                                                        <telerik:RadNumericTextBox ID="txtEmp3ToMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Emp3tomonth") %>' MinValue="1" MaxValue="12" Width="30px">
                                                        </telerik:RadNumericTextBox>
                                                        <telerik:RadNumericTextBox ID="txtEmp3ToYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            NumberFormat-GroupSeparator="" Value='<%# Bind("Emp3toyear") %>' MinValue="1970"
                                                            MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp3Name" runat="server" MaxLength="200" Width="96%" Text='<%# Bind("Emp3name") %>'
                                                            TextMode="MultiLine" Rows="2" Height="40px" onkeyDown="checkTextAreaMaxLength(this,event,'200');"
                                                            onblur="onBlurTextCounter(this,'200');">
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadMaskedTextBox ID="txtEmp3Phone" runat="server" Width="96%" Mask="###-###-####"
                                                            DisplayMask="###-###-####" Text='<%# Bind("Emp3phone") %>'>
                                                        </telerik:RadMaskedTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp3Position" runat="server" MaxLength="30" Width="96%"
                                                            Text='<%# Bind("Emp3position") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadNumericTextBox ID="txtEmp3Salary" runat="server" NumberFormat-DecimalDigits="2"
                                                            Value='<%# Bind("Emp3salary") %>' Width="96%">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp3Reason" runat="server" MaxLength="100" Width="96%"
                                                            Text='<%# Bind("Emp3reason") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        From M/Y
                                                        <telerik:RadNumericTextBox ID="txtEmp4FromMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Emp4frommonth") %>' MinValue="1" MaxValue="12" Width="30px">
                                                        </telerik:RadNumericTextBox>
                                                        <telerik:RadNumericTextBox ID="txtEmp4FromYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            NumberFormat-GroupSeparator="" Value='<%# Bind("Emp4fromyear") %>' MinValue="1970"
                                                            MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                                                        </telerik:RadNumericTextBox>
                                                        <br />
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                                                        <telerik:RadNumericTextBox ID="txtEmp4ToMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Emp4tomonth") %>' MinValue="1" MaxValue="12" Width="30px">
                                                        </telerik:RadNumericTextBox>
                                                        <telerik:RadNumericTextBox ID="txtEmp4ToYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            NumberFormat-GroupSeparator="" Value='<%# Bind("Emp4toyear") %>' MinValue="1970"
                                                            MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp4Name" runat="server" MaxLength="200" Width="96%" Text='<%# Bind("Emp4name") %>'
                                                            TextMode="MultiLine" Rows="2" Height="40px" onkeyDown="checkTextAreaMaxLength(this,event,'200');"
                                                            onblur="onBlurTextCounter(this,'200');">
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadMaskedTextBox ID="txtEmp4Phone" runat="server" Width="96%" Mask="###-###-####"
                                                            DisplayMask="###-###-####" Text='<%# Bind("Emp4phone") %>'>
                                                        </telerik:RadMaskedTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp4Position" runat="server" MaxLength="30" Width="96%"
                                                            Text='<%# Bind("Emp4position") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadNumericTextBox ID="txtEmp4Salary" runat="server" NumberFormat-DecimalDigits="2"
                                                            Value='<%# Bind("Emp4salary") %>' Width="96%">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp4Reason" runat="server" MaxLength="100" Width="96%"
                                                            Text='<%# Bind("Emp4reason") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        From M/Y
                                                        <telerik:RadNumericTextBox ID="txtEmp5FromMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Emp5frommonth") %>' MinValue="1" MaxValue="12" Width="30px">
                                                        </telerik:RadNumericTextBox>
                                                        <telerik:RadNumericTextBox ID="txtEmp5FromYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            NumberFormat-GroupSeparator="" Value='<%# Bind("Emp5fromyear") %>' MinValue="1970"
                                                            MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                                                        </telerik:RadNumericTextBox>
                                                        <br />
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                                                        <telerik:RadNumericTextBox ID="txtEmp5ToMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Emp5tomonth") %>' MinValue="1" MaxValue="12" Width="30px">
                                                        </telerik:RadNumericTextBox>
                                                        <telerik:RadNumericTextBox ID="txtEmp5ToYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            NumberFormat-GroupSeparator="" Value='<%# Bind("Emp5toyear") %>' MinValue="1970"
                                                            MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp5Name" runat="server" MaxLength="200" Width="96%" Text='<%# Bind("Emp5name") %>'
                                                            TextMode="MultiLine" Rows="2" Height="40px" onkeyDown="checkTextAreaMaxLength(this,event,'200');"
                                                            onblur="onBlurTextCounter(this,'200');">
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadMaskedTextBox ID="txtEmp5Phone" runat="server" Width="96%" Mask="###-###-####"
                                                            DisplayMask="###-###-####" Text='<%# Bind("Emp5phone") %>'>
                                                        </telerik:RadMaskedTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp5Position" runat="server" MaxLength="30" Width="96%"
                                                            Text='<%# Bind("Emp5position") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadNumericTextBox ID="txtEmp5Salary" runat="server" NumberFormat-DecimalDigits="2"
                                                            Value='<%# Bind("Emp5salary") %>' Width="96%">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp5Reason" runat="server" MaxLength="100" Width="96%"
                                                            Text='<%# Bind("Emp5reason") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle">
                                                        From M/Y
                                                        <telerik:RadNumericTextBox ID="txtEmp6FromMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Emp6frommonth") %>' MinValue="1" MaxValue="12" Width="30px">
                                                        </telerik:RadNumericTextBox>
                                                        <telerik:RadNumericTextBox ID="txtEmp6FromYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            NumberFormat-GroupSeparator="" Value='<%# Bind("Emp6fromyear") %>' MinValue="1970"
                                                            MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                                                        </telerik:RadNumericTextBox>
                                                        <br />
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To M/Y
                                                        <telerik:RadNumericTextBox ID="txtEmp6ToMonth" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Emp6tomonth") %>' MinValue="1" MaxValue="12" Width="30px">
                                                        </telerik:RadNumericTextBox>
                                                        <telerik:RadNumericTextBox ID="txtEmp6ToYear" runat="server" NumberFormat-DecimalDigits="0"
                                                            NumberFormat-GroupSeparator="" Value='<%# Bind("Emp6toyear") %>' MinValue="1970"
                                                            MaxValue='<%# DateTime.Now.Year %>' Width="40px">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp6Name" runat="server" MaxLength="200" Width="96%" Text='<%# Bind("Emp6name") %>'
                                                            TextMode="MultiLine" Rows="2" Height="40px" onkeyDown="checkTextAreaMaxLength(this,event,'200');"
                                                            onblur="onBlurTextCounter(this,'200');">
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadMaskedTextBox ID="txtEmp6Phone" runat="server" Width="96%" Mask="###-###-####"
                                                            DisplayMask="###-###-####" Text='<%# Bind("Emp6phone") %>'>
                                                        </telerik:RadMaskedTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp6Position" runat="server" MaxLength="30" Width="96%"
                                                            Text='<%# Bind("Emp6position") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadNumericTextBox ID="txtEmp6Salary" runat="server" NumberFormat-DecimalDigits="2"
                                                            Value='<%# Bind("Emp6salary") %>' Width="96%">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                    <td class="careersLblTitle">
                                                        <telerik:RadTextBox ID="txtEmp6Reason" runat="server" MaxLength="100" Width="96%"
                                                            Text='<%# Bind("Emp6reason") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="careersLblTitle" colspan="6">
                                                        Can we contact these employers?
                                                        <asp:RadioButton ID="chkContactEmp1" runat="server" GroupName="ContactEmp" Text="Yes"
                                                            Checked='<%# Bind("Cancontactemp") %>' ValidationGroup="VAL" />
                                                        <asp:RadioButton ID="chkContactEmp2" runat="server" GroupName="ContactEmp" Text="No"
                                                            Checked="false" ValidationGroup="VAL" />
                                                        <atg:DualRadioValidator CssClass="careersJobAppValidator" ID="reqContEmpRadio" runat="server" ErrorMessage="*" ToolTip="Select Yes or No"
                                                            Display="Static" SetFocusOnError="true" ValidationGroup="VAL"
                                                            RadioControl1ToValidate="chkContactEmp1" RadioControl2ToValidate="chkContactEmp2"></atg:DualRadioValidator>
                                                        <br />
                                                        If no, please list and explain:
                                                        <br />
                                                        <asp:TextBox ID="txtContactEmpReason" runat="server" MaxLength="100" Font-Size="12px"
                                                            BorderWidth="1px" Height="19px" Width="90%" Text='<%# Bind("Cantcontactreason") %>'
                                                            ValidationGroup="VAL">
                                                        </asp:TextBox>
                                                        <atg:RadioTextBoxValidator CssClass="careersJobAppValidator" ID="reqContactEmpReasonText"
                                                            runat="server" ErrorMessage="*" Display="Static" SetFocusOnError="true"
                                                            ValidationGroup="VAL" RadioControlToValidate="chkContactEmp2" TextBoxToValidate="txtContactEmpReason"
                                                            TextBoxWatermarkExtender="waterContactEmpReason"></atg:RadioTextBoxValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterContactEmpReason" runat="server" Enabled="false"
                                                            TargetControlID="txtContactEmpReason" WatermarkCssClass="careersJobAppWatermark"
                                                            WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <div class="careersLblTitle" style="font-size: medium; background-color: Silver">
                                                6) BUSINESS / PERSONAL REFERENCES
                                            </div>
                                            <br />
                                            <table width="100%" style="table-layout: fixed;" border="1">
                                                <tr>
                                                    <td class="careersLblTitle" style="background-color: Silver">
                                                        Name
                                                    </td>
                                                    <td class="careersLblTitle" style="background-color: Silver; width: 100px">
                                                        Phone Number
                                                    </td>
                                                    <td class="careersLblTitle" style="background-color: Silver">
                                                        Business
                                                    </td>
                                                    <td class="careersLblTitle" style="background-color: Silver; width: 100px">
                                                        Years Acquainted
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtRef1Name" runat="server" MaxLength="30" Font-Size="12px" BorderWidth="1px"
                                                            Height="19px" Width="90%" Text='<%# Bind("Ref1name") %>' ValidationGroup="VAL">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqRef1Name" runat="server" ControlToValidate="txtRef1Name"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterRef1Name" runat="server"
                                                            TargetControlID="txtRef1Name" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                    <td>
                                                        <telerik:RadMaskedTextBox ID="txtRef1Phone" runat="server" Width="80%" Mask="###-###-####"
                                                            DisplayMask="###-###-####" Text='<%# Bind("Ref1phone") %>' ValidationGroup="VAL"
                                                            OnLoad="Phone_OnLoad">
                                                            <ClientEvents OnValueChanged="DoChangeColor"/>
                                                        </telerik:RadMaskedTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqRef1Phone" runat="server" ControlToValidate="txtRef1Phone"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtRef1Business" runat="server" MaxLength="30" Font-Size="12px"
                                                            BorderWidth="1px" Height="19px" Width="90%" Text='<%# Bind("Ref1business") %>'
                                                            ValidationGroup="VAL">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqRef1Business" runat="server" ControlToValidate="txtRef1Business"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterRef1Business" runat="server"
                                                            TargetControlID="txtRef1Business" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ID="txtRef1Years" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Ref1years") %>' MinValue="1" MaxValue="100" Width="80%" ValidationGroup="VAL"
                                                            BackColor="#F9C2BB">
                                                            <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                        </telerik:RadNumericTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqRef1Years" runat="server" ControlToValidate="txtRef1Years"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtRef2Name" runat="server" MaxLength="30" Font-Size="12px" BorderWidth="1px"
                                                            Height="19px" Width="90%" Text='<%# Bind("Ref2name") %>' ValidationGroup="VAL">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqRef2Name" runat="server" ControlToValidate="txtRef2Name"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterRef2Name" runat="server"
                                                            TargetControlID="txtRef2Name" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                    <td>
                                                        <telerik:RadMaskedTextBox ID="txtRef2Phone" runat="server" Width="80%" Mask="###-###-####"
                                                            DisplayMask="###-###-####" Text='<%# Bind("Ref2phone") %>' ValidationGroup="VAL"
                                                            OnLoad="Phone_OnLoad">
                                                            <ClientEvents OnValueChanged="DoChangeColor"/>
                                                        </telerik:RadMaskedTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqRef2Phone" runat="server" ControlToValidate="txtRef2Phone"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtRef2Business" runat="server" MaxLength="30" Font-Size="12px"
                                                            BorderWidth="1px" Height="19px" Width="90%" Text='<%# Bind("Ref2business") %>'
                                                            ValidationGroup="VAL">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqRef2Business" runat="server" ControlToValidate="txtRef2Business"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterRef2Business" runat="server"
                                                            TargetControlID="txtRef2Business" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ID="txtRef2Years" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Ref2years") %>' MinValue="1" MaxValue="100" Width="80%" ValidationGroup="VAL"
                                                            BackColor="#F9C2BB">
                                                            <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                        </telerik:RadNumericTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqRef2Years" runat="server" ControlToValidate="txtRef2Years"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true"
                                                            OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <telerik:RadTextBox ID="txtRef3Name" runat="server" MaxLength="30" Width="96%" Text='<%# Bind("Ref3name") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadMaskedTextBox ID="txtRef3Phone" runat="server" Width="96%" Mask="###-###-####"
                                                            DisplayMask="###-###-####" Text='<%# Bind("Ref3phone") %>'>
                                                        </telerik:RadMaskedTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadTextBox ID="txtRef3Business" runat="server" MaxLength="30" Width="96%"
                                                            Text='<%# Bind("Ref3business") %>'>
                                                        </telerik:RadTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadNumericTextBox ID="txtRef3Years" runat="server" NumberFormat-DecimalDigits="0"
                                                            Value='<%# Bind("Ref3years") %>' MinValue="1" MaxValue="100" Width="96%">
                                                        </telerik:RadNumericTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <table width="100%" style="table-layout: fixed">
                                                <tr>
                                                    <td class="careersLblTitle" style="width: 200px">
                                                        In Case of Emergency Notify
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtIceName" runat="server" MaxLength="60" Font-Size="12px" BorderWidth="1px"
                                                            Height="19px" Width="90%" Text='<%# Bind("Icecontactname") %>' ValidationGroup="VAL">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqIceName" runat="server" ControlToValidate="txtIceName"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:Label ID="lblICEName" runat="server" CssClass="lblSubscript" Text="Name"></asp:Label>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterIceName" runat="server"
                                                            TargetControlID="txtIceName" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtIceAddr" runat="server" MaxLength="100" Font-Size="12px" BorderWidth="1px"
                                                            Height="19px" Width="90%" Text='<%# Bind("Icecontactaddr") %>' ValidationGroup="VAL">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqIceAddr" runat="server" ControlToValidate="txtIceAddr"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:Label ID="lblICEAddr" runat="server" CssClass="lblSubscript" Text="Address"></asp:Label>
                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterIceAddr" runat="server"
                                                            TargetControlID="txtIceAddr" WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                                        </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                    </td>
                                                    <td>
                                                        <telerik:RadMaskedTextBox ID="txtIcePhone" runat="server" Width="80px" Mask="###-###-####"
                                                            DisplayMask="###-###-####" Text='<%# Bind("Icecontactphone") %>' ValidationGroup="VAL"
                                                            OnLoad="Phone_OnLoad">
                                                            <ClientEvents OnValueChanged="DoChangeColor"/>
                                                        </telerik:RadMaskedTextBox>
                                                        <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqIcePhone" runat="server" ControlToValidate="txtIcePhone"
                                                            ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:Label ID="lblICEPhone" runat="server" CssClass="lblSubscript" Text="Phone Number"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <br />
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep5" runat="server" Title="Final: Certification" StepType="Finish">
                    <!-- STEP 5 CERT -->
                    <%--<atg:Certification ID="ctrlCertification" runat="server" MyJobApp='<%# Container.DataItem %>' />--%>
                    <%--<div id="divCert" runat="server"></div>--%>
                    <asp:DetailsView ID="grdApplication5" runat="server" DefaultMode="Insert" DataKeyNames="Jobapplicationid"
                        AutoGenerateRows="False" Width="100%" OnItemInserting="grdApplication5_ItemInserting">
                        <Fields>
                            <asp:TemplateField ControlStyle-BorderColor="Silver">
                                <InsertItemTemplate>
                                    <div class="lblInfoBold" style="text-align: center">
                                        <div class="lblInfoBold" style="text-align: left; margin: 10px 10px 10px 10px">
                                            I certify that all the information submitted by me on this application is true and
                                            complete, and I understand that if any false information, omissions, or misrepresentations
                                            are discovered, my application may be rejected and, if I am employed, my employment
                                            may be terminated at any time. In consideration of my employment, I agree to conform
                                            to the company's rules and regulations, and I agree that my employment and compensation
                                            can be terminated, with or without cause, and with or without notice, at any time
                                            at either my or the company's option. I also understand and agree that the terms
                                            and conditions of my employment may be changed, with or without cause, and with
                                            or without notice, at any time by the company. I understand that no company representative,
                                            other than it's president, and then only when in writing and signed by the president,
                                            has any authority to enter into any agreement for employment for any specific period
                                            of time, or to make any agreement contrary to the foregoing.
                                            <br />
                                            <br />
                                            Date Signed:&nbsp;&nbsp;
                                            <telerik:RadDatePicker ID="dtSigDate" runat="server" SelectedDate='<%# Bind("Sigdate") %>'
                                                Width="100px" MinDate='<%# DateTime.Now.Date %>' MaxDate='<%# DateTime.Now.Date %>'>
                                                <DateInput ID="dtSigDateInput" runat="server" BackColor="#F9C2BB">
                                                    <ClientEvents OnValueChanged="DoChangeColor" OnLoad="DoChangeColor" />
                                                </DateInput>
                                            </telerik:RadDatePicker>
                                            <asp:RequiredFieldValidator CssClass="careersJobAppValidator" ID="reqSigDate" runat="server" ControlToValidate="dtSigDate"
                                                ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></asp:RequiredFieldValidator>
                                            <br />
                                            Signature - Please Enter Your Email Address:&nbsp;&nbsp;
                                            <asp:TextBox ID="txtSignature" runat="server" MaxLength="100" Font-Size="12px" BorderWidth="1px"
                                                Height="19px" Width="300px" Text='<%# Bind("Signature") %>' ValidationGroup="VAL">
                                            </asp:TextBox>
                                            <atg:EmailValidator CssClass="careersJobAppValidator" ID="reqSignature" runat="server" ControlToValidate="txtSignature"
                                                ErrorMessage="*" Display="Static" ValidationGroup="VAL" SetFocusOnError="true" OnInit="reqRFV_OnInit"></atg:EmailValidator>
                                            <%--<ajaxToolkit:TextBoxWatermarkExtender ID="waterSignature" runat="server" TargetControlID="txtSignature"
                                                WatermarkCssClass="careersJobAppWatermark" WatermarkText=" ">
                                            </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                        </div>
                                        <br />
                                        <div style="float: right; text-align: center; width: 250px; margin: 10px 10px 10px 10px">
                                            <telerik:RadCaptcha ID="RadCaptcha1" runat="server" Display="Static" 
                                                Width="100%" ErrorMessage="Wrong Letters!" IgnoreCase="true"
                                                EnableRefreshImage="true" ProtectionMode="Captcha" CaptchaTextBoxLabel="" ValidationGroup="VAL" >  
                                                <CaptchaImage LineNoise="High" UseRandomFont="True" FontWarp="Medium" /> 
                                            </telerik:RadCaptcha>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>
                </asp:WizardStep>
            </WizardSteps>
        </asp:Wizard>
        <br />
    </div>
</asp:Content>

