﻿using System;
using System.Linq;

public partial class Careers_Default : System.Web.UI.MasterPage
{
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // Now dynamically register the PULSE SCRIPT
        // All pages with this master will keep the session alive.
        PageUtils.RegisterSessionTimeoutPulse(Page);

        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManagerMasterPage);
    }
}
