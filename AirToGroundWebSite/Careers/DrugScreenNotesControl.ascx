﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DrugScreenNotesControl.ascx.cs" Inherits="Careers_DrugScreenNotesControl" %>
<div class="lblInfoBold" style="text-align: left; margin: 10px 10px 10px 10px">
    <br />
    <div class="lblTitle" style="text-align: center; font-size: medium">
        AIR TO GROUND SERVICES, INC.<br />
        DRUG-FREE WORKPLACE POLICY
    </div>
    <br />
    <div class="lblTitle" style="text-align: center; border: 1px solid gray; font-weight: 900; font-size: medium">
        APPLICANT'S ACKNOWLEDGEMENT FORM FOR ALCOHOL AND DRUG SCREENING
    </div>
    <br />
    <div class="lblInfo" style="text-align: left; font-size: medium">
        The Omnibus Transportation employee Testing Act of 1991 requires drug and alcohol testing on 
        all safety-sensitive positions of employees in the aviation, trucking, railroads, mass transit, 
        pipelines, and other transportation industries. Air To Ground Services, Inc. is federally 
        regulated by the DOT 49 CFR Part 40 (F.A.A. industry) drug and alcohol testing regulations.
        <br />
        <br />
        I understand it is the policy of Air To Ground Services, Inc. to conduct pre-employment DOT or 
        NON-DOT regulated drug and/or alcohol tests of job applicants for the purpose
        of detecting drug and/or alcohol abuse, and that one of the requirements for consideration of 
        employment with the company is the satisfactory passing of the drug and/or alcohol tests(s).
        <br />
        <br />
        For the purpose of being futher considered for employment, I hereby understand that I
        will be submitted to DOR or NON-DOT regulated type drug and/or alcohol testing. I 
        understand that favorable test results will not necessarily guarantee that I will be employedd
        by Air To Ground Services, Inc.
        <br />
        <br />
        I hereby acknowledge that such type drug and/or alcohol testing will be conducted by an
        authorized collection facility to collect breath and/or urine sample from me and to
        conduct necessary medical tests to determine the presence of use of alcohol, drugs, or
        controlled substances. Further, I understand for the release of the test results and other 
        medical information to authorized company management (Drug Program Coordinator) for appropriate
        review. I also understand that, if I refuse to acknowledge, I will be denied employment and 
        may not reapply.
        <br />
        <br />
        If you agree, do the following and continue to the next step:
        <br />
        <br />
        <telerik:RadTextBox ID="txtDrugPrintName" runat="server" MaxLength="80" Text='<%# Bind("Drugprintname") %>'
            Width="300px">
        </telerik:RadTextBox>
        <asp:RequiredFieldValidator ID="reqDrugPrintName" runat="server" ControlToValidate="txtDrugPrintName"
            ErrorMessage="*" Display="Dynamic" ValidationGroup="DRUG" SetFocusOnError="true"></asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="lblDrugPrintName" runat="server" SkinID="SubscriptLabel" Text="Type Your Name"></asp:Label>
        <br />
        <telerik:RadTextBox ID="txtDrugEmail" runat="server" MaxLength="80" Text='<%# Bind("Drugemail") %>'
            Width="300px">
        </telerik:RadTextBox>
        <asp:RequiredFieldValidator ID="reqDrugEmail" runat="server" ControlToValidate="txtDrugEmail"
            ErrorMessage="*" Display="Dynamic" ValidationGroup="DRUG" SetFocusOnError="true"></asp:RequiredFieldValidator>
        <br />        
        <asp:Label ID="lblDrugEmailAddress" runat="server" SkinID="SubscriptLabel" Text="Type Your Email Address"></asp:Label>
        <br />
        <telerik:RadDatePicker ID="dtDrugDate" runat="server" SelectedDate='<%# Bind("Drugdate") %>'
            Width="100px">
        </telerik:RadDatePicker>
        <asp:RequiredFieldValidator ID="reqDrugDate" runat="server" ControlToValidate="dtDrugDate"
            ErrorMessage="*" Display="Dynamic" ValidationGroup="DRUG" SetFocusOnError="true"></asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="lblDrugDate" runat="server" SkinID="SubscriptLabel" Text="Date"></asp:Label>
    </div>
</div>
