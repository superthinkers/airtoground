﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CertificationHtml.aspx.cs"
    MasterPageFile="~/Careers/Default.master" Inherits="Careers_CertificationHtml" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolderCareers" runat="Server">
    <div>
        <div class="lblInfoBold" style="text-align: center">
            <div class="lblInfoBold" style="text-align: left; margin: 10px 10px 10px 10px">
                I certify that all the information submitted by me on this application is true and
                complete, and I understand that if any false information, omissions, or misrepresentations
                are discovered, my application may be rejected and, if I am employed, my employment
                may be terminated at any time. In consideration of my employment, I agree to conform
                to the company's rules and regulations, and I agree that my employment and compensation
                can be terminated, with or without cause, and with or without notice, at any time
                at either my or the company's option. I also understand and agree that the terms
                and conditions of my employment may be changed, with or without cause, and with
                or without notice, at any time by the company. I understand that no company representative,
                other than it's president, and then only when in writing and signed by the president,
                has any authority to enter into any agreement for employment for any specific period
                of time, or to make any agreement contrary to the foregoing.
                <br />
                <br />
                Date Signed
                <telerik:RadDatePicker ID="dtSigDate" runat="server" SelectedDate='<%# Bind("Sigdate") %>'
                    Width="100px">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="reqSigDate" runat="server" CssClass="careersJobAppValidator"
                    ControlToValidate="dtSigDate" ErrorMessage="*" Display="Dynamic" ValidationGroup="CERTSIG"
                    SetFocusOnError="true"></asp:RequiredFieldValidator>
                Signature - please enter your Email Address.
                <telerik:RadTextBox ID="txtSignature" runat="server" MaxLength="100" Width="300px"
                    Text='<%# Bind("Signature") %>'>
                </telerik:RadTextBox>
                <asp:RequiredFieldValidator ID="reqSignature" runat="server" CssClass="careersJobAppValidator"
                    ControlToValidate="txtSignature" ErrorMessage="*" Display="Dynamic" ValidationGroup="CERTSIG"
                    SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>
            <br />
            <%--<div style="float: right; text-align: center; width: 250px; margin: 10px 10px 10px 10px">
        <telerik:RadCaptcha ID="RadCaptcha1" runat="server" Display="Static" 
            Width="100%" ErrorMessage="Wrong Letters!" IgnoreCase="true"
            EnableRefreshImage="true" ProtectionMode="Captcha" CaptchaTextBoxLabel="" >  
            <CaptchaImage LineNoise="High" UseRandomFont="True" FontWarp="Medium" />
        </telerik:RadCaptcha>
    </div>--%>
        </div>    
    </div>
</asp:Content>