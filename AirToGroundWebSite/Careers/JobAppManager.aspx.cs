﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

public partial class Careers_JobAppManager : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName()
    {
        return AuditUtils.PAGE_CAREERS_JOBAPP_MGR;
    }

    // For postbacks in the JavaScript in the page.
    protected string PostBackString;

    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // For postbacks in the JavaScript in the page.
        PostBackString = Page.ClientScript.GetPostBackEventReference(this, "");
        //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
        //m.IsActive = true;

        if (!Page.IsPostBack) {
            // Date boxes.
            dtFrom.SelectedDate = DateTime.Now.AddDays(-7);
            dtTo.SelectedDate = DateTime.Now;
        }
    }
    protected void LoadJobApps() {
        RadGrid1.DataSourceID = "ObjectDataSource1";
        if (dtFrom.SelectedDate == null) {
            dtFrom.SelectedDate = DateTime.Now.AddDays(-7);
        }
        if (dtTo.SelectedDate == null) {
            dtTo.SelectedDate = DateTime.Now.Date;
        }
        ObjectDataSource1.SelectParameters["dtFrom"].DefaultValue = dtFrom.SelectedDate.Value.Date.ToShortDateString();
        ObjectDataSource1.SelectParameters["dtTo"].DefaultValue = dtTo.SelectedDate.Value.Date.ToShortDateString();
        ObjectDataSource1.SelectParameters["lastName"].DefaultValue = txtLastName.Text.ToUpper();
        ObjectDataSource1.Select();
        RadGrid1.DataBind();
        lblCount.Text = "Count: " + RadGrid1.Items.Count.ToString();
        if (RadGrid1.Items.Count > 0) {
            RadGrid1.Height = new Unit("100%");
            RadGrid1.Items[0].Selected = true;
        } else {
            RadGrid1.Height = new Unit("200px");
        }
    }
    protected void btnSearch_OnClick(object sender, EventArgs e) {
        LoadJobApps();
    }
}