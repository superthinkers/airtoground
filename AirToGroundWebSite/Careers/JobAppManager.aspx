﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="JobAppManager.aspx.cs" Inherits="Careers_JobAppManager" 
    Buffer="true" Explicit="true" Strict="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetJobApplicationsByParamsDS"
        DataObjectTypeName="ATGDB.Jobapplication" TypeName="ATGDB.Jobapplication" SortParameterName="sSortFieldName">
        <SelectParameters>
            <asp:Parameter Name="dtFrom" Type="DateTime" />
            <asp:Parameter Name="dtTo" Type="DateTime" />
            <asp:Parameter Name="lastName" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
<%--        <ClientEvents OnRequestStart="onRequestStart" />--%>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="lblCount" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="false" runat="server" EnableShadow="true" Modal="true" VisibleTitlebar="false" AutoSize="true">
    </telerik:RadWindowManager>     
    <telerik:RadCodeBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            // This fixes problems when exporting.
            function onRequestStart(sender, args) {
                if ((args.get_eventTarget().indexOf("export") >= 0)) {
                    // Disable ajax
                    //args.set_enableAjax(false);
                }
            }
            function onClientClose1(oWnd, args) {
                // Force a page postback. Causes refresh.
                <%= PostBackString %>
            }
            function ValidateDateRange(sender, e) {
                var dtFromDate = $find("<%= dtFrom.ClientID %>");
                var dtToDate = $find("<%= dtTo.ClientID %>");

                if ((dtFromDate.get_selectedDate() == null) && (dtToDate.get_selectedDate() != null)) {
                    alert("Please select the From/End Date");
                } else if ((dtFromDate.get_selectedDate() > dtToDate.get_selectedDate()) && (dtToDate.get_selectedDate() != null)) {
                    alert("The To/Start Date must be less than or equal to than the From/Begin Date");
                    dtFromDate.set_selectedDate(dtToDate.get_selectedDate());
                }
                return false;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="true"
        Skin="Office2007" BackgroundPosition="Top">
    </telerik:RadAjaxLoadingPanel>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Work Log Manager"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Work Log Manager</div>
            <div>
                <br />
                <!-- SEARCH -->
                <table width="97%">
                    <tr>
                        <td style="width: 70%">
                            <!-- SEARCH -->
                            <fieldset style="width: 600px">
                                <legend>Search Criteria:</legend>
                                <div class="lblInfo" style="margin: 10px 10px 10px 10px">
                                    <table width="99%">
                                        <tr>
                                            <td style="width: 33%">
                                                <div class="lblInfo">
                                                    Created From Date<br />
                                                    <telerik:RadDatePicker ID="dtFrom" runat="server" Font-Names="Arial" MinDate="1/1/0001 12:00:00 AM"
                                                        Width="98%">
                                                        <ClientEvents OnDateSelected="ValidateDateRange" />
                                                    </telerik:RadDatePicker>
                                                </div>
                                            </td>
                                            <td style="width: 33%">
                                                <div class="lblInfo">
                                                    To Date<br />
                                                    <telerik:RadDatePicker ID="dtTo" runat="server" Font-Names="Arial" MinDate="1/1/0001 12:00:00 AM"
                                                        Width="98%">
                                                        <ClientEvents OnDateSelected="ValidateDateRange" />
                                                    </telerik:RadDatePicker>
                                                </div>
                                            </td>
                                            <td style="width: 33%">
                                                <div class="lblInfo">
                                                    Last Name (opt)<br />
                                                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="30" Width="98%" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="text-align: center">
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_OnClick" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="div_container">
            <div class="lblInfoBold">
                Selected Job Applications:<br />
                <asp:Label ID="lblCount" runat="server" Text="Count: 0"></asp:Label>
            </div>
            <!-- Job App Results -->
            <telerik:RadGrid ID="RadGrid1" EnableViewState="false" GridLines="None" Height="200" Width="100%" runat="server"
                AllowAutomaticDeletes="false" AllowAutomaticInserts="false" AllowAutomaticUpdates="false"
                AllowMultiRowEdit="false" AllowMultiRowSelection="false"
                AllowPaging="false" AllowSorting="true" AutoGenerateColumns="false" BorderWidth="1">
                <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="Jobapplicationid"
                    AllowAutomaticUpdates="false" AllowAutomaticInserts="false" AllowAutomaticDeletes="false"
                    NoMasterRecordsText="No Job Applications Found" AllowSorting="true" BorderWidth="1"
                    CommandItemSettings-ShowRefreshButton="false" CommandItemSettings-ShowAddNewRecordButton="false"
                    TableLayout="Fixed" HorizontalAlign="Left" RowIndicatorColumn-Display="true"
                    AutoGenerateColumns="false">
                    <Columns>
                        <telerik:GridBoundColumn DataField="Jobapplicationid" HeaderText="Id" AllowSorting="false"
                            UniqueName="JobappidColumn" ReadOnly="true" Visible="false">
                        </telerik:GridBoundColumn>

                        <telerik:GridTemplateColumn HeaderText="Options">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkView" runat="server" Text="View" Target="_blank"
                                    NavigateUrl='<%# "~/Careers/EmailFullJobApplication.aspx?Id=" + 
                                            Eval("Guidkey").ToString() + "&Admin=true" %>'>
                                </asp:HyperLink>&nbsp;&nbsp;
                                        <asp:HyperLink ID="HyperLink1" runat="server" Text="Email" Target="_blank"
                                            NavigateUrl='<%# "~/Careers/EmailFullJobApplication.aspx?Id=" + 
                                            Eval("Guidkey").ToString() + "&Admin=true&Email=" + 
                                            ProviderUtils.GetUser(Profile.UserName).Email %>'>
                                        </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle Width="90px" HorizontalAlign="Center" />
                            <ItemStyle Width="90px" HorizontalAlign="Center" />
                        </telerik:GridTemplateColumn>

                        <telerik:GridBoundColumn DataField="Sigdate" HeaderText="Created On" AllowSorting="true"
                            UniqueName="CreatedateColumn" ReadOnly="true" Visible="true" DataFormatString="{0:d}">
                            <HeaderStyle Width="90px" HorizontalAlign="Center" />
                            <ItemStyle Width="90px" HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="Firstname" HeaderText="First Name" AllowSorting="true"
                            UniqueName="FirstnameColumn" ReadOnly="true" Visible="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Middlename" HeaderText="Middle Name" AllowSorting="true"
                            UniqueName="MiddlenameColumn" ReadOnly="true" Visible="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Lastname" HeaderText="Last Name" AllowSorting="true"
                            UniqueName="LastnameColumn" ReadOnly="true" Visible="true">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="Signature" HeaderText="Email" AllowSorting="true"
                            UniqueName="EmailColumn" ReadOnly="true" Visible="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Phonenumber" HeaderText="Phone" AllowSorting="true"
                            UniqueName="PhoneColumn" ReadOnly="true" Visible="true">
                            <HeaderStyle Width="80px" HorizontalAlign="Center" />
                            <ItemStyle Width="80px" HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="Currentaddress" HeaderText="Address" AllowSorting="true"
                            UniqueName="AddressColumn" ReadOnly="true" Visible="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Currentcity" HeaderText="City" AllowSorting="true"
                            UniqueName="CityColumn" ReadOnly="true" Visible="true">
                            <HeaderStyle Width="80px" HorizontalAlign="Center" />
                            <ItemStyle Width="80px" HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Currentstate" HeaderText="State" AllowSorting="true"
                            UniqueName="StateColumn" ReadOnly="true" Visible="true">
                            <HeaderStyle Width="50px" HorizontalAlign="Center" />
                            <ItemStyle Width="50px" HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Currentzip" HeaderText="Zip" AllowSorting="true"
                            UniqueName="ZipColumn" ReadOnly="true" Visible="true">
                            <HeaderStyle Width="50px" HorizontalAlign="Center" />
                            <ItemStyle Width="50px" HorizontalAlign="Center" />
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="Guidkey" HeaderText="GUID Key" AllowSorting="false"
                            UniqueName="GuidkeyColumn" ReadOnly="true" Visible="true">
                            <HeaderStyle Width="250px" />
                            <ItemStyle Width="250px" />
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <ExportSettings FileName="JobAppManager" OpenInNewWindow="true" ExportOnlyData="true"
                    IgnorePaging="true" HideStructureColumns="true">
                </ExportSettings>
                <ClientSettings EnablePostBackOnRowClick="false">
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
            </telerik:RadGrid>
            <br />
        </div>
    </div>
</asp:Content>

