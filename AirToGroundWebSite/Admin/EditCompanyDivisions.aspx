﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="EditCompanyDivisions.aspx.cs" Inherits="Admin_EditCompanyDivisions"
    Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <devart:DbLinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Companydivision" OrderBy="Description" TableName="Companydivisions"
        EnableInsert="true" EnableUpdate="true" EnableDelete="true">
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Divisionarea" OrderBy="Description" TableName="Divisionareas">
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource3" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Customer" OrderBy="Description" TableName="Customers">
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource4" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Companycustomerjoin" OrderBy="Customer.Description"
        TableName="Companycustomerjoins" EnableInsert="True" EnableUpdate="True"
        EnableDelete="True" Where="Companydivisionid == Convert.ToInt32(@Companydivisionid)">
        <InsertParameters>
            <asp:Parameter Name="Companydivisionid" Type="Int32" />
            <asp:Parameter Name="Customerid" Type="Int32" />
        </InsertParameters>
        <WhereParameters>
            <asp:ControlParameter ControlID="RadGrid1" Name="Companydivisionid" PropertyName="SelectedValue"
                Type="Int32" />
        </WhereParameters>
    </devart:DbLinqDataSource>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }
            function onRequestStart(sender, args)
            {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToPdfButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="true" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" Width="250" Height="75">
    </telerik:RadWindowManager>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Company Divisions"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Company Divisions</div>
            <%--<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />--%>
            <telerik:RadGrid ID="RadGrid1" GridLines="None"
                runat="server" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                AllowPaging="False" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="LinqDataSource1"
                OnItemUpdated="RadGrid1_ItemUpdated" OnItemDeleted="RadGrid1_ItemDeleted" OnItemInserted="RadGrid1_ItemInserted"
                OnDataBound="RadGrid1_DataBound" >
                <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="Companydivisionid"
                    DataSourceID="LinqDataSource1" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                    EditMode="InPlace" AllowSorting="true" AllowPaging="false" CommandItemSettings-ShowRefreshButton="false">
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn DataField="Companydivisionid" HeaderText="Id" UniqueName="CompanydivisionidColumn"
                            ColumnEditorID="GridTextBoxColumnEditor1" ItemStyle-ForeColor="Black" ReadOnly="true"
                            AllowSorting="false" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="Description" HeaderText="Description" SortExpression="Description"
                            UniqueName="DescriptionColumn">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblDescription" Text='<%# Eval("Description") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescription" runat="server" MaxLength="50" Text='<%# Bind("Description") %>' Width="98%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtDescription"
                                    ErrorMessage="<br />Required" runat="server" CssClass="stdValidator" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="Location" HeaderText="Location" SortExpression="Location" UniqueName="LocationColumn">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblLocation" Text='<%# Eval("Location") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLocation" runat="server" MaxLength="30" Text='<%# Bind("Location") %>' Width="98%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtLocation"
                                    ErrorMessage="<br />Required" runat="server" CssClass="stdValidator"
                                    Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <HeaderStyle Width="150px" />
                            <HeaderStyle Width="150px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Division Area" DataField="Divisionarea.Description"
                            UniqueName="DivisionareaColumn" SortExpression="Divisionarea.Description">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblDivisionarea" Text='<%# Eval("Divisionarea.Description") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadComboBox ID="lstDivisionarea" runat="server" Width="98%" DataSourceID="LinqDataSource2"
                                    DataTextField="Description" DataValueField="Divisionareaid" SelectedValue='<%# Bind("Divisionareaid") %>'
                                    OnDataBound="lstDivisionarea_DataBound">
                                </telerik:RadComboBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="lstDivisionarea"
                                    ErrorMessage="<br />Required" runat="server" CssClass="stdValidator"
                                    Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <HeaderStyle Width="100px" />
                            <HeaderStyle Width="100px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                            UniqueName="DeleteCommandColumn" CommandName="Delete">
                            <ItemStyle Width="40px" />
                            <HeaderStyle Width="40px" />
                        </telerik:GridButtonColumn>
                    </Columns>
                    <DetailTables>
                        <telerik:GridTableView DataKeyNames="Companydivisionid,Customerid" BorderColor="Black"
                            DataSourceID="LinqDataSource4" runat="server" CommandItemDisplay="Top" EditMode="InPlace"
                            Name="CompanyCustomers" AllowSorting="true" AllowFilteringByColumn="false" AllowAutomaticInserts="true"
                            AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" AllowPaging="false">
                            <ParentTableRelation>
                                <telerik:GridRelationFields DetailKeyField="Companydivisionid" MasterKeyField="Companydivisionid" />
                            </ParentTableRelation>
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                    <ItemStyle Width="60px" />
                                </telerik:GridEditCommandColumn>
                                <telerik:GridTemplateColumn HeaderText="Customer" DataField="Customer.Description"
                                    UniqueName="CustomeridColumn" SortExpression="Customer.Description">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblCustomer" Text='<%# Eval("Customer.Description") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <telerik:RadComboBox ID="lstCustomer" runat="server" Width="25%" DataSourceID="LinqDataSource3"
                                            DataTextField="Description" DataValueField="Customerid" SelectedValue='<%# Bind("Customerid") %>'
                                            OnDataBound="lstCustomer_DataBound">
                                        </telerik:RadComboBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="lstCustomer"
                                            ErrorMessage="<br />Required" runat="server" CssClass="stdValidator"
                                            Display="Dynamic">
                                        </asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record?"
                                    ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                                    UniqueName="DeleteCommandColumn" CommandName="Delete">
                                    <ItemStyle Width="40px" />
                                </telerik:GridButtonColumn>
                            </Columns>
                        </telerik:GridTableView>
                    </DetailTables>
                    <CommandItemSettings ShowExportToCsvButton="true" />
                    <AlternatingItemStyle BackColor="#F2F0F2" />
                </MasterTableView>
                <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false"
                    Csv-FileExtension="csv" />
                <ClientSettings>
                    <ClientEvents OnRowDblClick="RowDblClick" />
                    <Selecting AllowRowSelect="true" />
                    <Scrolling AllowScroll="False" />
                </ClientSettings>
            </telerik:RadGrid>
            <telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditor1" runat="server" TextBoxStyle-Width="100px" />
        </div>
    </div>
</asp:Content>

