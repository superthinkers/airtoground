﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using Telerik.Web.UI;
using ATGDB;

public partial class Admin_EditEquipment : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_AIRCRAFT;
    }
    // For postbacks in the JavaScript in the page.
    protected string PostBackString;

    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // For postbacks in the JavaScript in the page.
        PostBackString = Page.ClientScript.GetPostBackEventReference(this, "");
        //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
        //m.IsActive = true;

        if (!Page.IsPostBack) {
            // Security.
            if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_EQUIP)) {
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
            }
        } else {
            SetParams();
        }
    }
    protected void SetParams() {
        // Where statements.
        string sCWhere = "(Customerid == Convert.ToInt32(@Customerid))";
        string sEQTWhere = "(Equipmenttypeid == Convert.ToInt32(@Equipmenttypeid))";
        string sDESCRWhere = "(Description.ToUpper().Contains(@Description.ToUpper()))";
        string sTNWhere = "(Tailnumber.ToUpper().Contains(@Tailnumber.ToUpper()))";

        RadComboBox lstCustomerFilter = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1, "lstCustomerFilter");
        RadComboBox lstEquipmentTypeFilter = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1, "lstEquipmentTypeFilter");
        RadTextBox txtDescriptionFilter = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1, "txtDescriptionFilter");
        RadTextBox txtTailnumberFilter = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1, "txtTailnumberFilter");

        // Clear first.
        LinqDataSource1.WhereParameters.Clear();
            
        // Customer Filter.
        string id = lstCustomerFilter.SelectedValue;
        if (int.Parse(id) > 0) {
            if (LinqDataSource1.WhereParameters["Customerid"] == null) {
                LinqDataSource1.WhereParameters.Add("Customerid", System.Data.DbType.Int32, id);
            }
        }

        // EquipmentType Filter.
        id = lstEquipmentTypeFilter.SelectedValue;
        if (int.Parse(id) > 0) {
            if (LinqDataSource1.WhereParameters["Equipmenttypeid"] == null) {
                LinqDataSource1.WhereParameters.Add("Equipmenttypeid", System.Data.DbType.Int32, id);
            }
        }

        // Description Filter.
        id = txtDescriptionFilter.Text;
        if (!String.IsNullOrEmpty(id)) {
            if (LinqDataSource1.WhereParameters["Description"] == null) {
                LinqDataSource1.WhereParameters.Add("Description", System.Data.DbType.String, id);
            }
        }

        // Tailnumber Filter.
        id = txtTailnumberFilter.Text;
        if (!String.IsNullOrEmpty(id)) {
            if (LinqDataSource1.WhereParameters["Tailnumber"] == null) {
                LinqDataSource1.WhereParameters.Add("Tailnumber", System.Data.DbType.String, id);
            }
        }

        // Now, based on existing where parameters, build the where statement.
        // Clear first.
        LinqDataSource1.Where = "";
        
        // Customer
        if (LinqDataSource1.WhereParameters["Customerid"] != null) {
            LinqDataSource1.Where = sCWhere;
        }

        // EquipmentType
        if (LinqDataSource1.WhereParameters["Equipmenttypeid"] != null) {
            if (LinqDataSource1.Where == "") {
                LinqDataSource1.Where = sEQTWhere;
            } else {
                LinqDataSource1.Where = LinqDataSource1.Where + " && " + sEQTWhere;
            }
        }

        // Description
        if (LinqDataSource1.WhereParameters["Description"] != null) {
            if (LinqDataSource1.Where == "") {
                LinqDataSource1.Where = sDESCRWhere;
            } else {
                LinqDataSource1.Where = LinqDataSource1.Where + " && " + sDESCRWhere;
            }
        }

        // Tailnumber
        if (LinqDataSource1.WhereParameters["Tailnumber"] != null) {
            if (LinqDataSource1.Where == "") {
                LinqDataSource1.Where = sTNWhere;
            } else {
                LinqDataSource1.Where = LinqDataSource1.Where + " && " + sTNWhere;
            }
        }
    }
    protected void lstEqType_DataBound(object sender, EventArgs e) {
        // First reference the edited grid item through the NamingContainer attribute
        RadComboBox lst = (RadComboBox)sender;
        GridEditableItem editedItem = lst.NamingContainer as GridEditableItem;
        if (editedItem.IsInEditMode == true) {
            TableCell cell = editedItem["CustomeridColumn"];
            string cId = (cell.Controls[1] as RadComboBox).SelectedValue;

            cell = editedItem["EquipmenttypeidColumn"];
            string eqtId = (cell.Controls[1] as RadComboBox).SelectedValue;

            if (cId != "") {
                var eqTypes = Equipmenttype.GetEquipmentTypesByCustomerId(Convert.ToInt32(cId));
                lst.Items.Clear();
                RadComboBoxItem item;
                foreach (ATGDB.Equipmenttype et in eqTypes) {
                    item = new RadComboBoxItem(et.Description, et.Equipmenttypeid.ToString());
                    lst.Items.Add(item);
                    item.DataBind();
                }
                item = new RadComboBoxItem("", "");
                lst.Items.Insert(0, item);
                item.Selected = true;
                item.DataBind();
                if (eqtId != "") {
                    lst.SelectedValue = eqtId;
                }
            } else {
                // Inserting.
                lst.Items.Clear();
            }
        }
    }
    protected void lstCustomer_DataBound(object sender, EventArgs e) {
        RadComboBox lst = (RadComboBox)sender;
        if (lst.Items[0].Value != "") {
            RadComboBoxItem item = new RadComboBoxItem("", "");
            lst.Items.Insert(0, item);
        }
        if (!String.IsNullOrEmpty(lst.SelectedValue)) {
            var eqTypes = Equipmenttype.GetEquipmentTypesByCustomerId(int.Parse(lst.SelectedValue));
            lst = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lstEquipmenttype");
            lst.Items.Clear();
            RadComboBoxItem item;
            foreach (ATGDB.Equipmenttype et in eqTypes) {
                item = new RadComboBoxItem(et.Description, et.Equipmenttypeid.ToString());
                lst.Items.Add(item);
                item.DataBind();
            }
        }
    }                                    
    //protected void lstCustomer_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
    //    RadComboBox lst = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1.MasterTableView, "lstEquipmenttype");
    //    lst.Items.Clear();

    //    var eqTypes = Equipmenttype.GetEquipmentTypesByCustomerId(Convert.ToInt32(e.Value));
    //    lst.DataSource = eqTypes;
    //    lst.SelectedValue = "";
    //}
    protected void lstEquipmenttype_OnItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e) {
        // First reference the edited grid item through the NamingContainer attribute
        RadComboBox lst = (RadComboBox)sender;
        GridEditableItem editedItem = lst.NamingContainer as GridEditableItem;

        TableCell cell;
        string cId = "";
        if (e.Text == "") {
            cell = editedItem["CustomeridColumn"];
            cId = (cell.Controls[1] as RadComboBox).SelectedValue;
        } else {
            cId = e.Text;
        }

        cell = editedItem["EquipmenttypeidColumn"];
        string eqtId = (cell.Controls[1] as RadComboBox).SelectedValue;

        if (cId != "") {
            var eqTypes = Equipmenttype.GetEquipmentTypesByCustomerId(Convert.ToInt32(cId));
            lst.Items.Clear();
            RadComboBoxItem item;
            foreach (ATGDB.Equipmenttype et in eqTypes) {
                item = new RadComboBoxItem(et.Description, et.Equipmenttypeid.ToString());
                lst.Items.Add(item);
                item.DataBind();
            }
            item = new RadComboBoxItem("", "");
            lst.Items.Insert(0, item);
            item.Selected = true;
            item.DataBind();
            if (eqtId != "") {
                lst.SelectedValue = eqtId;
            }
        } else {
            // Inserting.
            lst.Items.Clear();
        }
    }
    protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("Equipmentid").ToString();
               
        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            if (e.Exception.InnerException == null) {
                SetMessage("Aircraft " + id + " cannot be updated. Reason: " +
                    e.Exception.Message);
            } else {
                SetMessage("Aircraft " + id + " cannot be updated. Reason: " +
                    e.Exception.InnerException.Message);
            }
            DisplayMessage(gridMessage);
        } else {
            SetMessage("Aircraft " + id + " is updated!");
        }
    }
    protected void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e) {
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("Aircraft cannot be inserted. Reason: " + e.Exception.InnerException.Message);
            DisplayMessage(gridMessage);
        } else {
            SetMessage("New Aircraft is inserted!");
        }
    }
    protected void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e) {
        GridDataItem dataItem = (GridDataItem)e.Item;
        String id = dataItem.GetDataKeyValue("Equipmentid").ToString();

        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("Aircraft " + id + " cannot be deleted. Reason: " +
                e.Exception.InnerException.Message);
            DisplayMessage(gridMessage);
        } else {
            SetMessage("Aircraft " + id + " is deleted!");
            DisplayMessage(gridMessage);
        }
    }
    protected void RadGrid1_DataBound(object sender, EventArgs e) {
        if (!string.IsNullOrEmpty(gridMessage)) {
            DisplayMessage(gridMessage);
        }
    }
    protected void RadGrid1_ItemCommand(object sender, CommandEventArgs e) {
        // Based on filter, add where parameters, or remove them.
        if ((e.CommandName == "Edit") ||
            (e.CommandName == "Update") ||
            (e.CommandName == "Delete") ||
            (e.CommandName == "InitInsert") ||
            (e.CommandName == "PerformInsert") ||
            (e.CommandName == "Cancel") ||
            (e.CommandName == "DoCustomerFilter") || 
            (e.CommandName == "DoEquipmentTypeFilter") ||
            (e.CommandName == "DoDescriptionFilter") ||
            (e.CommandName == "DoTailnumberFilter")) {

            // Finally, rebind.
            SetParams();

            // If filtering, reset the page.
            if ((e.CommandName == "DoCustomerFilter") ||
                (e.CommandName == "DoEquipmentTypeFilter") ||
                (e.CommandName == "DoDescriptionFilter") ||
                (e.CommandName == "DoTailnumberFilter")) {
                RadGrid1.MasterTableView.CurrentPageIndex = 0;
            }

            LinqDataSource1.DataBind();
        }
    }
    private string gridMessage = null;
    private void DisplayMessage(string text) {
        gridMessage = "";
        RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "RadGrid1");
        grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage(string message) {
        gridMessage = message;
    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e) {
        if (e.Item is GridDataItem) {
            if (!e.Item.IsInEditMode) {
                GridDataItem row = e.Item as GridDataItem;
                var dataRow = (e.Item.DataItem as ATGDB.Equipment);

                // The Data Cells.
                //TableCell logIdCell = row["EquipmentstatuslogidColumn"];
                //TableCell statusDateCell = row["StatusdateColumn"];
                //TableCell unholdDateCell = row["UnholddateColumn"];

                // The Link Cells
                TableCell statusCell = row["StatusColumn"];
                //TableCell holdCell = row["HoldColumn"];
                TableCell unholdCell = row["UnHoldColumn"];

                // Clear Status Indicator.
                if (dataRow.Equipmentstatuslogid <= 0) {
                    // NON-SCHEDULED - NEXT ONE
                    statusCell.Controls.Clear();
                }

                // Clear Hold Indicator, or UnHold Indicator.
				if ((dataRow.Equipmentstatuslogid > 0) && ((dataRow.Equipmentstatuslog != null) && (dataRow.Equipmentstatuslog.Statusdate != null) && (dataRow.Equipmentstatuslog.Unholddate == null))) {
                    //holdCell.Controls.Clear();
				} else if (((dataRow.Equipmentstatuslogid > 0) && (dataRow.Equipmentstatuslog != null) && (dataRow.Equipmentstatuslog.Statusdate != null) && (dataRow.Equipmentstatuslog.Unholddate != null)) || (dataRow.Equipmentstatuslogid == 0)) {
                    unholdCell.Controls.Clear();
				} else if ((dataRow.Equipmentstatuslogid > 0) && (dataRow.Equipmentstatuslog != null) && ((dataRow.Equipmentstatuslog.Statusdate == null) || (dataRow.Equipmentstatuslogid == 0))) {
                    unholdCell.Controls.Clear();
                }
            }
        }
    }
    protected void Status_ValueChanged(object sender, EventArgs e) {
        // Create the Status Log from the hidden fields.
        string logId = hiddenStatusLogId.Value;
        if ((Session["LAST_STATUSLOGID"] == null) || (Session["LAST_STATUSLOGID"].ToString() != logId)) {
            hiddenStatusTrigger.Value = "-1";
            Session["LAST_STATUSLOGID"] = logId;

            string eqId = hiddenEqId.Value;
            string status = hiddenStatus.Value;
            string reason = hiddenStatusReason.Value;

            DateTime statusdate = DateTime.MinValue;
            if (hiddenStatusDate.Value.Trim() != "") {
                if (!DateTime.TryParse(hiddenStatusDate.Value, out statusdate)) {
                    statusdate = PageUtils.LoadDateTimeChars(hiddenStatusDate.Value);
                }
            }
            DateTime returndate = DateTime.MinValue;
            if (hiddenReturnDate.Value.Trim() != "") {
                if (!DateTime.TryParse(hiddenReturnDate.Value, out returndate)) {
                    returndate = PageUtils.LoadDateTimeChars(hiddenReturnDate.Value);
                }
            }
            DateTime unholddate = DateTime.MinValue;
            if (hiddenUnHoldDate.Value.Trim() != "") {
                if (!DateTime.TryParse(hiddenUnHoldDate.Value, out unholddate)) {
                    unholddate = PageUtils.LoadDateTimeChars(hiddenUnHoldDate.Value);
                }
            }
            
            string notes = hiddenStatusNotes.Value;
            string statusby = hiddenStatusBy.Value;
            string unholdby = hiddenUnHoldBy.Value;
            string unholdnotes = hiddenUnHoldNotes.Value;
            string services = hiddenServices.Value;

            // Process.
            if (unholddate == DateTime.MinValue) {
                Equipmentstatuslog.PutOnHold(eqId, logId, status, reason, statusdate, statusby, returndate, notes);
            } else {
                Equipmentstatuslog.ReleaseHold(eqId, logId, services, Profile.Companydivisionid, unholddate, unholdby, unholdnotes);
            }

            // Rebind.
            RadGrid1.Rebind();
        }
    }
}