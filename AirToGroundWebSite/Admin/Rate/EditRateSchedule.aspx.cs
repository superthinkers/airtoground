﻿using System;
using System.Linq;

public partial class Admin_Rate_EditRateSchedule : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_RATE_SCHEDULE;
    }
    // For postbacks in the JavaScript in the page.
    protected string PostBackString;

    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // For postbacks in the JavaScript in the page.
        PostBackString = Page.ClientScript.GetPostBackEventReference(this, "");
        //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
        //m.IsActive = true;

        if (!Page.IsPostBack) {
            // Clear session variable.
            Session.Add(ATGDB.Ratedata.SESSION_RATESCHEDULE_REFRESH, false);

            // Load combo.
            //lstCompCust.DataSource = ATGDB.Companydivision.GetCustomCCJ();
            //lstCompCust.DataBind();

            // Load the rate schedule.
            LoadRateSchedule();
            //if (lstCompCust.SelectedValue != null) {
            //    LoadRateSchedule();
            //} else {
            //    if (lstCompCust.Items.Count > 0) {
            //        lstCompCust.Items[0].Selected = true;
            //        LoadRateSchedule();
            //    }
            //}
        } else {
            // Refresh
            if (Convert.ToBoolean(Session[ATGDB.Ratedata.SESSION_RATESCHEDULE_REFRESH]) == true) {
                Session.Add(ATGDB.Ratedata.SESSION_RATESCHEDULE_REFRESH, false);
                LoadRateSchedule();
            }
        }
    }
    protected void LoadRateSchedule() {
        RadGrid1.DataSource = ATGDB.Ratedata.GetRatedataByCompCustKeys(Profile.Companydivisionid + ":" + Profile.Customerid);//lstCompCust.SelectedValue);
        RadGrid1.DataBind();
        if (RadGrid1.Items.Count > 0) {
            RadGrid1.Items[0].Selected = true;
        }
        SetUserOptions();
    }
    protected void lstCompCust_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e) {
        // Reload the rate schedule.
        LoadRateSchedule();
    }
    protected void RadGrid1_SelectedIndexChanged(object sender, EventArgs e) {
        // Set user options.
        SetUserOptions();
    }
    protected void SetUserOptions() {
        if (RadGrid1.SelectedValue != null) {

            // Get currently selected item.
            int id = int.Parse(RadGrid1.SelectedValue.ToString());
            ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
            ATGDB.Jobratejoin jrj = db.Jobratejoins.SingleOrDefault(x => x.Jobratejoinid == id);
            
            // Break up the keys. Similar idea in Timesheets
            string keys = Profile.Companydivisionid + ":" + Profile.Customerid;// lstCompCust.SelectedValue;
            string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
            string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);
            ATGDB.Companydivision cd = ATGDB.Companydivision.GetCompanyDivisionById(companyDivisionId);
            ATGDB.Customer c = ATGDB.Customer.GetCustomerById(customerId);

            // Set labels.
            btnOption1.Text = "1) I want these rates to be specific to " + 
                cd.Description + ", regardless of the Customer, for this Job Code";
            btnOption2.Text = "2) I want these rates to be specific to " +  
                c.Description + ", regardless of the Company Division, for this Job Code";
            btnOption3.Text = "3) I want these rates to be specific to " +
                cd.Description + " and " + c.Description + ", for this Job Code";
            btnOption4.Text = "4) I want these rates to be specific to the Standard Corporate rates, for this Job Code";
            btnOption5.Text = "5) Edit Rates";

            // Set click values.
            btnOption1.OnClientClick = "openOption(\"" + RadGrid1.SelectedValue.ToString() +
                "\",\"" + "1\",\"" + Profile.Companydivisionid + ":" + Profile.Customerid + //lstCompCust.SelectedValue + 
                "\"); return false;";
            btnOption2.OnClientClick = "openOption(\"" + RadGrid1.SelectedValue.ToString() +
                "\",\"" + "2\",\"" + Profile.Companydivisionid + ":" + Profile.Customerid + //lstCompCust.SelectedValue + 
                "\"); return false;";
            btnOption3.OnClientClick = "openOption(\"" + RadGrid1.SelectedValue.ToString() +
                "\",\"" + "3\",\"" + Profile.Companydivisionid + ":" + Profile.Customerid + //lstCompCust.SelectedValue + 
                "\"); return false;";
            btnOption4.OnClientClick = "openOption(\"" + RadGrid1.SelectedValue.ToString() +
                "\",\"" + "4\",\"" + Profile.Companydivisionid + ":" + Profile.Customerid + //lstCompCust.SelectedValue + 
                "\"); return false;";
            btnOption5.OnClientClick = "openOption(\"" + RadGrid1.SelectedValue.ToString() +
                "\",\"" + "5\",\"" + Profile.Companydivisionid + ":" + Profile.Customerid + //lstCompCust.SelectedValue + 
                "\"); return false;";

            // Enable/Disable - can't pick same as current one.
            btnOption1.Enabled = true;
            btnOption2.Enabled = true;
            btnOption3.Enabled = true;
            btnOption4.Enabled = true;
            if ((jrj.Companydivisionid != null) && (jrj.Customerid == null)) {
                btnOption1.Enabled = false;
            } else if ((jrj.Companydivisionid == null) && (jrj.Customerid != null)) {
                btnOption2.Enabled = false;
            } else if ((jrj.Companydivisionid != null) && (jrj.Customerid != null)) {
                btnOption3.Enabled = false;
            } else if ((jrj.Companydivisionid == null) && (jrj.Customerid == null)) {
                btnOption4.Enabled = false;
            }

        }
    }



}