﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

public partial class Admin_Rate_ChangeRate : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_CHANGE_RATE;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);

        if (!Page.IsPostBack) {
            // Clear session variable
            Session.Add(ATGDB.Ratedata.SESSION_RATESCHEDULE_REFRESH, false);

            if ((Request.QueryString["Jobratejoinid"] != null) &&
                (Request.QueryString["Option"] != null)) {
                
                // Set the current rate data.
                SetCurrentRateData();

                // Now set the new rate data.
                string option = Request.QueryString["Option"];
                string id = Request.QueryString["Jobratejoinid"];
                ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
                ATGDB.Jobratejoin jrj = db.Jobratejoins.SingleOrDefault(x => x.Jobratejoinid == int.Parse(id));
                SetNewRateData(option, jrj.Jobcodeid);
            }
        }
    }
    protected void SetCurrentRateData() {
        // Get the parameters.
        string id = Request.QueryString["Jobratejoinid"];
        string keys = Request.QueryString["Keys"];

        // Set option. Should be 1-5.
        string option = Request.QueryString["Option"];
        RadioButton rdoOption = (RadioButton)PageUtils.FindControlRecursive(Page, "rdoOption" + option);
        if (rdoOption != null) {
            rdoOption.Checked = true;
        }

        // Lookup current jobratejoin and set UI elements for current rate.
        ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
        var data = from jrj in db.Jobratejoins
                   where jrj.Jobratejoinid == int.Parse(id)
                   join r1 in db.Ratedatas
                   on jrj.Rateid equals r1.Rateid into r2
                   from r in r2.DefaultIfEmpty()
                   where r.Isactive == true
                   join cd1 in db.Companydivisions
                   on jrj.Companydivisionid equals cd1.Companydivisionid into cd2
                   from cd in cd2.DefaultIfEmpty()
                   join c1 in db.Customers
                   on jrj.Customerid equals c1.Customerid into c2
                   from c in c2.DefaultIfEmpty()
                   select new ATGDB.Ratedata.MyJobRateJoinResult() {
                       Jobratejoinid = jrj.Jobratejoinid,
                       Jobcodeid = jrj.Jobcodeid,
                       Rateid = jrj.Rateid,
                       Companydivisionid = jrj.Companydivisionid ?? null,
                       Customerid = jrj.Customerid ?? null,
                       CompanyDescr = jrj.Companydivisionid == null ? "All Companies" : cd.Description,
                       CustomerDescr = jrj.Customerid == null ? "All Customers" : c.Description,
                       Jobcodedescr = jrj.Jobcode.Description,
                       Ratedescr = r.Description,
                       Flatfeeorhourly = r.Flatfeeorhourly,
                       Cost = r.Cost,
                       Rate = r.Rate,
                       Stdhours = r.Stdhours,
                       Stdquantity = r.Stdquantity,
                       Changehistory = r.Changehistory,
                       Upddt = r.Upddt,
                       Upduserid = r.Upduserid,
                       Imagesrc = ""
                   };

        // Assign the UI elements for current rate.
        if (data.Count() > 0) {
            hiddenJobCodeId.Value = data.ElementAt(0).Jobcodeid.ToString();
            hiddenRateId.Value = data.ElementAt(0).Rateid.ToString();
            lblJobCode1a.Text = data.ElementAt(0).Jobcodedescr;
            lblJobCode2a.Text = data.ElementAt(0).Jobcodedescr;
            lblCompany1a.Text = data.ElementAt(0).CompanyDescr;
            lblCustomer1a.Text = data.ElementAt(0).CustomerDescr;
            lblRateDescr1a.Text = data.ElementAt(0).Ratedescr;
            lblFlatfeeorhourly1a.Text = data.ElementAt(0).Flatfeeorhourly.ToString();
            lblCost1a.Text = data.ElementAt(0).Cost.ToString("$#,##0.00");
            lblRate1a.Text = data.ElementAt(0).Rate.ToString("$#,##0.00");
            lblStdhours1a.Text = data.ElementAt(0).Stdhours.ToString("#,##0.00");
            lblStdquantity1a.Text = data.ElementAt(0).Stdquantity.ToString("#,##0.00");
            lblUpdDt1a.Text = data.ElementAt(0).Upddt.ToString();
            lblUpdUserId1a.Text = data.ElementAt(0).Upduserid;
        }

        // Break up the keys. Similar idea in Timesheets
        string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
        string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);
        ATGDB.Companydivision comp = ATGDB.Companydivision.GetCompanyDivisionById(companyDivisionId);
        ATGDB.Customer cust = ATGDB.Customer.GetCustomerById(customerId);

        // Set labels.
        rdoOption1.Text = "1) I want these rates to be specific to " +
            comp.Description + ", regardless of the Customer, for this Job Code";
        rdoOption2.Text = "2) I want these rates to be specific to " +
            cust.Description + ", regardless of the Company Division, for this Job Code";
        rdoOption3.Text = "3) I want these rates to be specific to " +
            comp.Description + " and " + cust.Description + ", for this Job Code";
        rdoOption4.Text = "4) I want these rates to be specific to the Standard Corporate rates, for this Job Code";

        // Enable/Disable - can't pick same as current one.
        if (int.Parse(option) < 5) {
            rdoOption1.Enabled = true;
            rdoOption2.Enabled = true;
            rdoOption3.Enabled = true;
            rdoOption4.Enabled = true;
            if ((data.ElementAt(0).Companydivisionid != null) && (data.ElementAt(0).Customerid == null)) {
                rdoOption1.Enabled = false;
            } else if ((data.ElementAt(0).Companydivisionid == null) && (data.ElementAt(0).Customerid != null)) {
                rdoOption2.Enabled = false;
            } else if ((data.ElementAt(0).Companydivisionid != null) && (data.ElementAt(0).Customerid != null)) {
                rdoOption3.Enabled = false;
            } else if ((data.ElementAt(0).Companydivisionid == null) && (data.ElementAt(0).Customerid == null)) {
                rdoOption4.Enabled = false;
            }
        } else {
            // Just editing current rate.
            rdoOption1.Enabled = false;
            rdoOption2.Enabled = false;
            rdoOption3.Enabled = false;
            rdoOption4.Enabled = false;
        }
    }
    protected void SetNewRateData(string option, int jobcodeid) {
        ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();

        // Get the keys again.
        string keys = Request.QueryString["Keys"];
        string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
        string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);

        if (int.Parse(option) < 5) {

            // Get the new data, based on user's chosen option.
            // There may, or may not be an existing record.
            var data = from jrj in db.Jobratejoins
                       where jrj.Jobcodeid == jobcodeid
                       where ((option == "4") || (option == "2")) ? jrj.Companydivisionid == null :
                             ((option == "1") || (option == "3")) ? jrj.Companydivisionid == int.Parse(companyDivisionId) : jrj.Companydivisionid == null
                       where ((option == "4") || (option == "1")) ? jrj.Customerid == null :
                             ((option == "2") || (option == "3")) ? jrj.Customerid == int.Parse(customerId) : jrj.Customerid == null
                       from r in db.Ratedatas
                       where r.Rateid == jrj.Rateid
                       //where r.Isactive == false // DONT DO THIS - COULD BE ACTIVE.
                       join cd1 in db.Companydivisions
                       on jrj.Companydivisionid equals cd1.Companydivisionid into cd2
                       from cd in cd2.DefaultIfEmpty()
                       join c1 in db.Customers
                       on jrj.Customerid equals c1.Customerid into c2
                       from c in c2.DefaultIfEmpty()
                       select new ATGDB.Ratedata.MyJobRateJoinResult() {
                           Jobratejoinid = jrj.Jobratejoinid,
                           Jobcodeid = jrj.Jobcodeid,
                           Rateid = jrj.Rateid,
                           Companydivisionid = jrj.Companydivisionid ?? null,
                           Customerid = jrj.Customerid ?? null,
                           CompanyDescr = jrj.Companydivisionid == null ? "All Companies" : cd.Description,
                           CustomerDescr = jrj.Customerid == null ? "All Customers" : c.Description,
                           Jobcodedescr = jrj.Jobcode.Description,
                           Ratedescr = r.Description,
                           Flatfeeorhourly = r.Flatfeeorhourly,
                           Cost = r.Cost,
                           Rate = r.Rate,
                           Stdhours = r.Stdhours,
                           Stdquantity = r.Stdquantity,
                           Changehistory = r.Changehistory,
                           Upddt = r.Upddt,
                           Upduserid = r.Upduserid,
                           Imagesrc = ""
                       };

            // Assign the UI elements for current rate.
            if (data.Count() > 0) {
                // There is already a rate defined, use it.
                lblHeaderNew.Text = "Change Rate To:";
                hiddenRateId.Value = data.ElementAt(0).Rateid.ToString(); 
                lblJobCode2a.Text = data.ElementAt(0).Jobcodedescr;
                lblCompany2a.Text = data.ElementAt(0).CompanyDescr;
                lblCustomer2a.Text = data.ElementAt(0).CustomerDescr;
                txtRateDescr2a.Text = data.ElementAt(0).Ratedescr;
                lstFlatfeeorhourly2a.Text = data.ElementAt(0).Flatfeeorhourly.ToString();
                txtCost2a.Text = data.ElementAt(0).Cost.ToString();
                txtRate2a.Text = data.ElementAt(0).Rate.ToString();
                txtStdhours2a.Text = data.ElementAt(0).Stdhours.ToString();
                txtStdquantity2a.Text = data.ElementAt(0).Stdquantity.ToString();
                lblUpdDt2a.Text = data.ElementAt(0).Upddt.ToString();
                lblUpdUserId2a.Text = data.ElementAt(0).Upduserid;
            } else {
                // A rate is not defined for this option. Set all zeros
                ATGDB.Companydivision cd = db.Companydivisions.Single(x => x.Companydivisionid == int.Parse(companyDivisionId));
                ATGDB.Customer c = db.Customers.Single(x => x.Customerid == int.Parse(customerId));
                lblHeaderNew.Text = "Create new Rate";
                hiddenRateId.Value = "-1";
                lblJobCode2a.Text = lblJobCode1a.Text;
                lblCompany2a.Text = (option == "4") ? "All Companies" :
                                    ((option == "1") || (option == "3")) ? cd.Description : "All Companies";
                lblCustomer2a.Text = (option == "4") ? "All Customers" :
                                     ((option == "2") || (option == "3")) ? c.Description : "All Customers";
                txtRateDescr2a.Text = "";
                lstFlatfeeorhourly2a.ClearSelection();
                txtCost2a.Value = 0.00;
                txtRate2a.Value = 0.00;
                txtStdhours2a.Value = 0.00;
                txtStdquantity2a.Value = 0.00;
                lblUpdDt2a.Text = DateTime.Now.ToString();
                lblUpdUserId2a.Text = Profile.UserName;
                // Focus the rate descr
                txtRateDescr2a.Focus();
            }
        } else {
            // Editing existing rate.
            lblHeaderNew.Text = "Updating Current Rate";
            // This hidden field set in SetNewRateData()
            //hiddenRateId.Value = data.ElementAt(0).Rateid.ToString();
            lblJobCode2a.Text = lblJobCode1a.Text;
            lblCompany2a.Text = lblCompany1a.Text;
            lblCustomer2a.Text = lblCustomer1a.Text;
            txtRateDescr2a.Text = lblRateDescr1a.Text;
            lstFlatfeeorhourly2a.SelectedValue = lblFlatfeeorhourly1a.Text;
            txtCost2a.Value = Convert.ToDouble(lblCost1a.Text.Substring(1, lblCost1a.Text.Length-1));
            txtRate2a.Value = Convert.ToDouble(lblRate1a.Text.Substring(1, lblRate1a.Text.Length - 1));
            txtStdhours2a.Value = Convert.ToDouble(lblStdhours1a.Text);
            txtStdquantity2a.Value = Convert.ToDouble(lblStdquantity1a.Text);
            lblUpdDt2a.Text = DateTime.Now.ToString();
            lblUpdUserId2a.Text = Profile.UserName;
        }
    }
    protected void rdoOption1_CheckedChanged(object sender, EventArgs e) {
        SetNewRateData("1", int.Parse(hiddenJobCodeId.Value));
    }
    protected void rdoOption2_CheckedChanged(object sender, EventArgs e) {
        SetNewRateData("2", int.Parse(hiddenJobCodeId.Value));
    }
    protected void rdoOption3_CheckedChanged(object sender, EventArgs e) {
        SetNewRateData("3", int.Parse(hiddenJobCodeId.Value));
    }
    protected void rdoOption4_CheckedChanged(object sender, EventArgs e) {
        SetNewRateData("4", int.Parse(hiddenJobCodeId.Value));
    }
    protected void btnSaveChanges_OnClick(object sender, EventArgs e) {
        // Save Changes.
         ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();

        // Get the keys again.
        string id = Request.QueryString["Jobratejoinid"];
        string oldOption = Request.QueryString["Option"];
        string keys = Request.QueryString["Keys"];
        string companyDivisionId = keys.Substring(0, keys.IndexOf(":"));
        string customerId = keys.Substring(keys.IndexOf(":") + 1, keys.Length - keys.IndexOf(":") - 1);
        int jobcodeid = int.Parse(hiddenJobCodeId.Value);
        int rateid = int.Parse(hiddenRateId.Value);

        if (int.Parse(oldOption) < 5) {

            // Get our source/current option
            string curOption = "";
            if (rdoOption1.Checked == true) {
                curOption = "1";
            } else if (rdoOption2.Checked == true) {
                curOption = "2";
            } else if (rdoOption3.Checked == true) {
                curOption = "3";
            } else if (rdoOption4.Checked == true) {
                curOption = "4";
            }

            // Collect the data into a handy container.
            // This is what the NEW rate looks like.
            ATGDB.Ratedata.MyJobRateJoinResult data = new ATGDB.Ratedata.MyJobRateJoinResult();
            // New record, create it.
            data.Jobcodeid = jobcodeid;
            data.Rateid = rateid;
            data.Ratedescr = txtRateDescr2a.Text;
            data.Flatfeeorhourly = lstFlatfeeorhourly2a.SelectedValue;
            data.Cost = txtCost2a.Value ?? 0.00;
            data.Rate = txtRate2a.Value ?? 0.00;
            data.Stdhours = txtStdhours2a.Value ?? 0.00;
            data.Stdquantity = txtStdquantity2a.Value ?? 0.00;
            data.Upddt = DateTime.Now;
            data.Upduserid = Profile.UserName;

            if (curOption == "1") {
                data.Companydivisionid = int.Parse(companyDivisionId);
                data.Customerid = null;
            } else if (curOption == "2") {
                data.Companydivisionid = null;
                data.Customerid = int.Parse(customerId);
            } else if (curOption == "3") {
                data.Companydivisionid = int.Parse(companyDivisionId);
                data.Customerid = int.Parse(customerId);
            } else if (curOption == "4") {
                data.Companydivisionid = null;
                data.Customerid = null;
            }

            // Take current rate and decide what to do.
            if (curOption == "4") {
                // Changing to corporate rates.
                // Any company rate, or customer rate, or company/cust rate must be disabled.
                // This is a total downgrade to corporate rates. Any other rates must be disabled.
                // So, any rates for this jobcode get IsActive = false, except corporate.
                ATGDB.Ratedata.DisableAllRatesForJobCodeExceptCorporate(keys, data);
            } else if (curOption == "3") {
                // This is a company/cust rate override. 
                // In this case, just create the override record, or set to active if it exists.
                // This affects just the company and customer that is selected.
                // This is highly custom to the current selected comp/cust.
                ATGDB.Ratedata.CreateCompanyCustomerRateOverrideForJobCode(keys, data);
            } else if (curOption == "2") {
                // This is a customer override. 
                // In this scenario, the customer is upgrading or downgrading to a customer override.
                // Create the override record, or set to active if it exists.
                // There may be a comp/cust record that needs to be disabled.
                ATGDB.Ratedata.CreateCustomerRateOverrideForJobCode(keys, data);
            } else if (curOption == "1") {
                // This is a company override. Look for company/cust override, and/or cust override, and make inactive.
                // In this scenario, the user is essentially downgrading either a cust override, or a comp/cust override,
                // if they exist, and upgrading a corporate standard rate.
                ATGDB.Ratedata.CreateCompanyRateOverrideForJobCode(keys, data);
            }
        } else {
            // Edit mode - Save new edits
            ATGDB.Jobratejoin jrj = db.Jobratejoins.SingleOrDefault(x => x.Jobratejoinid == int.Parse(id));
            if (jrj != null) {
                jrj.Ratedata.Isactive = true;
                jrj.Ratedata.Description = txtRateDescr2a.Text;
                jrj.Ratedata.Flatfeeorhourly = lstFlatfeeorhourly2a.SelectedValue;
                jrj.Ratedata.Cost = txtCost2a.Value ?? 0.00;
                jrj.Ratedata.Rate = txtRate2a.Value ?? 0.00;
                jrj.Ratedata.Stdhours = txtStdhours2a.Value ?? 0.00;
                jrj.Ratedata.Stdquantity = txtStdquantity2a.Value ?? 0.00;
                jrj.Ratedata.Upddt = DateTime.Now;
                jrj.Ratedata.Upduserid = Profile.UserName;

                // Change history row
                jrj.Ratedata.Changehistory = jrj.Ratedata.Changehistory + "\n" +
                                  "UPDATE " + DateTime.Now.ToString() +
                                  " Descr: " + jrj.Ratedata.Description + " Active: " + jrj.Ratedata.Isactive + " Cost: " + jrj.Ratedata.Cost + " Rate: " + jrj.Ratedata.Rate +
                                  " Std Hours: " + jrj.Ratedata.Stdhours + " Std Qty: " + jrj.Ratedata.Stdquantity + " Upd Dt: " + jrj.Ratedata.Upddt + " Upd User: " + jrj.Ratedata.Upduserid;

                db.SubmitChanges();
            }
        }

        // Change/disable buttons.
        rdoOption1.Enabled = false;
        rdoOption2.Enabled = false;
        rdoOption3.Enabled = false;
        rdoOption4.Enabled = false;
        btnSaveChanges.Visible = false;
        btnClose.Text = "Close";
        lblStatus.Visible = true;
        txtRateDescr2a.Enabled = false;
        lstFlatfeeorhourly2a.Enabled = false;
        txtCost2a.Enabled = false;
        txtRate2a.Enabled = false;
        txtStdhours2a.Enabled = false;
        txtStdquantity2a.Enabled = false;

        // Update session so previous page refreshes.
        Session.Add(ATGDB.Ratedata.SESSION_RATESCHEDULE_REFRESH, true);
    }
}