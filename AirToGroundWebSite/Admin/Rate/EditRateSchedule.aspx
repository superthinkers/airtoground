﻿<%@ Page Title="Rate Schedule Manager" Language="C#" MasterPageFile="~/Default.master"
    AutoEventWireup="true" CodeFile="EditRateSchedule.aspx.cs"
    Inherits="Admin_Rate_EditRateSchedule" Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>                 
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="btnOption1" />
                    <telerik:AjaxUpdatedControl ControlID="btnOption2" />
                    <telerik:AjaxUpdatedControl ControlID="btnOption3" />
                    <telerik:AjaxUpdatedControl ControlID="btnOption4" />
                    <telerik:AjaxUpdatedControl ControlID="btnOption5" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnOption1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnOption1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnOption2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnOption2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnOption3">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnOption3" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnOption4">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnOption4" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnOption5">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnOption5" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important" ShowContentDuringLoad="false" VisibleStatusbar="false"
        runat="server" EnableShadow="true" Modal="true" VisibleTitlebar="false" AutoSize="true"> 
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server"
                Behaviors="Close" OnClientClose="onClientClose1" Title="Suspend Aircraft">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadCodeBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            // This fixes problems when exporting.
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                   args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                   args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                   args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                   args.get_eventTarget().indexOf("btnOption1") >= 0 ||
                   args.get_eventTarget().indexOf("btnOption2") >= 0 ||
                   args.get_eventTarget().indexOf("btnOption3") >= 0 ||
                   args.get_eventTarget().indexOf("btnOption4") >= 0 ||
                   args.get_eventTarget().indexOf("btnOption5") >= 0) {
                    // Disable ajax
                    args.set_enableAjax(false);
                }
            }
            // OPTIONS
            function openOption(id, option, keys) {
                //var grid = $find("< %= lstCompCust.ClientID %>");
                //if (grid) {
                    radopen("ChangeRate.aspx?Jobratejoinid=" + id + "&Option=" + option + "&Keys=" + keys, "RadWindow1");
                //}
            }
            // General close method. Causes postback refresh on server...reloads rate schedule.
            function onClientClose1(oWnd, args) {
                // Refresh via postback.
                <%= PostBackString %>
            }
        </script>
    </telerik:RadCodeBlock>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Rate Schedule Management"></asp:Label>
        <br />
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Rate Schedule</div>
            <%--<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true" Width="100%">--%>
                <div style="margin: 10px 10px 10px 10px">
                    <br />
                    <table width="100%">
                        <tr>
                            <td>
                                <!-- LEGEND -->
                                <table border="1" width="300px">
                                    <tr>
                                        <td align="center" valign="middle" style="width: 20px">
                                            <img alt="" src="../../App_Themes/Corp/Images/JobRate_Corp.bmp" width="20px" height="20px" />
                                        </td>
                                        <td align="left">
                                            Corporate Rates Only
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="middle" style="width: 20px">
                                            <img alt="" src="../../App_Themes/Corp/Images/JobRate_Comp.bmp" width="20px" height="20px" />
                                        </td>
                                        <td align="left">
                                            Company Rates Only
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="middle" style="width: 20px">
                                            <img alt="" src="../../App_Themes/Corp/Images/JobRate_Cust.bmp" width="20px" height="20px" />
                                        </td>
                                        <td align="left">
                                            Customer Rates Only
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="middle" style="width: 20px">
                                            <img alt="" src="../../App_Themes/Corp/Images/JobRate_CompCust.bmp" width="20px" height="20px" />
                                        </td>
                                        <td align="left">
                                            Company/Customer Rates Only
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div class="lblInfoBold">Complete Rate Schedule for Selected Company Division / Customer:</div>
                    <!-- RATES -->
                    <telerik:RadGrid ID="RadGrid1" GridLines="None"
                        Width="100%" Height="100%" runat="server" AllowAutomaticDeletes="false" AllowAutomaticInserts="false"
                        AllowMultiRowEdit="false" AllowMultiRowSelection="false" AllowAutomaticUpdates="true"
                        AllowPaging="false" AllowSorting="true" AutoGenerateColumns="false" 
                        OnSelectedIndexChanged="RadGrid1_SelectedIndexChanged">
                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="Jobratejoinid"
                            CommandItemSettings-ShowRefreshButton="false" CommandItemSettings-ShowAddNewRecordButton="false"
                            HorizontalAlign="NotSet" AutoGenerateColumns="false" EditMode="InPlace">
                            <Columns>
                                <telerik:GridBoundColumn DataField="Jobratejoinid" HeaderText="Id" AllowSorting="false"
                                    UniqueName="JobratejoinidColumn" ReadOnly="true" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="" UniqueName="StatusColumn">
                                    <ItemTemplate>
                                        <img alt="" src='<%# Eval("Imagesrc") %>' width="20px" height="20px" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="20px" HorizontalAlign="Center" />
                                    <ItemStyle Width="20px" HorizontalAlign="Center" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="Jobcodedescr" HeaderText="Job Code" AllowSorting="true"
                                    UniqueName="JobcodedescrColumn" ReadOnly="true" Visible="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Ratedescr" HeaderText="Rate Description" AllowSorting="true"
                                    UniqueName="RatedescrColumn" ReadOnly="true" Visible="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Flatfeeorhourly" HeaderText="FF/Hr" AllowSorting="true"
                                    UniqueName="FFHRColumn" ReadOnly="true" Visible="true">
                                    <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                    <ItemStyle Width="40px" HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Cost" HeaderText="Cost" AllowSorting="true" UniqueName="CostColumn"
                                    ReadOnly="true" Visible="true" DataFormatString="{0:c}">
                                    <HeaderStyle Width="60px" HorizontalAlign="Center" />
                                    <ItemStyle Width="60px" HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Rate" HeaderText="Rate" AllowSorting="true" UniqueName="RateColumn"
                                    ReadOnly="true" Visible="true" DataFormatString="{0:c}">
                                    <HeaderStyle Width="60px" HorizontalAlign="Center" />
                                    <ItemStyle Width="60px" HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Stdhours" HeaderText="Std Hours" AllowSorting="true"
                                    UniqueName="StdhoursColumn" ReadOnly="true" Visible="true" DataFormatString="{0:f}">
                                    <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                    <ItemStyle Width="70px" HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Stdquantity" HeaderText="Std Qty" AllowSorting="true"
                                    UniqueName="StdqtyColumn" ReadOnly="true" Visible="true" DataFormatString="{0:f}">
                                    <HeaderStyle Width="60px" />
                                    <ItemStyle Width="60px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Upddt" HeaderText="Upd Dt" AllowSorting="true"
                                    UniqueName="UpddtColumn" ReadOnly="true" Visible="true" DataFormatString="{0:d}">
                                    <HeaderStyle Width="80px" />
                                    <ItemStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Upduserid" HeaderText="Upd Userid" AllowSorting="true"
                                    UniqueName="UpduseridColumn" ReadOnly="true" Visible="true">
                                    <HeaderStyle Width="80px" />
                                    <ItemStyle Width="80px" />
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemSettings ShowExportToCsvButton="true" />
                            <AlternatingItemStyle BackColor="#F2F0F2" />
                        </MasterTableView>
                        <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false" Csv-FileExtension="csv" />
                        <ClientSettings EnablePostBackOnRowClick="true">
                            <Selecting AllowRowSelect="true" />
                            <Scrolling AllowScroll="false" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <br />
                    <!-- ACTIONS -->
                    <fieldset>
                        <legend>Actions You Can Take:</legend>
                        <div style="margin: 10px 10px 10px 10px">
                            <asp:LinkButton ID="btnOption1" runat="server" OnClientClick="openOption(); return false;"
                                Text="1)"></asp:LinkButton>
                            <br />
                            <asp:LinkButton ID="btnOption2" runat="server" OnClientClick="openOption(); return false;"
                                Text="2)"></asp:LinkButton>
                            <br />
                            <asp:LinkButton ID="btnOption3" runat="server" OnClientClick="openOption(); return false;"
                                Text="3)"></asp:LinkButton>
                            <br />
                            <asp:LinkButton ID="btnOption4" runat="server" OnClientClick="openOption(); return false;"
                                Text="4)"></asp:LinkButton>
                            <br />
                            <asp:LinkButton ID="btnOption5" runat="server" OnClientClick="openOption(); return false;"
                                Text="5)"></asp:LinkButton>
                        </div>
                    </fieldset>
                </div>
            <%--</telerik:RadAjaxPanel>--%>
        </div>
    </div>
</asp:Content>

