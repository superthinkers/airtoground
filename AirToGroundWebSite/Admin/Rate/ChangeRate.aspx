﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangeRate.aspx.cs" Inherits="Admin_Rate_ChangeRate"
    Buffer="true" Strict="true" Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rate Change Dialog</title>
    <style type="text/css">
        .style1
        {
            width: 15%;
            height: 26px;
        }
        .style2
        {
            height: 26px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function doCancel() {
                var oWnd = GetRadWindow();
                var oArg = new Object();
                oWnd.close(oArg);
            }
        </script>
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server" AsyncPostBackTimeout="600"
            SupportsPartialRendering="true" EnablePartialRendering="true"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin" ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator2" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <asp:HiddenField ID="hiddenJobCodeId" runat="server" />
        <asp:HiddenField ID="hiddenRateId" runat="server" />
        <div id="mainDiv" runat="server" style="text-align: center; margin: 10px 10px 10px 10px; width: 700px; height: 550px">
            <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Rate Change Dialog"></asp:Label>
            <br />
            <br />
            <fieldset style="margin-right: 10px;">
                <legend>What kind of Rate Change do you Want?</legend>
                <div style="text-align: left; position: relative; left:5%; width: 90%">
                    <asp:RadioButton ID="rdoOption1" runat="server" GroupName="OPTIONS" Text="1) I want these rates to be specific
                    to Louisville, and all Customers, for this Job Code" AutoPostBack="true" OnCheckedChanged="rdoOption1_CheckedChanged" />
                    <br />
                    <asp:RadioButton ID="rdoOption2" runat="server" GroupName="OPTIONS" Text="2) I want these rates to be specific
                    to FedEx, regardless of the Company Division, for this Job Code" AutoPostBack="true"
                        OnCheckedChanged="rdoOption2_CheckedChanged" />
                    <br />
                    <asp:RadioButton ID="rdoOption3" runat="server" GroupName="OPTIONS" Text="3) I want these rates to be specific
                    to Louisville and FedEx, for this Job Code" AutoPostBack="true" OnCheckedChanged="rdoOption3_CheckedChanged" />
                    <br />
                    <asp:RadioButton ID="rdoOption4" runat="server" GroupName="OPTIONS" Text="4) I want these rates to be specific
                    to the Standard Corporate rates, for this Job Code" AutoPostBack="true" OnCheckedChanged="rdoOption4_CheckedChanged" />
                    <br />
                </div>
            </fieldset>
            <fieldset style="margin-right: 10px;">
                <legend>Here are the associated Rate Values:</legend>
                <div style="position: relative; float: left; margin: 10px 0px 0px 0px; width: 100%">
                    <table style="border: 0.0px Solid Navy; border-collapse: separate; width: 100%">
                        <tr>
                            <td colspan="2" style="background-color: Navy; text-align: center; width: 50%">
                                <asp:Label ID="lblHeaderCurrent" runat="server" ForeColor="White" SkinID="SmallLabelBold" Text="Current Rate"></asp:Label>
                            </td>
                            <td colspan="2" style="background-color: Green; text-align: center; width: 50%">
                                <asp:Label ID="lblHeaderNew" runat="server" ForeColor="White" SkinID="SmallLabelBold" Text="New Rate"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblJobCode1" runat="server" SkinID="SmallLabel" Text="Job Code:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblJobCode1a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblJobCode2" runat="server" SkinID="SmallLabel" Text="Job Code:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblJobCode2a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblCompany1" runat="server" SkinID="SmallLabel" Text="Company:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblCompany1a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblCompany2" runat="server" SkinID="SmallLabel" Text="Company:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblCompany2a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; " class="style1">
                                <asp:Label ID="lblCustomer1" runat="server" SkinID="SmallLabel" Text="Customer:"></asp:Label>
                            </td>
                            <td style="text-align: left" class="style2">
                                <asp:Label ID="lblCustomer1a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                            <td style="text-align: right; " class="style1">
                                <asp:Label ID="lblCustomer2" runat="server" SkinID="SmallLabel" Text="Customer:"></asp:Label>
                            </td>
                            <td style="text-align: left" class="style2">
                                <asp:Label ID="lblCustomer2a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblRateDescr1" runat="server" SkinID="SmallLabel" Text="Rate Description:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblRateDescr1a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblRateDescr2" runat="server" SkinID="SmallLabel" Text="Rate Description:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <telerik:RadTextBox ID="txtRateDescr2a" runat="server" MaxLength="50" Width="88%" ValidationGroup="RATE">
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ID="reqRateDescr" runat="server" CssClass="stdValidator"
                                    ControlToValidate="txtRateDescr2a" ErrorMessage="*" SetFocusOnError="true" Display="Dynamic"
                                    ValidationGroup="RATE">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblFlatfeeorhourly1" runat="server" SkinID="SmallLabel" Text="Flat Fee / Hourly:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblFlatfeeorhourly1a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblFlatfeeorhourly2" runat="server" SkinID="SmallLabel" Text="Flat Fee / Hourly:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <telerik:RadComboBox ID="lstFlatfeeorhourly2a" runat="server" Width="100px">
                                    <Items>
                                        <telerik:RadComboBoxItem Value="F" Text="Flat Fee" />
                                        <telerik:RadComboBoxItem Value="H" Text="Hourly" />
                                    </Items>
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblCost1" runat="server" SkinID="SmallLabel" Text="Cost:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblCost1a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblCost2" runat="server" SkinID="SmallLabel" Text="Cost:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <telerik:RadNumericTextBox ID="txtCost2a" runat="server" MinValue="0.00" SelectionOnFocus="SelectAll"
                                    Width="100px" ValidationGroup="RATE">
                                    <NumberFormat DecimalDigits="2" />
                                </telerik:RadNumericTextBox>
                                <asp:RequiredFieldValidator ID="reqCost" runat="server" CssClass="stdValidator" ControlToValidate="txtCost2a"
                                    ErrorMessage="*" SetFocusOnError="true" Display="Dynamic" ValidationGroup="RATE">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblRate1" runat="server" SkinID="SmallLabel" Text="Rate:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblRate1a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblRate2" runat="server" SkinID="SmallLabel" Text="Rate:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <telerik:RadNumericTextBox ID="txtRate2a" runat="server" MinValue="0.00"
                                    SelectionOnFocus="SelectAll" Width="100px" ValidationGroup="RATE">
                                    <NumberFormat DecimalDigits="2" />
                                </telerik:RadNumericTextBox>
                                <asp:RequiredFieldValidator ID="reqRate" runat="server" CssClass="stdValidator" ControlToValidate="txtRate2a"
                                    ErrorMessage="*" SetFocusOnError="true" Display="Dynamic" ValidationGroup="RATE">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblStdhours1" runat="server" SkinID="SmallLabel" Text="Standard Hours:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblStdhours1a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblStdhours2" runat="server" SkinID="SmallLabel" Text="Standard Hours:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <telerik:RadNumericTextBox ID="txtStdhours2a" runat="server" MinValue="0.00"
                                    SelectionOnFocus="SelectAll" Width="100px" ValidationGroup="RATE">
                                    <NumberFormat DecimalDigits="2" />
                                </telerik:RadNumericTextBox>
                                <asp:RequiredFieldValidator ID="reqStdhours" runat="server" CssClass="stdValidator"
                                    ControlToValidate="txtStdhours2a" ErrorMessage="*" SetFocusOnError="true" Display="Dynamic"
                                    ValidationGroup="RATE">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblStdquantity1" runat="server" SkinID="SmallLabel" Text="Standard Quantity:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblStdquantity1a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblStdquantity2" runat="server" SkinID="SmallLabel" Text="Standard Quantity:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <telerik:RadNumericTextBox ID="txtStdquantity2a" runat="server" MinValue="0.00"
                                    SelectionOnFocus="SelectAll" Width="100px" ValidationGroup="RATE">
                                    <NumberFormat DecimalDigits="2" />
                                </telerik:RadNumericTextBox>
                                <asp:RequiredFieldValidator ID="reqStdquantity" runat="server" CssClass="stdValidator"
                                    ControlToValidate="txtStdquantity2a" ErrorMessage="*" SetFocusOnError="true"
                                    Display="Dynamic" ValidationGroup="RATE">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblUpdDt1" runat="server" SkinID="SmallLabel" Text="UpdDt:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblUpdDt1a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblUpdDt2" runat="server" SkinID="SmallLabel" Text="UpdDt:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblUpdDt2a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblUpdUserId1" runat="server" SkinID="SmallLabel" Text="UpdUserId:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblUpdUserId1a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                            <td style="text-align: right; width: 15%">
                                <asp:Label ID="lblUpdUserId2" runat="server" SkinID="SmallLabel" Text="UpdUserId:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblUpdUserId2a" runat="server" SkinID="SmallLabelBold"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
            <div>
                <br />
                <div style="text-align: center; width:100%">
                    <asp:LinkButton ID="btnSaveChanges" runat="server" Text="Accept These Rate Changes"
                        OnClick="btnSaveChanges_OnClick" ValidationGroup="RATE">
                    </asp:LinkButton>
                    <br />
                    <asp:Label ID="lblStatus" runat="server" ForeColor="Green" SkinID="SmallLabel" Visible="false"
                        Text="Changes Successfully Saved!"></asp:Label>
                    <br />
                    <asp:LinkButton ID="btnClose" runat="server" Text="Cancel" OnClientClick="doCancel(); return false;">
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
