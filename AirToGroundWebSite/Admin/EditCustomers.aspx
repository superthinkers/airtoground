﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="EditCustomers.aspx.cs" Inherits="Admin_EditCustomers" Buffer="true"
    Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <devart:DbLinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Customer" OrderBy="Description" TableName="Customers"
        EnableInsert="true" EnableUpdate="true" EnableDelete="true">                               
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Service" OrderBy="Customer.Description, Sequence, Description" EnableInsert="true"
        EnableUpdate="true" EnableDelete="true" 
        TableName="Services" Where="Customerid == Convert.ToInt32(@Customerid)">
        <WhereParameters>
            <asp:ControlParameter ControlID="RadGrid1" Name="Customerid" PropertyName="SelectedValue" Type="Int32" />
        </WhereParameters>
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource3" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Servicesectionjoin" OrderBy="Section.Description"
        EnableInsert="true" EnableUpdate="true" EnableDelete="true" 
        TableName="Servicesectionjoins" Where="Serviceid = Convert.ToInt32(@Serviceid)">
        <WhereParameters>
            <asp:ControlParameter ControlID="RadGrid2" Name="Serviceid" PropertyName="SelectedValue" Type="Int32" />
        </WhereParameters>
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource4" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Section" OrderBy="Description" TableName="Sections"
        Where="Customerid = Convert.ToInt32(@Customerid)">
        <WhereParameters>
            <asp:ControlParameter ControlID="RadGrid1" Name="Customerid" PropertyName="SelectedValue" Type="Int32" />
        </WhereParameters>
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource5" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Servicecategory" OrderBy="Description" TableName="Servicecategories">
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource9" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Serviceshortdescr" OrderBy="Description" TableName="Serviceshortdescrs">
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource33" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Serviceequipmenttypejobcodejoin" OrderBy="Equipmenttype.Description"
        EnableInsert="true" EnableUpdate="true" EnableDelete="true" TableName="Serviceequipmenttypejobcodejoins"
        Where="Serviceid = Convert.ToInt32(@Serviceid)">
        <WhereParameters>
            <asp:ControlParameter ControlID="RadGrid2" Name="Serviceid" PropertyName="SelectedValue" Type="Int32" />
        </WhereParameters>
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource44" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Equipmenttype" OrderBy="Description" TableName="Equipmenttypes"
        Where="Customerid = Convert.ToInt32(@Customerid)">
        <WhereParameters>
            <asp:ControlParameter ControlID="RadGrid1" Name="Customerid" PropertyName="SelectedValue" Type="Int32" />
        </WhereParameters>
    </devart:DbLinqDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetThemeData"
        TypeName="ATGDB.Companydivision"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetIntervalData"
        TypeName="ATGDB.Service"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="GetRateData"
        TypeName="ATGDB.Service"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" SelectMethod="GetIntervalData"
        TypeName="ATGDB.Service"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource8" runat="server" SelectMethod="GetUniqueJobsByCustomer"
        TypeName="ATGDB.Jobcode">
        <SelectParameters>
            <asp:Parameter Name="CustomerId" Type="String" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <%--<asp:ObjectDataSource ID="ObjectDataSource5" runat="server" SelectMethod="GetShortDescriptionData"
        TypeName="ATGDB.Service"></asp:ObjectDataSource>--%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid2" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid3" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid2" />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid3" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid3">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid3" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToPdfButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }
            function openExDescr(svcId) {
                radopen("EditServiceExDescr.aspx?ServiceId=" + svcId, "RadWindow1");
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="true" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" Width="250" Height="75">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" NavigateUrl="EditServiceExDescr.aspx"
                ShowContentDuringLoad="false" VisibleStatusbar="false"
                VisibleTitlebar="false" EnableShadow="false" ReloadOnShow="true" Title="Edit Service Extended Description"
                Width="850" Height="450" Modal="true" AutoSize="false">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Customers"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Customers</div>
            <!-- Customers -->
            <div>
                <telerik:RadGrid ID="RadGrid1" GridLines="None"
                    Height="100%" runat="server" AllowAutomaticDeletes="True" AllowAutomaticInserts="True"
                    AllowAutomaticUpdates="True" AllowPaging="False" AllowSorting="True" AutoGenerateColumns="False"
                    DataSourceID="LinqDataSource1" OnItemUpdated="RadGrid1_ItemUpdated" OnItemDeleted="RadGrid1_ItemDeleted"
                    OnItemInserted="RadGrid1_ItemInserted" OnDataBound="RadGrid1_DataBound" 
                    OnSelectedIndexChanged="RadGrid1_SelectedIndexChanged" Width="66%">
                    <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="Customerid"
                        DataSourceID="LinqDataSource1" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                        EditMode="InPlace" CommandItemSettings-ShowRefreshButton="false">
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                <HeaderStyle Width="60px" />
                                <ItemStyle Width="60px" />
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn DataField="Customerid" HeaderText="Id" AllowSorting="false"
                                UniqueName="CustomeridColumn" ReadOnly="true" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Description" SortExpression="Description"
                                UniqueName="DescriptionColumn">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblDescription" Text='<%# Eval("Description") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDescription" runat="server" Width="98%" MaxLength="30" Text='<%# Bind("Description") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtDescription"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Non-Routine Rate" UniqueName="NonRoutineRateColumn">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblNonRoutineRate" Text='<%# Eval("Nonroutinehourlyrate", "{0:C}") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <telerik:RadNumericTextBox ID="txtNonRoutineHourlyRate" runat="server" 
                                        SelectionOnFocus="SelectAll"
                                        Text='<%# Bind("Nonroutinehourlyrate") %>'
                                        Width="98%" AllowOutOfRangeAutoCorrect="true" MaxValue="1000" MinValue="0.00" >                                            
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtNonRoutineHourlyRate"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Site Theme" DataField="Sitetheme"
                                SortExpression="Sitetheme" UniqueName="SitethemeColumn">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblTheme" Text='<%# Eval("Sitetheme") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <telerik:RadComboBox ID="lstTheme" runat="server" 
                                        Width="98%" 
                                        DataSourceID="ObjectDataSource1"
                                        DataTextField="Description"                                   
                                        DataValueField="Sitetheme"
                                        SelectedValue='<%# Bind("Sitetheme") %>'
                                        OnDataBound="lstTheme_DataBound">
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="lstTheme"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <HeaderStyle Width="80px" />
                                <ItemStyle Width="80px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record?"
                                ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                                UniqueName="DeleteCommandColumn" CommandName="Delete">
                                <HeaderStyle Width="40px" />
                                <ItemStyle Width="40px" />
                            </telerik:GridButtonColumn>
                        </Columns>
                        <CommandItemSettings ShowExportToCsvButton="true" />
                        <AlternatingItemStyle BackColor="#F2F0F2" />
                    </MasterTableView>
                    <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false"
                        Csv-FileExtension="csv" />
                    <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                        <ClientEvents OnRowDblClick="RowDblClick" />
                        <Selecting AllowRowSelect="true" />
                        <Scrolling AllowScroll="False" />
                    </ClientSettings>
                </telerik:RadGrid>
                <br />
                <br />
            </div><!-- Customers -->
            <!-- Services -->
            <div>
                <div class="lblInfoBold">Selected Customer's Services</div>
                <telerik:RadGrid ID="RadGrid2" GridLines="None"
                    AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                    Height="100%" runat="server" AllowPaging="False" AllowSorting="True" AutoGenerateColumns="False"
                    DataSourceID="LinqDataSource2" OnItemUpdated="RadGrid2_ItemUpdated" OnItemDeleted="RadGrid2_ItemDeleted"
                    OnItemInserted="RadGrid2_ItemInserted" OnDataBound="RadGrid2_DataBound" OnSelectedIndexChanged="RadGrid2_SelectedIndexChanged">
                    <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="Serviceid" EditMode="InPlace"
                        DataSourceID="LinqDataSource2" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                        CommandItemSettings-ShowRefreshButton="false">
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn2">
                                <HeaderStyle Width="60px" />
                                <ItemStyle Width="60px" />
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn DataField="Serviceid" HeaderText="Id" AllowSorting="false"
                                UniqueName="ServiceidColumn" ReadOnly="true" AllowFiltering="false" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Customerid" HeaderText="Cust" AllowSorting="false"
                                UniqueName="CustomeridColumn" ReadOnly="false" AllowFiltering="false" Visible="false">
                                <ItemStyle Width="30px" HorizontalAlign="Center"/>
                                <HeaderStyle Width="30px" HorizontalAlign="Center" />
                            </telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn DataSourceID="LinqDataSource5" DataField="Servicecategoryid"
                                ListValueField="Servicecategoryid" ListTextField="Description" AllowFiltering="false"
                                AllowSorting="true" SortExpression="Servicecategory.Description" HeaderText="Category">
                                <HeaderStyle Width="170px" />
                                <ItemStyle Width="170px" />
                            </telerik:GridDropDownColumn>
                            <telerik:GridTemplateColumn HeaderText="Seq" DataField="Sequence" SortExpression="Sequence"
                                UniqueName="SeqColumn" AllowFiltering="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblSeq" runat="server" Text='<%# Eval("Sequence") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <telerik:RadNumericTextBox ID="txtSeq" runat="server" Width="98%" MinValue="0" ShowSpinButtons="true"
                                        SelectionOnFocus="SelectAll" Text='<%# Bind("Sequence") %>' Value="0">
                                        <IncrementSettings InterceptArrowKeys="true" Step="1" />
                                        <NumberFormat DecimalDigits="0" />
                                    </telerik:RadNumericTextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ControlToValidate="txtSeq"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                                <HeaderStyle Width="50px" HorizontalAlign="Center" />
                            </telerik:GridTemplateColumn>
                            <%--<telerik:GridCheckBoxColumn DataField="Recurring" HeaderText="Recurring?" AllowSorting="true"
                                UniqueName="RecurringColumn" AllowFiltering="false">
                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                                <HeaderStyle HorizontalAlign="Center" Width="80px" />
                            </telerik:GridCheckBoxColumn>--%>
                            <filters:TextBoxFilterGridTemplateColumn HeaderText="Description" DataField="Description"
                                UniqueName="DescriptionColumn" SortExpression="Description" FilterControlWidth="98%">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblDescription" Text='<%# Eval("Description") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDescription" runat="server" Width="98%" MaxLength="100" Text='<%# Bind("Description") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtDescription"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                            </filters:TextBoxFilterGridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Short Descr" DataField="Serviceshortdescrid"
                                UniqueName="ShortdescriptionColumn" SortExpression="Serviceshortdescr.Description"
                                AllowFiltering="false">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblShortDescription" Text='<%# Eval("Serviceshortdescr.Description") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <telerik:RadComboBox ID="lstShortDescription" runat="server" DataSourceID="LinqDataSource9"
                                        DataValueField="Serviceshortdescrid" DataTextField="Description" ZIndex="9000"
                                        DropDownWidth="140px" NoWrap="true" MaxHeight="300px" SelectedValue='<%# Bind("Serviceshortdescrid") %>'
                                        Width="98%">
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="lstShortDescription"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                <ItemStyle Width="70px" HorizontalAlign="Center" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Extended" UniqueName="ExDescrColumn" AllowFiltering="false">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <a href="#" onclick='<%# "openExDescr(" + Eval("Serviceid").ToString() + "); return false;" %>'>
                                        Edit/View</a>
                                </ItemTemplate>
                                <HeaderStyle Width="60px" />
                                <ItemStyle Width="60px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Interval" DataField="Serviceinterval" SortExpression="Serviceinterval"
                                UniqueName="ServiceintervalColumn" AllowFiltering="false">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblServiceinterval" Text='<%# Eval("Serviceinterval") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <telerik:RadComboBox ID="lstServiceinterval" runat="server" Width="98%" DataSourceID="ObjectDataSource2"
                                        DataTextField="Description" DataValueField="Interval" SelectedValue='<%# Bind("Serviceinterval") %>'>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="lstServiceinterval"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <ItemStyle Width="60px" HorizontalAlign="Center" />
                                <HeaderStyle Width="60px" HorizontalAlign="Center" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Report Interval" DataField="Reportinterval"
                                SortExpression="Reportinterval" UniqueName="ReportintervalColumn" AllowFiltering="false">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblReportinterval" Text='<%# Eval("Reportinterval") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <telerik:RadComboBox ID="lstReportinterval" runat="server" Width="98%" DataSourceID="ObjectDataSource4"
                                        DataTextField="Description" DataValueField="Interval" SelectedValue='<%# Bind("Reportinterval") %>'>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ControlToValidate="lstServiceinterval"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <ItemStyle Width="75px" HorizontalAlign="Center" />
                                <HeaderStyle Width="75px" HorizontalAlign="Center" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridCheckBoxColumn HeaderText="Show In SQ?" DataField="Showinservicequeue"
                                SortExpression="Showinservicequeue" AllowFiltering="false">
                                <ItemStyle Width="75px" HorizontalAlign="Center" />
                                <HeaderStyle Width="75px" HorizontalAlign="Center" />
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridCheckBoxColumn HeaderText="Show In MS?" DataField="Showinmasterschedule"
                                SortExpression="Showinmasterschedule" AllowFiltering="false">
                                <ItemStyle Width="75px" HorizontalAlign="Center" />
                                <HeaderStyle Width="75px" HorizontalAlign="Center" />
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridCheckBoxColumn HeaderText="Require Div Area?" DataField="Requiresdivisionarea"
                                SortExpression="Requiresdivisionarea" AllowFiltering="false">
                                <ItemStyle Width="75px" HorizontalAlign="Center" />
                                <HeaderStyle Width="75px" HorizontalAlign="Center" />
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record?"
                                ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                                UniqueName="DeleteCommandColumn" CommandName="Delete">
                                <HeaderStyle Width="40px" />
                                <ItemStyle Width="40px" />
                            </telerik:GridButtonColumn>
                        </Columns>
                        <DetailTables>
                            <telerik:GridTableView DataKeyNames="Serviceid,Equipmenttypeid" BorderColor="Black" BackColor="Silver"
                                DataSourceID="LinqDataSource33" runat="server" CommandItemDisplay="Top" EditMode="InPlace"
                                Name="ServiceEquipmentTypes" AllowSorting="true" AllowFilteringByColumn="false"
                                AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true"
                                AllowPaging="false" Width="75%">
                                <ParentTableRelation>
                                    <telerik:GridRelationFields DetailKeyField="Serviceid" MasterKeyField="Serviceid" />
                                </ParentTableRelation>
                                <Columns>
                                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                        <HeaderStyle Width="60px" />
                                        <ItemStyle Width="60px" />
                                    </telerik:GridEditCommandColumn>
                                    <%--<telerik:GridBoundColumn DataField="Serviceid" Visible="false" UniqueName="ServiceidColumn2">                                        
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Aircraft Type" DataField="Equipmenttype.Description"
                                        UniqueName="EquipmenttypeidColumn" SortExpression="Equipmenttype.Description">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEquipmentType" Text='<%# Eval("Equipmenttype.Description") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <telerik:RadComboBox ID="lstEquipmentType" runat="server" Width="98%" DataSourceID="LinqDataSource44"
                                                DataTextField="Description" DataValueField="Equipmenttypeid" SelectedValue='<%# Bind("Equipmenttypeid") %>'>
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator55" ControlToValidate="lstEquipmentType"
                                                ErrorMessage="<br />Required" runat="server" CssClass="stdValidator" Display="Dynamic">
                                            </asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <HeaderStyle Width="47%" />
                                        <ItemStyle Width="47%" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Job Code" DataField="Jobcode.Code"
                                        UniqueName="JobcodeidColumn" SortExpression="Jobcode.Code">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblJobCode" Text='<%# Eval("Jobcode.Code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <telerik:RadComboBox ID="lstJobCode" runat="server" Width="98%" DataSourceID="ObjectDataSource8"
                                                DataTextField="Code" DataValueField="Jobcodeid" SelectedValue='<%# Bind("Jobcodeid") %>'>
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator66" ControlToValidate="lstJobCode"
                                                ErrorMessage="<br />Required" runat="server" CssClass="stdValidator" Display="Dynamic">
                                            </asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record?"
                                        ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                                        UniqueName="DeleteCommandColumn" CommandName="Delete">
                                        <ItemStyle Width="40px" />
                                    </telerik:GridButtonColumn>
                                </Columns>
                            </telerik:GridTableView>
                        </DetailTables>
                        <CommandItemSettings ShowExportToCsvButton="true" />
                        <AlternatingItemStyle BackColor="#F2F0F2" />
                    </MasterTableView>
                    <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false"
                        Csv-FileExtension="csv" />
                    <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                        <ClientEvents OnRowDblClick="RowDblClick" />
                        <Selecting AllowRowSelect="true" />
                        <Scrolling AllowScroll="False" />
                    </ClientSettings>
                </telerik:RadGrid>
                <br />
                <br />
            </div><!-- Services -->
            <!-- Sections -->
            <div>
                <div class="lblInfoBold">Selected Service's Sections</div>
                <telerik:RadGrid ID="RadGrid3" GridLines="None"
                    Height="100%" runat="server" AllowPaging="False" AllowSorting="True" AutoGenerateColumns="False"
                    AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                    DataSourceID="LinqDataSource3" OnItemUpdated="RadGrid3_ItemUpdated" OnItemDeleted="RadGrid3_ItemDeleted"
                    OnItemInserted="RadGrid3_ItemInserted" OnDataBound="RadGrid3_DataBound" Width="66%">
                    <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="Serviceid, Sectionid"
                        CommandItemSettings-ShowRefreshButton="false" DataSourceID="LinqDataSource3"
                        HorizontalAlign="NotSet" AutoGenerateColumns="False" EditMode="InPlace">
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn3">
                                <HeaderStyle Width="60px" />
                                <ItemStyle Width="60px" />
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn DataField="Serviceid" HeaderText="Service Id" UniqueName="ServiceidColumn"
                                AllowFiltering="false" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Section" DataField="Section.Sectionid"
                                UniqueName="SectionidColumn" AllowFiltering="false" SortExpression="Section.Description">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblSection" Text='<%# Eval("Section.Description") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <telerik:RadComboBox ID="lstSection" runat="server" Width="98%" DataSourceID="LinqDataSource4"
                                        DataTextField="Description" DataValueField="Sectionid" SelectedValue='<%# Bind("Sectionid") %>'>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="lstSection"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record?"
                                ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                                UniqueName="DeleteCommandColumn" CommandName="Delete">
                                <HeaderStyle Width="40px" />
                                <ItemStyle Width="40px" />
                            </telerik:GridButtonColumn>
                        </Columns>
                        <CommandItemSettings ShowExportToCsvButton="true" />
                        <AlternatingItemStyle BackColor="#F2F0F2" />
                    </MasterTableView>
                    <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false"
                        Csv-FileExtension="csv" />
                    <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                        <ClientEvents OnRowDblClick="RowDblClick" />
                        <Selecting AllowRowSelect="true" />
                        <Scrolling AllowScroll="False" />
                    </ClientSettings>
                </telerik:RadGrid>
                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>

