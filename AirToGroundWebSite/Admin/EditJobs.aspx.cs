﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using Telerik.Web.UI;

public partial class Admin_EditJobs : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_JOBS;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            // Security.
            if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_JOBCODES)) {
                grdJobs.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                grdJobs.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                grdJobs.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                grdJobs.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
            }
        }
    }
    protected void rowCheckBox1_OnDataBinding(object sender, EventArgs e) {
        CheckBox chk = (CheckBox)sender;
        chk.CheckedChanged -= ToggleRowSelection1;
        chk.Checked = Convert.ToBoolean(DataBinder.Eval(((sender as CheckBox).NamingContainer as GridItem).DataItem, "IsChecked"));
        chk.CheckedChanged += ToggleRowSelection1;
    }
    protected void ToggleRowSelection1(object sender, EventArgs e) {
        ((sender as CheckBox).NamingContainer as GridItem).Selected = true;
        bool isChecked = (sender as CheckBox).Checked;
        int companyDivisionId = int.Parse(grdCompanyCusts.SelectedValues["Companydivisionid"].ToString());
        int custId = int.Parse(grdCompanyCusts.SelectedValues["Customerid"].ToString());
        int jobCodeId = int.Parse(grdSelectedJobs.SelectedValues["Jobcodeid"].ToString());

        // Can it be deleted?
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        var data1 = from wl in dc.Worklogs
                    where wl.Companydivisionid == companyDivisionId
                    where wl.Customerid == custId
                    where wl.Jobcodeid == jobCodeId
                    select wl;

        var data2 = from ts in dc.Timesheets
                    where ts.Companydivisionid == companyDivisionId
                    where ts.Customerid == custId
                    where ts.Jobcodeid == jobCodeId
                    select ts;
                 
        if ((data1.Count() > 0) || (data2.Count() > 0)) {
            RadWindowManager1.RadAlert("You Cannot Uncheck this Job Code. It Has Been Used to Log Work.", 500, 80, "Error", "");
            (sender as CheckBox).Checked = true;
            return;
        }
        
        JobCodeHelper.UpdateCompanyCustomerJob(isChecked, companyDivisionId, custId, jobCodeId);
        // Refresh
        grdSelectedCompanyCusts.DataBind();
    }
    protected void rowCheckBox2_OnDataBinding(object sender, EventArgs e) {
        CheckBox chk = (CheckBox)sender;
        chk.CheckedChanged -= ToggleRowSelection2;
        chk.Checked = Convert.ToBoolean(DataBinder.Eval(((sender as CheckBox).NamingContainer as GridItem).DataItem, "IsChecked"));
        chk.CheckedChanged += ToggleRowSelection2;
    }
    protected void ToggleRowSelection2(object sender, EventArgs e) {
        ((sender as CheckBox).NamingContainer as GridItem).Selected = true;
        bool isChecked = (sender as CheckBox).Checked;
        int companyDivisionId = int.Parse(grdSelectedCompanyCusts.SelectedValues["Companydivisionid"].ToString());
        int custId = int.Parse(grdSelectedCompanyCusts.SelectedValues["Customerid"].ToString());
        int jobCodeId = int.Parse(grdJobs.SelectedValues["Jobcodeid"].ToString());
        
        // Can it be deleted?
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        var data1 = from wl in dc.Worklogs
                    where wl.Companydivisionid == companyDivisionId
                    where wl.Customerid == custId
                    where wl.Jobcodeid == jobCodeId
                    select wl;

        var data2 = from ts in dc.Timesheets
                    where ts.Companydivisionid == companyDivisionId
                    where ts.Customerid == custId
                    where ts.Jobcodeid == jobCodeId
                    select ts;
                 
        if ((data1.Count() > 0) || (data2.Count() > 0)) {
            RadWindowManager1.RadAlert("You Cannot Uncheck this Job Code. It Has Been Used to Log Work.", 500, 80, "Error", "");
            (sender as CheckBox).Checked = true;
            return;
        }

        JobCodeHelper.UpdateCompanyCustomerJob(isChecked, companyDivisionId, custId, jobCodeId);
        // Refresh
        grdSelectedJobs.DataBind();
    }
    protected void grdCompanyCusts_DataBound(object sender, EventArgs e) {
        if (grdCompanyCusts.Items.Count > 0) {
            if (grdCompanyCusts.SelectedItems.Count == 0) {
                grdCompanyCusts.Items[0].Selected = true;
            }
        }
    }
    protected void grdJobs_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("Jobcodeid").ToString();

        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            SetMessage("Job Code " + id + " cannot be updated. Reason: " + e.Exception.Message);
        } else {
            SetMessage("Job Code " + id + " is updated!");
            grdSelectedJobs.DataBind();
        }
    }                           
    protected void grdJobs_ItemInserted(object source, GridInsertedEventArgs e) {
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("Job Code cannot be inserted. Reason: " + e.Exception.Message);
        } else {
            // Tell the user.
            SetMessage("New Job Code is inserted! DON'T FORGET TO SET UP YOUR RATE INFORMATION!!");
            grdSelectedJobs.DataBind();
        }
    }                               
    protected void grdJobs_ItemDeleted(object source, GridDeletedEventArgs e) {
        GridDataItem dataItem = (GridDataItem)e.Item;
        String id = dataItem.GetDataKeyValue("Jobcodeid").ToString();

        if (e.Exception != null) {
            e.ExceptionHandled = true;              
            if (e.Exception.InnerException != null) {
                if (e.Exception.InnerException.Message.IndexOf("Cannot delete or update a parent row") > -1) {
                    RadWindowManager1.RadAlert("You Cannot Delete this Job Code. It Has Been Used Elsewhere in the Website", 550, 80, "Error", "");
                }
            } else {
                SetMessage("Job Code " + id + " cannot be deleted. Reason: " + e.Exception.Message);
            }
        } else {
            SetMessage("Job Code " + id + " is deleted!");
            DisplayMessage(gridMessage);
            grdSelectedJobs.DataBind();
        }
    }
    protected void grdJobs_DataBound(object sender, EventArgs e) {
        if (grdJobs.Items.Count > 0) {
            if ((grdJobs.SelectedItems.Count == 0) && 
                (grdJobs.MasterTableView.IsItemInserted == false) &&
                (grdJobs.EditItems.Count == 0)) {
                grdJobs.Items[0].Selected = true;
            }
            //grdSelectedCompanyCusts.DataBind();
        }
        if (!string.IsNullOrEmpty(gridMessage)) {
            DisplayMessage(gridMessage);
        }
    }
    private string gridMessage = null;
    private void DisplayMessage(string text) {
        gridMessage = "";
        RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "grdJobs");
        grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage(string message) {
        gridMessage = message;
    }
}