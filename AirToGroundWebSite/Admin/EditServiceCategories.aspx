﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="EditServiceCategories.aspx.cs" Inherits="Admin_EditServiceCategories"
    Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <devart:DbLinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Servicecategory" OrderBy="Description" TableName="Servicecategories"
        EnableInsert="true" EnableUpdate="true" EnableDelete="true"> 
    </devart:DbLinqDataSource>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToPdfButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="true" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" Width="250" Height="75">
    </telerik:RadWindowManager>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Service Categories"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Service Categories</div>                    
            <telerik:RadGrid ID="RadGrid1" GridLines="None"
                Height="100%" runat="server" AllowAutomaticDeletes="True" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" AllowPaging="False" AllowSorting="True" AutoGenerateColumns="False"
                DataSourceID="LinqDataSource1" OnItemUpdated="RadGrid1_ItemUpdated" OnItemDeleted="RadGrid1_ItemDeleted"
                OnItemInserted="RadGrid1_ItemInserted" OnDataBound="RadGrid1_DataBound">
                <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="Servicecategoryid" CommandItemSettings-ShowRefreshButton="false"
                    DataSourceID="LinqDataSource1" HorizontalAlign="NotSet" AutoGenerateColumns="False" EditMode="InPlace">
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn DataField="Servicecategoryid" HeaderText="Id" AllowSorting="false"
                            UniqueName="DivisionareaidColumn" ItemStyle-ForeColor="Black" ReadOnly="true" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="Description" SortExpression="Description" UniqueName="DescriptionColumn">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblDescription" Text='<%# Eval("Description") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescription" runat="server" Width="98%" MaxLength="50" Text='<%# Bind("Description") %>'></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtDescription"
                                    ErrorMessage="Required" runat="server" CssClass="stdValidator"
                                    Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                            UniqueName="DeleteCommandColumn" CommandName="Delete">
                            <HeaderStyle Width="40px" />
                            <ItemStyle Width="40px" />
                        </telerik:GridButtonColumn>
                    </Columns>
                    <CommandItemSettings ShowExportToCsvButton="true" />
                    <AlternatingItemStyle BackColor="#F2F0F2" />
                </MasterTableView>
                <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false"
                    Csv-FileExtension="csv" />
                <ClientSettings>
                    <ClientEvents OnRowDblClick="RowDblClick" />
                    <Selecting AllowRowSelect="true" />
                    <Scrolling AllowScroll="False" />
                </ClientSettings>
            </telerik:RadGrid>
            <br />
        </div>
    </div>
</asp:Content>


