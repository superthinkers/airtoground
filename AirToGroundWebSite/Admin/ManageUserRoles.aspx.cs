﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;   
using System.Linq;
using Telerik.Web.UI;

public partial class Admin_ManageUserRoles : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_USER_SECURITY;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            // Security.
            if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_USERS)) {
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
            }

            // Clear the CCUJ cache.
            //if ((Cache[MembershipHelper.MH_CHECKED_CCUJ] != null) && (Cache[MembershipHelper.MH_CHECKED_CCUJ].ToString() != "")) {
            //    Cache[MembershipHelper.MH_CHECKED_CCUJ] = "";
            //}

            // Rate Columns.
            if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EMPLOYEE_RATE)) {
                RadGrid1.Columns.FindByUniqueName("SalaryamountColumn").Visible = false;
                RadGrid1.Columns.FindByUniqueName("HourlyrateColumn").Visible = false;
                RadGrid1.Columns.FindByUniqueName("Useca24hourruleColumn").Visible = false;
            }
        }
    }
    protected void RadGrid1_SelectedIndexChanged(object source, EventArgs e) {
        lblCCUJ.Text = "";
        if (RadGrid1.SelectedValues != null) {
            btnResetPassword.Text = "Reset Password: " + RadGrid1.SelectedValues["UserName"].ToString();
            btnResetPassword.Enabled = !RadGrid1.SelectedValues["UserName"].ToString().ToUpper().Contains("ADMIN");
        } else {
            btnResetPassword.Text = "";
            btnResetPassword.Enabled = false;
        }
        //RadGrid2.DataBind();
        // Clear the CCUJ cache.
        //if ((Cache[MembershipHelper.MH_CHECKED_CCUJ] != null) && (Cache[MembershipHelper.MH_CHECKED_CCUJ].ToString() != "")) {
        //    Cache[MembershipHelper.MH_CHECKED_CCUJ] = "";
        //}
        //RadGrid3.DataBind();
    }
    protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("UserName").ToString();

        // Refresh the local profile. This has to be done here.
        /*bool performRedirect = false;
        if (item.KeyValues.Contains(User.Identity.Name)) {
            ProfileCommon pc = new ProfileCommon().GetProfile(User.Identity.Name);
            // Update the local profile properties.
            Profile.SiteTheme = pc.SiteTheme;
            Profile.Companydivisionid = pc.Companydivisionid;
            Profile.Companydivisiondescription = pc.Companydivisiondescription;
            Profile.Companydivisionlocation = pc.Companydivisionlocation;
            Profile.Customerid = pc.Customerid;
            Profile.Customerdescription = pc.Customerdescription;
            Profile.Save();
            performRedirect = true;
        }*/   

        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;

            if ((e.Exception.InnerException != null) &&
               (e.Exception.InnerException.InnerException != null) &&
               (e.Exception.InnerException.InnerException.Message.Contains("Duplicate entry"))) {
                SetMessage("User " + id + " cannot be updated. Reason: Duplicate Employee Number or User name");
            } else {
                SetMessage("User " + id + " cannot be updated. Reason: " + e.Exception.InnerException.Message);
            }
            DisplayMessage(gridMessage);
        } else {
            SetMessage("User " + id + " is updated!");
        }

        // If the local logged in user was changed, refresh the page.
        //if (performRedirect) {
        //    Response.Redirect("~/Admin/ManageUserRoles.aspx");
        //}
    }
    protected void RadGrid1_ItemInserted(object source, Telerik.Web.UI.GridInsertedEventArgs e) {
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("User cannot be inserted. Reason: " + e.Exception.InnerException.Message);
            DisplayMessage(gridMessage);
        } else {
            SetMessage("New User is created!");
        }
    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e) {
        if ((e.Item is GridDataItem) && (e.Item.IsInEditMode == false)) {
            GridDataItem dataItem = e.Item as GridDataItem;
            int col1Idx = dataItem.OwnerTableView.GetColumn("UserNameColumn2").OrderIndex;
            TableCell cell1 = dataItem.Cells[col1Idx];
            if (!String.IsNullOrEmpty(((Label)cell1.Controls[1]).Text)) {
                // Edit col
                int col2Idx = dataItem.OwnerTableView.GetColumn("EditCommandColumn").OrderIndex;
                TableCell cell2 = dataItem.Cells[col2Idx];
                // Delete col
                int col3Idx = dataItem.OwnerTableView.GetColumn("DeleteCommandColumn").OrderIndex;
                TableCell cell3 = dataItem.Cells[col3Idx];
                if ((((Label)cell1.Controls[1]).Text.ToUpper().Contains("ADMIN")) &&
                     (!Profile.UserName.ToUpper().Contains("ADMIN"))) {
                    cell2.Text = "&nbsp;";
                    cell3.Text = "&nbsp;";
                }
            }
        }
    }
    protected void RadGrid1_DataBound(object sender, EventArgs e) {
        if (!string.IsNullOrEmpty(gridMessage)) {
            DisplayMessage(gridMessage);
        }
        if ((RadGrid1.SelectedValue == null) || (RadGrid1.SelectedValue.ToString() == "")) {
            if (RadGrid1.Items.Count > 0) {
                RadGrid1.Items[0].Selected = true;
                btnResetPassword.Text = "Reset Password: " + RadGrid1.SelectedValues["UserName"].ToString();
                btnResetPassword.Enabled = !RadGrid1.SelectedValues["UserName"].ToString().ToUpper().Contains("ADMIN");
            }
        }
        // Must refresh these - when filtering cols needs refresh.
        RadGrid1_SelectedIndexChanged(null, null);
    }
    private string gridMessage = null;
    private void DisplayMessage(string text) {
        gridMessage = "";
        RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "RadGrid1");
        grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage(string message) {
        gridMessage = message;
    }
    protected void ReqEmployeeNumber_Validate(object sender, ServerValidateEventArgs e) {
        // Employee Number is REQUIRED and must be UNIQUE
        RadTextBox txtEmpNum = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1, "txtEmployeeNumber");
        //CustomValidator reqEmpNum = (CustomValidator)PageUtils.FindControlRecursive(RadGrid1, "ReqEmployeeNumber");
        string sEmpNum = txtEmpNum.Text == null ? "" : txtEmpNum.Text.Trim();
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        var data = from exu in dc.Extendedusers
                   where exu.Employeenumber == sEmpNum.Trim()
                   select exu;
        e.IsValid = ((sEmpNum != "") && (data.Count() == 0));
        //if (string.IsNullOrEmpty(sEmpNum)) {
        //    reqEmpNum.ErrorMessage = "<br />Required";    
        //} else if (data.Count() > 0) {
        //    reqEmpNum.ErrorMessage = "<br />Not Unique";
        //}
    }
    protected void ReqPassword2_OnServerValidate(object sender, ServerValidateEventArgs e) {
        e.IsValid = e.Value != null && e.Value.Length >= 8;
    }
    // RadGrid2, for Role
    protected void ToggleRowSelection(object sender, EventArgs e) {
        ((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
        bool isChecked = (sender as CheckBox).Checked;
        string userName = RadGrid1.SelectedValues["UserName"].ToString();
        string roleName = (sender as CheckBox).Text;
        MembershipHelper h = new MembershipHelper();
        h.UpdateUserRoles(isChecked, roleName, userName);
    }
    // RadGrid3, for IsAssigned
    protected void ToggleRowSelection2(object sender, EventArgs e) {
        ((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
        bool isChecked = (sender as CheckBox).Checked;
        GridDataItem item = (sender as CheckBox).NamingContainer as GridDataItem;
        item.Selected = true;
        string userId = RadGrid1.SelectedValues["Userid"].ToString();
        string userName = RadGrid1.SelectedValues["UserName"].ToString();
        int companyDivisionId = int.Parse(RadGrid3.SelectedValues["CompanyDivisionId"].ToString());
        int customerId = int.Parse(RadGrid3.SelectedValues["CustomerId"].ToString());
        string errMsg = "";

        //if ((Profile.Userid != userId) || 
        //    (Profile.Companydivisionid != companyDivisionId.ToString()) || (Profile.Customerid != customerId.ToString())) {
            MembershipHelper h = new MembershipHelper();
            h.UpdateCCUJoins(isChecked, userId, userName, companyDivisionId, customerId, out errMsg);
            if (errMsg != "") {
                if (errMsg == MembershipHelper.INVALID_SELECTION) {
                    (sender as CheckBox).Checked = false;
                }
                //RadGrid3.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errMsg)));
                lblCCUJ.Text = errMsg;
            } else {
                lblCCUJ.Text = "&nbsp";
            }
            //RadGrid3.DataBind();
            //GridItem i = RadGrid1.SelectedItems[0];
            //RadGrid1.DataBind();
            //i.Selected = true;
        //} else {
        //    // Can't change the value that is currently assigned for the currently logged in user.
        //    (sender as CheckBox).Checked = !(sender as CheckBox).Checked;
        //    lblCCUJ.Text = "You cannot change this record (it is for the currently logged in user and current company and division)";
        //}
    }
    // RadGrid3, for IsDefault
    //protected void ToggleRowSelection3(object sender, EventArgs e) {
    //    ((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
    //    bool isDefault = (sender as CheckBox).Checked;
    //    GridDataItem item = (sender as CheckBox).NamingContainer as GridDataItem;
    //    string userId = RadGrid1.SelectedValues["Userid"].ToString();
    //    string userName = RadGrid1.SelectedValues["UserName"].ToString();
    //    int companyDivisionId = int.Parse(item["CompanyDivisionIdColumn"].Text);
    //    int customerId = int.Parse(item["CustomerIdColumn"].Text);
    //    string errMsg = "";

    //    if (Profile.Userid != userId) {
    //        MembershipHelper h = new MembershipHelper();
    //        h.UpdateProfileDefaults(isDefault, userId, userName, companyDivisionId, customerId, out errMsg);
    //        if (errMsg != "") {
    //            if (errMsg == MembershipHelper.INVALID_SELECTION) {
    //                (sender as CheckBox).Checked = false;
    //            }
    //            //RadGrid3.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errMsg)));
    //            lblCCUJ.Text = errMsg;
    //        } else {
    //            lblCCUJ.Text = "&nbsp;";
    //        }
    //        RadGrid3.DataBind();
    //        //GridItem i = RadGrid1.SelectedItems[0];
    //        //RadGrid1.DataBind();
    //        //i.Selected = true;
    //    } else {
    //        // Can't change the value that is currently assigned for the currently logged in user.
    //        (sender as CheckBox).Checked = !(sender as CheckBox).Checked;
    //        lblCCUJ.Text = "You cannot change this record (it is for the currently logged in user and current company and division)";
    //    }
    //}
    protected void btnResetPassword_Click(object sender, EventArgs e) {
        string un = RadGrid1.SelectedValues["UserName"].ToString();
        if ((un != null) && (un != "")) {
            if (!un.ToUpper().Contains("ADMIN")) {
                //MembershipUser u = ProviderUtils.GetUser(un);
                try {
                    MembershipHelper.ResetPassword(Server, un, Profile.UserName, "");
                    RadAjaxManager1.ResponseScripts.Add(@"radalert('Password Reset and Email Sent to: " + un + "', 330, 110);");
                } catch (Exception ex) {
                    throw ex;
                }
            } else {
                RadWindowManager1.RadAlert("You May Not Reset an Administrator's Password", null, null, "Alert", "");
            }
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e) {
        GridDataItem item = (sender as ImageButton).NamingContainer as GridDataItem;
        string userName = ((Label)item["UserNameColumn2"].Controls[1]).Text;
        if ((userName != "Administrator") && (userName != "&nbsp;")) {
            MembershipHelper.DeleteUser(userName);
            // Clear the CCUJ cache.
            //if ((Cache[MembershipHelper.MH_CHECKED_CCUJ] != null) && (Cache[MembershipHelper.MH_CHECKED_CCUJ].ToString() != "")) {
            //    Cache[MembershipHelper.MH_CHECKED_CCUJ] = "";
            //}
            ObjectDataSource1.DataBind();
            RadGrid1.DataBind();
        }
    }
    protected void btnRunLoad_OnClick(object sender, EventArgs e) {
        //MembershipHelper h = new MembershipHelper();
        //h.InsertUserEx("Login", Profile.UserName, "@", "!password", false, "F", "L", "#");
        //h.InsertUserEx("cnilest", Profile.UserName, "cnilest@airtoground.com", "!password", false, "Chris", "Nilest", "1234");
        //h.InsertUserEx("rgraham", Profile.UserName, "rgraham@airtoground.com", "!password", false, "Roger", "Graham", "1235");
        /*h.InsertUserEx("eburke", Profile.UserName, "eburke@airtoground.com", "!password", false, "Burke", "Effie", "102073");
        h.InsertUserEx("dcheatham", Profile.UserName, "dcheatham@airtoground.com", "!password", false, "Cheatham", "Delayno", "100004");
        h.InsertUserEx("scheeks", Profile.UserName, "scheeks@airtoground.com", "!password", false, "Cheeks", "Staci", "101997");
        h.InsertUserEx("kfuchs", Profile.UserName, "kfuchs@airtoground.com", "!password", false, "Fuchs", "Karen", "000877");
        h.InsertUserEx("ggoodman", Profile.UserName, "ggoodman@airtoground.com", "!password", false, "Goodman", "Gina", "101949");
        h.InsertUserEx("jgreenwood", Profile.UserName, "jgreenwood@airtoground.com", "!password", false, "Greenwood IV", "James", "101992");
        h.InsertUserEx("phamilton", Profile.UserName, "phamilton@airtoground.com", "!password", false, "Hamilton", "Patrick", "001576");
        h.InsertUserEx("dhensley", Profile.UserName, "dhensley@airtoground.com", "!password", false, "Hensley", "Daryl", "102213");
        h.InsertUserEx("klewis", Profile.UserName, "klewis@airtoground.com", "!password", false, "Lewis", "Kirk", "102051");
        h.InsertUserEx("mlynch", Profile.UserName, "mlynch@airtoground.com", "!password", false, "Lynch", "Michael", "101944");
        h.InsertUserEx("mnelms", Profile.UserName, "mnelms@airtoground.com", "!password", false, "Nelms", "Michelle", "102511");
        h.InsertUserEx("eorange", Profile.UserName, "eorange@airtoground.com", "!password", false, "Orange", "Eric", "001443");
        h.InsertUserEx("cpham", Profile.UserName, "cpham@airtoground.com", "!password", true, "Pham", "Claudia", "102359");
        h.InsertUserEx("bprice", Profile.UserName, "bprice@airtoground.com", "!password", false, "Price", "Brian", "100022");
        h.InsertUserEx("mspencer", Profile.UserName, "mspencer@airtoground.com", "!password", false, "Spencer Jr", "Michael", "001548");
        h.InsertUserEx("eyoung", Profile.UserName, "eyoung@airtoground.com", "!password", false, "Young", "Eugene", "102360");
        h.InsertUserEx("callen", Profile.UserName, "callen@airtoground.com", "!password", false, "Allen", "Christopher", "102644");
        h.InsertUserEx("jalltop", Profile.UserName, "jalltop@airtoground.com", "!password", false, "Alltop", "Jeff", "102441");
        h.InsertUserEx("randerson", Profile.UserName, "randerson@airtoground.com", "!password", false, "Anderson", "Robert", "102636");
        h.InsertUserEx("savalos", Profile.UserName, "savalos@airtoground.com", "!password", false, "Avalos", "Sarvelio", "02558");
        h.InsertUserEx("ebiesiada", Profile.UserName, "ebiesiada@airtoground.com", "!password", false, "Biesiada", "Eric", "102617");
        h.InsertUserEx("sbohannon", Profile.UserName, "sbohannon@airtoground.com", "!password", false, "Bohannon", "Shawn", "102631");
        h.InsertUserEx("abowman", Profile.UserName, "abowman@airtoground.com", "!password", false, "Bowman", "Anna Marie", "102163");
        */
        /*
        h.InsertUserEx("abray", Profile.UserName, "abray@airtoground.com", "!password", false, "Bray", "Anthony", "102020");
        h.InsertUserEx("lbrewer", Profile.UserName, "lbrewer@airtoground.com", "!password", false, "Brewer", "LaVon", "102642");
        h.InsertUserEx("bbridwell", Profile.UserName, "bbridwell@airtoground.com", "!password", false, "Bridwell", "Brittney", "102634");
        h.InsertUserEx("rbritt", Profile.UserName, "rbritt@airtoground.com", "!password", false, "Britt", "Robert", "102536");
        h.InsertUserEx("abrown", Profile.UserName, "abrown@airtoground.com", "!password", false, "Brown", "Asia", "102439");
        h.InsertUserEx("tburgett", Profile.UserName, "tburgett@airtoground.com", "!password", false, "Burgett", "Thomas", "102269");
        h.InsertUserEx("mburriell", Profile.UserName, "mburriell@airtoground.com", "!password", false, "Burriell", "Michael", "102645");
        h.InsertUserEx("jcallahan", Profile.UserName, "jcallahan@airtoground.com", "!password", false, "Callahan", "Joshua", "102629");
        h.InsertUserEx("dcarlton", Profile.UserName, "dcarlton@airtoground.com", "!password", false, "Carlton II", "Daniel", "102611");
        h.InsertUserEx("kchau", Profile.UserName, "kchau@airtoground.com", "!password", true, "Chau", "Kent", "102368");
        h.InsertUserEx("bclark", Profile.UserName, "bclark@airtoground.com", "!password", false, "Clark", "Bolivar", "102525");
        h.InsertUserEx("ecompton", Profile.UserName, "ecompton@airtoground.com", "!password", false, "Compton", "Edwin", "102227");
        h.InsertUserEx("scothron", Profile.UserName, "scothron@airtoground.com", "!password", false, "Cothron", "Shaun", "102158");
        h.InsertUserEx("mcroley", Profile.UserName, "mcroley@airtoground.com", "!password", false, "Croley", "Mark", "101956");
        h.InsertUserEx("tcroley", Profile.UserName, "tcroley@airtoground.com", "!password", false, "Croley", "Tim", "102389");
        h.InsertUserEx("dcurry", Profile.UserName, "dcurry@airtoground.com", "!password", true, "Curry", "Davel", "102350");
        h.InsertUserEx("cdavis", Profile.UserName, "cdavis@airtoground.com", "!password", false, "Davis", "Charles", "102572");
        h.InsertUserEx("cderas", Profile.UserName, "cderas@airtoground.com", "!password", false, "Deras", "Cruz", "102277");
        h.InsertUserEx("wdiehl", Profile.UserName, "wdiehl@airtoground.com", "!password", false, "Diehl", "William", "102186");
        h.InsertUserEx("sdotson", Profile.UserName, "sdotson@airtoground.com", "!password", false, "Dotson Jr", "Stanley", "102602");
        h.InsertUserEx("jdrury", Profile.UserName, "jdrury@airtoground.com", "!password", false, "Drury", "Jordan", "102527");
        h.InsertUserEx("cdugger", Profile.UserName, "cdugger@airtoground.com", "!password", false, "Dugger", "Cody", "102449");
        h.InsertUserEx("tdunagan", Profile.UserName, "tdunagan@airtoground.com", "!password", false, "Dunagan", "Timothy", "102630");
        h.InsertUserEx("beggers", Profile.UserName, "beggers@airtoground.com", "!password", false, "Eggers III", "Bert", "102459");
        h.InsertUserEx("eegharevba", Profile.UserName, "eegharevba@airtoground.com", "!password", true, "Egharevba", "Efosa", "102344");
        h.InsertUserEx("mekler", Profile.UserName, "mekler@airtoground.com", "!password", false, "Elder", "Michelle", "102330");
        h.InsertUserEx("jfelts", Profile.UserName, "jfelts@airtoground.com", "!password", false, "Felts", "Jeffrey", "102570");
        h.InsertUserEx("jfisher", Profile.UserName, "jfisher@airtoground.com", "!password", false, "Fisher", "Jerome", "102650");
        h.InsertUserEx("tfoster", Profile.UserName, "tfoster@airtoground.com", "!password", false, "Foster", "Toya", "102270");
        h.InsertUserEx("dfunk", Profile.UserName, "dfunk@airtoground.com", "!password", false, "Funk", "Dale", "102230");
        h.InsertUserEx("egaldamez", Profile.UserName, "egaldamez@airtoground.com", "!password", true, "Galdamez", "Eric", "102319");
        h.InsertUserEx("wgaldamez", Profile.UserName, "wgaldamez@airtoground.com", "!password", true, "Galdamez", "William", "102511b");
        */
        /*
        h.InsertUserEx("lgalvez", Profile.UserName, "lgalvez@airtoground.com", "!password", true, "Galvez", "Laurence", "102606");
        h.InsertUserEx("sgaona", Profile.UserName, "sgaona@airtoground.com", "!password", true, "Gaona", "Saul", "102332");
        h.InsertUserEx("jgarcia", Profile.UserName, "jgarcia@airtoground.com", "!password", true, "Garcia", "Johathan", "102316");
        h.InsertUserEx("tgarrett", Profile.UserName, "tgarrett@airtoground.com", "!password", false, "Garrett", "Trisha", "102545");
        h.InsertUserEx("agodinez", Profile.UserName, "agodinez@airtoground.com", "!password", true, "Godinez", "Andy", "102560");
        h.InsertUserEx("jgoldsmith", Profile.UserName, "jgoldsmith@airtoground.com", "!password", false, "Goldsmith", "Jason", "102494");
        h.InsertUserEx("ggomez", Profile.UserName, "ggomez@airtoground.com", "!password", true, "Gomez", "Gerald", "102648");
        h.InsertUserEx("agonzalez", Profile.UserName, "agonzalez@airtoground.com", "!password", false, "Gonzalez", "Alejandro", "101865");
        h.InsertUserEx("cgonzalez", Profile.UserName, "cgonzalez@airtoground.com", "!password", true, "Gonzalez", "Christopher", "102515");
        h.InsertUserEx("cgorbett", Profile.UserName, "cgorbett@airtoground.com", "!password", false, "Gorbett", "Christopher", "102192");
        h.InsertUserEx("jgraham", Profile.UserName, "jgraham@airtoground.com", "!password", false, "Graham", "Justin", "101927");
        h.InsertUserEx("sgraham", Profile.UserName, "sgraham@airtoground.com", "!password", false, "Graham", "Shawn", "001773");
        h.InsertUserEx("jgreen", Profile.UserName, "jgreen@airtoground.com", "!password", false, "Green", "Joshua", "102643");
        h.InsertUserEx("wguardado", Profile.UserName, "wguardado@airtoground.com", "!password", true, "Guardado", "Wilfredo", "102311");
        h.InsertUserEx("zhamilton", Profile.UserName, "zhamilton@airtoground.com", "!password", false, "Hamilton", "Zachary", "102637");
        h.InsertUserEx("qharris", Profile.UserName, "qharris@airtoground.com", "!password", false, "Harris", "Quentin", "102586");
        h.InsertUserEx("jheard", Profile.UserName, "jheard@airtoground.com", "!password", false, "Heard", "Johathan", "102160");
        h.InsertUserEx("jherbert", Profile.UserName, "jherbert@airtoground.com", "!password", false, "Hebert", "Justin", "102646");
        h.InsertUserEx("shernandez", Profile.UserName, "shernandez@airtoground.com", "!password", true, "Hernandez", "Saul", "102292");
        h.InsertUserEx("ghines", Profile.UserName, "ghines@airtoground.com", "!password", false, "Hines", "Gregory", "102590");
        h.InsertUserEx("dhinton", Profile.UserName, "dhinton@airtoground.com", "!password", false, "Hinton", "David", "102625");
        h.InsertUserEx("jhoke", Profile.UserName, "jhoke@airtoground.com", "!password", false, "Hoke", "Jeremy", "102623");
        h.InsertUserEx("dhubbard", Profile.UserName, "dhubbard@airtoground.com", "!password", false, "Hubbard", "Darrin", "102166");
        h.InsertUserEx("mhunt", Profile.UserName, "mhunt@airtoground.com", "!password", false, "Hunt Jr", "Micheal", "001641");
        h.InsertUserEx("jhunter", Profile.UserName, "jhunter@airtoground.com", "!password", false, "Hunter", "Jay", "102447");
        h.InsertUserEx("ajohnson", Profile.UserName, "ajohnson@airtoground.com", "!password", false, "Johnson", "Andrew", "100043");
        h.InsertUserEx("rjones", Profile.UserName, "rjones@airtoground.com", "!password", false, "Jones", "Roger", "102238");
        h.InsertUserEx("jkappel", Profile.UserName, "jkappel@airtoground.com", "!password", false, "Kappel", "Joshua", "102613");
        h.InsertUserEx("jkeith", Profile.UserName, "jkeith@airtoground.com", "!password", false, "Keith", "Justin", "102404");
        h.InsertUserEx("skluttz", Profile.UserName, "skluttz@airtoground.com", "!password", false, "Kluttz", "Scott", "102232");
        h.InsertUserEx("nlam", Profile.UserName, "nlam@airtoground.com", "!password", true, "Lam", "Nelson", "102380");
        h.InsertUserEx("tlawton", Profile.UserName, "tlawton@airtoground.com", "!password", false, "Lawton", "Tyheem", "102627");
        h.InsertUserEx("jleeper", Profile.UserName, "jleeper@airtoground.com", "!password", false, "Leeper", "Jeffrey", "102593");
        h.InsertUserEx("jlemus", Profile.UserName, "jlemus@airtoground.com", "!password", true, "Lemus", "Jose", "102365");
        h.InsertUserEx("clendman", Profile.UserName, "clendman@airtoground.com", "!password", false, "Lendman", "Cody", "102640");
        h.InsertUserEx("rleos", Profile.UserName, "rleos@airtoground.com", "!password", true, "Leos Jr", "Raul", "102321");
        h.InsertUserEx("jlloyd", Profile.UserName, "jlloyd@airtoground.com", "!password", false, "Lloyd", "Joshua", "102599");
        h.InsertUserEx("slopez", Profile.UserName, "slopez@airtoground.com", "!password", true, "Lopez", "Salvador", "102364");
        h.InsertUserEx("lluneta", Profile.UserName, "lluneta@airtoground.com", "!password", true, "Luneta", "Steve Leomar", "102559");
        h.InsertUserEx("jlusk", Profile.UserName, "jlusk@airtoground.com", "!password", false, "Lusk", "Jacob", "001733");
        h.InsertUserEx("dmahaffey", Profile.UserName, "dmahaffey@airtoground.com", "!password", false, "Mahaffey", "Douglas", "102369");
        h.InsertUserEx("gmartinez", Profile.UserName, "gmartinez@airtoground.com", "!password", true, "Martinez", "Guadalupe", "102608");
        h.InsertUserEx("mmason", Profile.UserName, "mmason@airtoground.com", "!password", false, "Mason", "McCellies", "102547");
        h.InsertUserEx("gmccallister", Profile.UserName, "gmccallister@airtoground.com", "!password", false, "McCallister", "Gregory", "102594");
        h.InsertUserEx("bmccullough", Profile.UserName, "bmccullough@airtoground.com", "!password", false, "McCullough", "Bill", "101987");
        h.InsertUserEx("dmeadows", Profile.UserName, "dmeadows@airtoground.com", "!password", false, "Meadows", "David", "102221");
        h.InsertUserEx("rmidkiff", Profile.UserName, "rmidkiff@airtoground.com", "!password", false, "Midkiff", "Robert", "102457");
        h.InsertUserEx("mmiller", Profile.UserName, "mmiller@airtoground.com", "!password", false, "Miller", "Megan", "102358");
        h.InsertUserEx("mmoore", Profile.UserName, "mmoore@airtoground.com", "!password", false, "Moore", "Mark", "102211");
        h.InsertUserEx("tmoore", Profile.UserName, "tmoore@airtoground.com", "!password", false, "Moore", "Tracy", "102313");
        h.InsertUserEx("dmorrison", Profile.UserName, "dmorrison@airtoground.com", "!password", false, "Morrison", "Donald", "102504");
        h.InsertUserEx("bmurphy", Profile.UserName, "bmurphy@airtoground.com", "!password", false, "Murphy", "Bradley", "102194");
        h.InsertUserEx("smuth", Profile.UserName, "smuth@airtoground.com", "!password", false, "Muth", "Samantha", "102624");
        h.InsertUserEx("aochoarojas", Profile.UserName, "aochoarojas@airtoground.com", "!password", true, "Ochoa-Rojas", "Arturo", "102351");
        h.InsertUserEx("koclair", Profile.UserName, "koclair@airtoground.com", "!password", false, "O'Clair", "Keith", "102618");
        h.InsertUserEx("jodaniel", Profile.UserName, "jodaniel@airtoground.com", "!password", false, "O'Daniel", "Jacob", "102433");
        h.InsertUserEx("dolin", Profile.UserName, "dolin@airtoground.com", "!password", false, "Olin", "David", "102482");
        h.InsertUserEx("cpeevyhouse", Profile.UserName, "cpeevyhouse@airtoground.com", "!password", false, "Peevyhouse", "Cindy", "102463");
        h.InsertUserEx("jpena", Profile.UserName, "jpena@airtoground.com", "!password", true, "Pena", "Jan Vincent", "102505");
        h.InsertUserEx("rpence", Profile.UserName, "rpence@airtoground.com", "!password", false, "Pence", "Randy", "102336");
        h.InsertUserEx("wperry", Profile.UserName, "wperry@airtoground.com", "!password", false, "Perry", "Walter", "102241");
        h.InsertUserEx("dpetry", Profile.UserName, "dpetry@airtoground.com", "!password", false, "Petry", "Donna", "102521");
        h.InsertUserEx("rporter", Profile.UserName, "rporter@airtoground.com", "!password", false, "Porter", "Ronald", "001326");
        h.InsertUserEx("squezada", Profile.UserName, "squezada@airtoground.com", "!password", true, "Quezada", "Sergio", "102649");
        h.InsertUserEx("arodriguez", Profile.UserName, "arodriguez@airtoground.com", "!password", true, "Rodriguez", "Adrian", "102317");
        h.InsertUserEx("grodriguez", Profile.UserName, "grodriguez@airtoground.com", "!password", true, "Rodriguez", "Gloria", "102348");
        h.InsertUserEx("jrodrigruez", Profile.UserName, "jrodrigruez@airtoground.com", "!password", false, "Rodriguez", "John", "102363");
        h.InsertUserEx("aroque", Profile.UserName, "aroque@airtoground.com", "!password", true, "Roque", "Angelie", "102607");
        h.InsertUserEx("srose", Profile.UserName, "srose@airtoground.com", "!password", true, "Rose", "Steve", "102561");
        h.InsertUserEx("rsangalang", Profile.UserName, "rsangalang@airtoground.com", "!password", true, "Sangalang", "Raymond", "102310");
        h.InsertUserEx("ashannon", Profile.UserName, "ashannon@airtoground.com", "!password", false, "Shannon", "Andrew", "102436");
        h.InsertUserEx("ashramm", Profile.UserName, "ashramm@airtoground.com", "!password", false, "Shramm", "Amber", "102489");
        h.InsertUserEx("csimmons", Profile.UserName, "csimmons@airtoground.com", "!password", false, "Simmons", "Claude", "001815");
        h.InsertUserEx("sskiff", Profile.UserName, "sskiff@airtoground.com", "!password", false, "Skiff", "Shelly", "102229");
        h.InsertUserEx("jsmith", Profile.UserName, "jsmith@airtoground.com", "!password", false, "Smith", "Joseph", "001416");
        h.InsertUserEx("ksortore", Profile.UserName, "ksortore@airtoground.com", "!password", false, "Sortore", "Keith", "102442");
        h.InsertUserEx("cspiller", Profile.UserName, "cspiller@airtoground.com", "!password", false, "Spiller", "Connie", "000769");
        h.InsertUserEx("mstearns", Profile.UserName, "mstearns@airtoground.com", "!password", false, "Stearns", "Michael", "102254");
        h.InsertUserEx("wsteele", Profile.UserName, "wsteele@airtoground.com", "!password", false, "Steele", "William", "102467");
        h.InsertUserEx("mstephens", Profile.UserName, "mstephens@airtoground.com", "!password", false, "Stephens", "Marissa", "102609");
        h.InsertUserEx("pstone", Profile.UserName, "pstone@airtoground.com", "!password", false, "Stone", "Phillip", "102422");
        h.InsertUserEx("jtennill", Profile.UserName, "jtennill@airtoground.com", "!password", false, "Tennill", "James", "001802");
        h.InsertUserEx("cthipe", Profile.UserName, "cthipe@airtoground.com", "!password", false, "Thipe", "Christopher", "102472");
        h.InsertUserEx("vtorres", Profile.UserName, "vtorres@airtoground.com", "!password", true, "Torres", "Vicente", "102366");
        h.InsertUserEx("vtumax", Profile.UserName, "vtumax@airtoground.com", "!password", false, "Tumax Jr", "Victor", "102164");
        h.InsertUserEx("mvierling", Profile.UserName, "mvierling@airtoground.com", "!password", false, "Vierling", "Matthew", "102290");
        h.InsertUserEx("mwaddell", Profile.UserName, "mwaddell@airtoground.com", "!password", false, "Waddell", "Mickey", "102522");
        h.InsertUserEx("swalker", Profile.UserName, "swalker@airtoground.com", "!password", false, "Walker III", "Steven", "102647");
        h.InsertUserEx("swhiteside", Profile.UserName, "swhiteside@airtoground.com", "!password", false, "Whiteside", "Steven", "001439");
        h.InsertUserEx("bwilliams", Profile.UserName, "bwilliams@airtoground.com", "!password", true, "Williams", "Brandon", "102499");
        h.InsertUserEx("cwilson", Profile.UserName, "cwilson@airtoground.com", "!password", false, "Wilson", "Christopher", "102356");
        h.InsertUserEx("dwilson", Profile.UserName, "dwilson@airtoground.com", "!password", false, "Wilson", "Drew", "102639");
        h.InsertUserEx("dwrightsman", Profile.UserName, "dwrightsman@airtoground.com", "!password", false, "Wrightsman", "David", "102247");
        h.InsertUserEx("cyoung", Profile.UserName, "cyoung@airtoground.com", "!password", false, "Young", "Christian", "102471");
        */
    }
}