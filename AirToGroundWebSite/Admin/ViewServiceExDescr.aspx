﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewServiceExDescr.aspx.cs"
    Inherits="Admin_ViewServiceExDescr" Buffer="true" Strict="true" Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function doClose() {
                var oWnd = GetRadWindow();
                oWnd.close();
            }
        </script>
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server" AsyncPostBackTimeout="600"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin" ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator2" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <div id="mainDiv" runat="server" style="margin: 2px 2px 2px 2px; width: 500px; height: 400px">
            <div style="text-align: center; width: 100%">
                <div style="width: 100%; margin: 5px 0px 5px 0px">
                    <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Extended Description"></asp:Label>
                </div>
				<br />
                <div style="width: 100%; margin: 5px 0px 5px 0px">
                    <asp:Label ID="lblService" runat="server" SkinID="InfoLabel" Text=""></asp:Label>
                </div>
            </div>
            <div style="text-align: left; width: 100%; margin: 5px 0px 5px 0px">
                <telerik:RadAjaxPanel ID="radAjaxPanel1" runat="server">
                    <asp:Panel ID="pnlScroll" runat="server" Width="99%" Height="340px" ScrollBars="Vertical"
                        BorderStyle="Solid" BorderColor="Silver" BorderWidth="1">
                        <asp:Label ID="txtExDescr" runat="server">
                        </asp:Label>
                    </asp:Panel>
                    <br />
                    <div style="text-align: center; width: 98%">
                        <%--<asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />--%>
                        <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="doClose(); return false;" />
                    </div>
                </telerik:RadAjaxPanel>
            </div>
        </div>
    </form>
</body>
</html>
