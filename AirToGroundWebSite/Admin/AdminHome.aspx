﻿<%@ Page ViewStateMode="Disabled" Title="" Language="C#" MasterPageFile="~/Default.master"
    AutoEventWireup="true" CodeFile="AdminHome.aspx.cs" Inherits="Admin_AdminHome"
    Buffer="true" Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Website Administration"></asp:Label>
        <br />
        <br />
        <div style="position:relative; left:17%; width:66%">
            <div class="div_container" style="width: 50%;">
                <div class="div_header">Code Table Maintenance</div>
                <div style="text-align: left; position: relative; margin: 10px 0px 0px 10px; width: 98%">
                    <asp:Label ID="lblSecurity" runat="server" SkinID="SmallLabel" Text="<br />You Must Have the &quot;Code Table Editor&quot; Role to Edit Code Tables"></asp:Label>
                    <asp:Label ID="lblDivisionTables" runat="server" SkinID="SmallLabel" Text="You Must Have the &quot;Corporate User&quot; Role to Edit Division / Customer Information, Including Aircraft<br />"></asp:Label>
                    <table width="98%">
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkCompanyDivisions1" runat="server" SkinID="ImageCompany" Width="32px"
                                    Height="32px" NavigateUrl="~/Admin/EditCompanyDivisions.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkCompanyDivisions2" runat="server" Text="Company Divisions"
                                    NavigateUrl="~/Admin/EditCompanyDivisions.aspx"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkCustomers1" runat="server" SkinID="ImageCustServiceSections"
                                    Width="32px" Height="32px" NavigateUrl="~/Admin/EditCustomers.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkCustomers2" runat="server" Text="Customers, Services, and Sections"
                                    NavigateUrl="~/Admin/EditCustomers.aspx"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkDivisionAreas1" runat="server" SkinID="ImageArea" Width="32px"
                                    Height="32px" NavigateUrl="~/Admin/EditDivisionAreas.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkDivisionAreas2" runat="server" Text="Division Areas" NavigateUrl="~/Admin/EditDivisionAreas.aspx"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkEquipment1" runat="server" SkinID="ImageAirplane1" Width="32px"
                                    Height="32px" NavigateUrl="~/Admin/EditEquipment.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkEquipment2" runat="server" Text="Aircraft" NavigateUrl="~/Admin/EditEquipment.aspx"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkEquipmentTypes1" runat="server" SkinID="ImageAirplane2" Width="32px"
                                    Height="32px" NavigateUrl="~/Admin/EditEquipmentTypes.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkEquipmentTypes2" runat="server" Text="Aircraft Types"
                                    NavigateUrl="~/Admin/EditEquipmentTypes.aspx"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkServices1" runat="server" SkinID="ImageServiceRules" Width="32px"
                                    Height="32px" NavigateUrl="~/Admin/EditServices.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkServices2" runat="server" Text="Services and Service Rules"
                                    NavigateUrl="~/Admin/EditServices.aspx"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkServiceShortDescr1" runat="server" SkinID="ImageServiceRules" Width="32px"
                                    Height="32px" NavigateUrl="~/Admin/EditServiceShortDescr.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkServiceShortDescr2" runat="server" Text="Service Short Descriptions"
                                    NavigateUrl="~/Admin/EditServiceShortDescr.aspx"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkServiceCats1" runat="server" SkinID="ImageServiceRules" Width="32px"
                                    Height="32px" NavigateUrl="~/Admin/EditServiceCategories.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkServiceCats2" runat="server" Text="Service Categories" NavigateUrl="~/Admin/EditServiceCategories.aspx"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkSections1" runat="server" SkinID="ImageSection" Width="32px"
                                    Height="32px" NavigateUrl="~/Admin/EditSections.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkSections2" runat="server" Text="Service Sections" NavigateUrl="~/Admin/EditSections.aspx"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkJobs1" runat="server" SkinID="ImageServiceRules" Width="32px"
                                    Height="32px" NavigateUrl="~/Admin/EditJobs.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkJobs2" runat="server" Text="Job Codes" NavigateUrl="~/Admin/EditJobs.aspx"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkRates1" runat="server" SkinID="ImageServiceRules" Width="32px"
                                    Height="32px" NavigateUrl="~/Admin/Rate/EditRateSchedule.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkRates2" runat="server" Text="Rate Schedules" NavigateUrl="~/Admin/Rate/EditRateSchedule.aspx"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="div_container" style="width: 50%;">
                <div class="div_header">User Security Settings</div>
                <div style="text-align: left; position: relative; margin: 10px 0px 0px 10px; width: 98%">
                    <asp:Label ID="lblUserSecurity" runat="server" SkinID="SmallLabel" Text="<br />You Must Have the &quot;User Security&quot; Role to Edit User Security"></asp:Label>
                    <table width="98%">
                        <tr>
                            <td style="width: 32px">
                                <asp:HyperLink ID="lnkUserRoles1" runat="server" SkinID="ImageUsers" Width="32px"
                                    Height="32px" NavigateUrl="~/Admin/ManageUserRoles.aspx"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkUserRoles2" runat="server" Text="Manage Users and Their Roles"
                                    NavigateUrl="~/Admin/ManageUserRoles.aspx"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

