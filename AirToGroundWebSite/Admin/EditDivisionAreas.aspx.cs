﻿using System;
using System.Web.UI;
using Telerik.Web.UI;

public partial class Admin_EditDivisionAreas : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_DIVISION_AREAS;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
        //m.IsActive = true;
        if (!Page.IsPostBack) {
            // Security.
            if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_GEN)) {
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
            }
        }
    }
    protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("Divisionareaid").ToString();

        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            SetMessage("Division Area " + id + " cannot be updated. Reason: " + e.Exception.Message);
        } else {
            SetMessage("Division Area " + id + " is updated!");
        }
    }

    protected void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e) {
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("Division Area cannot be inserted. Reason: " + e.Exception.Message);
        } else {
            SetMessage("New Division Area is inserted!");
        }
    }

    protected void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e) {
        GridDataItem dataItem = (GridDataItem)e.Item;
        String id = dataItem.GetDataKeyValue("Divisionareaid").ToString();

        if (e.Exception != null) {
            e.ExceptionHandled = true;
            if (e.Exception.Message.ToUpper().Contains("ERROR ON EXECUTING DBCOMMAND")) {
                SetMessage("Cannot Delete. This Division Area has been used on a Service Schedule.");
            } else {
                SetMessage("Division Area " + id + " cannot be deleted. Reason: " + e.Exception.Message);
            }
        } else {
            SetMessage("Division Area " + id + " is deleted!");
            DisplayMessage(gridMessage);
        }
    }
    protected void RadGrid1_DataBound(object sender, EventArgs e) {
        if (!string.IsNullOrEmpty(gridMessage)) {
            DisplayMessage(gridMessage);
        }
    }
    private string gridMessage = null;
    private void DisplayMessage(string text) {
        gridMessage = "";
        RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "RadGrid1");
        grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage(string message) {
        gridMessage = message;
    }
}