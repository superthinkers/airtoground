﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="EditEquipmentTypes.aspx.cs" Inherits="Admin_EditEquipmentTypes" Buffer="true"
    Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <devart:DbLinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Equipmenttype" OrderBy="Description" TableName="Equipmenttypes"
        EnableInsert="true" EnableUpdate="true" EnableDelete="true" >
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Customer" OrderBy="Description" TableName="Customers">
    </devart:DbLinqDataSource>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToPdfButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="true" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" Width="250" Height="75">
    </telerik:RadWindowManager>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Aircraft Types"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">
                Aircraft Types</div>
            <telerik:RadGrid ID="RadGrid1" GridLines="None"
                Height="100%" runat="server" AllowAutomaticDeletes="True" AllowAutomaticInserts="True"
                AllowAutomaticUpdates="True" AllowPaging="False" AllowSorting="True" AutoGenerateColumns="False"
                DataSourceID="LinqDataSource1" OnItemUpdated="RadGrid1_ItemUpdated" OnItemDeleted="RadGrid1_ItemDeleted"
                OnItemInserted="RadGrid1_ItemInserted" OnDataBound="RadGrid1_DataBound" OnItemCommand="RadGrid1_ItemCommand"
                AllowFilteringByColumn="true">
                <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="Equipmenttypeid"
                    DataSourceID="LinqDataSource1" HorizontalAlign="NotSet" AutoGenerateColumns="False"
                    EditMode="InPlace" CommandItemSettings-ShowRefreshButton="false">
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn DataField="Equipmenttypeid" HeaderText="Id" AllowSorting="false"
                            UniqueName="EquipmenttypeidColumn"
                            ItemStyle-ForeColor="Black" ReadOnly="true" AllowFiltering="false" Visible="false">
                        </telerik:GridBoundColumn>
                        <filters:TextBoxFilterGridTemplateColumn HeaderText="Description" DataField="Description"
                            UniqueName="DescriptionColumn" SortExpression="Description" FilterControlWidth="98%">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblDescription" Text='<%# Eval("Description") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescription" runat="server" Width="98%" MaxLength="100" Text='<%# Bind("Description") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtDescription"
                                    ErrorMessage="<br />Required" runat="server" CssClass="stdValidator"
                                    Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </EditItemTemplate>
                        </filters:TextBoxFilterGridTemplateColumn>
                        <filters:CustomerFilterGridTemplateColumn HeaderText="Customer" DataField="Customer.Description"
                            UniqueName="CustomeridColumn" SortExpression="Customer.Description" FilterControlWidth="98%">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblCustomer" Text='<%# Eval("Customer.Description") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <telerik:RadComboBox ID="lstCustomer" runat="server" Width="98%" DataSourceID="LinqDataSource2"
                                    DataTextField="Description" DataValueField="Customerid" SelectedValue='<%# Bind("Customerid") %>'
                                    OnDataBound="lstCustomer_DataBound">
                                </telerik:RadComboBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="lstCustomer"
                                    ErrorMessage="<br />Required" runat="server" CssClass="stdValidator"
                                    Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <HeaderStyle Width="200px" />
                            <HeaderStyle Width="200px" />
                        </filters:CustomerFilterGridTemplateColumn>
                        <telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                            UniqueName="DeleteCommandColumn" CommandName="Delete">
                            <HeaderStyle Width="40px" />
                            <ItemStyle Width="40px" />
                        </telerik:GridButtonColumn>
                    </Columns>
                    <CommandItemSettings ShowExportToCsvButton="true" />
                    <AlternatingItemStyle BackColor="#F2F0F2" />
                </MasterTableView>
                <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false"
                    Csv-FileExtension="csv" />
                <ClientSettings>
                    <ClientEvents OnRowDblClick="RowDblClick" />
                    <Selecting AllowRowSelect="true" />
                    <Scrolling AllowScroll="False" />
                </ClientSettings>
            </telerik:RadGrid>
            <br />
        </div>
    </div>
</asp:Content>
                                       