﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="EditJobs.aspx.cs" Inherits="Admin_EditJobs" Buffer="true" Strict="true"
    Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <asp:ObjectDataSource ID="ObjectDataSource0" runat="server" SelectMethod="GetCompanyCustomerJoinsDS"
        TypeName="ATGDB.Companydivision">
        <SelectParameters>
            <asp:Parameter Name="Distinct" DefaultValue="true" Type="Boolean" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetCheckedJobsDS"
        TypeName="JobCodeHelper">
        <SelectParameters>
            <asp:ControlParameter ControlID="grdCompanyCusts" Name="Companydivisionid" PropertyName="SelectedValues['Companydivisionid']"
                Type="Int32" />
            <asp:ControlParameter ControlID="grdCompanyCusts" Name="Customerid" PropertyName="SelectedValues['Customerid']"
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource1" runat="server" 
        ContextTypeName="ATGDB.ATGDataContext" EnableDelete="True" EnableInsert="True" 
        EnableUpdate="True" EntityTypeName="ATGDB.Jobcode" TableName="Jobcodes" 
        OrderBy="Code">
    </devart:DbLinqDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetCheckedCompanyCustomerJobsDS"
        TypeName="JobCodeHelper">
        <SelectParameters>
            <asp:ControlParameter ControlID="grdJobs" Name="Jobcodeid" PropertyName="SelectedValues['Jobcodeid']"
                Type="Int32" />
            <asp:ControlParameter ControlID="grdJobs" Name="Description" PropertyName="SelectedValues['Description']"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToPdfButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important"
        ShowContentDuringLoad="false" VisibleStatusbar="false" KeepInScreenBounds="true"
        Overlay="true" ReloadOnShow="true" runat="server" EnableShadow="false"
        Modal="true" VisibleTitlebar="false" Width="250" Height="75">
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="grdCompanyCusts">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdSelectedJobs" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="grdJobs">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdJobs" />
                    <telerik:AjaxUpdatedControl ControlID="grdSelectedCompanyCusts" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rowCheckbox1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdSelectedCompanyCusts" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rowCheckbox2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdSelectedJobs" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="true"
        Skin="Office2007">
    </telerik:RadAjaxLoadingPanel>
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Job Code Administration"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Company / Customer Job Assignment</div>
            <div style="position: relative; float:left; width: 49%">
                <br />
                <asp:Label ID="lblTitle1" runat="server" SkinID="InfoLabelBold" Text="Company Division / Customer"></asp:Label>
                <telerik:RadGrid ID="grdCompanyCusts" DataSourceID="ObjectDataSource0"
                    GridLines="None" runat="server" Height="200px" AllowAutomaticUpdates="false"
                    AllowPaging="false" AllowFilteringByColumn="false" AllowSorting="true" AutoGenerateColumns="false"
                    AllowMultiRowSelection="false" HorizontalAlign="Left" OnDataBound="grdCompanyCusts_DataBound">
                    <MasterTableView DataSourceID="ObjectDataSource0" DataKeyNames="Companydivisionid,Customerid"
                        HorizontalAlign="Left" AllowAutomaticUpdates="false" AllowAutomaticInserts="false"
                        AutoGenerateColumns="false" CommandItemDisplay="Top" CommandItemSettings-ShowRefreshButton="false"
                        CommandItemSettings-ShowAddNewRecordButton="false">
                        <Columns>
                            <telerik:GridBoundColumn DataField="Companydivisionid" UniqueName="CompanydivisionidColumn"
                                ReadOnly="true" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Customerid" UniqueName="CustomeridColumn"
                                ReadOnly="true" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Companydescr" HeaderText="Company"
                                UniqueName="CompanydivisiondescrColumn" ReadOnly="true" Visible="true" AllowSorting="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Customerdescr" HeaderText="Customer"
                                UniqueName="CustomerdescrColumn" ReadOnly="true" Visible="true" AllowSorting="true">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <CommandItemSettings ShowExportToCsvButton="true" />
                        <AlternatingItemStyle BackColor="#F2F0F2" />
                    </MasterTableView>
                    <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false"
                        Csv-FileExtension="csv" />
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <ClientEvents />
                        <Selecting AllowRowSelect="true" />
                        <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="false" />
                    </ClientSettings>
                </telerik:RadGrid>
                <br />
            </div>
            <div style="position: relative; float: left; width: 49%; margin-left: 5px">
                <br />
                <asp:Label ID="lblTitle2" runat="server" SkinID="InfoLabelBold" Text="Selected Job Codes"></asp:Label>
                <telerik:RadGrid ID="grdSelectedJobs" GridLines="None" runat="server"
                    Height="200px" AllowMultiRowSelection="false" AutoGenerateColumns="false" DataSourceID="ObjectDataSource1">
                    <MasterTableView DataKeyNames="Companydivisionid,Customerid,Jobcodeid" DataSourceID="ObjectDataSource1" HorizontalAlign="NotSet"
                        AutoGenerateColumns="false" AllowSorting="true" CommandItemDisplay="Top" CommandItemSettings-ShowRefreshButton="false"
                        CommandItemSettings-ShowAddNewRecordButton="false">
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Selected">
                                <ItemTemplate>
                                    <asp:CheckBox ID="rowCheckbox1" OnCheckedChanged="ToggleRowSelection1" AutoPostBack="True"
                                        runat="server" OnDataBinding="rowCheckBox1_OnDataBinding" Text="">
                                    </asp:CheckBox>
                                </ItemTemplate>
                                <HeaderStyle Width="70" />
                                <ItemStyle Width="70" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="Jobcodeid" UniqueName="JobcodeidColumn" ReadOnly="true"
                                Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Companydivisionid" UniqueName="CompanydivisionidColumn"
                                ReadOnly="true" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Customerid" UniqueName="CustomeridColumn" ReadOnly="true"
                                Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Code" HeaderText="Code"
                                UniqueName="CodeColumn" ReadOnly="true" Visible="true" AllowSorting="true">
                                <HeaderStyle Width="200" />
                                <ItemStyle Width="200" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Description" HeaderText="Description" 
                                UniqueName="DescriptionColumn" ReadOnly="true" Visible="true" AllowSorting="true">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <CommandItemSettings ShowExportToCsvButton="true" />
                        <AlternatingItemStyle BackColor="#F2F0F2" />
                    </MasterTableView>
                    <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false"
                        Csv-FileExtension="csv" />
                    <ClientSettings EnablePostBackOnRowClick="false">
                        <Selecting AllowRowSelect="true" />
                        <Scrolling AllowScroll="true" SaveScrollPosition="true" UseStaticHeaders="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <br />
            </div>
        </div>
        <div class="div_container">
            <div class="div_header" style=" position: relative; float: left">Manage Job Codes and Company / Customer Selections</div>
            <div style="position: relative; float: left; width: 49%">
                <asp:Label ID="lblTitle3" runat="server" SkinID="InfoLabelBold" Text="All Job Codes"></asp:Label>
                <telerik:RadGrid ID="grdJobs" GridLines="None"
                    runat="server" Height="300px" AllowAutomaticUpdates="true" AllowAutomaticInserts="true" AllowAutomaticDeletes="true"
                    AllowSorting="true" AllowFilteringByColumn="true" AutoGenerateColumns="false"
                    DataSourceID="LinqDataSource1" HorizontalAlign="Left"
                    OnItemUpdated="grdJobs_ItemUpdated" OnItemDeleted="grdJobs_ItemDeleted" OnItemInserted="grdJobs_ItemInserted"
                    OnDataBound="grdJobs_DataBound" CellSpacing="0">
                    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Jobcodeid,Description" DataSourceID="LinqDataSource1"
                        HorizontalAlign="Left" AllowAutomaticUpdates="true" AllowAutomaticInserts="true" AllowAutomaticDeletes="true"
                        AutoGenerateColumns="false" CommandItemSettings-ShowRefreshButton="false" EditMode="InPlace">
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                <HeaderStyle Width="60px" />
                                <ItemStyle Width="60px" />
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn DataField="Jobcodeid" UniqueName="JobcodeidColumn" ReadOnly="true"
                                Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="CodeColumn"
                                ReadOnly="false" Visible="true" AllowFiltering="true" AllowSorting="true"
                                ShowFilterIcon="false" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                <HeaderStyle Width="24%" />
                                <ItemStyle Width="98%" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Description" HeaderText="Description" UniqueName="DescriptionColumn"
                                ReadOnly="false" Visible="true" AllowFiltering="true" AllowSorting="true" 
                                ShowFilterIcon="false" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                <HeaderStyle Width="24%" />
                                <ItemStyle Width="98%" />
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn DataField="Requiresworkcard" HeaderText="Requires WC?" 
                                AllowFiltering="false" DefaultInsertValue="false">
                                <HeaderStyle HorizontalAlign="Center" Width="16%" />
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="98%" />
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridCheckBoxColumn DataField="Requirestailnumber" HeaderText="Requires Tail?"
                                AllowFiltering="false" DefaultInsertValue="false">
                                <HeaderStyle HorizontalAlign="Center" Width="16%" />
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="98%" />
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridCheckBoxColumn DataField="Isactive" HeaderText="Active?"
                                AllowFiltering="false" DefaultInsertValue="true">
                                <HeaderStyle HorizontalAlign="Center" Width="16%" />
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="98%" />
                            </telerik:GridCheckBoxColumn>
                            <%--<telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record?"
                                ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                                UniqueName="DeleteCommandColumn" CommandName="Delete">
                                <HeaderStyle Width="40px" />
                                <ItemStyle Width="40px" />
                            </telerik:GridButtonColumn>--%>
                        </Columns>
                        <CommandItemSettings ShowExportToCsvButton="true" />
                        <AlternatingItemStyle BackColor="#F2F0F2" />
                    </MasterTableView>
                    <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false"
                        Csv-FileExtension="csv" />
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <ClientEvents OnRowDblClick="RowDblClick" />
                        <Selecting AllowRowSelect="true" />
                        <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="false" />
                    </ClientSettings>
                </telerik:RadGrid>
                <br />
            </div>
            <div style="position: relative; float: left; width: 49%; margin-left: 5px">
                <asp:Label ID="lblTitle4" runat="server" SkinID="InfoLabelBold" Text="Selected Company Divisions / Customers"></asp:Label>
                <telerik:RadGrid ID="grdSelectedCompanyCusts" GridLines="None" runat="server"
                    Height="300px" AllowMultiRowSelection="false" AutoGenerateColumns="false" DataSourceID="ObjectDataSource2">
                    <MasterTableView DataKeyNames="Companydivisionid,Customerid,Jobcodeid" DataSourceID="ObjectDataSource2"
                        HorizontalAlign="NotSet" AutoGenerateColumns="false" AllowSorting="true" CommandItemDisplay="Top"
                        CommandItemSettings-ShowRefreshButton="false" CommandItemSettings-ShowAddNewRecordButton="false">
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Selected">
                                <ItemTemplate>
                                    <asp:CheckBox ID="rowCheckbox2" OnCheckedChanged="ToggleRowSelection2" AutoPostBack="True"
                                        runat="server" OnDataBinding="rowCheckBox2_OnDataBinding" Text="">
                                    </asp:CheckBox>
                                </ItemTemplate>
                                <HeaderStyle Width="70" />
                                <ItemStyle Width="70" HorizontalAlign="Center" VerticalAlign="Middle" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="Jobcodeid" UniqueName="JobcodeidColumn" ReadOnly="true"
                                Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Companydivisionid" UniqueName="CompanydivisionidColumn"
                                ReadOnly="true" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Customerid" UniqueName="CustomeridColumn" ReadOnly="true"
                                Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Companydescr" HeaderText="Company" UniqueName="CompanyDescrColumn"
                                ReadOnly="true" Visible="true" AllowSorting="true">
                                <HeaderStyle Width="250" />
                                <ItemStyle Width="250" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Customerdescr" HeaderText="Customer" UniqueName="CustomerDescrColumn"
                                ReadOnly="true" Visible="true" AllowSorting="true">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <CommandItemSettings ShowExportToCsvButton="true" />
                        <AlternatingItemStyle BackColor="#F2F0F2" />
                    </MasterTableView>
                    <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false"
                        Csv-FileExtension="csv" />
                    <ClientSettings EnablePostBackOnRowClick="false">
                        <Selecting AllowRowSelect="true" />
                        <Scrolling AllowScroll="true" SaveScrollPosition="true" UseStaticHeaders="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <br />
            </div>
        </div>
    </div>
</asp:Content>

