﻿using System;
using System.Web.UI;
using Telerik.Web.UI;
using System.Linq;

public partial class Admin_EditCustomers : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_CUST_SERV_SEC;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
        //m.IsActive = true;
        
        if (!Page.IsPostBack) {
            if (!Page.IsPostBack) {
                // Security.
                if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_GEN)) {
                    RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                    RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                    RadGrid1.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                    RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
                    RadGrid2.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                    RadGrid2.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                    RadGrid2.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                    RadGrid2.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
                    RadGrid3.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                    RadGrid3.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                    RadGrid3.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                    RadGrid3.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
                }
                // Customer Rate Column.
                if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_CUSTOMER_NONROUTINE_RATE)) {
                    RadGrid1.Columns.FindByUniqueName("NonRoutineRateColumn").Visible = false;
                }
            }
        }
    }
    protected void lstTheme_DataBound(object source, EventArgs e) {
        RadComboBox cmb = (RadComboBox)source;
        if (cmb != null) {
            cmb.Items.FindItem(x => x.Text == "Corporate").Remove();
        }
    }
    protected string GetFlatFeeOrHourlyText(string s) {
        if (!string.IsNullOrEmpty(s)) {
            if (s == "F") {
                return "Flat Fee";
            } else {
                return "Hourly";
            }
        } else {
            return "";
        }
    }
    protected void RadGrid1_SelectedIndexChanged(object sender, EventArgs e) {
        string custId = RadGrid1.SelectedValue.ToString();
        ObjectDataSource8.SelectParameters["CustomerId"].DefaultValue = custId;
        ObjectDataSource8.DataBind();
        RadGrid2.DataBind();
        // Set the key insert value.
        ((GridBoundColumn)RadGrid2.Columns.FindByUniqueName("CustomeridColumn")).DefaultInsertValue = RadGrid1.SelectedValue.ToString(); 
        if (RadGrid2.Items.Count > 0) {
            RadGrid2.Items[0].Selected = true;
            // Set the key insert value.
            ((GridBoundColumn)RadGrid3.Columns.FindByUniqueName("ServiceidColumn")).DefaultInsertValue = RadGrid2.SelectedValue.ToString();
        }
        RadGrid3.DataBind();
        if (RadGrid3.Items.Count > 0) {
            RadGrid3.Items[0].Selected = true;
        }
    }
    protected void RadGrid2_SelectedIndexChanged(object sender, EventArgs e) {
        RadGrid3.DataBind();
        // Set the key insert value.
        ((GridBoundColumn)RadGrid3.Columns.FindByUniqueName("ServiceidColumn")).DefaultInsertValue = RadGrid2.SelectedValue.ToString();
        if (RadGrid3.Items.Count > 0) {
            RadGrid3.Items[0].Selected = true;
        }
    }
    // GRID 1
    protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("Customerid").ToString();
        item.Selected = true;
        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            if (e.Exception.InnerException != null) {
                SetMessage("Customer " + id + " cannot be updated. Reason: " +
                    e.Exception.InnerException.Message);
            } else {
                SetMessage("Customer " + id + " cannot be updated. Reason: " +
                    e.Exception.Message);
            }
            DisplayMessage(gridMessage);
        } else {
            SetMessage("Customer " + id + " is updated!");
        }
    }
    protected void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e) {
        e.Item.Selected = true;
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("Customer cannot be inserted. Reason: " + e.Exception.InnerException.Message);
            DisplayMessage(gridMessage);
        } else {
            SetMessage("New Customer is inserted!");
        }
    }
    protected void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e) {
        GridDataItem dataItem = (GridDataItem)e.Item;
        String id = dataItem.GetDataKeyValue("Customerid").ToString();
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("Customer " + id + " cannot be deleted. Reason: " + 
                e.Exception.InnerException.Message);
            DisplayMessage(gridMessage);
        } else {
            SetMessage("Customer " + id + " is deleted!");
            DisplayMessage(gridMessage);
        }
    }
    protected void RadGrid1_DataBound(object sender, EventArgs e) {
        if ((RadGrid1.SelectedValue == null) && (RadGrid1.Items.Count > 0)) {
            RadGrid1.Items[0].Selected = true;
            RadGrid2.DataBind();
        }
        if (!string.IsNullOrEmpty(gridMessage)) {
            DisplayMessage(gridMessage);
        }
    }
    private string gridMessage = null;
    private void DisplayMessage(string text) {
        gridMessage = "";
        RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "RadGrid1");
        grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage(string message) {
        gridMessage = message;
    }
    // GRID 2
    protected void RadGrid2_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("Serviceid").ToString();
        item.Selected = true;
        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            if (e.Exception.InnerException != null) {
                SetMessage2("Service " + id + " cannot be updated. Reason: " +
                    e.Exception.InnerException.Message);
            } else {
                SetMessage2("Service " + id + " cannot be updated. Reason: " +
                    e.Exception.Message);
            }
            DisplayMessage2(gridMessage2);
        } else {
            SetMessage2("Customer " + id + " is updated!");
        }
    }
    protected void RadGrid2_ItemInserted(object source, GridInsertedEventArgs e) {
        e.Item.Selected = true;
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage2("Service cannot be inserted. Reason: " + e.Exception.InnerException.Message);
            DisplayMessage2(gridMessage2);
        } else {
            SetMessage2("New Service is inserted!");
        }
    }
    protected void RadGrid2_ItemDeleted(object source, GridDeletedEventArgs e) {
        GridDataItem dataItem = (GridDataItem)e.Item;
        String id = dataItem.GetDataKeyValue("Serviceid").ToString();
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage2("Service " + id + " cannot be deleted. Reason: " +
                e.Exception.InnerException.Message);
            DisplayMessage2(gridMessage2);
        } else {
            SetMessage2("Service " + id + " is deleted!");
            DisplayMessage2(gridMessage2);
        }
    }
    protected void RadGrid2_DataBound(object sender, EventArgs e) {
        if (!RadGrid1.MasterTableView.IsItemInserted) {
            string custId = RadGrid1.SelectedValue.ToString();
            ObjectDataSource8.SelectParameters["CustomerId"].DefaultValue = custId;
            ObjectDataSource8.DataBind();
            if (!string.IsNullOrEmpty(gridMessage2)) {
                DisplayMessage2(gridMessage2);
            }
        }
    }
    private string gridMessage2 = null;
    private void DisplayMessage2(string text) {
        gridMessage2 = "";
        RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "RadGrid2");
        grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage2(string message) {
        gridMessage2 = message;
    }
    // GRID 3
    protected void RadGrid3_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("Sectionid").ToString();
        item.Selected = true;
        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            if (e.Exception.InnerException != null) {
                SetMessage3("Section " + id + " cannot be updated. Reason: " +
                    e.Exception.InnerException.Message);
            } else {
                SetMessage3("Section " + id + " cannot be updated. Reason: " +
                    e.Exception.Message);
            }
            DisplayMessage3(gridMessage3);
        } else {
            SetMessage3("Section " + id + " is updated!");
        }
    }
    protected void RadGrid3_ItemInserted(object source, GridInsertedEventArgs e) {
        e.Item.Selected = true;
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage3("Section cannot be inserted. Reason: " + e.Exception.InnerException.Message);
            DisplayMessage3(gridMessage3);
        } else {
            SetMessage3("New Section is inserted!");
        }
    }
    protected void RadGrid3_ItemDeleted(object source, GridDeletedEventArgs e) {
        GridDataItem dataItem = (GridDataItem)e.Item;
        String id = dataItem.GetDataKeyValue("Sectionid").ToString();
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage3("Section " + id + " cannot be deleted. Reason: " +
                e.Exception.InnerException.Message);
            DisplayMessage3(gridMessage3);
        } else {
            SetMessage3("Section " + id + " is deleted!");
            DisplayMessage3(gridMessage3);
        }
    }
    protected void RadGrid3_DataBound(object sender, EventArgs e) {
        if (!string.IsNullOrEmpty(gridMessage3)) {
            DisplayMessage3(gridMessage3);
        }
    }
    private string gridMessage3 = null;
    private void DisplayMessage3(string text) {
        gridMessage3 = "";
        RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "RadGrid3");
        grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage3(string message) {
        gridMessage3 = message;
    }
}