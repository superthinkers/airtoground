﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    CodeFile="EditEquipment.aspx.cs" Inherits="Admin_EditEquipment" Buffer="true"
    Strict="true" Explicit="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <devart:DbLinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Equipment" OrderBy="Customer.Description, Tailnumber" TableName="Equipments"
        EnableInsert="true" EnableUpdate="true" EnableDelete="true">
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Customer" OrderBy="Description" TableName="Customers">
    </devart:DbLinqDataSource>
    <devart:DbLinqDataSource ID="LinqDataSource3" runat="server" ContextTypeName="ATGDB.ATGDataContext"
        EntityTypeName="ATGDB.Equipmenttype" OrderBy="Description" TableName="Equipmenttypes">
    </devart:DbLinqDataSource>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="true"
        Skin="Office2007">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadWindowManager ID="RadWindowManager1" Style="z-index: 8000 !important" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true" Modal="true" VisibleTitlebar="false" AutoSize="true"> 
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" NavigateUrl="~/Admin/Equipment/EquipmentStatus.aspx"
                Behaviors="Close" OnClientClose="onClientClose1" VisibleTitlebar="false" Title="Suspend Aircraft">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" NavigateUrl="~/Admin/Equipment/EquipmentHold.aspx"
                Behaviors="Close" OnClientClose="onClientClose2" VisibleTitlebar="false" Title="Suspend Aircraft">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" NavigateUrl="~/Admin/Equipment/EquipmentUnHold.aspx"
                Behaviors="Close" OnClientClose="onClientClose3" VisibleTitlebar="false" Title="Release Aircraft">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadCodeBlock id="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToPdfButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }
            var eqTypeCombo;
            function doEQTypeLoad(sender) {
                //alert(sender);
                eqTypeCombo = sender;
            }
            function doSelectCustomer(sender) {
                //alert("here");
                eqTypeCombo.requestItems(sender.get_value());
            }
            function openStatus(eqId) {
                radopen("Equipment/EquipmentStatus.aspx?eqId=" + eqId, "RadWindow1");
            }
            function openHold(eqId) {
                radopen("Equipment/EquipmentHold.aspx?eqId=" + eqId, "RadWindow2");
            }
            function openUnHold(eqId) {
                radopen("Equipment/EquipmentUnHold.aspx?eqId=" + eqId, "RadWindow3");
            }
            function onClientClose1(oWnd, args) {
                // Do Nothing.
                //<%= PostBackString %>
            }
            function onClientClose2(oWnd, args) {
                var arg = args.get_argument();
                if (arg && (arg.OK == "TRUE")) {
                    // By setting the trigger field here to ssId, it shouldn't fire too often.
                    var hidden = $get("<%= hiddenStatusTrigger.ClientID %>");
                    hidden.value = arg.StatusLogId;

                    hidden = $get("<%= hiddenEqId.ClientID %>");
                    hidden.value = arg.EqId;

                    hidden = $get("<%= hiddenStatusLogId.ClientID %>");
                    hidden.value = arg.StatusLogId;

                    hidden = $get("<%= hiddenServices.ClientID %>");
                    //hidden.value = arg.Services;

                    hidden = $get("<%= hiddenStatus.ClientID %>");
                    hidden.value = arg.Status;

                    hidden = $get("<%= hiddenStatusReason.ClientID %>");
                    hidden.value = arg.StatusReason;

                    hidden = $get("<%= hiddenStatusDate.ClientID %>");
                    hidden.value = arg.StatusDate;

                    hidden = $get("<%= hiddenStatusNotes.ClientID %>");
                    hidden.value = arg.StatusNotes;

                    hidden = $get("<%= hiddenStatusBy.ClientID %>");
                    hidden.value = arg.StatusBy;

                    hidden = $get("<%= hiddenReturnDate.ClientID %>");
                    hidden.value = arg.ReturnDate;

                    hidden = $get("<%= hiddenUnHoldDate.ClientID %>");
                    hidden.value = "";//arg.UnHoldDate;

                    hidden = $get("<%= hiddenUnHoldBy.ClientID %>");
                    hidden.value = "";//arg.UnHoldBy;

                    hidden = $get("<%= hiddenUnHoldNotes.ClientID %>");
                    hidden.value = "";//arg.UnHoldNotes;
                
                    <%= PostBackString %>
                }
            }
            function onClientClose3(oWnd, args) {
                var arg = args.get_argument();
                if (arg && (arg.OK == "TRUE")) {
                    // By setting the trigger field here to ssId, it shouldn't fire too often.
                    var hidden = $get("<%= hiddenStatusTrigger.ClientID %>");
                    hidden.value = arg.StatusLogId;

                    hidden = $get("<%= hiddenEqId.ClientID %>");
                    hidden.value = arg.EqId;

                    hidden = $get("<%= hiddenStatusLogId.ClientID %>");
                    hidden.value = arg.StatusLogId;

                    hidden = $get("<%= hiddenServices.ClientID %>");
                    hidden.value = arg.Services;

                    hidden = $get("<%= hiddenStatus.ClientID %>");
                    hidden.value = "";//arg.Status;

                    hidden = $get("<%= hiddenStatusReason.ClientID %>");
                    hidden.value = "";//arg.StatusReason;

                    hidden = $get("<%= hiddenStatusDate.ClientID %>");
                    hidden.value = "";//arg.StatusDate;

                    hidden = $get("<%= hiddenStatusNotes.ClientID %>");
                    hidden.value = "";//arg.StatusNotes;

                    hidden = $get("<%= hiddenStatusBy.ClientID %>");
                    hidden.value = "";//arg.StatusBy;

                    hidden = $get("<%= hiddenUnHoldDate.ClientID %>");
                    hidden.value = arg.UnHoldDate;

                    hidden = $get("<%= hiddenUnHoldBy.ClientID %>");
                    hidden.value = arg.UnHoldBy;

                    hidden = $get("<%= hiddenUnHoldNotes.ClientID %>");
                    hidden.value = arg.UnHoldNotes;
                
                    <%= PostBackString %>
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <asp:HiddenField ID="hiddenStatusTrigger" runat="server" OnValueChanged="Status_ValueChanged" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenEqId" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenStatusLogId" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenStatus" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenStatusReason" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenStatusDate" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenStatusBy" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenReturnDate" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenStatusNotes" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenUnHoldDate" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenUnHoldBy" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenUnHoldNotes" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <asp:HiddenField ID="hiddenServices" runat="server" Value="&nbsp;" ClientIDMode="Static" />
    <div id="div_main">
        <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Aircraft"></asp:Label>
        <br />
        <br />
        <div class="div_container">
            <div class="div_header">Aircraft</div>
            <div style="width: 100%;">
                <telerik:RadGrid ID="RadGrid1" GridLines="None"
                    runat="server" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                    AllowPaging="False" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="LinqDataSource1"
                    OnItemUpdated="RadGrid1_ItemUpdated" OnItemDeleted="RadGrid1_ItemDeleted" OnItemInserted="RadGrid1_ItemInserted"
                    OnDataBound="RadGrid1_DataBound" OnItemDataBound="RadGrid1_ItemDataBound" OnItemCommand="RadGrid1_ItemCommand"
                    AllowMultiRowEdit="false" AllowFilteringByColumn="true" AllowMultiRowSelection="false" Width="100%">
                    <MasterTableView CommandItemDisplay="Top" DataKeyNames="Equipmentid" DataSourceID="LinqDataSource1"
                        HorizontalAlign="NotSet" AutoGenerateColumns="False" EditMode="InPlace" CommandItemSettings-ShowRefreshButton="false" 
                        AllowFilteringByColumn="true" Width="100%" AllowPaging="true" PageSize="15">
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                <HeaderStyle Width="60px" />
                                <ItemStyle Width="60px" />
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn DataField="Equipmentid" HeaderText="Id" AllowSorting="false"
                                UniqueName="EquipmentidColumn" ItemStyle-ForeColor="Black"
                                ReadOnly="true" AllowFiltering="false" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Equipmentstatuslogid" HeaderText="Log Id" AllowSorting="false"
                                UniqueName="EquipmentstatuslogidColumn" ItemStyle-ForeColor="Black"
                                ReadOnly="true" AllowFiltering="false" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Equipmentstatuslog.Statusdate" HeaderText="Status Date"
                                AllowSorting="false" UniqueName="StatusdateColumn" ItemStyle-ForeColor="Black"
                                ReadOnly="true" AllowFiltering="false" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Equipmentstatuslog.Unholddate" HeaderText="Release Date" AllowSorting="false"
                                UniqueName="UnholddateColumn" ItemStyle-ForeColor="Black" ReadOnly="true"
                                AllowFiltering="false" Visible="false">
                            </telerik:GridBoundColumn>
                            <filters:TextBoxFilterGridTemplateColumn HeaderText="Tail Number" DataField="Tailnumber"
                                UniqueName="TailnumberColumn" SortExpression="Tailnumber" FilterControlWidth="98%">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblTailNumber" Text='<%# Eval("TailNumber") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtTailNumber" runat="server" MaxLength="30" Text='<%# Bind("TailNumber") %>'
                                        Width="98%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtTailNumber"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator"
                                        Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <HeaderStyle Width="120px" />
                                <ItemStyle Width="120px" />
                            </filters:TextBoxFilterGridTemplateColumn>
                            <filters:TextBoxFilterGridTemplateColumn HeaderText="Description" DataField="Description"
                                UniqueName="DescriptionColumn" SortExpression="Description" FilterControlWidth="98%">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblDescription" Text='<%# Eval("Description") %>' ></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDescription" runat="server" MaxLength="100" Text='<%# Bind("Description") %>' Width="98%" ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtDescription"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator"
                                        Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <HeaderStyle Width="120px" />
                                <ItemStyle Width="120px" />
                            </filters:TextBoxFilterGridTemplateColumn>
                            <filters:CustomerFilterGridTemplateColumn HeaderText="Customer" DataField="Customer.Description"
                                UniqueName="CustomeridColumn" SortExpression="Customer.Description" FilterControlWidth="98%">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblCustomer" Text='<%# Eval("Customer.Description") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <telerik:RadComboBox ID="lstCustomer" runat="server"
                                        DataSourceID="LinqDataSource2"
                                        DataTextField="Description" DataValueField="Customerid" SelectedValue='<%# Bind("Customerid") %>'
                                        Width="98%"
                                        AutoPostBack="false"
                                        CausesValidation="false" OnDataBound="lstCustomer_DataBound"
                                        OnClientSelectedIndexChanged="doSelectCustomer">
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="lstCustomer"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator"
                                        Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <HeaderStyle Width="210px" />
                                <ItemStyle Width="210px" />
                            </filters:CustomerFilterGridTemplateColumn>
                            <filters:EquipmentTypeFilterGridTemplateColumn HeaderText="Aircraft Type" DataField="Equipmenttype.Description"
                                UniqueName="EquipmenttypeidColumn" SortExpression="Equipmenttype.Description"
                                FilterControlWidth="98%">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblEquipmenttype" Text='<%# Eval("Equipmenttype.Description") %>'></asp:Label>
                                </ItemTemplate>    
                                <EditItemTemplate>
                                    <telerik:RadComboBox ID="lstEquipmenttype" runat="server"
                                        DataTextField="Description"
                                        DataValueField="Equipmenttypeid" Width="98%"
                                        OnClientLoad="doEQTypeLoad" OnDataBound="lstEqType_DataBound"
                                        OnItemsRequested="lstEquipmenttype_OnItemsRequested"
                                        SelectedValue='<%# Bind("Equipmenttypeid") %>'>
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="lstEquipmenttype"
                                        ErrorMessage="<br />Required" runat="server" CssClass="stdValidator"
                                        Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <HeaderStyle Width="150px" />
                                <ItemStyle Width="150px" />
                            </filters:EquipmentTypeFilterGridTemplateColumn>
                            <telerik:GridTemplateColumn DataField="Notes" HeaderText="Notes" UniqueName="NotesColumn"
                                AllowFiltering="false" CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblNotes" Text='<%# Eval("Notes") %>' ></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtNotes" runat="server" MaxLength="255" Text='<%# Bind("Notes") %>' Width="98%"></asp:TextBox>
                                </EditItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="StatusColumn" AllowFiltering="false">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <a href="#" onclick='<%# "openStatus(" + Eval("Equipmentid").ToString() + "); return false;" %>'>
                                        Status</a>
                                </ItemTemplate>
                                <HeaderStyle Width="48px" />
                                <ItemStyle Width="48px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="HoldColumn" AllowFiltering="false"> 
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <a href="#" onclick='<%# "openHold(" + Eval("Equipmentid").ToString() + "); return false;" %>'>
                                        Hold</a>
                                </ItemTemplate>
                                <HeaderStyle Width="40px" />
                                <ItemStyle Width="40px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="UnHoldColumn" AllowFiltering="false">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <a href="#" onclick='<%# "openUnHold(" + Eval("Equipmentid").ToString() + "); return false;" %>'>
                                        Release</a>
                                </ItemTemplate>
                                <HeaderStyle Width="60px" />
                                <ItemStyle Width="60px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridButtonColumn ConfirmText="Are You Sure You Want to Delete This Record?"
                                ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record?" ButtonType="ImageButton"
                                UniqueName="DeleteCommandColumn" CommandName="Delete">
                                <HeaderStyle Width="40px" />
                                <ItemStyle Width="40px" />
                            </telerik:GridButtonColumn>
                        </Columns>
                        <CommandItemSettings ShowExportToCsvButton="true" />
                        <AlternatingItemStyle BackColor="#F2F0F2" />
                        <PagerStyle AlwaysVisible="true" Mode="NumericPages" Position="Bottom" />
                    </MasterTableView>
                    <ExportSettings OpenInNewWindow="true" HideStructureColumns="false" ExportOnlyData="false"
                        Csv-FileExtension="csv" />
                    <ClientSettings>
                        <ClientEvents OnRowDblClick="RowDblClick"/>
                        <Selecting AllowRowSelect="true" />
                        <Scrolling AllowScroll="false" SaveScrollPosition="false" UseStaticHeaders="false" />
                    </ClientSettings>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
</asp:Content>
