﻿using System;
using System.Web.UI;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

public partial class Admin_EditServices : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_SERVICES_RULES;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Devart.Data.MySql.MySqlMonitor m = new Devart.Data.MySql.MySqlMonitor();
        //m.IsActive = true;
        if (!Page.IsPostBack) {
            // Security.
            if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_GEN)) {
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
                RadGrid2.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                RadGrid2.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                RadGrid2.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                RadGrid2.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
                RadGrid3.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                RadGrid3.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                RadGrid3.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                RadGrid3.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
            }
        } else {
            // CAUSES detail table not to expand!!!!!!
            //SetParams();
        }
    }
    protected void SetParams() {
        // Where statements.
        string sCWhere = "(Customerid == Convert.ToInt32(@Customerid))";
        string sDESCRWhere = "(Description.ToUpper().Contains(@Description.ToUpper()))";

        RadComboBox lstCustomerFilter = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1, "lstCustomerFilter");
        RadTextBox txtDescriptionFilter = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1, "txtDescriptionFilter");

        // Clear first.
        LinqDataSource1.WhereParameters.Clear();

        // Customer Filter.
        string id = lstCustomerFilter.SelectedValue;
        if (int.Parse(id) > 0) {
            if (LinqDataSource1.WhereParameters["Customerid"] == null) {
                LinqDataSource1.WhereParameters.Add("Customerid", System.Data.DbType.Int32, id);
            }
        }

        // Description Filter.
        id = txtDescriptionFilter.Text;
        if (!String.IsNullOrEmpty(id)) {
            if (LinqDataSource1.WhereParameters["Description"] == null) {
                LinqDataSource1.WhereParameters.Add("Description", System.Data.DbType.String, id);
            }
        }

        // Now, based on existing where parameters, build the where statement.
        // Clear first.
        LinqDataSource1.Where = "";

        // Customer
        if (LinqDataSource1.WhereParameters["Customerid"] != null) {
            LinqDataSource1.Where = sCWhere;
        }

        // Description
        if (LinqDataSource1.WhereParameters["Description"] != null) {
            if (LinqDataSource1.Where == "") {
                LinqDataSource1.Where = sDESCRWhere;
            } else {
                LinqDataSource1.Where = LinqDataSource1.Where + " && " + sDESCRWhere;
            }
        }
    }
    // GRID 1
    //private int lastIndex = 0;
    protected void lstCustomer_DataBound(object sender, EventArgs e) {
        RadComboBox lst = (RadComboBox)sender;
        if ((lst.Items.Count > 0) && (lst.Items[0].Value != "")) {
            RadComboBoxItem item = new RadComboBoxItem("", "");
            lst.Items.Insert(0, item);
        }
    }
    protected void lstCustomer_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e) {
        if ((RadGrid1.EditItems.Count > 0) || (RadGrid1.MasterTableView.IsItemInserted)) {
            //GridDataItem item = (GridDataItem)((RadComboBox)sender).NamingContainer;
            GridDropDownListColumnEditor jobCodeBox = null;// (GridDropDownListColumnEditor)item.EditManager.GetColumnEditor("JobcodeidColumn");
            RadComboBox custBox = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1, "lstCustomer");
            if ((jobCodeBox != null) && (custBox != null) && (custBox.SelectedValue != "")) {
                int custId = int.Parse(custBox.SelectedValue.ToString());
                ObjectDataSource8.SelectParameters["CustomerId"].DefaultValue = custId.ToString();
                jobCodeBox.ComboBoxControl.Items.Clear();
                jobCodeBox.ComboBoxControl.DataBind();
            }
        }
    }
    protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("Serviceid").ToString();
        e.Item.Selected = true;
        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            if (e.Exception.Message.ToUpper().Contains("A MEMBER DEFINING THE IDENTITY")) {
                SetMessage("You cannot update the Section. Delete and add a New Section.");
            } else {
                if (e.Exception.InnerException == null) {
                    SetMessage("Service " + id + " cannot be updated. Reason: " +
                        e.Exception.Message);
                } else {
                    SetMessage("Service " + id + " cannot be updated. Reason: " +
                        e.Exception.InnerException.Message);
                }
            }
            DisplayMessage(gridMessage);
        } else {
            SetMessage("Service " + id + " is updated!");
        }
    }
    protected void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e) {
        e.Item.Selected = true;
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("Service cannot be inserted. Reason: " + e.Exception.InnerException.Message);
            DisplayMessage(gridMessage);
        } else {
            SetMessage("New Service is inserted!");
        }
    }
    protected void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e) {
        GridDataItem dataItem = (GridDataItem)e.Item;
        String id = dataItem.GetDataKeyValue("Serviceid").ToString();
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("Service " + id + " cannot be deleted. Reason: " +
                e.Exception.InnerException.Message);
            DisplayMessage(gridMessage);
        } else {
            SetMessage("Service " + id + " is deleted!");
            DisplayMessage(gridMessage);
        }
    }
    protected void RadGrid1_DataBound(object sender, EventArgs e) {
        if (RadGrid1.EditItems.Count == 1) {
            RadComboBox custBox = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1, "lstCustomer");
            if ((custBox != null) && (custBox.SelectedValue != "")) {
                int custId = int.Parse(custBox.SelectedValue.ToString());
                ObjectDataSource8.SelectParameters["CustomerId"].DefaultValue = custId.ToString();
            }
        }
        if (!string.IsNullOrEmpty(gridMessage)) {
            DisplayMessage(gridMessage);
        }
    }
    private string gridMessage = null;
    private void DisplayMessage(string text) {
        gridMessage = "";
        //RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "RadGrid1");
        //grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage(string message) {
        gridMessage = message;
    }
    // CUSTOM FILTERING.
    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e) {
        // Based on filter, add where parameters, or remove them.
        if (/*(e.CommandName == "Edit") ||       
            (e.CommandName == "Update") ||
            (e.CommandName == "Delete") ||
            (e.CommandName == "InitInsert") ||    
            (e.CommandName == "PerformInsert") || 
            (e.CommandName == "Cancel") ||*/ // Causes problems with sub-grid
            (e.CommandName == "DoCustomerFilter") ||
            (e.CommandName == "DoDescriptionFilter")) {

            // Finally, set parms to refresh. NO DATABIND.
            SetParams(); 
        }
    }
    protected string GetFlatFeeOrHourlyText(string s) {
        if (!string.IsNullOrEmpty(s)) {
            if (s == "F") {
                return "Flat Fee";
            } else {
                return "Hourly";
            }
        } else {
            return "";
        }
    }
    // GRID 2
    protected void RadGrid2_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("Sectionid").ToString();
        item.Selected = true;
        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            if (e.Exception.InnerException != null) {
                SetMessage2("Section " + id + " cannot be updated. Reason: " +
                    e.Exception.InnerException.Message);
            } else {
                SetMessage2("Section " + id + " cannot be updated. Reason: " +
                    e.Exception.Message);
            }
            DisplayMessage2(gridMessage2);
        } else {
            SetMessage2("Section " + id + " is updated!");
        }
    }
    protected void RadGrid2_ItemInserted(object source, GridInsertedEventArgs e) {
        e.Item.Selected = true;
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage2("Section cannot be inserted. Reason: " + e.Exception.InnerException.Message);
            DisplayMessage2(gridMessage2);
        } else {
            SetMessage2("New Section is inserted!");
        }
    }
    protected void RadGrid2_ItemDeleted(object source, GridDeletedEventArgs e) {
        GridDataItem dataItem = (GridDataItem)e.Item;
        String id = dataItem.GetDataKeyValue("Sectionid").ToString();
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage2("Section " + id + " cannot be deleted. Reason: " +
                e.Exception.InnerException.Message);
            DisplayMessage2(gridMessage2);
        } else {
            SetMessage2("Section " + id + " is deleted!");
            DisplayMessage2(gridMessage2);
        }
    }
    protected void RadGrid2_DataBound(object sender, EventArgs e) {
        if (RadGrid1.SelectedValue != null) {
            ((GridBoundColumn)RadGrid2.Columns.FindByUniqueName("ServiceidColumn")).DefaultInsertValue = RadGrid1.SelectedValue.ToString();
        }
        if (!string.IsNullOrEmpty(gridMessage2)) {
            DisplayMessage2(gridMessage2);
        }
    }
    private string gridMessage2 = null;
    private void DisplayMessage2(string text) {
        gridMessage2 = "";
        RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "RadGrid2");
        grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage2(string message) {
        gridMessage2 = message;
    }
    // GRID 3
    protected string GetRuleActionText(string s) {
        if (!string.IsNullOrEmpty(s)) {
            if (s == "C2") {
                return "CLOSE the Exception, COMPLETE the Trigger Seq SS";
            } else if (s == "C1") {
                return "COMPLETE the Exception, COMPLETE the Trigger Seq SS";
            } else if (s == "C3") {
                return "COMPLETE the Exception, CANCEL the Trigger Seq SS";
            } else if (s == "AS") {
                return "AUTO-SCHEDULE Next Service";
            } else {
                return "";
            }
        } else {
            return "";
        }
    }
    protected void RadGrid3_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("Servicesectionruleid").ToString();
        item.Selected = true;
        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            if (e.Exception.InnerException != null) {
                SetMessage3("Service Rule " + id + " cannot be updated. Reason: " +
                    e.Exception.InnerException.Message);
            } else {
                SetMessage3("Service Rule " + id + " cannot be updated. Reason: " +
                    e.Exception.Message);
            }
            DisplayMessage3(gridMessage3);
        } else {
            SetMessage3("Service Rule " + id + " is updated!");
        }
    }
    protected void RadGrid3_ItemInserted(object source, GridInsertedEventArgs e) {
        e.Item.Selected = true;
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage3("Service Rule cannot be inserted. Reason: " + e.Exception.InnerException.Message);
            DisplayMessage3(gridMessage3);
        } else {
            SetMessage3("New Service Rule is inserted!");
        }
    }
    protected void RadGrid3_ItemDeleted(object source, GridDeletedEventArgs e) {
        GridDataItem dataItem = (GridDataItem)e.Item;
        String id = dataItem.GetDataKeyValue("Servicesectionruleid").ToString();
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage3("Service Rule " + id + " cannot be deleted. Reason: " +
                e.Exception.InnerException.Message);
            DisplayMessage3(gridMessage3);
        } else {
            SetMessage3("Service Rule " + id + " is deleted!");
            DisplayMessage3(gridMessage3);
        }
    }
    protected void RadGrid3_DataBound(object sender, EventArgs e) {
        if (RadGrid1.SelectedValue != null) {
            ((GridBoundColumn)RadGrid3.Columns.FindByUniqueName("ServiceidColumn")).DefaultInsertValue = RadGrid1.SelectedValue.ToString();
        }
        if (!string.IsNullOrEmpty(gridMessage3)) {
            DisplayMessage3(gridMessage3);
        }
    }
    private string gridMessage3 = null;
    private void DisplayMessage3(string text) {
        gridMessage3 = "";
        RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "RadGrid3");
        grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage3(string message) {
        gridMessage3 = message;
    }
    protected void reqSection_OnServerValidate(object sender, ServerValidateEventArgs e) {
        RadComboBox cmb = (RadComboBox)PageUtils.FindControlRecursive(RadGrid3.MasterTableView, "lstRuleAction");
        CustomValidator val = (CustomValidator)PageUtils.FindControlRecursive(RadGrid3.MasterTableView, "reqSection");
        if (cmb.SelectedValue == "AS") { // AUTOSCHEDULE;
            e.IsValid = e.Value == "AUTO SCHEDULE ONLY";
            val.ErrorMessage = "Must Choose AUTO SCHEDULE ONLY";
        } else {                                                    
            e.IsValid = e.Value != "AUTO SCHEDULE ONLY";
            val.ErrorMessage = "Wrong Selection, Choose Another";
        }
    }
    protected void RadGrid1_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e) {
        GridDataItem item = e.DetailTableView.ParentItem;
        string custId = item.GetDataKeyValue("Customerid").ToString();
        LinqDataSource44.WhereParameters["Customerid"].DefaultValue = custId;
        LinqDataSource44.DataBind();

        ObjectDataSource8.SelectParameters["CustomerId"].DefaultValue = custId.ToString();
        ObjectDataSource8.DataBind();
        //RadComboBox jobCodeBox = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1, "lstJobCode");
        //if (jobCodeBox != null) {
        //    jobCodeBox.Items.Clear();
        //    ObjectDataSource8.DataBind();
        //}
    }
}