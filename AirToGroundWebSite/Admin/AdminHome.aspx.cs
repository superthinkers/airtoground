﻿using System;

public partial class Admin_AdminHome : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_HOME;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // Security setup.
        // CODE TABLES
        if (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_CODETABLES) ||
            ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN)) {
            lnkCompanyDivisions1.Enabled = true;
            lnkCompanyDivisions2.Enabled = lnkCompanyDivisions1.Enabled;
            lnkCustomers1.Enabled = true;
            lnkCustomers2.Enabled = lnkCustomers1.Enabled;
            lnkDivisionAreas1.Enabled = true;
            lnkDivisionAreas2.Enabled = lnkDivisionAreas1.Enabled;
            lnkEquipment1.Enabled = true;
            lnkEquipment2.Enabled = lnkEquipment1.Enabled;
            lnkEquipmentTypes1.Enabled = true;
            lnkEquipmentTypes2.Enabled = lnkEquipmentTypes1.Enabled;
            lnkServices1.Enabled = true;
            lnkServices2.Enabled = lnkServices1.Enabled;
            lnkServiceShortDescr1.Enabled = true;
            lnkServiceShortDescr2.Enabled = lnkServices1.Enabled;
            lnkServiceCats1.Enabled = true;
            lnkServiceCats2.Enabled = true;
            lnkSections1.Enabled = true;
            lnkSections2.Enabled = lnkSections1.Enabled;
            lnkJobs1.Enabled = true;
            lnkJobs2.Enabled = lnkJobs1.Enabled;
            lnkRates1.Enabled = true;
            lnkRates2.Enabled = lnkRates1.Enabled;
            lblSecurity.Visible = false;
            lblDivisionTables.Visible = false;
        } else {
            lnkCompanyDivisions1.Enabled = false;
            lnkCompanyDivisions2.Enabled = false;
            lnkCustomers1.Enabled = false;
            lnkCustomers2.Enabled = false;
            lnkDivisionAreas1.Enabled = false;
            lnkDivisionAreas2.Enabled = false;
            lnkEquipment1.Enabled = false;
            lnkEquipment2.Enabled = false;
            lnkEquipmentTypes1.Enabled = false;
            lnkEquipmentTypes2.Enabled = false;
            lnkServices1.Enabled = false;
            lnkServices2.Enabled = false;
            lnkServiceShortDescr1.Enabled = false;
            lnkServiceShortDescr2.Enabled = lnkServices1.Enabled;
            lnkServiceCats1.Enabled = false;
            lnkServiceCats2.Enabled = false;
            lnkSections1.Enabled = false;
            lnkSections2.Enabled = false;
            lnkJobs1.Enabled = false;
            lnkJobs2.Enabled = lnkJobs1.Enabled;
            lnkRates1.Enabled = false;
            lnkRates2.Enabled = lnkRates1.Enabled;
            lblSecurity.Visible = true;
            lblDivisionTables.Visible = true;
        }
        
        // USER SECURITY
        if (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_USERSECURITY) ||
            ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN)) {
            lblUserSecurity.Visible = false;
            lnkUserRoles1.Enabled = true;
            lnkUserRoles2.Enabled = true;
        } else {
            lblUserSecurity.Visible = true;
            lnkUserRoles1.Enabled = false;
            lnkUserRoles2.Enabled = false;
        }

        // CHANGE DIVISION SECURITY
        //if (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_CORP) || 
        //    ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN)) {
        //    lblDivisionSecurityUtils.Visible = false;
        //} else {
        //    lblDivisionSecurityUtils.Visible = true;
        //}
    }
}