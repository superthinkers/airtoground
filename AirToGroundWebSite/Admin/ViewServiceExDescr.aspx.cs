﻿using System;
using System.Linq;

public partial class Admin_ViewServiceExDescr : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_SERVICE_EX_DESCR_VIEW;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);

        if (!Page.IsPostBack) {
            if (Request.QueryString["ServiceId"] != null) {
                // Load the Extended Description for the service.
                string serviceId = Request.QueryString["ServiceId"];
                ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
                ATGDB.Service s = db.Services.SingleOrDefault(x => x.Serviceid == int.Parse(serviceId));
                if (s != null) {
                    txtExDescr.Enabled = true;
                    txtExDescr.Text = s.Exdescription;
                    lblService.Text = s.Description;
                } else {
                    txtExDescr.Text = "Error Occurred While Loading Service Description. Close and Try Again";
                    txtExDescr.Enabled = false;
                    lblService.Text = "";
                }
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e) {
        // Save the text.
        string serviceId = Request.QueryString["ServiceId"];
        ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
        ATGDB.Service s = db.Services.SingleOrDefault(x => x.Serviceid == int.Parse(serviceId));
        if (s != null) {
            s.Exdescription = txtExDescr.Text;
            db.SubmitChanges();
        }
    }
}