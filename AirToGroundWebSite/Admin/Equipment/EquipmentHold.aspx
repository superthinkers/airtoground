﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EquipmentHold.aspx.cs" Inherits="Equipment_EquipmentHold"
    Buffer="true" Strict="true" Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function doComplete() {
                // Create the argument that will be returned to the parent page
                var oArg = new Object();

                // Let's the calling form know the user is committing the changes, versus cancelling.
                oArg.OK = "TRUE";

                // Get a reference to the current RadWindow
                var oWnd = GetRadWindow();

                // Assign args.
                var hidden = $get("<%= hiddenStatusLogId.ClientID %>");
                oArg.StatusLogId = hidden.value;

                hidden = $get("<%= hiddenEqId.ClientID %>");
                oArg.EqId = hidden.value;

                var txtStatusDate = $find("<%= txtStatusDate.ClientID %>");
                if (txtStatusDate.get_selectedDate()) {
                    oArg.StatusDate = txtStatusDate.get_selectedDate().toLocaleString();
                } else {
                    oArg.StatusDate = "";
                }

                var lstStatus = $find("<%= lstStatus.ClientID %>");
                if (lstStatus.get_value()) {
                    oArg.Status = lstStatus.get_value();
                } else {
                    oArg.Status = "";
                }

                var lstReason = $find("<%= lstReason.ClientID %>");
                if (lstReason.get_value()) {
                    oArg.StatusReason = lstReason.get_value();
                } else {
                    oArg.StatusReason = "";
                }

                var lblBy = $get("<%= lblBy.ClientID %>");
                if (lblBy.innerHTML) {
                    oArg.StatusBy = lblBy.innerHTML;
                } else {
                    oArg.StatusBy = "";
                }

                var dtReturnDate = $find("<%= dtReturnDate.ClientID %>");
                if (dtReturnDate.get_selectedDate()) {
                    oArg.ReturnDate = dtReturnDate.get_selectedDate().toLocaleString();
                } else {
                    oArg.ReturnDate = "";
                }

                if (document.getElementById("txtNotes").value) {
                    oArg.StatusNotes = document.getElementById("txtNotes").value;
                } else {
                    oArg.StatusNotes = "";
                }

                // Validation.
                if ((oArg.StatusDate != "") && (oArg.Status != "") && (oArg.StatusReason != "")) {
                    oWnd.close(oArg);
                } else {
                    alert("The Status Date, Status, and Reason are Required.");
                }
            }
            function doCancel() {
                var oWnd = GetRadWindow();
                var oArg = new Object();
                oArg.OK = "FALSE";
                oWnd.close(oArg);
            }
        </script>
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" AsyncPostBackTimeout="600"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin" ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <asp:HiddenField ID="hiddenEqId" runat="server" Value="" />
        <asp:HiddenField ID="hiddenStatusLogId" runat="server" Value="" />
        <div id="mainDiv" runat="server" style="padding: 10px; width: 480px; height: 360px;">
            <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Suspend Aircraft"></asp:Label>
            <br />
            <br />
            <div style="position: relative; border: 1px solid Navy; width: 100%;">
                <table style="border-collapse: separate; width: 100%">
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="lblCustomerLbl" runat="server" SkinID="InfoLabel" Text="Customer:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCustomer" runat="server" SkinID="InfoLabelBold" Text="Customer"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="lblTailNumberLbl" runat="server" SkinID="InfoLabel" Text="Tail Number:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblTailNumber" runat="server" SkinID="InfoLabelBold" Text="Tail Number"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="lblEqTypeLbl" runat="server" SkinID="InfoLabel" Text="Type:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblEqType" runat="server" SkinID="InfoLabelBold" Text="Type"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="lblDescrLbl" runat="server" SkinID="InfoLabel" Text="Description:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblDescr" runat="server" SkinID="InfoLabelBold" Text="Description"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div style="position: relative; border: 1px solid Navy; width: 100%;">
                <table style="border-collapse: separate; width: 100%;">
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="lblStatusDate" runat="server" SkinID="InfoLabel" Text="Status Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadDatePicker ID="txtStatusDate" runat="server">
                            </telerik:RadDatePicker>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="lblStatus" runat="server" Text="Status:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadComboBox ID="lstStatus" runat="server">
                                <Items>
                                    <telerik:RadComboBoxItem Value="Indefinite Suspension" Text="Indefinite Suspension" />
                                    <telerik:RadComboBoxItem Value="Temporary Suspension" Text="Temporary Suspension" />
                                    <telerik:RadComboBoxItem Value="Terminated" Text="Terminated" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="Label1" runat="server" Text="Reason:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadComboBox ID="lstReason" runat="server">
                                <Items>
                                    <telerik:RadComboBoxItem Value="Location" Text="Location" />
                                    <telerik:RadComboBoxItem Value="Other" Text="Other" />
                                    <telerik:RadComboBoxItem Value="Painted" Text="Painted" />
                                    <telerik:RadComboBoxItem Value="Scheduled Maintenance" Text="Scheduled Maintenance" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="lblByLbl" runat="server" SkinID="InfoLabel" Text="By:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblBy" runat="server" SkinID="InfoLabelBold"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="lblReturnDateLbl" runat="server" SkinID="InfoLabel" Text="Return Date:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <telerik:RadDatePicker ID="dtReturnDate" runat="server">
                            </telerik:RadDatePicker>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="Label2" runat="server" SkinID="InfoLabel" Text="Notes:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtNotes" runat="server" SkinID="PopBox" Width="95%" MaxLength="255" Rows="10"
                                Height="50px" Wrap="true" TextMode="MultiLine">
                            </asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <div style="text-align: center;width:100%">
                    <button title="Submit" id="Submit" onclick="doComplete(); return false;">
                        Ok</button>&nbsp;&nbsp;&nbsp;
                    <button title="Cancel" id="Cancel" onclick="doCancel(); return false;">
                        Cancel</button>
                </div>
                <br />
            </div>
        </div>
    </form>
</body>
</html>
