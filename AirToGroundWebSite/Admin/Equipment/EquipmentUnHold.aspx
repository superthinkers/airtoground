﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EquipmentUnHold.aspx.cs"
    Inherits="Equipment_EquipmentUnHold" Buffer="true" Strict="true" Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function doComplete() {
                // Create the argument that will be returned to the parent page
                var oArg = new Object();

                // Let's the calling form know the user is committing the changes, versus cancelling.
                oArg.OK = "TRUE";

                // Get a reference to the current RadWindow
                var oWnd = GetRadWindow();

                // Assign args.
                var hidden = $get("<%= hiddenStatusLogId.ClientID %>");
                oArg.StatusLogId = hidden.value;

                hidden = $get("<%= hiddenEqId.ClientID %>");
                oArg.EqId = hidden.value;

                hidden = $get("<%= hiddenSelectedServices.ClientID %>");
                oArg.Services = hidden.value;

                var txtUnHoldDate = $find("<%= txtUnHoldDate.ClientID %>");
                if (txtUnHoldDate.get_selectedDate()) {
                    oArg.UnHoldDate = txtUnHoldDate.get_selectedDate().toLocaleString();
                } else {
                    oArg.UnHoldDate = "";
                }

                var lblUnHoldBy = $get("<%= lblUnHoldBy.ClientID %>");
                if (lblUnHoldBy.innerHTML) {
                    oArg.UnHoldBy = lblUnHoldBy.innerHTML;
                } else {
                    oArg.UnHoldBy = "";
                }

                if (document.getElementById("txtUnHoldNotes").value) {
                    oArg.UnHoldNotes = document.getElementById("txtUnHoldNotes").value;
                } else {
                    oArg.UnHoldNotes = "";
                }

                // Validation.
                if (oArg.UnHoldDate != "") {
                    oWnd.close(oArg);
                } else {
                    alert("The Date is Required.");
                }
            }
            function doCancel() {
                var oWnd = GetRadWindow();
                var oArg = new Object();
                oArg.OK = "FALSE";
                oWnd.close(oArg);
            }
        </script>
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server" AsyncPostBackTimeout="600"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin" ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <asp:HiddenField ID="hiddenStatusLogId" runat="server" Value="" />
        <asp:HiddenField ID="hiddenEqId" runat="server" Value="" />
        <asp:HiddenField ID="hiddenSelectedServices" runat="server" Value="" />
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OnUpdated="ObjectDataSource1_Updated"
            SelectMethod="GetServices" TypeName="ATGDB.Equipmentstatuslog" UpdateMethod="UpdateService">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="0" Name="eqId" QueryStringField="eqId" 
                    Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <div id="mainDiv" runat="server" style="padding: 10px; width: 650px; height: 440px;">
            <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Release Aircraft"></asp:Label>
            <br />
            <br />
            <div style="position: relative; float: left; text-align: left; border: 1px solid Navy; width: 100%">
                <table style="border-collapse: separate; width: 100%">
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblCustomerLbl" runat="server" SkinID="InfoLabel" Text="Customer:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCustomer" runat="server" SkinID="InfoLabelBold" Text="Customer"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblTailNumberLbl" runat="server" SkinID="InfoLabel" Text="Tail Number:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblTailNumber" runat="server" SkinID="InfoLabelBold" Text="Tail Number"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblEqTypeLbl" runat="server" SkinID="InfoLabel" Text="Type:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblEqType" runat="server" SkinID="InfoLabelBold" Text="Type"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 20%">
                            <asp:Label ID="lblDescrLbl" runat="server" SkinID="InfoLabel" Text="Description:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblDescr" runat="server" SkinID="InfoLabelBold" Text="Description"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div style="position: relative; float: left; text-align: left; width: 100%">
                <div style="position: relative; float: left; width: 48%">
                    <table style="border-collapse: separate; width: 100%; padding: 10px;">
                        <tr>
                            <td style="text-align: right; width: 30%">
                                <asp:Label ID="lblStatusDateLbl" runat="server" Text="Status Date:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblStatusDate" runat="server" SkinID="InfoLabelBold" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 30%">
                                <asp:Label ID="lblStatusLbl" runat="server" Text="Status:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblStatus" runat="server" SkinID="InfoLabelBold" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 30%">
                                <asp:Label ID="lblReasonLbl" runat="server" Text="Reason:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblReason" runat="server" SkinID="InfoLabelBold" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 30%">
                                <asp:Label ID="lblByLbl" runat="server" Text="By:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblBy" runat="server" SkinID="InfoLabelBold" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 35%">
                                <asp:Label ID="lblReturnDateLbl" runat="server" Text="Return Date:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblReturnDate" runat="server" SkinID="InfoLabelBold" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 30%">
                                <asp:Label ID="lblNotesLbl" runat="server" Text="Notes:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblNotes" runat="server" SkinID="InfoLabelBold" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 30%">
                                <asp:Label ID="lblUnHoldDate" runat="server" Text="Released Date:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <telerik:RadDatePicker ID="txtUnHoldDate" runat="server">
                                </telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 30%">
                                <asp:Label ID="lblUnHoldByLbl" runat="server" Text="Released By:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblUnHoldBy" runat="server" SkinID="InfoLabelBold" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 30%">
                                <asp:Label ID="Label1" runat="server" Text="Released Notes:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtUnHoldNotes" runat="server" SkinID="PopBox" Width="98%" MaxLength="255"
                                    Rows="10" Height="100px" Wrap="true" TextMode="MultiLine">
                                </asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="position: relative; float: left; text-align: left; width: 50%">
                    <br />
                    <div class="lblInfoBold" style="text-align: center; width: 100%">
                        Select Services To Schedule
                    </div>
                    <telerik:RadGrid ID="grdServices" runat="server" Width="100%"
                        AutoGenerateColumns="False" DataSourceID="ObjectDataSource1"  
                        CellSpacing="0" GridLines="None" AllowAutomaticUpdates="True">
                        <MasterTableView DataSourceID="ObjectDataSource1" AllowAutomaticUpdates="true" DataKeyNames="ServiceId"
                            EditMode="InPlace">
                            <Columns>
                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                    <HeaderStyle Width="60px" />
                                    <ItemStyle Width="60px" />
                                </telerik:GridEditCommandColumn>
                                <telerik:GridBoundColumn DataField="ServiceId" HeaderText="Id" ReadOnly="true" Visible="false" UniqueName="ServiceIdColumn">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Description" HeaderText="Description" ReadOnly="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="ServiceDate"
                                    ColumnEditorID="ServiceDateColumnEditor" DataFormatString="{0:d}" PickerType="DatePicker"
                                    HeaderText="Service Date" ReadOnly="false" MinDate="1/1/0001 12:00:00 AM" AllowFiltering="false"
                                    AllowSorting="false" UniqueName="ServiceDateColumn" ConvertEmptyStringToNull="true">
                                    <HeaderStyle Width="120px" />
                                    <ItemStyle Width="120px" />
                                </telerik:GridDateTimeColumn>
                                <%--<telerik:GridBoundColumn DataField="Comments" HeaderText="Comments">
                                </telerik:GridBoundColumn>--%>
                            </Columns>
                            <AlternatingItemStyle BackColor="#F2F0F2" />
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents />
                            <Selecting AllowRowSelect="true" />
                            <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <telerik:GridDateTimeColumnEditor ID="ServiceDateColumnEditor" runat="server" TextBoxStyle-Width="98%" />
                </div>
            </div>
            <div style="position: relative; float: left; text-align: center; width: 100%; margin-top: 15px;">
                <button title="Submit" id="Submit" onclick="doComplete(); return false;">
                    Ok</button>&nbsp;&nbsp;&nbsp;
                <button title="Cancel" id="Cancel" onclick="doCancel(); return false;">
                    Cancel</button>
                <br />
                <br />
            </div>
        </div>
    </form>
</body>
</html>
