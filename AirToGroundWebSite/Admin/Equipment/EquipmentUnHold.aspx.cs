﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ATGDB;

public partial class Equipment_EquipmentUnHold : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_AIRCRAFT_UNHOLD;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // Now dynamically register the PULSE SCRIPT
        // All pages with this master will keep the session alive.
        PageUtils.RegisterSessionTimeoutPulse(Page);

        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);
      
        // Update RadGrid.ImagePaths
        PageUtils.SetRadGridImagePathAllControls(this, Page.Theme);    

        if (!Page.IsPostBack) {
            if (Request.QueryString["eqId"] != null) {
                Session["LAST_STATUSLOGID"] = null;

                // Clear the cache.
                HttpContext context = HttpContext.Current;
                context.Session[Equipmentstatuslog.EQ_STATUS_LOG_DATA] = "";

                ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();

                // Assign some UI elements.
                int eqId = int.Parse(Request.QueryString["eqId"]);
                hiddenEqId.Value = eqId.ToString();
                ATGDB.Equipment eq = db.Equipments.SingleOrDefault(x => x.Equipmentid == eqId);
                if (eq != null) {
                    lblCustomer.Text = eq.Customer.Description;
                    lblTailNumber.Text = eq.Tailnumber;
                    lblDescr.Text = eq.Description;
                    lblEqType.Text = eq.Equipmenttype.Description;
                }

                // Get the status log.
                ATGDB.Equipmentstatuslog log = eq.Equipmentstatuslog;
                if (log != null) {
                    lblStatusDate.Text = log.Statusdate.ToShortDateString();
                    lblStatus.Text = log.Status;
                    lblReason.Text = log.Statusreason;
                    lblBy.Text = log.Statusby;
                    lblReturnDate.Text = ((log.Returndate != null) && (log.Returndate.ToString() != "")) ? log.Returndate.Value.ToShortDateString() : "";
                    lblNotes.Text = log.Statusnotes;
                    hiddenStatusLogId.Value = log.Equipmentstatuslogid.ToString();
                } else {
                    // Should never come here.
                    lblStatusDate.Text = "None";
                    lblStatus.Text = "None";
                    lblReason.Text = "None";
                    lblBy.Text = "None";
                    lblReturnDate.Text = "None";
                    lblNotes.Text = "None";
                    hiddenStatusLogId.Value = "0";
                }

                // Defaults.
                lblUnHoldBy.Text = Profile.UserName;
                txtUnHoldDate.SelectedDate = DateTime.Now.Date;
            }
        }
    }
    protected void ObjectDataSource1_Updated(object sender, EventArgs e) {
        HttpContext context = HttpContext.Current;
        List<Equipmentstatuslog.SelectedService> logs = (List<Equipmentstatuslog.SelectedService>)context.Session[Equipmentstatuslog.EQ_STATUS_LOG_DATA];
        hiddenSelectedServices.Value = "";
        foreach (Equipmentstatuslog.SelectedService ss in logs) {
            if ((ss.ServiceDate != null) && (ss.ServiceDate != DateTime.MinValue)) {
                if (hiddenSelectedServices.Value == "") {
                    hiddenSelectedServices.Value = ss.ServiceId + "|" + ss.ServiceDate.Value.ToShortDateString();
                } else {
                    hiddenSelectedServices.Value = hiddenSelectedServices.Value + ";" + ss.ServiceId + "|" + ss.ServiceDate.Value.ToShortDateString();
                }
            }
        }
    }
}