﻿using System;
using System.Linq;
using System.Data;

public partial class Equipment_EquipmentStatus : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_AIRCRAFT_STATUS;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // Now dynamically register the PULSE SCRIPT
        // All pages with this master will keep the session alive.
        PageUtils.RegisterSessionTimeoutPulse(Page);

        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);

        if (!Page.IsPostBack) {
            if (Request.QueryString["eqId"] != null) {
                ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
                
                // Assign some UI elements.
                int eqId = int.Parse(Request.QueryString["eqId"]);
                ATGDB.Equipment eq = db.Equipments.SingleOrDefault(x => x.Equipmentid == eqId);
                if (eq != null) {
                    lblCustomer.Text = eq.Customer.Description;
                    lblTailNumber.Text = eq.Tailnumber;
                    lblDescr.Text = eq.Description;
                    lblEqType.Text = eq.Equipmenttype.Description;
                }

                lstLogs.DataSource = GetLogs();
                lstLogs.DataBind();
            }
        }
    }
    protected void lstLogs_NeedDataSource(object sender, EventArgs e) {
        lstLogs.DataSource = GetLogs();
    }
    protected DataSet GetLogs() {
        ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();

        // Assign some UI elements.
        int eqId = int.Parse(Request.QueryString["eqId"]);

        // Get the status log.
        var logs = from l in db.Equipmentstatuslogs
                   where l.Equipmentid == eqId
                   orderby l.Equipmentstatuslogid descending
                   select l;

        // Have to use a dataset so the pager works.
        DataSet ds = new DataSet("DSLog");
        DataTable tbl = ds.Tables.Add("Logs");
        tbl.Columns.Add("Equipmentstatuslogid", typeof(int));
        tbl.Columns.Add("Status", typeof(string));
        tbl.Columns.Add("Statusreason", typeof(string));
        tbl.Columns.Add("Statusby", typeof(string));
        tbl.Columns.Add("Statusdate", typeof(DateTime));
        tbl.Columns.Add("Statusnotes", typeof(string));
        tbl.Columns.Add("Unholdby", typeof(string));
        tbl.Columns.Add("Unholddate", typeof(DateTime));
        tbl.Columns.Add("Unholdnotes", typeof(string));
        tbl.Columns.Add("Returndate", typeof(string));

        foreach (ATGDB.Equipmentstatuslog log in logs) {
            tbl.Rows.Add(new object[] {
                        log.Equipmentstatuslogid,
                        log.Status,
                        log.Statusreason,
                        log.Statusby,
                        log.Statusdate,
                        log.Statusnotes,
                        log.Unholdby,
                        log.Unholddate,
                        log.Unholdnotes,
                        log.Returndate
                    });
        }

        return ds;
    }
}