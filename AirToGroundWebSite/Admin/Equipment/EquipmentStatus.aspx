﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EquipmentStatus.aspx.cs"
    Inherits="Equipment_EquipmentStatus" Buffer="true" Strict="true" Explicit="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function doClose() {
                var oWnd = GetRadWindow();
                var oArg = new Object();
                oWnd.close(oArg);
            }
        </script>
        <!-- Script Manager -->
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" AsyncPostBackTimeout="600"
            EnableScriptCombine="true" OutputCompression="Disabled" LoadScriptsBeforeUI="true"
            ClientIDMode="AutoID" EnablePageMethods="true" EnableViewState="true" EnableHistory="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-2.1.0.min.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/Core.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQuery.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Common/jQueryPlugins.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Ajax/Ajax.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Window/RadWindowManager.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Grid/RadGridScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDatePicker.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadPickersPopupDirectionEnumeration.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadTmeViewScripts.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Caneldar/RadCalendarCommonScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Calendar/RadDateTimePickerScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/TextBox/RadInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Input/DateInput/RadDateInputScript.js" />
                <asp:ScriptReference Path="~/Scripts/Telerik/Button/RadButton.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="ATGSkin" ShowChooser="false">
        </telerik:RadSkinManager>
        <telerik:RadFormDecorator ID="RadFormDecorator2" runat="server" DecoratedControls="All" />
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="lstLogs">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="lstLogs" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <div id="mainDiv" runat="server" style="padding: 10px; width: 430px; height: 330px;">
            <asp:Label ID="lblTitle" runat="server" SkinID="TitleLabel" Text="Aircraft Status"></asp:Label>
            <br />
            <br />
            <div style="text-align: left; border: 1px solid Navy; width: 100%">
                <table style="border-collapse: separate; width: 100%">
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="lblCustomerLbl" runat="server" SkinID="InfoLabel" Text="Customer:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblCustomer" runat="server" SkinID="InfoLabelBold" Text="Customer"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="lblTailNumberLbl" runat="server" SkinID="InfoLabel" Text="Tail Number:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblTailNumber" runat="server" SkinID="InfoLabelBold" Text="Tail Number"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="lblEqTypeLbl" runat="server" SkinID="InfoLabel" Text="Type:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblEqType" runat="server" SkinID="InfoLabelBold" Text="Type"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 35%">
                            <asp:Label ID="lblDescrLbl" runat="server" SkinID="InfoLabel" Text="Description:"></asp:Label>
                        </td>
                        <td style="text-align: left">
                            <asp:Label ID="lblDescr" runat="server" SkinID="InfoLabelBold" Text="Description"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div style="text-align: left; border: 1px solid Navy; width: 100%">
                <telerik:RadListView ID="lstLogs" runat="server" BorderWidth="0" BorderStyle="None" 
                    AllowPaging="true" DataKeyNames="Equipmentstatuslogid"
                    Width="100%" PageSize="1" OnNeedDataSource="lstLogs_NeedDataSource">
                    <ItemTemplate>
                        <table style="border-collapse: separate; width: 100%">
                            <tr>
                                <td style="text-align: right; width: 35%">
                                    <asp:Label ID="lblStatusDateLbl" runat="server" Text="Status Date:"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:Label ID="lblStatusDate" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Statusdate", "{0:d}") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 35%">
                                    <asp:Label ID="lblStatusLbl" runat="server" Text="Status:"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:Label ID="lblStatus" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Status") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 35%">
                                    <asp:Label ID="lblReasonLbl" runat="server" Text="Reason:"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:Label ID="lblReason" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Statusreason") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 35%">
                                    <asp:Label ID="lblByLbl" runat="server" Text="By:"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:Label ID="lblBy" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Statusby") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 35%">
                                    <asp:Label ID="lblReturnDateLbl" runat="server" Text="Return Date:"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:Label ID="lblReturnDate" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Returndate", "{0:d}") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 35%">
                                    <asp:Label ID="lblNotesLbl" runat="server" Text="Notes:"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:Label ID="lblNotes" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Statusnotes") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 35%">
                                    <asp:Label ID="lblUnHoldDateLbl" runat="server" Text="Released Date:"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:Label ID="lblUnHoldDate" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Unholddate", "{0:d}") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 35%">
                                    <asp:Label ID="lblUnHoldByLbl" runat="server" Text="Released By:"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:Label ID="lblUnHoldBy" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Unholdby") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 35%">
                                    <asp:Label ID="lblUnHoldNotesLbl" runat="server" Text="Released Notes:"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:Label ID="lblUnHoldNotes" runat="server" SkinID="InfoLabelBold" Text='<%# Eval("Unholdnotes") %>'></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadListView>
            </div>
            <telerik:RadDataPager ID="RadDataPager1" runat="server" PagedControlID="lstLogs" PageSize="1" Width="100%" EnableTheming="true">
                <Fields>
                    <telerik:RadDataPagerButtonField FieldType="FirstPrev" FirstButtonText="Prev"  />
                    <telerik:RadDataPagerButtonField FieldType="Numeric"  />
                    <telerik:RadDataPagerButtonField FieldType="NextLast" NextButtonText="Next" />
                    <telerik:RadDataPagerTemplatePageField>
                        <PagerTemplate>
                            <div class="stdLabel" style="float: right">
                                <b>Items
                                    <asp:Label runat="server" ID="CurrentPageLabel" Text="<%# Container.Owner.StartRowIndex+1%>" />
                                    of
                                    <asp:Label runat="server" ID="TotalItemsLabel" Text="<%# Container.Owner.TotalRowCount%>" />
                                    <br />
                                </b>
                            </div>
                        </PagerTemplate>
                    </telerik:RadDataPagerTemplatePageField>
                </Fields>
            </telerik:RadDataPager>
            <div style="width: 100%">
                <br />
                <div style="text-align: center;width:100%">
                    <button title="Submit" id="Submit" onclick="doClose(); return false;">
                        Close</button>
                </div>
                <br />
            </div>
        </div>
    </form>
</body>
</html>
