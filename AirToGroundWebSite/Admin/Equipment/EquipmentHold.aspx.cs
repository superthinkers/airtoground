﻿using System;
using System.Linq;

public partial class Equipment_EquipmentHold : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_AIRCRAFT_HOLD;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // Now dynamically register the PULSE SCRIPT
        // All pages with this master will keep the session alive.
        PageUtils.RegisterSessionTimeoutPulse(Page);

        // StyleSheets
        PageUtils.RegisterAllSkins(Page, RadStyleSheetManager1);

        if (!Page.IsPostBack) {
            if (Request.QueryString["eqId"] != null) {
                Session["LAST_STATUSLOGID"] = null;

                ATGDB.ATGDataContext db = new ATGDB.ATGDataContext();
                
                // Assign some UI elements.
                int eqId = int.Parse(Request.QueryString["eqId"]);
                hiddenEqId.Value = eqId.ToString();
                ATGDB.Equipment eq = db.Equipments.SingleOrDefault(x => x.Equipmentid == eqId);
                if (eq != null) {
                    lblCustomer.Text = eq.Customer.Description;
                    lblTailNumber.Text = eq.Tailnumber;
                    lblDescr.Text = eq.Description;
                    lblEqType.Text = eq.Equipmenttype.Description;

                    if (eq.Equipmentstatuslog != null) {
                        if (eq.Equipmentstatuslog.Status != "") {
                            lstStatus.SelectedValue = eq.Equipmentstatuslog.Status;
                            lstReason.SelectedValue = eq.Equipmentstatuslog.Statusreason;
                            txtStatusDate.SelectedDate = eq.Equipmentstatuslog.Statusdate;
                            lblBy.Text = eq.Equipmentstatuslog.Statusby;
                            if (eq.Equipmentstatuslog.Returndate != null) {
                                dtReturnDate.SelectedDate = eq.Equipmentstatuslog.Returndate;
                            }
                        }

                        if (eq.Equipmentstatuslog.Statusnotes != "") {
                            txtNotes.Text = eq.Equipmentstatuslog.Statusnotes;
                        }

                        // This determines if updating an existing hold record, or creating a new one.
                        if (eq.Equipmentstatuslog.Unholddate != null) {
                            // New one.
                            hiddenStatusLogId.Value = "0";
                            // Clear the notes.
                            txtNotes.Text = "";
                            // Update status by.
                            lblBy.Text = Profile.UserName;
                        } else {
                            // Updating.
                            hiddenStatusLogId.Value = eq.Equipmentstatuslogid.ToString();
                            // Update status by.
                            lblBy.Text = Profile.UserName;
                        }
                    } else {
                        // New one.
                        lblBy.Text = Profile.UserName;
                        txtNotes.Text = "";
                        txtStatusDate.SelectedDate = DateTime.Now.Date;
                        hiddenStatusLogId.Value = "0";
                    }
                } else {
                    // Defaults.
                    lblBy.Text = Profile.UserName;
                    txtStatusDate.SelectedDate = DateTime.Now.Date;
                }
            }
        }
    }
}