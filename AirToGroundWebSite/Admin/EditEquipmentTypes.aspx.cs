﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_EditEquipmentTypes : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_AIRCRAFT_TYPE;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            if (!Page.IsPostBack) {
                // Security.
                if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_GEN)) {
                    RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                    RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                    RadGrid1.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                    RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
                }
            }
        }
    }
    protected void lstCustomer_DataBound(object sender, EventArgs e) {
        RadComboBox lst = (RadComboBox)sender;
        if (lst.Items[0].Value != "") {
            RadComboBoxItem item = new RadComboBoxItem("", "");
            lst.Items.Insert(0, item);
        }
    }
    protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("Equipmenttypeid").ToString();

        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
            SetMessage("Equipment Type " + id + " cannot be updated. Reason: " +
                e.Exception.Message);
            if (e.Exception.InnerException != null) {
                SetMessage("Equipment Type " + id + " cannot be updated. Reason: " +
                    e.Exception.InnerException.Message);
            }
            DisplayMessage(gridMessage);
        } else {
            SetMessage("Equipment Type " + id + " is updated!");
        }
    }

    protected void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e) {
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("Equipment Type cannot be inserted. Reason: " + e.Exception.InnerException.Message);
            DisplayMessage(gridMessage);
        } else {
            SetMessage("New Equipment Type is inserted!");
        }
    }

    protected void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e) {
        GridDataItem dataItem = (GridDataItem)e.Item;
        String id = dataItem.GetDataKeyValue("Equipmenttypeid").ToString();

        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("Equipment Type " + id + " cannot be deleted. Reason: " +
                e.Exception.InnerException.Message);
            DisplayMessage(gridMessage);
        } else {
            SetMessage("Equipment Type " + id + " is deleted!");
            DisplayMessage(gridMessage);
        }
    }
    protected void RadGrid1_DataBound(object sender, EventArgs e) {
        if (!string.IsNullOrEmpty(gridMessage)) {
            DisplayMessage(gridMessage);
        }
    }
    private string gridMessage = null;
    private void DisplayMessage(string text) {
        gridMessage = "";
        RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "RadGrid1");
        grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage(string message) {
        gridMessage = message;
    }
    protected void RadGrid1_ItemCommand(object sender, CommandEventArgs e) {
        // Where statements.
        string sCWhere = "(Customerid == Convert.ToInt32(@Customerid))";
        string sDESCRWhere = "(Description.ToUpper().Contains(@Description.ToUpper()))";

        RadComboBox lstCustomerFilter = (RadComboBox)PageUtils.FindControlRecursive(RadGrid1, "lstCustomerFilter");
        RadTextBox txtDescriptionFilter = (RadTextBox)PageUtils.FindControlRecursive(RadGrid1, "txtDescriptionFilter");

        // Based on filter, add where parameters, or remove them.
        if ((e.CommandName == "Edit") ||
            (e.CommandName == "Update") ||
            (e.CommandName == "Delete") ||
            (e.CommandName == "InitInsert") ||
            (e.CommandName == "PerformInsert") ||
            (e.CommandName == "Cancel") ||
            (e.CommandName == "DoCustomerFilter") ||
            (e.CommandName == "DoDescriptionFilter")) {
            // Clear first.
            LinqDataSource1.WhereParameters.Clear();

            // Customer Filter.
            string id = lstCustomerFilter.SelectedValue;
            if (int.Parse(id) > 0) {
                if (LinqDataSource1.WhereParameters["Customerid"] == null) {
                    LinqDataSource1.WhereParameters.Add("Customerid", System.Data.DbType.Int32, id);
                }
            }

            // Description Filter.
            id = txtDescriptionFilter.Text;
            if (!String.IsNullOrEmpty(id)) {
                if (LinqDataSource1.WhereParameters["Description"] == null) {
                    LinqDataSource1.WhereParameters.Add("Description", System.Data.DbType.String, id);
                }
            }

        }

        // Now, based on existing where parameters, build the where statement.
        // Clear first.
        LinqDataSource1.Where = "";

        // Customer
        if (LinqDataSource1.WhereParameters["Customerid"] != null) {
            LinqDataSource1.Where = sCWhere;
        }

        // Description
        if (LinqDataSource1.WhereParameters["Description"] != null) {
            if (LinqDataSource1.Where == "") {
                LinqDataSource1.Where = sDESCRWhere;
            } else {
                LinqDataSource1.Where = LinqDataSource1.Where + " && " + sDESCRWhere;
            }
        }

        // Finally, rebind.
        LinqDataSource1.DataBind();
    }

}