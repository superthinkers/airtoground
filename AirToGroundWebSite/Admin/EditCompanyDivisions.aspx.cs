﻿using System;
using System.Web.UI;
using Telerik.Web.UI;

public partial class Admin_EditCompanyDivisions : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_ADMIN_COMPANY_DIVISIONS;
    }
    protected void Page_PreInit(object sender, EventArgs e) {
        Page.Theme = Profile.SiteTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            if (!Page.IsPostBack) {
                // Security.
                if (!ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_EXP_GEN)) {
                    RadGrid1.MasterTableView.CommandItemSettings.ShowExportToCsvButton = false;
                    RadGrid1.MasterTableView.CommandItemSettings.ShowExportToExcelButton = false;
                    RadGrid1.MasterTableView.CommandItemSettings.ShowExportToPdfButton = false;
                    RadGrid1.MasterTableView.CommandItemSettings.ShowExportToWordButton = false;
                }
            }
        }
    }
    protected void lstCustomer_DataBound(object sender, EventArgs e) {
        RadComboBox lst = (RadComboBox)sender;
        if (lst.Items[0].Value != "") {
            RadComboBoxItem item = new RadComboBoxItem("", "");
            lst.Items.Insert(0, item);
        }
    }
    protected void lstDivisionarea_DataBound(object sender, EventArgs e) {
        RadComboBox lst = (RadComboBox)sender;
        if (lst.Items[0].Value != "") {
            RadComboBoxItem item = new RadComboBoxItem("", "");
            lst.Items.Insert(0, item);
        }
    }
    protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e) {
        GridEditableItem item = e.Item;
        String id = item.GetDataKeyValue("Companydivisionid").ToString();

        if (e.Exception != null) {
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;    
            if (e.Exception.Message.ToUpper().Contains("A MEMBER DEFINING THE IDENTITY")) {
                SetMessage("You cannot update Customers. Delete and add a New Customer."); 
            } else {
                SetMessage("Company Division " + id + " cannot be updated. Reason: " + 
                    e.Exception.Message);
            }
            DisplayMessage(gridMessage);
        } else {
            SetMessage("Company Division " + id + " is updated!");
        }
    }

    protected void RadGrid1_ItemInserted(object source, GridInsertedEventArgs e) {
        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("Company Division cannot be inserted. Reason: " + e.Exception.InnerException.Message);
            DisplayMessage(gridMessage);
        } else {
            SetMessage("New Company Division is inserted!");
        }
    }

    protected void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e) {
        GridDataItem dataItem = (GridDataItem)e.Item;
        String id = dataItem.GetDataKeyValue("Companydivisionid").ToString();

        if (e.Exception != null) {
            e.ExceptionHandled = true;
            SetMessage("Company Division " + id + " cannot be deleted. Reason: " + 
                e.Exception.Message);
            DisplayMessage(gridMessage);
        } else {
            SetMessage("Company Division " + id + " is deleted!");
            DisplayMessage(gridMessage);
        }
    }
    protected void RadGrid1_DataBound(object sender, EventArgs e) {
        if (!string.IsNullOrEmpty(gridMessage)) {
            DisplayMessage(gridMessage);
        }
    }
    private string gridMessage = null;
    private void DisplayMessage(string text) {
        gridMessage = "";
        RadGrid grid = (RadGrid)PageUtils.FindControlRecursive(this.Page, "RadGrid1");
        grid.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", text)));
    }
    private void SetMessage(string message) {
        gridMessage = message;
    }
 
}