﻿using System;
using System.Collections.Generic;
using System.Linq;

public partial class Home : System.Web.UI.Page, IAuditTrailPage
{
    public string GetAuditPageName() {
        return AuditUtils.PAGE_HOME;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            // Default the summary range.
            if (Profile.DefaultSummaryRange.ToString() == "") {
                Profile.DefaultSummaryRange = "30";
                Profile.Save();
            }
            // Security setup.
            // Revenue Reports
            tblRevenue.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_REVENUE);
            // Work Reports
            tblWork.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_WORK);
            // Equipment Reports
            tblEquipment.Visible = ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_RPT_EQUIP);

            // Set up the drop down list.
            lstRange.SelectedItem.Selected = false;
            lstRange.Items.FindByValue(Profile.DefaultSummaryRange).Selected = true;

            // Load Queue Summary
            ATGDB.Servicequeue.GetSynchronizedServiceQueueItemsByCompCustAndDateDS(Profile.Companydivisionid + ":" + Profile.Customerid,
                DateTime.Now.Date, false, false);
            LoadQueueSummary();

            if (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_VERIFY_SS) || 
                ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKLOG) ||
                ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN)) {
                // Show the feature.
                divVerifications.Visible = true;
                // Load Verifications
                LoadVerificationSummary();
            } else {
                // Hide the feature.
                divVerifications.Visible = false;
            }
        }
    }

    public class ServiceQueueSummary
    {
        public string Description { get; set; }
        public int Count { get; set; }
        public ServiceQueueSummary(string descr, int count) {
            Description = descr;
            Count = count;
        }
    }
    protected void LoadQueueSummary() {
        
        // Get today's queue for the logged in user.
        string keys = Profile.Companydivisionid + ":" + Profile.Customerid;
        var data = ATGDB.Servicequeue.GetServiceQueueItemsByCompCustAndDate(keys, DateTime.Now.Date);

        // Build a summary dataset.
        List<ServiceQueueSummary> list = new List<ServiceQueueSummary>();

        int cnt = (from x in data
                   where x.Isdone == true
                   where x.Closeddate != null
                   select x).Count();
        list.Add(new ServiceQueueSummary("Done and Closed", cnt));

        cnt = (from x in data
               where x.Isdone == false
               where x.Closeddate != null
               select x).Count();
        list.Add(new ServiceQueueSummary("Not Done, Closed", cnt));

        cnt = (from x in data
               where x.Isdone == false
               where x.Closeddate == null
               select x).Count();
        list.Add(new ServiceQueueSummary("Not Done, Not Closed", cnt));

        lstQueueSummary.DataSource = list;
        lstQueueSummary.DataBind();
    }

    public class VerificationSummary
    {
        public string Description { get; set; }
        public int Count { get; set; }
        public VerificationSummary(string descr, int count) {
            Description = descr;
            Count = count;
        }
    }
    protected void LoadVerificationSummary() {
        ATGDB.ATGDataContext dc = new ATGDB.ATGDataContext();
        List<VerificationSummary> list = new List<VerificationSummary>();
        if (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_VERIFY_SS) ||
            ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN)) {
            var data1 = from ss in dc.Serviceschedules
                        where ((ss.Companydivisionid == int.Parse(Profile.Companydivisionid)) || (ss.Companydivisionid == null))
                        where ss.Equipment.Customerid == int.Parse(Profile.Customerid)
                        where ss.Approvaldate == null
                        select ss;
            list.Add(new VerificationSummary("Verify Scheduled Services", data1.Count()));
        }
        if (ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_WORKLOG) ||
            ProviderUtils.IsUserInRole(Profile.UserName, SecurityUtils.ROLE_ADMIN)) {
            var data2 = from wl in dc.Worklogs
                        where wl.Approvaldate == null
                        select wl;

            list.Add(new VerificationSummary("Verify Work Logs", data2.Count()));
        }
        lstVerifications.DataSource = list;
        lstVerifications.DataBind();
    }

    protected System.Drawing.Color GetColor() {
        if (Eval("Scheduled").ToString() == "True") {
            return System.Drawing.Color.MediumSpringGreen;
        } else {
            return System.Drawing.Color.LightBlue;
        }
    }
    protected void btnRefreshSummary_Click(object sender, EventArgs e) {
        // Save the selection to the profile.
        Profile.DefaultSummaryRange = lstRange.SelectedValue.ToString();
        Profile.Save();
        // Rebind.
        ObjectDataSource1.DataBind();
        lstSummary.DataBind();
    }
}
