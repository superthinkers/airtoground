/*
5/26/2012 jmp Initial.
*/
USE atg;

ALTER TABLE atg.Service
        ADD (Showinservicequeue BIT NOT NULL DEFAULT 1);
ALTER TABLE atg.Service
        ADD (Showinmasterschedule BIT NOT NULL DEFAULT 1);
