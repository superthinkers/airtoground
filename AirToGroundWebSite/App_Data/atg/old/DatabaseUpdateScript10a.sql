/*

Change UserName columns for UserId columns.

9/22/2012 jmp Initial.

*/
USE atg;
-- Misc
ALTER TABLE atg.companycustomeruserjoin
        DROP FOREIGN KEY FK_companycustomeruserjoin;
ALTER TABLE atg.companycustomeruserjoin
        DROP INDEX IX_companycustomeruserjoin_UserName;
ALTER TABLE atg.companycustomeruserjoin
        DROP INDEX UK_companycustomeruserjoin;
ALTER TABLE atg.companycustomeruserjoin
        DROP INDEX UK_companycustomeruserjoin_CompanyDivisionId;
ALTER TABLE atg.companycustomeruserjoin
        DROP INDEX UK_companycustomeruserjoin_CustomerId;
COMMIT;
ALTER TABLE atg.companycustomeruserjoin
        DROP PRIMARY KEY;
COMMIT;
-- Add UserId columns. 
ALTER TABLE atg.companycustomeruserjoin
        ADD COLUMN (userid VARCHAR(40) NOT NULL DEFAULT '');
ALTER TABLE atg.extendeduser
        ADD COLUMN (userid VARCHAR(40) NOT NULL DEFAULT '');
ALTER TABLE atg.timesheet
        ADD COLUMN (userid VARCHAR(40) NOT NULL DEFAULT '');
ALTER TABLE atg.worklog
        ADD COLUMN (userid VARCHAR(40) NOT NULL DEFAULT '');
-- Populate with data.
COMMIT;
UPDATE atg.companycustomeruserjoin ccuj
SET
  userid = (SELECT max(u.userid)
            FROM
              atg.aspnet_users u
            WHERE
              u.username = ccuj.UserName);
UPDATE atg.extendeduser exu
SET
  userid = (SELECT max(u.userid)
            FROM
              atg.aspnet_users u
            WHERE
              u.username = exu.UserName);
UPDATE atg.timesheet ts
SET
  userid = (SELECT max(u.userid)
            FROM
              atg.aspnet_users u
            WHERE
              u.username = ts.UserName);
UPDATE atg.worklog wl
SET
  userid = (SELECT max(u.userid)
            FROM
              atg.aspnet_users u
            WHERE
              u.username = wl.UserName);
UPDATE worklog
SET
  userid = 'dd1666ce-78cc-423f-b810-a026880a1448'
WHERE
  username IS NULL;
UPDATE timesheet
SET
  userid = 'dd1666ce-78cc-423f-b810-a026880a1448'
WHERE
  username IS NULL;
DELETE
FROM
  atg.companycustomeruserjoin
WHERE
  UserName = 'Temp 1';
DELETE
FROM
  atg.companycustomeruserjoin
WHERE
  UserName = 'User 1';
COMMIT;
/* Drop the UserName columns */
ALTER TABLE atg.extendeduser
        DROP COLUMN UserName;
ALTER TABLE atg.timesheet
        DROP COLUMN UserName;
ALTER TABLE atg.worklog
        DROP COLUMN UserName;
ALTER TABLE atg.companycustomeruserjoin
        DROP COLUMN UserName;
-- New Indexes
COMMIT;
CREATE INDEX IDX_FK_CompanyCustomerUserJoin_CompanyDivisionId ON atg.companycustomeruserjoin (Companydivisionid);
CREATE INDEX IDX_FK_CompanyCustomerUserJoin_CustomerId ON atg.companycustomeruserjoin (Customerid);
CREATE INDEX IDX_FK_CompanyCustomerUserJoin_UserId ON atg.companycustomeruserjoin (userid);  
CREATE INDEX IDX_FK_ExtendedUser_UserId ON atg.extendeduser (userid);
CREATE INDEX IDX_FK_TimeSheet_UserId ON atg.timesheet (userid);
CREATE INDEX IDX_FK_WorkLog_UserId ON atg.worklog (userid);
-- New Constraints 
ALTER TABLE atg.extendeduser
        ADD CONSTRAINT FK_ExtendedUser_UserId FOREIGN KEY (userid)
        REFERENCES atg.aspnet_users (userid) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE atg.timesheet
        ADD CONSTRAINT FK_TimeSheet_UserId FOREIGN KEY (userid)
        REFERENCES atg.aspnet_users (userid) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE atg.worklog
        ADD CONSTRAINT FK_WorkLog_UserId FOREIGN KEY (userid)
        REFERENCES atg.aspnet_users (userid) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE atg.companycustomeruserjoin
        ADD CONSTRAINT FK_CompanyCustomerUserJoin_CompanyDivisionId FOREIGN KEY (Companydivisionid)
        REFERENCES atg.Companydivision (Companydivisionid) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE atg.companycustomeruserjoin
        ADD CONSTRAINT FK_CompanyCustomerUserJoin_CustomerId FOREIGN KEY (Customerid)
        REFERENCES atg.Customer (Customerid) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE atg.companycustomeruserjoin
        ADD CONSTRAINT FK_CompanyCustomerUserJoin_UserId FOREIGN KEY (userid)
        REFERENCES atg.aspnet_users (userid) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE atg.companycustomeruserjoin
        ADD CONSTRAINT PK_CompanyCustomerUserJoin PRIMARY KEY (Companydivisionid, Customerid, userid);
COMMIT;
/* Add Primary Keys back */
-- ALTER TABLE atg.companycustomeruserjoin
--        ADD PRIMARY KEY (companydivisionid, customerid, userid);
-- ROLLBACK;


