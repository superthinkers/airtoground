DELETE
FROM
  worklog
WHERE
  ((SELECT count(*)
    FROM
      serviceschedule ss
    WHERE
      ss.worklogid = worklog.WorkLogId) = 0);

  SELECT *
FROM
  serviceexception
WHERE
  ((SELECT count(*)
    FROM
      serviceschedule ss
    WHERE
      ss.ServiceScheduleId = serviceexception.ServiceScheduleId) = 0);

  SELECT *
FROM
  servicequeueitem
WHERE
  ((SELECT count(*)
    FROM
      serviceschedule ss
    WHERE
      ss.ServiceScheduleId = servicequeueitem.ServiceScheduleId) = 0);
