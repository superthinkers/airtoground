CREATE TABLE atg.audittrail(
  AuditTrailId INT(11) NOT NULL AUTO_INCREMENT,
  UserId VARCHAR(40) NOT NULL,
  PageName VARCHAR(100) NOT NULL,
  StartDateTime DATETIME NOT NULL,
  EndDateTime DATETIME DEFAULT NULL,
  PRIMARY KEY (AuditTrailId),
  INDEX IX_audittrail_EndDateTime (EndDateTime),
  INDEX IX_audittrail_PageName (PageName),
  INDEX IX_audittrail_StartDateTime (StartDateTime),
  INDEX IX_audittrail_UserId (UserId),
  UNIQUE INDEX UK_audittrail_AuditTrailId (AuditTrailId),
  CONSTRAINT FK_audittrail_aspnet_users_userid FOREIGN KEY (UserId)
  REFERENCES atg.aspnet_users (userid) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET latin1
COLLATE latin1_swedish_ci;