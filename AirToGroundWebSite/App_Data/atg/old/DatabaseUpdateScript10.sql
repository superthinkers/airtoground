/*

Add new join table for Service, EquipmentType, and JobCode

9/4/2012 jmp Initial.

*/
USE atg;

CREATE TABLE IF NOT EXISTS atg.serviceequipmenttypejobcodejoin(
  ServiceId INT(11) NOT NULL,
  EquipmentTypeId INT(11) NOT NULL,
  JobCodeId INT(11) NOT NULL,
  PRIMARY KEY (ServiceId, EquipmentTypeId),
  INDEX FK_serviceequipmenttypejobcodejoin_Service (ServiceId),
  INDEX FK_serviceequipmenttypejobcodejoin_EquipmentType (EquipmentTypeId),
  INDEX FK_serviceequipmenttypejobcodejoin_JobCode (JobCodeId),
  UNIQUE INDEX UK_serviceequipmenttypejobcodejoin (ServiceId, EquipmentTypeId),
  CONSTRAINT FK_serviceequipmenttypejobcodejoin_ServiceId FOREIGN KEY (ServiceId)
  REFERENCES atg.service (ServiceId) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_serviceequipmenttypejobcodejoin_JobCodeId FOREIGN KEY (JobCodeId)
  REFERENCES atg.jobcode (JobCodeId) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_serviceequipmenttypejobcodejoin_EquipmentTypeId FOREIGN KEY (EquipmentTypeId)
  REFERENCES atg.equipmenttype (EquipmentTypeId) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

/* Remove the JobCodeId column from Service */
ALTER TABLE atg.Service 
        DROP FOREIGN KEY FK_service_jobcodeid;
DROP INDEX FK_service_jobcodeid 
        ON atg.service;
ALTER TABLE atg.Service
        DROP COLUMN JobCodeId;

