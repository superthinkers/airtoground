delete from servicequeueitem
  where servicescheduleid in (select servicescheduleid from serviceschedule 
                                where serviceid in (104,105,108,110,111,113,115,149,117,88,89,160,164,92,93,98,122,125));

DELETE
FROM
  serviceschedule
WHERE
  serviceid IN (104, 105, 108, 110, 111, 113, 115, 149, 117, 88, 89, 160, 164, 92, 93, 98, 122, 125);

DELETE
FROM
  servicesectionjoin
WHERE
  serviceid IN (104, 105, 108, 110, 111, 113, 115, 149, 117, 88, 89, 160, 164, 92, 93, 98, 122, 125);

DELETE
FROM
  service
WHERE
  serviceid IN (104, 105, 108, 110, 111, 113, 115, 149, 117, 88, 89, 160, 164, 92, 93, 98, 122, 125);