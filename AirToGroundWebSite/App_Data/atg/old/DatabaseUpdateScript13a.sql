USE atg;

ALTER TABLE ServiceSchedule
        ADD (ApprovalDate DATETIME);
ALTER TABLE ServiceSchedule
        ADD (ApprovalUserId VARCHAR(245) DEFAULT "Administrator");
ALTER TABLE ServiceSchedule
        ADD (UpdDt DATETIME NOT NULL);
ALTER TABLE ServiceSchedule
        ADD (UpdUserId VARCHAR(245) DEFAULT 'Administrator' NOT NULL);
ALTER TABLE ServiceSchedule
        ADD (ChangeHistory TEXT);
UPDATE ServiceSchedule
SET
  UpdDt = curdate(), ApprovalDate = curdate();

-- ROLLBACK;
-- COMMIT;