/*
5/26/2012 jmp Initial.
*/
USE atg;

CREATE TABLE IF NOT EXISTS atg.serviceshortdescr(
  ServiceShortDescrId INT(11) NOT NULL,
  Description VARCHAR(5) NOT NULL,
  PRIMARY KEY (ServiceShortDescrId),
  UNIQUE INDEX UK_serviceshortdescr_ServiceShortDescrId (ServiceShortDescrId)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 16384
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

INSERT INTO atg.Serviceshortdescr (ServiceShortDescrId, Description) VALUES (1, 'MISC');

ALTER TABLE atg.Service
        DROP ShortDescription;
ALTER TABLE atg.Service
        ADD (ServiceShortDescrId INT(11) NOT NULL DEFAULT 1);
ALTER TABLE atg.Service
        ADD INDEX IX_service_ServiceShortDescrId (ServiceShortDescrId);
ALTER TABLE atg.Service
        ADD CONSTRAINT FK_service_serviceshortdescr_ServiceShortDescrId FOREIGN KEY (ServiceShortDescrId)
        REFERENCES atg.serviceshortdescr (ServiceShortDescrId) ON DELETE RESTRICT ON UPDATE RESTRICT;
