USE atg;

ALTER TABLE ServiceSchedule ADD (DivisionAreaId int(10));

CREATE INDEX ServiceSchedule_UK_DivisionAreaId ON ServiceSchedule (DivisionAreaId);

ALTER TABLE ServiceSchedule ADD CONSTRAINT FK_serviceschedule_divisionareaid FOREIGN KEY (DivisionAreaId)
    REFERENCES atg.DivisionArea (DivisionAreaId) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Service ADD (RequiresDivisionArea bit NOT NULL DEFAULT FALSE);

-- ROLLBACK;
-- COMMIT;