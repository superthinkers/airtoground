USE atg;

ALTER TABLE TimeSheet
        ADD (UpdDt DATETIME NOT NULL);
ALTER TABLE TimeSheet
        ADD (UpdUserId VARCHAR(245) DEFAULT 'Administrator' NOT NULL);
ALTER TABLE TimeSheet
        ADD (ChangeHistory TEXT);
UPDATE TimeSheet
SET
  UpdUserId = UpdUserName, UpdDt = curdate();
ALTER TABLE TimeSheet
        DROP COLUMN UpdUserName;

ALTER TABLE WorkCard
        ADD (BudgetAmount DOUBLE(7,2) DEFAULT 0.00 NOT NULL);
ALTER TABLE WorkCard
        ADD (BudgetHours DOUBLE(7, 2) DEFAULT 0.00 NOT NULL);
ALTER TABLE WorkCard
        ADD (UpdDt DATETIME NOT NULL);
ALTER TABLE WorkCard
        ADD (UpdUserId VARCHAR(245) DEFAULT 'Administrator' NOT NULL);
ALTER TABLE WorkCard
        ADD (ChangeHistory TEXT);
UPDATE WorkCard
SET
  UpdDt = curdate();

ALTER TABLE WorkLog
        ADD (ApprovalDate DATETIME);
ALTER TABLE WorkLog
        ADD (ApprovalUserId VARCHAR(245) DEFAULT "Administrator");
ALTER TABLE WorkLog
        ADD (UpdDt DATETIME NOT NULL);
ALTER TABLE WorkLog
        ADD (UpdUserId VARCHAR(245) DEFAULT 'Administrator' NOT NULL);
ALTER TABLE WorkLog
        ADD (ChangeHistory TEXT);
UPDATE WorkLog
SET
  UpdDt = curdate(), ApprovalDate = curdate();

ROLLBACK;
-- COMMIT;