﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        string JQueryVer = "2.1.0";
        ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
        {
            Path = "~/Scripts/jquery-" + JQueryVer + ".min.js",
            DebugPath = "~/Scripts/jquery-" + JQueryVer + ".js",
            CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + JQueryVer + ".min.js",
            CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + JQueryVer + ".js",
            CdnSupportsSecureConnection = true,
            LoadSuccessExpression = "window.jQuery"
        });
    }                                                         
             
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown
    }
        
    void Application_Error(object sender, EventArgs e) 
    {
        // Get the last error.
        Exception ex = Server.GetLastError();
        Exception bex = ex.GetBaseException();
        string err = "ERROR MESSAGE: " + ex.Message + "\n" + ex.StackTrace;
        if (bex != null) {
            err += "\n\n BASE EXCEPTION: " + bex.Message + "\n" + bex.StackTrace;
        }
        if (bex.InnerException != null) {
            err += "\n\n INNER EXCEPTION: " + bex.InnerException.Message + "\n" + bex.InnerException.StackTrace;
        }
        // Put the error in the session.
        if (System.Web.HttpContext.Current.Session != null) {
            System.Web.HttpContext.Current.Session.Add("SERVER_LAST_ERROR", err);
        }
        // DO NOT CLEAR THE ERROR!!
        //Server.ClearError();
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        if (Session.IsNewSession) {
            // Timeout.
            Session.Timeout = 120;

            // DEPRECATED
            // Update the role provider cookie name.
            //SecurityUtils.SetRoleProviderCookieName();

            // DEPRECATED
            // Dynamically update the authentication cookie.
            //SecurityUtils.UpdateAuthCookie();
        }
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

        // Close the session and kill the cookie.
        //FormsAuthentication.GetAuthCookie(Profile.UserName, true).Expires = DateTime.Now;
        //FormsAuthentication.SignOut();
    }
       
</script>
