﻿using System.Web.UI;

// Style Sheets
[assembly: WebResource("ATGCorpSkin.Button.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Calendar.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.ColorPicker.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.ComboBox.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.DataPager.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Dock.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Editor.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.FileExplorer.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Filter.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.FormDecorator.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Grid.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.ImageEditor.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Input.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.ListBox.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.ListView.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Menu.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Notification.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.OrgChart.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.PanelBar.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Rating.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.RibbonBar.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Rotator.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Scheduler.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.SiteMap.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Slider.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.SocialShare.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Splitter.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.TabStrip.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.TagCloud.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.ToolBar.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.ToolTip.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.TreeList.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.TreeView.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Upload.ATGSkin.css", "text/css")]
[assembly: WebResource("ATGCorpSkin.Window.ATGSkin.css", "text/css")]
// IMAGES
[assembly: WebResource("ATGCorpSkin.Button.ButtonSprites.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Button.ToggleSprite.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Calendar.sprite.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ColorPicker.ColorPickerSprites.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ColorPicker.rcpVerticalSprites.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ColorPicker.rcpVerticalSpritesIE6.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ComboBox.rcbSprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ComboBox.rcbSpriteIE6.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Common.CommandSpritesLight.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Common.CommandSpritesLightIE6.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Common.CommonIcons.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Common.loading.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Common.loading_small.gif", "image/gif")]
//[assembly: WebResource("ATGCorpSkin.DataPager.css", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Dock.CommandSprite.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Dock.HorizontalSprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Dock.VerticalSprite.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Editor.ToolBarSprites.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Editor.ToolbarVerticalSprites.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.FileExplorer.FileExplorerToolbarSprites.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.FileExplorer.FileExplorerToolbarSpritesIE6.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Filter.rfLast.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Filter.rfLast-rtl.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Filter.rfLine.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Filter.rfLine-rtl.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Filter.rfMiddle.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Filter.rfMiddle-rtl.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Filter.sprite.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.FormDecorator.ButtonSprites.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.FormDecorator.CheckBoxSprites.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.FormDecorator.ComboSprites.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.FormDecorator.FieldsetBgr.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.FormDecorator.RadioButtonSprites.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.FormDecorator.RadioButtonSprites.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.AddRecord.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.Cancel.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.Delete.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.Edit.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.export.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.Filter.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.MoveDown.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.MoveUp.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.PagingFirst.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.PagingLast.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.PagingNext.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.PagingPrev.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.Refresh.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.rgDrag.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.SingleMinus.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.SinglePlus.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.SortAsc.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.SortDesc.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.sprite.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Grid.Update.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ImageEditor.ImageEditorTools.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ImageEditor.ImageEditorToolsIE6.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Input.sprite.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ListBox.rlbButtonDisabled.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ListBox.rlbButtonDisabledIE6.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ListBox.rlbButtonHover.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ListBox.rlbButtonHoverIE6.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ListBox.rlbButtonNormal.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ListBox.rlbButtonNormalIE6.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ListBox.rlbDropClue.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ListBox.rlbHeaderFooter.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ListBox.rlbSprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ListView.drag.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Menu.rmRootExpand.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Menu.rmRoundedLeft.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Menu.rmRoundedLeft_rtl.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Menu.rmRoundedRight.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Menu.rmRoundedRight_rtl.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Menu.rmSprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Menu.rmVSprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Notification.NotificationSprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.OrgChart.rocItemTileLight.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.OrgChart.rocLinesDark.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.PanelBar.Expandable.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.PanelBar.RootItemBg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.PanelBar.SubItemStates.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Rating.Sprite.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Rating.Sprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.RibbonBar.rrbApplicationTabSprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.RibbonBar.rrbApplicationTabSprite_IE6.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.RibbonBar.rrbArrowSprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.RibbonBar.rrbContextualTabSmoothTile.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.RibbonBar.rrbGradients.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.RibbonBar.rrbTabSprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.RibbonBar.rrbTabSprite_IE6.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Rotator.RotatorButtons.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Scheduler.rsAppointmentBg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Scheduler.rsInlineEditHSprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Scheduler.rsInlineEditVSprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Scheduler.rsModalBg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Scheduler.rsModalBgIE6.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Scheduler.rsModalTitleBg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Scheduler.rsSprites.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.SiteMap.NodeLines.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.SiteMap.rsmHover.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.DragHandle.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.DragHandleUp.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.DragVerticalHandle.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.DragVerticalHandleRight.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.Handles.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.HandlesVertical.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.ItemHorizontalBgr.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.ItemVerticalBgr.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.LargeChangeHorizontal.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.LargeChangeMiddleHorizontal.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.LargeChangeCenterVertical.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.LargeChangeVertical.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.SelectedRegionHorizontalBgr.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.SelectedRegionVerticalBgr.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.SmallChangeCenterVertical.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.SmallChangeHorizontal.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.SmallChangeMiddleHorizontal.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.SmallChangeVertical.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.TrackBgr.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Slider.TrackVerticalBgr.gif", "image/gif")]
//[assembly: WebResource("ATGCorpSkin.SocialShare.css", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Splitter.ExpandCollapseBarsCommands.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Splitter.ResizeBarHorizontalSprites.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Splitter.ResizeBarVerticalSprites.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Splitter.slideTitleContainerBgr.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TabStrip.Level2Bg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TabStrip.TabStripStates.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TabStrip.TabStripVStates.png", "image/gif")]
//[assembly: WebResource("ATGCorpSkin.TagCloud.css", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbActiveBg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbClickedBg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbControlBg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbDropArrows.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbDropDownBg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbDropDownHover.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbHoverBg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbRoundedLeft.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbRoundedLeft_rtl.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbRoundedRight.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbRoundedRight_rtl.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbUpArrows.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolBar.rtbVerticalControlBg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolTip.Callouts.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolTip.ToolTipSprites.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.ToolTip.ToolTipVerticalSprites.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeList.rtlBottom.gift", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeList.rtlBottomRTL.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeList.rtlMiddle.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeList.rtlMiddleRTL.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeList.rtlSingle.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeList.rtlTop.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeList.rtlTopRTL.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeList.sprite.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.BottomLine.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.BottomLine_rtl.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.FirstNodeSpan.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.FirstNodeSpan_rtl.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.ItemHoveredBg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.ItemSelectedBg.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.LoadingIcon.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.MiddleLine.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.MiddleLine_rtl.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.NodeSpan.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.NodeSpan_rtl.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.PlusMinus.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.SingleLine.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.SingleLine_rtl.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.TopLine.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.TopLine_rtl.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.TreeView.TriState.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Upload.AsyncProgress.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Upload.ruProgress.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Upload.ruSprite.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Window.CommandButtonSprites.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Window.WindowHorizontalSprites.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Window.WindowHorizontalSprites.png", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Window.WindowVerticalSprites.gif", "image/gif")]
[assembly: WebResource("ATGCorpSkin.Window.WindowVerticalSprites.png", "image/gif")]
namespace ATGCorpSkin
{
    public class ATGSkin
    {
    }
}
