﻿using System.Net;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Web.Configuration;

/// <summary>
/// Summary description for EmailUtils
/// </summary>
public static class EmailUtils
{
    public static void SendMail(HttpServerUtility server, string pUrlVars) {
        // Create the message.
        MailMessage msg = new MailMessage();
        string emailAddrs = WebConfigurationManager.AppSettings["EMAIL_RECIPIENTS"];

        string addr;
        StringTokenizer st = new StringTokenizer(emailAddrs, ";");
        st.Reset();
        msg.To.Clear();
        while (st.hasMoreTokens()) {
            addr = st.nextToken();
            msg.To.Add(new MailAddress(addr));
        }

        // Make sure at least one address is there.
        if (msg.To.Count <= 0) {
            // Just return, this isn't really an error, but is a feature to disable sending contact emails, in the case of a DOS attack.
            return;
        }

        msg.Subject = "Air to Ground Website Contact Request";
        msg.Body = ""; // no body.
        msg.IsBodyHtml = true;
        msg.ReplyToList.Clear();
        msg.ReplyToList.Add("DoNotReply@airtoground.com");
        msg.Priority = MailPriority.High;

        // Turns out, if you just want to disable certificate validation altogether, you can change the ServerCertificateValidationCallback on the ServicePointManager, like so:
        // Clear down below.
        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; }; 

        // Create a request for the page that you want
        string url = WebConfigurationManager.AppSettings["EMAIL_URI"];
        WebRequest request = System.Net.HttpWebRequest.Create(url + "EmailContact.aspx?" + pUrlVars);

        // Get the processed response
        WebResponse response = request.GetResponse();

        // Get the HTML for the page
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string sHTML = reader.ReadToEnd();
        reader.Close();

        // Get a stream for the HTML.
        MemoryStream stream = new MemoryStream(System.Text.Encoding.ASCII.GetBytes(sHTML));

        // Create a customized view for the HTML.
        AlternateView htmlView = new AlternateView(stream, System.Net.Mime.MediaTypeNames.Text.Html);

        // Link the splash image.
        LinkedResource resource = new LinkedResource((server.MapPath("~/images/SiteSplash.png")), System.Net.Mime.MediaTypeNames.Image.Jpeg);

        // Name the resource
        resource.ContentId = "imgSplash";

        // Add the resource to the alternate view
        htmlView.LinkedResources.Add(resource);

        // Add the style sheet.
        resource = new LinkedResource(server.MapPath("~/Email.css"), System.Net.Mime.MediaTypeNames.Text.Html);

        // Name the resource
        resource.ContentId = "txtStyle";

        // Add the resource to the alternate view
        htmlView.LinkedResources.Add(resource);

        // Add the views to the message.
        msg.AlternateViews.Add(htmlView);

        // Clear
        ServicePointManager.ServerCertificateValidationCallback = null;
            
        SmtpClient smtp = new SmtpClient(); // Uses web.config settings.
        if (smtp.Host.Contains("gmail")) {
            smtp.EnableSsl = true;
            smtp.Port = 587;
        }

        try {
            smtp.Send(msg);
        } catch (System.Exception ex) {
            throw ex;
        }

        // Cleanup.
        stream.Close();
        response.Close();
    }
}
