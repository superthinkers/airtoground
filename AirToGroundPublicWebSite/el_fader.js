/*
A elFader Bit...
website: www.elFader.com
*/


elFaderFOID = new Object();
elFaderFTID = new Object();

function elFader(object, destOp, rate, delta){
if (!document.all)
return
    if (object != "[object]"){
        setTimeout("elFader("+object+","+destOp+","+rate+","+delta+")",0);
        return;
    }
        
    clearTimeout(elFaderFTID[object.sourceIndex]);
    
    diff = destOp-object.filters.alpha.opacity;
    direction = 1;
    if (object.filters.alpha.opacity > destOp){
        direction = -1;
    }
    delta=Math.min(direction*diff,delta);
    object.filters.alpha.opacity+=direction*delta;

    if (object.filters.alpha.opacity != destOp){
        elFaderFOID[object.sourceIndex]=object;
        elFaderFTID[object.sourceIndex]=setTimeout("elFader(elFaderFOID["+object.sourceIndex+"],"+destOp+","+rate+","+delta+")",rate);
    }
}