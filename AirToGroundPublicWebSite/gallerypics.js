/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Written by Bugimus 
	Copyright � 1998-2001 Bugimus, all rights reserved.
	You may use this code for your own *personal* use provided you leave this comment block intact.  
	A link back to Bugimus' page would be much appreciated.  
	http://bugimus.com/
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

function showPic( imgName, imgCaption, imgWidth, imgHeight, textColor, bgColor ) {
	if(textColor=="")textColor="#000000"
	if(bgColor=="")bgColor="#ffffff"
	if(imgWidth<=100)imgWidth=100
	if(imgHeight<=100)imgHeight=100
	winHeight=imgHeight+20;
	w = window.open('','Demo','toolbar=no,location=no,directories=no,status=no,scrollbars=no,resizable=yes,copyhistory=no,width='+imgWidth+',height='+winHeight);
	w.document.write( "<html><head><title>"+imgCaption+"</title>" );
	w.document.write( "<STYLE TYPE='text/css'>" );
	w.document.write( "A {font-family: verdana; font-size: 10px; color: "+textColor+"; text-decoration : none;}" );
	w.document.write( "A:Visited {font-family: verdana;font-size: 10px; color: "+textColor+"; }" );
	w.document.write( "A:Active { font-family: verdana; font-size: 10px; color: "+textColor+"; }" );
	w.document.write( "A:Hover { font-family: verdana; font-size: 10px; color: "+textColor+"; }" );
	w.document.write( "IMG {border-color : "+textColor+";}" );
	w.document.write( "BODY { font-family: verdana; font-size : 10px; font-weight: normal; color : "+textColor+"; background-color : "+bgColor+"; }" );
	w.document.write( "</STYLE>" );
	w.document.write( "<script language='JavaScript'>\n");
	w.document.write( "IE5=NN4=NN6=false\n");
	w.document.write( "if(document.all)IE5=true\n");
	w.document.write( "else if(document.layers)NN4=true\n");
	w.document.write( "else if(document.getElementById)NN6=true\n");
	w.document.write( "function autoSize() {\n");
	w.document.write( "	if(IE5) self.resizeTo(document.images[0].width+10,document.images[0].height+41+20)\n");
	w.document.write( "	else if(NN6) self.sizeToContent()\n");
	w.document.write( "	else top.window.resizeTo(document.images[0].width,document.images[0].height+20)\n");
	w.document.write( "	self.focus()\n");
	w.document.write( "}\n");
	w.document.write( "</script>\n");
	w.document.write( "</head><body leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad=" );
	w.document.write( "'javascript:autoSize();'>" );
	w.document.write( "<table cellpadding=0 cellspacing=0 border=0><tr><td colspan=3><img src='"+imgName+"' border=0 alt='"+imgCaption+"'></td></tr>" );
	w.document.write( "<tr><td align='left'>&nbsp;&nbsp;<a>&copy; Air To Ground.com</a></td>" );
	w.document.write( "<td align='center'>&nbsp;</td>" );
	w.document.write( "<td align='right'><a href='javascript:top.window.close();'>close window</a>&nbsp;&nbsp;</td></tr>" );
	w.document.write( "</table></body></html>" );
	w.document.close();
}

