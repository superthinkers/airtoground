﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SendEmail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Help prevent DOS attack.
        if (Request.Form.ToString().Contains("subscriber_name") == false) {
            Response.Redirect("~/error.shtml");
        }
        EmailUtils.SendMail(Server, Request.Form.ToString());
        // 3, subscriber_name
        // 4, last_name
        // 5, company
        // 6, phone
        // 7, subscriber_email
        // 8, address
        // 9, city
        // 10, state
        // 11, zip
        // 12, message
        Response.Redirect("~/contact_thanks.shtml");
    }
}