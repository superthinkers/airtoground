﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmailContact.aspx.cs" Inherits="EmailNotifications_PasswordChanged" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="cid:txtStyle" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="div_main">
            <div style="width: 100%">
                <div style="height: 76px; top: 0px">
                    <img src="cid:imgSplash" id="imgSplash" runat="server" alt="Air to Ground" style="height: 76px;
                        top: 0px" />
                </div>
                <br />
                <br />
                <table class="div_container" style="width: 600px">
                    <tr>
                        <td>
                            <div class="div_header">Website Contact Request</div>
                            <br />
                            <table cellpadding="0" cellspacing="9" border="0" align="center" width="100%">
                                <tr>
                                    <td valign="top" align="left" colspan="2">
                                        &nbsp;&nbsp;&nbsp;<asp:Label ID="lblDateTime" runat="server" CssClass="lblSubscript" Text=""></asp:Label>
                                    </td>
                                </tr>
		                        <tr>
                                    <td valign="top" align="right" class="body" style="width: 100px">
                                        <asp:Label ID="lblFirstName1" runat="server" CssClass="lblInfo" Text="First Name:"></asp:Label>
                                    </td>
                                    <td valign="top" class="body">
                                        <asp:Label ID="lblFirstName2" runat="server" CssClass="lblInfoBold" Text=""></asp:Label>
                                    </td>
                                </tr>
		                        <tr>
                                    <td valign="top" align="right" class="body">
                                        <asp:Label ID="lblLastName1" runat="server" CssClass="lblInfo" Text="Last Name:"></asp:Label>
                                    </td>
                                    <td valign="top" class="body">
                                        <asp:Label ID="lblLastName2" runat="server" CssClass="lblInfoBold" Text=""></asp:Label>
                                    </td>
                                </tr>
		                        <tr>
                                    <td valign="top" align="right" class="body">
                                        <asp:Label ID="lblCompany1" runat="server" CssClass="lblInfo" Text="Company:"></asp:Label>
                                    </td>
                                    <td valign="top" class="body">
                                        <asp:Label ID="lblCompany2" runat="server" CssClass="lblInfoBold" Text=""></asp:Label>
                                    </td>
                                </tr>
		                        <tr>
                                    <td valign="top" align="right" class="body">
                                        <asp:Label ID="lblPhone1" runat="server" CssClass="lblInfo" Text="Phone:"></asp:Label>
                                    </td>
                                    <td valign="top" class="body">
                                        <asp:Label ID="lblPhone2" runat="server" CssClass="lblInfoBold" Text=""></asp:Label>
                                    </td>
                                </tr>
		                        <tr>
                                    <td valign="top" align="right" class="body">
                                        <asp:Label ID="lblEmail1" runat="server" CssClass="lblInfo" Text="Email:"></asp:Label>
                                    </td>
                                    <td valign="top" class="body">
                                        <asp:Label ID="lblEmail2" runat="server" CssClass="lblInfoBold" Text=""></asp:Label>
                                    </td>
                                </tr>
		                        <tr>
                                    <td valign="top" align="right" class="body">
                                        <asp:Label ID="lblAddress1" runat="server" CssClass="lblInfo" Text="Address:"></asp:Label>
                                    </td>
                                    <td valign="top" class="body">
                                        <asp:Label ID="lblAddress2" runat="server" CssClass="lblInfoBold" Text=""></asp:Label>
                                    </td>
                                </tr>
		                        <tr>
                                    <td valign="top" align="right" class="body">
                                        <asp:Label ID="lblCity1" runat="server" CssClass="lblInfo" Text="City:"></asp:Label>
                                    </td>
                                    <td valign="top" class="body">
                                        <asp:Label ID="lblCity2" runat="server" CssClass="lblInfoBold" Text=""></asp:Label>
                                    </td>
                                </tr>
		                        <tr>
                                    <td valign="top" align="right" class="body">
                                        <asp:Label ID="lblState1" runat="server" CssClass="lblInfo" Text="State:"></asp:Label>
                                    </td>
                                    <td valign="top" class="body">
                                        <asp:Label ID="lblState2" runat="server" CssClass="lblInfoBold" Text=""></asp:Label>
                                    </td>
                                </tr>
		                        <tr>
                                    <td valign="top" align="right" class="body">
                                        <asp:Label ID="lblZip1" runat="server" CssClass="lblInfo" Text="Zip:"></asp:Label>
                                    </td>
                                    <td valign="top" class="body">
                                        <asp:Label ID="lblZip2" runat="server" CssClass="lblInfoBold" Text=""></asp:Label>
                                    </td>
                                </tr>
		                        <tr>
                                    <td valign="top" align="right" class="body">
                                        <asp:Label ID="lblMessage1" runat="server" CssClass="lblInfo" Text="Message:"></asp:Label>
                                    </td>
                                    <td valign="top" class="body">
                                        <asp:Label ID="lblMessage2" runat="server" CssClass="lblInfoBold" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <br />
                            <br />
                        </td>
                    </tr>
                </table>
                <br />
            </div>
        </div>
    </form>
</body>
</html>
