﻿using System;
//using System.Web.Security;

public partial class EmailNotifications_PasswordChanged : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            lblDateTime.Text = "On: " + DateTime.Now.ToShortDateString() + " at: " + DateTime.Now.ToShortTimeString();
            // 3, subscriber_name
            // 4, last_name
            // 5, company
            // 6, phone
            // 7, subscriber_email
            // 8, address
            // 9, city
            // 10, state
            // 11, zip
            // 12, message
            if (!String.IsNullOrEmpty(Request.QueryString["subscriber_name"])) {
                lblFirstName2.Text = Request.QueryString["subscriber_name"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["last_name"])) {
                lblLastName2.Text = Request.QueryString["last_name"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["company"])) {
                lblCompany2.Text = Request.QueryString["company"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["phone"])) {
                lblPhone2.Text = Request.QueryString["phone"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["subscriber_email"])) {
                lblEmail2.Text = Request.QueryString["subscriber_email"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["address"])) {
                lblAddress2.Text = Request.QueryString["address"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["city"])) {
                lblCity2.Text = Request.QueryString["city"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["state"])) {
                lblState2.Text = Request.QueryString["state"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["zip"])) {
                lblZip2.Text = Request.QueryString["zip"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["message"])) {
                lblMessage2.Text = Request.QueryString["message"];
            }
        }
    }
}