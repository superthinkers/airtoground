﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Resources;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web;
using System.Web.Compilation;
using System.CodeDom.Compiler;
using System.CodeDom;
using System.Security;
using System.Security.Permissions;

namespace WebResourceAutoBuilder
{
    /// <summary>
    /// Generates an assembly for telerik web resources.
    /// </summary>
    /// <remarks>
    /// Author: Bryan Boudreau
    /// Web: www.vitalz.com, www.fusionworlds.com, www.facebook.com/banshi
    /// Date: 08/03/2009
    /// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
    /// and associated documentation files (the "Software"), to deal in the Software without restriction, 
    /// including without limitation the rights to use, copy, modify, merge, publish, distribute, 
    /// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
    /// furnished to do so, subject to the following conditions:
    /// The above copyright notice and this permission notice shall be included in all copies or 
    /// substantial portions of the Software.
    /// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    /// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    /// </remarks>
    public class AssemblyGenerator
    {
        #region < Members & Properties >

        /// <summary>
        /// 
        /// </summary>
        private string _namespace;
        /// <summary>
        /// 
        /// </summary>
        private string _skinFileRootFolder;
        /// <summary>
        /// 
        /// </summary>
        private ModuleBuilder _mb;
        /// <summary>
        /// 
        /// </summary>
        private System.Reflection.Emit.AssemblyBuilder _myAsmBuilder;

        #endregion

        #region < Methods >

        /// <summary>
        /// Generates this instance of the assembly for the web resources
        /// </summary>
        /// <param name="webResourceArr">The web resource array for the web resource attributes needed</param>
        public void Generate(Dictionary<string, string> webResourceArr)
        {
            string resourceName = string.Empty;
            string myAsmFileName = string.Format("{0}.dll", _namespace);
            string telerikClassTypeName = string.Format("{0}.TelerikSkin", _namespace);

            DirectoryInfo rootFolder = new DirectoryInfo(_skinFileRootFolder);
            DirectoryInfo[] dirs = rootFolder.GetDirectories();
            FileInfo[] rootFiles = rootFolder.GetFiles();

            AppDomain appDomain = Thread.GetDomain();

            AssemblyName asmName = new AssemblyName();
            asmName.Name = _namespace;
            asmName.CodeBase = _skinFileRootFolder;

            //asmName.CultureInfo = new CultureInfo("en");
            asmName.Version = new Version("1.0.0.0");

            //create the assembly definition and generate the module builder
            _myAsmBuilder = appDomain.DefineDynamicAssembly(asmName, AssemblyBuilderAccess.RunAndSave, _skinFileRootFolder);

            //add the web resource attributes to the assembly
            foreach (string key in webResourceArr.Keys)
            {
                Type[] ctorParams = new Type[] { typeof(string), typeof(string) };

                ConstructorInfo classCtorInfo = typeof(WebResourceAttribute).GetConstructor(ctorParams);

                CustomAttributeBuilder myCABuilder = new CustomAttributeBuilder(
                                    classCtorInfo,
                                    new object[] { key, webResourceArr[key] });

                _myAsmBuilder.SetCustomAttribute(myCABuilder);
            }

            //define the module for the assembly
            _mb = _myAsmBuilder.DefineDynamicModule(myAsmFileName, myAsmFileName);

            //process any of the root files into the assembly as resources
            ProcessFilesIntoResourceManifest(rootFiles, _namespace);

            //navigate the sub directories and process the files
            foreach (DirectoryInfo dir in dirs)
            {
                var namespaceText = string.Format("{0}.{1}", _namespace, dir.Name);
                GenerateSub(dir, namespaceText);
            }

            //create the telerik type definition so that the web resources will work.
            TypeBuilder tb = _mb.DefineType(telerikClassTypeName, TypeAttributes.Public);
            tb.CreateType();

            //finally save the assembly
            _myAsmBuilder.Save(myAsmFileName);

        }

        /// <summary>
        /// Recursive method that generates the sub directory resources
        /// </summary>
        /// <param name="parentFolder">The parent folder.</param>
        /// <param name="namespaceText">The namespace text.</param>
        private void GenerateSub(DirectoryInfo parentFolder, string namespaceText)
        {
            FileInfo[] files = parentFolder.GetFiles();

            //process the resources in this folder
            ProcessFilesIntoResourceManifest(files, namespaceText);

            DirectoryInfo[] subDirs = parentFolder.GetDirectories();
            foreach (DirectoryInfo subDir in subDirs)
            {
                var newNamespaceText = string.Format("{0}.{1}", namespaceText, subDir.Name);
                GenerateSub(subDir, newNamespaceText);
            }
        }

        /// <summary>
        /// Processes the files into resource manifest.
        /// </summary>
        /// <param name="files">The files.</param>
        /// <param name="namespaceText">The namespace text.</param>
        private void ProcessFilesIntoResourceManifest(FileInfo[] files, string namespaceText)
        {
            string name;
            foreach (FileInfo file in files)
            {
                name = string.Format("{0}.{1}", namespaceText, file.Name);
                _mb.DefineManifestResource(name, file.Open(FileMode.Open, FileAccess.Read), ResourceAttributes.Public);
            }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AssemblyGenerator"/> class.
        /// </summary>
        /// <param name="namespaceText">The namespace text.</param>
        /// <param name="rootFolderName">Name of the root folder.</param>
        public AssemblyGenerator(string namespaceText, string rootFolderName)
        {
            _namespace = namespaceText;
            _skinFileRootFolder = rootFolderName;
        }

        #region < Events & Handlers >

        #endregion

    }

}
