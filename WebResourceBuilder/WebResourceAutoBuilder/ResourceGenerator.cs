﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;

namespace WebResourceAutoBuilder
{
    /// <summary>
    /// Generates web resources and corrects css urls with web resource urls for telerik web resources.
    /// </summary>
    /// <remarks>
    /// Author: Bryan Boudreau
    /// Web: www.vitalz.com, www.fusionworlds.com, www.facebook.com/banshi
    /// Date: 08/03/2009
    /// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
    /// and associated documentation files (the "Software"), to deal in the Software without restriction, 
    /// including without limitation the rights to use, copy, modify, merge, publish, distribute, 
    /// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
    /// furnished to do so, subject to the following conditions:
    /// The above copyright notice and this permission notice shall be included in all copies or 
    /// substantial portions of the Software.
    /// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    /// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    /// </remarks>
    public class ResourceGenerator
    {

        #region < Members & Properties >

        /// <summary>
        /// 
        /// </summary>
        private StringBuilder _results;

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, string> _webResourceArr;

        /// <summary>
        /// 
        /// </summary>
        private string _namespace;

        /// <summary>
        /// 
        /// </summary>
        private string _folderPath;

        /// <summary>
        /// 
        /// </summary>
        private string _outputFolderPath;

        /// <summary>
        /// 
        /// </summary>
        private string _skinPackageName;

        /// <summary>
        /// 
        /// </summary>
        private readonly string cssMime = "text/css";
        /// <summary>
        /// 
        /// </summary>
        private readonly string gifMime = "image/gif";
        /// <summary>
        /// 
        /// </summary>
        private readonly string jpgMime = "image/jpg";
        /// <summary>
        /// 
        /// </summary>
        private readonly string pngMime = "image/png";
        /// <summary>
        /// 
        /// </summary>
        private readonly string assemLine = "[assembly: WebResource(\"{0}\", \"{1}\")]";

        /// <summary>
        /// 
        /// </summary>
        private readonly string webResourceLine = "url(<%= WebResource(\"{0}\") %>)";

        /// <summary>
        /// 
        /// </summary>
        private Regex rx = new Regex(@"url\((.+)\)");

        /// <summary>
        /// Gets the results.
        /// </summary>
        /// <value>The results.</value>
        public string Results
        {
            get { return _results.ToString(); }
        }

        /// <summary>
        /// Gets the web resource arr.
        /// </summary>
        /// <value>The web resource arr.</value>
        public Dictionary<string, string> WebResourceArr
        {
            get { return _webResourceArr; }
        }

        #endregion

        #region < Methods >

        /// <summary>
        /// Gets the type of the correct MIME.
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        private string GetCorrectMimeType(string extension)
        {
            string retVal = cssMime;

            switch (extension.ToLower())
            {
                case ".css":

                    retVal = cssMime;
                    break;
                case ".jpg":

                    retVal = jpgMime;
                    break;
                case ".gif":

                    retVal = gifMime;
                    break;
                case ".png":

                    retVal = pngMime;
                    break;
            }

            return retVal;
        }

        /// <summary>
        /// Fixes the resource CSS.
        /// </summary>
        /// <param name="cssFilePath">The CSS file path.</param>
        private void FixResourceCss(string cssFilePath)
        {
            string[] cssFolderImage = new string[2];
            string fullNamespaceWithFileName = string.Empty;
            string innerOldUrl = string.Empty;
            DirectoryInfo rootDir = new DirectoryInfo(_folderPath);
            StreamReader s = File.OpenText(cssFilePath);
            string read = s.ReadToEnd();
            s.Close();
            string str = rx.Replace(
                read,
                 new MatchEvaluator(delegate(Match m)
                 {
                     if (m.Groups.Count == 2)
                     {
                         innerOldUrl = m.Value.Replace("url('", string.Empty).Replace("')", string.Empty);
                         cssFolderImage = innerOldUrl.Split(new char[] { '/' });
                         if (cssFolderImage.Length > 1)
                         {
                             fullNamespaceWithFileName = string.Format("{0}.{1}.{2}", _namespace, cssFolderImage[0], cssFolderImage[1]);
                             return string.Format(webResourceLine, fullNamespaceWithFileName.ToString());
                         }
                         else
                         {
                             throw new Exception("The skin package has already been processed.");
                         }
                     }
                     return m.Value;
                 }
                 ));
            // Write the modification into the same file
            StreamWriter streamWriter = File.CreateText(cssFilePath);
            streamWriter.Write(str);
            streamWriter.Close();
        }

        /// <summary>
        /// Generates this instance of the web resources and then calls the css fix methods
        /// </summary>
        public void Generate()
        {
            _results = new StringBuilder();
            _webResourceArr = new Dictionary<string, string>();
            DirectoryInfo rootDir = new DirectoryInfo(_folderPath);
            DirectoryInfo[] subDirs = rootDir.GetDirectories();
            FileInfo[] rootFiles = rootDir.GetFiles();
            _namespace = string.Format("{0}.{1}", _namespace, rootDir.Name);
            bool isOut = false;
            foreach (FileInfo rootFile in rootFiles)
            {
                var fullNamespaceWithFileName = string.Format("{0}.{1}", _namespace, rootFile.Name);
                var assemNamespace = String.Format(assemLine, fullNamespaceWithFileName, GetCorrectMimeType(rootFile.Extension));
                _webResourceArr.Add(fullNamespaceWithFileName, GetCorrectMimeType(rootFile.Extension));

                _results.Append(assemNamespace);
                _results.Append("\r\n");

                if (rootFile.Extension.Contains("css"))
                {
                    try
                    {
                        FixResourceCss(rootFile.FullName);
                    }
                    catch (Exception e)
                    {
                        isOut = true;
                        _results = new StringBuilder();
                        _results.Append(e.Message);
                        break;
                    }
                }
            }
            if (!isOut)
            {
                foreach (DirectoryInfo subDir in subDirs)
                {
                    var namespaceText = string.Format("{0}.{1}", _namespace, subDir.Name);
                    GenerateSub(subDir, namespaceText);
                }
            }
        }

        /// <summary>
        /// Recursive method that generates the sub directory resources
        /// </summary>
        /// <param name="parentFolder">The parent folder.</param>
        /// <param name="namespaceText">The namespace text.</param>
        private void GenerateSub(DirectoryInfo parentFolder, string namespaceText)
        {
            FileInfo[] files = parentFolder.GetFiles();
            foreach (FileInfo dirFile in files)
            {
                var fullNamespaceWithFileName = string.Format("{0}.{1}", namespaceText, dirFile.Name);
                var assemNamespace = String.Format(assemLine, fullNamespaceWithFileName, GetCorrectMimeType(dirFile.Extension));
                _webResourceArr.Add(fullNamespaceWithFileName, GetCorrectMimeType(dirFile.Extension));
                _results.Append(assemNamespace);
                _results.Append("\r\n");
            }
            DirectoryInfo[] subDirs = parentFolder.GetDirectories();
            foreach (DirectoryInfo subDir in subDirs)
            {
                var newNamespaceText = string.Format("{0}.{1}", namespaceText, subDir.Name);
                GenerateSub(subDir, newNamespaceText);
            }
        }

        /// <summary>
        /// Unzips the package.
        /// </summary>
        private void UnzipAndProcessPackage()
        {
            try
            {
                CreateFolder(_outputFolderPath);
                FastZip fastZip = new FastZip();
                fastZip.ExtractZip(_skinPackageName, _outputFolderPath, string.Empty);
                _folderPath = _outputFolderPath;
                Generate();
            }
            catch (Exception e)
            {
                _results = new StringBuilder();
                _results.Append(e.Message);
            }
        }

        /// <summary>
        /// Creates the folder.
        /// </summary>
        /// <param name="folderName">Name of the folder.</param>
        /// <returns></returns>
        private DirectoryInfo CreateFolder(string folderName)
        {
            if (Directory.Exists(folderName))
            {
                return new DirectoryInfo(folderName);
            }
            else
            { return Directory.CreateDirectory(folderName); }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceGenerator"/> class.
        /// </summary>
        /// <param name="namespaceText">The namespace text.</param>
        /// <param name="outputFolderPathText">The output folder path text.</param>
        /// <param name="skinPackageName">Name of the skin package.</param>
        public ResourceGenerator(string namespaceText, string outputFolderPathText, string skinPackageName)
        {
            _namespace = namespaceText;
            _outputFolderPath = outputFolderPathText;
            _skinPackageName = skinPackageName;
            UnzipAndProcessPackage();
        }
    }
}
