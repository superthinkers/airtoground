﻿namespace WebResourceAutoBuilder
{
    partial class WebResourceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.GenerateButton = new System.Windows.Forms.Button();
            this.ResultsText = new System.Windows.Forms.TextBox();
            this.NamespaceText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SkinPackageText = new System.Windows.Forms.TextBox();
            this.SelectSkinPackageButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.OutputFolderText = new System.Windows.Forms.TextBox();
            this.SelectOutputFolderButton = new System.Windows.Forms.Button();
            this.FileBrowserDialog = new System.Windows.Forms.OpenFileDialog();
            this.GenerateAssemblyCheck = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // GenerateButton
            // 
            this.GenerateButton.Location = new System.Drawing.Point(442, 62);
            this.GenerateButton.Name = "GenerateButton";
            this.GenerateButton.Size = new System.Drawing.Size(60, 23);
            this.GenerateButton.TabIndex = 2;
            this.GenerateButton.Text = "Generate";
            this.GenerateButton.UseVisualStyleBackColor = true;
            this.GenerateButton.Click += new System.EventHandler(this.GenerateButton_Click);
            // 
            // ResultsText
            // 
            this.ResultsText.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ResultsText.Location = new System.Drawing.Point(0, 91);
            this.ResultsText.Multiline = true;
            this.ResultsText.Name = "ResultsText";
            this.ResultsText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ResultsText.Size = new System.Drawing.Size(750, 303);
            this.ResultsText.TabIndex = 3;
            // 
            // NamespaceText
            // 
            this.NamespaceText.Location = new System.Drawing.Point(114, 64);
            this.NamespaceText.Name = "NamespaceText";
            this.NamespaceText.Size = new System.Drawing.Size(322, 20);
            this.NamespaceText.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Namespace:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Skin Zip Package:";
            // 
            // SkinPackageText
            // 
            this.SkinPackageText.Location = new System.Drawing.Point(114, 12);
            this.SkinPackageText.Name = "SkinPackageText";
            this.SkinPackageText.Size = new System.Drawing.Size(322, 20);
            this.SkinPackageText.TabIndex = 8;
            // 
            // SelectSkinPackageButton
            // 
            this.SelectSkinPackageButton.Location = new System.Drawing.Point(442, 10);
            this.SelectSkinPackageButton.Name = "SelectSkinPackageButton";
            this.SelectSkinPackageButton.Size = new System.Drawing.Size(29, 23);
            this.SelectSkinPackageButton.TabIndex = 7;
            this.SelectSkinPackageButton.Text = "...";
            this.SelectSkinPackageButton.UseVisualStyleBackColor = true;
            this.SelectSkinPackageButton.Click += new System.EventHandler(this.SelectSkinPackageButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Ouput Folder:";
            // 
            // OutputFolderText
            // 
            this.OutputFolderText.Location = new System.Drawing.Point(114, 38);
            this.OutputFolderText.Name = "OutputFolderText";
            this.OutputFolderText.Size = new System.Drawing.Size(322, 20);
            this.OutputFolderText.TabIndex = 14;
            // 
            // SelectOutputFolderButton
            // 
            this.SelectOutputFolderButton.Location = new System.Drawing.Point(442, 36);
            this.SelectOutputFolderButton.Name = "SelectOutputFolderButton";
            this.SelectOutputFolderButton.Size = new System.Drawing.Size(29, 23);
            this.SelectOutputFolderButton.TabIndex = 13;
            this.SelectOutputFolderButton.Text = "...";
            this.SelectOutputFolderButton.UseVisualStyleBackColor = true;
            this.SelectOutputFolderButton.Click += new System.EventHandler(this.SelectOutputFolderButton_Click);
            // 
            // FileBrowserDialog
            // 
            this.FileBrowserDialog.Filter = "Skin Packages|*.zip";
            this.FileBrowserDialog.Multiselect = true;
            this.FileBrowserDialog.Title = "Select the skin packages to process";
            // 
            // GenerateAssemblyCheck
            // 
            this.GenerateAssemblyCheck.AutoSize = true;
            this.GenerateAssemblyCheck.Checked = true;
            this.GenerateAssemblyCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GenerateAssemblyCheck.Location = new System.Drawing.Point(509, 66);
            this.GenerateAssemblyCheck.Name = "GenerateAssemblyCheck";
            this.GenerateAssemblyCheck.Size = new System.Drawing.Size(117, 17);
            this.GenerateAssemblyCheck.TabIndex = 16;
            this.GenerateAssemblyCheck.Text = "Generate Assembly";
            this.GenerateAssemblyCheck.UseVisualStyleBackColor = true;
            // 
            // WebResourceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 394);
            this.Controls.Add(this.GenerateAssemblyCheck);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.OutputFolderText);
            this.Controls.Add(this.SelectOutputFolderButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SkinPackageText);
            this.Controls.Add(this.SelectSkinPackageButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NamespaceText);
            this.Controls.Add(this.ResultsText);
            this.Controls.Add(this.GenerateButton);
            this.Name = "WebResourceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fission Skin Conversion Tool";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog FolderBrowserDialog;
        private System.Windows.Forms.Button GenerateButton;
        private System.Windows.Forms.TextBox ResultsText;
        private System.Windows.Forms.TextBox NamespaceText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox SkinPackageText;
        private System.Windows.Forms.Button SelectSkinPackageButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox OutputFolderText;
        private System.Windows.Forms.Button SelectOutputFolderButton;
        private System.Windows.Forms.OpenFileDialog FileBrowserDialog;
        private System.Windows.Forms.CheckBox GenerateAssemblyCheck;
    }
}

