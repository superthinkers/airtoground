﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WebResourceAutoBuilder
{
    /// <summary>
    /// Main windows form to compile and embed resources
    /// </summary>
    /// <remarks>
    /// Author: Bryan Boudreau
    /// Web: www.vitalz.com, www.fusionworlds.com, www.facebook.com/banshi
    /// Date: 08/03/2009
    /// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
    /// and associated documentation files (the "Software"), to deal in the Software without restriction, 
    /// including without limitation the rights to use, copy, modify, merge, publish, distribute, 
    /// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
    /// furnished to do so, subject to the following conditions:
    /// The above copyright notice and this permission notice shall be included in all copies or 
    /// substantial portions of the Software.
    /// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    /// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    /// </remarks>
    public partial class WebResourceForm : Form
    {

        #region < Members & Properties >

        #endregion

        #region < Methods >

        /// <summary>
        /// Processes the and generate the web resources.
        /// </summary>
        private void ProcessAndGenerate()
        {
            string[] files = SkinPackageText.Text.Split(new char[] { ',' });
            Dictionary<string, string> webResArr = new Dictionary<string, string>();
            Dictionary<string, string> tempWebResArr = new Dictionary<string, string>();

            foreach (string file in files)
            {
                var fileName = Path.GetFileName(file).Replace(".zip", string.Empty);
                var outputFolder = Path.Combine(OutputFolderText.Text, fileName);
                ResourceGenerator generator = new ResourceGenerator(NamespaceText.Text, outputFolder, file);
                ResultsText.Text += generator.Results;
                webResArr = webResArr.Concat(generator.WebResourceArr).ToDictionary(e => e.Key, e => e.Value);
            }
            if (GenerateAssemblyCheck.Checked)
            {
                AssemblyGenerator assemGenerator = new AssemblyGenerator(NamespaceText.Text, OutputFolderText.Text);
                assemGenerator.Generate(webResArr);
            }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Form1"/> class.
        /// </summary>
        public WebResourceForm()
        {
            InitializeComponent();
        }

        #region < Events & Handlers >

        /// <summary>
        /// Handles the Click event of the GenerateButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void GenerateButton_Click(object sender, EventArgs e)
        {
            ProcessAndGenerate();
        }

        /// <summary>
        /// Handles the Click event of the SelectSkinPackageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void SelectSkinPackageButton_Click(object sender, EventArgs e)
        {
            if (FileBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                this.SkinPackageText.Text = string.Join(",", FileBrowserDialog.FileNames);
            }
        }

        /// <summary>
        /// Handles the Click event of the SelectOutputFolderButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void SelectOutputFolderButton_Click(object sender, EventArgs e)
        {
            if (FolderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                this.OutputFolderText.Text = FolderBrowserDialog.SelectedPath;
            }
        }

        #endregion

    }
}
